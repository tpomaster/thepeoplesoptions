<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'member', 'namespace' => 'Member'], function () {
    Route::get('/login', 'LoginController@showLoginForm')->name('member.login');
    Route::post('/login', 'LoginController@loginAjax')->name('member.login.ajax');
    Route::post('/login-code', 'LoginController@loginStep2Ajax')->name('member.login2.ajax');

    Route::get('/forgot-password', 'ForgotPasswordController@showForgotPasswordForm')->name('member.forgotPassword');
    Route::post('/forgot-password', 'ForgotPasswordController@sendPasswordResetLink')->name('member.forgotPassword.submit');
    Route::post('/set-new-password', 'ForgotPasswordController@setNewPassword')->name('member.setNewPassword.submit');

    Route::get('/{avatar}/register', 'RegistrationController@showRegisterForm')->name('member.register');
    Route::post('/register', 'RegistrationController@registerAjax')->name('member.register.submit');

    Route::get('/logout', 'DashboardController@logout')->name('member.logout');

    # my account
    Route::get('/my-account', 'MyAccountController@index')->name('member.myAccount');
    Route::post('/update-my-account', 'MyAccountController@updateMyAccount')->name('member.updateMyAccount');
    Route::post('/update-bts-account', 'MyAccountController@updateBtsAccount')->name('member.updateBtsAccount');
    Route::post('/tpom-login', 'MyAccountController@tpoMarketingLogin')->name('member.tpoMarketingLogin');

    # team
    Route::get('/team', 'TeamController@index')->name('member.team');
    Route::post('/get-team', 'TeamController@getTeam')->name('member.team.ajax');
    Route::post('/get-team-2', 'TeamController@getTeam2')->name('member.team2.ajax');

    # tpo bank
    Route::get('/tpo-bank', 'TpoBankController@index')->name('member.tpoBank');
    Route::post('/tpo-bank/add-fund', 'TpoBankController@addFund')->name('member.addFund');
    Route::post('/tpo-bank/cancel-addfund', 'TpoBankController@cancelAddFund')->name('member.cancelAddFund');
    Route::get('/tpo-bank/bank-transactions', 'TpoBankController@getBankTransactions')->name('member.bankTransactionsGet');
    Route::post('/tpo-bank/bank-transactions', 'TpoBankController@getBankTransactions')->name('member.bankTransactions');

    # payment gateway ipn
    Route::get('/process-coinpayments-ipn', 'ProcessIPNController@coinpaymentsIPN');
    Route::post('/process-coinpayments-ipn', 'ProcessIPNController@coinpaymentsIPN')->name('member.processCoinpaymentsIpn');
    Route::get('/coinpayments-ipn-test', 'ProcessIPNController@coinpaymentsIpnTest');


    # add fund via bitpal
    Route::post('/tpo-bank/bitpal-add-fund', 'BitpalAddFundController@addFund')->name('member.tpoBankBitpalAddFund');
    Route::get('/oauth-code/', 'BitpalAddFundController@oAuthCode')->name('member.tpoBankBitpalOAuthCode');
    Route::post('/exchange-rate/', 'BitpalAddFundController@getExchangeRate')->name('member.tpoBankBitpalExchangeRate');

    # withdraw / payout
    Route::post('/tpo-bank/withdraw', 'PayoutController@processPayoutRequest')->name('member.processPayoutRequest');
    Route::post('/tpo-bank/cancel-withdraw', 'PayoutController@cancelPayout')->name('member.cancelPayout');


    # crypto trading
    Route::get('/packages/crypto-trading/', 'CryptoTradingController@index')->name('member.crypto-trading-pacakges-list');
    Route::get('/packages/crypto-trading/my-packages-2/', 'CryptoTradingController@myPackages')->name('member.crypto-trading-my-packages-list');
    Route::get('/packages/crypto-trading/{status}', 'CryptoTradingController@viewPackagesByStatus')->name('member.crypto-trading-packages-available');
    Route::post('/packages/crypto-trading/', 'CryptoTradingController@viewPackagesAjax')->name('member.crypto-trading-pacakges-list-ajax');
    Route::get('/packages/crypto-trading/details/{packageId}', 'CryptoTradingController@viewPackageDetails')->name('member.crypto-trading-package-details');
    Route::post('/packages/crypto-trading/process-order/', 'CryptoTradingController@processOrder')->name('member.cryptoTradingProcessOrder');
    Route::post('/packages/crypto-trading/push-fund-to-bank/', 'CryptoTradingController@pushFundToBank')->name('member.cryptoTradingPushFundToBank');

    # crypto trading trades
    Route::post('/packages/crypto-trading/user-trades/', 'CryptoTradingTradeController@getUserTrades')->name('member.cTradingGetUserTrades');
    Route::post('/packages/crypto-trading/user-trades/pushtobank/', 'CryptoTradingTradeController@pushToBank')->name('member.cTradingUserTradesPushToBank');
    Route::post('/packages/crypto-trading/cap-off-trade-amount/', 'CryptoTradingTradeController@capOffTradeAmount')->name('member.cTradingCapOffTradeAmount');


    # crypto mining
    Route::get('/packages/crypto-mining/', 'CryptoMiningController@index')->name('member.cryptoMiningPackagesList');
    Route::post('/packages/crypto-mining/', 'CryptoMiningController@viewPackagesAjax')->name('member.cryptoMiningPackagesListAjax');
    Route::get('/packages/crypto-mining/details/{packageId}', 'CryptoMiningController@viewPackageDetails')->name('member.cryptoMiningPackageDetails');
    Route::post('/packages/crypto-mining/process-order/', 'CryptoMiningController@processOrder')->name('member.cryptoMiningProcessOrder');
    Route::get('/packages/crypto-mining/my-packages/', 'CryptoMiningController@myPackages')->name('member.cryptoMiningMyPackagesList');
    Route::get('/packages/crypto-mining/{status}', 'CryptoMiningController@viewPackagesByStatus')->name('member.cryptoMiningPackagesAvailable');
    Route::post('/packages/crypto-mining/push-fund-to-bank/', 'CryptoMiningController@pushFundToBank')->name('member.cryptoMiningPushFundToBank');

    # crypto mining profits
    Route::post('/packages/crypto-mining/user-profits/', 'CryptoMiningProfitController@getUserProfits')->name('member.cMiningGetUserProfits');
    Route::post('/packages/crypto-mining/user-profits/pushtobank/', 'CryptoMiningProfitController@pushToBank')->name('member.cMiningUserProfitsPushToBank');


    # crypto exchange
    Route::get('/packages/crypto-exchange/', 'CryptoExchangeController@index')->name('member.cryptoExchangePackagesList');
    Route::post('/packages/crypto-exchange/', 'CryptoExchangeController@viewPackagesAjax')->name('member.cryptoExchangePackagesListAjax');
    Route::get('/packages/crypto-exchange/details/{packageId}', 'CryptoExchangeController@viewPackageDetails')->name('member.cryptoExchangePackageDetails');
    Route::post('/packages/crypto-exchange/process-order/', 'CryptoExchangeController@processOrder')->name('member.cryptoExchangeProcessOrder');
    Route::get('/packages/crypto-exchange/my-packages/', 'CryptoExchangeController@myPackages')->name('member.cryptoExchangeMyPackagesList');
    Route::get('/packages/crypto-exchange/{status}', 'CryptoExchangeController@viewPackagesByStatus')->name('member.cryptoExchangePackagesAvailable');
    Route::post('/packages/crypto-exchange/push-fund-to-bank/', 'CryptoExchangeController@pushFundToBank')->name('member.cryptoExchangePushFundToBank');
    
    # crypto exchange profits
    Route::post('/packages/crypto-exchange/user-profits/', 'CryptoExchangeProfitController@getUserProfits')->name('member.cExchangeGetUserProfits');
    Route::post('/packages/crypto-exchange/user-profits/pushtobank/', 'CryptoExchangeProfitController@pushToBank')->name('member.cExchangeUserProfitsPushToBank');


    # crypto shuffle
    Route::get('/packages/crypto-shuffle/', 'CryptoShuffleController@index')->name('member.cryptoShufflePackagesList');
    Route::post('/packages/crypto-shuffle/', 'CryptoShuffleController@viewPackagesAjax')->name('member.cryptoShufflePackagesListtAjax');
    Route::get('/packages/crypto-shuffle/details/{packageId}', 'CryptoShuffleController@viewPackageDetails')->name('member.cryptoShufflePackageDetails');
    Route::post('/packages/crypto-shuffle/queue-order/', 'CryptoShuffleController@queueOrder')->name('member.cryptoShuffleQueueOrder');
    Route::post('/packages/crypto-shuffle/process-order/', 'CryptoShuffleController@processOrder')->name('member.cryptoShuffleProcessOrder');


    # referral credits
    Route::get('/referral-credits/', 'ReferralCreditController@index')->name('member.referralCredits');
    Route::post('/referral-credits/', 'ReferralCreditController@getReferralCredits')->name('member.getReferralCredits');
    Route::post('/referral-credits/pushtobank/', 'ReferralCreditController@pushToBank')->name('member.refCredPushToBank');

    # package earnings
    Route::get('/package-earnings/', 'PackageEarningController@index')->name('member.packageEarnings');
    Route::post('/package-earnings/', 'PackageEarningController@getPackageEarnings')->name('member.getPackageEarnings');
    Route::post('/package-earnings/pushtobank/', 'PackageEarningController@pushToBank')->name('member.pkgEarningPushToBank');


    # membership
    Route::get('/packages/membership/', 'MembershipController@index')->name('member.membership-packages');
    Route::post('/packages/membership/process-order/', 'MembershipController@processOrder')->name('member.membershipProcessOrder');
    Route::post('/packages/membership/power-bonus/', 'MembershipController@getPowerBonuses')->name('member.getMembershipPowerBonus');
    Route::post('/packages/membership/power-bonus/pushtobank/', 'MembershipController@powerBonusPushToBank')->name('member.powerBonusPushToBank');

    Route::get('/', 'DashboardController@index')->name('member.dashboard');

});
