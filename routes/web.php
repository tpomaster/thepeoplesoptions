<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// if (env('APP_ENV') === 'production') {
//     URL::forceSchema('https');
// }

Route::get('/', 'FrontEndController@index')->name('home.index');
Route::get('/home', 'FrontEndController@index')->name('home');


Route::get('/about-tpo', function () {
    return view('about-tpo');
})->name('about-tpo');

Route::get('/about-cryptocurrency', function () {
    return view('about-cryptocurrency');
})->name('about-cryptocurrency');

Route::get('/faq', function () {
    return view('faq');
})->name('faq');

Route::get('/membership-packages', function () {
    return view('membership-packages');
})->name('membership-packages');

Route::get('/referral-commissions', function () {
    return view('referral-commissions');
})->name('referral-commissions');


Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact', ['as'=>'contactus.store','uses'=>'ContactController@contactUSPost']);
// Route::get('/rss', 'FrontEndController@index')->name('rss');


Auth::routes();


// User/Members Routes
//Route::get('/dashboard', 'HomeController@index')->name('dashboard');
//Route::get('/user/logout', 'Auth\LoginController@userLogout')->name('user.logout');


include 'web-admin.php';
include 'web-member.php';



# members dashboard
Route::group(['prefix' => 'member', 'namespace' => 'Member'], function () {
    Route::post('/dashboard/announcements', 'DashboardController@getAnnouncements')->name('member.getDashboardAnnouncements');
    Route::post('/dashboard/news', 'DashboardController@getNews')->name('member.getDashboardNews');
    Route::post('/dashboard/faq', 'DashboardController@getFaqs')->name('member.getDashboardFaqs');
    Route::get('/dashboard/charts-data', 'DashboardController@getChartData')->name('member.getChartData');

    // graph data loop
    Route::get('/dashboard/graph', 'DashboardController@loopDashboardGraphData')->name('member.loopDashboardGraphData');
    Route::post('/dashboard/graph', 'DashboardController@loopDashboardGraphData')->name('member.loopDashboardGraphDataPost');

    Route::get('/dashboard/trading-pkg-charts-data', 'TradeGraphsController@getTradingPackageChartData')->name('member.getTradingPkgChartData');
    Route::get('/dashboard/mining-pkg-charts-data', 'TradeGraphsController@getMiningPackageChartData')->name('member.getMiningPkgChartData');
    Route::get('/dashboard/exchange-pkg-charts-data', 'TradeGraphsController@getExchangePackageChartData')->name('member.getExchangePkgChartData');

    Route::get('/faq', 'FaqsController@index')->name('member.faqindex');
    Route::post('/member/faq', 'FaqsController@getFaqs')->name('member.getFaqsContent');

    Route::get('/investment-loop', 'InvestmentLoopController@index')->name('investment-loop');
    Route::post('/investment-loop', 'InvestmentLoopController@getUsersPayment')->name('member.getUsersPayment');

});
