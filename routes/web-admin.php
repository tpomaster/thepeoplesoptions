<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    // Login & Logout
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\LoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
});

// Admin Routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {
    // Dashboad
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/dashboard/charts-data', 'DashboardController@getChartData')->name('admin.getChartData');

    // graph data loop
    Route::get('/dashboard/graph', 'DashboardController@loopDashboardGraphData')->name('admin.loopDashboardGraphData');
    Route::post('/dashboard/graph', 'DashboardController@loopDashboardGraphData')->name('admin.loopDashboardGraphDataPost');

    // Users
    Route::get('/users', 'AdminsController@index')->name('admin.index');
    Route::post('/users', 'AdminsController@store')->name('admin.store');
    Route::get('/users/create', 'AdminsController@create')->name('admin.create');
    Route::put('/users/{admin}', 'AdminsController@update')->name('admin.update');
    Route::delete('/users/{admin}', 'AdminsController@destroy')->name('admin.destroy');
    Route::get('/users/{admin}/edit', 'AdminsController@edit')->name('admin.edit');
    Route::post('/users/alladmins', 'AdminsController@allAdmins')->name('admin.all');
    Route::get('/users/{admin}/confirmation', 'AdminsController@confirmation')->name('admin.confirmation');

    // Affiliates
    // Route::resource('/affiliates', 'AffiliatesController');

    Route::get('affiliates/view/{view?}/{username?}', 'AffiliatesController@index')->name('affiliates.index');
    Route::get('affiliates/{affiliate}/edit', 'AffiliatesController@edit')->name('affiliates.edit');
    Route::put('affiliates/{affiliate}', 'AffiliatesController@update')->name('affiliates.update');
    Route::put('affiliates/{affiliate}/bitshares', 'AffiliatesController@updateBitshares')->name('affiliates.update.bitshares');
    Route::delete('affiliates/{affiliate}', 'AffiliatesController@destroy')->name('affiliates.destroy');
    Route::match(['post', 'get'],'affiliates/show/tree', 'AffiliatesController@showTree')->name('affiliates.show.tree');

    // DataTable
    Route::resource('/datatable/users', 'DataTable\AdminsController');
    Route::resource('/datatable/affiliates', 'DataTable\AffiliatesController',
        [ 'names' => 'datatable.affiliates' ]
    );

    // Change Password
    Route::get('/users/{admin}/change-password', 'Auth\UpdatePasswordController@showChangeForm')->name('admin.password.change');
    Route::post('/users/{admin}/change-password', 'Auth\UpdatePasswordController@update');

    // Roles
    Route::resource('/roles', 'RolesController');

    // Permissions
    Route::resource('/permissions', 'PermissionsController');

    // Packages
    Route::resource('/packages', 'PackagesController');

    // Membership Packages
    Route::get('/membership-packages/{package}/confirmation', 'MembershipPackagesController@confirmation')->name('membership-packages.confirmation');
    Route::get('/membership-packages/members/{package}', 'MembershipPackagesController@members')->name('membership-packages.members');
    Route::resource('/membership-packages', 'MembershipPackagesController', ['parameters' => [
        'membership-packages' => 'package'
    ]]);

    // Crypto Trading Packages
    Route::get('/crypto-trading-packages/{package}/confirmation', 'CryptoTradingPackagesController@confirmation')->name('crypto-trading-packages.confirmation');
    Route::get('/crypto-trading-packages/{package}/investors', 'CryptoTradingPackagesController@investors')->name('crypto-trading-packages.investors');
    Route::get('/crypto-trading-packages/{package}/trades', 'CryptoTradingPackagesController@trades')->name('crypto-trading-packages.trades');
    Route::patch('crypto-trading-packages/{package}/close', 'CryptoTradingPackagesController@close')->name('crypto-trading-packages.close');
    Route::get('/crypto-trading-packages/{package}/investors', 'CryptoTradingPackagesController@investors')->name('crypto-trading-packages.investors');
    Route::match(['get', 'post'], '/crypto-trading-packages/filter', 'CryptoTradingPackagesController@filter')->name('crypto-trading-packages.filter');
    Route::resource('crypto-trading-packages', 'CryptoTradingPackagesController', ['parameters' => [
        'crypto-trading-packages' => 'package'
    ]]);

    // Crypto Trading Trades Queue
    Route::get('crypto-trading-trades-queue/{trade}/confirmation', 'CryptoTradingTradesQueueController@confirmation')->name('crypto-trading-trades-queue.confirmation');
    Route::resource('crypto-trading-trades-queue', 'CryptoTradingTradesQueueController', ['parameters' => [
        'crypto-trading-trades-queue' => 'trade'
    ]]);
    Route::get('crypto-trading-trades-queue/create/{package?}','CryptoTradingTradesQueueController@create')->name('crypto-trading-trades-queue.create');

    // Crypto Trading Trades
    Route::post('crypto-trading-trades/compute', 'CryptoTradingTradesController@computeTrade')->name('crypto-trading-trades.compute');
    Route::get('/crypto-trading-trades/{trade}/confirmation', 'CryptoTradingTradesController@confirmation')->name('crypto-trading-trades.confirmation');
    Route::resource('crypto-trading-trades', 'CryptoTradingTradesController', ['parameters' => [
        'crypto-trading-trades' => 'trade'
    ]]);

    // Crypto Mining Packages
    Route::get('/crypto-mining-packages/{package}/confirmation', 'CryptoMiningPackagesController@confirmation')->name('crypto-mining-packages.confirmation');
    Route::get('/crypto-mining-packages/{package}/investors', 'CryptoMiningPackagesController@investors')->name('crypto-mining-packages.investors');
    Route::get('/crypto-mining-packages/{package}/profits', 'CryptoMiningPackagesController@profits')->name('crypto-mining-packages.profits');
    Route::patch('crypto-mining-packages/{package}/close', 'CryptoMiningPackagesController@close')->name('crypto-mining-packages.close');
    Route::match(['get', 'post'], '/crypto-mining-packages/filter', 'CryptoMiningPackagesController@filter')->name('crypto-mining-packages.filter');
    Route::resource('/crypto-mining-packages', 'CryptoMiningPackagesController', ['parameters' => [
        'crypto-mining-packages' => 'package'
    ]]);

    // Crypto Mining Profits Queue
    Route::get('crypto-mining-profits-queue/{profit}/confirmation', 'CryptoMiningProfitsQueueController@confirmation')->name('crypto-mining-profits-queue.confirmation');
    Route::resource('crypto-mining-profits-queue', 'CryptoMiningProfitsQueueController', ['parameters' => [
        'crypto-mining-profits-queue' => 'profit'
    ]]);
    Route::get('crypto-mining-profits-queue/create/{package?}','CryptoMiningProfitsQueueController@create')->name('crypto-mining-profits-queue.create');

    // Crypto Mining Profits
    Route::post('crypto-mining-profits/compute', 'CryptoMiningProfitsController@computeProfit')->name('crypto-mining-profits.compute');
    Route::get('/crypto-mining-profits/{profit}/confirmation', 'CryptoMiningProfitsController@confirmation')->name('crypto-mining-profits.confirmation');
    Route::resource('crypto-mining-profits', 'CryptoMiningProfitsController', ['parameters' => [
        'crypto-mining-profits' => 'profit'
    ]]);


    // Crypto Exchange Packages
    Route::get('/crypto-exchange-packages/{package}/confirmation', 'CryptoExchangePackagesController@confirmation')->name('crypto-exchange-packages.confirmation');
    Route::get('/crypto-exchange-packages/{package}/investors', 'CryptoExchangePackagesController@investors')->name('crypto-exchange-packages.investors');
    Route::get('/crypto-exchange-packages/{package}/profits', 'CryptoExchangePackagesController@profits')->name('crypto-exchange-packages.profits');
    Route::patch('crypto-exchange-packages/{package}/close', 'CryptoExchangePackagesController@close')->name('crypto-exchange-packages.close');
    Route::match(['get', 'post'], '/crypto-exchange-packages/filter', 'CryptoExchangePackagesController@filter')->name('crypto-exchange-packages.filter');
    Route::resource('/crypto-exchange-packages', 'CryptoExchangePackagesController', ['parameters' => [
        'crypto-exchange-packages' => 'package'
    ]]);

    // Crypto Mining Profits Queue
    Route::get('crypto-exchange-profits-queue/{profit}/confirmation', 'CryptoExchangeProfitsQueueController@confirmation')->name('crypto-exchange-profits-queue.confirmation');
    Route::resource('crypto-exchange-profits-queue', 'CryptoExchangeProfitsQueueController', ['parameters' => [
        'crypto-exchange-profits-queue' => 'profit'
    ]]);
    Route::get('crypto-exchange-profits-queue/create/{package?}','CryptoExchangeProfitsQueueController@create')->name('crypto-exchange-profits-queue.create');

    // Crypto Exchange Profits
    Route::post('crypto-exchange-profits/compute', 'CryptoExchangeProfitsController@computeProfit')->name('crypto-exchange-profits.compute');
    Route::get('/crypto-exchange-profits/{profit}/confirmation', 'CryptoExchangeProfitsController@confirmation')->name('crypto-exchange-profits.confirmation');
    Route::resource('crypto-exchange-profits', 'CryptoExchangeProfitsController', ['parameters' => [
        'crypto-exchange-profits' => 'profit'
    ]]);


    // Crypto Shuffle Packages
    Route::get('/crypto-shuffle-packages/{package}/confirmation', 'CryptoShufflePackagesController@confirmation')->name('crypto-shuffle-packages.confirmation');
    Route::get('/crypto-shuffle-packages/{package}/orders', 'CryptoShufflePackagesController@orders')->name('crypto-shuffle-packages.orders');
    Route::resource('/crypto-shuffle-packages', 'CryptoShufflePackagesController', ['parameters' => [
        'crypto-shuffle-packages' => 'package'
    ]]);

    // Crypto Shuffle Orders
    Route::get('/crypto-shuffle-orders/{order}/confirmation', 'CryptoShuffleOrdersController@confirmation')->name('crypto-shuffle-orders.confirmation');
    Route::resource('/crypto-shuffle-orders', 'CryptoShuffleOrdersController', ['parameters' => [
        'crypto-shuffle-orders' => 'order'
    ]]);

    // Bank
    Route::get('/tpo-bank', 'BankController@index')->name('tpo-bank.index');
    Route::post('/tpo-bank/send-fund', 'BankController@sendFund')->name('tpo-bank.send-fund');
    Route::get('/tpo-bank/send-fund/confirmation', 'BankController@sendFundConfirmation')->name('tpo-bank.send-fund-confirmation');

    // Bank Transactions
    Route::match(['get', 'post'], '/tpo-bank-transaction', 'BankTransactionsController@index')->name('tpo-bank-transaction.index');
    Route::get('/tpo-bank-transaction/{transaction}/edit', 'BankTransactionsController@edit')->name('tpo-bank-transaction.edit');
    Route::put('/tpo-bank-transaction/{transaction}', 'BankTransactionsController@update')->name('tpo-bank-transaction.update');
    Route::get('/tpo-bank-transaction/{transaction}/confirmation', 'BankTransactionsController@confirmation')->name('tpo-bank-transaction.confirmation');
    Route::delete('/tpo-bank-transaction/{transaction}', 'BankTransactionsController@destroy')->name('tpo-bank-transaction.destroy');
    Route::any('/tpo-bank-transaction/search-user', 'BankTransactionsController@searchUserBank')->name('tpo-bank-transaction.search');
    Route::get('/tpo-bank-transaction/{transaction}', 'BankTransactionsController@show')->name('tpo-bank-transaction.show');
    Route::match(['get','post'], '/tpo-bank-transaction/filter', 'BankTransactionsController@filterTransactions')->name('tpo-bank-transaction.filter');

    // Referral Credits
    Route::get('referral-credits', 'ReferralCreditsController@index')->name('referral-credits.index');
    Route::get('referral-credits/{user_id}/{package_cat}/{package_id}', 'ReferralCreditsController@show')->name('referral-credits.show');

    // Users Bank
    Route::get('/user-bank', 'UsersBankController@index')->name('user-bank.index');

    // Faq
    Route::get('/faq/{faq}/confirmation', 'FaqController@confirmation')->name('faq.confirmation');
    Route::resource('/faq', 'FaqController');

    // News
    Route::get('/news/{news}/confirmation', 'NewsController@confirmation')->name('news.confirmation');
    Route::resource('/news', 'NewsController');

     // Announcement
    Route::get('/announcements/{announcement}/confirmation', 'AnnouncementsController@confirmation')->name('announcements.confirmation');
    Route::resource('/announcements', 'AnnouncementsController');

    // Password reset routes
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

    // Settings Routes
    Route::get('/settings/{setting}/confirmation/', 'SettingsController@confirmation')->name('settings.confirmation');
    Route::resource('/settings', 'SettingsController');

    // Payouts Routes
    Route::get('/payouts/{payout}/approve', 'PayoutsController@approve')->name('payouts.approve');
    Route::post('/payouts/{payout}/cancel', 'PayoutsController@cancel')->name('payouts.cancel');
    Route::get('/payouts/{payout}/confirmation', 'PayoutsController@confirmation')->name('payouts.confirmation');
    Route::any('/payouts/filter', 'PayoutsController@filter')->name('payouts.filter');
    Route::resource('/payouts', 'PayoutsController');

    // Bitpal Routes
    Route::get('/oauth-code', 'BitPalController@oAuthCode')->name('bitpal.oauthcode');

    // TPO Marketing
    Route::get('/tpo-marketing/login', 'TPOMarketingController@login')->name('tpomarketing.login');

    
});