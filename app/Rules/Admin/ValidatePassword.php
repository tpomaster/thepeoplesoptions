<?php

namespace App\Rules\Admin;

use App\Model\Admin\Admin;
use Illuminate\Contracts\Validation\Rule;

class ValidatePassword implements Rule
{
    protected $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }

    public function passes($attribute, $value)
    {
        if ( Admin::checkPassword($this->admin, $value) ) {
            return true;
        }
        return false;
    }

    public function message()
    {
        return 'The given password is invalid.';
    }
}
