<?php

namespace App\Rules\Admin;

use App\Model\Admin\Admin;
use Illuminate\Contracts\Validation\Rule;

class ValidatePincode implements Rule
{
    protected $admin;

    public function __construct($admin)
    {
        $this->admin = $admin;
    }

    public function passes($attribute, $value)
    {
        if ( Admin::checkPincode($this->admin, $value) ) {
            return true;
        }
        return false;
    }

    public function message()
    {
        return 'The given pincode is invalid.';
    }
}
