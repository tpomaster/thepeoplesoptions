<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Http\Request;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'admin':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('admin.dashboard');
                }
                break;
                
            case 'member':
                if (Auth::guard($guard)->check()) {
                    if( $request->ajax() ){
                        $ret =  array(
                            'isError' => false, 
                            'msg' => array('Login Successfull.'),
                            'redirectUrl' => route('member.dashboard')
                        );
                        return response()->json($ret);
                        
                    }else{
                        return redirect()->route('member.dashboard');
                    }
                }
                break;

            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/dashboard');
                }
                break;
        }

        return $next($request);
    }
}
