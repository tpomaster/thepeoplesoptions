<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\ContactUS;
use Mail;

use App\Helpers\LpjHelpers;

class ContactController extends Controller
{
	/**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('contact');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function contactUSPost(Request $request)
   {
       $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'address' => 'required',
        'phone' => 'required|numeric',
        'message' => 'required'
        ]);
 
       ContactUS::create($request->all());

       Mail::send('email',
	       array(
	           'name' => $request->get('name'),
	           'email' => $request->get('email'),
	           'address' => $request->get('address'),
	           'phone' => $request->get('phone'),
	           'user_message' => $request->get('message')
	       ), function($message)
	   {
	       $message->from('tech@thepeoplesoptions.com');
	       $message->to('tech@thepeoplesoptions.com', 'Admin')->subject('TPO Feedback');
	   });
 
       return back()->with('success', 'Thanks for contacting us!');
   }
}
