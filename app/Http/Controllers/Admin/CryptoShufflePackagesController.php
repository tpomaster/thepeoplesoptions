<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CryptoShufflePackage\{StorePackageForm, UpdatePackageForm};
use App\Model\Admin\CryptoShufflePackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CryptoShufflePackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', CryptoShufflePackage::class)) {
            return view('admin.errors.403');
        }

        $packages = CryptoShufflePackage::latest()->paginate(4);

        if (request()->ajax()) {
            return view('admin.packages.crypto-shuffle.partials.package-list', ['packages' => $packages])->render();
        }

        return view('admin.packages.crypto-shuffle.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', CryptoShufflePackage::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $package = new CryptoShufflePackage;

        $statuses = ['available', 'active', 'closed'];

        return view('admin.packages.crypto-shuffle.create', compact('package', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageForm $form)
    {
        return response()->json($form->response());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\CryptoShufflePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoShufflePackage $package)
    {
        if (! Auth::user()->can('view', $package)) {
            return view('admin.errors.403');
        }

        return view('admin.packages.crypto-shuffle.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\CryptoShufflePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoShufflePackage $package)
    {
        if (! Auth::user()->can('update', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $statuses = ['available', 'active', 'closed'];

        return view('admin.packages.crypto-shuffle.edit', compact('package', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\CryptoShufflePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackageForm $form, CryptoShufflePackage $package)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\CryptoShufflePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoShufflePackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Package Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoShufflePackage $package)
    {
        $action = request()->input('action');

        if (! Auth::user()->can($action, $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $route = $this->getActionRoute($action, $package->id);

        $investors_count = $package->orders()->count();

        if (request()->ajax()) {
            return view('admin.packages.crypto-shuffle.confirmation', compact('package', 'action', 'route', 'investors_count'));
        }

        return abort(403);
    }

    /**
     * Get package orders.
     */
    public function orders(CryptoShufflePackage $package)
    {
        $args   = ['limit' => 10];
        $orders = $package->orders($args);

        if (request()->ajax()) {
            return view('admin.packages.crypto-shuffle.partials.orders-table', compact('orders'))->render();
        }

        return abort(403);
    }

    /**
     * Get route base on action and package id given.
     * @param  string $action Form action
     * @param  integer $id     Package id
     * @return string        Generated route action
     */
    protected function getActionRoute($action, $id)
    {
        switch ($action) {
            case 'update':
                $route = route('crypto-shuffle-packages.update', $id);
                break;

            case 'delete':
                $route = route('crypto-shuffle-packages.destroy', $id);
                break;

            default:
                $route = '#';
                break;
        }

        return $route;
    }
}
