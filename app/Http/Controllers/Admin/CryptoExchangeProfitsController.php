<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoExchangePackage;
use App\Model\Admin\CryptoExchangeProfit;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CryptoExchangeProfitsController extends Controller
{

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CryptoExchangeProfit  $trade
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoExchangeProfit $profit)
    {

        try {
            \DB::table('crypto_exchange_packages')
                ->where([
                    ['id', $profit->package_id],
                    ['posted_profits', '!=', 0]
                ])
                ->decrement('posted_profits');

            \DB::table('crypto_exchange_funds')
                ->where([
                    ['package_id', $profit->package_id],
                    ['posted_profits_count', '!=', 0],
                    ['exchange_status', 'active']
                ])
                ->decrement('posted_profits_count');

            \DB::table('crypto_exchange_profits')
                ->where('id', $profit->id)
                ->delete();

            \DB::table('crypto_exchange_user_profits')
                ->where('profit_id', $profit->id)
                ->delete();

            \DB::table('admin_earnings')
                ->where([
                    ['ref_id', $profit->id],
                    ['package_cat', 'cryptoexchange']
                ])
                ->delete();

            \DB::table('package_earnings')
                ->where([
                    ['profit_id', $profit->id],
                    ['package_cat', 'cryptoexchange']
                ])
                ->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Profit Successfully Deleted!',
                'status'    => 'success'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoExchangeProfit $profit)
    {
        $action = request()->input('action');

        return view('admin.packages.crypto-exchange.profits.confirmation', compact('profit', 'action'));
    }

    /**
     * Compute the given queued trade.
     */
    public function computeProfit()
    {

        /*request()->validate([
            'queued_item_id' => 'required|numeric'
        ]);*/

        $response = array(
            'title' => 'Trade',
            'error' => false,
            'message' => '',
        );

        $queue_id = request('queue_id');

        // Get queued trade using queued trade id
        $queued_item = \DB::table('crypto_exchange_profits_queue')
            ->where('id', $queue_id)
            ->where('status', 'active')
            ->first();

        if ( ! $queued_item ) {
            $response['error'] = true;
            $response['status'] = 'completed';
            $response['message'] = 'All queued profit scheduled for posting today are now processed.';
        }

        # check double entry
        # ---------------------------------------
        $uncomputed_profits_cnt = 0;
        $existing_profit = false;
        if ( $response['error'] === false ) {
            $existing_profit = \DB::table('crypto_exchange_profits')
                        ->where('package_id', $queued_item->package_id)
                        ->where('code', $queued_item->code)
                        ->first();

            if ( $existing_profit ) {
                # count package funds that haven't received profit from this trade
                $uncomputed_profits_cnt = \DB::table('crypto_exchange_funds')
                    ->where([
                        ['package_id', $queued_item->package_id],
                        ['exchange_status', 'active'],
                        ['last_post_code', '<>', $queued_item->code]
                    ])
                    ->if($queued_item->for_membership, 'created_at', '<', $queued_item->date )
                    ->count();
            }
        }

        if ($response['error'] === false) {
            # Continue computing user trades
            # ---------------------------------------
            if ($existing_profit) {

                if ($uncomputed_profits_cnt > 0) {

                    $this->computeUserProfit($existing_profit);

                    $response['status'] = 'incomplete';
                    $response['message'] = 'Please wait I`m still computing. ( queue id: '.$queue_id.' )';

                } else {
                    $response['status'] = 'completed';
                    $response['message'] = 'Exchange profits are now successfully computed. ( queue id: '.$queue_id.' )';

                    \DB::table('crypto_exchange_profits_queue')
                        ->where('id', $queue_id)
                        ->update(['status' => 'completed']);
                }

            } else {

                $exchange_package = CryptoExchangePackage::where('id', $queued_item->package_id)->firstOrFail();

                if ( ! $exchange_package->start_date ) {
                    \DB::table('crypto_exchange_packages')
                        ->where('id', $exchange_package->id)
                        ->update(['start_date' => $queued_item->date]);
                }

                if ( ! $exchange_package->end_date ) {
                    $add_days = $exchange_package->duration - 1;

                    \DB::table('crypto_exchange_packages')
                        ->where('id', $exchange_package->id)
                        ->update(['end_date' => date('Y-m-d', strtotime($queued_item->date. ' + '. $add_days .' days'))] );
                }

                if ( ($exchange_package->posted_profits < $exchange_package->duration) || $exchange_package->for_membership ) {
                    // Save queued trade to trades
                    $profit = CryptoExchangeProfit::create([
                        'package_id'        => $queued_item->package_id,
                        'code'              => $queued_item->code,
                        'date'              => $queued_item->date,
                        'investment'        => $queued_item->exchange_amount,
                        'profit'            => $queued_item->profit,
                        'profit_percent'    => $queued_item->profit_percent,
                        'for_membership'    => $queued_item->for_membership,
                    ]);

                    $exchange_package->increment('posted_profits');

                    $this->computeUserProfit($profit);

                    $response['status'] = 'incomplete';
                    $response['message'] = 'Start computing profit profits. ( queue id: '.$queue_id.' )';
                } else {
                    $response['status'] = 'completed';
                    $response['message'] = 'Package reached maximum posted count.';

                    \DB::table('crypto_exchange_profits_queue')
                        ->where('id', $queue_id)
                        ->update(['status' => 'completed']);
                }

            }
        }

        return response()->json($response);
    }

    private function computeUserProfit($profit)
    {
        $package = \DB::table('crypto_exchange_packages')
                ->where('id', $profit->package_id)
                ->first();

        if ($profit->for_membership) {
            $package->amount = $profit->investment;
        }

        $package_membership_config = unserialize($package->management_fee);

        # get investors
        $args = array(
            'page' => 1,
            'limit' => 10,
            'package_id' => $profit->package_id,
            'exchange_status' => 'active',
            'last_post_code' => $profit->code
        );

        if ( $profit->for_membership ) {
            $args['exclude_new_funds'] = true;
            $args['profit_date'] = $profit->date;
        }

        // getInvestors()
        $investors = $this->getInvestors($args);

        foreach ($investors['list'] as $key => $investor) {

            $user = \DB::table('users')
                    ->select('membership')
                    ->where('id', $investor->user_id)->first();

            $management_fee = (int) $package_membership_config[$user->membership];
            $management_fee = $management_fee / 100;

            $investor_percentage = $investor->amount / $package->amount;
            $profit_amount = $profit->profit * $investor_percentage;

            if ($profit_amount <= 0) {
                $management_fee_amount = 0;
                $user_profit = 0;

                # reduce miing amount if its a negative profit
                $new_trade_amount = $investor->amount + $profit_amount;

                \DB::table('crypto_exchange_funds')
                    ->where('id', $investor->id)
                    ->update([
                        'amount' => $new_trade_amount,
                        'updated_at' => Carbon::now()
                    ]);
            } else {
                $management_fee_amount = $profit_amount * $management_fee;
                $user_profit = $profit_amount - $management_fee_amount;
            }

            $details = array(
                'package_amount' => $profit->investment,
                'gross_profit'  => $profit->profit,
                'user_profit_share' => $investor_percentage,
                'user_membership' => $user->membership,
                'management_fee'  => $management_fee,
                'management_fee_amount'  => $management_fee_amount,
                'trade_code' => $profit->code
            );

            $user_exchange_profit = [
                'package_id' => $profit->package_id,
                'profit_id' => $profit->id,
                'package_fund_id' => $investor->id,
                'user_id' => $investor->user_id,
                'date' => $profit->date,
                'investment' => $investor->amount,
                'gross_profit' => $profit_amount,
                'management_fee' => $management_fee_amount,
                'net_profit' => $user_profit,
                'status' => 'completed',
                'details' => json_encode($details),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            if ( ! $investor->start_date ) {
                \DB::table('crypto_exchange_funds')
                    ->where('id', $investor->id)
                    ->update(['start_date' => $profit->date]);
            }

            if ( ! $investor->end_date ) {
                $add_days = $package->duration - 1;
                \DB::table('crypto_exchange_funds')
                    ->where('id', $investor->id)
                    ->update(['end_date' => date('Y-m-d', strtotime($profit->date. ' + '. $add_days .' days'))] );
            }


            try {
                \DB::table('crypto_exchange_user_profits')
                ->insert( $user_exchange_profit );
            } catch (Exception $e) {
                return $e->getMessage();
            }


            # insert tpo admin earnings
            if ( $management_fee_amount > 0 ) {
                $admin_profit_earning = array(
                    'package_id' => $profit->package_id,
                    'package_cat' => 'cryptoexchange',
                    'ref_id' => $profit->id,
                    'description' => 'Crypto Exchange Management Fee',
                    'amount'  => $management_fee_amount,
                    'date' => $profit->date,
                    'details' => '',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                );

                \DB::table('admin_earnings')
                    ->insert($admin_profit_earning);
            }

            \DB::table('crypto_exchange_funds')
                ->where('id', $investor->id)
                ->increment('posted_profits_count');

            $update_columns = [
                'last_post_code' => $profit->code,
                'last_posted_date' => $profit->date,
                'updated_at'     => Carbon::now()
            ];


            if ($package->for_membership) {
                $profits_count = $investor->posted_profits_count + 1;

                if ($profits_count >= $package->duration) {
                    $update_columns['exchange_status'] = 'closed';
                }
            }



            \DB::table('crypto_exchange_funds')
                ->where('id', $investor->id)
                ->update($update_columns);
        }
    }

    private function getInvestors(array $args) {
        $defaults = [
            'page'              => 1,
            'limit'             => 10,
            'orderby'           => 'id',
            'order'             => ' ASC',
            'id'                => '',
            'package_id'        => 0,
            'user_id'           => 0,
            'exchange_status'   => '',
            'last_post_code'    => '',
            'exclude_new_funds' => false,
            'adding_new_trades' => false,
            'profit_date'       => date('Y-m-d', time()),
            'data_list'         => null
        ];

        extract( array_merge($defaults, $args) );

        $query = \DB::table('crypto_exchange_funds')
            ->if ($id, 'id', '=', $id)
            ->if ($package_id, 'package_id', '=', $package_id)
            ->if ($user_id, 'user_id', '=', $user_id)
            ->if ($exchange_status, 'exchange_status', '=', $exchange_status)
            ->if ($exclude_new_funds, 'created_at', '<', $profit_date)
            ->if ($adding_new_trades, 'last_date', '<', $profit_date)
            ->if ($last_post_code, 'last_post_code', '<>', $last_post_code);

        $investors_list     = $query->orderBy($orderby, $order)->limit($limit)->get();
        $investors_count    = $query->count();
        $investors_funds    = $query->sum('amount');

        $investors = [
            'list'  => $investors_list,
            'count' => $investors_count,
            'funds' => $investors_funds
        ];

        return $investors;
    }
}
