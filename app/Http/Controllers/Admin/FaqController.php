<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Faq;
use App\Helpers\LpjHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Faq::class)) {
            return view('admin.errors.403');
        }

        $faqs = Faq::latest()->paginate(4);

        if (request()->ajax()) {
            return view('admin.faq.partials.faq-list', compact('faqs'))->render();
        }

        return view('admin.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Faq::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $faq = new Faq;

        return view('admin.faq.create', compact('faq'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required|max:50',
            'content' => 'required'
        ]);

        Faq::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'thumbnail_path' => Input::hasFile('thumbnail') ? $request->file('thumbnail')->store('thumbnails', 'public') : null
        ]);

        return response()->json([
            'title'     => 'Success',
            'message'   => 'FAQ Successfully Created',
            'status'    => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        if (! Auth::user()->can('create', $faq)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.faq.show', compact('faq'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        if (! Auth::user()->can('update', $faq)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.faq.edit', compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required',
            'content' => 'required'
        ]);

        $faq->update([
            'title' => $data['title'],
            'content' => $data['content'],
        ]);

        if (Input::hasFile('thumbnail')) {
            $faq->update([
                'thumbnail_path' => $request->file('thumbnail')->store('thumbnails', 'public')
            ]);
        }

        return response()->json([
            'title'     => 'Success',
            'message'   => 'FAQ Successfully Updated',
            'status'    => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
         $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $faq->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'FAQ Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(Faq $faq)
    {
        if (! Auth::user()->can('delete', $faq)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $action = request()->input('action');

        return view('admin.faq.partials.confirmation', compact('faq', 'action'));
    }
}
