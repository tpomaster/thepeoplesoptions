<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CryptoTradingPackage\{StorePackageForm, UpdatePackageForm};
use App\Model\Admin\CryptoTradingPackage;
use Illuminate\Support\Facades\Auth;

class CryptoTradingPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', CryptoTradingPackage::class)) {
            return view('admin.errors.403');
        }

        $packages   = CryptoTradingPackage::latest()->paginate(4);
        $options    = CryptoTradingPackage::options();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.partials.package-list', compact('packages'))->render();
        }

        return view('admin.packages.crypto-trading.index', compact('packages', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', CryptoTradingPackage::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $package = new CryptoTradingPackage;
        $options = $package->options();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.create', compact('package', 'options'));
        }

        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageForm $form)
    {
        return response()->json($form->response());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoTradingPackage $package)
    {
        if (! Auth::user()->can('view', $package)) {
            return view('admin.errors.403');
        }

        // Return package information on ajax request
        if (request()->ajax()) {
            // Replace package amount with package current funds if package
            $package->amount = $package->funds()->sum('amount');

            return $package;
        }

        $referral_credits   = $package->referral_credits()->sum('amount');
        $trade_earning      = $package->user_trades()->sum('trade_earning');
        $management_fee     = $package->user_trades()->sum('management_fee');
        $net_earning        = $package->user_trades()->sum('user_profit');

        $earnings = [
            'trade_earning'     => $trade_earning,
            'management_fee'    => $management_fee,
            'net_earning'       => $net_earning,
        ];

        $package->computeInvestorsTrades();

        return view('admin.packages.crypto-trading.show', compact('package', 'referral_credits', 'earnings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoTradingPackage $package)
    {
        if (! Auth::user()->can('update', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $options = $package->options();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.edit', compact('package', 'options'))->render();
        }

        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackageForm $form, CryptoTradingPackage $package)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoTradingPackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Package Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Close the selected package
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function close(CryptoTradingPackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->end();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Package Successfully Closed',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoTradingPackage $package)
    {
        $action = request()->input('action');

        if (! Auth::user()->can($action, $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $route = $this->getActionRoute($action, $package->id);

        $investors_count = $package->investors()->count();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.confirmation', compact('package', 'investors_count', 'action', 'route'))->render();
        }

        return abort(403);
    }

    /**
     * Get package investors
     * Refactor - Create a CryptoTradingInvestors Model and CryptoTrading
     */
    public function investors(CryptoTradingPackage $package)
    {
        $investors = $package->investors(['limit' => 10]);

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.partials.investors-table', compact('package', 'investors'))->render();
        }

        return $investors;
    }

    /**
     * Get package trades
     */
    public function trades(CryptoTradingPackage $package)
    {
        $trades = $package->trades(['limit' => 10]);

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.partials.trades-table', compact('package', 'trades'))->render();
        }

        return $trades;
    }

    /**
     * Filter package list
     */
    public function filter()
    {

        // $packages = CryptoTradingPackage::query();

        $packages = CryptoTradingPackage::latest()
            ->if (request()->get('name'), 'name', 'LIKE', '%'.request()->get('name').'%')
            ->if (request()->get('status'), 'status', '=', request()->get('status'))
            ->if (request()->get('risk_profile'), 'risk_profile', '=', request()->get('risk_profile'))
            ->paginate(4)
            ->appends(request()->except(['page','_token']));

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.partials.package-list', compact('packages'))->render();
        }
    }

    /**
     * Get route base on action and package id given.
     * @param  string $action Form action
     * @param  integer $id     Package id
     * @return string        Generated route action
     */
    protected function getActionRoute($action, $id)
    {
        switch ($action) {
            case 'update':
                $route = route('crypto-trading-packages.update', $id);
                break;

            case 'delete':
                $route = route('crypto-trading-packages.destroy', $id);
                break;

            case 'close':
                $route = route('crypto-trading-packages.close', $id);
                break;

            default:
                $route = '#';
                break;
        }

        return $route;
    }
}
