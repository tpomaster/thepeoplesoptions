<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Permission;
use App\Model\Admin\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Role::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }


        $roles = DB::table('roles')->get();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Role::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $role           = new Role;
        $permissions    = $this->organizePermissions();
        $selected_permissions = [];

        return view('admin.roles.create', compact('permissions', 'role', 'selected_permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' =>'required|max:50|unique:roles',
            'slug' =>'required|max:50|unique:roles',
            'permissions' => 'sometimes',
        ]);

        $role = Role::create($data);

        $permissions = isset($data['permissions']) ? $data['permissions'] : [];
        $role->permissions()->sync($permissions);
        return redirect(route('roles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        if (! Auth::user()->can('update', $role)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $selected_permissions = \DB::table('permission_role')->where('role_id', $role->id)->pluck('permission_id')->toArray();

        $permissions = $this->organizePermissions();

        return view('admin.roles.edit', compact('role', 'permissions', 'selected_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    public function update(Role $role)
    {
        $data = request()->validate([
            'name' =>'required|min:5|max:50|unique:roles,name,'.$role->id,
            'slug' =>'required|max:50|unique:roles,slug,'.$role->id,
            'permissions' => 'sometimes',
        ]);

        $role->update($data);

        $permissions = isset($data['permissions']) ? $data['permissions'] : [];

        $role->permissions()->sync($permissions);

        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        if (! Auth::user()->can('delete', $role)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $role->delete();

        return back();
    }

    public function organizePermissions()
    {
        $permissions    = Permission::select('id', 'name', 'slug', 'category')->get()->toArray();
        $result         = [];

        foreach ($permissions as $permission) {
            $result[$permission['category']][] = $permission;
        }

        return $result;
    }
}
