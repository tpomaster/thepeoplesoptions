<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Setting::class)) {
            return view('admin.errors.403');
        }

        $settings = Setting::paginate(10);

        if (request()->ajax()) {
            return view('admin.settings.partials.table', compact('settings'))->render();
        }

        return view('admin.settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Setting::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $setting = new Setting;

        if (request()->ajax()) {
            return view('admin.settings.create', compact('setting'))->render();
        }

        return abort('403');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'key'   => 'required|alpha_dash',
            'name'  => 'required',
            'value' => 'required',
            'page'  => 'required|alpha_dash'
        ]);

        try {
            Setting::create($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Setting Successfully Saved',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        if (! Auth::user()->can('update', $setting)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        if (request()->ajax()) {
            return view('admin.settings.edit', compact('setting'))->render();
        }

        return abort('403');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $data = request()->validate([
            'key'   => 'required|alpha_dash',
            'name'  => 'required',
            'value' => 'required',
            'page'  => 'required|alpha_dash'
        ]);

        try {
           $setting->update($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Setting Successfully Saved',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        request()->validate([
            'pincode' => 'required'
        ]);

        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $setting->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Setting Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    public function confirmation(Setting $setting)
    {
        if (! Auth::user()->can('delete', $setting)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $action = request()->input('action');

        return view('admin.settings.confirmation', compact('setting', 'action'));
    }
}
