<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoShuffleOrder;
use Illuminate\Http\Request;

class CryptoShuffleOrdersController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CryptoMiningProfit  $trade
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoShuffleOrder $order)
    {

        try {
            $order->delete();

            \DB::table('referral_credits')
                ->where([
                    ['package_cat', 'cryptoshuffle'],
                    ['package_id', $order->package_id],
                    ['investor_user_id', $order->user_id],
                    ['fund_id', $order->id],
                ])
                ->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Shuffle Order Successfully Deleted!',
                'status'    => 'success'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoShuffleOrder $order)
    {
        $action = request()->input('action');

        return view('admin.packages.crypto-shuffle.orders.confirmation', compact('order', 'action'));
    }
}
