<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LpjHelpers;
use App\Http\Controllers\Controller;
use App\Model\Admin\{Affiliate, AffiliateTier, User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AffiliatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($view = '', $username = '')
    {
        if (! Auth::user()->can('index', Affiliate::class)) {
            return view('admin.errors.403');
        }

        $view = request()->input('view') ?: $view;
        $username = request()->input('username') ?: $username;

        if ($view == 'tree') {

            $settings = [
                'parent_username' => $username ?: 'papagroup'
            ];

            if (request()->ajax()) {
                return route('affiliates.index', [$view, $username]);
            }

            return view('admin.affiliates.tree.index', compact('settings'));
        }

        return view('admin.affiliates.list.index');
    }

    public function showTree()
    {
        $response = [
            'error'     => false,
            'message'   => ''
        ];

        $args = request()->validate([
            'parent_username'   => 'alpha_dash',
            'current_tier_lvl'  => 'numeric',
            'qpage'             => 'numeric',
            'page'              => 'numeric',
            'qlimit'            => 'numeric',
            'sort_by'           => 'alpha',
            'sort_order'        => 'alpha'
        ]);

        $defaults = [
            'parent_username'   => 'litopj2',
            'current_tier_lvl'  => 1,
            'next_lvl'          => $args['current_tier_lvl'] + 1,
            'qpage'             => 1,
            'page'              => 1,
            'qlimit'            => 5,
            'sort_by'           => 'id',
            'sort_order'        => 'DESC'
        ];

        $args = array_merge($defaults, $args);

        // Get Sponsor Information
        $sponsor = \DB::table('users')
            ->where('username', $args['parent_username'])
            ->first();

        if (! $sponsor) {
            $response['error'] = true;
            $response['message'] = 'Sponsor Information not found.';
        }

        // Get Referrals
        if (! $response['error']) {
            $query_params = [
                'parent_id'         => $sponsor->id,
                'parent_username'   => strtolower($sponsor->username),
                'qpage'             => $args['qpage'],
                'qlimit'            => $args['qlimit'],
                'sort_by'           => $args['sort_by'],
                'sort_order'        => $args['sort_order'],
                'current_tier_lvl'  => $args['current_tier_lvl']
            ];

            $referrals = AffiliateTier::getReferrals($query_params);

            // $response['referrals']  = $referrals['data'];
            // $response['pagination'] = $referrals['pagination'];

            if ($referrals->isEmpty()) {
                $response['error'] = true;
                $response['message'] = 'No Referrals';
            }
        }

        if (! $response['error']) {

            $settings = [
                'btn_class' => $args['current_tier_lvl'] == 6 ? 'getTeamBtnNew' : 'getTeamBtn',
                // 'btn_link'  => $args['current_tier_lvl'] == 6 ? url('admin/view/tree/') : '#',
                'tier_lvl_class' => $this->getTierLevelClass($args['current_tier_lvl']),
                'current_tier_lvl' => $args['current_tier_lvl'],
                'next_lvl' => $args['next_lvl']
            ];

            $response['html'] = view('admin.affiliates.tree.tier-list', compact('referrals', 'settings'))->render();
            $response['message'] = 'Affiliate tree loaded successfully.';
        }

        return response()->json($response);

    }

    public function getTierLevelClass($tier_lvl)
    {
        $tier_lvl_class = 'levelOne';

        switch ($tier_lvl) {
            case 1:
                $tier_lvl_class = 'levelOne';
                break;

            case 2:
                $tier_lvl_class = 'levelTwo';
                break;

            case 3:
                $tier_lvl_class = 'levelThree';
                break;

            case 4:
                $tier_lvl_class = 'levelFour';
                break;

            case 5:
                $tier_lvl_class = 'levelFive';
                break;

            case 6:
                $tier_lvl_class = 'levelSix';
                break;

            default:
                $tier_lvl_class = 'levelOne';
                break;
        }

        return $tier_lvl_class;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $error = false;
        $message = '';
        $sponsor = [];

        $user = \DB::table('users')
        ->select('parent_id', 'username', 'email', 'phone', 'firstname', 'lastname', 'status', 'bitshares_name')
        ->join('users_meta', 'users.id', '=', 'users_meta.user_id')
        ->where('users.id', $id)->first();

        if (!$user) {
            $error = true;
            $message = 'Incomplete user information.';
        }else {
            $sponsor = \DB::table('users')
            ->select('username', 'email')
            ->where('id', $user->parent_id)->first();
        }

        if (! Auth::user()->can('update', Affiliate::class)) {
            $error = true;
            $message = "Full authentication is required to access this resource.";
        }

        return response()->json([
            'user'      => $user,
            'error'     => $error,
            'message'   => $message,
            'user'      => $user,
            'sponsor'   => $sponsor,
            'url'       => url('/')
        ]);

            // return view('admin.affiliates.edit', compact('user', 'sponsor'));
        // }

        // abort(403, 'Unauthorized action.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update($id)
    {

        $data = request()->validate([
            'username' => 'required|unique:users,username,'.$id,
            'email' => 'required|email|unique:users,email,'.$id,
            'firstname' => 'required',
            'lastname' => 'required',
            'status'    => 'required|numeric',
            'password' => 'sometimes|required|min:8|confirmed',
            'password_confirmation ' => 'sometimes|required|min:8'
        ]);

        if ( array_key_exists('password', $data) ) {
            $data['password'] = bcrypt($data['password']);
        }

        try {
            $user = \DB::table('users')->where('id', $id)->update($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Affiliate Successfully Updated',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            /*return response()->json(
                ['status' => $e->getMessage()], 422
            );*/
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Update the affialites bitshares information
     */
    public function updateBitshares($id)
    {
        $data = request()->validate([
            'bitshares_name' => 'required|alpha_dash',
        ]);

        try {
            $user = \DB::table('users')->where('id', $id)->update($data);
            $user = \DB::table('users_meta')->where('user_id', $id)->update(['bitpal_default_bts_account' => $data['bitshares_name']]);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Affiliate Successfully Updated',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            /*return response()->json(
                ['status' => $e->getMessage()], 422
            );*/
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    public function destroy(Affiliate $affiliate)
    {
        $error = false;
        $message = '';

        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if (! Auth::user()->checkPincode(Auth::user(), $data['pincode'])) {
            $error = true;
            $message = "Incorrect Pincode!";
        }

        if (! Auth::user()->can('delete', $affiliate)) {
            $error = true;
            $message = "Full authentication is required to access this resource.";
        }

        $affiliate_children = \DB::table('users')
            ->where('parent_id', $affiliate->id)
            ->get();

        if ($affiliate_children->count() > 1) {
            $error = true;
            $message = "Can't delete affiliate. Affiliate have child.";
        }

        if ($error) {
            return response()->json([
                'title'     => 'Oops Something went wrong!',
                'message'   => $message,
                'status'    => 'error'
            ]);
        }

        try {
            $affiliate->delete();

            // Remove user other information
            \DB::table('users_meta')->where('user_id', $affiliate->id)->delete();
            \DB::table('users_login_codes')->where('username', $affiliate->username)->delete();
            \DB::table('users_bank')->where('user_id', $affiliate->id)->delete();
            \DB::table('users_bts_accounts')->where('user_id', $affiliate->id)->delete();
            \DB::table('users_bank_transactions')->where('user_id', $affiliate->id)->delete();
            \DB::table('users_bank_addfund_request_bts')->where('user_id', $affiliate->id)->delete();
            \DB::table('users_bank_addfund_request')->where('user_id', $affiliate->id)->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Affiliate Successfully Deleted',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ]);
        }
    }
}
