<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CryptoMiningPackage\StorePackageForm;
use App\Http\Requests\Admin\CryptoMiningPackage\UpdatePackageForm;
use App\Model\Admin\CryptoMiningPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CryptoMiningPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', CryptoMiningPackage::class)) {
            return view('admin.errors.403');
        }

        $packages  = CryptoMiningPackage::latest()->paginate(4);
        $options   = CryptoMiningPackage::options();

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.partials.package-list', compact('packages'))->render();
        }

        return view('admin.packages.crypto-mining.index', compact('packages', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', CryptoMiningPackage::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $package = new CryptoMiningPackage;

        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $posting_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        return view('admin.packages.crypto-mining.create', compact('package', 'statuses', 'risk_profiles', 'posting_frequencies', 'member_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageForm $form)
    {
        return response()->json($form->response());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\CryptoMiningPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoMiningPackage $package)
    {
        if (! Auth::user()->can('view', $package)) {
            return view('admin.errors.403');
        }

        // Return package information on ajax request
        if (request()->expectsJson()) {
            // Replace package amount with package current funds if package is for membership
            $package->amount = $package->funds()->sum('amount');

            return $package;
        }

        $referral_credits   = $package->referral_credits()->sum('amount');
        $mining_profit      = $package->user_profits()->sum('mining_profit');
        $management_fee     = $package->user_profits()->sum('management_fee');
        $net_earning        = $package->user_profits()->sum('user_profit');

        $earnings = [
            'mining_profit'     => $mining_profit,
            'management_fee'    => $management_fee,
            'net_earning'       => $net_earning
        ];

        $package->computeInvestorsProfits();

        return view('admin.packages.crypto-mining.show', compact('package', 'referral_credits', 'earnings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\CryptoMiningPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoMiningPackage $package)
    {
        if (! Auth::user()->can('update', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $posting_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        return view('admin.packages.crypto-mining.edit', compact('package', 'statuses', 'risk_profiles', 'posting_frequencies', 'member_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\CryptoMiningPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackageForm $form, CryptoMiningPackage $package)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\CryptoMiningPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoMiningPackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Mining Package Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Incorrect Pincode!',
            'status'    => 'error'
        ]);
    }

    /**
     * Close the selected package
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function close(CryptoMiningPackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->end();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Mining Package Successfully Closed',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoMiningPackage $package)
    {
        $action = request()->input('action');

        if (! Auth::user()->can($action, $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $route = $this->getActionRoute($action, $package->id);

        $investors_count = \DB::table('crypto_mining_funds')
            ->where('package_id', $package->id)
            ->count();

        return view('admin.packages.crypto-mining.partials.confirmation', compact('package', 'investors_count', 'action', 'route'));
    }

    public function investors(CryptoMiningPackage $package)
    {
        $investors = $package->investors(['limit' => 10]);

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.partials.investors-table', compact('package', 'investors'))->render();
        }
    }

    public function profits(CryptoMiningPackage $package)
    {
        $profits = $package->profits(['limit' => 10]);

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.partials.profits-table', compact('package', 'profits'))->render();
        }
    }

    /**
     * Filter package list
     */
    public function filter()
    {
        $packages = CryptoMiningPackage::query()
            ->if (request()->get('name'), 'name', 'LIKE', '%'.request()->get('name').'%')
            ->if (request()->get('status'), 'status', '=', request()->get('status'))
            ->if (request()->get('risk_profile'), 'risk_profile', '=', request()->get('risk_profile'))
            ->latest()
            ->paginate(4)
            ->appends(request()->except(['page','_token']));

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.partials.package-list', compact('packages'))->render();
        }
    }

    /**
     * Get route base on action and package id given.
     * @param  string $action Form action
     * @param  integer $id     Package id
     * @return string        Generated route action
     */
    protected function getActionRoute($action, $id)
    {
        switch ($action) {
            case 'update':
                $route = route('crypto-mining-packages.update', $id);
                break;

            case 'delete':
                $route = route('crypto-mining-packages.destroy', $id);
                break;

            case 'close':
                $route = route('crypto-mining-packages.close', $id);
                break;

            default:
                $route = '#';
                break;
        }

        return $route;
    }
}
