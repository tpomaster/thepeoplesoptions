<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Admin\DataTable\DataTableController;
use App\Http\Controllers\Controller;
use App\Model\Admin\User;
use Illuminate\Http\Request;

class AffiliatesController extends DataTableController
{
    /**
     * If we can create an entity or not.
     *
     * @var boolean
     */
    protected $allowCreation = false;


    public function builder()
    {
        return User::query();
    }

    public function getDisplayableColumns()
    {
        return [
            'id', 'firstname', 'lastname', 'username', 'email', 'created_at'
        ];
    }
}
