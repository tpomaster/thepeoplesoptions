<?php

namespace App\Http\Controllers\Admin\DataTable;

use App\Http\Controllers\Admin\DataTable\DataTableController;
use App\Http\Controllers\Controller;
use App\Model\Admin\Admin;
use Illuminate\Http\Request;

class AdminsController extends AdminDataTableController
{
    public function builder()
    {
        return Admin::query();
    }

    public function getDisplayableColumns()
    {
        return [
            'id', 'firstname', 'lastname', 'email',
        ];
    }

    public function getUpdatableColumns()
    {
        return [
            'firstname', 'lastname', 'email',
        ];
    }
}
