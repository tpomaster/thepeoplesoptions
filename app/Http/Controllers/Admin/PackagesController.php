<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Gate::denies('view-packages', Auth::user())) {
            return view('admin.errors.403');
        }

        return view('admin.packages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-packages', Auth::user())) {
            return view('admin.errors.403');
        }

        return view('admin.packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModelAdminPackage  $modelAdminPackage
     * @return \Illuminate\Http\Response
     */
    public function show(ModelAdminPackage $modelAdminPackage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelAdminPackage  $modelAdminPackage
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelAdminPackage $modelAdminPackage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelAdminPackage  $modelAdminPackage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelAdminPackage $modelAdminPackage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelAdminPackage  $modelAdminPackage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelAdminPackage $modelAdminPackage)
    {
        //
    }
}
