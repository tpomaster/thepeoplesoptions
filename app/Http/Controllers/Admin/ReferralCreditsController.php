<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\ReferralCredit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReferralCreditsController extends Controller
{

    public function index()
    {
        if (! Auth::user()->can('index', ReferralCredit::class)) {
            return view('admin.errors.403');
        }

        $referral_credits = \DB::table('referral_credits')
            ->select(
                \DB::raw('CONCAT(firstname, " ", lastname) as affiliate_name'), 'username', 'amount',
                'investor_user_id', 'package_id', 'package_cat','tier_level', 'referral_credits.status',
                'referral_credits.created_at as date'
            )
            ->join('users', 'referral_credits.user_id', '=', 'users.id')
            ->paginate(10);

        $referrals = [];

        foreach ($referral_credits as $key => $referral) {
            $referrals[$key] = $referral;
        }

        foreach ($referrals as $referral) {

            // Get package table
            switch ($referral->package_cat) {
                case 'cryptotrading':
                    $table = 'crypto_trading_packages';
                    break;

                case 'cryptomining':
                    $table = 'crypto_mining_packages';
                    break;

                case 'cryptoexchange':
                    $table = 'crypto_exchange_packages';
                    break;

                case 'membership':
                    $table = 'membership_packages';
                    break;

                case 'raffle':
                    $table = 'crypto_shuffle_packages';
                    break;

                case 'cryptoshuffle':
                    $table = 'crypto_shuffle_packages';
                    break;

                default:
                    $table = '';
                    break;
            }

            $package = \DB::table($table)
                ->select('name')
                ->where('id', $referral->package_id)
                ->first();

            $user = \DB::table('users')
                ->select('username')
                ->where('id', $referral->investor_user_id)
                ->first();

            $referral->package_name = $package ? $package->name : 'Package Deleted';
            $referral->investor_name = $user ? $user->username : 'Unknown User';
        }

        if (request()->ajax()) {
            return view('admin.referral-credits.partials.table', compact('referral_credits', 'referrals'))->render();
        }

        // dd($referral_credits);
        return view('admin.referral-credits.index', compact('referral_credits', 'referrals'));
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $package_cat, $package_id)
    {
        switch ($package_cat) {
            case 'cryptotrading':
                $table = 'crypto_trading_funds';
                break;

            case 'cryptoexchange':
                $table = 'crypto_exchange_funds';
                break;

            case 'cryptomining':
                $table = 'crypto_mining_funds';
                break;

            case 'cryptoshuffle':
                $table = 'crypto_shuffle_orders';
                break;

            case 'membership':
                $table = 'membership_payments';
                break;

            default:
                $table = null;
                break;
        }

        if (! $table) return "Invalid Package Category";

        $fund = \DB::table($table)
            ->where([
                ['user_id', $user_id],
                ['package_id', $package_id]
            ])
            ->first();

        $codes = explode(',', $fund->transaction_code);

        $referral_credits = \DB::table('referral_credits')
            ->join('users', 'users.id', '=', 'referral_credits.user_id')
            ->where([
                ['investor_user_id', $user_id],
                ['package_cat', $package_cat]
            ])
            ->whereIn('transaction_code', $codes)
            ->get();

        if ( request()->ajax() ) {
            return view('admin.referral-credits.show', compact('referral_credits'))->render();
        }

        return response()->json($referral_credits);
    }
}
