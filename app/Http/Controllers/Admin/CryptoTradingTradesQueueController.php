<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoTradingPackage;
use App\Model\Admin\CryptoTradingTradesQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CryptoTradingTradesQueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('compute', CryptoTradingPackage::class)) {
            return view('admin.errors.403');
        }

        $packages = \DB::table('crypto_trading_packages')
                ->where('status', 'funded-active')
                ->get();

        $queued_trades = \DB::table('crypto_trading_trades_queue as trades_queue')
                ->join('crypto_trading_packages as packages', 'trades_queue.package_id', '=', 'packages.id' )
                ->select('trades_queue.id as id', 'name', 'profit', 'date', 'exec_date')
                ->where('trades_queue.status', 'active')
                ->orderBy('date', 'asc')
                ->paginate(50);

        if (request()->expectsJson()) {
            return view('admin.packages.crypto-trading.trade-queue.partials.table', compact('queued_trades'))->render();
        }

        return view('admin.packages.crypto-trading.trade-queue.index', compact('packages', 'queued_trades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CryptoTradingPackage $package)
    {
        $packages = \DB::table('crypto_trading_packages')
            ->where('status', 'funded-active')
            ->get();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.trade-queue.create', compact('package', 'packages'))->render();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'package_id'        => 'required',
            'exec_date'         => 'required',
            'date'              => 'required',
            'num_of_trades'     => 'required',
            'profit_percent'    => 'required',
            'profit'            => 'required'
        ]);

        $date_queued = \DB::table('crypto_trading_trades_queue')
            ->whereDate('date', $data['date'])
            ->where('package_id', $data['package_id'])
            ->get();

        $date_exists = \DB::table('crypto_trading_user_trades')
            ->whereDate('date', $data['date'])
            ->where('package_id', $data['package_id'])
            ->get();

        if ($date_exists->count() > 0) {
            return response()->json([
                'title'     => 'Duplicate Post Date',
                'message'   => 'Package trade is already posted for this date ('. $data['date'] .').',
                'status'    => 'info'
            ]);
        }

        if ($date_queued->count() > 0) {
            return response()->json([
                'title'     => 'Duplicate Queue',
                'message'   => 'Package trade date (' .$data['date']. ') is already on queue.',
                'status'    => 'info'
            ]);
        }

        $package = \DB::table('crypto_trading_packages')
                ->select('id', 'amount', 'status', 'for_membership', 'posted_trades', 'duration')
                ->where('id', $data['package_id'])
                ->first();

        if ( ($package->posted_trades >= $package->duration) && !$package->for_membership ) {
            return response()->json([
                'title'     => 'Posted Maximum Trades',
                'message'   => 'Package reached maximum trades post count.',
                'status'    => 'info'
            ]);
        }

        if ($package->for_membership) {
            $package_fund = \DB::table('crypto_trading_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;
        }

        $data['trade_amount']   = $package->amount;
        $data['status']         = 'active';
        $data['for_membership'] = $package->for_membership;
        $data['code']           = strtotime(date('Y-m-d H:i:s')) . rand(10000, 99999);

        try {
            CryptoTradingTradesQueue::create($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Trade Successfully Saved',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\CryptoTradingTradesQueue  $trade
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoTradingTradesQueue $trade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\CryptoTradingTradesQueue  $trade
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoTradingTradesQueue $trade)
    {
        $package = \DB::table('crypto_trading_packages')
                ->where('id', $trade->package_id)
                ->first();

        if (request()->ajax()) {
            return view('admin.packages.crypto-trading.trade-queue.edit', compact('trade', 'package'))->render();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\CryptoTradingTradesQueue  $trade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CryptoTradingTradesQueue $trade)
    {
        if ($request->ajax()) {
            $data = $request->validate([
                'exec_date'         => 'required',
                'date'              => 'required',
                'num_of_trades'     => 'required',
                'profit_percent'    => 'required|numeric',
                'profit'            => 'required|numeric'
            ]);

            try {
                $trade->update($data);

                return response()->json([
                    'title'     => 'Success',
                    'message'   => 'Crypto Trading Trade Successfully Updated',
                    'status'    => 'success'
                ]);

            } catch (\Exception $e) {
                // return $e->getMessage();
                return response()->json([
                    'title'     => 'Oh No!',
                    'message'   => 'Oops Something went wrong!',
                    'status'    => 'error'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\CryptoTradingTradesQueue  $trade
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoTradingTradesQueue $trade)
    {
        if ( request()->ajax() ) {
            try {
                $data = request()->validate(
                    ['pincode' => 'required|numeric']
                );

                if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
                    $trade->delete();

                    return response()->json([
                        'title'     => 'Success',
                        'message'   => 'News Successfully Deleted',
                        'status'    => 'success'
                    ]);
                }
            } catch (\Exception $e) {
                return response()->json([
                    'title'     => 'Oh No!',
                    'message'   => 'Oops Something went wrong!',
                    'status'    => 'error'
                ]);
            }
        }
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoTradingTradesQueue $trade)
    {
        $package = \DB::table('crypto_trading_packages')
                ->where('id', $trade->package_id)
                ->first();

        // $action = request()->input('action');

        if ( request()->ajax() ) {
            return view('admin.packages.crypto-trading.trade-queue.partials.confirmation', compact('trade', 'package', 'action'));
        }
    }
}
