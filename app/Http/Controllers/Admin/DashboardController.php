<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Model\Admin\Dashboard;

use App\Helpers\LpjHelpers;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ret = array();

        $userId = Auth::id();

        $curDate = date('Y-m-d H:i:s');

        $pieData = Dashboard::getDashboardPie();
        
        $total_members = Dashboard::getTeamCount();
        $balance = Dashboard::getBankBalance();
        $combined_earnings = Dashboard::getCombinedEarnings();
        $payout = Dashboard::getPayout();

        $users = Dashboard::getLatestUsers();

        $transactions = Dashboard::getLatestTransactions();

        // dashboard top content
        $ret['total_members'] = $total_members;
        $ret['balance'] = $balance;
        $ret['combined_earnings'] = $combined_earnings;
        $ret['payout'] = $payout;

        // pie data
        $ret['miningFunds'] = $pieData['miningFunds'];
        $ret['tradingFunds'] = $pieData['tradingFunds'];
        $ret['exchangeFunds'] = $pieData['exchangeFunds'];
        $ret['shuffleFunds'] = $pieData['shuffleFunds'];

        // latest transactions
        $ret['transactions'] = $transactions;

        $catSlugs = array(
            'cryptomining' => 'crypto-mining-packages',
            'cryptotrading' => 'crypto-trading-packages',
            'cryptoexchange' => 'crypto-exchange-packages',
            'cryptoshuffle' => 'crypto-shuffle-packages',
            'membership' => 'membership-packages',
        );
        $ret['catSlugs'] = $catSlugs;

        // latest users
        $ret['users'] = $users;

        Dashboard::updateLastLogin( $userId, $curDate ); 

        $ret['new_start_date'] = '';
        $ret['runTime'] = '0';


        return view('admin.home', $ret);
    }

    public function getChartData ( Request $request ) {
        $ret = array();
        $userId = Auth::id();
        $chartData = Dashboard::getGraphData();

        $ret['chartData'] = $chartData;

        return view('admin/dashboard/charts/data', $ret);
    }

    public function loopDashboardGraphData ( Request $request ) {
        $ret = array();
        $userId = Auth::id();

        $dataChecker = Dashboard::checkAdminDailyEarnings();
        $ret['dataChecker'] = $dataChecker;

        $runTime = 0;

        $ret['runTime'] = $runTime;

        $new_start_date = '';

        $input = $request->all();
        $input = LpjHelpers::fss( $input );
        extract( $input );

        if ( $dataChecker == 0 ) {
            $start_date = '2017-03-01';
        } else {
            $start_date = Dashboard::getLastLogin( $userId );
        }
        $ret['start_date'] = $start_date;
        

        if ( !empty($new_start_date) ) {
            $start_date = date('Y-m-d', strtotime($new_start_date));
        }

        $ret['start_date'] = $start_date;

        if ($runTime <= 9) {
            $ret['message'] = 'Computing Related Resources...';
        } elseif($runTime >= 10 && $runTime <= 18) {
            $ret['message'] = 'Loading Charts Data...';
        } else {
            $ret['message'] = 'Initializing...';
        }
        
        $current_date = date('Y-m-d', strtotime("-1 days"));

        $end_date = date ("Y-m-d", strtotime("+10 days", strtotime($start_date)));

        $ret['end_date'] = $end_date;

        while( strtotime($start_date) <= strtotime($end_date) ) {

            if ( strtotime($start_date) <= strtotime($current_date) ) {
                $dailyEarnings = Dashboard::insertDailyEarnings( $start_date );
            }

            $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
        }

        $new_start_date = date ("Y-m-d", strtotime("+1 days", strtotime($end_date)));

        $runTime++;

        if ( strtotime($end_date) >= strtotime($current_date) ) {
            $ret['msg'] = 'stop';

        }else{
            $ret['msg'] = 'continue';
        }

        $ret['new_start_date2'] = $new_start_date;
        $ret['new_runTime'] = $runTime;

        return $ret;
    }
}
