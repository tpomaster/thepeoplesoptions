<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Announcement;
use App\Helpers\LpjHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Input};

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Announcement::class)) {
            return view('admin.errors.403');
        }

        $announcements = Announcement::latest()->paginate(4);

        if (request()->ajax()) {
            return view('admin.announcements.partials.announcement-list', compact('announcements'))->render();
        }

        return view('admin.announcements.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Announcement::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $announcement = new Announcement;

        return view('admin.announcements.create', compact('announcement'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required|max:50',
            'content' => 'required'
        ]);

        Announcement::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'thumbnail_path' => Input::hasFile('thumbnail') ? $request->file('thumbnail')->store('thumbnails', 'public') : null
        ]);

        return response()->json([
            'title'     => 'Success',
            'message'   => 'Announcement Successfully Created',
            'status'    => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        if (! Auth::user()->can('view', $announcement)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.announcements.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement)
    {
        if (! Auth::user()->can('update', $announcement)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.announcements.edit', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required',
            'content' => 'required'
        ]);

        $announcement->update([
            'title' => $data['title'],
            'content' => $data['content'],
        ]);

        if (Input::hasFile('thumbnail')) {
            $announcement->update([
                'thumbnail_path' => $request->file('thumbnail')->store('thumbnails', 'public')
            ]);
        }

        return response()->json([
            'title'     => 'Success',
            'message'   => 'Announcement Successfully Updated',
            'status'    => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $announcement->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Announcement Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(Announcement $announcement)
    {
        if (! Auth::user()->can('delete', $announcement)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $action = request()->input('action');

        return view('admin.announcements.partials.confirmation', compact('announcement', 'action'));
    }
}
