<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MembershipPackage\StoragePackageForm;
use App\Http\Requests\Admin\MembershipPackage\UpdatePackageForm;
use App\Model\Admin\MembershipPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MembershipPackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', MembershipPackage::class)) {
            return view('admin.errors.403');
        }

        $packages = MembershipPackage::latest()->paginate(4);

        if (request()->ajax()) {
            return view('admin.packages.membership.partials.package-list', ['packages' => $packages])->render();
        }

        return view('admin.packages.membership.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', MembershipPackage::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $package = new MembershipPackage;

        $statuses = ['available', 'active', 'closed'];

        return view('admin.packages.membership.create', compact('package', 'statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoragePackageForm $form)
    {
        return response()->json($form->response());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\MembershipPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function show(MembershipPackage $package)
    {
        if (! Auth::user()->can('view', $package)) {
            return view('admin.errors.403');
        }

        $members = \DB::table('membership_payments')
            ->select('user_id', 'username', 'firstname', 'lastname', 'amount', 'start_date', 'end_date', 'membership_payments.status')
            ->join('users', 'users.id', '=', 'membership_payments.user_id')
            ->where('package_id', $package->id)
            ->paginate(10)
            ->setPath('members/' . $package->id);

        return view('admin.packages.membership.show', compact('package', 'members'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\MembershipPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(MembershipPackage $package)
    {
        if (! Auth::user()->can('update', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $statuses = ['available', 'active', 'closed'];

        return view('admin.packages.membership.edit', compact('package', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\MembershipPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackageForm $form, MembershipPackage $package)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\MembershipPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(MembershipPackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Membership Package Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }


    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(MembershipPackage $package)
    {
        $action = request()->input('action');

        if (! Auth::user()->can($action, $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $members_count = \DB::table('membership_payments')
            ->where('package_id', $package->id)
            ->count();

        return view('admin.packages.membership.partials.confirmation', compact('package', 'members_count', 'action'));
    }

    public function members(MembershipPackage $package)
    {
        $members = \DB::table('membership_payments')
            ->select('user_id', 'username', 'firstname', 'lastname', 'amount', 'start_date', 'end_date', 'membership_payments.status')
            ->join('users', 'users.id', '=', 'membership_payments.user_id')
            ->where('package_id', $package->id)
            ->paginate(10)
            ->setPath('members/' . $package->id);

        return view('admin.packages.membership.partials.members-table', compact('package', 'members'));
    }

}
