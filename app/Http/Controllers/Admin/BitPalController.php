<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\BitPalAPI;
use App\Helpers\LpjHelpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BitPalController extends Controller
{
    public function oAuthCode( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => '',
        );

        $user   = Auth::user();
        $userId = Auth::id();


        # redirect if no "code" in query string
        # =====================================================
        if ( !isset($_GET['code']) ) {
            return redirect('admin/tpo-bank');
            exit();
        }


        # get paramters
        # =====================================================
        $code = LpjHelpers::fss($_GET['code']);
        $mode = isset($_GET['m']) ? LpjHelpers::fss($_GET['m']) : 'a';

        $clientId = BitPalAPI::config('client_id');
        $clientSecret = BitPalAPI::config('client_secret');

        # retrieve access token
        # =====================================================
        $callbackUrl = url('/').'/admin/oauth-code';
        $authCodeRes = BitPalAPI::getAccessToken( $code, $callbackUrl );

        # check api error
        if ( isset($authCodeRes->error) ) {
            $ret['isError'] = true;
            $ret['msg'] = $authCodeRes->error;

            //dd($authCodeRes);

            if ( $authCodeRes->error == 'invalid_grant') {
                $ret['msg'] = 'Invalid Access Token.';
            }
        }

        # check empty access token
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( empty($authCodeRes->access_token) ) {
                $ret['isError'] = true;
                $ret['msg'] = 'Empty Access Token';

            }else{
                $params = array(
                    'bitpal_access_token' => $authCodeRes->access_token,
                    'bitpal_access_token_time' => time()
                );
                // UserMeta::updateUserMeta( $userId, $params );
                $user->update($params);
            }
        }

        # get bitshares wallets from api
        # =====================================================
        $accountsDropdown = array();
        if ( $ret['isError'] == false ) {
            $getBtsAcctsRes = $this->getBitsharesAccounts( $authCodeRes->access_token );

            if ( $getBtsAcctsRes['isError'] == true ) {
                $ret['isError'] = true;
                $ret['msg'] = $getBtsAcctsRes['msg'];
            }else{
                $accountsDropdown = $getBtsAcctsRes['btsAccountsDropdown'];
            }
        }

        # save/update bitshares wallet
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( !empty($accountsDropdown) ) {
                $params = array(
                    'bitpal_bts_accounts' => json_encode($accountsDropdown),
                    'bitpal_default_bts_account' => $accountsDropdown[0]
                );
                // UserMeta::updateUserMeta( $userId, $params );
                $user->update($params);

            }else{
                $ret['isError'] = true;
                $ret['msg'] = "The Bitshares wallet you are trying to connect to your TPO account is already connected to another TPO account.<br><br> Connecting one BitPalBTS account to more than one TPO accounts is NOT allowed.";
            }
        }

        if ( $ret['isError'] == false ) {
            $ret['msg'] = 'Authentication successful.';
        }

        # redirect to tpo bank
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( $mode == 'w') {
                $bankUrl = url('/').'/admin/payouts/';
                return redirect( $bankUrl );
                exit();

            }else{
                $bankUrl = url('/').'/admin/payouts/';
                return redirect( $bankUrl );
                exit();
            }
        }

        return view('admin.bitpal.oauth-code', $ret);
    }

    public function getBitsharesAccounts( $accessToken ){
        $btsAccountsDropdown = array();
        $btsAccountsAssets = array();

        $ret = array(
            'isError' => false,
            'msg' => '',
        );

        $btsAccounts = BitPalAPI::getBitsharesAccountsFromApi( $accessToken );
        //dd($btsAccounts);

        $apiErrorMsg = BitPalAPI::parseBitpalMsg($btsAccounts);

        if ( !empty($apiErrorMsg) ) {
            $ret['isError'] = true;
            $ret['msg'] = $apiErrorMsg;
            $ret['apiFullRet'] = $btsAccounts;

            return $ret;
        } else {
            $btsAccountsDropdown = [];
            foreach ($btsAccounts->data as $value) {
                $btsAccountsDropdown[] = $value->username;
            }

            $ret['btsAccountsDropdown'] = $btsAccountsDropdown;

            return $ret;
        }
    }

    public function getExchangeRate(Request $request){
        $rate = BitPalAPI::getExchangeRate( 'USD', 'BTS');
        //echo $rate;

        $ret = array(
            'usd_to_bts_rate' => $rate,
        );

        return response()->json($ret);
    }
}
