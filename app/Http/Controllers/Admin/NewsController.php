<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LpjHelpers;
use App\Http\Controllers\Controller;
use App\Model\Admin\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Input};

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Auth::user()->can('index', News::class)) {
            return view('admin.errors.403');
        }

        $news_collection = News::latest()->paginate(4);

        if (request()->ajax()) {
            return view('admin.news.partials.news-list', compact('news_collection'))->render();
        }

        return view('admin.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', News::class)) {
            return view('admin.errors.403-alert');
        }

        $news = new News;

        return view('admin.news.create', compact('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required | max:50',
            'content' => 'required'
        ]);

        News::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'thumbnail_path' => Input::hasFile('thumbnail') ? $request->file('thumbnail')->store('thumbnails', 'public') : null
        ]);

        return response()->json([
            'title'     => 'Success',
            'message'   => 'News Successfully Created',
            'status'    => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        if (! Auth::user()->can('view', $news)) {
            return view('admin.errors.403-alert');
        }

        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        if (! Auth::user()->can('update', $news)) {
            return view('admin.errors.403-alert');
        }

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $data = $request->validate([
            'thumbnail' => 'image',
            'title' => 'required | max:50',
            'content' => 'required'
        ]);

        // $news->update($data);

        $news->update([
            'title' => $data['title'],
            'content' => $data['content'],
        ]);

        if (Input::hasFile('thumbnail')) {
            $news->update([
                'thumbnail_path' => $request->file('thumbnail')->store('thumbnails', 'public')
            ]);
        }


        return response()->json([
            'title'     => 'Success',
            'message'   => 'News Successfully Updated',
            'status'    => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $news->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'News Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(News $news)
    {
        if (! Auth::user()->can('delete', $news)) {
            return view('admin.errors.403-alert');
        }

        $action = request()->input('action');

        return view('admin.news.partials.confirmation', compact('news', 'action'));
    }
}
