<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\{Admin, UserBank};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', UserBank::class)) {
            return view('admin.errors.403');
        }

        // All Transactions
        $transactions = UserBank::getTransactions(['limit' => 10]);
        $transactions->withPath('tpo-bank-transaction');

        $summary = $this->computeTransactionsSummary();

        return view('admin.bank.admin.index', compact( 'transactions', 'summary' ) );
    }

    public function sendFund()
    {
        if (! Auth::user()->can('send-fund', UserBank::class)) {
            return response()->json([
                'title'     => 'Access denied!',
                'message'   => 'Full authentication is required to access this resource.',
                'status'    => 'error'
            ]);
        }

        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $data = request()->validate([
                'amount'        => 'required|numeric',
                'username'      => 'required',
                'description'   => 'required'
            ]);

            UserBank::addFund($data['username'], $data['amount'], $data['description']);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Send Fund Successful',
                'status'    => 'success'
            ]);
        }
    }

    public function sendFundConfirmation()
    {
        $data = request()->validate([
            'amount'      => 'required|numeric',
            'username'    => 'required',
            'description' => 'required',
        ]);

        if (! Auth::user()->can('send-fund', UserBank::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        extract($data);

        return view('admin.bank.admin.partials.confirmation', compact('amount', 'username', 'description'));
    }

    public function computeTransactionsSummary()
    {
        $in_transactions = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['type', '=', 'in'] ] ])->sum('amount'); // In Transactions
        $out_transactions = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['type', '=', 'out'] ] ])->sum('amount'); // Out Transactions
        $balance = \DB::table('users_bank')->get()->sum('available_bal'); // Bank Balance
        $added_fund = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['section', '=', 'deposit'] ] ])->sum('amount'); // Added Fund
        $closed_package_fund = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['section', '=', 'closed-package-fund'] ] ])->sum('amount'); // Closed Package Fund
        $payouts = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['section', '=', 'withdrawal'] ] ])->sum('amount'); // Payouts
        $package_payment = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['section', '=', 'package-payment'] ] ])->sum('amount'); // Package Payment
        $cap_off_trade_amount = UserBank::getTransactions(['limit' => -1, 'conditions' => [ ['section', '=', 'cap-off-trade-amount'] ] ])->sum('amount'); // Cap Off Trade Amount
        $referral_credits = \DB::table('users_bank')->sum('referral_credit'); // Referral Credits
        $package_earnings = \DB::table('users_bank')->sum('package_earning'); // Package Earnings

        $summary = [
            'added_fund'            => $added_fund,
            'referral_credits'      => $referral_credits,
            'package_earnings'      => $package_earnings,
            'closed_package_fund'   => $closed_package_fund,
            'payouts'               => $payouts,
            'package_payment'       => $package_payment,
            'cap_off_trade_amount'  => $cap_off_trade_amount,
            'balance'               => $balance
        ];

        return $summary;
    }
}
