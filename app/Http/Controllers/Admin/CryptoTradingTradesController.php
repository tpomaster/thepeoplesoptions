<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoTradingPackage;
use App\Model\Admin\CryptoTradingTrade;
use App\Model\Admin\CryptoTradingTradesQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class CryptoTradingTradesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CryptoTradingTrade  $trade
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoTradingTrade $trade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CryptoTradingTrade  $trade
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoTradingTrade $trade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CryptoTradingTrade  $trade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CryptoTradingTrade $trade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CryptoTradingTrade  $trade
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoTradingTrade $trade)
    {

        try {
            \DB::table('crypto_trading_packages')
                ->where([
                    ['id', $trade->package_id],
                    ['posted_trades', '!=', 0]
                ])
                ->decrement('posted_trades');

            \DB::table('crypto_trading_funds')
                ->where([
                    ['package_id', $trade->package_id],
                    ['trade_count', '!=', 0],
                    ['trade_status', 'active']
                ])
                ->decrement('trade_count');

            \DB::table('crypto_trading_trades')
                ->where('id', $trade->id)
                ->delete();

            \DB::table('crypto_trading_user_trades')
                ->where('trade_id', $trade->id)
                ->delete();

            \DB::table('admin_earnings')
                ->where([
                    ['ref_id', $trade->id],
                    ['package_cat', 'cryptotrading']
                ])
                ->delete();

            \DB::table('package_earnings')
                ->where([
                    ['profit_id', $trade->id],
                    ['package_cat', 'cryptotrading']
                ])
                ->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Trade Successfully Deleted!',
                'status'    => 'success'
            ]);

        } catch (Exception $e) {
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoTradingTrade $trade)
    {
        $action = request()->input('action');

        return view('admin.packages.crypto-trading.trades.confirmation', compact('trade', 'action'));
    }

    /**
     * Compute the given queued trade.
     */
    public function computeTrade()
    {

        /*request()->validate([
            'queued_trade_id' => 'required|numeric'
        ]);*/

        $response = array(
            'title' => 'Trade',
            'error' => false,
            'message' => '',
        );

        $queue_id = request('queued_trade_id');

        // Get queued trade using queued trade id
        // $queued_trade = CryptoTradingTradesQueue::where('id', $queue_id)->first();
        $queued_trade = \DB::table('crypto_trading_trades_queue')
            ->where('id', $queue_id)
            ->where('status', 'active')
            ->first();

        if ( ! $queued_trade ) {
            $response['error'] = true;
            $response['status'] = 'completed';
            $response['message'] = 'All queued trade scheduled for posting today are now processed.';
        }

        # check double entry
        # ---------------------------------------
        $uncomputed_profits_cnt = 0;
        $existing_trade = false;
        if ( $response['error'] === false ) {
            $existing_trade = \DB::table('crypto_trading_trades')
                        ->where('package_id', $queued_trade->package_id)
                        ->where('code', $queued_trade->code)
                        ->first();

            if ( $existing_trade ) {
                # count package funds that haven't received profit from this trade
                $uncomputed_profits_cnt = \DB::table('crypto_trading_funds')
                    ->where([
                        ['package_id', $queued_trade->package_id],
                        ['trade_status', 'active'],
                        ['last_trade_code', '<>', $queued_trade->code]
                    ])
                    ->if($queued_trade->for_membership, 'created_at', '<', $queued_trade->date )
                    ->count();
            }
        }

        if ($response['error'] === false) {
            # Continue computing user trades
            # ---------------------------------------
            if ($existing_trade) {

                if ($uncomputed_profits_cnt > 0) {

                    $this->computeUserTrade($existing_trade);

                    $response['status'] = 'incomplete';
                    $response['message'] = 'Please wait I`m still computing. ( queue id: '.$queue_id.' )';

                } else {
                    $response['status'] = 'completed';
                    $response['message'] = 'Trade profits are now successfully computed. ( queue id: '.$queue_id.' )';

                    \DB::table('crypto_trading_trades_queue')
                        ->where('id', $queue_id)
                        ->update(['status' => 'completed']);
                }

            } else {

                $trading_package = CryptoTradingPackage::where('id', $queued_trade->package_id)->firstOrFail();

                if ( ! $trading_package->start_date && ! $trading_package->for_membership ) {
                    \DB::table('crypto_trading_packages')
                        ->where('id', $trading_package->id)
                        ->update(['start_date' => $queued_trade->date]);
                }

                if ( ! $trading_package->end_date && ! $trading_package->for_membership ) {
                    $add_days = $trading_package->duration - 1;
                    \DB::table('crypto_trading_packages')
                        ->where('id', $trading_package->id)
                        ->update( ['end_date' => date('Y-m-d', strtotime($queued_trade->date. ' + '. $add_days .' days'))] );
                }

                if ( ($trading_package->posted_trades < $trading_package->duration) || $trading_package->for_membership ) {
                    // Save queued trade to trades
                    $trade = CryptoTradingTrade::create([
                        'package_id'        => $queued_trade->package_id,
                        'code'              => $queued_trade->code,
                        'num_of_trades'     => $queued_trade->num_of_trades,
                        'date'              => $queued_trade->date,
                        'trade_amount'      => $queued_trade->trade_amount,
                        'profit'            => $queued_trade->profit,
                        'profit_percent'    => $queued_trade->profit_percent,
                        'for_membership'    => $queued_trade->for_membership,
                    ]);

                    $trading_package->increment('posted_trades');

                    $this->computeUserTrade($trade);

                    $response['status'] = 'incomplete';
                    $response['message'] = 'Start computing trade profits. ( queue id: '.$queue_id.' )';
                } else {
                    $response['status'] = 'completed';
                    $response['message'] = 'Package reached maximum posted count.';

                    \DB::table('crypto_trading_trades_queue')
                        ->where('id', $queue_id)
                        ->update(['status' => 'completed']);
                }

            }
        }

        return response()->json($response);
    }

    private function computeUserTrade($trade)
    {
        $package = \DB::table('crypto_trading_packages')
                ->where('id', $trade->package_id)
                ->first();

        if ($trade->for_membership) {
            $package->amount = $trade->trade_amount;
        }

        $package_membership_config = unserialize($package->management_fee);

        # get investors
        $args = array(
            'page'          => 1,
            'limit'         => 10,
            'package_id'    => $trade->package_id,
            'trade_status'  => 'active',
            'last_trade_code' => $trade->code
        );

        if ( $trade->for_membership ) {
            $args['exclude_new_funds']  = true;
            $args['trade_date']         = $trade->date;
        }

        // getInvestors()
        $investors = $this->getInvestors($args);

        foreach ($investors['list'] as $key => $investor) {

            $user = \DB::table('users')
                    ->select('membership')
                    ->where('id', $investor->user_id)->first();

            $management_fee = (int) $package_membership_config[$user->membership];
            $management_fee = $management_fee / 100;

            $investor_percentage = $investor->amount / $package->amount;
            $profit_amount = $trade->profit * $investor_percentage;

            if ($profit_amount <= 0) {
                $management_fee_amount = 0;
                $user_profit = 0;

                # reduce trade amount if its a negative profit
                $new_trade_amount = $investor->amount + $profit_amount;

                \DB::table('crypto_trading_funds')
                    ->where('id', $investor->id)
                    ->update([
                        'amount' => $new_trade_amount,
                        'updated_at' => Carbon::now()
                    ]);
            } else {
                $management_fee_amount = $profit_amount * $management_fee;
                $user_profit = $profit_amount - $management_fee_amount;
            }

            $details = array(
                'package_amount' => $trade->trade_amount,
                'trade_profit'  => $trade->profit,
                'user_profit_share' => $investor_percentage,
                'user_membership' => $user->membership,
                'management_fee'  => $management_fee,
                'management_fee_amount'  => $management_fee_amount,
                'num_of_trades' => $trade->num_of_trades,
                'trade_code' => $trade->code
            );

            $user_profit_trade = [
                'package_id' => $trade->package_id,
                'trade_id' => $trade->id,
                'package_fund_id' => $investor->id,
                'user_id' => $investor->user_id,
                'date' => $trade->date,
                'trade_amount' => $investor->amount,
                'trade_earning' => $profit_amount,
                'management_fee' => $management_fee_amount,
                'user_profit' => $user_profit,
                'status' => 'completed',
                'details' => json_encode($details),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            if ( ! $investor->trade_start_date ) {
                \DB::table('crypto_trading_funds')
                    ->where('id', $investor->id)
                    ->update(['trade_start_date' => $trade->date]);
            }

            if ( ! $investor->trade_end_date ) {
                $add_days = $package->duration - 1;
                \DB::table('crypto_trading_funds')
                    ->where('id', $investor->id)
                    ->update(['trade_end_date' => date('Y-m-d', strtotime($trade->date. ' + '. $add_days .' days'))] );
            }

            \DB::table('crypto_trading_funds')
                    ->where('id', $investor->id)
                    ->increment('trade_count');

            \DB::table('crypto_trading_user_trades')
                ->insert( $user_profit_trade );

            # insert tpo admin earnings
            if ( $management_fee_amount > 0 ) {
                $admin_profit_earning = array(
                    'package_id' => $trade->package_id,
                    'package_cat' => 'cryptotrading',
                    'ref_id' => $trade->id,
                    'description' => 'Crypto Trading Management Fee',
                    'amount'  => $management_fee_amount,
                    'date' => $trade->date,
                    'details' => '',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                );

                \DB::table('admin_earnings')
                    ->insert($admin_profit_earning);
            }


            $update_columns = [
                'last_trade_code' => $trade->code,
                'last_trade_date' => $trade->date,
                'updated_at'      => Carbon::now()
            ];

            if ($package->for_membership) {
                $trade_count = $investor->trade_count + 1;

                if ($trade_count >= $package->duration) {
                    $update_columns['trade_status'] = 'closed';
                }
            }



            \DB::table('crypto_trading_funds')
                ->where('id', $investor->id)
                ->update($update_columns);
        }
    }

    private function getInvestors(array $args) {
        $defaults = [
            'page'          => 1,
            'limit'         => 10,
            'orderby'       => 'id',
            'order'         => 'asc',
            'id'            => null,
            'package_id'    => null,
            'user_id'       => null,
            'trade_status'  => null,
            'last_trade_code'   => null,
            'exclude_new_funds' => false,
            'adding_new_trades' => false,
            'trade_date'        => date('Y-m-d', time()),
            'data_list'         => null
        ];

        extract( array_merge($defaults, $args) );

        $query = \DB::table('crypto_trading_funds')
            ->if ($id, 'id', '=', $id)
            ->if ($package_id, 'package_id', '=', $package_id)
            ->if ($user_id, 'user_id', '=', $user_id)
            ->if ($trade_status, 'trade_status', '=', $trade_status)
            ->if ($exclude_new_funds, 'created_at', '<', $trade_date)
            ->if ($adding_new_trades, 'last_trade_date', '<', $trade_date)
            ->if ($last_trade_code, 'last_trade_code', '<>', $last_trade_code);

        $investors_list     = $query->orderBy($orderby, $order)->limit($limit)->get();
        $investors_count    = $query->count();
        $investors_funds    = $query->sum('amount');

        $investors = [
            'list'  => $investors_list,
            'count' => $investors_count,
            'funds' => $investors_funds
        ];

        return $investors;
    }
}
