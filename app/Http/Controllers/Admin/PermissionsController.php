<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PermissionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Permission::class)) {
            return view('admin.errors.403');
        }

        $permissions = Permission::all();

        return view('admin.permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Permission::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission = $request->validate([
            'name' => 'required|max:50|unique:permissions',
            'slug' => 'required|alpha_dash|max:50|unique:permissions',
            'category' => 'required|max:50',
        ]);

        Permission::create($permission);

        return redirect(route('permissions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        if (! Auth::user()->can('update', $permission)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        return view('admin.permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $data = $request->validate([
            'name' => 'required|max:50|unique:permissions,name,'.$permission->id,
            'slug' => 'required|alpha_dash|max:50|unique:permissions,slug,'.$permission->id,
            'category' => 'required|max:50',
        ]);

        $permission->update($data);

        return redirect(route('permissions.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        if (! Auth::user()->can('delete', $permission)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $permission->delete();

        return back();
    }
}
