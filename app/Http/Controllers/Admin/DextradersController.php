<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoTradingPackage;
use App\Model\Admin\CryptoTradingTradesQueue;
use App\Model\Admin\CryptoExchangeProfitsQueue;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

use App\Helpers\LpjHelpers;

class DextradersController extends Controller
{   
    public function queue(Request $request)
    {
        //header("Access-Control-Allow-Origin: *");

        # get input
        $inp = $request->all();
        
        $validator = Validator::make($request->all(), [
            'access_token'      => 'required',
            'package_id'        => 'required',
            'package_cat'       => 'required',
            'date'              => 'required',
            'num_of_trades'     => 'required',
            'profit_percent'    => 'required',
            'profit'            => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Fill-out all requied fields!',
                'status'    => 'error',
            ]);
        }


        if ($inp['access_token'] != '6B6A84F8B759F13F1E4FED3AB9C8E') {
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Invalid Access Token.',
                'status'    => 'error',
            ]);
        }


        if ($inp['package_cat'] == 'cryptotrading') {
            $ret = $this->queueCTradingTrade($inp);
            return response()->json($ret);
        }

        if ($inp['package_cat'] == 'cryptoexchange') {
            $ret = $this->queueCExchangeProfit($inp);
            return response()->json($ret);
        }
    }

    public function queue2(Request $request)
    {
        header('Access-Controll-Allow-Methods', 'POST');

        $input = $request->all();
        $input = LpjHelpers::fss($input);
        //dd($input);

        $data = $request->validate([
            'package_id'        => 'required',
            'exec_date'         => 'required',
            'date'              => 'required',
            'num_of_trades'     => 'required',
            'profit_percent'    => 'required',
            'profit'            => 'required'
        ]);


        $package = DB::table('crypto_trading_packages')
                ->select('id', 'amount', 'status', 'for_membership', 'posted_trades', 'duration')
                ->where('id', $data['package_id'])
                ->first();

        if ( ($package->posted_trades >= $package->duration) && !$package->for_membership ) {
            return response()->json([
                'title'     => 'Posted Maximum Trades',
                'message'   => 'Package reached maximum trades post count.',
                'status'    => 'info'
            ]);
        }

        if ($package->for_membership) {
            $package_fund = DB::table('crypto_trading_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;
        }

        $data['status']         = 'active';
        $data['for_membership'] = $package->for_membership;
        $data['code']           = strtotime(date('Y-m-d H:i:s')) . rand(10000, 99999);
        $data['exec_date']      = date('Y-m-d', strtotime("+ 1 day"));

        $tradeData = array(
            'package_id' 
        );

        try {
            CryptoTradingTradesQueue::create($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Trading Trade Successfully Saved',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }

        return response()->json($response);
    }

    private function queueCTradingTrade($inp)
    {
        $tradeData = array();

        $tradeData['package_id'] = $inp['package_id'];
        $tradeData['date'] = $inp['date'];
        $tradeData['num_of_trades'] = $inp['num_of_trades'];
        $tradeData['profit_percent'] = $inp['profit_percent'];
        $tradeData['trade_amount'] = $inp['trade_amount'];
        $tradeData['profit'] = $inp['profit'];

        $tradeData['status']         = 'active';
        $tradeData['code']           = strtotime(date('Y-m-d H:i:s')) . rand(10000, 99999);
        $tradeData['exec_date']      = date('Y-m-d', strtotime("+ 1 day"));


        # get package
        $package = \DB::table('crypto_trading_packages')
            ->select('id', 'amount', 'status', 'for_membership', 'posted_trades', 'duration')
            ->where('id', $inp['package_id'])
            ->first();

        if (!$package) {
            return array(
                'title'     => 'Oh No!',
                'message'   => 'I cant find the package!',
                'status'    => 'error',
            );
        }

        if ($package->for_membership) {
            $package_fund = DB::table('crypto_trading_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;
        }

        # check max limit
        if ( ($package->posted_trades >= $package->duration) && !$package->for_membership ) {
            return array(
                'title'     => 'Posted Maximum Trades',
                'message'   => 'Package reached maximum trades post count.',
                'status'    => 'info'
            );
        }

        $tradeData['for_membership'] = $package->for_membership;
        
        try {
            CryptoTradingTradesQueue::create($tradeData);

            return array(
                'title'     => 'Success',
                'message'   => 'Crypto Trading Trade Successfully Saved',
                'status'    => 'success'
            );

        } catch (\Exception $e) {
            return array(
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            );
        }
    }

    private function queueCExchangeProfit($inp){
        $data = array(
            'package_id'        => $inp['package_id'],
            'exec_date'         => date('Y-m-d', strtotime("+ 1 day")),
            'date'              => $inp['date'],
            'profit_percent'    => $inp['profit_percent'],
            'profit'            => $inp['profit'],
            'status'            => 'active',
            'code'              => strtotime(date('Y-m-d H:i:s')) . rand(10000, 99999),
            'exchange_amount'   => $inp['trade_amount']
        );

        
        # get package info
        $package = \DB::table('crypto_exchange_packages')
                ->select('id', 'amount', 'status', 'for_membership', 'posted_profits', 'duration')
                ->where('id', $data['package_id'])
                ->first();

        if ( ($package->posted_profits >= $package->duration) && !$package->for_membership) {
            return array(
                'title'     => 'Posted Maximum Profit',
                'message'   => 'Package reached maximum profits post count.',
                'status'    => 'info'
            );
        }

        if ($package->for_membership) {
            $package_fund = \DB::table('crypto_exchange_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;
        }

        //$data['exchange_amount']  = $package->amount;
        $data['for_membership'] = $package->for_membership;
        

        try {
            CryptoExchangeProfitsQueue::create($data);

            return array(
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Profits Successfully Saved',
                'status'    => 'success'
            );

        } catch (\Exception $e) {
            return array(
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            );
        }
    }

}