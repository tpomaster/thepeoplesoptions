<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\TpoMarketingAPI;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TPOMarketingController extends Controller
{
    public function login()
    {
        if (! Auth::user()->can('tpo-marketing', Auth::user())) {
            return [
                'title'     => 'TPO Marketing Login',
                'message'   => 'Full authentication is required to access this resource.',
                'status'    => 'error'
            ];
        }

        $error          = false;
        $message        = '';
        $redirect_url   = '';

        $args = [
            'username'  => 'tpo master',
            'password'  => '$P$Bq/sN1XctX423yoHXwWDOXLd05LY1c/'
        ];

        $login_data = array(
            'username' => $args['username'],
            'password' => $args['password']
        );

        $tpo_marketing = TpoMarketingAPI::login( $login_data );

        if ( $tpo_marketing ) {
            if ( isset($tpo_marketing->is_error) && $tpo_marketing->is_error ) {
                $error      = true;
                $message    = 'Failed to login to TPO Marketing.';
            } else {
                $message = 'Successfully Logged In! Redirecting, Please wait...';
                $redirect_url = TpoMarketingAPI::config('api_url').'?login='.$tpo_marketing->login_token;
            }
        } else {
            $message = 'TPO Marketing API Connection Error.';
        }

        $response = [
            'title'     => 'TPO Marketing Login',
            'message'   => $message,
            'status'    => $error ? 'error' : 'success',
            'url'       => $redirect_url
        ];

        return response()->json($response);
    }
}
