<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\CryptoMiningPackage;
use App\Model\Admin\CryptoMiningProfitsQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CryptoMiningProfitsQueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('compute', CryptoMiningPackage::class)) {
            return view('admin.errors.403');
        }

        $packages = \DB::table('crypto_mining_packages')
                ->where('status', 'funded-active')
                ->get();

        $queued_profits = \DB::table('crypto_mining_profits_queue as profits_queue')
                ->join('crypto_mining_packages as packages', 'profits_queue.package_id', '=', 'packages.id' )
                ->select('profits_queue.id as id', 'name', 'profit', 'date', 'exec_date')
                ->where('profits_queue.status', 'active')
                ->orderBy('date', 'asc')
                ->paginate(50);

        if (request()->expectsJson()) {
            return view('admin.packages.crypto-mining.profit-queue.partials.table', compact('queued_profits'))->render();
        }

        return view('admin.packages.crypto-mining.profit-queue.index', compact('packages', 'queued_profits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CryptoMiningPackage $package)
    {
        $packages = \DB::table('crypto_mining_packages')
            ->where('status', 'funded-active')
            ->get();

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.profit-queue.create', compact('package', 'packages'))->render();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'package_id'        => 'required',
            'exec_date'         => 'required',
            'date'              => 'required',
            'profit_percent'    => 'required',
            'profit'            => 'required'
        ]);

        $date_queued = \DB::table('crypto_mining_profits_queue')
            ->whereDate('date', $data['date'])
            ->where('package_id', $data['package_id'])
            ->get();

        $date_exists = \DB::table('crypto_mining_user_profits')
            ->whereDate('date', $data['date'])
            ->where('package_id', $data['package_id'])
            ->get();

        if ($date_exists->count() > 0) {
            return response()->json([
                'title'     => 'Duplicate Post Date',
                'message'   => 'Package profit is already posted for this date ('. $data['date'] .').',
                'status'    => 'info'
            ]);
        }

        if ($date_queued->count() > 0) {
            return response()->json([
                'title'     => 'Duplicate Queue',
                'message'   => 'Package profit date (' .$data['date']. ') is already on queue.',
                'status'    => 'info'
            ]);
        }

        $package = \DB::table('crypto_mining_packages')
                ->select('id', 'amount', 'status', 'for_membership', 'posted_profits', 'duration')
                ->where('id', $data['package_id'])
                ->first();

        if ( ($package->posted_profits >= $package->duration) && !$package->for_membership ) {
            return response()->json([
                'title'     => 'Posted Maximum Profit',
                'message'   => 'Package reached maximum profits post count.',
                'status'    => 'info'
            ]);
        }

        if ($package->for_membership) {
            $package_fund = \DB::table('crypto_mining_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;
        }

        $data['mining_amount']  = $package->amount;
        $data['status']         = 'active';
        $data['for_membership'] = $package->for_membership;
        $data['code']           = strtotime(date('Y-m-d H:i:s')) . rand(10000, 99999);

        try {
            CryptoMiningProfitsQueue::create($data);

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Mining Trade Successfully Saved',
                'status'    => 'success'
            ]);

        } catch (\Exception $e) {
            // return $e->getMessage();
            return response()->json([
                'title'     => 'Oh No!',
                'message'   => 'Oops Something went wrong!',
                'status'    => 'error'
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\CryptoMiningProfitsQueue  $profit
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoMiningProfitsQueue $profit)
    {
        $package = \DB::table('crypto_mining_packages')
                ->where('id', $profit->package_id)
                ->first();

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.profit-queue.edit', compact('profit', 'package'))->render();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\CryptoMiningProfitsQueue  $profit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CryptoMiningProfitsQueue $profit)
    {
        if ($request->ajax()) {
            $data = $request->validate([
                'exec_date'         => 'required',
                'date'              => 'required',
                'profit_percent'    => 'required|numeric',
                'profit'            => 'required|numeric'
            ]);

            try {
                $profit->update($data);

                return response()->json([
                    'title'     => 'Success',
                    'message'   => 'Crypto Mining Profit Successfully Updated',
                    'status'    => 'success'
                ]);

            } catch (\Exception $e) {
                // return $e->getMessage();
                return response()->json([
                    'title'     => 'Oh No!',
                    'message'   => 'Oops Something went wrong!',
                    'status'    => 'error'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\CryptoMiningProfitsQueue  $profit
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoMiningProfitsQueue $profit)
    {
        if ( request()->ajax() ) {
            try {
                $data = request()->validate(
                    ['pincode' => 'required|numeric']
                );

                if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
                    $profit->delete();

                    return response()->json([
                        'title'     => 'Success',
                        'message'   => 'News Successfully Deleted',
                        'status'    => 'success'
                    ]);
                }
            } catch (\Exception $e) {
                return response()->json([
                    'title'     => 'Oh No!',
                    'message'   => 'Oops Something went wrong!',
                    'status'    => 'error'
                ]);
            }
        }
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoMiningProfitsQueue $profit)
    {
        $package = \DB::table('crypto_mining_packages')
                ->where('id', $profit->package_id)
                ->first();

        // $action = request()->input('action');

        if ( request()->ajax() ) {
            return view('admin.packages.crypto-mining.profit-queue.partials.confirmation', compact('profit', 'package', 'action'));
        }
    }

}
