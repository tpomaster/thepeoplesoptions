<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (! Auth::user()->can('index', UserBank::class)) {
            return view('admin.errors.403');
        }

        $users = \DB::table('users')
                ->join('users_bank', 'users.id', '=', 'users_bank.user_id')
                ->select('users.*', 'users_bank.available_bal')
                ->paginate(10);

        if (request()->ajax()) {
            return view('admin.bank.users.users-list', compact('users'))->render();
        }

        return view('admin.bank.users.index', compact('users'));
    }
}
