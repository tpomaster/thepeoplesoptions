<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Admin\{StoreAdminForm, UpdateAdminForm};
use App\Model\Admin\{Admin, Permission, Role};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Gate};
use Illuminate\Validation\Rule;

class AdminsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Admin::class)) {
            return view('admin.errors.403');
        }

        $args   = [ 'roles_not_in' => ['super-admin'] ];
        $admins = Admin::getAdmins($args);
        $roles  = Role::all();

        if (request()->ajax()) {
            return view('admin.admins.table', compact('admins', 'roles'))->render();
        }

        return view('admin.admins.index', compact('admins', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', Admin::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $admin          = new Admin;
        $roles          = Role::where('slug', '!=', 'super-admin')->get();
        $permissions    = $this->organizePermissions();
        $selected_permissions = [];

        // return view('admin.admins.modal.create', compact('roles', 'permissions'));
        return view('admin.admins.create', compact('admin', 'roles', 'permissions', 'selected_permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdminForm $form)
    {
        return $form->response();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Auth::user()->can('update', Admin::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $admin          = Admin::findOrFail($id);
        // $admin_role     = $admin->roles->first();
        $roles          = Role::where('slug', '!=', 'super-admin')->get();
        $permissions    = $this->organizePermissions();

        $selected_permissions = \DB::table('admin_permission')->where('admin_id', $admin->id)->pluck('permission_id')->toArray();

        return view('admin.admins.edit', compact('admin', 'roles', 'permissions', 'selected_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminForm $form, Admin $admin)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin) // use route model binding
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $admin->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'User successfully deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(Admin $admin)
    {
        if (! Auth::user()->can('delete', Admin::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $action = request()->input('action');

        return view('admin.admins.partials.confirmation', compact('admin', 'action'));
    }

    protected function syncRolePermissions(Admin $admin, Role $role) {
        // Sync records to admin_role
        $admin->roles()->sync($data['role']);

        // Sync role permissions to admin permissions
        foreach ($admin->roles as $role) {
            $role_permissions = \DB::table('permission_role')->select('permission_id')->where('role_id', $role->id)->get();
            $permission_ids = [];

            foreach ($role_permissions->toArray() as $role_permission) {
                $permission_ids[] = $role_permission->permission_id;
            }

            $admin->permissions()->sync($permission_ids);
        }

        return $this;
    }

    protected function syncAdminPermissions() {
        $admin->permissions()->syncWithoutDetaching(request()->permissions);

        return $this;
    }

    /**
     * Get super admins
     * @return [type] [description]
     */
    protected function getSuperAdminIDs() {
        // Get all super admins
        $super_admins = \DB::table('admin_role')->where('role_id', 1)->pluck('admin_id');

        return $super_admins;
    }

    public function organizePermissions()
    {
        $permissions = Permission::select('id', 'name', 'slug', 'category')
            ->whereNotin('category', ['role', 'permission'])
            ->get()->toArray();

        $result         = [];

        foreach ($permissions as $permission) {
            $result[$permission['category']][] = $permission;
        }

        return $result;
    }

    public function allAdmins(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'firstname',
            2 => 'lastname',
            3 => 'email',
            4 => 'username',
            5 => 'role',
            6 => 'status',
            7 => 'id'
        );

        $totalData = Admin::whereHas('roles', function ($query) {
                $query->where('slug', '!=', 'super-admin');
            })->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $super_admin_ids = \DB::table('admin_role')->where('role_id', 1)->pluck('admin_id');

        if(empty($request->input('search.value')))
        {
            $admins = Admin::whereHas('roles', function ($query) {
                $query->where('slug', '!=', 'super-admin');
            })->offset($start)->limit($limit)->orderBy($order,$dir)->get();
        } else {
            $search = $request->input('search.value');

            $admins = Admin::where('id','LIKE',"%{$search}%")
                            ->orWhere('firstname', 'LIKE',"%{$search}%")
                            ->orWhere('lastname', 'LIKE',"%{$search}%")
                            ->whereNotin('id', $super_admin_ids)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            /*$admins = Admin::whereHas('roles', function ($query) {
                $query->where('slug', '!=', 'super-admin');
            })->orWhere('firstname', 'LIKE',"%{$search}%")->offset($start)->limit($limit)->orderBy($order,$dir)->get();*/

            $totalFiltered = Admin::where('id','LIKE',"%{$search}%")
                             ->orWhere('firstname', 'LIKE',"%{$search}%")
                             ->orWhere('lastname', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($admins))
        {
            foreach ($admins as $admin)
            {
                $edit =  route('admin.edit', $admin->id);
                $destroy =  route('admin.destroy', $admin->id);
                $confirmation = route('admin.confirmation', $admin->id);
                $role = '';
                $actions = '';

                foreach ($admin->roles as $role) {
                    $role = $role->name;
                }


                    // $actions.= "<a href='{$edit}' title='EDIT' class='btn btn-xs theme-btn'><span class='glyphicon glyphicon-edit'></span></a>";
                    $actions .= "<a href='{$edit}' class='modal-btn btn btn-xs theme-btn' title='Edit Profile' data-id = '$admin->id' data-action='edit' data-model='users' data-title='Edit Admin User'><span class='glyphicon glyphicon-edit'></span></a>";



                    $actions.= "<a href='{$confirmation}' title='DELETE' data-token='".csrf_field()."' class='delete-user btn btn-xs btn-danger'><span class='glyphicon glyphicon-remove'></span></a>";
                    // $actions.="<form action='$destroy' method='POST'>".csrf_field()." ".method_field('DELETE')."<button type='submit' class='btn btn-xs btn-danger'><span class='glyphicon glyphicon-remove'></span></button></form>";


                $nestedData['id']           = $admin->id;
                $nestedData['firstname']    = $admin->firstname;
                $nestedData['lastname']     = $admin->lastname;
                $nestedData['email']        = $admin->email;
                $nestedData['username']     = $admin->username;
                $nestedData['role']         = $role;
                $nestedData['status']       = $admin->status;
                $nestedData['actions']      = $actions;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
