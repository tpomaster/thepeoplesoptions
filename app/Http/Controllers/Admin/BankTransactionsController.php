<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class BankTransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // All Transactions
        $transactions = UserBank::getTransactions(['limit' => 10]);
        $transactions->withPath('tpo-bank-transaction');

        if (request()->ajax()) {
            return view('admin.bank.transactions.transaction-list', compact('transactions'))->render();
        }

        // return view('admin.packages.crypto-exchange.index', compact('packages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transactions = \DB::table('users_bank_transactions')
                ->where('user_id', $id)
                ->orderBy('transaction_date', 'desc')
                ->paginate(10);

        return view('admin.bank.transactions.show', compact('transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = \DB::table('users_bank_transactions')
                        ->where('id', $id)
                        ->first();

        if (request()->ajax()) {
            return view('admin.bank.transactions.edit', compact('transaction'));
        }

        return "Error on edit transaction";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'description' => 'required'
        ]);

        \DB::table('users_bank_transactions')
            ->where('id', $id)
            ->update(['description' => $data['description']]);

        return response()->json([
            'title'     => 'Success',
            'message'   => 'Bank Transaction History Updated!',
            'status'    => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {

            \DB::table('users_bank_transactions')->where('id', $id)->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Bank Transaction History Deleted!',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation($id)
    {
        $transaction = \DB::table('users_bank_transactions')
                ->where('id', $id)
                ->first();

        return view('admin.bank.transactions.confirmation', compact('transaction'));
    }

    public function searchUserBank()
    {
        $user_search = request()->input('user_search');
        $filter = request()->input('filter');

        $query = \DB::table('users')
            ->join('users_bank', 'users.id', '=', 'users_bank.user_id')
            ->select('users.*', 'users_bank.available_bal');

        if ($filter == 'firstname') {
            $query->where('users.firstname', 'LIKE', '%'.$user_search.'%');
        } else if ($filter == 'lastname') {
            $query->where('users.lastname', 'LIKE', '%'.$user_search.'%');
        } else if ($filter == 'username') {
            $query->where('users.username', 'LIKE', '%'.$user_search.'%');
        } else {
            $query->where('users.username', 'LIKE', '%'.$user_search.'%')
                ->orWhere('users.firstname', 'LIKE', '%'.$user_search.'%')
                ->orWhere('users.lastname', 'LIKE', '%'.$user_search.'%');
        }

        $users = $query->paginate(1)->setPath('tpo-bank-transaction/search-user');

        $pagination = $users->appends ( array (
            'user_search' => Input::get('user_search')
        ) );

        if (count($users) > 0)
            return view('admin.bank.users.list')->withDetails($users)->withQuery($user_search);
            else return view ('admin.bank.users.list')->withMessage('No Details found. Try to search again !');
    }

    public function filterTransactions(Request $request)
    {
        if (request()->ajax()) {

            $data = request()->validate([
                'to_date' => 'required_with:from_date',
                'from_date' => 'required_with:to_date',
            ]);

            $transactions = DB::table('users_bank_transactions')
                ->join('users', 'users_bank_transactions.user_id', '=', 'users.id')
                ->select('users_bank_transactions.*', 'users.username')
                ->orderBy('transaction_date', 'desc')
                ->if(request()->username, 'users.username', 'LIKE', '%'.request()->username.'%')
                ->if(request()->type, 'type', '=', request()->type)
                ->if(request()->status, 'users_bank_transactions.status', '=', request()->status)
                ->if(request()->section, 'users_bank_transactions.section', '=', request()->section)
                ->when($request->from_date, function($query) use ($request){
                    $from_date = date('Y-m-d', strtotime($request->from_date) );
                    $to_date = date('Y-m-d', strtotime($request->to_date) );

                    return $query->whereBetween('transaction_date', array($from_date, $to_date));
                })
                ->paginate(10);

            return view('admin.bank.transactions.transaction-list-filter', compact('transactions'))->render();
        }
    }
}
