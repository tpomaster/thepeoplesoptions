<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\BitPalAPI;
use App\Http\Controllers\Controller;
use App\Model\Admin\Payout;
use App\Rules\Admin\ValidatePincode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

use Log;

class PayoutsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', Payout::class)) {
            return view('admin.errors.403');
        }

        $bitpal_info = $this->getBitPalInfo();

        $payouts = Payout::orderBy('status', 'desc')
            ->orderBy('request_date', 'desc')
            ->paginate(15);

        $completed = Payout::total('completed');
        $pending = Payout::total('pending');
        $total = $completed + $pending;

        $total_payouts = [
            'all'       => $total,
            'completed' => $completed,
            'pending'   => $pending
        ];

        if (request()->ajax()) {
            return view('admin.payouts.partials.table', compact('payouts'))->render();
        }

        return view('admin.payouts.index', compact('total_payouts', 'bitpal_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\Payout  $payout
     * @return \Illuminate\Http\Response
     */
    public function edit(Payout $payout)
    {
        if (! Auth::user()->can('update', $payout)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $bts_accounts = Auth::user()->accounts();

        if (request()->ajax()) {
            return view('admin.payouts.edit', compact('payout', 'bts_accounts'))->render();
        }

        return abort(403);
    }

    /**
     * Show the form for approving the specified resource.
     *
     * @param  \App\Model\Admin\Payout  $payout
     * @return \Illuminate\Http\Response
     */
    public function approve(Payout $payout)
    {
        if (! Auth::user()->can('approve', $payout)) {
            return response()->json([
                'title'     => 'Failed',
                'message'   => 'Full authentication is required to access this resource.',
                'status'    => 'error',
            ]);
        }

        $data = request()->validate([
            'pincode' => [
                'required',
                'numeric',
                new ValidatePincode(Auth::user())
            ],
            'bts_account'   => 'required',
            'comment'       => 'required'
        ]);

        

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {

            $args = [
                'bts_account'   => request()->input('bts_account'),
                'comment'       => request()->input('comment')
            ];

            try {
                $transfer = $this->sendFund($args, $payout);
                Log::info('approve, transfer:'. json_encode($transfer));

                return response()->json([
                    'title'     => $transfer['error'] ? 'Failed' : 'Success',
                    'message'   => $transfer['message'],
                    'status'    => $transfer['error'] ? 'error' : 'success',
                ]);
            } catch (Exception $e) {
                return response()->json([
                    'title'     => 'Oh No!',
                    'message'   => 'Oops Something went wrong!',
                    'status'    => 'error'
                ]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\Payout  $payout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payout $payout)
    {
        if ( $request->input('pincode') == Auth::user()->pincode ) {

            $payout->approve();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Payout Successfully Approved',
                'status'    => 'success'
            ]);

        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    public function cancel(Request $request, Payout $payout)
    {
        $data = request()->validate(
            ['pincode' => [
                'required',
                'numeric',
                new ValidatePincode(Auth::user())
            ]]
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {

            $payout->update(['status' => 'cancelled']);

            \DB::table('users_bank_transactions')
                ->where('transaction_code', $payout->transaction_code)
                ->update(['status' => 'cancelled']);


            return response()->json([
                'title'     => 'Success',
                'message'   => 'Payout Successfully Cancelled',
                'status'    => 'success'
            ]);

        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     */
    public function confirmation(Payout $payout)
    {
        request()->validate([
            'action' => 'required|alpha_dash'
        ]);

        $action = request()->input('action');
        $route  = '';

        if (! Auth::user()->can($action, $payout)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        if ($action == 'delete')  {
            $route = route('payouts.destroy', $payout->id);
        } else if ($action == 'update' || $action == 'approve') {
            $route = route('payouts.approve', $payout->id);
        } else if ($action == 'cancel') {
            $route = route('payouts.cancel', $payout->id);
        }

        return view('admin.payouts.confirmation', compact('payout', 'action', 'route'));
    }

    public function filter()
    {
        $conditions = [
            ['status', '=', request()->status]
        ];

        $payouts = Payout::where($conditions)
        ->orderBy('request_date', 'desc')
        ->paginate(15);

        if (request()->ajax()) {
            return view('admin.payouts.partials.table', compact('payouts'))->render();
        }
    }

    public function getBitPalInfo()
    {
        $current_user = Auth::user();

        $bitpal_client_id = BitPalAPI::config('client_id');

        $bitpal_info = [
            'access_token'      => $current_user->bitpal_access_token ?: '',
            'access_token_time' => $current_user->bitpal_access_token_time ?: '',
            'accounts'          => $current_user->bitpal_bts_accounts ? json_decode($current_user->bitpal_bts_accounts) : [],
            'usd_to_bts_rate'   => 1,
            'auth_url'          => BitPalAPI::config('api_home_url').'/#/oauth/authorize?client_id='.$bitpal_client_id.'&response_type=code&redirect_uri='.url('/').'/admin/oauth-code',
            'signup_url'        => BitPalAPI::config('api_home_url').'/#/signup',
            'access_token_lifetime' => 1,
            'default_account'   => 1
        ];

        if (! empty($bitpal_info['accounts'])) {
            $bitpal_info['default_account'] = $bitpal_info['accounts'][0];
        }

        # access token lifetime
        if ( $bitpal_info['access_token_time'] ) {
            $access_token_time = $bitpal_info['access_token_time'];
            $seconds_diff = time() - (int) $access_token_time;
            $minute_diff = $seconds_diff / 60;
            $bitpal_info['access_token_lifetime'] = $minute_diff;
        }

        return $bitpal_info;
    }

    public function sendFund($args = [], $payout){
        $error      = false;
        $message    = '';

        $bitpal_info = $this->getBitPalInfo();

        $defaults = [
            'bts_account'   => '',
            'comment'       => ''
        ];

        $args = array_merge($defaults, $args);

        if (! $error) {
            if (! $args['bts_account']) {
                $error      = true;
                $message    = 'Please select/enter your bitshares account/wallet';
            }
        }
        
        if (! $error) {
            if (empty($bitpal_info['access_token'] ||
                empty($bitpal_info['access_token_time'])
            )) {
                $error      = true;
            $message    = 'BitPalBTS Access Token is missing';
        }
    }

    if (! $error) {
        if ($bitpal_info['access_token_lifetime'] > 60) {
            $error      = true;
            $message    = 'BitPalBTS Access Token has expired';
        }
    }

    if (! $payout) {
        $error      = true;
        $message    = 'Invalid payout request.';
    }

    if (! $error) {
        if ($payout->status != 'pending') {
            $error      = true;
            $message    = 'Transaction was approved/cancelled.';
        }
    }

    if (! $error) {
            $usd_btccore = BitPalAPI::getExchangeRate( 'USD', 'BTCCORE'); # get usd amount per 1[asset] value (ex: 0.194 USD per 1 BTS)
            if (!$usd_btccore) {
                $error      = true;
                $message    = 'Failed to pull USD to BTCCORE exchange rate from BitPal API';
            }
        }

        if (! $error && $payout) {
            $payout_details = json_decode($payout->details);

            // $btccore_val = $payout->amount / $usd_btccore;

            $bitpal_params = [
                'access_token'  => $bitpal_info['access_token'],
                'bts_asset'     => $payout->asset,
                'pinned_asset'  => 'fiat',
                'amount'        => BitPalAPI::bitpalAmtFormat($payout->net_amount),
                'to_wallet'     => $payout_details->bts_wallet,
                'from_wallet'   => $args['bts_account'],
                'comment'       => $args['comment']
            ];
            Log::info('PayoutsController:sendfund:bitpal_params: '. json_encode($bitpal_params));

            $transfer = BitPalAPI::transfer($bitpal_params);
            //Log::info('sendfund, BitPalAPI::transfer:'. json_encode($transfer));

            if ($transfer->status == 200) {
                $payout->update([
                    'exchange_rate' => $usd_btccore,
                    'release_date' => Carbon::now(),
                    'status' => 'completed'
                ]);

                \DB::table('users_bank_transactions')
                    ->where('transaction_code', $payout->transaction_code)
                    ->update(['status' => 'completed']);

                $message = 'Payout Successfully Processed.';
            } else {
                $error      = true;
                $message    = BitPalAPI::parseBitpalMsg($transfer);
            }
        }

        return [
            'error'     => $error,
            'message'   => $message,
        ];

    }
}
