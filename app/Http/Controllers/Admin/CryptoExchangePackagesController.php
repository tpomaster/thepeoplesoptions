<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CryptoExchangePackage\{StorePackageForm, UpdatePackageForm};
use App\Model\Admin\CryptoExchangePackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CryptoExchangePackagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Auth::user()->can('index', CryptoExchangePackage::class)) {
            return view('admin.errors.403');
        }

        $packages = CryptoExchangePackage::latest()->paginate(4);
        $options  = CryptoExchangePackage::options();

        if (request()->ajax()) {
            return view('admin.packages.crypto-exchange.partials.package-list', compact('packages'))->render();
        }

        return view('admin.packages.crypto-exchange.index', compact('packages', 'options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Auth::user()->can('create', CryptoExchangePackage::class)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $package = new CryptoExchangePackage;

        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $posting_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        return view('admin.packages.crypto-exchange.create', compact('package', 'statuses', 'risk_profiles', 'posting_frequencies', 'member_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackageForm $form)
    {
        return response()->json($form->response());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Admin\CryptoExchangePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function show(CryptoExchangePackage $package)
    {
        if (! Auth::user()->can('view', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        if (request()->expectsJson()) {
            // Replace package amount with package current funds if package
            $package_fund = \DB::table('crypto_exchange_funds')
                ->where('package_id', $package->id)
                ->sum('amount');

            $package->amount = $package_fund;

            return $package;
        }

        $referral_credits = \DB::table('referral_credits')
            ->where([
                ['package_id', $package->id],
                ['package_cat', 'cryptoexchange']
            ])
            ->sum('amount');

        // Profit Earnings
        $gross_profit = \DB::table('crypto_exchange_user_profits')
                    ->where('package_id', $package->id)
                    ->sum('gross_profit');

        $management_fee = \DB::table('crypto_exchange_user_profits')
                    ->where('package_id', $package->id)
                    ->sum('management_fee');

        $net_earning = \DB::table('crypto_exchange_user_profits')
                    ->where('package_id', $package->id)
                    ->sum('net_profit');

        $earnings = [
            'gross_profit' => $gross_profit,
            'management_fee' => $management_fee,
            'net_earning' => $net_earning
        ];

        $investors = \DB::table('crypto_exchange_funds')
            ->select('user_id', 'username', 'firstname', 'lastname', 'posted_profits_count AS profits_count',
                'amount', 'start_date', 'end_date', 'crypto_exchange_funds.status',
                'crypto_exchange_funds.exchange_status', 'crypto_exchange_funds.created_at as purchase_date')
            ->join('users', 'users.id', '=', 'crypto_exchange_funds.user_id')
            ->orderBy('purchase_date', 'DESC')
            ->where('package_id', $package->id)
            ->paginate(10)
            ->setPath($package->id . '/investors');

        $profits = \DB::table('crypto_exchange_profits')
            ->where('package_id', $package->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10)
            ->setPath($package->id . '/profits');

        $package->computeInvestorsProfits();

        return view('admin.packages.crypto-exchange.show', compact('package', 'referral_credits', 'earnings', 'investors', 'profits'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Admin\CryptoExchangePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(CryptoExchangePackage $package)
    {
        if (! Auth::user()->can('update', $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $posting_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        return view('admin.packages.crypto-exchange.edit', compact('package', 'statuses', 'risk_profiles', 'posting_frequencies', 'member_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Admin\CryptoExchangePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePackageForm $form, CryptoExchangePackage $package)
    {
        return response()->json($form->response());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Admin\CryptoExchangePackage  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(CryptoExchangePackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->delete();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Package Successfully Deleted',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Close the selected package
     *
     * @param  \App\Model\Admin\CryptoTradingPackage  $package
     * @return \Illuminate\Http\Response
     */
    public function close(CryptoExchangePackage $package)
    {
        $data = request()->validate(
            ['pincode' => 'required|numeric']
        );

        if ( Auth::user()->checkPincode(Auth::user(), $data['pincode']) ) {
            $package->end();

            return response()->json([
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Package Successfully Closed',
                'status'    => 'success'
            ]);
        }

        return response()->json([
            'title'     => 'Oh No!',
            'message'   => 'Oops Something went wrong!',
            'status'    => 'error'
        ]);
    }

    /**
     * Show the form for action confirmation.
     *
     * @return \Illuminate\Http\Response
     */
    public function confirmation(CryptoExchangePackage $package)
    {
        $action = request()->input('action');

        if (! Auth::user()->can($action, $package)) {
            if (request()->ajax()) {
                return view('admin.errors.403-alert');
            }
            return view('admin.errors.403');
        }

        $route = $this->getActionRoute($action, $package->id);

        $investors_count = \DB::table('crypto_exchange_funds')
            ->where('package_id', $package->id)
            ->count();

        return view('admin.packages.crypto-exchange.partials.confirmation', compact('package', 'investors_count', 'action', 'route'));
    }

    public function investors(CryptoExchangePackage $package)
    {
        $investors = \DB::table('crypto_exchange_funds')
            ->select('user_id', 'username', 'firstname', 'lastname', 'posted_profits_count AS profits_count',
                'amount', 'start_date', 'end_date', 'crypto_exchange_funds.status',
                'crypto_exchange_funds.exchange_status', 'crypto_exchange_funds.created_at as purchase_date')
            ->join('users', 'users.id', '=', 'crypto_exchange_funds.user_id')
            ->orderBy('purchase_date', 'DESC')
            ->where('package_id', $package->id)
            ->paginate(10)
            ->setPath($package->id . '/investors');

        if (request()->ajax()) {
            return view('admin.packages.crypto-exchange.partials.investors-table', compact('package', 'investors'));
        }
    }

    public function profits(CryptoExchangePackage $package)
    {
        $profits = \DB::table('crypto_exchange_profits')
            ->where('package_id', $package->id)
            ->orderBy('created_at', 'desc')
            ->paginate(10)
            ->setPath($package->id . '/profits');

        if (request()->ajax()) {
            return view('admin.packages.crypto-exchange.partials.profits-table', compact('package', 'profits'));
        }
    }

    /**
     * Filter package list
     */
    public function filter()
    {
        $packages = CryptoMiningPackage::query()
            ->if (request()->get('name'), 'name', 'LIKE', '%'.request()->get('name').'%')
            ->if (request()->get('status'), 'status', '=', request()->get('status'))
            ->if (request()->get('risk_profile'), 'risk_profile', '=', request()->get('risk_profile'))
            ->latest()
            ->paginate(4)
            ->appends(request()->except(['page','_token']));

        if (request()->ajax()) {
            return view('admin.packages.crypto-mining.partials.package-list', compact('packages'))->render();
        }
    }

    /**
     * Get route base on action and package id given.
     * @param  string $action Form action
     * @param  integer $id     Package id
     * @return string        Generated route action
     */
    protected function getActionRoute($action, $id)
    {
        switch ($action) {
            case 'update':
                $route = route('crypto-exchange-packages.update', $id);
                break;

            case 'delete':
                $route = route('crypto-exchange-packages.destroy', $id);
                break;

            case 'close':
                $route = route('crypto-exchange-packages.close', $id);
                break;

            default:
                $route = '#';
                break;
        }

        return $route;
    }

}
