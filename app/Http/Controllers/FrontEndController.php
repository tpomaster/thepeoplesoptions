<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DOMDocument;

use App\Helpers\LpjHelpers;

class FrontEndController extends Controller
{
    public function index()
    {
    	$url = "https://bitcoinist.com/feed/";

    	$getContent = file_get_contents($url);
		$getContent = str_replace(":encoded","",$getContent);
        $getContent = str_replace("http://","https://",$getContent);

    	$xml = simplexml_load_string($getContent);

    	$mainNews = array();
    	$subNews = array();
    	$miniNews = array();

    	$counter = 1;

    	foreach ($xml->channel->item as $item) {
    		$content = $item->content;

    		$itemTitle = (array)$item->title;
    		$itemLink = (array)$item->link;
    		$itemContent = (array)$item->content;
    		$itemImage = $this->getNewsImage($content);

    		$itemImage2 = LpjHelpers::asset("images/main-news.png");
    		if (isset($itemImage[0])) {
                $itemImage2 = str_replace("http://","https://",$itemImage[0]);
    		}


    		if ($counter == 1) {
    			$mainNews = array(
    				'title' 		=> $itemTitle[0],
    				'content' 		=> $itemContent[0],
    				'link' 			=> $itemLink[0],
    				'getNewsImage' 	=> $itemImage2
    			);
    		} elseif ($counter > 1 && $counter < 7) {
    			$subNews[] = array(
    				'title' 		=> $itemTitle[0],
    				'content' 		=> $itemContent[0],
    				'link' 			=> $itemLink[0],
    				'getNewsImage' 	=> $itemImage2
    			);
    		} elseif ( $counter >=7 && $counter < 11 ) {
    			$miniNews[] = array(
    				'title' 		=> $itemTitle[0],
    				'content' 		=> $itemContent[0],
    				'link' 			=> $itemLink[0],
    				'getNewsImage' 	=> $itemImage2
    			);
    		} else {
    			break;
    		}
		   	
		   	$counter++;
		}

		$ret = array(
			'mainNews' => $mainNews,
			'subNews' => $subNews,
			'miniNews' => $miniNews
		);


        return view('welcome', $ret);
    }

    public function getNewsImage( $content ) {
    	try {
            $description_dom = new DOMDocument();

            $content = str_replace('id="5cea"',"",$content);

            $description_dom->loadHTML( (string)$content );

            // Switch back to SimpleXML for readability
            $description_sxml = simplexml_import_dom( $description_dom );
            $imgs = $description_sxml->xpath('//img');

            $documentImage = array();

            foreach($imgs as $image) {
                $documentImage[] = (string)$image['src'];
            }

            return $documentImage;
        }catch(\ErrorException $e) {
            return false;
        }
	    
    }
}
