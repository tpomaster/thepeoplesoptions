<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\ChartsData;

use App\Helpers\LpjHelpers;

class TradeGraphsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:member');
    }


    public function getTradingPackageChartData ( Request $request ) {
        $ret = array();

        $package_id = LpjHelpers::fss($_GET['pkgchartdata']);

        $packageChartData = ChartsData::getTradingPackageGraphData( $package_id );

        $ret['packageChartData'] = $packageChartData;

        return view('member/packages/crypto-trading/detail-parts/ct-chart-data', $ret);
    }

    public function getMiningPackageChartData ( Request $request ) {
        $ret = array();

        $package_id = LpjHelpers::fss($_GET['pkgchartdata']);

        $packageChartData = ChartsData::getMiningPackageGraphData( $package_id );

        $ret['packageChartData'] = $packageChartData;

        return view('member/packages/crypto-mining/detail-parts/cm-chart-data', $ret);
    }

    public function getExchangePackageChartData ( Request $request ) {
        $ret = array();

        $package_id = LpjHelpers::fss($_GET['pkgchartdata']);

        $packageChartData = ChartsData::getExchangePackageGraphData( $package_id );

        $ret['packageChartData'] = $packageChartData;

        return view('member/packages/crypto-exchange/detail-parts/ce-chart-data', $ret);
    }
}
