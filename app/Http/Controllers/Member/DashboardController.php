<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;

use App\Http\Controllers\Controller;

use App\Model\Member\Dashboard;

use App\Helpers\LpjHelpers;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ret = array();

        $userId = Auth::id();
        $user = Auth::user();

        $curDate = date('Y-m-d H:i:s');

        $teamCount = Dashboard::getTeamCount( $userId );
        $userBank = Dashboard::getUserBank( $userId );
        $profitsReminder = Dashboard::getReminders( $userId );
        $pieData = Dashboard::getDashboardPie( $userId );
        $annPopups = Dashboard::getAnnouncementsAds( $userId );
        

        $ret['teamCount'] = $teamCount;
        $ret['availBal'] = $userBank->available_bal;
        $ret['payout'] = $userBank->payout;
        $ret['totalEarnings'] = $userBank->referral_credit + $userBank->package_earning;
        $ret['tradingProfit'] = $profitsReminder['tradingProfit'];
        $ret['miningProfit'] = $profitsReminder['miningProfit'];
        $ret['exchangeProfit'] = $profitsReminder['exchangeProfit'];
        $ret['RefCreditsProfit'] = $profitsReminder['RefCreditsProfit'];
        $ret['powerBonus'] = $profitsReminder['powerBonus'];
        $ret['miningFunds'] = $pieData['miningFunds'];
        $ret['tradingFunds'] = $pieData['tradingFunds'];
        $ret['exchangeFunds'] = $pieData['exchangeFunds'];
        $ret['shuffleFunds'] = $pieData['shuffleFunds'];

        $ret['latestPopups'] = $annPopups['latestPopups'];
        $ret['countPopups'] = $annPopups['countPopups'];

        Dashboard::updateLastLogin( $userId, $curDate ); 

        $lifespan = time() + (86400 * 30);

        # php live coockies
        // setcookie('phplive_vname_a', $user->username, $lifespan, "/");
        // setcookie('phplive_vemail_a', $user->email, $lifespan, "/");

        Cookie::queue(Cookie::make('phplive_vname', $user->username, 86400, "/phplive"));
        Cookie::queue(Cookie::make('phplive_vemail', $user->email, 86400, "/phplive"));

        Cookie::queue('phplive_vname', $user->username, 86400);
        Cookie::queue('phplive_vemail', $user->email, 86400);


        $ret['new_start_date'] = '';
        $ret['runTime'] = '0';

        if (Auth::check()) {
            return view('member/dashboard', $ret);

        }else{
            return redirect('member/login');
        }
    }

    public function getChartData ( Request $request ) {
        $ret = array();
        $userId = Auth::id();
        $chartData = Dashboard::getGraphData( $userId );

        $ret['chartData'] = $chartData;

        return view('member/dashboard/charts/data', $ret);
    }

    public function loopDashboardGraphData ( Request $request ) {
        $ret = array();
        $userId = Auth::id();
        $curDate = date('Y-m-d');

        $dataChecker = Dashboard::checkDailyEarnings( $userId );

        $runTime = 0;

        $ret['runTime'] = $runTime;
        
        $ret['dataChecker'] = $dataChecker;
        // $ret['dataChecker'] = $dataChecker;
        if ( $dataChecker == 0 ) {
            $create_date = Dashboard::getUserDateCreated( $userId );
        } else {
            // $create_date = Dashboard::getLastLogin( $userId );
            $create_date = Dashboard::getLatestDailyEarnings( $userId );
        }
        
        $ret['create_date'] = $create_date;

        $new_start_date = '';
        // $start_date = '2018-02-01';

        $input = $request->all();
        $input = LpjHelpers::fss( $input );
        extract( $input );

        $start_date = date('Y-m-d', strtotime($create_date));

        $date1=date_create($curDate);
        $date2=date_create($start_date);
        $diff=date_diff($date1,$date2);

        $dateDiff = $diff->format("%a");

        $ret['diff'] = $dateDiff;

        if ($runTime <= 7) {
            $ret['message'] = 'Computing Related Resources...';
        } elseif($runTime >= 8 && $runTime <= 14) {
            $ret['message'] = 'Loading Charts Data...';
        } else {
            $ret['message'] = 'Initializing...';
        }
        
        if ( !empty($new_start_date) ) {
            $start_date = date('Y-m-d', strtotime($new_start_date));
        }

        $ret['start_date'] = $start_date;

        
        $current_date = date('Y-m-d', strtotime("-1 days"));

        $end_date = date ("Y-m-d", strtotime("+30 days", strtotime($start_date)));

        $ret['end_date'] = $end_date;

        while( strtotime($start_date) <= strtotime($end_date) ) {

            if ( strtotime($start_date) <= strtotime($current_date) ) {
                $dailyEarnings = Dashboard::insertDailyEarnings( $userId, $start_date );
            }

            $start_date = date ("Y-m-d", strtotime("+1 days", strtotime($start_date)));
        }

        $new_start_date = date ("Y-m-d", strtotime("+1 days", strtotime($end_date)));

        $runTime++;


        if ( strtotime($end_date) >= strtotime($current_date) ) {
            $ret['msg'] = 'stop';

        }else{
            $ret['msg'] = 'continue';
        }

        $ret['new_start_date2'] = $new_start_date;
        $ret['new_runTime'] = $runTime;

        return $ret;
    }

    public function getAnnouncements( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'qpage' => $qpage,
            'qlimit' => $qlimit,
        );
        $ret['data'] = Dashboard::getAnnouncements( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $Announcements = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $Announcements->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $Announcements->total() ) {
                $showingCount = $Announcements->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$Announcements->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        $ret2 = array(
            'Announcements' => $ret['data']['list']
        );


        $ret['returnHTML'] = view('member.dashboard.announcements-item')->with($ret2)->render();
            

        return response()->json($ret);
    }

    public function getNews( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'qpage' => $qpage,
            'qlimit' => $qlimit,
        );
        $ret['data'] = Dashboard::getNews( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $News = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $News->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $News->total() ) {
                $showingCount = $News->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$News->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        $ret2 = array(
            'News' => $ret['data']['list']
        );


        $ret['returnHTML'] = view('member.dashboard.news-item')->with($ret2)->render();
            

        return response()->json($ret);
    }

    public function getFaqs( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'qpage' => $qpage,
            'qlimit' => $qlimit,
        );
        $ret['data'] = Dashboard::getFaqs( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $Faqs = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $Faqs->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $Faqs->total() ) {
                $showingCount = $Faqs->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$Faqs->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        $ret2 = array(
            'Faqs' => $ret['data']['list']
        );


        $ret['returnHTML'] = view('member.dashboard.faq-item')->with($ret2)->render();
            

        return response()->json($ret);
    }

    public function logout(){
        Auth::logout();

        return redirect('member/login');
    }
}
