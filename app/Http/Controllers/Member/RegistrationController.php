<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Cookie;

use App\Http\Controllers\Controller;
use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\ReferralCredit;

use App\Helpers\LpjHelpers;
use App\Helpers\TpoMarketingAPI;
use App\Helpers\Sendpulse\ApiClient;
use App\Helpers\Sendpulse\Storage\FileStorage;

class RegistrationController extends Controller
{

    public function __construct() 
    {
        $this->middleware('guest:member');
    }

    public function showRegisterForm( $avatar )
    {
        $ret = array(
            'isError' => false,
            'msg' => array()
        );

        # check if avatar 
        $sponsor = new Member;
        //$sponsorInfo = $sponsor->getSingleByUsername( $avatar );
        $sponsorInfo = $sponsor->getSingle('username', $avatar);

        if ( !$sponsorInfo ) {
            $ret['msg'][] = 'Invalid sponsor link.';
            $ret['isError'] = true;
        }

        if ( !$ret['isError'] ) {
            Cookie::queue('aff', $avatar, 9999);
            session(['aff' => $avatar]);
        }


        $username = LpjHelpers::generateName();

        $fake_data = array(
            'firstname' => LpjHelpers::generateName(),
            'lastname' => LpjHelpers::generateName(),
            'username' => $username,
            'email' => $username.'@xmail.com',
            'password' => 'Test@123',
        );
        
        return view('member.auth.register', ['errsuc' => $ret, 'fake_data' => $fake_data]);
    }

    public function registerAjax( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        # get sponsor cookie
        $sponsorAvatar = Cookie::get('aff');
        //$ret['sponsor'] = $sponsorAvatar;

        if ( !$sponsorAvatar ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Invalid sponsor link.';
        }

        # get sponsor info
        $sponsor = false;
        if ( !$ret['isError'] ) {
            $sponsor = new Member;
            $sponsorInfo = $sponsor->getSingle('username', $sponsorAvatar);
            //$ret['sponsorInfo'] = $sponsorInfo;

            if ( !$sponsorInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Invalid sponsor link.';
            }
        }

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        # validate required fields
        $validate = Validator::make($request->all(), [
            'username' => 'required|max:255|unique:users',
            'password' => 'required|max:255',
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|max:255|unique:users',
            'password' => 'required|max:255',
        ]);

        $ret['validate'] = $validate;
        if ($validate->fails()) {
            
            $errors = $validate->errors();
            
            if ( $errors ) {
                foreach ($errors->get('*') as $message) {
                    $ret['msg'][] = $message;
                }

            }else{
                $ret['msg'][] = 'Please fill out all fields and make sure you entered the correct data.';
            }

            $ret['errors'] = $errors;
            $ret['isError'] = true;
        }

        # VALIDATIONS
        if ( !isset($agreetoterms) || $agreetoterms != 'on' ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'You have to agree with the terms and conditions to continue.';
        }

        # password strength
        if ( !$ret['isError'] ) {
            $ret = LpjHelpers::lpjCheckPasswordStrength( $password, $ret );
        }

        # email format
        if ( !$ret['isError'] ) {
            $ret = LpjHelpers::lpjIsEmail( $email, $ret );
        }

        # useranme format
        if ( !$ret['isError'] ) {
            $ret = LpjHelpers::lpjValidateUsername( $username, $ret );
        }


        if ( !$ret['isError'] ) {
            # generate activation code
            $salt = LpjHelpers::lpjGenerateRandString(8);
            $combi = $salt.':'.$email.':'.time();
            $activationKey = Hash::make($combi);

            # add user
            $regData = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'username' => $username,
                'password' => Hash::make($password),
                'parent_id' => $sponsorInfo->id,
                'membership' => 'free',
                'status' => 1, # status: 0 = activated, 1 = email not verified, 2 = deactivated
                'activation_code' => $activationKey,
                'remember_token' => '',
                'created_at' => date('Y-m-d H:i:s')
            );
            $newUserId = Member::addNew( $regData );
            //$ret['regData'] = $regData;

            # add user meta
            $userM = new UserMeta( $newUserId );
            $params = array();
            $userMeta = $userM->addNew( $params );
            

            # update sponsor's direct referral count
            $sponsor->updateReferralCount();


            # send account verification email
            $verificationData = array(
                'name' => $firstname.' '.$lastname,
                'verificationUrl' => url('/').'/member/login/?verify='.$activationKey,
                'loginUrl' => url('/').'/member/login',
            );
            $ret['emailActivation'] = $this->sendActivationCodeEmail( $verificationData, $email );

            # send welcome email 
            $welcomeData = array(
                'username' => $username,
                'plaintextPass' => $password,
                'loginUrl' => url('/').'/member/login',
            );
            $ret['emailWelcome'] = $this->sendWelcomeEmail( $welcomeData, $email );


            # create tpo bank and referral credit totals
            if ( $newUserId ) {
                $userBank = UserBank::createUserBank2( $newUserId );
                ReferralCredit::createRefCredTotals( $newUserId );

                # FOR BETA TESTING ONLY
                if ( config('app.is_sandbox') == true ) {
                    $testFund = array(
                        'user_id' => $newUserId,
                        'transaction_code' => LpjHelpers::lpjGenerateRandString(12),
                        'amount' => 300000,
                        'description' => 'Test Fund',
                        'type' => 'in',
                        'section' => 'deposit',
                        'transaction_date' => date('Y-m-d H:i:s'),
                        'status' => 'completed',
                        'details' => ''
                    );
                    UserBank::insertBankTransaction2( $testFund );
                    UserBank::reComputeBankTotals( array( 'userId' => $newUserId ) );
                }
            }


            # tpo marketing - update contact activity
            $registerRet = TpoMarketingAPI::contactRegistered( $sponsorAvatar, $email );
            $ret['registerRet'] = $registerRet;

            $ret['msg'][] = 'Registration Successfull.
                Please check your email inbox. We sent you an account activation link. Thank You.';
        }

        return response()->json($ret);
    }

    private function sendWelcomeEmail( $welcomeData, $toEmail ){
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $messageHtml = view('member.emails.member-welcome')->with($welcomeData)->render();

        $messageText = "Hi ".$welcomeData['username'].", \n\n";
        $messageText .= "Thank you for your registration. \n";
        $messageText .= "Here is you Login Info: \n";
        $messageText .= "username: ".$welcomeData['username']."\n";
        $messageText .= "password: ".$welcomeData['plaintextPass']."\n";
        $messageText .= "login link: ".$welcomeData['loginUrl']."\n\n";
        $messageText .= "Thank you \n";
        $messageText .= "The People's Options Team";

        $subject = "The People's Options | Your Login Info";

        // if ( config('app.is_sandbox') == true ) {
        //     $toEmail = 'ceco@duck2.club';
        // }

        $email = array(
            'html' => $messageHtml,
            'text' => $messageText,
            'subject' => $subject,
            'from' => array(
                'name' => 'TPO Team',
                'email' => 'info@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => $welcomeData['username'],
                    'email' => $toEmail,
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'LitoPJ',
                    'email' => 'litopj@yahoo.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $emailSend = $SPApiClient->smtpSendMail($email);

        if ( !$emailSend->result ) {
            Mail::send('member.emails.member-welcome', $welcomeData, function ($message) {
                $message->from('info@thepeoplesoptions.com', 'TPO Team');
                $message->to($toEmail);
            });
        }

        return $emailSend->result;
    }

    private function sendActivationCodeEmail( $verificationData, $toEmail ){
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $messageHtml = view('member.emails.activation-code')->with($verificationData)->render();

        $messageText = "Hi ".$verificationData['name'].", \n\n";
        $messageText .= "Thank you for registering.  Please click the link below to verify your email address. \n";
        $messageText .= "Account activation link: ".$verificationData['verificationUrl']."\n";
        $messageText .= "Once you have completed this step you can go back to our site, ".$verificationData['loginUrl']." to login and complete your profile.\n\n";
        $messageText .= "Thank you \n";
        $messageText .= "The People's Options Team";

        $subject = "The People's Options | Account Activation Link";

        // if ( config('app.is_sandbox') == true ) {
        //     $toEmail = 'ceco@duck2.club';
        // }

        $email = array(
            'html' => $messageHtml,
            'text' => $messageText,
            'subject' => $subject,
            'from' => array(
                'name' => 'TPO Team',
                'email' => 'info@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => $verificationData['name'],
                    'email' => $toEmail,
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'LitoPJ',
                    'email' => 'litopj@yahoo.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $emailSend = $SPApiClient->smtpSendMail($email);

        if ( !$emailSend->result ) {
            Mail::send('member.emails.activation-code', $verificationData, function ($message) {
                $message->from('info@thepeoplesoptions.com', 'TPO Team');
                $message->to($toEmail);
            });
        }

        return $emailSend->result;
    }


    
}