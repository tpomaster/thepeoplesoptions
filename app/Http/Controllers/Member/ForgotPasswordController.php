<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;
use App\Model\Member\Member;
// use App\MyClasses\CustomClass;
use App\Helpers\LpjHelpers;

use App\Helpers\Sendpulse\ApiClient;
use App\Helpers\Sendpulse\Storage\FileStorage;

class ForgotPasswordController extends Controller
{

    public function __construct() 
    {
        //$this->middleware('guest:member');
    }

    public function showForgotPasswordForm( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'isValidResetKey' => 0
        );

        $e = '';
        $key = '';

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        if ( isset($request->action) && $request->action == 'rp' ) {
            $validateResult = $this->validateResetKey( $key, $e);

            if ( $validateResult['isError'] == false ) {
                $ret['isValidResetKey'] = 1;
                $ret['keyId'] = $validateResult['keyId'];
                $ret['token'] = $validateResult['token'];

            }else{
                $ret = $validateResult;
            }
            //dd( $ret );

            return view('member/set-new-password', $ret);

        }else{
            return view('member/forgot-password');
        }
    }

    public function sendPasswordResetLink ( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $email = '';

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        // $included_files = get_included_files();
        // $ret['included_files'] = $included_files;

        # VALIDATIONS
        # ----------------------------
        # validate required fields
        $validate = Validator::make($request->all(), [
            'email' => 'required|max:255',
        ]);
        //$ret['validate'] = $validate;
        
        if ($validate->fails()) {
            $errors = $validate->errors();
            
            if ( $errors ) {
                foreach ($errors->get('*') as $message) {
                    $ret['msg'][] = $message;
                }

            }else{
                $ret['msg'][] = 'Please fill out all fields and make sure you entered the correct data.';
            }

            //$ret['errors'] = $errors;
            $ret['isError'] = true;
        }

        # email format
        if ( !$ret['isError'] ) {
            $ret = LpjHelpers::lpjIsEmail( $email, $ret );
        }

        # check if email exist
        if ( !$ret['isError'] ) {
            $memberInfo = Member::getSingle2('email', $email);
            if ( !$memberInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Email not found.';
            }
        }

        # get seset key
        if ( !$ret['isError'] ) {
            $resetKey = Member::getPasswordResetKey('email', $email);

            if ( !$resetKey ) {
                $resetKey = LpjHelpers::lpjGenerateRandString(32).time().LpjHelpers::lpjGenerateRandString(32);
                Member::setPasswordResetKey( $resetKey, $email );
            }
        }

        # Process
        # ----------------------------
        if ( !$ret['isError'] ) {
            $emailData = array(
                'username' => $memberInfo->username,
                'siteurl' => url('/').'/member',
                'resetUrl' => url('/')."/member/forgot-password/?action=rp&key=$resetKey&e=" . rawurlencode($email),
            );
            //$ret['emailData'] = $emailData;
            $this->sendPasswordResetEmail( $emailData, $email );

            $ret['msg'][] = 'Please check your email, we sent you a password reset link.';
        }

        return response()->json($ret);
    }

    public function setNewPassword( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $password1 = '';
        $password2 = '';

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );


        # check empty
        if( empty($password1) ){
            $ret['msg'][] = 'Please enter your new PASSWORD.';
            $ret['isError'] = true;
        }

        # check if both matches
        if( $ret['isError'] == false ){
            if( $password1 != $password2 ){
                $ret['isError'] = true;
                $ret['msg'][] = 'Password did not match!';
            }
        }

        # check strength
        if( $ret['isError'] == false ){
            $ret = LpjHelpers::lpjCheckPasswordStrength( $password1 );
        }

        # check email / id
        if( $ret['isError'] == false ){
            if( empty($email) ){
                $ret['msg'][] = 'Please enter your EMAIL address.';
                $ret['isError'] = true;
            }
        }

        # validate email and reset key and password
        if( $ret['isError'] == false ){
            $keyData = Member::getPasswordResetKeyRow('id', $email);

            if ( !$keyData ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Invalid Email';

            }elseif( $keyData->token != $token ){
                $ret['isError'] = true;
                $ret['msg'][] = 'Invalid password reset key.';
            }
        }

        # generate password and update user password
        $newPassord = '';
        if( $ret['isError'] == false ){
            $newPassord = Hash::make($password1);

            $updUser = array(
                'password' => $newPassord
            );
            $updWhere = array(
                'email' => $keyData->email 
            );
            $updateResult = Member::updateUserWhere( $updUser, $updWhere );

            if ( !$updateResult ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'System failed to update password.';
            }
        }

        # email new password
        if( $ret['isError'] == false ){
            $memberInfo = Member::getSingle2('email', $keyData->email);

            # delete reset password data

            # send email
            $emailData = array(
                'username' => $memberInfo->username,
                'newPass' => $password1,
                'loginUrl' => url('/').'/member/login',
            );
            $ret['emailData'] = $emailData;
            $ret['sendNewPasswordEmail'] = $this->sendNewPasswordEmail( $emailData, $keyData->email );


            // Test@333
            $ret['msg'][] = 'Password successfully updated.';
        }

        return response()->json($ret);
    }

    private function validateResetKey( $token, $email )
    {
        $ret = array(
            'isError' => false,
            'msg' => array()
        );

        $token = preg_replace('/[^a-z0-9]/i', '', $token);
        $email = rawurldecode($email);


        if ( empty($token) || empty($email) ) {
            $ret['isError'] = true;
            $ret['msg'] = 'Missing Parameters';
        }

        # validate reset key
        if ( !$ret['isError'] ) {
            $keyData = Member::getPasswordResetKeyRow( 'email', $email );

            if ( !$keyData ) {
                $ret['isError'] = true;
                $ret['msg'] = 'Invalid Email';

            }elseif( $keyData->token != $token ){
                $ret['isError'] = true;
                $ret['msg'] = 'Invalid password reset key.';
            }
        }

        if ( !$ret['isError'] ) {
            $ret['keyId'] = $keyData->id;
            $ret['token'] = $keyData->token;
            $ret['msg'] = 'Valid reset key.';
        }

        return $ret;
    }

    private function sendPasswordResetEmail( $emailData, $toEmail )
    {
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $messageHtml = view('member.emails.password-reset-link')->with($emailData)->render();

        $messageText = "Password Reset Request. \n\n";
        $messageText .= "Using this email address, someone requested to reset the password for this account with The Peoples Options. \n";
        $messageText .= "Username: ".$emailData['username']."\n";
        $messageText .= "If this was a mistake, just ignore this email and nothing will happen. \n";
        $messageText .= "To reset your password, visit this page: ".$emailData['resetUrl']."\n\n";
        $messageText .= "Thank you \n";
        $messageText .= "TPO Team";

        $subject = "The Peoples Options | Password Reset Request";

        // if ( config('app.is_sandbox') == true ) {
        //     $userEmail = 'ceco@duck2.club';
        // }

        $email = array(
            'html' => $messageHtml,
            'text' => $messageText,
            'subject' => $subject,
            'from' => array(
                'name' => 'TPO Team',
                'email' => 'info@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => $emailData['username'],
                    'email' => $toEmail,
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'LitoPJ',
                    'email' => 'litopj@yahoo.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $emailSend = $SPApiClient->smtpSendMail($email);

        if ( !$emailSend->result ) {
            Mail::send('member.emails.password-reset-link', $emailData, function ($message) {
                $message->from('info@thepeoplesoptions.com', 'TPO Team');
                $message->to($toEmail);
            });
        }

        return $emailSend->result;
    }

    private function sendNewPasswordEmail( $emailData, $toEmail )
    {
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $messageHtml = view('member.emails.password-reset-new-password')->with($emailData)->render();

        $messageText = "Password Reset Request. \n\n";
        $messageText .= "Our records indicate you have reset your password.  Below is your login information.  Please keep this in file for future use. \n";
        $messageText .= "Username: ".$emailData['username']."\n";
        $messageText .= "Password: ".$emailData['newPass']."\n";
        $messageText .= "Login Link: ".$emailData['loginUrl']."\n\n";
        $messageText .= "Thank you \n";
        $messageText .= "TPO Team";

        $subject = "The Peoples Options | Password Reset Request";

        // if ( config('app.is_sandbox') == true ) {
        //     $userEmail = 'ceco@duck2.club';
        // }

        $email = array(
            'html' => $messageHtml,
            'text' => $messageText,
            'subject' => $subject,
            'from' => array(
                'name' => 'TPO Team',
                'email' => 'info@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => $emailData['username'],
                    'email' => $toEmail,
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'LitoPJ',
                    'email' => 'litopj@yahoo.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $emailSend = $SPApiClient->smtpSendMail($email);

        return $emailSend;

        // if ( !$emailSend || !$emailSend->result ) {
        //     $ret = Mail::send('member.emails.password-reset-new-password', $emailData, function ($message) {
        //         $message->from('info@thepeoplesoptions.com', 'TPO Team');
        //         $message->to($toEmail);
        //     });

        //     return $ret;

        // }else{
        //     return $emailSend->result;
        // }
    }
}