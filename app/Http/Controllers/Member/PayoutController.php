<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\Payout;

use App\Helpers\LpjHelpers;

class PayoutController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function processPayoutRequest(  Request $request  ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # default
        $amount = 0;
        $btsWallet = '';
        $currDate = date('Y-m-d H:i:s');
        $currency = 'BitPalbts';
        $asset = 'BTCCORE';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();
        $membershipLevel = $user->membership;
        //$membershipLevel = 'gold';

        # get input
        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );


        $btsWallet = $user->bitshares_name;

        # validate amount
        if ( !is_numeric( $amount ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Invalid Amount. ';
        }

        # validate minimum amount
        if ( $ret['isError'] == false ) {
            if ( $amount < 10 ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Minimum amount is 10. ';
            }
        }

        # validate bitshares account
        if ( $ret['isError'] == false ) {
            if ( empty($btsWallet) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Please set your bitshares account in the My Account page.';
            }
        }

        # validate bank totals
        if ( $ret['isError'] == false ) {
            $userBank = UserBank::getUserBank2( $userId );
            if ( $userBank->available_bal < $amount ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Insuficient bank balance ( your bank balance is $'.$userBank->available_bal.').';
            }
        }

        # monthly/weekly/biweekly payout validation
        if ( $ret['isError'] == false ) {
            $res = $this->validateMonthlyPayoutCount( $userId, $membershipLevel, $currDate );

            if ( $res['isError'] ) {
                $ret['bPayoutCount'] = $res;
                $ret['isError'] = true;
                $ret['msg'][] = $res['msg'];
            }
        }

        # daily payout validation
        if ( $ret['isError'] == false ) {
            $count = Payout::countDailyPayout( $userId, $currDate );

            if ( $count >= 1 && $membershipLevel != 'platinum' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'You`ve reached your maximum withdrawal count per day.';
            }
        }

        # max withdraw amount per month
        if ( $ret['isError'] == false ) {
            $monthlyMaxAmount = Payout::getMaxWithdrawAmountByMembership( $membershipLevel );

            if ( $amount > $monthlyMaxAmount ) {
                $ret['isError'] = true;
                $ret['msg'][] = strtoupper($membershipLevel).' members can only withdraw maximum of $'.$monthlyMaxAmount.' per month.';
            }
        }

        if ( $ret['isError'] == false ) {
            $transactionCode = LpjHelpers::generateTrCode();


            $wd = LpjHelpers::getWeekDateRange( $currDate );
            $releaseDate = date( 'Y-m-d H:i:s', strtotime("+3 day", strtotime( $wd['dateTo'] ) ) );

            # compute withdrawal fee
            $withdrawalFeePercent = Payout::getWithdrawalFeeByMembership( $membershipLevel );
            $withdrawalFeeAmount = $amount * $withdrawalFeePercent;

            $netAmount = $amount - $withdrawalFeeAmount;

            $details = array(
                'membership_level' => $membershipLevel,
                'withdrawal_fee_percent' => $withdrawalFeeAmount,
                'monthly_limit_amount' => $monthlyMaxAmount,
                'tpo_username' => $user->username,
                'crpto_address' => $btsWallet,
                'bts_wallet' => $btsWallet
            );

            # insert payout request
            $params = array(
                'transactionCode' => $transactionCode,
                'userId' => $userId,
                'amount' => $amount,
                'withdrawalFee' => $withdrawalFeeAmount,
                'netAmount' => $netAmount,
                'currency' => $currency,
                'request_date' => $currDate,
                'releaseDate' => $releaseDate,
                'status' => 'pending',
                'details' => $details,
            );
            $res2 = Payout::insert($params);
            $ret['insertPayoutRes'] = $res2;

            # insert bank transaction
            if ( $res2 ) {
                $params2 = array(
                    'user_id' => $userId,
                    'transaction_code' => $transactionCode,
                    'amount' => $amount,
                    'description' => 'Transfer TPO Bank balance to '.$currency.': ( '.$btsWallet.' )',
                    'section' => 'withdrawal',
                    'type' => 'out',
                    'transaction_date' => $currDate,
                    'status' => 'pending'
                );
                UserBank::insertBankTransaction2( $params2 );

                $ret['msg'][] = 'Your request is successfully submitted.';

            }else{
                $ret['isError'] = true;
                $ret['msg'][] = 'Failed to insert payout request.';
            }
        }



        return response()->json($ret);
    }


    public function validateMonthlyPayoutCount( $userId, $membershipLevel, $reqDate ){
        $ret = array(
            'isError' => false,
            'msg' => '',
        );

        $maxCount = 1;

        if ( $reqDate == '' ) {
            $reqDate = date('Y-m-d');
        }

        if ( $membershipLevel == '' ) {
            $membershipLevel = 'free';
        }

        if ( $membershipLevel == 'free' || $membershipLevel == 'bronze' ) {
            $mPayoutCount = Payout::countMonthlyPayout( $userId, $reqDate );

            if ( $mPayoutCount >= $maxCount ) {
                $ret['isError'] = true;
                $ret['msg'] = 'FREE members can only withdraw once a month. ';
            }
        }

        if ( $membershipLevel == 'silver' ) {
            $bPayoutCount = Payout::countBiweeklyPayout( $userId, $reqDate );

            if ( $bPayoutCount >= $maxCount ) {
                $ret['bPayoutCount'] = $bPayoutCount;
                $ret['isError'] = true;
                $ret['msg'] = 'SILVER members can only withdraw once every 2 weeks. ';
            }
        }

        if ( $membershipLevel == 'gold' ) {
            $wPayoutCount = Payout::countWeeklyPayout( $userId, $reqDate );

            if ( $wPayoutCount >= $maxCount ) {
                $ret['isError'] = true;
                $ret['msg'] = 'GOLD members can only withdraw once a week. ';
            }
        }

        return $ret;
    }


    public function cancelPayout( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $transactionCode = '';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $transactionCode ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        if ( $ret['isError'] == false ) {
            $ret['transactionCode'] = $transactionCode;
            $ret['msg'][] = 'Your withdrawal request is successfully cancelled.';

            $updParams = array(
                'status' => 'cancelled'
            );
            Payout::updateField($transactionCode, $updParams);
            UserBank::updateTransactionStatusStatic($transactionCode, 'cancelled');
        }

        return response()->json($ret);
    }
}
