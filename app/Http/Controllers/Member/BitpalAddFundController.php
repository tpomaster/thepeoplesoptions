<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\UserbankFund;
use App\Model\Member\UserBtsAccounts;

use App\Helpers\LpjHelpers;
use App\Helpers\BitPalAPI;

class BitpalAddFundController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }


    public function addFund( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # init vars
        $amount = 0;
        $amount2 = 0;
        $btsAccount = '';
        $btsAsset = 'bts';
        $accessToken = false;

        $minAmount = 10;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        $btsAccount = $user->bitshares_name;



        if ( $btsAccount == 'litopj2017' ) {
            $minAmount = 1;
        }
        

        # validate minimum amount
        if ( $ret['isError'] == false ) {
            if ( $amount < $minAmount ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Mininmum amount is $'.$minAmount;
            }
        }

        # validate wallet
        if ( $ret['isError'] == false ) {
            if ( $btsAccount == '' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Please select your default bitshares account in MY ACCOUNT page';
            }
        }


        # get access token
        if ( $ret['isError'] == false ) {
            $fields = array( 'bitpal_access_token', 'bitpal_access_token_time', 'bitpal_bts_accounts' );
            $bitPalInfo = UserMeta::getUserMeta( $userId, $fields );

            if ( empty($bitPalInfo->bitpal_access_token) || empty($bitPalInfo->bitpal_access_token_time) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'BitPalBTS Access Token is missing';
                $ret['send_status'] = 401;
            }
        }

        # check if access token has expired
        if ( $ret['isError'] == false ) {
            $accessToken = $bitPalInfo->bitpal_access_token;

            $secondsDiff = time() - (int)$bitPalInfo->bitpal_access_token_time;
            $timeDiff = $secondsDiff/3600;

            // if ( $timeDiff > 2 ) {
            //     $ret['isError'] = true;
            //     $ret['msg'][] = 'BitPalBTS Access Token has expired';
            //     $ret['send_status'] = 401;
            // }
        }

        # get exchange rate
        if ( $ret['isError'] == false ) {
            $exchangeRate = 1; # default exchange rate
            $exchangeRateFromApi = BitPalAPI::getExchangeRate( 'USD', 'BTS'); # get usd amount per 1[asset] value (ex: 0.194 USD per 1 BTS)

            if ( $exchangeRateFromApi ) {
                $exchangeRate = $exchangeRateFromApi;

            }else{
                $ret['isError'] = true;
                $ret['msg'][] = 'Failed to pull exchange rate';
                $ret['send_status'] = 401;
            }
        }

        #
        if ( $ret['isError'] == false ) {
            $usdAmountPerAsset = (float) $exchangeRate; # ex: .194 USD per 1 BTS
            $usdAmount = (float) $amount;

            $assetValuePerUSD = 1/$usdAmountPerAsset;

            $price = $assetValuePerUSD * $usdAmount;
            $assetAmount = $price;
            $ret['assetAmount'] = $assetAmount;
            $ret['usdAmount'] = $usdAmount;


            if ( $btsAsset == 'bitusd' ) {
                $btsAsset2 = 'USD';
                $price = $usdAmount;
                $ret['computed'] = $price;

            }else{
                $btsAsset2 = 'BTS';
            }


            $details = array(
                'usd_val_per_asset' => $usdAmountPerAsset,
                'asset_val_per_usd' => $assetValuePerUSD,
                'usd' => $usdAmount,
                'asset' => $assetAmount,
                'btsuserid' => $btsAccount,
                'bts_account' => $btsAccount,
            );

            # insert bank fund request
            $transactionCode = LpjHelpers::generateTrCode();

            $addFundParams = array(
                'userId' => $userId,
                'transactionCode' => $transactionCode,
                'usdAmount' => $usdAmount,
                'assetAmount' => $assetAmount,
                'asset' => $btsAsset2,
                'gTrId' => '0',
                'gUserId' => $btsAccount,
                'details' => json_encode($details)
            );
            $addFundRet = UserbankFund::insertAddFundRequestBTS( $addFundParams );
            //$ret['add_fund_req_ret'] = $addFundRet;
            //$ret['addFundParams'] = $addFundParams;


            if ( $addFundRet ) {
                # BITPALBTS SEND
                //$access_token = 'SJO8n10QwSFmwPtBzmPLTIlQpWIrl5';
                $paramHash = 'test';

                $bitpalParams = array(
                    'access_token' => $accessToken,
                    'bts_asset' => $btsAsset2,
                    //'pinned_asset' => 'fiat',
                    'amount' => BitPalAPI::bitpalAmtFormat($price),
                    'to_wallet' => 'tpo-addfunds',
                    'from_wallet' => $btsAccount,
                    'comment' => "TPO Add Funds (".$user->username.")",
                    'callback' => url('/').'/member/oauth-code?state='.$transactionCode,
                );
                //$ret['bitpal_params'] = $bitpalParams;

                $sendRet = BitPalAPI::transfer( $bitpalParams );
                //$ret['send_ret'] = $sendRet;
                //$ret['send_status'] = $sendRet->status;

                # temporarily success message
                //$sendRet->status = 200;

                if ( $sendRet->status == 200 ) {
                    //$ret['msg'][] = 'Add Fund Successfully Processed.';

                    # insert bank transaction
                    # -------------------------------
                    $params = array(
                        'user_id' => $userId,
                        'transaction_code' => $transactionCode,
                        'amount' => $usdAmount,
                        'status' => 'completed',
                        'description' => 'Added fund via BitPalBTS ('.$btsAccount.')',
                    );
                    UserBank::insertBankTransaction2( $params );

                    # update add_fund transaction
                    # --------------------------------
                    $params = array(
                        //'gateway_tr_id' => $sendRet->data->id,
                        'status' => 'completed',
                    );
                    $ret['update_bank_fund_tr'] = UserbankFund::updateBtsAddFundStatus( $transactionCode, $params );

                    $ret['msg'][] = 'Add Fund Successfully Processed.';
                }else{
                    $ret['isError'] = true;
                    $ret['msg'][] = BitPalAPI::parseBitpalMsg( $sendRet );
                }

            }
        }


        return response()->json($ret);
    }


    public function oAuthCode( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => '',
        );

        $userId = Auth::id();


        # redirect if no "code" in query string
        # =====================================================
        if ( !isset($_GET['code']) ) {
            return redirect('member/tpo-bank');
            exit();
        }


        # get paramters
        # =====================================================
        $code = LpjHelpers::fss($_GET['code']);
        $state = isset($_GET['state']) ? LpjHelpers::fss($_GET['state']) : 'addfund';

        $clientId = BitPalAPI::config('client_id');
        $clientSecret = BitPalAPI::config('client_secret');


        # retrieve access token
        # =====================================================
        $callbackUrl = url('/').'/member/oauth-code';
        $authCodeRes = BitPalAPI::getAccessToken( $code, $callbackUrl );

        # check api error
        if ( isset($authCodeRes->error) ) {
            $ret['isError'] = true;
            $ret['msg'] = $authCodeRes->error;

            //dd($authCodeRes);

            if ( $authCodeRes->error == 'invalid_grant') {
                $ret['msg'] = 'Invalid Access Token.';
            }
        }

        # check empty access token
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( empty($authCodeRes->access_token) ) {
                $ret['isError'] = true;
                $ret['msg'] = 'Empty Access Token';

            }else{
                $params = array(
                    'bitpal_access_token' => $authCodeRes->access_token,
                    'bitpal_access_token_time' => time()
                );
                UserMeta::updateUserMeta( $userId, $params );
            }
        }

        # get bitshares wallets from api
        # =====================================================
        $accountsDropdown = array();
        if ( $ret['isError'] == false ) {
            $getBtsAcctsRes = $this->getBitsharesAccounts( $userId, $authCodeRes->access_token );

            if ( $getBtsAcctsRes['isError'] == true ) {
                $ret['isError'] = true;
                $ret['msg'] = $getBtsAcctsRes['msg'];

                //dd($getBtsAcctsRes);

            }else{
                $accountsDropdown = $getBtsAcctsRes['btsAccountsDropdown'];
            }
        }

        # save/update bitshares wallet
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( !empty($accountsDropdown) ) {
                $params = array(
                    'bitpal_bts_accounts' =>json_encode($accountsDropdown)
                );
                UserMeta::updateUserMeta( $userId, $params );

            }else{
                $ret['isError'] = true;
                $ret['msg'] = "The Bitshares wallet you are trying to connect to your TPO account is aleady connected to another TPO account.<br><br> Connecting one BitPalBTS account to more than one TPO accounts is NOT allowed.";
            }
        }

        if ( $ret['isError'] == false ) {
            $ret['msg'] = 'Authentication successful.';
        }

        # redirect to tpo bank
        # =====================================================
        if ( $ret['isError'] == false ) {
            if ( $state == 'btsaccount') {
                $bankUrl = url('/').'/member/my-account?tab=bitshares';
                $ret['btnUrl'] = $bankUrl;
                return redirect( $bankUrl );
                exit();

            }else{
                $bankUrl = url('/').'/member/tpo-bank/?tab=addfundform';
                $ret['btnUrl'] = $bankUrl;
                return redirect( $bankUrl );
                exit();
            }
        }

        //dd( $ret );

        return view('member.oauth-code', $ret);
    }

    public function getBitsharesAccounts( $userId, $accessToken ){
        $btsAccountsDropdown = array();
        $btsAccountsAssets = array();

        $ret = array(
            'isError' => false,
            'msg' => '',
        );

        $btsAccounts = BitPalAPI::getBitsharesAccountsFromApi( $accessToken );
        //dd($btsAccounts);

        $apiErrorMsg = BitPalAPI::parseBitpalMsg($btsAccounts);

        if ( !empty($apiErrorMsg) ) {
            $ret['isError'] = true;
            $ret['msg'] = $apiErrorMsg;
            $ret['apiFullRet'] = $btsAccounts;
        }

        if ( $ret['isError'] == false ) {
            if ( isset($btsAccounts->data) ) {
                foreach( $btsAccounts->data as $k => $v ) {
                    $savedBtsAccount = UserBtsAccounts::saveBtsAccount( $userId, $v->username );

                    if ( $savedBtsAccount ) {
                        $btsAccountsDropdown[] = $savedBtsAccount;
                    }

                    # get asset balances
                    if ( !empty($v->balances) ) {
                        foreach ($v->balances as $kk => $vv) {
                            $btsAccountsAssets[$v->username][$vv->symbol] = $vv->amount;
                        }
                    }

                    # update default bts account
                    if ( $v->is_default == 1 ) {
                        $userMeta = array(
                            'bitpal_default_bts_account' => $v->username
                        );
                        UserMeta::updateUserMeta( $userId, $userMeta );
                    }
                }
            }

            $ret['btsAccountsDropdown'] = $btsAccountsDropdown;
        }

        return $ret;
    }

    public function getExchangeRate(Request $request){
        $rate = BitPalAPI::getExchangeRate( 'USD', 'BTS');
        //echo $rate;

        $ret = array(
            'usd_to_bts_rate' => $rate,
        );

        return response()->json($ret);
    }
}

