<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserBank;
use App\Model\Member\ReferralCredit;

use App\Helpers\LpjHelpers;

class ReferralCreditController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function index(){
        $ret = array();

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $ret['CTrefCred'] = ReferralCredit::getRefCredSumByCategory( 'withdrawn', 'cryptotrading', $userId );
        $ret['MSrefCred'] = ReferralCredit::getRefCredSumByCategory( 'withdrawn', 'membership', $userId );
        $ret['CMrefCred'] = ReferralCredit::getRefCredSumByCategory( 'withdrawn', 'cryptomining', $userId );
        $ret['CErefCred'] = ReferralCredit::getRefCredSumByCategory( 'withdrawn', 'cryptoexchange', $userId );
        $ret['CSrefCred'] = ReferralCredit::getRefCredSumByCategory( 'withdrawn', 'cryptoshuffle', $userId );

        $ret['TotalRefCred'] = $ret['CTrefCred'] + $ret['MSrefCred'] + $ret['CMrefCred'] + $ret['CErefCred'] + $ret['CSrefCred'];
        //dd( $ret );

        return view('member.referral-credits', $ret);
    }

    public function getReferralCredits( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $packageId = 0;
        $packageCat = '';

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        // if ( $packageCat == 'cryptoshuffle' ) {
        //     $packageCat = 'raffle';
        // }

        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
            'packageId' => $packageId,
            'packageCat' => $packageCat,
        );
        $ret['data'] = ReferralCredit::getReferralCredits( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $RefCredits = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $RefCredits->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $RefCredits->total() ) {
                $showingCount = $RefCredits->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$RefCredits->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        # referral IDs
        $referralIds = array();
        foreach ($ret['data']['list'] as $k => $item) {
            if ( !in_array( $item->investor_user_id, $referralIds)) {
                $referralIds[] = $item->investor_user_id;
            }
        }
        //$ret['referralIds'] = $referralIds;

        # get referral usernames
        $referrals = Member::getUsersByIds( $referralIds );
        $ret['referrals'] = $referrals;

        $referralNames = array();
        if ( $referrals ) {
            foreach ($referrals as $key => $ref) {
                $referralNames[$ref->id]['username'] = $ref->username;
                $referralNames[$ref->id]['membership'] = $ref->membership;
            }
        }
        $ret['referralNames'] = $referralNames;


        # total referral credit
        $ret['rcTotal'] = LpjHelpers::amt2( ReferralCredit::getRefCredSumByPackageId( 'withdrawn', $packageCat, $userId, $packageId ) );

        $ret2 = array(
            'RefCredits' => $ret['data']['list'],
            'ReferralNames' => $referralNames
        );

        if ( $packageCat == 'membership' ) {
            # apply referall credits template
            $ret['returnHTML'] = view('member.referral-credits.referral-credit-list-item-2')->with($ret2)->render();

        }else{
            # apply referall credits template
            $ret['returnHTML'] = view('member.referral-credits.referral-credit-list-item')->with($ret2)->render();
        }
            

        return response()->json($ret);
    }

    public function pushToBank( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $refCredId = '';
        $packageId = false;
        $packageCat = '';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $refCredId ) || empty( $packageCat ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        # check if not yet withdrawn
        $refCred = false;
        if ( $ret['isError'] == false ) {
            $refCred = ReferralCredit::getSingleRefCred( $refCredId );

            if ( !$refCred ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Referral credit not found.';
            }
        }

        if ( $ret['isError'] == false ) {
            if ( $refCred->status == 'withdrawn' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'This referral credit is already withdrawn.';
            }
        }

        if ( $ret['isError'] == false ) {
            # update referral credit status
            ReferralCredit::updateRefCredStatus( $refCredId );


            # get referral credit totals
            $refCredTotals = ReferralCredit::getRefCredTotals( $userId );

            # fix for un updated table column name
            if ( $packageCat == 'cryptoshuffle' ) {
                $packageCat = 'raffle';
            }
            //$ret['refCred'] = $packageCat;
            //return response()->json($refCredTotals);
            

            # update referal credits earned in a pkg categroy
            # --------------------------
            $refCredSum = $refCredTotals->$packageCat + $refCred->amount;
            ReferralCredit::updatePkgCatTotalRefCred( $packageCat, $refCredSum, $userId );

            # update referral credits earning from all categories
            # --------------------------
            $refCredTotals->$packageCat = $refCredSum;
            $rct = $refCredTotals;
            $refCredOverallTotal = $rct->raffle + $rct->cryptotrading + $rct->cryptomining + $rct->membership + $rct->cryptoexchange + $rct->cryptoatm;
            ReferralCredit::updateOverallTotal( $userId, $refCredOverallTotal );
            $ret['rcOverallTotal'] = $refCredOverallTotal;


            # update user bank
            # --------------------------
            # update bank total - referral_credit column
            UserBank::updateBankSectionTotal( $userId, 'referral_credit', $refCredOverallTotal );
            UserBank::computeAvalableBal( $userId );


            if ( $packageCat == 'membership' ) {
                $ret['rcTotal'] = LpjHelpers::amt2( $refCredSum );

            }else{
                if ( $packageId ) {
                    $ret['rcTotal'] = LpjHelpers::amt2( ReferralCredit::getRefCredSumByPackageId( 'withdrawn', $packageCat, $userId, $packageId ) );

                }else{
                    $ret['rcTotal'] = LpjHelpers::amt2( $refCredSum );
                }
            }
        }

        return response()->json($ret);
    }
} 
