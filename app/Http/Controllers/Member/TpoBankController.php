<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\UserbankFund;

use App\Model\Member\Payout;

use App\Model\Member\ReferralCredit;
use App\Model\Member\PackageEarning;

use App\Helpers\LpjHelpers;
use App\Helpers\CoinPaymentsPG;
use App\Helpers\BitPalAPI;

class TpoBankController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function index( Request $request ) {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );
        
        $userId = Auth::id();
        $user = Auth::user();

        //$x = Payout::countCurrentMonthPayouts( $userId, date('Y-m-d H:i:s') );
        //dd( $x );




        # compute referral credit overall total
        # ------------------------------------------------------------
        $refCredTotal = ReferralCredit::getOverallSum( $userId );
        $pkgEarningsTotal = PackageEarning::getOverallSum( $userId );

        $args = array(
            'userId' => $userId, 
            'refCredOverallTotal' => $refCredTotal,
            'packageEarningTotal' => $pkgEarningsTotal
        );
        UserBank::reComputeBankTotals( $args );




        # get user bank info
        # ------------------------------------------------------------
        $uBank = new UserBank( $userId );
        $userBank = $uBank->getUserBank();
        $ret['userBank'] = $userBank;

        if ( config('app.is_sandbox') == true ) {
            if ( $userBank->available_bal == 0 ) {
                $testFund = array(
                    'user_id' => $userId,
                    'transaction_code' => LpjHelpers::lpjGenerateRandString(12),
                    'amount' => 300000,
                    'description' => 'Test Fund',
                    'type' => 'in',
                    'section' => 'deposit',
                    'transaction_date' => date('Y-m-d H:i:s'),
                    'status' => 'completed',
                    'details' => ''
                );
                $uBank->insertBankTransaction( $testFund );

                UserBank::updateOvarallTotal($userId, 300000);
            }
        }
        
        

        # add fund modal
        # ------------------------------------------------------------
        $openTab = '';
        if ( isset($_GET['tab']) ) {
            $openTab = LpjHelpers::fss($_GET['tab']);
        }
        $ret['openTab'] = $openTab;




        # Bitpal
        # ------------------------------------------------------------
        $bitpalClientID = BitPalAPI::config('client_id');

        $ret['accessToken'] = '';
        $ret['btsAccountsDropdown'] = array();
        $ret['USDtoBTSRate'] = 1;
        $ret['bitpalAuthUrl'] = BitPalAPI::config('api_home_url').'/#/oauth/authorize?client_id='.$bitpalClientID.'&response_type=code&redirect_uri='.url('/').'/member/oauth-code&state=addfund';
        $ret['bitpalSignupUrl'] = BitPalAPI::config('api_home_url').'/#/signup';
        $ret['bitpalAccessTokenLifeTime'] = 1;
        $ret['bitpalDefaultBtsAccount'] = 1;

        $fields = array( 'bitpal_access_token', 'bitpal_access_token_time', 'bitpal_bts_accounts', 'bitpal_default_bts_account' );
        $bitPalInfo = UserMeta::getUserMeta( $userId, $fields );
        //dd( $bitPalInfo );

        if ( $bitPalInfo ) {
            # default crypto account and dropdown account selections
            if ( $bitPalInfo->bitpal_bts_accounts != '' ) {
                $ret['btsAccountsDropdown'] = json_decode($bitPalInfo->bitpal_bts_accounts);
                $btsAccountsDropdown = $ret['btsAccountsDropdown'];

                if ( $bitPalInfo->bitpal_default_bts_account != '' ) {
                    $ret['bitpalDefaultBtsAccount'] = $bitPalInfo->bitpal_default_bts_account;

                }else{
                    $ret['bitpalDefaultBtsAccount'] = $btsAccountsDropdown[0];
                }
            }

            # access token
            if ( $bitPalInfo->bitpal_access_token != '' ) {
                $ret['accessToken'] = $bitPalInfo->bitpal_access_token;
            }

            # access token lifetime
            if ( $bitPalInfo->bitpal_access_token_time != '' ) {
                $accessTokenTime = $bitPalInfo->bitpal_access_token_time;
                $secondsDiff = time() - (int)$accessTokenTime;
                $minuteDiff = ($secondsDiff/60);
                $ret['bitpalAccessTokenLifeTime'] = $minuteDiff;
            }
        }

        $ret['bitsharesAccount'] = $user->bitshares_name; 




        # withdraw popup
        # ------------------------------------------------------------
        $ret['withdrawFOrmEnabled'] = true;
        $ret['withdrawFormReqmtPassed'] = false;

        # get latest addfund
        $LatestBitpalAddFund = UserbankFund::getLatestBitpalAddFund( $userId );
        //$LatestBitpalAddFund = true;

        # check lastest add fund via bitpal
        if ( !empty($bitPalInfo->bitpal_default_bts_account) && !empty($LatestBitpalAddFund)) {
            $ret['withdrawFormReqmtPassed'] = true;
        }


        //dd( $ret );

        return view('member.tpo-bank', $ret);
    }

    public function getBankTransactions( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $userBank = new UserBank( $userId );

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;
        $section = '';

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
            'section' => $section,
        );
        $ret['params'] = $params;
        $ret['bankTrnxs'] = $userBank->getTransactions2( $params );
        $ret['pageLinks'] = $ret['bankTrnxs']['pageLinks'];

        $trList = $ret['bankTrnxs']['list'];

        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $trList->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $trList->total() ) {
                $showingCount = $trList->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$trList->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        //dd($ret['bankTrnxs']);
        $ret['returnHTML'] = view('member.bank.tpo-bank-transactions')->with('bankTrnxs', $ret['bankTrnxs']['list'])->render();

        return response()->json($ret);
    }



    /*  
    ADD FUND
    */
    public function addFund( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # init vars
        $amount = 0;
        $paymentGateway = 'coinpayments';
        
        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        $pgClass = "\\App\\Helpers\\". $this->getClassname( $paymentGateway );

        if ( $pgClass ) {
            $pg = new $pgClass();

        }else{
            $ret['isError'] = true;
            $ret['msg'][] = 'Invalid Payment Gateway';
        }
        
        # validate minimum amount
        if ( $ret['isError'] == false ) {
            if ( !$pg->isValidMinAmount( $amount ) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Mininmum amount is $'.$pg->getMinAmount();
            }
        }

        if ( $ret['isError'] == false ) {
            $transactionCode = LpjHelpers::lpjGenerateRandString(8).time();

            # insert add fund request
            $bankFund = new UserBankFund;
            $params = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'amount' => $amount,
                'gateway' => $paymentGateway,
            );
            $bankFund->insertAddFundRequest( $params );

            # insert bank transaction
            $userBank = new UserBank( $userId );
            $ret['userBank'] = $userBank->getUserBank();

            $params2 = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'amount' => $amount,
                'description' => $pg->trDescription(),
                'type' => 'in',
                'section' => 'deposit',
                'status' => 'pending'
            );
            $userBank->insertBankTransaction( $params2 );

            # generate button
            $params = array(
                'amount' => $amount,
                'trCode' => $transactionCode,
            );
            $buttonScript = $pg->generatePaymentButton( $params );

            $ret['buttonScript'] = $buttonScript;
        }

        if ( $ret['isError'] == false ) {
            $ret['msg'][] = 'Request successfull';
        }

        return response()->json($ret);
    }

    public function cancelAddFund( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $cnl = 'test';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        # cancel add fund request
        $bankFund = new UserBankFund;
        $trnx = $bankFund->getSingleTransaction('transaction_code', $cnl );

        # cancel bank transaction
        $userBank = new UserBank( $userId );
        
        if ( $trnx->user_id != $userId ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'This transaction is not yours';
        }

        if ( $ret['isError'] == false ) {
            $bankFund->updateTransactionStatus( $cnl, 'cancelled');
            $userBank->updateTransactionStatus( $cnl, 'cancelled');

            $ret['msg'][] = 'Transaction successfully cancelled';
        }

        return response()->json($ret);
    }

    // get payment gateway classname 
    private function getClassname( $pg ){
        $className = false;

        $paymentGateways = array(
            'coinpayments' => 'CoinPaymentsPG',
            'bitshares' => 'BitsharesPG',
            'blockchain' => 'BlockchainPG'
        );

        if ( isset($paymentGateways[$pg]) ) {
            $className = $paymentGateways[$pg];
        }

        return $className;
    }

} 
