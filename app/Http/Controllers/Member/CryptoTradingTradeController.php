<?php
namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;

use App\Model\Member\CryptoTradingPackage;
use App\Model\Member\CryptoTradingPackageTrade;

use App\Model\Member\PackageEarning;
use App\Helpers\LpjHelpers;

class CryptoTradingTradeController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function getUserTrades( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $packageId = 0;
        $packageCat = '';

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;
        $status = '';

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }


        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
            'packageId' => $packageId,
            'status' => $status
        );
        $ret['data'] = CryptoTradingPackageTrade::getAll( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $userTrades = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $userTrades->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $userTrades->total() ) {
                $showingCount = $userTrades->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$userTrades->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        # get total package earnings from this package
        $pkgEarningTotal = CryptoTradingPackageTrade::getSumByPackageId( $userId, $packageId, 'withdrawn' );
        $pkgEarningTotalPending = CryptoTradingPackageTrade::getSumByPackageId( $userId, $packageId, 'completed' );

        $ret['pkgEarningTotalWithdrawn'] =  LpjHelpers::amt2( $pkgEarningTotal );
        $ret['pkgEarningTotalPending'] =  LpjHelpers::amt2( $pkgEarningTotalPending );
        $ret['pkgEarningTotal'] =  LpjHelpers::amt2( $pkgEarningTotalPending + $pkgEarningTotal );


        $ret2 = array(
            'userTrades' => $ret['data']['list']
        );

        $ret['returnHTML'] = view('member.packages.crypto-trading.user-trades.trade-list-item')->with($ret2)->render();

        return response()->json($ret);
    }

    public function pushToBank( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $tradeId = '';
        $packageId = false;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $tradeId ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        # get user trade info
        # check if not yet withdrawn
        $trade = false;
        if ( $ret['isError'] == false ) {
            $trade = CryptoTradingPackageTrade::getSingle( $tradeId );

            if ( !$trade ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Trade Profit not found.';
            }
        }

        if ( $ret['isError'] == false ) {
            if ( $trade->status == 'withdrawn' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'This Trade Profit is already withdrawn.';
            }
        }
        //$ret['trade'] = $trade;

        //$ret['isError'] = true;

        if ( $ret['isError'] == false ) {
            # update trade status
            CryptoTradingPackageTrade::updateStatus( $tradeId, 'withdrawn' );

            
            # get total trade profit from a specific package
            $tradeProfitTotalWithdrawn = CryptoTradingPackageTrade::getSumByPackageId( $userId, $trade->package_id, 'withdrawn' );
            $tradeProfitTotalPending = CryptoTradingPackageTrade::getSumByPackageId( $userId, $trade->package_id, 'completed' );

            $ret['tradeProfitTotalWithdrawn'] =  LpjHelpers::amt2( $tradeProfitTotalWithdrawn );
            $ret['tradeProfitTotalPending'] =  LpjHelpers::amt2( $tradeProfitTotalPending );
            $ret['tradeProfitTotal'] =  LpjHelpers::amt2( $tradeProfitTotalWithdrawn + $tradeProfitTotalPending );


            # get trade profit total
            //$tradeProfitOverallTotal = CryptoTradingPackageTrade::getOverallSum( $userId );
            //$ret['tradeProfitTotal'] = $tradeProfitTotal;


            # insert package earning
            # -----------------------------------------------------
            $packageCat = 'cryptotrading';

            $pkgProfitParams = array(
                'row_id' => $tradeId,
                'user_id' => $userId,
                'profit_id' => $trade->trade_id,
                'package_id' => $trade->package_id,
                'package_cat' => $packageCat,
                'type' => 'trade-profit',
                'amount' => $trade->user_profit,
                'withdraw_date' => $trade->date,
                'data' => $trade
            );
            PackageEarning::insert( $pkgProfitParams );


            # get package earning totals
            # -----------------------------------------------------
            $pkgEarningTotals = PackageEarning::getSingleTotals( $userId );
            if ( !$pkgEarningTotals ) {
                $pkgEarningTotals = PackageEarning::createTotalsRow( $userId );
            }

            # update package_earning_totals.cryptotrading
            # -----------------------------------------------------
            $newPkgEarningTotal = $pkgEarningTotals->$packageCat + $trade->user_profit;
            PackageEarning::updatePkgCatTotalEarning( $userId, $packageCat, $newPkgEarningTotal );

            # update overall total
            $pkgEarningTotals->$packageCat = $newPkgEarningTotal;
            $pkgEarningOverallTotal = PackageEarning::updatePackageEarningTotals( $userId, $pkgEarningTotals );


            # update user bank - package_earning column
            # -----------------------------------------------------
            UserBank::updateBankSectionTotal( $userId, 'package_earning', $pkgEarningOverallTotal );
            # update available balance
            UserBank::computeAvalableBal( $userId );
        }

        return response()->json($ret);
    }

    public function capOffTradeAmount( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # default values
        $tradeId = '';
        $fundId = '';
        $packageId = '';

        $amountToCapOff = 0;
        $cappedOffTradeAmount = 0;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $packageId ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        if ( empty($tradeId) && empty($fundId) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters (Trade Id and Fund Id).';
        }

        # get pacakge fund info
        if ( $ret['isError'] == false ) {
            if ( !empty($fundId) ) {
                $fundInfo = CryptoTradingPackage::getSinglePackageFund( $userId, $fundId );

            }else{
                $fundInfo = CryptoTradingPackage::getSinglePackageFundByPkgId( $userId, $packageId );
            }
            
            if ( !$fundInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Failed to locate Package Fund.';
            }

            $ret['fundInfo'] = $fundInfo;
        }

        # update if its already capped off before
        if ( $ret['isError'] == false ) {
            $amountToCapOff = $fundInfo->max_amount - $fundInfo->amount;
            $cappedOffTradeAmount = $fundInfo->max_amount;

            if ( $amountToCapOff <= 0 ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'I think this was already capped-off. Trade is now updated to correct status. Please refresh this page.';

                # update trade status
                CryptoTradingPackageTrade::updateStatusByFundId( $userId, $fundInfo->id, 'cappedoff' );

                # update trade amount
                CryptoTradingPackage::updateTradeAmount( $fundInfo->id, $cappedOffTradeAmount );
            }
        }

        # cap off a single negative trade
        if ( $ret['isError'] == false && $tradeId != '' ) {
            $tradeInfo = CryptoTradingPackageTrade::getSingle( $tradeId );
            $ret['tradeInfo'] = $tradeInfo;

            if ( $tradeInfo ) {
                $amountToCapOff = abs($tradeInfo->trade_earning);
                $cappedOffTradeAmount = $fundInfo->amount + $amountToCapOff;
            }
        }

        $ret['amountToCapOff'] = $amountToCapOff;
        $ret['cappedOffTradeAmount'] = $cappedOffTradeAmount;

        # get user bank
        if ( $ret['isError'] == false ) {
            $userBank = UserBank::getUserBank2( $userId );
            $ret['userBank'] = $userBank;

            if ( $userBank->available_bal < $amountToCapOff ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Insufficient bank balance. Your bank balance is $'.LpjHelpers::amt2($userBank->available_bal).', You have to cap off a total of $'.LpjHelpers::amt2($amountToCapOff);
            }
        }

        if ( $ret['isError'] == false ) {
            $packageName = CryptoTradingPackage::getPackageName( $fundInfo->package_id );

            $details = array(
                'package_fund_id' => $fundInfo->id,
                'trade_amount' => $fundInfo->amount
            );

            # add bank transaction
            $params = array(
                'user_id' => $userId,
                'amount' => $amountToCapOff,
                'transaction_code' => 0,
                'description' => 'Cap Off Trade Amount for: '.$packageName,
                'section' => 'cap-off-trade-amount',
                'type' => 'out',
                'status' => 'completed',
                'details' => json_encode($details)
            );
            UserBank::insertBankTransaction2( $params );

            
            # update trade amount
            CryptoTradingPackage::updateTradeAmount( $fundInfo->id, $cappedOffTradeAmount );

            # update user trade status
            if ( $tradeId != '' ) {
                CryptoTradingPackageTrade::updateStatus( $tradeId, 'cappedoff' );

            }else{
                CryptoTradingPackageTrade::updateStatusByFundId( $userId, $fundInfo->id, 'cappedoff' );
            }

            # update bank totals
            UserBank::computeAvalableBal( $userId );

            $ret['new_trade_amount'] = $amountToCapOff + $fundInfo->amount;
            $ret['msg'][] = 'Trade amount successfully capped off. Please refresh this page.';
        }
        
        //$ret['isError'] = true;
        // $ret['msg'][] = 'xxx.';

        return response()->json($ret);
    }

} 
