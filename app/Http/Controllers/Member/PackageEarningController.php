<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserBank;
use App\Model\Member\PackageEarning;

use App\Model\Member\CryptoTradingPackage;
use App\Model\Member\CryptoMiningPackage;
use App\Model\Member\CryptoExchangePackage;

use App\Helpers\LpjHelpers;

class PackageEarningController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function index(){
        $ret = array();

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $ret['CTpkgEarning'] = PackageEarning::getSumByCategory( $userId, 'cryptotrading' );
        $ret['MSpkgEarning'] = PackageEarning::getSumByCategory( $userId, 'membership' );
        $ret['CMpkgEarning'] = PackageEarning::getSumByCategory( $userId, 'cryptomining' );
        $ret['CEpkgEarning'] = PackageEarning::getSumByCategory( $userId, 'cryptoexchange' );
        $ret['CSpkgEarning'] = PackageEarning::getSumByCategory( $userId, 'cryptoshuffle' );

        $ret['TotalPkgEarning'] = $ret['CTpkgEarning'] + $ret['MSpkgEarning'] + $ret['CMpkgEarning'] + $ret['CEpkgEarning'] + $ret['CSpkgEarning'];
        //dd( $ret );

        return view('member.package-earnings', $ret);
    }

    public function getPackageEarnings( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $packageId = 0;
        $packageCat = '';

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        // if ( $packageCat == 'cryptoshuffle' ) {
        //     $packageCat = 'raffle';
        // }


        # get earnings
        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
            'packageId' => $packageId,
            'packageCat' => $packageCat,
        );
        $ret['data'] = PackageEarning::getAll( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $pkgEarnings = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $pkgEarnings->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $pkgEarnings->total() ) {
                $showingCount = $pkgEarnings->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$pkgEarnings->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;



        # get package names
        $pkgNames = array();

        $packageIds = array();
        foreach ($ret['data']['list'] as $k => $item) {
            if ( !in_array( $item->package_id, $packageIds)) {
                $packageIds[$item->package_cat][$k][] = $item->package_id;
            }
        }
        $ret['packageIds'] = $packageIds;


        # cryptotrading package names
        if ( isset($packageIds['cryptotrading']) ) {
            $ctPackageNames = CryptoTradingPackage::getPackageByIds($packageIds['cryptotrading']);

            foreach ($ctPackageNames as $k1 => $v1) {
                $pkgNames['cryptotrading'][$v1->id] = $v1->name;
            }
        }

        # cryptomining package names
        if ( isset($packageIds['cryptomining']) ) {
            $cmPackageNames = CryptoMiningPackage::getPackageByIds($packageIds['cryptomining']);

            foreach ($cmPackageNames as $k1 => $v1) {
                $pkgNames['cryptomining'][$v1->id] = $v1->name;
            }
        }

        # cryptoexchange package names
        if ( isset($packageIds['cryptoexchange']) ) {
            $cePackageNames = CryptoExchangePackage::getPackageByIds($packageIds['cryptoexchange']);

            foreach ($cePackageNames as $k1 => $v1) {
                $pkgNames['cryptoexchange'][$v1->id] = $v1->name;
            }
        }

        $ret['pkgNames'] = $pkgNames;


        $ret2 = array(
            'pkgEarnings' => $ret['data']['list'],
            'pkgNames' => $pkgNames,
        );


        # apply referall credits template
        $ret['returnHTML'] = view('member.package-earnings.list-item')->with($ret2)->render();
        
        return response()->json($ret);
    }

    public function pushToBank( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $refCredId = '';
        $packageId = false;
        $packageCat = '';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $refCredId ) || empty( $packageCat ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        # check if not yet withdrawn
        $refCred = false;
        if ( $ret['isError'] == false ) {
            $refCred = ReferralCredit::getSingleRefCred( $refCredId );

            if ( !$refCred ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Referral credit not found.';
            }
        }

        if ( $ret['isError'] == false ) {
            if ( $refCred->status == 'withdrawn' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'This referral credit is already withdrawn.';
            }
        }

        if ( $ret['isError'] == false ) {
            # update referral credit status
            ReferralCredit::updateRefCredStatus( $refCredId );


            # get referral credit totals
            $refCredTotals = ReferralCredit::getRefCredTotals( $userId );

            # fix for un updated table column name
            if ( $packageCat == 'cryptoshuffle' ) {
                $packageCat = 'raffle';
            }
            //$ret['refCred'] = $packageCat;
            //return response()->json($refCredTotals);
            

            # update referal credits earned in a pkg categroy
            # --------------------------
            $refCredSum = $refCredTotals->$packageCat + $refCred->amount;
            ReferralCredit::updatePkgCatTotalRefCred( $packageCat, $refCredSum, $userId );

            # update referral credits earning from all categories
            # --------------------------
            $refCredTotals->$packageCat = $refCredSum;
            $rct = $refCredTotals;
            $refCredOverallTotal = $rct->raffle + $rct->cryptotrading + $rct->cryptomining + $rct->membership + $rct->cryptoexchange + $rct->cryptoatm;
            ReferralCredit::updateOverallTotal( $userId, $refCredOverallTotal );
            $ret['rcOverallTotal'] = $refCredOverallTotal;


            # update user bank
            # --------------------------
            # update bank total - referral_credit column
            UserBank::updateBankSectionTotal( $userId, 'referral_credit', $refCredOverallTotal );
            UserBank::computeAvalableBal( $userId );


            if ( $packageCat == 'membership' ) {
                $ret['rcTotal'] = LpjHelpers::amt2( $refCredSum );

            }else{
                if ( $packageId ) {
                    $ret['rcTotal'] = LpjHelpers::amt2( ReferralCredit::getRefCredSumByPackageId( 'withdrawn', $packageCat, $userId, $packageId ) );

                }else{
                    $ret['rcTotal'] = LpjHelpers::amt2( $refCredSum );
                }
            }
        }

        return response()->json($ret);
    }
} 
