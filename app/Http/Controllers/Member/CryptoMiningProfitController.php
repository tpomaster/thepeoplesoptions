<?php
namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;

use App\Model\Member\CryptoMiningPackage;
use App\Model\Member\CryptoMiningPackageProfit;

use App\Model\Member\PackageEarning;
use App\Helpers\LpjHelpers;

class CryptoMiningProfitController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function getUserProfits( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        $packageId = 0;
        $packageCat = '';

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;
        $status = '';

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
            'packageId' => $packageId,
            'status' => $status
        );
        $ret['data'] = CryptoMiningPackageProfit::getAll( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $userProfit = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $userProfit->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $userProfit->total() ) {
                $showingCount = $userProfit->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$userProfit->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        # get total package earnings from this package
        $pkgEarningTotal = CryptoMiningPackageProfit::getSumByPackageId( $userId, $packageId, 'withdrawn' );
        $pkgEarningTotalPending = CryptoMiningPackageProfit::getSumByPackageId( $userId, $packageId, 'completed' );

        $ret['pkgEarningTotalWithdrawn'] =  LpjHelpers::amt2( $pkgEarningTotal );
        $ret['pkgEarningTotalPending'] =  LpjHelpers::amt2( $pkgEarningTotalPending );
        $ret['pkgEarningTotal'] =  LpjHelpers::amt2( $pkgEarningTotalPending + $pkgEarningTotal );


        $ret2 = array(
            'userProfit' => $ret['data']['list']
        );

        $ret['returnHTML'] = view('member.packages.crypto-mining.user-profit.cm-profit-list-item')->with($ret2)->render();

        return response()->json($ret);
    }

    public function pushToBank( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $profitId = '';
        $packageId = false;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $profitId ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        # get user profit info
        # check if not yet withdrawn
        $profit = false;
        if ( $ret['isError'] == false ) {
            $profit = CryptoMiningPackageProfit::getSingle( $profitId );

            if ( !$profit ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Mining Profit not found.';
            }
        }

        if ( $ret['isError'] == false ) {
            if ( $profit->status == 'withdrawn' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'This Mining Profit is already withdrawn.';
            }
        }
        //$ret['profit'] = $profit;

        //$ret['isError'] = true;

        if ( $ret['isError'] == false ) {
            # update profit status
            CryptoMiningPackageProfit::updateStatus( $profitId, 'withdrawn' );

            
            # get total profit profit from a specific package
            $miningProfitTotalWithdrawn = CryptoMiningPackageProfit::getSumByPackageId( $userId, $profit->package_id, 'withdrawn' );
            $miningProfitTotalPending = CryptoMiningPackageProfit::getSumByPackageId( $userId, $profit->package_id, 'completed' );

            $ret['miningProfitTotalWithdrawn'] =  LpjHelpers::amt2( $miningProfitTotalWithdrawn );
            $ret['miningProfitTotalPending'] =  LpjHelpers::amt2( $miningProfitTotalPending );
            $ret['miningProfitTotal'] =  LpjHelpers::amt2( $miningProfitTotalWithdrawn + $miningProfitTotalPending );


            # get mining profit total
            //$miningProfitOverallTotal = CryptoMiningPackageProfit::getOverallSum( $userId );
            //$ret['miningProfitTotal'] = $miningProfitTotal;


            # insert package earning
            # -----------------------------------------------------
            $packageCat = 'cryptomining';

            $pkgProfitParams = array(
                'row_id' => $profitId,
                'user_id' => $userId,
                'transaction_code' => '',
                'package_id' => $profit->package_id,
                'package_cat' => $packageCat,
                'type' => 'profit-profit',
                'amount' => $profit->user_profit,
                'withdraw_date' => $profit->date,
                'data' => $profit
            );
            PackageEarning::insert( $pkgProfitParams );


            # get package earning totals
            # -----------------------------------------------------
            $pkgEarningTotals = PackageEarning::getSingleTotals( $userId );
            if ( !$pkgEarningTotals ) {
                $pkgEarningTotals = PackageEarning::createTotalsRow( $userId );
            }

            # update package_earning_totals.cryptomining
            # -----------------------------------------------------
            $newPkgEarningTotal = $pkgEarningTotals->$packageCat + $profit->user_profit;
            PackageEarning::updatePkgCatTotalEarning( $userId, $packageCat, $newPkgEarningTotal );

            # update overall total
            $pkgEarningTotals->$packageCat = $newPkgEarningTotal;
            $pkgEarningOverallTotal = PackageEarning::updatePackageEarningTotals( $userId, $pkgEarningTotals );


            # update user bank - package_earning column
            # -----------------------------------------------------
            UserBank::updateBankSectionTotal( $userId, 'package_earning', $pkgEarningOverallTotal );
            # update available balance
            UserBank::computeAvalableBal( $userId );
        }

        return response()->json($ret);
    }

} 
