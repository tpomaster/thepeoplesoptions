<?php
namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\CryptoMiningPackage;
use App\Model\Member\ReferralCredit;

use App\Model\Member\CryptoMiningPackageProfit;

use App\Helpers\LpjHelpers;

class CryptoMiningController extends Controller {

    public function __construct()
    {
        $this->middleware('auth:member');
    }

    public function index(Request $request)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => '',
            'myPackages' => '',
            'pkg_details_url' => url('/').'/member/packages/crypto-mining/details/'
        );

        $ret['pageTitle2'] = 'All Crypto Mining Packages';
        
        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-mining.cm-packages-list', $ret);
    }

    public function viewPackagesByStatus($status)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => $status,
            'myPackages' => '',
            'pkg_details_url' => url('/').'/member/packages/crypto-mining/details/'
        );

        $ret['pageTitle2'] = strtoupper($status).' Crypto Mining Packages';
        
        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-mining.cm-packages-list', $ret);
    }

    public function myPackages(Request $request)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => 'funded-active',
            'myPackages' => 'yes',
            'pkg_details_url' => url('/').'/member/packages/crypto-mining/details/'
        );

        $ret['pageTitle2'] = 'Crypto Mining: My Funded Packages';

        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-mining.cm-my-packages', $ret);
    }

    public function viewPackagesAjax(Request $request)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        $status = '';
        $myPackages = '';
        $packageIds = false;

        # get input
        if ($ret['isError'] == false){
            $input = $request->all();
            $input = LpjHelpers::fss($input);
            extract($input);

            $qpage = $page;
        }

        # my packages params
        if ($myPackages != ''){
            $trList = CryptoMiningPackage::getUserFundedPackages($userId);
            $myPkgsIds = array();

            if ($trList){
                foreach ($trList as $k => $v) {
                    $myPkgsIds[] = $v->package_id;
                }
            }

            if (!empty($myPkgsIds)){
                $packageIds = $myPkgsIds;
            }

            $ret['packageIds'] = $packageIds;
        }

        if ($myPackages != '' && empty($packageIds)){
            $ret['packages'] = array();
            $ret['pageLinks'] = array();

            $trList = array();

            # pagination html
            $paginationHtml = $ret['pageLinks'];
            $ret['paginationHtml'] = $paginationHtml;

            //dd($ret['packages']);
            $ret['returnHTML'] = "You haven't funded any package.";

        }else{
            $params = array(
                'status' => $status,
                'packageIds' => $packageIds,
                'qpage' => $qpage,
                'qlimit' => $qlimit,
            );
            $ret['packages'] = CryptoMiningPackage::getPackages($params);
            $ret['pageLinks'] = $ret['packages']['pageLinks'];

            $packages = $ret['packages']['list'];


            # get unwithdrawn profit sum
            $unwithdrawnProfitSum = array();
            $packageIds = array();

            if (!empty($packages)){
                foreach($packages as $k => $p){
                    $packageIds[] = $p->id;
                }

                $unwithdrawnProfitSum = CryptoMiningPackageProfit::getUnwithdrawnProfitSumByGroup($userId, $packageIds);
            }

            $ret['unwithdrawnProfitSum'] = $unwithdrawnProfitSum;




            # pagination html
            $paginationHtml = $ret['pageLinks'];

            if ($packages->total() >  $qlimit){
                $showingCount = $qpage*$qlimit;
                if ($showingCount > $packages->total()){
                    $showingCount = $packages->total();
                }
                $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$packages->total().'</span>';
                $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
            }
            $ret['paginationHtml'] = $paginationHtml;

            $ret2 = array(
                'uwProfitSum' => $unwithdrawnProfitSum,
                'packages' => $ret['packages']['list']
            );

            //dd($ret['packages']);
            if ($packages->total() > 0){
                $ret['returnHTML'] = view('member.packages.crypto-mining.cm-package-summary')->with($ret2)->render();

            }else{
                $ret['returnHTML'] = "NO RESULT...";
            }
        }

        return response()->json($ret);
    }


    public function viewPackageDetails($packageId)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        //dd($user);

        # get users membership level
        $membershipLevel = $user->membership;


        # get package info
        # --------------------------
        $packageInfo = CryptoMiningPackage::getSingePackage($packageId);
        //dd($packageInfo);

        # end if package info is not found
        if (!$packageInfo){
            return view('member.packages.not-found', $ret);
        }
        
        $managementFee = unserialize($packageInfo->management_fee);
        if (!isset($managementFee[$membershipLevel])){
            if (isset($managementFee['free'])){
                $packageInfo->management_fee = $managementFee['free'];
            }else{
                $packageInfo->management_fee = $managementFee['bronze'];
            }

        }else{
            $packageInfo->management_fee = $managementFee[$membershipLevel];
        }

        $ret['managementFee'] = $packageInfo->management_fee;
        $ret['withrawableFund'] = $packageInfo->withrawable_fund;

        $packageInfo->tier_config = unserialize($packageInfo->tier_config);
        //dd($packageInfo);


        # compute funded percent
        # --------------------------
        $totalFunded = CryptoMiningPackage::getTotalFunds($packageId);
        $packageInfo->funded_amount = $totalFunded;
        $funded_percent = ((float)$totalFunded / (float)$packageInfo->amount )*100;
        $packageInfo->funded_percent = $funded_percent;



        # available for funding
        # --------------------------
        $packageInfo->available_for_funding =  (float)$packageInfo->amount - (float)$totalFunded;

        # update pacakge status
        if ($packageInfo->available_for_funding <= 0 && $packageInfo->status == 'available'){
            $params3 = array(
                'packageId' => $packageId,
                'status' => 'funded-active',
            );
            CryptoMiningPackage::updatePackageStatus($params3 );
        }



        # get my investment/fund
        # --------------------------
        $myFund = CryptoMiningPackage::getUserInvestment($packageInfo->id, $userId, '');
        $tradeStartDate = 'Pending';
        $tradeEndDate = 'Pending';
        if ($myFund){
            $myFundBreakdown = CryptoMiningPackage::getUserInvestmentBreakdown($myFund->id, $userId);
            $ret['myFundBreakdown'] = $myFundBreakdown;

            # start and end date
            if (!empty($myFund->start_date) && $myFund->start_date != '0000-00-00 00:00:00'){
                $tradeStartDate = LpjHelpers::niceDate($myFund->start_date);
                $tradeEndDate = LpjHelpers::addDays($myFund->start_date, $packageInfo->duration);

                $tradeStartDate = LpjHelpers::niceDate($tradeStartDate);
                $tradeEndDate = LpjHelpers::niceDate($tradeEndDate);
            }

            if ($myFund->start_date == '0000-00-00 00:00:00'){
                $tradeStartDate = CryptoMiningPackageProfit::getFirstProfit($userId, $packageInfo->id )->date;
                $tradeEndDate = LpjHelpers::addDays($tradeStartDate, $packageInfo->duration);

                $tradeStartDate = LpjHelpers::niceDate($tradeStartDate);
                $tradeEndDate = LpjHelpers::niceDate($tradeEndDate);
            }

            # TEMPORARY
            # --------------------------
            # count number of posted trades
            $postedProfitsCount = CryptoMiningPackageProfit::countPostedProfits($myFund->id);
            $ret['postedProfitsCount'] = $postedProfitsCount;

            $updParams = array(
                'posted_profits_count' => $postedProfitsCount,
                'updated_at' => LpjHelpers::dbDate()
            );
            CryptoMiningPackage::updateFundField($myFund->id, $updParams);
        }
        $ret['tradeStartDate'] = $tradeStartDate;
        $ret['tradeEndDate'] = $tradeEndDate;

        $ret['pkg'] = $packageInfo;
        $ret['myFund'] = $myFund;


        # exchange profit summary
        # --------------------------
        $totalManagementFee = 0;
        $grossEarning = 0;
        $netEarning = 0;
        if ($packageInfo->status != 'available'){
            $totalManagementFee = CryptoMiningPackageProfit::getFieldTotal($userId, $packageInfo->id, 'management_fee');
            $netEarning = CryptoMiningPackageProfit::getFieldTotal($userId, $packageInfo->id, 'user_profit');

            $grossEarning = $totalManagementFee + $netEarning;
        }
        $ret['totalManagementFee'] = $totalManagementFee;
        $ret['grossEarning'] = $grossEarning;
        $ret['netEarning'] = $netEarning;



        # get user bank
        # --------------------------
        $uBank = new UserBank($userId);
        $userBank = $uBank->getUserBank();

        $ret['userBank'] = $userBank;

        //dd($ret);
        return view('member.packages.crypto-mining.cm-package-details', $ret);
    }


    public function getMangementFees()
    {
        
    }


    public function processOrder(Request $request)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # expected parameters
        $pid = 0;
        $fundAmount = 0;

        # initialize other important variables
        $transactionCode = LpjHelpers::generateTrCode();
        $minPurchase = 10;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ($ret['isError'] == false){
            $input = $request->all();
            $input = LpjHelpers::fss($input);
            extract($input);
        }

        # get package info
        $packageInfo = CryptoMiningPackage::getSingePackage($pid);
        $ret['packageInfo'] = $packageInfo;

        # check if package is available
        if ($packageInfo->status != 'available'){
            $ret['isError'] = true;
            $ret['msg'][] = 'Package is fully funded.';
        }

        # check fundable amount
        if ($ret['isError'] == false){
            $totalFunds = CryptoMiningPackage::getTotalFunds($pid);
            $ret['totalFunds'] = $totalFunds;

            $fundableAmount = $packageInfo->amount - $totalFunds;
            $ret['fundableAmount'] = $fundableAmount;

            if ($fundableAmount <= 0){
                $ret['isError'] = true;
                $ret['msg'][] = 'Package is now fully funded.';
            }
        }

        # check if fund amount is bigger than fundable amount
        if ($ret['isError'] == false){
            $fundAmount = (int) $fundAmount;
            if ($fundableAmount < $fundAmount){
                $ret['isError'] = true;
                $ret['msg'][] = 'You exceeded the available amount for funding. Amount Avalable for funding is $'.$fundableAmount.'. (you submitted $'.$fundAmount.')';
            }
        }

        # check minimum purchase 
        if ($ret['isError'] == false){
            if ($fundAmount < $minPurchase){
                if ($fundableAmount < $minPurchase){
                    if ($fundableAmount > $fundAmount){
                        $ret['isError'] = true;
                        $ret['msg'][] = 'Minimum fund amount is $'.$fundableAmount.'. (you submitted $'.$fundAmount.')';

                    }

                }else{
                    $ret['isError'] = true;
                    $ret['msg'][] = 'Minimum fund amount is $'.$minPurchase.'. (you submitted $'.$fundAmount.')';
                }
            }
        }

        # check bank balance
        if ($ret['isError'] == false){
            $uBank = new UserBank($userId);
            $userBank = $uBank->getUserBank();
            if ($userBank->available_bal < $fundAmount){
                $ret['isError'] = true;
                $ret['msg'][] = 'Insuficient bank balance (your bank balance is $'.$userBank->available_bal.').';
            }
        }

        if ($ret['isError'] == false){
            $ret['msg'][] = 'Payment succcessfully processed. Please refresh this page.';

            $newTotalFunded = $packageInfo->funded_amount + $fundAmount;

            if ($newTotalFunded >= $packageInfo->amount){
                $newStatus = 'funded-active';
                $ret['newStatus'] = $newStatus;
            }

            # insert bank transaction
            # -------------------------
            $details = array(
                'package_id' => $packageInfo->id,
                'package_name' => $packageInfo->name,
                'package_amount' => $packageInfo->amount,
                'package_category' => 'cryptomining',
            );
            $params = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'amount' => $fundAmount,
                'description' => 'Package Purchase: '.$packageInfo->name,
                'type' => 'out',
                'section' => 'package-payment',
                'transaction_date' => date('Y-m-d H:i:s'),
                'status' => 'completed',
                'details' => $details
            );
            $uBank->insertBankTransaction($params);

        
            # insert package fund, compute referral credits
            # -------------------------
            $params5 = array(
                'userId' => $userId,
                'packageId' => $packageInfo->id,
                'packageName' => $packageInfo->name,
                'packageCat' => 'cryptomining',
                'fundAmount' => $fundAmount,
                'paymentStatus' => 'completed',
                'transactionCode' => $transactionCode,
                'duration' => $packageInfo->duration,
                'totalFunds' => $totalFunds,
                'tierConfig' => $packageInfo->tier_config,
                'forMembership' => 0,
            );
            if(isset($newStatus)){
                $params5['newPackageStatus'] = $newStatus;
            }
            CryptoMiningPackage::onPaymentComplete($params5 );


            # update bank totals - package_purchase
            # --------------------------
            $newPacakgePurchaseVal = $userBank->package_purchase + $fundAmount;
            UserBank::updateBankSectionTotal($userId, 'package_purchase', $newPacakgePurchaseVal );
            //UserBank::recomputeBankSectionTotal($userId, 'package-payment', 'completed');
            
            # update bank totals
            $userBank->package_purchase = $newPacakgePurchaseVal;
            UserBank::computeAvalableBal($userId, $userBank );
        }

        //$ret['msg'][] = 'insuficient_funds';
        //$ret['msg2'] = 'insuficient_funds';

        return response()->json($ret);
    }

    public function pushFundToBank(Request $request)
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # expected parameters
        $fundId = 0;
        $packageId = 0;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ($ret['isError'] == false){
            $input = $request->all();
            $input = LpjHelpers::fss($input);
            extract($input);
        }

        # get package info
        $packageInfo = CryptoMiningPackage::getSingePackage($packageId);
        $ret['packageInfo'] = $packageInfo;

        # get fund info
        # --------------------------
        $myFund = CryptoMiningPackage::getUserInvestment($packageInfo->id, $userId, '');

        if (!$myFund) {
            $ret['isError'] = true;
            $ret['msg'][] = 'I cannot find your Package Fund. ';
        }

        if ($ret['isError'] == false){
            $ret['myFund'] = $myFund;

            if ($myFund->mining_status != 'closed'){
                $ret['isError'] = true;
                $ret['msg'][] = 'Fund is still active. ';
            }

            if ($myFund->status == 'withdrawn'){
                $ret['isError'] = true;
                $ret['msg'][] = 'Fund was already pushed to bank. ';
            }
        }

        if ($ret['isError'] == false){
            CryptoMiningPackage::pushFundToBank($myFund);
        }

        return response()->json($ret);
    }
} 
