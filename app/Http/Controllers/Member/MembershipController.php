<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Helpers\LpjHelpers;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\MemberTier;
use App\Model\Member\UserBank;

use App\Model\Member\MembershipPackage;
use App\Model\Member\MembershipPowerBonus;
use App\Model\Member\CryptoTradingPackage;
use App\Model\Member\CryptoMiningPackage;
use App\Model\Member\CryptoShufflePackage;

use App\Model\Member\ReferralCredit;
use App\Model\Member\PackageEarning;

use DateTime;

class MembershipController extends Controller {
    public function __construct()  {
        $this->middleware('auth:member');

        // $this->middleware(function( $request, $next ) {
            
        //     $userInfo = $request->user();
 
        //     return $next($request);
        // });
    }


    public function index( Request $request ) {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );
        
        $userId = Auth::id();
        $user = Auth::user();
        //dd($user);

        # 
        $user = new Member;
        $userInfo = $user->getSingle('id', $userId);
        $ret['membership'] = $userInfo->membership;
        $ret['membershipEndDate'] = $userInfo->membership_end_date;

        
        $params = array();
        $pkgs = MembershipPackage::getPackages($params);

        $ret['pkgs'] = $pkgs;
        //dd($pkgs);

        $uBank = new UserBank( $userId );
        $userBank = $uBank->getUserBank();

        $ret['userBank'] = $userBank;


        // PackageEarning::recomputePackageEarningTotals( $userId );
        // $params = array( 'userId' => $userId );
        // UserBank::reComputeBankTotals( $params );

        if ( $userInfo->membership == '' || empty($userInfo->membership) ) {
            $userInfo->membership = 'free';
        }

        $ret['membershipDaysLeft'] = 0;


        # get membership package info / connected packages
        if ( $userInfo->membership != 'free' ) {
            if ( $userInfo->membership == 'silver' ) {
                $pkgTitle = 'Silver';
            }
            if ( $userInfo->membership == 'gold' ) {
                $pkgTitle = 'Gold';
            }
            if ( $userInfo->membership == 'platinum' ) {
                $pkgTitle = 'Platinum';
            }

            $membershipPkg = MembershipPackage::getSingePackageByName( $pkgTitle );
            $pkgs = unserialize( $membershipPkg->packages_config );

            $pkgs2 = array();
            foreach ($pkgs as $k => $v) {
                # CryptoTradingPackage
                if ( $v['package_category'] == 'cryptotrading' ) {
                    $pkgs2['cryptotrading']['package_id'] = $v['package_id'];
                    $pkgs2['cryptotrading']['package_name'] = CryptoTradingPackage::getPackageName( $v['package_id'] );
                    $pkgs2['cryptotrading']['amount_invested'] = $v['package_amount'];
                }

                # CryptoMiningPackage
                if ( $v['package_category'] == 'cryptomining' ) {
                    $pkgs2['cryptomining']['package_id'] = $v['package_id'];
                    $pkgs2['cryptomining']['package_name'] = CryptoMiningPackage::getPackageName( $v['package_id'] );
                    $pkgs2['cryptomining']['amount_invested'] = $v['package_amount'];
                }

                # CryptoShuffle
                if ( $v['package_category'] == 'raffle' ) {
                    $pkgs2['cryptoshuffle']['package_id'] = $v['package_id'];
                    $pkgs2['cryptoshuffle']['package_name'] = CryptoShufflePackage::getPackageName( $v['package_id'] );
                    $pkgs2['cryptoshuffle']['amount_invested'] = $v['package_amount'];
                }
            }

            //dd($pkgs2);
            $ret['mpkgs'] = $pkgs2;

            # get total referral credits
            $ret['refCredTotal'] = 0; 
            $refCredTotals = ReferralCredit::getRefCredTotals( $userId );
            if ( $refCredTotals ) {
                $ret['refCredTotal'] = $refCredTotals->membership;
            }


            # get membeship expiry date
            # ----------------------------
            $currentDate = new DateTime(date('Y-m-d'));
            $MembershipEndDate  = new DateTime($userInfo->membership_end_date);
            $dDiff = $currentDate->diff($MembershipEndDate);
            //$membershipDaysLeft = $dDiff->days;
            //$ret['membershipDaysLeft'] = $membershipDaysLeft;
            $membershipDaysLeft = (int)$dDiff->format("%r%a");
            $ret['membershipDaysLeft'] = $membershipDaysLeft;


            # update membership to FREE if member fails to pay 5 days after membership expiration
            # ----------------------------
            if ( $membershipDaysLeft < -5 ) {
                Member::updateMembershipLevel( $userId, 'free', $userInfo->membership_end_date );
            }


            return view('member.packages.membership.membership-list', $ret);

        }else{
            return view('member.packages.membership.membership-list-free', $ret);
        }
    }


    public function processOrder( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'msg2' => ''
        );

        # expected parameters
        $pid = 0;
        $fundAmount = 0;

        # initialize other important variables
        $transactionCode = LpjHelpers::generateTrCode();
        $minPurchase = 10;

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        # get package info
        $packageInfo = MembershipPackage::getSingePackage( $pid );
        // $ret['packageInfo'] = $packageInfo;

        # check if package is available
        if ( $packageInfo->status != 'available' ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Package is fully funded.';
        }

        # check amount to invest
        if ( $ret['isError'] == false ) {
            $fundAmount = $packageInfo->membership_fee;
        }

        # check bank balance
        if ( $ret['isError'] == false ) {
            $uBank = new UserBank( $userId );
            $userBank = $uBank->getUserBank();
            if ( $userBank->available_bal < $fundAmount ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Insuficient bank balance ( your bank balance is $'.$userBank->available_bal.').';
            }
        }

        if ( $ret['isError'] == false ) {
            $ret['msg'][] = 'Payment succcessfully processed. Please refresh this page.';


            # insert bank transaction
            # -------------------------
            $details = array(
                'package_id' => $packageInfo->id,
                'package_name' => $packageInfo->name,
                'package_amount' => $packageInfo->membership_fee,
                'package_category' => 'membership',
            );
            $params = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'amount' => $fundAmount,
                'description' => 'Membership Payment: '.$packageInfo->name,
                'type' => 'out',
                'section' => 'package-payment',
                'transaction_date' => date('Y-m-d H:i:s'),
                'status' => 'completed',
                'details' => $details
            );
            $uBank->insertBankTransaction( $params );


            # insert membership payment
            # -------------------------
            $params2 = array(
                'userId' => $userId,
                'packageId' => $packageInfo->id,
                'amount' => $fundAmount,
                'status' => 'completed',
                'transactionCode' => $transactionCode,
                'membershipLevel' => $packageInfo->name,
            );
            $res2 = MembershipPackage::insertMembershipPayment( $params2 );

            $investorMembershipLevel = strtolower($packageInfo->name);

            if ( $res2 ) {
                # update investor membership level
                # -------------------------
                Member::updateMembershipLevel( $userId, $investorMembershipLevel );

                # compute affililate commissions
                # -------------------------
                $params3 = array(
                    'investorUserId' => $userId,
                    'downlineUserId' => $userId,
                    'transactionCode' => $transactionCode,
                    'packageId' => $packageInfo->id,
                    'packageName' => $packageInfo->name,
                    'packageCat' => 'membership',
                    'fundId' => $res2['packageFundId'],
                    'amount' => $fundAmount,
                    'tierConfig' => unserialize( $packageInfo->tier_config )
                );
                ReferralCredit::computeUplineRC( $params3 );



                # invest on packages
                # -------------------------
                if ( $packageInfo->packages_config != '' ) {
                    $params = array(
                        'membershipPkgConfig' => $packageInfo->packages_config,
                        'transactionCode' => $transactionCode,
                        'userId' => $userId
                    );
                    $this->investOnpackages( $params );
                }


                # compute upline's power bonus - if upline and downline are platinum members
                # -------------------------
                $uplineInfo = MemberTier::getUpline( $userId );
                if ( $uplineInfo ) {
                    $uplineMembershipLevel = $uplineInfo->membership;

                    if ( $investorMembershipLevel == 'platinum' && $uplineMembershipLevel == 'platinum' ) {
                        $uplineReferralCnt = MembershipPowerBonus::getPlatinumRererralCount( $uplineInfo->id );

                        if ( $uplineReferralCnt ) {
                            $trCodes = unserialize( $uplineReferralCnt->tr_codes );
                            $bCount = $uplineReferralCnt->count;

                            $newCount = $bCount+1;
                            $trCodes[] = $transactionCode;

                            # give bonus every 3 platinum referral
                            if ( $newCount >= 3 ) {
                                MembershipPowerBonus::insertPowerBonus( $uplineInfo->id, $trCodes );
                                MembershipPowerBonus::deleteReferraCount( $uplineReferralCnt->id );

                            }else{
                                # update referral counter
                                MembershipPowerBonus::updateReferralCount( $uplineInfo->id, $trCodes, $newCount );
                            }

                        }else{
                            # insert platinum counter
                            MembershipPowerBonus::insertReferralCount( $uplineInfo->id, $transactionCode, 1 );
                        }
                    }
                }


                # update bank totals - package_purchase
                # --------------------------
                $newPacakgePurchaseVal = $userBank->package_purchase + $fundAmount;
                UserBank::updateBankSectionTotal( $userId, 'package_purchase', $newPacakgePurchaseVal );
                //UserBank::recomputeBankSectionTotal( $userId, 'package-payment', 'completed' );
                
                # update bank totals
                $userBank->package_purchase = $newPacakgePurchaseVal;
                UserBank::computeAvalableBal( $userId, $userBank );
            }

            
        }

        return response()->json($ret);
    }


    private function investOnpackages( $params ){
        $transactionCode = 0;
        $membershipPkgConfig = '';
        $userId = 0;

        if ( !empty($params) ) {
            extract($params);
        }


        $packagesConfigArr = unserialize( $membershipPkgConfig );
        $ret['packagesConfigArr'] = $packagesConfigArr;

        $packagesConfigArr2 = array();
        foreach ( $packagesConfigArr as $k => $v ) {
            $packagesConfigArr2[$v['package_category']]['id'] = $v['package_id'];
            $packagesConfigArr2[$v['package_category']]['amount'] = $v['package_amount'];
        }
        $ret['packagesConfigArr2'] = $packagesConfigArr2;

        # cryptotrading
        # -------------------------
        if ( isset($packagesConfigArr2['cryptotrading']) ) {
            $cryptotradingId = $packagesConfigArr2['cryptotrading']['id'];
            $cryptotradingAmount = (int)$packagesConfigArr2['cryptotrading']['amount'];

            $totalFunds = CryptoTradingPackage::getTotalFunds( $cryptotradingId );
            $ctPkgInfo = CryptoTradingPackage::getSingePackage( $cryptotradingId, array('name', 'duration', 'tier_config') );

            if ( $ctPkgInfo ) {
                $params5 = array(
                    'userId' => $userId,
                    'packageId' => $cryptotradingId,
                    'packageName' => $ctPkgInfo->name,
                    'packageCat' => 'cryptotrading',
                    'fundAmount' => $cryptotradingAmount,
                    'paymentStatus' => 'completed',
                    'transactionCode' => $transactionCode,
                    'duration' => $ctPkgInfo->duration,
                    'totalFunds' => $totalFunds,
                    'tierConfig' => $ctPkgInfo->tier_config,
                    'forMembership' => 1,
                    'newPackageStatus' => 'funded-active'
                );
                CryptoTradingPackage::onPaymentComplete( $params5 );
            }
        }

        # cryptomining
        # -------------------------
        if ( isset($packagesConfigArr2['cryptomining']) ) {
            $cryptoMiningId = $packagesConfigArr2['cryptomining']['id'];
            $cryptoMiningAmount = (int)$packagesConfigArr2['cryptomining']['amount'];

            $totalFunds = CryptoMiningPackage::getTotalFunds( $cryptoMiningId );
            $cmPkgInfo = CryptoMiningPackage::getSingePackage( $cryptoMiningId, array('name', 'duration', 'tier_config') );

            if ( $cmPkgInfo ) {
                $params5 = array(
                    'userId' => $userId,
                    'packageId' => $cryptoMiningId,
                    'packageName' => $cmPkgInfo->name,
                    'packageCat' => 'cryptomining',
                    'fundAmount' => $cryptoMiningAmount,
                    'paymentStatus' => 'completed',
                    'transactionCode' => $transactionCode,
                    'duration' => $cmPkgInfo->duration,
                    'totalFunds' => $totalFunds,
                    'tierConfig' => $cmPkgInfo->tier_config,
                    'forMembership' => 1,
                    'newPackageStatus' => 'funded-active'
                );
                CryptoMiningPackage::onPaymentComplete( $params5 );
            }
        }

        # cryptoshuffle
        # -------------------------
        if ( isset($packagesConfigArr2['raffle']) ) {
            $cryptoShuffleId = $packagesConfigArr2['raffle']['id'];
            $cryptoShuffleAmount = (int)$packagesConfigArr2['raffle']['amount'];

            $csPkgInfo = CryptoShufflePackage::getSingePackage( $cryptoShuffleId );

            if ( $csPkgInfo ) {
                $params6 = array(
                    'userId' => $userId,
                    'packageId' => $cryptoShuffleId,
                    'ticketQuantity' => $cryptoShuffleAmount,
                    'totalAmount' => $cryptoShuffleAmount,
                    'paymentStatus' => 'completed',
                    'transactionCode' => $transactionCode,
                    'ticketPrice' => $csPkgInfo->ticket_price,
                    'tierConfig' => $csPkgInfo->tier_config,
                    'soldTicketCnt' => $csPkgInfo->sold_ticket_cnt,
                    'ticketTotalCnt' => $csPkgInfo->ticket_total_cnt,
                    'packageName' => $csPkgInfo->name,
                );
                CryptoShufflePackage::onPaymentComplete( $params6 );
            }
        }
    }


    public function getPowerBonuses( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();


        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit
        );
        $ret['data'] = MembershipPowerBonus::getPowerBonuses( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $powBonuses = $ret['data']['list'];

        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $powBonuses->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $powBonuses->total() ) {
                $showingCount = $powBonuses->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$powBonuses->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        $ret2 = array(
            'powBonuses' => $ret['data']['list']
        );

        $ret['powBonusTotal'] = LpjHelpers::amt2( MembershipPowerBonus::getPowerBonusSum( $userId ) );

        # apply power bonus item template
        $ret['returnHTML'] = view('member.packages.membership.parts.power-bonus-list-item')->with($ret2)->render();
        
        return response()->json($ret);
    }


    public function powerBonusPushToBank( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $powBonusId = '';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        if ( empty( $powBonusId ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing Parameters.';
        }

        # check if not yet withdrawn
        $powBonus = false;
        if ( $ret['isError'] == false ) {
            $powBonus = MembershipPowerBonus::getSinglePowerBonus( $powBonusId );

            if ( !$powBonus ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Power bonus not found.';
            }
        }

        if ( $ret['isError'] == false ) {
            if ( $powBonus->status == 'withdrawn' ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'This power bonus is already withdrawn.';
            }
        }

        if ( $ret['isError'] == false ) {
            # update pwer bonus status
            MembershipPowerBonus::updatePowBonusStatus( $powBonusId, 'withdrawn' );

            # get power bonus total
            $pBonusTotalWithdrawn = MembershipPowerBonus::getPowerBonusSumByStatus( $userId, 'withdrawn' );
            $pBonusTotalPending = MembershipPowerBonus::getPowerBonusSumByStatus( $userId, 'completed' );

            $ret['pBonusTotalWithdrawn'] =  LpjHelpers::amt2( $pBonusTotalWithdrawn );
            $ret['pBonusTotalPending'] =  LpjHelpers::amt2( $pBonusTotalPending );
            $ret['pBonusTotal'] =  LpjHelpers::amt2( $pBonusTotalWithdrawn + $pBonusTotalPending );
            $powerBonusOverallTotal = $pBonusTotalWithdrawn + $pBonusTotalPending;

            # insert package earning
            $pkgProfitParams = array(
                'row_id' => $powBonusId,
                'user_id' => $userId,
                'profit_id' => $powBonusId,
                'package_id' => 0,
                'package_cat' => 'membership',
                'type' => 'power-bonus',
                'amount' => $powBonus->amount,
                'withdraw_date' => date('Y-m-d H:i:s'),
                'data' => $powBonus
            );
            PackageEarning::insert( $pkgProfitParams );



            # -----------------------------------------------------
            # update package_earning_totals.membership
            PackageEarning::updatePkgCatTotalEarning( $userId, 'membership', $powerBonusOverallTotal );
            # update overall total
            $pkgEarningOverallTotal = PackageEarning::updatePackageEarningTotals( $userId );

            # update user bank - package_earning column
            UserBank::updateBankSectionTotal( $userId, 'package_earning', $pkgEarningOverallTotal );
            # update available balance
            UserBank::computeAvalableBal( $userId );

        }

        return response()->json($ret);
    }
} 
