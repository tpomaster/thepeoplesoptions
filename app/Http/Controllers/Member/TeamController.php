<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;
use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\MemberTier;
use App\Helpers\LpjHelpers;
// use App\Helpers\LpjPagination;


class TeamController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:member');
    }

    public function index( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $userId = Auth::id();
        $user = Auth::user();
        $parentUsername = 'litopj2';


        if ( $request->input('sponsor') != '' ) {
            $parentUsername = LpjHelpers::fss( $request->input('sponsor') );

        }else{
            $parentUsername = $user->username;
        }

        $member = new Member();
        $member->setUserId( $userId );
        $userInfo = $member->getSingle( 'id', $userId );

        $ret['user'] = $userInfo;
        $ret['parentUsername'] = $parentUsername;
        $ret['qlimit'] = 10;


        # get upline info
        $ret['sponsorInfo'] = false;
        $uplineInfo = MemberTier::getUpline( $userId );
        if ( $uplineInfo ) {
            $ret['sponsorInfo'] = $uplineInfo;
        }
        


        return view('member.team', $ret);
    }

    # this uses LpjPaginator
    public function getTeam( Request $request )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );
        $userId = Auth::id();

        # for tree ui
        $parentId = 'litopj2';
        $nextLevel = 2;
        $currTierLvl = 1;

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortBy2 = 'created_at';
        $sortOrder = 'DESC';

        # check if user is loggedin
        if ( !$userId ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Please Login.';
        }

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $nextLevel = (int)$currTierLvl + 1;
        }

        # get sponsor info
        if ( $ret['isError'] == false ) {
            $spnsorData = new Member();
            $sponsorUserInfo = $spnsorData->getSingle('username', $parentId);

            if ( !$sponsorUserInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Sponsor Info not found.';
            }
        }

        # get referrals
        if ( $ret['isError'] == false ) {
            $qryParams = array(
                'parentId' => $sponsorUserInfo->id,
                'qpage' => $qpage,
                'qlimit' => $qlimit,
                'sortBy' => $sortBy,
                'sortBy2' => $sortBy2,
                'sortOrder' => $sortOrder,
            );

            $myteam = new MemberTier;
            $ref = $myteam->getReferrals( $qryParams );
            $ret['referrals'] = $ref;

            $referrals = $ref['referrals'];
            $refCount = $ref['refCount'];

            if ( !$referrals || !isset($referrals[0]) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'No Referrals.';
            }
        }

        # generate referral html
        if ( $ret['isError'] == false ) {
            $refHtml = '';

            # pagination html
            $urlParams = $qryParams;
            $urlParams['parentId'] = $parentId;
            $urlParams['currTierLvl'] = $currTierLvl;

            $p = (new Lpjpagination( $qpage, $refCount, $urlParams ));
            $p->setRPP( $qlimit );
            $p->setTarget( url('/').'/member/team/' );
            $paginationHtml = $p->parse();

            if ( $paginationHtml ) {
                $showingCount = $qpage*$qlimit;
                if ( $showingCount > $refCount ) {
                    $showingCount = $refCount;
                }
                $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$refCount.'</span>';
                $paginationHtml = '<div class="tierPaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
            }
            // $ret['paginationHtml'] = $paginationHtml;
            // $ret['qpage'] = $qpage;



            # generate tier html
            $tierLvlClass = $this->getTierLevelClass( $currTierLvl );

            foreach( $referrals as $k => $v ){
                //Member::updateReferralCount2( $v->id );

                # show indicator that the user have referrals
                $has_referral_arrow = '';
                if ( $v->direct_ref_cnt >= 1 ) {
                    $has_referral_arrow = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
                }

                # if level 6, restart tree
                $btnClass = 'getTeamBtn';
                $btnLink = '#';
                if ( $currTierLvl == 6 ) {
                    $btnClass = 'getTeamBtnNew';
                    $btnLink = url('/').'/member/team/?sponsor='.$v->username;
                }

                $refHtml .= '
                <div class="clearfix '.$tierLvlClass.' tierWrap tierWrap'.$v->username.'">
                    <a href="'.$btnLink.'" class="tableBar click '.$btnClass.' tableCol" data-parentid="'.$v->username.'" data-currtierlvl="'.$nextLevel.'">
                        '.$has_referral_arrow.'
                        <span>Tier '.$currTierLvl.'</span>
                    </a>
                    <div class="tableUserName tableCol">'.$v->username.'</div>
                    <div class="tableFirstName tableCol">'.$v->firstname.'</div>
                    <div class="tableLastName tableCol">'.$v->lastname.'</div>
                    <div class="tableDateReg tableCol">'.$v->created_at.'</div>
                    <div class="tableMembership tableCol">'.$v->membership.'</div>
                    <div class="clear"></div>
                    <div class="childTierWrap"></div>
                </div>';
            }

            $ret['msg'][] = $refHtml.$paginationHtml;
        }

        return response()->json($ret);
    }

    # this uses laravel paginationi
    public function getTeam2( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );
        $userId = Auth::id();

        # for tree ui
        $parentId = 'litopj2';
        $nextLevel = 2;
        $currTierLvl = 1;
        $page = 1;

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortBy2 = 'created_at';
        $sortOrder = 'DESC';

        # check if user is loggedin
        if ( !$userId ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Please Login.';
        }

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $nextLevel = (int)$currTierLvl + 1;
            $qpage = $page;
        }

        # get sponsor info
        if ( $ret['isError'] == false ) {
            $spnsorData = new Member();
            $sponsorUserInfo = $spnsorData->getSingle('username', $parentId);

            if ( !$sponsorUserInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Sponsor Info not found.';
            }
        }

        # get referrals
        if ( $ret['isError'] == false ) {
            $qryParams = array(
                'currTierLvl' => $currTierLvl,
                'sponsorUsername' => $sponsorUserInfo->username,
                'parentId' => $sponsorUserInfo->id,
                'qpage' => $qpage,
                'qlimit' => $qlimit,
                'sortBy' => $sortBy,
                'sortBy2' => $sortBy2,
                'sortOrder' => $sortOrder,
            );

            $myteam = new MemberTier;
            $ref = $myteam->getReferrals2( $qryParams );
            //dd($ref);

            $referrals = $ref['referrals'];

            # pagination html
            $paginationHtml = $ref['pageLinks'];
            $ret['ref'] = $ref;

            if ( $referrals->total() >  $qlimit ) {
                $showingCount = $qpage*$qlimit;
                if ( $showingCount > $referrals->total() ) {
                    $showingCount = $referrals->total();
                }
                $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$referrals->total().'</span>';
                $paginationHtml = '<div class="tierPaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
            }

            if ( !$referrals || !isset($referrals[0]) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'No Referrals.';
            }
        }

        # generate referral html
        if ( $ret['isError'] == false ) {
            $refHtml = '';

            # generate tier html
            $tierLvlClass = $this->getTierLevelClass( $currTierLvl );

            foreach( $referrals as $k => $v ){
                //Member::updateReferralCount2( $v->id );

                # show indicator that the user have referrals
                $has_referral_arrow = '';
                if ( $v->direct_ref_cnt >= 1 ) {
                    $has_referral_arrow = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
                }

                # if level 6, restart tree
                $btnClass = 'getTeamBtn';
                $btnLink = '#';
                if ( $currTierLvl == 6 ) {
                    $btnClass = 'getTeamBtnNew';
                    $btnLink = url('/').'/member/team/?sponsor='.$v->username;
                }

                $refHtml .= '
                <div class="clearfix '.$tierLvlClass.' tierWrap tierWrap'.$v->username.'">
                    <a href="'.$btnLink.'" class="tableBar click '.$btnClass.' tableCol" data-parentid="'.$v->username.'" data-currtierlvl="'.$nextLevel.'">
                        '.$has_referral_arrow.'
                        <span>Tier '.$currTierLvl.'</span>
                    </a>
                    <div class="tableUserName tableCol">'.$v->username.'</div>
                    <div class="tableFirstName tableCol">'.$v->firstname.'</div>
                    <div class="tableLastName tableCol">'.$v->lastname.'</div>
                    <div class="tableDateReg tableCol">'.$v->created_at.'</div>
                    <div class="tableMembership tableCol">'.$v->membership.'</div>
                    <div class="clear"></div>
                    <div class="childTierWrap"></div>
                </div>';
            }

            $ret['msg'][] = $refHtml.$paginationHtml;
        }


        return response()->json($ret);
    }

    private function getTierLevelClass( $tierLevel ){
        $tierLevelClass = 'LevelOne';
        if ( $tierLevel == 1 ) {
            $tierLevelClass = 'levelOne';
        }
        if ( $tierLevel == 2 ) {
            $tierLevelClass = 'levelTwo';
        }
        if ( $tierLevel == 3 ) {
            $tierLevelClass = 'levelThree';
        }
        if ( $tierLevel == 4 ) {
            $tierLevelClass = 'levelFour';
        }
        if ( $tierLevel == 5 ) {
            $tierLevelClass = 'levelFive';
        }
        if ( $tierLevel == 6 ) {
            $tierLevelClass = 'levelSix';
        }

        return $tierLevelClass;
    }
}
