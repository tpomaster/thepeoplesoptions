<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\Controller;

use App\Helpers\LpjHelpers;
use App\Helpers\CoinPaymentsPG;
use App\Model\Member\DbLogger;

use App\Model\Member\UserBank;
use App\Model\Member\UserbankFund;

class ProcessIPNController extends Controller {

    public function coinpaymentsIPN( Request $request ) {
        $_POST = LpjHelpers::fss($_POST);

        $ret = array(
            'isError' => false,
            'msg' => array('test'),
            'post' => $_POST
        );

        //pr($arr); 

        $params = array(
            'gateway' => 'coinpayments',
            'trCode' => $_POST['invoice'],
            'gTrCode' => $_POST['txn_id'],
            'status' => $_POST['status'],
            'ipn' => $_POST
        );
        DbLogger::logIPN( $params );

        $ret = CoinPaymentsPG::processIPN( $_POST, false );

        if ( $ret['isError'] == false ) {
            $paymentTrnx = $ret['paymentTrnx'];
            $ipnData = $ret['ipnData'];

            $ret['msg'] = 'Add Fund Confirmed';

            # update bank transaction
            UserbankFund::updateTransactionStatusStatic( $paymentTrnx->transaction_code, $ret['status'] );
            UserBank::updateTransactionStatusStatic( $paymentTrnx->transaction_code, $ret['status'] );

            # update bank stats
            //Bank::update_bank_totals( $paymentTrnx->user_id );
        }

        return response()->json($ret);
    }

    public function coinpaymentsIpnTest(){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        $str = 'a:27:{s:11:"ipn_version";s:3:"1.0";s:6:"ipn_id";s:32:"92f7380c6f80c68039798a4dbf2ad80e";s:8:"ipn_mode";s:4:"hmac";s:8:"merchant";s:32:"b108fa9d023bf3368a89d1ef6655675f";s:8:"ipn_type";s:6:"simple";s:6:"txn_id";s:26:"CPAI2LK0WZMIB4USMO3JMV7QCA";s:6:"status";s:3:"100";s:11:"status_text";s:8:"Complete";s:9:"currency1";s:3:"USD";s:9:"currency2";s:3:"BTC";s:7:"amount1";s:1:"1";s:7:"amount2";s:7:"0.00165";s:8:"subtotal";s:1:"1";s:8:"shipping";s:1:"0";s:3:"tax";s:1:"0";s:3:"fee";s:6:"1.0E-5";s:3:"net";s:7:"0.00164";s:11:"item_amount";s:1:"1";s:9:"item_name";s:8:"TPO Fund";s:10:"first_name";s:4:"lito";s:9:"last_name";s:2:"pj";s:5:"email";s:16:"litopj@yahoo.com";s:11:"item_number";s:1:"4";s:7:"invoice";s:18:"1474955708srCzyykJ";s:6:"custom";s:3:"114";s:15:"received_amount";s:7:"0.00165";s:17:"received_confirms";s:1:"2";}';

        $arr = unserialize($str);
        $ret['ipn'] = $arr;

        return view('member.coinpayments-ipn-test', $ret );
    }
} 
