<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\InvestmentLoop;
use App\Model\Member\CryptoShufflePackage;
use App\Model\Member\UserBank;

use App\Helpers\LpjHelpers;

class InvestmentLoopController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    public function index( Request $request )
    {
    	$ret = array();

        $userId = Auth::id();

        // $checkBalance = InvestmentLoop::getUsersPayment( $userId );

    	return view('member.investment-loop');
    }

    public function getUsersPayment( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;


        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'userId' => $userId,
            'qpage' => $qpage,
            'qlimit' => $qlimit,
        );
        $ret['data'] = InvestmentLoop::getUsersPayment( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $InvestLoop = $ret['data']['list'];


        // $ret = array(
        // 	'returnHTML' => $ret['data']['list'],
        // );
        // return response()->json($ret);


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $InvestLoop->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $InvestLoop->total() ) {
                $showingCount = $InvestLoop->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$InvestLoop->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;


        $ret2 = array(
            'InvestLoop' => $ret['data']['list']
        );


        # referral IDs
        $investorIDs = array();
        foreach ($ret['data']['list'] as $k => $item) {
            if ( !in_array( $item->user_id, $investorIDs)) {
                $investorIDs[] = $item->user_id;

                $cryptoShuffleId = 14;
                $cryptoShuffleAmount = 100;

                $uBank = new UserBank( $item->user_id );
	            $userBank = $uBank->getUserBank();
	            if ( $userBank->available_bal < $cryptoShuffleAmount ) {
	                $ret['isError'] = true;
	                $ret['msg'][] = 'Insuficient bank balance ( your bank balance is $'.$userBank->available_bal.').';
	            } else {
	            	$transactionCode = LpjHelpers::generateTrCode();

	                # cryptoshuffle
	                # -------------------------

	                $packageInfo = CryptoShufflePackage::getSingePackage( $cryptoShuffleId );

	                $params6 = array(
	                    'userId' => $item->user_id,
	                    'packageId' => 14,
	                    'ticketQuantity' => 1000,
	                    'totalAmount' => $cryptoShuffleAmount,
	                    'paymentStatus' => 'completed',
	                    'transactionCode' => $transactionCode,
	                    'ticketPrice' => $packageInfo->ticket_price,
	                    'tierConfig' => $packageInfo->tier_config,
	                    'soldTicketCnt' => $packageInfo->sold_ticket_cnt,
	                    'ticketTotalCnt' => $packageInfo->ticket_total_cnt,
	                    'packageName' => $packageInfo->name,
	                );
	                CryptoShufflePackage::onPaymentComplete( $params6 );


	                # insert bank transaction
		            # -------------------------
		            $details = array(
		                'package_id' => $packageInfo->id,
		                'package_name' => $packageInfo->name,
		                'package_amount' => $packageInfo->ticket_price,
		                'package_category' => 'crytoshuffle',
		            );
		            $params = array(
		                'user_id' => $item->user_id,
		                'transaction_code' => $transactionCode,
		                'amount' => $cryptoShuffleAmount,
		                'description' => 'Package Payment: '.$packageInfo->name,
		                'type' => 'out',
		                'section' => 'package-payment',
		                'transaction_date' => date('Y-m-d H:i:s'),
		                'status' => 'completed',
		                'details' => $details
		            );
		            $uBank->insertBankTransaction( $params );

		            # update bank totals - package_purchase
	                # --------------------------
	                $newPacakgePurchaseVal = $userBank->package_purchase + $cryptoShuffleAmount;
	                UserBank::updateBankSectionTotal( $item->user_id, 'package_purchase', $newPacakgePurchaseVal );
	                //UserBank::recomputeBankSectionTotal( $userId, 'package-payment', 'completed' );
	                
	                # update bank totals
	                $userBank->package_purchase = $newPacakgePurchaseVal;
	                UserBank::computeAvalableBal( $item->user_id, $userBank );
	            }
            }
        }
        $ret['investorIDs'] = $investorIDs;


        # get referral usernames
        $investors = Member::getUsersByIds( $investorIDs );
        $ret['investors'] = $investors;

        $investorsName = array();
        if ( $investors ) {
            foreach ($investors as $key => $ref) {
                $investorsName[$ref->id]['username'] = $ref->username;
                $investorsName[$ref->id]['membership'] = $ref->membership;
            }
        }
        $ret['investorsName'] = $investorsName;
        $ret2['investorsName'] = $investorsName;


        $ret['returnHTML'] = view('member.investment-loop-list')->with($ret2)->render();
       
        return response()->json($ret);
    }
}
