<?php
namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\Http\Controllers\Controller;
use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Helpers\LpjHelpers;
use App\Helpers\TpoMarketingAPI;

use App\Mail\DemoMail;

use App\Helpers\Sendpulse\ApiClient;
use App\Helpers\Sendpulse\Storage\FileStorage;

use App\Helpers\BitPalAPI;


class MyAccountController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth:member');
    }

    public function index() 
    {
        $userId = Auth::id();
        $user = Auth::user();

        $userM = new UserMeta( $userId );
        $userMeta = $userM->getSingle();
        //dd( $user->membership );

        # create user meta
        # ---------------------
        if ( !$userMeta ) {
            $newUserMeta = array(
                'phone' => '123'
            );
            $userM->addNew( $newUserMeta );
            $userMeta = $userM->getSingle();
        }
        //dd($userMeta);


        # TEMPORARY
        # last name
        # ---------------------
        if ( empty($user->lastname) ) {
            $url = 'https://thepeoplesoptions.com/tpo/check-user/';
            $params = array(
                'client_id2' => 'xlitopjx',
                'username' => $user->username
            );
            $lNameCheck = LpjHelpers::curlGet( $url, $params );
            $lNameCheckResult = json_decode($lNameCheck);

            if ( $lNameCheckResult->is_error == false ) {
                $updUser = array(
                    'lastname' => $lNameCheckResult->lastname
                );
                $updWhere = array(
                    'username' => $user->username,
                );
                Member::updateUserWhere( $updUser, $updWhere );

                $user->lastname = $lNameCheckResult->lastname;
            }
        } 


        $ret = array(
            'user' => $user,
            'user_meta' => $userMeta,
            'aff_url' => url('/').'/member/'.$user->username.'/register',
        );

        # BITPAL
        $bitpalClientID = BitPalAPI::config('client_id');
        $ret['bitpalAuthUrl'] = BitPalAPI::config('api_home_url').'/#/oauth/authorize?client_id='.$bitpalClientID.'&response_type=code&redirect_uri='.url('/').'/member/oauth-code&state=btsaccount';


        # TPO MARKETING
        if ( in_array($user->membership, array('silver', 'gold', 'platinum')) && $userMeta->tpo_marketing_code == '' ) {
            $tpoMarketingCode = LpjHelpers::generateTrCode(12);

            $userData = array(
                'role' => 'member',
                'username' => $user->username,
                'membership' => $user->membership,
                'password' => $tpoMarketingCode,

                'email' => $user->email,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'phone' => $userMeta->phone,
                'address' => $userMeta->address,
                'city' => $userMeta->city,
                'state' => $userMeta->state,
                'zipcode' => $userMeta->zip,
                'country' => $userMeta->country,
            );
            $ret2 = TpoMarketingAPI::createAccount($userData);

            //dd($ret2);
            if ( $ret2 && $ret2->is_error == false ) {
                # update user_meta : tpo_marketing_code
                $updData = array(
                    'tpo_marketing_code' => $tpoMarketingCode,
                );
                UserMeta::updateUserMeta( $userId, $updData );
            }
        }

        # TEMPORARY
        # BTS ACCOUNT 
        if ( $user->bitshares_name == '' ) {
            if ( $userMeta->bitpal_default_bts_account != '' ) {
                $user->bitshares_name = $userMeta->bitpal_default_bts_account;

                $updData = array(
                    'bitshares_name' => $userMeta->bitpal_default_bts_account,
                );
                Member::updateUserField( $userId, $updData );
            }
        }
        

        return view('member.my-account', $ret);
    }


    public function updateMyAccount(  Request $request  ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $userId = Auth::id();

        $member = new Member();
        $member->setUserId( $userId );
        $userInfo = $member->getSingle( 'id', $userId );

        $userMeta = new UserMeta( $userId );

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        # update personal info
        if ( $action == 'update_personal_info') {
            $updUserMeta = array(
                'phone' => $phone,
                'address' => $address,
                'city' => $city,
                'state' => $state,
                'zip' => $zip,
                'country' => $country
            );
            $userMeta->update( $updUserMeta );

            $updUser = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
            );
            $member->updateUser( $updUser );


            # update TPO Marketing
            if ( in_array($userInfo->membership, array('silver', 'gold', 'platinum')) ) {
                $userMeta2 = UserMeta::getUserMeta( $userId, 'tpo_marketing_code' );

                $tpoMarketingCode = $userMeta2->tpo_marketing_code;

                if ( empty($tpoMarketingCode) ) {
                    $tpoMarketingCode = LpjHelpers::generateTrCode(12);
                }

                $tpoMarketingUserInfo = array(
                    'username' => $userInfo->username,
                    'password' => $tpoMarketingCode,
                    'email' => $userInfo->email,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'phone' => $phone,
                    'address' => $address,
                    'city' => $city,
                    'state' => $state,
                    'zipcode' => $zip,
                    'country' => $country,
                );
                $ret2 = TpoMarketingAPI::updatePersonalInfo($tpoMarketingUserInfo);
                //$ret['tpoMupdate'] = $ret2;

                # update user meta
                $updUserMeta = array(
                    'tpo_marketing_code' => $tpoMarketingCode
                );
                $userMeta->update( $updUserMeta );
            }
                

            $ret['msg'][] = 'Personal Information Succefully Updated.';
        }

        # update login info
        if ( $action == 'update_account_info') {
            $updUser = array();

            # validation
            /* UNCOMMENT THIS TO MAKE USERNAME EDITABLE
            if ( $userInfo->username != $username ) {
                # validate username format
                $ret = LpjHelpers::lpjValidateUsername( $username, $ret );

                # check if unique
                if ( !$ret['isError'] ) {
                    if ( $member->checkDuplicateUsername($username) ) {
                        $ret['msg'][] = 'Username is already taken.';
                        $ret['isError'] = true;
                    }
                }

                if ( !$ret['isError'] ) {
                    if ( $userInfo->membership != 'free' ) {
                        $ret['msg'][] = 'Only members with FREE membership can their username. Please email us if you want to change your email.';
                        $ret['isError'] = true;
                    }
                }

                if ( !$ret['isError'] ) {
                    $updUser['username'] = $username;
                }
            }
            */

            if ( isset($password) && $password != '' ) {
                # check password confirmation
                if ( $password != $password2 ) {
                    $ret['msg'][] = 'Password did not match.';
                    $ret['isError'] = true;
                }

                if ( !$ret['isError'] ) {
                    $ret = LpjHelpers::lpjCheckPasswordStrength( $password, $ret );
                }

                if ( !$ret['isError'] ) {
                    $updUser['password'] = Hash::make($password);
                }
                
            }

            if ( !$ret['isError'] ) {
                $member->updateUser( $updUser );
                $ret['msg'][] = 'Login Information Succefully Updated.';
            }
            
        }

        return response()->json($ret);
    }

    public function updateBtsAccount( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $bitshares_account = '';

        $userId = Auth::id();

        $member = new Member();
        $member->setUserId( $userId );
        $userInfo = $member->getSingle( 'id', $userId );

        $userMeta = new UserMeta( $userId );

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        # update bitshares account
        if ( empty($bitshares_account) ) {
            $ret['msg'][] = 'Please enter your bitshares account name.';
            $ret['isError'] = true;
        }

        if ( !$ret['isError'] ) {
            $updUser = array(
                'bitshares_name' => $bitshares_account
            );
            $member->updateUser( $updUser );

            $ret['msg'][] = 'Bitshares Account Succefully Updated.';
        }

        return response()->json($ret);
    }

    public function sendTestEmail(){
        $fileStorage = new FileStorage();
        $apiUserId = config('sendpulse.sendpulse_api_user_id');
        $apiSecret = config('sendpulse.sendpulse_api_secret');

        $SPApiClient = new ApiClient($apiUserId, $apiSecret, $fileStorage);

        $email = array(
            'html' => '<p>Hello!</p>',
            'text' => 'text',
            'subject' => 'TPO Test Email',
            'from' => array(
                'name' => 'TPO Tech',
                'email' => 'tech@thepeoplesoptions.com',
            ),
            'to' => array(
                array(
                    'name' => 'GetNada',
                    'email' => 'ceco@duck2.club',
                ),
            ),
            'bcc' => array(
                array(
                    'name' => 'LitoPJ',
                    'email' => 'litopj@yahoo.com',
                ),
            ),
            // 'attachments' => array(
            //     'file.txt' => file_get_contents(PATH_TO_ATTACH_FILE),
            // ),
        );

        $result = $SPApiClient->smtpSendMail($email);
        dd($result);
    }

    public function tpoMarketingLogin( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'redirectUrl' => ''
        );

        $userId = Auth::id();
        $userInfo = Auth::user();
        $userMeta = UserMeta::getUserMeta( $userId, 'tpo_marketing_code' );

        $formData = $request->all();
        $formData = LpjHelpers::fss( $formData );
        extract( $formData );

        # check membership
        if ( !in_array($userInfo->membership, array('silver', 'gold', 'platinum')) ) {
            $ret['msg'][] = 'Please join one of ouor membership packages to access TPO Marketing.';
            $ret['isError'] = true;
        }

        # login to tpo marketing site
        if ( !$ret['isError'] ) {
            $loginData = array(
                'username' => $userInfo->username,
                'password' => $userMeta->tpo_marketing_code,
                'membership' => $userInfo->membership
            );
            $ret2 = TpoMarketingAPI::login( $loginData );
            //$ret['ret2'] = $ret2;

            if ( !$ret2 ) {
                $ret['msg'][] = 'TPO Marketing API Connection Error.';
                $ret['isError'] = true;
            }
            
            if ( $ret2 ) {
                if ( isset($ret2->is_error) && $ret2->is_error == true ) {
                    $ret['msg'][] = 'Failed to login to TPO Marketing.';
                    $ret['isError'] = true;

                }else{
                    $ret['msg'][] = 'Successfully LoggedIn! Redirecting, Please wait...';
                    $ret['redirectUrl'] = TpoMarketingAPI::config('api_url').'?login='.$ret2->login_token;
                }
            }
            
        }

        return response()->json($ret);
    }
}