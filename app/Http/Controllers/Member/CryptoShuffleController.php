<?php
namespace App\Http\Controllers\Member;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Model\Member\Member;
use App\Model\Member\UserMeta;
use App\Model\Member\UserBank;
use App\Model\Member\CryptoShufflePackage;
//use App\Model\Member\CryptoTradingPackageTrade;
use App\Model\Member\ReferralCredit;

use App\Helpers\LpjHelpers;

class CryptoShuffleController extends Controller {

    public function __construct()  {
        $this->middleware('auth:member');
    }

    public function index( Request $request ) {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => '',
            'myPackages' => '',
            'pkg_details_url' => url('/').'/member/packages/crypto-trading/details/'
        );

        $ret['pageTitle2'] = 'All Crypto Trading Packages';
        
        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-shuffle.cs-packages-list', $ret);
    }

    public function viewPackagesByStatus( $status ) {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => $status,
            'myPackages' => '',
            'pkg_details_url' => url('/').'/member/packages/crypto-trading/details/'
        );

        $ret['pageTitle2'] = strtoupper( $status ).' Crypto Trading Packages';
        
        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-trading.ct-packages-list', $ret);
    }

    public function myPackages( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'status' => 'funded-active',
            'myPackages' => 'yes',
            'pkg_details_url' => url('/').'/member/packages/crypto-trading/details/'
        );

        $ret['pageTitle2'] = 'Crypto Trading: My Funded Packages';

        $userId = Auth::id();
        $user = Auth::user();

        return view('member.packages.crypto-trading.ct-my-packages', $ret);
    }

    public function viewPackagesAjax( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        $status = '';
        $myPackages = '';
        $packageIds = false;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        # my packages params
        if ( $myPackages != '' ) {
            $trList = CryptoShufflePackage::getUserFundedPackages( $userId );
            $myPkgsIds = array();

            if ( $trList ) {
                foreach ($trList as $k => $v) {
                    $myPkgsIds[] = $v->package_id;
                }
            }

            if ( !empty($myPkgsIds) ) {
                $packageIds = $myPkgsIds;
            }

            $ret['packageIds'] = $packageIds;
        }

        if ( $myPackages != '' && empty($packageIds) ) {
            $ret['packages'] = array();
            $ret['pageLinks'] = array();

            $trList = array();

            # pagination html
            $paginationHtml = $ret['pageLinks'];
            $ret['paginationHtml'] = $paginationHtml;

            //dd($ret['packages']);
            $ret['returnHTML'] = "You haven't funded any package.";

        }else{
            $params = array(
                'status' => $status,
                'packageIds' => $packageIds,
                'qpage' => $qpage,
                'qlimit' => $qlimit,
            );
            $ret['packages'] = CryptoShufflePackage::getPackages( $params );
            $ret['pageLinks'] = $ret['packages']['pageLinks'];

            $packages = $ret['packages']['list'];

            $ret['sqlWhere'] = $ret['packages']['sqlWhere'];
            

            # pagination html
            $paginationHtml = $ret['pageLinks'];

            if ( $packages->total() >  $qlimit ) {
                $showingCount = $qpage*$qlimit;
                if ( $showingCount > $packages->total() ) {
                    $showingCount = $packages->total();
                }
                $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$packages->total().'</span>';
                $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
            }
            $ret['paginationHtml'] = $paginationHtml;

            $ret2 = array(
                'packages' => $ret['packages']['list']
            );

            if ( $packages->total() > 0 ) {
                $ret['returnHTML'] = view('member.packages.crypto-shuffle.cs-package-summary')->with($ret2)->render();

            }else{
                $ret['returnHTML'] = "NO RESULT... ";
            }
        }

        return response()->json($ret);
    }


    public function viewPackageDetails( $packageId ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        # -------------------------------
        $userId = Auth::id();
        $user = Auth::user();

        # get users membership level
        $membershipLevel = $user->membership;


        # get package info
        # -------------------------------
        $packageInfo = CryptoShufflePackage::getSingePackage( $packageId );

        # end if package info is not found
        if ( !$packageInfo ) {
            return view('member.packages.not-found', $ret);
        }

        $packageInfo->tier_config = unserialize($packageInfo->tier_config);


        # package stats
        # -------------------------------
        // $soldTicketsCount = CryptoShufflePackage::countSoldTickets( $packageId );
        $soldTicketsCount = $packageInfo->sold_ticket_cnt;
        
        //$packageInfo->sold_ticket_cnt = $soldTicketsCount;

        $soldPercent = ( (float)$soldTicketsCount / (float)$packageInfo->ticket_total_cnt )*100;
        $packageInfo->sold_percent = $soldPercent;

        # available for fund
        $packageInfo->available_tickets_count =  (float)$packageInfo->ticket_total_cnt - (float)$soldTicketsCount;


        if ( $packageInfo->available_tickets_count <= 0 && $packageInfo->status == 'available') {
            $params3 = array(
                'packageId' => $packageId,
                'status' => 'active',
            );
            CryptoShufflePackage::updatePackageStatus( $params3 );
        }


        # get puchased tickets
        # -------------------------------
        $myPurchasedTickets = CryptoShufflePackage::getUserPurchasedTickets( $packageInfo->id, $userId );
        if ( $myPurchasedTickets ) {
            $ret['myPurchasedTickets'] = $myPurchasedTickets;
        }
        $ret['pkg'] = $packageInfo;
        $ret['myPurchasedTickets'] = $myPurchasedTickets;


        # earning stats
        # -------------------------------
        $purchasedTickets = 100;
        $referralCredits = 0;
        $winningTickets = 0;


        # get user bank
        # -------------------------------
        $uBank = new UserBank( $userId );
        $userBank = $uBank->getUserBank();
        $ret['userBank'] = $userBank;


        $ret['queueCode'] = LpjHelpers::generateTrCode();

        //dd( $ret );
        return view('member.packages.crypto-shuffle.cs-package-details', $ret);
    }


    public function queueOrder( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'msg2' => ''
        );

        # variables - default values
        $pid = 0; 
        $totalAmount = 0;
        $itemPrice = 0;
        $isTicketShort = false;
        $ticketQuantity = 0;
        $transactionCode = LpjHelpers::generateTrCode();
        $minTicket = 10;
        $maxTicket = 5000;
        $queueCode = '';

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        
        # validations
        # -------------------------------
        if ( $pid == '' || $pid == 0 ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Invalid Pacakge ID.';
        }

        # max ticket per order
        # -------------------------------
        if ( $ticketQuantity > $maxTicket ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Maximum ticket per order is '.$maxTicket.'.';
        }

        # min ticket per order
        # -------------------------------
        if ( $ticketQuantity < $minTicket ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Minimum ticket per order is '.$minTicket.'.';
        }

        # get package info
        # -------------------------------
        if ( $ret['isError'] == false ) {
            $packageInfo = CryptoShufflePackage::getSingePackage( $pid );
            //$ret['packageInfo'] = $packageInfo;

            if ( !$packageInfo ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Package not found.';

            }else{
                # compute order total amount
                $totalAmount = $packageInfo->ticket_price * $ticketQuantity;
                $ret['totalAmount'] = $totalAmount;
            }
        }

        # validate available tickets count
        # -------------------------------
        if ( $ret['isError'] == false ) {
            $ticketTotalCnt = $packageInfo->ticket_total_cnt;
            $availableTicketCnt = $ticketTotalCnt - $packageInfo->sold_ticket_cnt;

            if ( $availableTicketCnt < $ticketQuantity ) {
                $isTicketShort = true;
                //$ret['isError'] = true;
                //$ret['msg'][] = 'Only '.$availableTicketCnt.' Tickets are available.';

                $ticketQuantity = $availableTicketCnt;
            }
        }

        # validate bank balance
        # -------------------------------
        if ( $ret['isError'] == false ) {
            $uBank = new UserBank( $userId );
            $userBank = $uBank->getUserBank();
            if ( $userBank->available_bal < $totalAmount ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Insuficient bank balance ( your bank balance is $'.$userBank->available_bal.').';
            }
        }

        # queue order
        # -------------------------------
        if ( $ret['isError'] == false ) {
            $queueParams = array(
                'userId' => $userId,
                'packageId' => $pid,
                'ticketQuantity' => floor( $ticketQuantity ),
                'queueCode' => $queueCode,
                'status' => 'q'
            );
            CryptoShufflePackage::queueOrder( $queueParams );

            $msg = 'Your Order was placed in the order queue. Please do not close your browser/tab. This will not take more than 2 minutes. ';
            if ( $isTicketShort ) {
                $msg = 'Only '.$availableTicketCnt.' Tickets are available. ';
            }
            $ret['msg'][] = $msg;

            $ret['msg2'] = 'order_queued';
        }
        
        return response()->json($ret);
    }

    public function processOrder( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'action' => ''
        );

        # variables - default values
        $pid = 0;
        $queueCode = '';

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }


        if ( $pid == '' || $pid == 0 ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Invalid Pacakge ID.';
        }

        # get queued order
        $queuedOrder = false;
        if ( $ret['isError'] == false ) {
            $queuedOrder = CryptoShufflePackage::getCurrentQueuedOrder( $pid );
            //$ret['queuedOrder'] = $queuedOrder;

            if ( !$queuedOrder ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Invalid Pacakge ID.';
            }
        }
        
        # check if current queued order is current user's order
        if ( $ret['isError'] == false ) {
            $timeElapsed = time() - strtotime( $queuedOrder->time );
            $ret['timeElapsed'] = $timeElapsed;

            if ( $queuedOrder->code != $queueCode ) {
                # delete any queued order if user fails to pay within 1 minute
                if ( $timeElapsed > 60 ) {
                    CryptoShufflePackage::deleteQueuedOrder( $queuedOrder->code );
                }

                $ret['isError'] = true;
                $ret['msg'][] = 'Not the current queued order.';
                $ret['action'] = 'loopform';
            }
        }

        # process queued order if its current user's order: compute amount to pay, insert bank transaction
        if ( $ret['isError'] == false ) {
            $orderParams = $queuedOrder ;
            $orderResult = $this->processOrderStageOne( $orderParams );
            //$ret['stage1'] = $orderResult;

            if ( $orderResult['isError'] == true ) {
                $ret['isError'] = true;
                $ret['msg'][] = $orderResult['msg'];
            }
        }

        # generate ticket, insert order, compute referral credits
        if ( $ret['isError'] == false ) {
            CryptoShufflePackage::deleteQueuedOrder( $queuedOrder->code );

            CryptoShufflePackage::onPaymentComplete( $orderResult );
        }

        return response()->json($ret);
    }


    public function processOrderStageOne( $params ){
        # INIT
        # -------------------------------
        $ticketTotalCnt = 2097150;
        $availableTicketCnt = 2097150;
        $soldTicketCnt = 0;
        $isTicketShort = false;
        
        $userId = 0;
        $packageId = 178;
        $ticketQuantity = 1;
        $transactionCode = '';

        $ret = array(
            'isError' => false,
            'msg' => array()
        );

        if ( !empty($params) ) {
            $ticketQuantity = $params->quantity;
            $packageId = $params->package_id;
            $userId = $params->user_id;
            $ret['userId'] = $userId;
        }

        $transactionCode = LpjHelpers::generateTrCode();
        $ret['transactionCode'] = $transactionCode;

        # get package data
        # -------------------------------
        $packageInfo = CryptoShufflePackage::getSingePackage( $packageId );
        $totalAmount = false;
        if ( $packageInfo ) {
            $ticketTotalCnt = $packageInfo->ticket_total_cnt;
            $availableTicketCnt = $ticketTotalCnt - $packageInfo->sold_ticket_cnt;
            $ticketPrice = $packageInfo->ticket_price;
            
            $ret['packageId'] = $packageId;
            $ret['packageName'] = $packageInfo->name;
            $ret['tierConfig'] = $packageInfo->tier_config;
            $ret['soldTicketCnt'] = $packageInfo->sold_ticket_cnt;

            $ret['ticketTotalCnt'] = $ticketTotalCnt;
            $ret['availableTicketCnt'] = $availableTicketCnt;
            $ret['ticketPrice'] = $ticketPrice;
        }

        # check package status
        # -------------------------------
        if ( $packageInfo->status == 'active' ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Tickets Sold Out..';
        }

        # check available tickets
        # -------------------------------
        if ( $availableTicketCnt <= 0 ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'No more available tickets.';
        }

        # adjust ticket quantity pag kulang na yung available
        # -------------------------------
        if ( $ret['isError'] == false ) {
            if ( $availableTicketCnt < $ticketQuantity ) {
                $isTicketShort = true;

                $ret['isError'] = true;
                $ret['isTicketShort'] = $isTicketShort;
                $ret['msg'][] = 'Only '.$availableTicketCnt.' Tickets are available.';

                $ticketQuantity = $availableTicketCnt;
            }
        }

        if ( $ret['isError'] == false ) {
            $totalAmount = $ticketQuantity * $ticketPrice;

            $ret['ticketQuantity'] = $ticketQuantity;
            $ret['totalAmount'] = $totalAmount;
        }

        # get ticket codes
        # -------------------------------
        if ( $ret['isError'] == false ) {

            # insert bank transaction
            # -------------------------
            $details = array(
                'package_id' => $packageId,
                'package_name' => $packageInfo->name,
                'package_category' => 'cryptoshuffle',
            );
            $params = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'amount' => $totalAmount,
                'description' => 'Package Purchase: '.$packageInfo->name,
                'type' => 'out',
                'section' => 'package-payment',
                'transaction_date' => date('Y-m-d H:i:s'),
                'status' => 'completed',
                'details' => $details
            );
            UserBank::insertBankTransaction2( $params );

            $ret['paymentStatus'] = 'completed';
        }
        
        return $ret;
    }


    public function getTicketCodes( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        return response()->json($ret);
    }

    public function currentPackage( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # get user info
        $userId = Auth::id();
        $user = Auth::user();

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );
        }

        return response()->json($ret);
    }
} 
