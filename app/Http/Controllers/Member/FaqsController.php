<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Model\Member\Faq;
use App\Helpers\LpjHelpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class FaqsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:member');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ret = array();

        $userId = Auth::id();

        return view('member/faq/index', $ret);
    }

    public function getFaqs( Request $request ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # for pagination
        $qryParams = array();
        $qpage = 1;
        $page = 1;
        $qlimit = 5;

        # get input
        if ( $ret['isError'] == false ) {
            $input = $request->all();
            $input = LpjHelpers::fss( $input );
            extract( $input );

            $qpage = $page;
        }

        $params = array(
            'qpage' => $qpage,
            'qlimit' => $qlimit,
        );
        $ret['data'] = Faq::getFaqs( $params );
        $ret['pageLinks'] = $ret['data']['pageLinks'];

        $Faqs = $ret['data']['list'];


        # pagination html
        $paginationHtml = $ret['pageLinks'];

        if ( $Faqs->total() >  $qlimit ) {
            $showingCount = $qpage*$qlimit;
            if ( $showingCount > $Faqs->total() ) {
                $showingCount = $Faqs->total();
            }
            $paginationInfoHtml = '<span class="paginationInfo">Showing '.$showingCount.' of '.$Faqs->total().'</span>';
            $paginationHtml = '<div class="PaginationWrap">'.$paginationInfoHtml.$paginationHtml.'</div>';
        }
        $ret['paginationHtml'] = $paginationHtml;

        

        $ret2 = array(
            'Faqs' => $ret['data']['list']
        );


        $ret['returnHTML'] = view('member.faq.faq-list')->with($ret2)->render();
            

        return response()->json($ret);
    }
}
