<?php

namespace App\Http\Requests\Admin\Admin;

use App\Model\Admin\Admin;
use Illuminate\Foundation\Http\FormRequest;

class StoreAdminForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     => 'required|string',
            'lastname'      => 'required|string',
            'username'      => 'required|min:6|unique:admins',
            'password'      => 'required|confirmed|string|min:6',
            'pincode'       => 'required|confirmed|digits:5',
            'email'         => 'required|string|email|max:255|unique:admins',
            // 'role'          => 'required',
            // 'permissions'   => 'required_without_all',
        ];
    }

    /**
     * Generate data for creating a admin
     */
    public function inputs()
    {
        $role = \DB::table('roles')->where('slug', 'admin')->first();

        $data               = $this->only(['firstname', 'lastname', 'username', 'email', 'role', 'permissions']);
        $data['password']   = bcrypt($this->get('password'));
        $data['pincode']    = bcrypt($this->get('pincode'));
        $data['role']       = $role->id;

        return $data;
    }

    /**
     * Create admin with the generated form inputs.
     */
    public function persist()
    {
        $data   = $this->inputs();
        $admin  = Admin::create($data);

        // Sync records to admin_role
        $admin->roles()->sync($data['role']);

        // Sync role permissions to admin permissions
        foreach ($admin->roles as $role) {
            $role_permissions = \DB::table('permission_role')->select('permission_id')->where('role_id', $role->id)->get();
            $permission_ids = [];

            foreach ($role_permissions->toArray() as $role_permission) {
                $permission_ids[] = $role_permission->permission_id;
            }

            $admin->permissions()->sync($permission_ids);
        }

        // Synch permissions to admin_permission
        $admin->permissions()->syncWithoutDetaching($this->get('permissions'));
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Create packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Admin Successfully Created',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
