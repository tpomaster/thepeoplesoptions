<?php

namespace App\Http\Requests\Admin\Admin;

use App\Model\Admin\Admin;
use App\Rules\Admin\{ValidatePassword, ValidatePincode};
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateAdminForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'username' => [
                'required',
                Rule::unique('admins')->ignore($this->admin->id)
            ],
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('admins')->ignore($this->admin->id)
            ],
            // 'permissions'   => 'required_without_all',
            'current_password'  => [
                    'sometimes',
                    'nullable',
                    'min:6',
                    new ValidatePassword($this->admin)
                ],
            'password'      => 'sometimes|nullable|required_with:current_password|confirmed|min:6',
            'current_pincode'   => [
                    'sometimes',
                    'nullable',
                    'digits:5',
                    new ValidatePincode($this->admin)
                ],
            'pincode'       => 'sometimes|nullable|required_with:current_pincode|confirmed|digits:5',
        ];
    }

    /**
     * Generate data for creating a admin
     */
    public function inputs()
    {
        $data = $this->only(['username', 'firstname', 'lastname', 'email', 'permissions']);

        if (auth()->user()->id == $this->admin->id) {
            if (Admin::checkPassword($this->admin->id, $this->get('current_password'))) {
                $data['password'] = bcrypt($this->get('password'));
            }

            if (Admin::checkPincode($this->admin->id, $this->get('current_pincode'))) {
                $data['pincode'] = bcrypt($this->get('pincode'));
            }
        }

        if (auth()->user()->hasRole('super-admin')) {
            if ($this->get('password')) {
                $data['password'] = bcrypt($this->get('password'));
            }

            if ($this->get('pincode')) {
                $data['pincode'] = bcrypt($this->get('pincode'));
            }
        }

        return $data;
    }

    /**
     * Create admin with the generated form inputs.
     */
    public function persist()
    {
        $this->admin->update($this->inputs());

        $admin = $this->admin;

        // Sync records to admin_role
        // $admin->roles()->sync($this->get('role'));

        // Sync role permissions to admin permissions
        foreach ($admin->roles as $role) {
            $role_permissions = \DB::table('permission_role')->select('permission_id')->where('role_id', $role->id)->get();
            $permission_ids = [];

            foreach ($role_permissions->toArray() as $role_permission) {
                $permission_ids[] = $role_permission->permission_id;
            }

            $admin->permissions()->sync($permission_ids);
        }

        // Synch permissions to admin_permission
        $admin->permissions()->syncWithoutDetaching($this->get('permissions'));
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Create packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Admin Successfully Updated',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
