<?php

namespace App\Http\Requests\Admin\CryptoShufflePackage;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePackageForm extends StorePackageForm
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->package->sold_ticket_cnt > 0) {
            return [
                'name'      => ['required', Rule::unique('crypto_shuffle_packages')->ignore($this->package->id)],
                'status'    => 'required',
                'hide'      => 'required|boolean',
            ];
        }

        return array_merge(parent::rules(), [
            'name' => ['required', Rule::unique('crypto_shuffle_packages')->ignore($this->package->id)]
        ]);
    }

    public function inputs()
    {
        if ($this->package->sold_ticket_cnt > 0) {
            $data = $this->only('name', 'status', 'hide');
            return $data;
        }

        $data = parent::inputs();
        return $data;
    }

    public function persist()
    {
        $this->package->update($this->inputs());
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Update packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Crypto Shuffle Package Successfully Updated',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
