<?php

namespace App\Http\Requests\Admin\CryptoShufflePackage;

use App\Model\Admin\CryptoShufflePackage;
use Illuminate\Foundation\Http\FormRequest;

class StorePackageForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|unique:crypto_shuffle_packages',
            'status'                => 'required',
            'hide'                  => 'required|boolean',
            'max_per_order'         => 'required|numeric',
            'ticket_price'          => 'required|numeric',
            'referral_base_credit'  => 'required|numeric',
            'tier_level.*'          => 'required|numeric',
            'referral_credit.*'     => 'required|numeric'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'tier_level.*.required'         => 'Tier level field is required.',
            'referral_credit.*.required'    => 'Referral credit field is required.',
        ];
    }

    /**
     * Generate data for creating a package
     */
    public function inputs()
    {
        $data = $this->only('name', 'status', 'hide', 'max_per_order', 'ticket_price', 'referral_base_credit');

        $tier_config = [];

        // Format tier config
        $temp_tier_config = $this->only(['tier_level', 'referral_credit']);

        for ($i=0; $i < sizeof($temp_tier_config['tier_level']); $i++) {
            $tier_config[$i]['tier_level']      = $temp_tier_config['tier_level'][$i];
            $tier_config[$i]['referral_credit'] = $temp_tier_config['referral_credit'][$i];
        }

        $data['tier_config'] = serialize($tier_config);

        return $data;
    }

    /**
     * Create package with the generated form inputs.
     */
    public function persist()
    {
        $data = $this->inputs();

        $data['ticket_total_cnt']   = 2097150;
        $data['sold_ticket_cnt']    = 0;

        CryptoShufflePackage::create($data);
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Create packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Crypto Shuffle Package Successfully Created',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
