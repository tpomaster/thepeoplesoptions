<?php

namespace App\Http\Requests\Admin\MembershipPackage;

use App\Model\Admin\MembershipPackage;
use Illuminate\Foundation\Http\FormRequest;

class StoragePackageForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|unique:membership_packages',
            'status'                => 'required',
            'membership_fee'        => 'required',
            'package_category.*'    => 'required',
            'package_id.*'          => 'required',
            'package_amount.*'      => 'required',
            'tier_level.*'          => 'required',
            'referral_credit.*'     => 'required'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'package_category.*.required'   => 'Package category is required.',
            'package_id.*.required'         => 'Package ID is required.',
            'package_amount.*.required'     => 'Package amount is required.',
            'tier_level.*.required'         => 'Tier level field is required.',
            'referral_credit.*.required'    => 'Referral credit field is required.',
        ];
    }

    /**
     * Generate data for creating a package
     */
    public function inputs()
    {
        // Get validated inputs
        $data = $this->only(['name', 'status', 'membership_fee']);


        $packages_config = [];

        // Format packages config
        $temp_packages_config = $this->only(['package_category', 'package_id', 'package_amount']);

        for ($i=0; $i < sizeof($temp_packages_config['package_category']); $i++) {
            $packages_config[$i]['package_category']    = $temp_packages_config['package_category'][$i];
            $packages_config[$i]['package_id']          = $temp_packages_config['package_id'][$i];
            $packages_config[$i]['package_amount']      = $temp_packages_config['package_amount'][$i];
        }

        $data['packages_config'] = serialize($packages_config);

        $tier_config = [];

        // Format tier config
        $temp_tier_config = $this->only(['tier_level', 'referral_credit']);

        for ($i=0; $i < sizeof($temp_tier_config['tier_level']); $i++) {
            $tier_config[$i]['tier_level']      = $temp_tier_config['tier_level'][$i];
            $tier_config[$i]['referral_credit'] = $temp_tier_config['referral_credit'][$i];
        }

        $data['tier_config'] = serialize($tier_config);

        return $data;
    }

    /**
     * Create package with the generated form inputs.
     */
    public function persist()
    {
        MembershipPackage::create($this->inputs());
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Create packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Membership Package Successfully Created',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
