<?php

namespace App\Http\Requests\Admin\CryptoExchangePackage;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePackageForm extends StorePackageForm
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->package->funded_amount > 0) {
            return [
                'name'      => ['required', Rule::unique('crypto_mining_packages')->ignore($this->package->id)],
                'status'    => 'required',
                'hide'      => 'required|boolean'
            ];
        }

        return array_merge(parent::rules(), [
            'name' => ['required', Rule::unique('crypto_mining_packages')->ignore($this->package->id)]
        ]);
    }

    /**
     * Generate data for updating a package
     */
    public function inputs()
    {
        if ($this->package->funded_amount > 0) {
            return $this->only('name', 'status', 'hide');
        }

        $data = parent::inputs();
        return $data;
    }

    /**
     * Create package with the generated form inputs.
     */
    public function persist()
    {
        $this->package->update($this->inputs());

        $package = $this->package;

        // Update investors package duration
        // No model creted for this table
        \DB::table('crypto_exchange_funds')
            ->where('package_id', $package->id)
            ->update(['duration' => $package->duration]);

        if ($package->status == 'closed')
            $package->end();
        else if ($package->status == 'funded-active')
            $package->start();
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Update packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Package Successfully Updated',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
