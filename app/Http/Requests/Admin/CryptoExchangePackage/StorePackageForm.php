<?php

namespace App\Http\Requests\Admin\CryptoExchangePackage;

use App\Model\Admin\CryptoExchangePackage;
use Illuminate\Foundation\Http\FormRequest;

class StorePackageForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|unique:crypto_exchange_packages',
            'amount'                => 'required|numeric',
            'duration'              => 'required|numeric',
            'status'                => 'required',
            'hide'                  => 'required|boolean',
            'for_membership'        => 'required|boolean',
            'risk_profile'          => 'required',
            'profit_posting_freq'   => 'required',
            'management_fee.*'      => 'required',
            'tier_level.*'          => 'required|numeric',
            'referral_credit.*'     => 'required|numeric'
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'management_fee.platinum.required'      => 'Required field',
            'management_fee.gold.required'          => 'Required field',
            'management_fee.silver.required'        => 'Required field',
            'management_fee.free.required'          => 'Required field',
            'tier_level.*.required'                 => 'Tier level field is required.',
            'referral_credit.*.required'            => 'Referral credit field is required.',
        ];
    }

    /**
     * Generate data for creating a package
     */
    public function inputs()
    {
        // Get validated inputs
        $data = $this->only(['name', 'amount', 'duration', 'status', 'hide', 'for_membership', 'risk_profile', 'profit_posting_freq']);
        $data['management_fee'] = serialize($this->get('management_fee'));

        $tier_config = [];

        // Format tier config
        $temp_tier_config = $this->only(['tier_level', 'referral_credit']);

        for ($i=0; $i < sizeof($temp_tier_config['tier_level']); $i++) {
            $tier_config[$i]['tier_level']      = $temp_tier_config['tier_level'][$i];
            $tier_config[$i]['referral_credit'] = $temp_tier_config['referral_credit'][$i];
        }

        $data['tier_config'] = serialize($tier_config);

        return $data;
    }

    /**
     * Create package with the generated form inputs.
     */
    public function persist()
    {
        CryptoExchangePackage::create($this->inputs());
    }

    /**
     * Store package form response.
     */
    public function response()
    {
       try {
            // Create packages with the generated inputs
            $this->persist();

            $response = [
                'title'     => 'Success',
                'message'   => 'Crypto Exchange Package Successfully Created',
                'status'    => 'success'
            ];
        } catch (\Illuminate\Database\QueryException $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        } catch (\Exception $e) {
            $response = [
                'title'     => 'Oops Something went wrong!',
                'message'   => $e->getMessage(),
                'status'    => 'error'
            ];
        }

        return $response;
    }
}
