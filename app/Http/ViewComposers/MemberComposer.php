<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class MemberComposer{
    public $movieList = [];
    public $currentUser = [];
    public $currentUserBank = [];

    /**
     * Create a movie composer.
     *
     *  @param MovieRepository $movie
     *
     * @return void
     */
    public function __construct(){
        
        $this->currentUser = Auth::User();
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view){
        $view->with('currentUser', $this->currentUser);
    }
}