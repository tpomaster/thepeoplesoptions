<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, Setting};
use Illuminate\Auth\Access\HandlesAuthorization;

class SettingPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view settings.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-settings');
    }

    /**
     * Determine whether the user can view the setting.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Setting  $setting
     * @return mixed
     */
    public function view(Admin $admin, Setting $setting)
    {
        return $admin->hasPermissionTo('view-setting');
    }

    /**
     * Determine whether the user can create settings.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-setting');
    }

    /**
     * Determine whether the user can update the setting.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Setting  $setting
     * @return mixed
     */
    public function update(Admin $admin, Setting $setting)
    {
        return $admin->hasPermissionTo('update-setting');
    }

    /**
     * Determine whether the user can delete the setting.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Setting  $setting
     * @return mixed
     */
    public function delete(Admin $admin, Setting $setting)
    {
        return $admin->hasPermissionTo('delete-setting');
    }
}
