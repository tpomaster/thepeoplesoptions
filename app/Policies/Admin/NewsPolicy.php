<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, News};
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the news list.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-news-list');
    }

    /**
     * Determine whether the user can view the news.
     *
     * @param  \App\Admin  $admin
     * @param  \App\News  $news
     * @return mixed
     */
    public function view(Admin $admin, News $news)
    {
        return $admin->hasPermissionTo('view-news');
    }

    /**
     * Determine whether the user can create news.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-news');
    }

    /**
     * Determine whether the user can update the news.
     *
     * @param  \App\Admin  $admin
     * @param  \App\News  $news
     * @return mixed
     */
    public function update(Admin $admin, News $news)
    {
        return $admin->hasPermissionTo('update-news');
    }

    /**
     * Determine whether the user can delete the news.
     *
     * @param  \App\Admin  $admin
     * @param  \App\News  $news
     * @return mixed
     */
    public function delete(Admin $admin, News $news)
    {
        return $admin->hasPermissionTo('delete-news');
    }
}
