<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, UserBank};
use Illuminate\Auth\Access\HandlesAuthorization;

class UserBankPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the user bank.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\UserBank  $userBank
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-bank-transactions');
    }

    /**
     * Determine whether the user can view the userBank.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\UserBank  $userBank
     * @return mixed
     */
    public function view(Admin $admin, UserBank $userBank)
    {
        return $admin->hasPermissionTo('view-bank-transactions');
    }

    /**
     * Determine whether the user can create userBanks.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-bank-transactions');
    }

    /**
     * Determine whether the user can update the userBank.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\UserBank  $userBank
     * @return mixed
     */
    public function update(Admin $admin)
    {
        return $admin->hasPermissionTo('update-bank-transactions');
    }

    /**
     * Determine whether the user can send fund to the userBank.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\UserBank  $userBank
     * @return mixed
     */
    public function sendFund(Admin $admin)
    {
        return $admin->hasPermissionTo('send-bank-fund');
    }
}
