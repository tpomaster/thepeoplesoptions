<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, ReferralCredit};
use Illuminate\Auth\Access\HandlesAuthorization;

class ReferralCreditPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the referralCredits.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\ReferralCredit  $referralCredit
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-referral-credits');
    }
}
