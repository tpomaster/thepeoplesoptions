<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, Affiliate};
use Illuminate\Auth\Access\HandlesAuthorization;

class AffiliatePolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the affiliates.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Affiliate  $affiliate
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-affiliates');
    }

    /**
     * Determine whether the user can view the affiliate.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Affiliate  $affiliate
     * @return mixed
     */
    public function view(Admin $admin, Affiliate $affiliate)
    {
        return $admin->hasPermissionTo('view-affiliate');
    }

    /**
     * Determine whether the user can create affiliates.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-affiliate');
    }

    /**
     * Determine whether the user can update the affiliate.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Affiliate  $affiliate
     * @return mixed
     */
    public function update(Admin $admin)
    {
        return $admin->hasPermissionTo('update-affiliate');
    }

    /**
     * Determine whether the user can delete the affiliate.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Affiliate  $affiliate
     * @return mixed
     */
    public function delete(Admin $admin, Affiliate $affiliate)
    {
        return $admin->hasPermissionTo('delete-affiliate');
    }
}
