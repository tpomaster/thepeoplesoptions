<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, MembershipPackage};
use Illuminate\Auth\Access\HandlesAuthorization;

class MembershipPackagePolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-packages');
    }

    /**
     * Determine whether the user can view the membershipPackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\MembershipPackage  $membershipPackage
     * @return mixed
     */
    public function view(Admin $admin, MembershipPackage $membershipPackage)
    {
        return $admin->hasPermissionTo('view-package');
    }

    /**
     * Determine whether the user can create membershipPackages.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-package');
    }

    /**
     * Determine whether the user can update the membershipPackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\MembershipPackage  $membershipPackage
     * @return mixed
     */
    public function update(Admin $admin, MembershipPackage $membershipPackage)
    {
        return $admin->hasPermissionTo('update-package');
    }

    /**
     * Determine whether the user can delete the membershipPackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\MembershipPackage  $membershipPackage
     * @return mixed
     */
    public function delete(Admin $admin, MembershipPackage $membershipPackage)
    {
        return $admin->hasPermissionTo('delete-package');
    }

    public function close(Admin $admin, MembershipPackage $membershipPackage)
    {
        return $admin->hasPermissionTo('close-package');
    }

    public function compute(Admin $admin)
    {
        return $admin->hasPermissionTo('compute-package-profit');
    }
}
