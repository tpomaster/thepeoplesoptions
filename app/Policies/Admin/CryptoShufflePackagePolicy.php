<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, CryptoShufflePackage};
use Illuminate\Auth\Access\HandlesAuthorization;

class CryptoShufflePackagePolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-packages');
    }

    /**
     * Determine whether the user can view the cryptoShufflePackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\CryptoShufflePackage  $cryptoShufflePackage
     * @return mixed
     */
    public function view(Admin $admin, CryptoShufflePackage $cryptoShufflePackage)
    {
        return $admin->hasPermissionTo('view-package');
    }

    /**
     * Determine whether the user can create cryptoShufflePackages.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-package');
    }

    /**
     * Determine whether the user can update the cryptoShufflePackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\CryptoShufflePackage  $cryptoShufflePackage
     * @return mixed
     */
    public function update(Admin $admin, CryptoShufflePackage $cryptoShufflePackage)
    {
        return $admin->hasPermissionTo('update-package');
    }

    /**
     * Determine whether the user can delete the cryptoShufflePackage.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\CryptoShufflePackage  $cryptoShufflePackage
     * @return mixed
     */
    public function delete(Admin $admin, CryptoShufflePackage $cryptoShufflePackage)
    {
        return $admin->hasPermissionTo('delete-package');
    }

    public function close(Admin $admin, CryptoShufflePackage $cryptoShufflePackage)
    {
        return $admin->hasPermissionTo('close-package');
    }

    public function compute(Admin $admin)
    {
        return $admin->hasPermissionTo('compute-package-profit');
    }
}
