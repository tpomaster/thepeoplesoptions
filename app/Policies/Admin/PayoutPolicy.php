<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, Payout};
use Illuminate\Auth\Access\HandlesAuthorization;

class PayoutPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view faqs.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-payouts');
    }

    /**
     * Determine whether the user can view the payout.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Payout  $payout
     * @return mixed
     */
    public function view(Admin $admin, Payout $payout)
    {
        return $admin->hasPermissionTo('view-payout');
    }

    /**
     * Determine whether the user can approve payout.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function approve(Admin $admin, Payout $payout)
    {
        return $admin->hasPermissionTo('approve-payout');
    }

    /**
     * Determine whether the user can update the payout.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Payout  $payout
     * @return mixed
     */
    public function update(Admin $admin, Payout $payout)
    {
        return $admin->hasPermissionTo('update-payout');
    }

    /**
     * Determine whether the user can cancel the payout.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Payout  $payout
     * @return mixed
     */
    public function cancel(Admin $admin, Payout $payout)
    {
        return $admin->hasPermissionTo('cancel-payout');
    }

}
