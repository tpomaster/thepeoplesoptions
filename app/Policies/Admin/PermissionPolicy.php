<?php

namespace App\Policies\Admin;

use App\Model\Admin\Admin;
use App\Model\Admin\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-permissions');
    }

    /**
     * Determine whether the user can view the permission.
     *
     * @param  \App\User  $user
     * @param  \App\Model\Admin\Permission  $permission
     * @return mixed
     */
    public function view(Admin $admin, Permission $permission)
    {
        return $admin->hasPermissionTo('view-permission');
    }

    /**
     * Determine whether the user can create permissions.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-permission');
    }

    /**
     * Determine whether the user can update the permission.
     *
     * @param  \App\User  $user
     * @param  \App\Model\Admin\Permission  $permission
     * @return mixed
     */
    public function update(Admin $admin, Permission $permission)
    {
        return $admin->hasPermissionTo('update-permission');
    }

    /**
     * Determine whether the user can delete the permission.
     *
     * @param  \App\User  $user
     * @param  \App\Model\Admin\Permission  $permission
     * @return mixed
     */
    public function delete(Admin $admin, Permission $permission)
    {
        return $admin->hasPermissionTo('delete-permission');
    }
}
