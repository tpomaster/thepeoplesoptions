<?php

namespace App\Policies\Admin;

use App\Model\Admin\Admin;
use App\Model\Admin\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }


    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\User  $user
     * @param  \App\Role  $role
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-roles');
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\User  $user
     * @param  \App\Role  $role
     * @return mixed
     */
    public function view(Admin $admin)
    {
        return $admin->hasPermissionTo('view-role');
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(Admin $admin, Role $role)
    {
        return $admin->hasPermissionTo('create-role');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\User  $user
     * @param  \App\Role  $role
     * @return mixed
     */
    public function update(Admin $admin, Role $role)
    {
        return $admin->hasPermissionTo('update-role');
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\User  $user
     * @param  \App\Role  $role
     * @return mixed
     */
    public function delete(Admin $admin, Role $role)
    {
        return $admin->hasPermissionTo('delete-role');
    }
}
