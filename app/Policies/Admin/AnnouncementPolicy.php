<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, Announcement};
use Illuminate\Auth\Access\HandlesAuthorization;

class AnnouncementPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view announcements.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-announcements');
    }

    /**
     * Determine whether the user can view the announcement.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return mixed
     */
    public function view(Admin $admin, Announcement $announcement)
    {
        return $admin->hasPermissionTo('view-announcement');
    }

    /**
     * Determine whether the user can create announcement.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-announcement');
    }

    /**
     * Determine whether the user can update the announcement.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return mixed
     */
    public function update(Admin $admin, Announcement $announcement)
    {
        return $admin->hasPermissionTo('update-announcement');
    }

    /**
     * Determine whether the user can delete the announcement.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Announcement  $announcement
     * @return mixed
     */
    public function delete(Admin $admin, Announcement $announcement)
    {
        return $admin->hasPermissionTo('delete-announcement');
    }
}
