<?php

namespace App\Policies\Admin;

use App\Model\Admin\{Admin, Faq};
use Illuminate\Auth\Access\HandlesAuthorization;

class FaqPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view faqs.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-faqs');
    }

    /**
     * Determine whether the user can view the faq.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Faq  $faq
     * @return mixed
     */
    public function view(Admin $admin, Faq $faq)
    {
        return $admin->hasPermissionTo('view-faq');
    }

    /**
     * Determine whether the user can create faqs.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-faq');
    }

    /**
     * Determine whether the user can update the faq.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Faq  $faq
     * @return mixed
     */
    public function update(Admin $admin, Faq $faq)
    {
        return $admin->hasPermissionTo('update-faq');
    }

    /**
     * Determine whether the user can delete the faq.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Faq  $faq
     * @return mixed
     */
    public function delete(Admin $admin, Faq $faq)
    {
        return $admin->hasPermissionTo('delete-faq');
    }
}
