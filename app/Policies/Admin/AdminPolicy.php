<?php

namespace App\Policies\Admin;

use App\Model\Admin\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    public function before(Admin $admin, $ability)
    {
        if ($admin->roles->first()->slug == 'super-admin') {
            return true;
        }
    }

    /**
     * Determine whether the user can view the admins.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function index(Admin $admin)
    {
        return $admin->hasPermissionTo('view-users');
    }

    /**
     * Determine whether the user can view the admin.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function view(Admin $admin)
    {
        return $admin->hasPermissionTo('view-user');
    }

    /**
     * Determine whether the user can create admins.
     *
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $admin->hasPermissionTo('create-user');
    }

    /**
     * Determine whether the user can update the admin.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function update(Admin $admin)
    {
        return $admin->hasPermissionTo('update-user');
    }

    /**
     * Determine whether the user can delete the admin.
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function delete(Admin $admin)
    {
        return $admin->hasPermissionTo('delete-user');
    }

    /**
     * Determine whether the user can login to tpo marketing
     *
     * @param  \App\Admin  $admin
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function tpoMarketing(Admin $admin)
    {
        return $admin->hasPermissionTo('tpo-marketing');
    }
}
