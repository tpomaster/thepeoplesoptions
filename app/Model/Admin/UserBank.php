<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    /**
     * Add fund to user bank
     *
     * @param string $username    User's username.
     * @param int $amount      Amount to be added.
     * @param string $description Description for add fund.
     */
    public static function addFund( $username, $amount, $description = '' )
    {
        // Get user id using username
        $user = \DB::table('users')->select('id')->where('username', $username)->first();

        // Check user if exist
        if ($user === null) {
            return 'User does no exist.';
        }

        // Get user's bank using user id
        $bank = \DB::table('users_bank')->select('available_bal')->where('user_id', $user->id)->first();

        // Check if user bank exist
        if ($bank === null) {
            return 'User bank does no exist.';
        }

        // Add current available balance and send fund amount
        $new_balance = $bank->available_bal + $amount;

        // Update user's available balance
        \DB::table('users_bank')->where('user_id', $user->id)->update(['available_bal' => $new_balance]);

        // Save new transaction record
        \DB::table('users_bank_transactions')
            ->insert([
                'user_id'           => $user->id,
                'amount'            => $amount,
                'description'       => $description,
                'type'              => 'in',
                'section'           => 'deposit',
                'transaction_date'  => date('Y-m-d H:i:s'),
                'status'            => 'completed'
            ]);
    }

    /**
     * Retrieve list of latest transactions or transactions matching criteria.
     *
     * @param  array  $args Optional. Arguments to retrieve transactions.
     * @return collection Lists of transactions
     */
    public static function getTransactions($args = [])
    {
        $defaults = [
            'limit'         => 5,
            'order'         => 'desc',
            'orderby'       => 'transaction_date',
            'conditions' => []
        ];

        $args = array_merge($defaults, $args);

        $columns = [
            'users_bank_transactions.*',
            'users.username'
        ];

        extract($args);

        $transactions = \DB::table('users_bank_transactions')
            ->join('users', 'users_bank_transactions.user_id', '=', 'users.id')
            ->select($columns)
            ->orderBy($orderby, $order)
            ->when(! empty($conditions), function ($query) use ($conditions) {
                return $query->where($conditions);
            })
            ->when($limit > 0, function ($query) use ($limit) {
                return $query->paginate($limit);
            }, function ($query) {
                return $query->get();
            });

        return $transactions;
    }
}
