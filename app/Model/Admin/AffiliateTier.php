<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class AffiliateTier extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $user_id = 0;

    public function __construct()
    {

    }

    # this function uses laravel pagination
    public static function getReferrals( $args = [] ){
        $defaults = [
            'parent_id'         => 2,
            'parent_username'   => 'litopj2',
            'current_tier_lvl'  => 1,
            'qpage'             => 1,
            'page'              => 1,
            'qlimit'            => 5,
            'sort_by'           => 'id',
            'sort_order'        => 'DESC'
        ];

        $args = array_merge($defaults, $args);

        $referrals = \DB::table('users')
            ->select('id', 'username', 'membership', 'firstname', 'lastname', 'email', 'created_at', 'direct_ref_cnt')
            ->where([
                ['parent_id', $args['parent_id']]
            ])
            ->orderBy($args['sort_by'], $args['sort_order'])
            ->paginate($args['qlimit']);

        $pagination = $referrals->appends([
                'sort_by'           => $args['sort_by'],
                'sort_order'        => $args['sort_order'],
                'parent_id'         => $args['parent_id'],
                'parent_username'   => $args['parent_username'],
                'qpage'             => $args['qpage'],
                'page'              => $args['qpage'],
                'qlimit'            => $args['qlimit'],
                'current_tier_lvl'  => $args['current_tier_lvl']
            ])->links();

        return $referrals;
    }
}
