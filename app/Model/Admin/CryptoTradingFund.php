<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoTradingFund extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_trading_funds';

}
