<?php

namespace App\Model\Admin;

use App\Model\Admin\Permission;
use App\Traits\Admin\HasPermissionsTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     use HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug'
    ];

    public function permissions()
    {
        return $this->belongsTomany(Permission::class);
    }
}
