<?php

namespace App\Model\Admin;

use App\Model\Admin\User;
use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payout';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'exchange_rate',
        'release_date',
        'status',
    ];

    /**
     * Get the member that owns the payout.
     */
    public function member()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function approve()
    {
        return self::find($this->id)->update(['status' => 'completed']);
    }

    public static function total($status = '')
    {
        return self::if($status, 'status', '=', $status)->sum('amount');
    }
}
