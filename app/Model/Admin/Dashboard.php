<?php

namespace App\Model\Admin;

use Illuminate\Support\Facades\DB;

class Dashboard
{
    public static function myUserTable(){
        return 'users';
    }

    public static function adminTable(){
        return 'admins';
    }

    public static function bankTransactionTable(){
        return 'users_bank_transactions';
    }

    public static function usersBankTable(){
        return 'users_bank';
    }

    public static function miningFundsTable(){
        return 'crypto_mining_funds';
    }

    public static function tradingFundsTable(){
        return 'crypto_trading_funds';
    }

    public static function exchangeFundsTable(){
        return 'crypto_exchange_funds';
    }

    public static function cryptoShuffleTable(){
        return 'crypto_shuffle_orders';
    }

    public static function packageEarningsTable(){
        return 'package_earnings';
    }

    public static function myRefCreditsTable(){
        return 'referral_credits';
    }

    public static function dailyEarningsTable(){
        return 'member_daily_earnings_admin';
    }

    public static function getTeamCount(){
        $total_members = DB::table( self::myUserTable() )
            ->count('id');

        return $total_members;
    }

    public static function getBankBalance(){
        $balance = \DB::table( self::usersBankTable() )
                ->sum('available_bal');

        return $balance;
    }

    public static function getCombinedEarnings(){
        $package_earnings = \DB::table( self::usersBankTable() )
                        ->sum('package_earning');

        $referral_credits = \DB::table( self::usersBankTable() )
                        ->sum('referral_credit');

        $combined_earnings = $package_earnings + $referral_credits;

        return $combined_earnings;
    }

    public static function getPayout(){
        $payout = \DB::table( self::usersBankTable() )
                ->sum('payout');

        return $payout;
    }

    public static function getDashboardPie(){
        $miningFunds = DB::table( self::miningFundsTable() )
            ->sum('amount');

        $tradingFunds = DB::table( self::tradingFundsTable() )
            ->sum('amount');

        $exchangeFunds = DB::table( self::exchangeFundsTable() )
            ->sum('amount');

        $shuffleFunds = DB::table( self::cryptoShuffleTable() )
            ->sum('total_amount');

        return $pieData = [
            'miningFunds' => $miningFunds,
            'tradingFunds' => $tradingFunds,
            'exchangeFunds' => $exchangeFunds,
            'shuffleFunds' => $shuffleFunds
        ];
    }

    public static function getDailyTotalEarning( $date ){
        $packageEarnings = DB::table( self::packageEarningsTable() )
            ->whereDay('withdraw_date', '=', date('d', strtotime($date)))
            ->whereMonth('withdraw_date', '=', date('m', strtotime($date)))
            ->whereYear('withdraw_date', '=', date('Y', strtotime($date)))
            ->sum('amount');

        $refCreditsTotal = DB::table( self::myRefCreditsTable() )
            ->whereDay('created_at', '=', date('d', strtotime($date)))
            ->whereMonth('created_at', '=', date('m', strtotime($date)))
            ->whereYear('created_at', '=', date('Y', strtotime($date)))
            ->sum('amount');

        $totalEarnings = $packageEarnings + $refCreditsTotal;

        return $totalEarnings;
    }

    public static function checkAdminDailyEarnings(){
        $dataChecker = DB::table( self::dailyEarningsTable() )
            ->count();

        return $dataChecker;
    }

    public static function insertDailyEarnings( $currentDate ){
        $totalEarnings = 0;

        $dateCheck = DB::table( self::dailyEarningsTable() )
            ->whereDay('date', '=', date('d', strtotime($currentDate)))
            ->whereMonth('date', '=', date('m', strtotime($currentDate)))
            ->whereYear('date', '=', date('Y', strtotime($currentDate)))
            ->first();

        if ( !$dateCheck ) {
            $totalEarnings = self::getDailyTotalEarning( $currentDate );

            $MemDailyEarningsParams = array(
                'date' => $currentDate, // current date
                'total_earnings' => $totalEarnings, // total earnings
            );

            DB::table( self::dailyEarningsTable() )->insert( $MemDailyEarningsParams );
        } 
           
        return $totalEarnings;
    }

    public static function getGraphData() {
        $chartData = DB::table( self::dailyEarningsTable() )
            ->select('date', 'total_earnings')
            ->orderBy('date','ASC')
            ->get();

        return $chartData;
    }

    public static function getLatestTransactions(){
        $transactions = \DB::table('users_bank_transactions')
                        ->join('users', 'users_bank_transactions.user_id', '=', 'users.id')
                        ->select('users_bank_transactions.amount', 'users_bank_transactions.description', 'users_bank_transactions.transaction_date', 'users_bank_transactions.details', 'users.username')
                        ->where('users_bank_transactions.section', 'package-payment')
                        ->orderBy('users_bank_transactions.transaction_date', 'desc')
                        ->paginate(5);

        return $transactions;
    }

    public static function getLatestUsers(){
        $users = \DB::table( self::myUserTable() )
                ->select('username', 'firstname', 'lastname', 'email', 'membership', 'created_at')
                ->orderBy('created_at', 'desc')
                ->paginate(5);

        return $users;
    }

    public static function updateLastLogin ( $userId, $curDate ) {

        DB::table ( self::adminTable() )
            ->where([
                ['id', '=', $userId]
            ])
            ->update(['last_login' => $curDate]);
    }

    public static function getLastLogin ( $userId ) {

        $last_login = DB::table( self::adminTable() )
            ->where('id', '=', $userId)
            ->value('last_login');

        return $last_login;
    }
}
