<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class ReferralCredit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'referral_credits';
}
