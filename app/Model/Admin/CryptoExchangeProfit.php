<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoExchangeProfit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_exchange_profits';

    protected $fillable = [
        'package_id',
        'code',
        'date',
        'investment',
        'profit',
        'profit_percent',
        'for_membership'
    ];
}
