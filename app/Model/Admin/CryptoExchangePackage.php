<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CryptoExchangePackage extends Model
{
    protected $fillable = [
        'name',
        'amount',
        'status',
        'duration',
        'hide',
        'funded_status',
        'for_membership',
        'risk_profile',
        'profit_posting_freq',
        'management_fee',
        'tier_config',
    ];

    /**
     * Set package basic field options.
     */
    public static function options()
    {
        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $trade_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        $options = [
            'statuses'          => $statuses,
            'risk_profiles'     => $risk_profiles,
            'trade_frequencies' => $trade_frequencies,
            'member_types'      => $member_types
        ];

        return $options;
    }

    /**
     * Set package and investors status to closed.
     */
    public function end()
    {
        $this->update([
            'status'        => 'closed',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_exchange_funds')
            ->where('package_id', $this->id)
            ->update([
                'exchange_status'  => 'closed',
                'updated_at'    => Carbon::now()
            ]);
    }

    /**
     * Set package and investors status to active.
     */
    public function start()
    {
        $this->update([
            'status'        => 'funded-active',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_exchange_funds')
            ->where([
                ['package_id', $this->id],
                ['status', 'completed']
            ])
            ->update([
                'exchange_status'  => 'active',
                'updated_at'    => Carbon::now()
            ]);
    }

    public function computeInvestorsProfits()
    {
        $active_investors = \DB::table('crypto_exchange_funds')
            ->select('id AS fund_id', 'user_id', 'package_id')
            ->where([
                ['package_id', $this->id],
                ['exchange_status', 'active']
            ])
            ->get();

        foreach ($active_investors as $investor) {
            $profits_count = \DB::table('crypto_exchange_user_profits')
                ->where([
                    ['package_id', $this->id],
                    ['package_fund_id', $investor->fund_id]
                ])
                ->count();

            \DB::table('crypto_exchange_funds')
                ->where([
                    ['id', $investor->fund_id],
                    ['exchange_status', 'active']
                ])
                ->update([
                    'posted_profits_count'  => $profits_count,
                    'updated_at'            => Carbon::now()
                ]);
        }

        return $active_investors;
    }
}
