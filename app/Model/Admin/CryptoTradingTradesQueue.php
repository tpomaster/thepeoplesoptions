<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoTradingTradesQueue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_trading_trades_queue';

    protected $fillable = [
        'package_id',
        'code',
        'exec_date',
        'date',
        'num_of_trades',
        'trade_amount',
        'profit_percent',
        'profit',
        'status',
        'for_membership'
    ];

}
