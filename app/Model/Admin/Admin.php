<?php

namespace App\Model\Admin;

use App\Model\Admin\{Permission, Role};
use App\Notifications\AdminResetPasswordNotification;
use App\Traits\Admin\HasPermissionsTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\{Auth, Hash};

class Admin extends Authenticatable
{
    use Notifiable, HasPermissionsTrait;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'username',
        'email',
        'password',
        'pincode',
        'bitpal_access_token',
        'bitpal_access_token_time',
        'bitpal_bts_accounts',
        'bitpal_default_bts_account'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    /*public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }*/

    /**
     * Check admin pincode if correct.
     */
    public static function checkPincode($admin, $pincode)
    {
        $admin_id   = is_numeric($admin) ? $admin : $admin->id;
        $admin      = \DB::table('admins')->where('id', $admin_id)->first();

        if (Hash::check($pincode, $admin->pincode)) {
            return true;
        }

        return false;
    }

    /**
     * Check admin password if correct.
     */
    public static function checkPassword($admin, $password)
    {
        $admin_id   = is_numeric($admin) ? $admin : $admin->id;
        $admin      = \DB::table('admins')->where('id', $admin_id)->first();

        if (Hash::check($password, $admin->password)) {
            return true;
        }

        return false;
    }

    /**
     * Get admin BTS accounts
     */
    public function accounts()
    {
        $bitpal_bts_accounts = $this->bitpal_bts_accounts;
        $bitpal_bts_accounts = str_replace(['[',']','"'], '', $bitpal_bts_accounts);
        $bitpal_bts_accounts = explode(',', $bitpal_bts_accounts);

        return $bitpal_bts_accounts;
    }

    /**
     * Get list of admins base on criteria.
     * @param  array  $args criteria
     * @return collection     List of admins.
     */
    public static function getAdmins($args = [])
    {
        $defaults = ['limit' => 5, 'roles_in' => [], 'roles_not_in' => [] ];

        $args = array_merge($defaults, $args);

        extract($args);

        $columns = ['admins.id as id', 'firstname', 'lastname', 'username', 'email', 'status', 'roles.slug as role'];

        $admins = \DB::table('admins')
            ->join('admin_role', 'admins.id', '=', 'admin_role.admin_id')
            ->join('roles', 'admin_role.role_id', '=', 'roles.id')
            ->select($columns)
            ->when(! empty($roles_in), function ($query) use ($roles_in) {
                return $query->whereIn('roles.slug', $roles_in);
            })
            ->when(! empty($roles_not_in), function ($query) use ($roles_not_in) {
                return $query->whereNotIn('roles.slug', $roles_not_in);
            })
            ->when($limit > 0, function ($query) use ($limit) {
                return $query->paginate($limit);
            }, function ($query) {
                return $query->get();
            });

        return $admins;
    }
}
