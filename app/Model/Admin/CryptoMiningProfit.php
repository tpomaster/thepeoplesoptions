<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoMiningProfit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_mining_profits';

    protected $fillable = [
        'package_id',
        'code',
        'date',
        'mining_amount',
        'profit',
        'profit_percent',
        'for_membership'
    ];
}
