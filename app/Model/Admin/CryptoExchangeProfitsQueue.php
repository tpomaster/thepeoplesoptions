<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoExchangeProfitsQueue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_exchange_profits_queue';

    protected $fillable = [
        'package_id',
        'code',
        'exec_date',
        'date',
        'exchange_amount',
        'profit_percent',
        'profit',
        'status',
        'for_membership'
    ];
}
