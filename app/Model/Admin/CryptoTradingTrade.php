<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoTradingTrade extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_trading_trades';

    protected $fillable = [
        'package_id',
        'code',
        'num_of_trades',
        'date',
        'trade_amount',
        'profit',
        'profit_percent',
        'for_membership'
    ];
}
