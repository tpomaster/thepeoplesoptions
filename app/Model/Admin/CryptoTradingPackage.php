<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CryptoTradingPackage extends Model
{
    protected $fillable = [
        'name',
        'amount',
        'status',
        'duration',
        'hide',
        'funded_status',
        'for_membership',
        'risk_profile',
        'trade_frequency',
        'management_fee',
        'tier_config',
    ];

    /**
     * Set package basic field options.
     */
    public static function options()
    {
        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $trade_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        $options = [
            'statuses'          => $statuses,
            'risk_profiles'     => $risk_profiles,
            'trade_frequencies' => $trade_frequencies,
            'member_types'      => $member_types
        ];

        return $options;
    }

    /**
     * Set package and investors status to closed.
     */
    public function end()
    {
        $this->update([
            'status'        => 'closed',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_trading_funds')
            ->where('package_id', $this->id)
            ->update([
                'trade_status'  => 'closed',
                'updated_at'    => Carbon::now()
            ]);
    }

    /**
     * Set package and investors status to active.
     */
    public function start()
    {
        $this->update([
            'status'        => 'funded-active',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_trading_funds')
            ->where([
                ['package_id', $this->id],
                ['status', 'completed']
            ])
            ->update([
                'trade_status'  => 'active',
                'updated_at'    => Carbon::now()
            ]);
    }

    /**
     * Get package current funds.
     */
    public function funds()
    {
        $funds = \DB::table('crypto_trading_funds')
            ->where('package_id', $this->id)
            ->get();

        return $funds;
    }

    /**
     * Get package referral credits
     */
    public function referral_credits()
    {
        $referral_credits = \DB::table('referral_credits')
            ->where([
                ['package_id', $this->id],
                ['package_cat', 'cryptotrading']
            ])
            ->get();

        return $referral_credits;
    }

    /**
     * Get package user trades
     */
    public function user_trades()
    {
        $user_trades = \DB::table('crypto_trading_user_trades')
            ->select('trade_amount', 'trade_earning', 'management_fee', 'user_profit')
            ->where('package_id', $this->id)
            ->get();

        return $user_trades;
    }

    /**
     * Get package investors.
     */
    public function investors($args = [])
    {
        $defaults = [
            'limit'     => 5,
            'orderby'   => 'purchase_date',
            'order'     => 'DESC'
        ];

        extract( array_merge($defaults, $args) );

        $columns = [
            'user_id', 'username', 'firstname', 'lastname',
            'amount', 'trade_start_date', 'trade_end_date', 'trade_count', 'trade_status',
            'crypto_trading_funds.created_at as purchase_date'
        ];

        $query = \DB::table('crypto_trading_funds')
            ->join('users', 'users.id', '=', 'crypto_trading_funds.user_id')
            ->select($columns)
            ->orderBy($orderby, $order)
            ->where('package_id', $this->id);

        $investors_count = $query->count();

        if ($investors_count > $limit) {
            $investors = $query->paginate($limit);
        } else {
            $investors = $query->get();
        }

        return $investors;
    }

    /**
     * Get package trades
     * @return [type] [description]
     */
    public function trades($args = [])
    {
        $defaults = [
            'limit' => 5
        ];

        extract( array_merge($defaults, $args) );

        $query = \DB::table('crypto_trading_trades')
            ->where('package_id', $this->id)
            ->orderBy('date', 'desc');

        $trades_count = $query->count();

        if ($trades_count > $limit) {
            $trades = $query->paginate($limit);
        } else {
            $trades = $query->get();
        }

        return $trades;
    }
    /**
     * Compute investors trades and record
     */
    public function computeInvestorsTrades()
    {
        $active_investors = \DB::table('crypto_trading_funds')
            ->select('id AS fund_id', 'user_id', 'package_id')
            ->where([
                ['package_id', $this->id],
                ['trade_status', 'active']
            ])
            ->get();

        foreach ($active_investors as $investor) {
            $trades_count = \DB::table('crypto_trading_user_trades')
                ->where([
                    ['package_id', $this->id],
                    ['package_fund_id', $investor->fund_id]
                ])
                ->count();

            \DB::table('crypto_trading_funds')
                ->where([
                    ['id', $investor->fund_id],
                    ['trade_status', 'active']
                ])
                ->update([
                    'trade_count' => $trades_count,
                    'updated_at' => Carbon::now()
                ]);
        }

        return;
    }
}
