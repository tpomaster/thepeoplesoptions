<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoShufflePackage extends Model
{
    protected $fillable = [
        'name',
        'status',
        'hide',
        'ticket_price',
        'referral_base_credit',
        'max_per_order',
        'tier_config',
        'ticket_total_cnt',
        'sold_ticket_cnt',
        'package_terms',
        'wire_payment_inst',
    ];

    public function orders($args = [])
    {
        $defaults = [
            'limit'     => 5,
            'order'     => 'desc',
            'order_by'  => 'id'
        ];

        extract( array_merge($defaults, $args) );

        $columns = [
            'crypto_shuffle_orders.*',
            'users.username as username',
            'crypto_shuffle_packages.name as package_name',
        ];

        $orders_count = \DB::table('crypto_shuffle_orders')
            ->where('package_id', $this->id)
            ->count();

        $query = \DB::table('crypto_shuffle_orders')
            ->join('crypto_shuffle_packages', 'crypto_shuffle_orders.package_id', '=', 'crypto_shuffle_packages.id')
            ->join('users', 'crypto_shuffle_orders.user_id', '=', 'users.id')
            ->where('crypto_shuffle_orders.package_id', $this->id)
            ->orderBy($order_by, $order)
            ->select($columns);

        if ($orders_count > $limit) {
            $orders = $query->paginate($limit);
        } else {
            $orders = $query->get();
        }

        return $orders;
    }
}
