<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class MembershipPackage extends Model
{
    protected $fillable = [
        'name',
        'membership_fee',
        'duration',
        'status',
        'hide',
        'packages_config',
        'tier_config',
    ];
}
