<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class CryptoShuffleOrder extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crypto_shuffle_orders';
}
