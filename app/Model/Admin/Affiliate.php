<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
}
