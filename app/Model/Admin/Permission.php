<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = ['name', 'slug', 'category'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public static function defaultPermissions()
    {
        $default_permissions = [];

        $default_permissions['users'] = [
            'name' => [
                'View Users',
                'Add Users',
                'Edit Users',
                'Delete Users',
            ]
        ];

        $default_permissions['roles'] = [
            'name' => [
                'View Roles',
                'Add Roles',
                'Edit Roles',
                'Delete Roles',
            ]
        ];

        $default_permissions['permissions'] = [
            'name' => [
                'View Permissions',
                'Add Permissions',
                'Edit Permissions',
                'Delete Permissions',
            ]
        ];

        $default_permissions['packages'] = [
            'name' => [
                'View Packages',
                'Add Packages',
                'Edit Packages',
                'Delete Packages',
            ]
        ];

        return $default_permissions;
    }
}
