<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title',
        'content',
        'thumbnail_path'
    ];

    public function thumbnail()
    {
        return asset( $this->thumbnail_path ? 'storage/'. $this->thumbnail_path : 'images/thumbnails/default.jpg');
    }
}
