<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class CryptoMiningPackage extends Model
{
    protected $fillable = [
        'name',
        'amount',
        'status',
        'duration',
        'hide',
        'withrawable_fund',
        'funded_status',
        'for_membership',
        'risk_profile',
        'profit_posting_freq',
        'management_fee',
        'tier_config',
    ];

    /**
     * Set package basic field options.
     */
    public static function options()
    {
        $statuses           = ['available', 'funded-active', 'closed'];
        $risk_profiles      = ['conservative', 'moderate', 'aggressive', 'risky', 'high-risk'];
        $trade_frequencies  = ['daily', 'weekly', 'monthly'];
        $member_types       = ['platinum', 'gold', 'silver', 'free'];

        $options = [
            'statuses'          => $statuses,
            'risk_profiles'     => $risk_profiles,
            'trade_frequencies' => $trade_frequencies,
            'member_types'      => $member_types
        ];

        return $options;
    }

    /**
     * Set package and investors status to closed.
     */
    public function end()
    {
        $this->update([
            'status'        => 'closed',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_mining_funds')
            ->where('package_id', $this->id)
            ->update([
                'mining_status'  => 'closed',
                'updated_at'    => Carbon::now()
            ]);
    }

    /**
     * Set package and investors status to active.
     */
    public function start()
    {
        $this->update([
            'status'        => 'funded-active',
            'updated_at'    => Carbon::now()
        ]);

        \DB::table('crypto_mining_funds')
            ->where([
                ['package_id', $this->id],
                ['status', 'completed']
            ])
            ->update([
                'mining_status'  => 'active',
                'updated_at'    => Carbon::now()
            ]);
    }

    /**
     * Get package current funds.
     */
    public function funds()
    {
        $funds = \DB::table('crypto_mining_funds')
            ->where('package_id', $this->id)
            ->get();

        return $funds;
    }

    /**
     * Get package referral credits
     */
    public function referral_credits()
    {
        $referral_credits = \DB::table('referral_credits')
            ->where([
                ['package_id', $this->id],
                ['package_cat', 'cryptomining']
            ])
            ->get();

        return $referral_credits;
    }

    /**
     * Get package user trades
     */
    public function user_profits()
    {
        $user_profits = \DB::table('crypto_mining_user_profits')
            ->select('mining_amount', 'mining_profit', 'management_fee', 'user_profit')
            ->where('package_id', $this->id)
            ->get();

        return $user_profits;
    }

    /**
     * Get package investors.
     */
    public function investors($args = [])
    {
        $defaults = [
            'limit'     => 5,
            'orderby'   => 'purchase_date',
            'order'     => 'DESC'
        ];

        extract( array_merge($defaults, $args) );

        $columns = [
            'crypto_mining_funds.id AS fund_id', 'user_id', 'username', 'firstname', 'lastname', 'mining_status',
            'amount', 'start_date', 'end_date', 'crypto_mining_funds.status', 'posted_profits_count AS profits_count',
            'crypto_mining_funds.created_at as purchase_date'
        ];

        $query = \DB::table('crypto_mining_funds')
            ->join('users', 'users.id', '=', 'crypto_mining_funds.user_id')
            ->select($columns)
            ->orderBy($orderby, $order)
            ->where('package_id', $this->id);

        $investors_count = $query->count();

        if ($investors_count > $limit && $limit > 1) {
            $investors = $query->paginate($limit);
        }else if ($limit < 0) {
            $investors = $query->get();
        } else {
            $investors = $query->get();
        }

        return $investors;
    }

    /**
     * Get package profits
     */
    public function profits($args = [])
    {
        $defaults = [
            'limit' => 5
        ];

        extract( array_merge($defaults, $args) );

        $query = \DB::table('crypto_mining_profits')
            ->where('package_id', $this->id)
            ->orderBy('date', 'desc');

        $profits_count = $query->count();

        if ($profits_count > $limit) {
            $profits = $query->paginate($limit);
        } else {
            $profits = $query->get();
        }

        return $profits;

    }

    public function computeInvestorsProfits()
    {
        $active_investors = \DB::table('crypto_mining_funds')
            ->select('id AS fund_id', 'user_id', 'package_id')
            ->where([
                ['package_id', $this->id],
                ['mining_status', 'active']
            ])
            ->get();

        foreach ($active_investors as $investor) {
            $profits_count = \DB::table('crypto_mining_user_profits')
                ->where([
                    ['package_id', $this->id],
                    ['package_fund_id', $investor->fund_id]
                ])
                ->count();

            \DB::table('crypto_mining_funds')
                ->where([
                    ['id', $investor->fund_id],
                    ['mining_status', 'active']
                ])
                ->update([
                    'posted_profits_count'  => $profits_count,
                    'updated_at'            => Carbon::now()
                ]);
        }

        return $active_investors;
    }
}
