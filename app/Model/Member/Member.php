<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;
    protected $guard = 'member';
    protected $table = 'users';
    protected $userId = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function myTable(){
        return 'users';
    }

    public function setUserId( $userId ){
        $this->userId = $userId;
    }

    public function updateUser( $params ){
        if ( $params ) {
            extract($params);

        }else{
            return false;
        }

        $updateArr = array(
            'updated_at' => date('Y-m-d H:i:s'),
        );

        if ( isset($firstname) ) {
            $updateArr['firstname'] = $firstname;
        }

        if ( isset($lastname) ) {
            $updateArr['lastname'] = $lastname;
        }

        if ( isset($username) ) {
            $updateArr['username'] = $username;
        }

        if ( isset($password) ) {
            $updateArr['password'] = $password;
        }

        if ( isset($bitshares_name) ) {
            $updateArr['bitshares_name'] = $bitshares_name;
        }

        return DB::table($this->table)
            ->where('id', $this->userId)
            ->update($updateArr);
    }

    public static function updateUserWhere( $params, $where ){

        if ( $params ) {
            extract($params);

        }else{
            return false;
        }

        $updateArr = array(
            'updated_at' => date('Y-m-d H:i:s'),
        );

        if ( isset($firstname) ) {
            $updateArr['firstname'] = $firstname;
        }

        if ( isset($lastname) ) {
            $updateArr['lastname'] = $lastname;
        }

        if ( isset($username) ) {
            $updateArr['username'] = $username;
        }

        if ( isset($password) ) {
            $updateArr['password'] = $password;
        }


        if ( !empty($where) ) {
            return DB::table(self::myTable())
                ->where($where)
                ->update($updateArr);
                
        }else{
            return false;
        }
    }

    public static function updateUserField( $userId, $updData ){
        $ret = DB::table( self::myTable() )
            ->where('id', $userId)
            ->update($updData);

        return $ret;
    }

    public function getSingle( $field, $value ){
        $user = DB::table($this->table)->where([
            [$field, '=', $value]
        ])->first();

        if ( $user ) {
            $this->userId = $user->id;
        }

        return $user;
    }

    public function checkDuplicateUsername( $username ){
        $user = DB::table($this->table)->where([
            ['id', '!=', $this->userId],
            ['username', '=', $username]
        ])->first();

        if ( $user ) {
            return true;
        }else{
            return false;
        }
    }

    public function updateReferralCount(){
        $refCount = DB::table($this->table)
            ->where([
                ['parent_id', '=', $this->userId]
            ])->count();

        DB::table($this->table)
            ->where('id', $this->userId)
            ->update(['direct_ref_cnt' => $refCount]);
    }


    # you can delete this anytime
    public static function updateReferralCount2( $userId ){
        $refCount = DB::table( self::myTable() )
            ->where([
                ['parent_id', '=', $userId]
            ])->count();

        DB::table( self::myTable() )
            ->where('id', $userId)
            ->update(['direct_ref_cnt' => $refCount]);
    }


    public static function getUsersByIds( $userIdArray ){
        if ( empty($userIdArray) ) {
            return false;
        }

        $users = DB::table( self::myTable() )
            ->select('id', 'username', 'membership')
            ->whereIn('id', $userIdArray)
            ->get();

        return $users;
    }

    public static function addNew( $params ){
        if ( $params ) {
            extract($params);

        }else{
            return false;
        }

        $regData = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'username' => $username,
            'password' => $password,
            'parent_id' => $parent_id,
            'membership' => $membership,
            'status' => $status, # status: 0 = deactivated, 1 = email not verified, 2 = verified/active
            'activation_code' => $activation_code,
            'remember_token' => $remember_token,
            'created_at' => $created_at
        );
        return  DB::table('users')->insertGetId( $regData );
    }

    public static function getSingle2( $field, $value ){
        $user = DB::table( self::myTable() )->where([
            [$field, '=', $value]
        ])->first();

        return $user;
    }

    public function getSingleByUsername( $username ){
        $user = DB::table('users')->where([
            ['username', '=', $username]
        ])->first();

        return $user;
    }

    public static function getUserByUsername( $username ){
        $user = DB::table('users')->where([
            ['username', '=', $username]
        ])->first();

        return $user;
    }

    public function getSingleByUsernameAndPassword( $username, $password ){
        $user = DB::table('users')->where([
            ['username', '=', $username],
            ['password', '=', $password],
        ])->first();

        return $user;
    }

    public static function getSingleByActivationCode( $code ){
        $user = DB::table( self::myTable() )->where([
            ['activation_code', '=', $code]
        ])->first();

        return $user;
    }

    public static function updateMembershipLevel( $userId, $newMembership = 'silver', $endDate = '' ){
        $updateInfo = array(
            'membership' => $newMembership,
        );

        if ( empty($endDate) ) {
            $updateInfo['membership_end_date'] = date('Y-m-d', strtotime('+1 year'));

        }else{
            $updateInfo['membership_end_date'] = date('Y-m-d', strtotime($endDate.' +1 year'));
        }

        DB::table( self::myTable() )
            ->where('id', $userId)
            ->update($updateInfo);
    }

    public static function getUserFieldById( $userId, $field ){
        $user = DB::table( self::myTable() )->where([
            ['id', '=', $userId]
        ])->value($field);

        return $user;
    }




    public static function getPasswordResetKey( $field, $value ){
        $resetKey = DB::table('password_resets')->where([
            [$field, '=', $value]
        ])->value('token');

        return $resetKey;
    }

    public static function getPasswordResetKeyRow( $field, $value ){
        $k = DB::table('password_resets')->where([
            [$field, '=', $value]
        ])->first();

        return $k;
    }

    public static function setPasswordResetKey( $token, $email ){
        $ins = array(
            'email' => $email,
            'token' => $token,
            'created_at' => date('Y-m-d H:i:s')
        );
        return  DB::table('password_resets')->insert( $ins );
    }
}
