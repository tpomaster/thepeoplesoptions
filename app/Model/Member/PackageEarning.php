<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

use App\Model\Member\MemberTier;
use App\Model\Member\CryptoTradingPackage;

class PackageEarning{
    private $table = 'referral_credits';
    private $totalsTable = 'package_earning_totals';
    
    private $userId = 0;
    private $me = false;

    public function __construct() {

    }

    public static function myTable(){
        return 'package_earnings';
    }
    public static function myTotalsTable(){
        return 'package_earning_totals';
    }

    public static function insert( $params ){
        $row_id = 0; # profit id (crypto_trading_user_trades.id)
        $user_id = 0;
        $profit_id = 0; # main profit id (crypto_trading_trades.id)
        $package_id = 0;
        $package_cat = 'pkgcat';
        $type = 'xxx';
        $amount = 0;
        $withdraw_date = date('Y-m-d H:i:s');
        $note = '';
        $data = array();

        extract($params);

        $bankTrData = array(
            'row_id' => $row_id,
            'user_id' => $user_id,
            'profit_id' => $profit_id,
            'package_id' => $package_id,
            'package_cat' => $package_cat,
            'type' => $type,
            'amount' => $amount,
            'withdraw_date' => $withdraw_date,
            'note' => $note,
            'data' => json_encode( $data )
        );
        DB::table( self::myTable() )->insert( $bankTrData );

        return $bankTrData;
    }

    public static function getSingle( $tradeId ){
        $tr = DB::table( self::myTable() )->where([
                ['id', '=', $tradeId]
            ])
            ->first();

        return $tr;
    }

    public static function getAll( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';
        $fundId = '';
        $packageCat = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( isset( $packageId ) && $packageId != 0 ){
            $sqlWhere[] = array('package_id', '=', $packageId );
        }
        if( isset( $fundId ) && $fundId != '' ){
            $sqlWhere[] = array('fund_id', '=', $fundId );
        }
        if( isset( $packageCat ) && $packageCat != '' ){
            $sqlWhere[] = array('package_cat', '=', $packageCat );
        }

        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function updateStatus( $commissionId ){
        $updateInfo = array(
            'status' => 'withdrawn'
        );
        DB::table( self::myTable() )
            ->where('id', $commissionId)
            ->update($updateInfo);
    }

    public static function getSumByCategory( $userId, $category ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['package_cat', '=', $category],
            ])->sum('amount');

        return $sum;
    }

    public static function getSumByPackageId( $userId, $packageId, $category = 'cryptotrading' ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['package_id', '=', $packageId],
                ['package_cat', '=', $category],
            ])->sum('amount');

        return $sum;
    }

    public static function getSumByType( $userId, $type ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['type', '=', $type],
            ])->sum('amount');

        return $sum;
    }

    public static function getOverallSum( $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('amount');

        return $sum;
    }



    # ==================================================
    # functions for tatals table

    public static function createTotalsRow( $userId ){
        $pkgEarningData = array(
            'user_id' => $userId,
            'raffle' => 0,
            'cryptotrading' => 0,
            'cryptomining' => 0,
            'membership' => 0,
            'cryptoexchange' => 0,
            'cryptoatm' => 0,
            'total' => 0
        );
        DB::table( self::myTotalsTable() )->insert( $pkgEarningData );

        return (object) $pkgEarningData;
    }

    public static function getSingleTotals( $userId ){
        $row = DB::table( self::myTotalsTable() )->where([
                ['user_id', '=', $userId]
            ])
            ->first();

        return $row;
    }

    public static function updatePkgCatTotalEarning( $userId, $packageCat, $total ){
        # check for existing refcred totals
        $pkgEarninTotals = self::getSingleTotals( $userId );
        if ( !$pkgEarninTotals ) {
            self::createTotalsRow( $userId );
        }

        $updateInfo = array(
            $packageCat => $total
        );

        $res = DB::table( self::myTotalsTable() )
            ->where('user_id', $userId)
            ->update($updateInfo);

        return $res;
    }

    public static function updateOverallTotal( $userId, $total ){
        $updateInfo = array(
            'total' => $total
        );

        $res = DB::table( self::myTotalsTable() )
            ->where('user_id', $userId)
            ->update($updateInfo);

        return $res;
    }

    public static function updatePackageEarningTotals( $userId, $pkgEarningTotals = false ){
        # get package earning total
        if ( !$pkgEarningTotals ) {
            $pkgEarningTotals = PackageEarning::getSingleTotals( $userId );
        }
        
        $pet = $pkgEarningTotals;
        
        $pkgEarningOverallTotal = 0;
        if ( $pet ) {
            $pkgEarningOverallTotal = $pet->raffle + $pet->membership + $pet->cryptomining + $pet->cryptotrading + $pet->cryptoexchange + $pet->cryptoatm;

            # update pacakge earning overall total
            PackageEarning::updateOverallTotal( $userId, $pkgEarningOverallTotal );
        }

        return $pkgEarningOverallTotal;
    }

    public static function recomputePackageEarningTotals( $userId ){
        $membership = PackageEarning::getSumByCategory( $userId, 'membership' );
        $cryptomining = PackageEarning::getSumByCategory( $userId, 'cryptomining' );
        $cryptotrading = PackageEarning::getSumByCategory( $userId, 'cryptotrading' );
        $cryptoexchange = PackageEarning::getSumByCategory( $userId, 'cryptoexchange' );
        $raffle = PackageEarning::getSumByCategory( $userId, 'raffle' );

        $total = $membership + $cryptomining + $cryptotrading + $cryptoexchange + $raffle;

        $updateInfo = array(
            'raffle' => $raffle,
            'membership' => $membership,
            'cryptomining' => $cryptomining,
            'cryptotrading' => $cryptotrading,
            'cryptoexchange' => $cryptoexchange,
            'total' => $total,
        );

        DB::table( self::myTotalsTable() )
            ->where('user_id', $userId)
            ->update( $updateInfo );
    }
}

