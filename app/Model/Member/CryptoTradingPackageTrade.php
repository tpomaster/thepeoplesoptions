<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class CryptoTradingPackageTrade{
    private $table = 'crypto_trading_user_trades';

    private $userId = 0;
    private $me = false;

    public function __construct() {

    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'crypto_trading_user_trades';
    }

    public static function myTradesTable(){
        return 'crypto_trading_trades';
    }

    public static function getAll( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';

        $id = '';
        $userId = '';
        $fundId = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( isset( $packageId ) && $packageId != 0 ){
            $sqlWhere[] = array('package_id', '=', $packageId );
        }
        if( isset( $fundId ) && $fundId != '' ){
            $sqlWhere[] = array('fund_id', '=', $fundId );
        }

        $sqlWhere[] = array('trade_earning', '<>', 0 );

        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function getSingle( $tradeId ){
        $refCred = DB::table( self::myTable() )->where([
                ['id', '=', $tradeId]
            ])
            ->first();

        return $refCred;
    }

    public static function getSumByPackageId( $userId, $packageId, $status = '' ){
        $params = array(
            array( 'user_id', '=', $userId ),
            array( 'package_id', '=', $packageId )
        );

        if ( $status != '' ) {
            $params[] = array( 'status', '=', $status );
        }

        $sum = DB::table( self::myTable() )
            ->where($params)->sum('user_profit');

        return $sum;
    }

    public static function getOverallSum( $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('user_profit');

        return $sum;
    }

    public static function updateStatus( $tradeId, $status = 'withdrawn' ){
        $updateInfo = array(
            'status' => $status
        );
        DB::table( self::myTable() )
            ->where('id', $tradeId)
            ->update($updateInfo);
    }

    public static function updateStatusByFundId( $userId, $fundId, $status = 'cappedoff' ){
        $updateInfo = array(
            'status' => $status
        );

        DB::table( self::myTable() )
            ->where([
                ['package_fund_id', '=', $fundId],
                ['user_id', '=', $userId],
                ['trade_earning', '<', 0]
            ])
            ->update($updateInfo);
    }

    public static function getFieldTotal( $userId, $packageId, $field ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['trade_earning', '>', 0]
            ])
            ->sum($field);

        return $sum;
    }

    public static function getFirstTrade( $userId, $packageId ){
        $tr = DB::table( self::myTable() )->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
            ])
            ->first();

        return $tr;
    }




    public static function getUnwithdrawnProfitSum( $userId, $packageId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '<>', 0]
            ])
            ->sum('user_profit');

        return $sum;
    }

    public static function getUnwithdrawnProfitSumByGroup( $userId, $packageIds ){
        $sums = DB::table( self::myTable() )
            ->select( DB::raw('sum(user_profit) as profit_sum, package_id') )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '<>', 0]
            ])
            ->whereIn('package_id', $packageIds)
            ->groupBy('package_id')
            ->get();

        $ret = array();
        if ( $sums ) {
            foreach ($sums as $k => $s) {
                $ret[$s->package_id] = $s->profit_sum;
            }
        }

        return $ret;
    }

    public static function getNegativeProfitSum( $userId, $packageId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '<', 0]
            ])
            ->sum('trade_earning');

        return $sum;
    }

    public static function getNegativeProfitSumByGroup( $userId, $packageIds ){
        $sums = DB::table( self::myTable() )
            ->select( DB::raw('sum(trade_earning) as profit_sum, package_id') )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '<', 0]
            ])
            ->whereIn('package_id', $packageIds)
            ->groupBy('package_id')
            ->get();

        $ret = array();
        if ( $sums ) {
            foreach ($sums as $k => $s) {
                $ret[$s->package_id] = $s->profit_sum;
            }
        }

        return $ret;
    }

    public static function getUnwithdrawnProfitCount( $userId, $packageId ){
        $count = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '>', 0]
            ])
            ->count();

        return $count;
    }

    public static function countPostedTrades( $fundId ){
        $count = DB::table( self::myTable() )
            ->where([
                ['package_fund_id', '=', $fundId]
            ])
            ->count();

        return $count;
    }


    public static function countPkgPostedTrades( $packageId ){
        $count = DB::table( self::myTradesTable() )
            ->where([
                ['package_id', '=', $packageId]
            ])
            ->count();

        return $count;
    }
}

