<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class MembershipPowerBonus {
    private $table = 'membership_power_bonus';
    private $ctrTable = 'membership_power_bonus_counter';
    private $user_id = 0;


    public static function myTable(){
        return 'membership_power_bonus';
    }
     public static function myCtrTable(){
        return 'membership_power_bonus_counter';
    }


    public static function getPlatinumRererralCount( $userId ){
        $tr = DB::table( self::myCtrTable() )->where([
            ['user_id', '=', $userId]
        ])->first();

        return $tr;
    }

    public static function insertReferralCount( $uplineUserId, $transactionCode, $bonusCount ){
        $data = array(
            'user_id' => $uplineUserId,
            'tr_codes' => serialize( array( $transactionCode ) ),
            'count' => $bonusCount
        );
        DB::table( self::myCtrTable() )->insert( $data );
    }

    public static function updateReferralCount( $uplineUserId, $trCodes, $bonusCount ){

        $updateInfo = array(
            'tr_codes' => serialize( $trCodes ),
            'count' => $bonusCount
        );

        $res = DB::table( self::myCtrTable() )
            ->where('user_id', $uplineUserId)
            ->update($updateInfo);

        return $res;
    }

    public static function deleteReferraCount( $id ){
        DB::table( self::myCtrTable() )->where('id', '=', $id)->delete();
    }

    public static function insertPowerBonus( $uplineUserId, $trCodes ){
        $data = array(
            'user_id' => $uplineUserId,
            'transaction_codes' => serialize( $trCodes ),
            'amount' => 1500,
            'date_added' => date('Y-m-d H:i:s'),
            'status' => 'completed',
        );
        DB::table( self::myTable() )->insert( $data );
    }

    public static function computeUplinePowerBonus( $params ){
        
    }

    public static function getPowerBonusSum( $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('amount');

        return $sum;
    }

    public static function getPowerBonusSumByStatus( $userId, $status ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', $status]
            ])->sum('amount');

        return $sum;
    }


    public static function getPowerBonuses( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';
        $fundId = '';
        $packageCat = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }


        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }


    public static function getSinglePowerBonus( $bonusId ){
        $s = DB::table( self::myTable() )->where([
                ['id', '=', $bonusId]
            ])
            ->first();

        return $s;
    }

    public static function updatePowBonusStatus( $bonusId, $status = 'withdrawn' ){
        $updateInfo = array(
            'status' => 'withdrawn'
        );
        DB::table( self::myTable() )
            ->where('id', $bonusId)
            ->update($updateInfo);
    }
}
