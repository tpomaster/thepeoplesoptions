<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class UserbankFund {
    private $table = 'users_bank_addfund_request';
    private $tableBts = 'users_bank_addfund_request_bts';

    public static function myTable(){
        return 'users_bank_addfund_request';
    }

    public static function myTableBts(){
        return 'users_bank_addfund_request_bts';
    }

    public function insertAddFundRequest( $params ){
        $trData = array(
            'user_id' => $params['user_id'],
            'transaction_code' => $params['transaction_code'],
            'amount' => $params['amount'],
            'request_date' => date('Y-m-d H:i:s'),
            'gateway' => $params['gateway'],
            'status' => 'pending',
            'currency' => 'BTC',
        );
        DB::table($this->table)->insert( $trData );
    }

    public function updateTransactionStatus( $trCode, $status ){
        $updateArr = array(
            'status' => $status
        );

        return DB::table($this->table)
            ->where('transaction_code', $trCode)
            ->update($updateArr);
    }
    public static function updateTransactionStatusStatic( $trCode, $status ){
        $me = new self();

        $updateArr = array(
            'status' => $status
        );

        return DB::table($me->table)
            ->where('transaction_code', $trCode)
            ->update($updateArr);
    }

    public function getSingleTransaction( $field, $value ){
        $tr = DB::table($this->table)->where([
            [$field, '=', $value]
        ])->first();
        
        return $tr;
    }
    public static function getSingleTransactionStatic( $field, $value ){
        $me = new self();
        return $me->getSingleTransaction( $field, $value );
    }

    public static function checkDuplicateTransaction( $gatewayTransactionId, $transactionCode = '' ){
        $me = new self();

        $sqlWhere  = array();

        if( $transactionCode != '' ){
            $sqlWhere[] = array('transaction_code', '=', $transactionCode );
        }

        $sqlWhere[]  = array( 'gateway_tr_id', '=', $gatewayTransactionId );

        $trCount = DB::table($me->table)
            ->where($sqlWhere)
            ->count();

        return $trCount;
    }

    public static function updateTransaction( $params, $id ){
        $me = new self();

        return DB::table($me->table)
            ->where('id', $id)
            ->update($params);
    }


    # BITPAL / BITSHARES
    public static function insertAddFundRequestBTS( $params ){
        $userId = 0;
        $transactionCode = 'x';
        $usdAmount = 0;
        $assetAmount = 0;
        $asset = 'bitUSD';
        $requestDate = date('Y-m-d H:i:s');
        $gTrId = 'x';
        $gUserId = 'y';
        $status = 'pending';
        $details = array();

        if ( !empty($params)) {
            extract($params);
        }


        $trData = array(
            'user_id' => $userId,
            'transaction_code' => $transactionCode,
            'usd_amount' => $usdAmount,
            'asset_amount' => $assetAmount,
            'asset' => $asset,
            'request_date' => $requestDate,
            'gateway_tr_id' => $gTrId,
            'gateway_user_id' => $gUserId,
            'status' => $status,
            'details' => json_encode($details)
        );
        return DB::table( self::myTableBts() )->insert( $trData );
    }

    public static function updateBtsAddFundStatus( $trCode, $params ){
        return DB::table( self::myTableBts() )
            ->where('transaction_code', $trCode)
            ->update($params);
    }

    public static function getLatestBitpalAddFund( $userId ){
        $tr = DB::table( self::myTableBts() )->where([
            ['user_id', '=', $userId]
        ])->first();
        
        return $tr;
    }
}
