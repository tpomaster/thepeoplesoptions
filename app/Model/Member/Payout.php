<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;
use App\Helpers\LpjHelpers;

class Payout{
    private $table = 'payout';

    public static function myTable(){
        return 'payout';
    }

    public static function insert( $params ){
        $userId = 0;
        $transactionCode = 'x';
        $amount = 0;
        $withdrawalFee = 0;
        $netAmount = 0;
        $paymentGateway = 'BitPalBTS';
        $currency = 'Bitshares';
        $asset = 'BTCCORE';
        $requestDate = date('Y-m-d H:i:s');
        $releaseDate = date('Y-m-d H:i:s');
        $status = 'pending';
        $details = array();

        if ( !empty($params)) {
            extract($params);
        }

        $data = array(
            'transaction_code' => $transactionCode,
            'user_id' => $userId,
            'amount' => $amount,
            'withdrawal_fee' => $withdrawalFee,
            'net_amount' => $netAmount,
            'payment_gateway' => $paymentGateway,
            'currency' => $currency,
            'asset' => $asset,
            'request_date' => $requestDate,
            'release_date' => $releaseDate,
            'status' => $status,
            'details' => json_encode($details),
        );
        return DB::table( self::myTable() )->insert( $data );
    }

    public static function countDailyPayout( $userId, $reqDate ){
        $cnt = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '!=', 'cancelled']
            ])
            ->whereDay('request_date', '=', date('d', strtotime($reqDate)))
            ->whereMonth('request_date', '=', date('m', strtotime($reqDate)))
            ->whereYear('request_date', '=', date('Y', strtotime($reqDate)))
            ->count();

        return $cnt;
    }

    public static function countWeeklyPayout( $userId, $reqDate ){
        $dateRange = LpjHelpers::getWeekDateRange( date('Y-m-d', strtotime($reqDate)) );

        $cnt = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '!=', 'cancelled']
            ])
            ->whereRaw('request_date BETWEEN ? AND ?', [$dateRange['dateFrom'],$dateRange['dateTo']])
            ->count();

        return $cnt;
    }

    public static function countBiweeklyPayout( $userId, $reqDate ){
        $monthLastDay = date('Y-m-t', strtotime($reqDate));

        $day = date('d', strtotime($reqDate));
        $month = date('m', strtotime($reqDate));
        $year = date('Y', strtotime($reqDate));

        if ( $day >= 16 ) {
            $dateRange = array(
                'dateFrom' => $year.'-'.$month.'-16',
                'dateTo' => $monthLastDay,
            );
            
        }else{
            $dateRange = array(
                'dateFrom' => $year.'-'.$month.'-01',
                'dateTo' => $year.'-'.$month.'-15',
            );
        }

        $cnt = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '!=', 'cancelled']
            ])
            ->whereRaw('request_date BETWEEN ? AND ?', [$dateRange['dateFrom'],$dateRange['dateTo']])
            ->count();

        return $cnt;
    }

    public static function countMonthlyPayout( $userId, $reqDate ){
        $cnt = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '!=', 'cancelled']
            ])
            ->whereMonth('request_date', '=', date('m', strtotime($reqDate)))
            ->whereYear('request_date', '=', date('Y', strtotime($reqDate)))
            ->count();

        return $cnt;
    }

    public static function countCurrentMonthPayouts( $userId, $reqDate ){
        $cnt = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '!=', 'cancelled']
            ])
            ->whereMonth('request_date', '=', date('n', strtotime($reqDate)))
            ->whereYear('request_date', '=', date('Y', strtotime($reqDate)))
            ->count();

        return $cnt;
    }


    public static function getMaxWithdrawAmountByMembership( $membershipLevel ){
        $m = array(
            'free' => 1000,
            'bronze' => 1000,
            'silver' => 2000,
            'gold' => 3000,
            'platinum' => 100000,
        );

        if ( isset($m[$membershipLevel]) ) {
            return $m[$membershipLevel];

        }else{
            return 0.05;
        }
    }

    public static function getMaxWithdrawCountByMembership( $membershipLevel ){
        $m = array(
            'free' => 1,
            'bronze' => 1,
            'silver' => 2,
            'gold' => 4,
            'platinum' => 100000,
        );

        if ( isset($m[$membershipLevel]) ) {
            return $m[$membershipLevel];

        }else{

            return 1;
        }
    }

    public static function getManagementFeeByMembership( $membershipLevel ){
        $m = array(
            'free' => 0.45,
            'bronze' => 0.45,
            'silver' => 0.35,
            'gold' => 0.30,
            'platinum' => 0.25,
        );

        if ( isset($m[$membershipLevel]) ) {
            return $m[$membershipLevel];

        }else{
            return 0.45;
        }
    }


    public static function getWithdrawalFeeByMembership( $membershipLevel ){
        $m = array(
            'free' => 0.05,
            'bronze' => 0.05,
            'silver' => 0.04,
            'gold' => 0.03,
            'platinum' => 0.02,
        );

        if ( isset($m[$membershipLevel]) ) {
            return $m[$membershipLevel];

        }else{
            return 0.05;
        }
    }


    public static function updateField( $transactionCode, $updData ){
        $ret = DB::table( self::myTable() )
            ->where('transaction_code', $transactionCode)
            ->update($updData);

        return $ret;
    }
}
