<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class CryptoMiningPackageProfit{
    private $table = 'crypto_mining_user_profits';

    private $userId = 0;
    private $me = false;

    public function __construct() {

    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'crypto_mining_user_profits';
    }

    public static function getAll( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';

        $id = '';
        $userId = '';
        $fundId = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( isset( $packageId ) && $packageId != 0 ){
            $sqlWhere[] = array('package_id', '=', $packageId );
        }
        if( isset( $fundId ) && $fundId != '' ){
            $sqlWhere[] = array('fund_id', '=', $fundId );
        }

        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function getSingle( $profitId ){
        $refCred = DB::table( self::myTable() )->where([
                ['id', '=', $profitId]
            ])
            ->first();

        return $refCred;
    }

    public static function updateStatus( $profitId, $status = 'withdrawn' ){
        $updateInfo = array(
            'status' => 'withdrawn'
        );
        DB::table( self::myTable() )
            ->where('id', $profitId)
            ->update($updateInfo);
    }

    public static function getSumByPackageId( $userId, $packageId, $status = '' ){
        $params = array(
            array( 'user_id', '=', $userId ),
            array( 'package_id', '=', $packageId )
        );

        if ( $status != '' ) {
            $params[] = array( 'status', '=', $status );
        }

        $sum = DB::table( self::myTable() )
            ->where($params)->sum('user_profit');

        return $sum;
    }

    public static function getOverallSum( $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('user_profit');

        return $sum;
    }


    public static function updateStatusByFundId( $userId, $fundId, $status = 'cappedoff' ){
        $updateInfo = array(
            'status' => $status
        );

        DB::table( self::myTable() )
            ->where([
                ['package_fund_id', '=', $fundId],
                ['user_id', '=', $userId],
                ['mining_profit', '<', 0]
            ])
            ->update($updateInfo);
    }





    public static function getUnwithdrawnProfitSum( $userId, $packageId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->sum('user_profit');

        return $sum;
    }

    public static function getUnwithdrawnProfitSumByGroup( $userId, $packageIds ){
        $sums = DB::table( self::myTable() )
            ->select( DB::raw('sum(user_profit) as profit_sum, package_id') )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->whereIn('package_id', $packageIds)
            ->groupBy('package_id')
            ->get();

        $ret = array();
        if ( $sums ) {
            foreach ($sums as $k => $s) {
                $ret[$s->package_id] = $s->profit_sum;
            }
        }

        return $ret;
    }

    public static function getUnwithdrawnProfitCount( $userId, $packageId ){
        $count = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->count();

        return $count;
    }


    public static function getFieldTotal( $userId, $packageId, $field ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
            ])
            ->sum($field);

        return $sum;
    }

    public static function getFirstProfit( $userId, $packageId ){
        $tr = DB::table( self::myTable() )->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId],
            ])
            ->first();

        return $tr;
    }

    public static function countPostedProfits( $fundId ){
        $count = DB::table( self::myTable() )
            ->where([
                ['package_fund_id', '=', $fundId]
            ])
            ->count();

        return $count;
    }
}

