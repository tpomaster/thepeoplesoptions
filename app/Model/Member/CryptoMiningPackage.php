<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class CryptoMiningPackage{
    private $table = 'crypto_mining_packages';
    private $fundsTable = 'crypto_mining_funds';
    private $fundsBreakdownTable = 'crypto_mining_funds_breakdown';
    
    private $userId = 0;

    private $me = false;

    public function __construct() {

    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'crypto_mining_packages';
    }
    public static function myFundsTable(){
        return 'crypto_mining_funds';
    }
    public static function myFundsBreakdownTable(){
        return 'crypto_mining_funds_breakdown';
    }

    public static function getPackages( $params ) {
        $me = new self();

        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';
        $packageIds = false;

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        $sqlWhere[] = array('hide', '=', 0 );
        
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }

        # get count
        $trCount = DB::table($me->table)
            ->where($sqlWhere)
            ->count();

        # get list
        if( $packageIds ){
            $trList = DB::table($me->table)
                ->select('id', 'name', 'amount', 'duration', 'status', 'funded_amount', 'risk_profile', 'profit_posting_freq')
                ->where($sqlWhere)
                ->whereIn('id', $packageIds)
                ->orderBy($sortBy, $sortOrder)
                ->paginate($qlimit);

        }else{
            $trList = DB::table($me->table)
                ->select('id', 'name', 'amount', 'duration', 'status', 'funded_amount', 'risk_profile', 'profit_posting_freq')
                ->where($sqlWhere)
                ->orderBy($sortBy, $sortOrder)
                ->paginate($qlimit);
        }
            

        # 
        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks,
        );

        return $ret;
    }

    public static function getSingePackage( $packageId, $fields = array() ){
        if ( !empty($fields) ) {
            $pkg = DB::table( self::myTable() )
            ->select( $fields )
            ->where([
                    ['id', '=', $packageId]
                ])
            ->first();

        }else{
            $pkg = DB::table( self::myTable() )->where([
                ['id', '=', $packageId]
            ])->first();
        }

        return $pkg;
    }

    public static function getSingePackageByName( $packageName ){
        $pkg = DB::table( self::myTable() )->where([
            ['name', '=', $packageName]
        ])->first();

        return $pkg;
    }

    public static function getPackageName( $packageId ){
        $name = DB::table( self::myTable() )->where([
            ['id', '=', $packageId]
        ])->value('name');

        return $name;
    }

    public static function getPackageByIds( $packageIds ){
        if ( empty($packageIds) ) {
            return false;
        }

        $pkg = DB::table( self::myTable() )
            ->select('id', 'name')
            ->whereIn('id', $packageIds)
            ->get();

        return $pkg;
    }

    public static function updatePackageStatus( $params ){
        $packageId = 0;

        if ( !empty($params) ) {
            extract($params);
        }

        $updateInfo = array(
            'updated_at' => date('Y-m-d H:i:s')
        );

        if ( isset($fundedStatus) ) {
            $updateInfo['funded_amount'] = $fundedStatus; 
        }

        if ( isset($status) ) {
            $updateInfo['status'] = $status; 
        }

        DB::table( self::myTable() )
            ->where('id', $packageId)
            ->update($updateInfo);
    }

    public static function updatePkgField( $packageId, $updData ){
        $ret = DB::table( self::myTable() )
            ->where('id', $packageId)
            ->update($updData);

        return $ret;
    }




    # FUNDS
    public static function onPaymentComplete( $params ){
        # insert package fund and fund breakdown
        # -------------------------
        $params2 = array(
            'userId' => $params['userId'],
            'packageId' => $params['packageId'],
            'amount' => $params['fundAmount'],
            'status' => $params['paymentStatus'],
            'transactionCode' => $params['transactionCode'],
            'duration' => $params['duration'],
            'forMembership' => $params['forMembership']
        );
        $res2 = self::insertPackageFund( $params2 );

        # update package status
        # -------------------------
        $fundedStatus = $params['totalFunds'] + $params['fundAmount'];
        $params3 = array(
            'packageId' => $params['packageId'],
            'fundedStatus' => $fundedStatus,
        );
        if( isset($params['newPackageStatus']) ) {
            $params3['status'] = $params['newPackageStatus'];
        }
        self::updatePackageStatus( $params3 );

        # compute affililate commissions
        # -------------------------
        $params4 = array(
            'investorUserId' => $params['userId'],
            'downlineUserId' => $params['userId'],
            'transactionCode' => $params['transactionCode'],
            'packageId' => $params['packageId'],
            'packageName' => $params['packageName'],
            'packageCat' => 'cryptomining',
            'fundId' => $res2['packageFundId2'],
            'amount' => $params['fundAmount'],
            'tierConfig' => unserialize( $params['tierConfig'] )
        );
        ReferralCredit::computeUplineRC( $params4 );
    }

    public static function insertPackageFund( $params ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
        );

        # expected parameters, default values
        $userId = 0;
        $packageId = 0;
        $amount = 0;
        $date = date('Y-m-d H:i:s');
        $status = 'pending';
        $transactionCode = '';
        $forMembership = 0;
        $duration = 90;

        # other important parameters
        $packageFundId = 0;
        $hasInvestment = false;

        # initial variable values above will be replaced if they exist in the $params data
        if( !empty( $params ) ){
            $ret['params_arr'] = $params;
            extract($params);

            # preserve submitted amount
            $newAmount = $amount;
            $newTransactionCode = $transactionCode;
            $ret['transaction_code'] = $newTransactionCode;
        }

        # check if the user already funded the packge before
        # this will only pull funds with active status
        $prevInvestment = self::checkInvestor( $packageId, $userId );

        if ( $prevInvestment ) {
            # for packages connected to a membership package, 
            # i have to push the first fund to the bank before adding the new one to avoid conflict on last trade date
            if ( $forMembership == 1 ) {
                self::pushFundToBank( $prevInvestment );
                $hasInvestment = false;

            }else{
                $hasInvestment = true;

                $transactionCode .= ', '.$prevInvestment->transaction_code;
                $amount = $amount + $prevInvestment->amount;
                $packageFundId = $prevInvestment->id;
            }
        }

        if ( $hasInvestment ) {
            # just update the invesment amount if the user has previously funded this package
            $updateInfo = array(
                'transaction_code' => $transactionCode,
                'amount' => $amount,
                'max_amount' => $amount,
                'status' => $status
            );
            DB::table( self::myFundsTable() )
                ->where('id', $packageFundId)
                ->update($updateInfo);

            $ret['msg'] = 'success';

        } else {
            # this is the user's first time to fund this package
            # insert package fund or user investment
            $newFund = array(
                'user_id' => $userId,
                'transaction_code' => $transactionCode,
                'package_id' => $packageId,
                'amount' => $amount,
                'max_amount' => $amount,
                'share_percentage' => 0,
                'duration' => $duration,
                'created_at' => $date,
                'status' => $status,
            );
            if ( $forMembership == 1 ) {
                $newFund['start_date'] = $date;
            }
            $packageFundId = DB::table( self::myFundsTable() )->insertGetId( $newFund );
            $ret['packageFundId'] = $packageFundId;

            if( $packageFundId ){
                $ret['msg'] = 'success';
                
            }else{
                $ret['isError'] = true;
                $ret['msg'] = 'failed';
            }
        }

        # insert to breakdown table
        $fundInfo = array(
            'package_id' => $packageId,
            'package_fund_id' => $packageFundId,
            'transaction_code' => $newTransactionCode,
            'user_id' => $userId,
            'created_at' => date('Y-m-d H:i:s'),
            'amount' => $newAmount,
        );
        $packageFundId2 = DB::table( self::myFundsBreakdownTable() )->insertGetId( $fundInfo );
        $ret['packageFundId2'] = $packageFundId2;

        return $ret;
    }

    public static function getUserFundedPackages( $userId ){
        $pkgFund = DB::table( self::myFundsTable() )
            ->select( DB::raw('package_id') ) # i don't really know why we need to used the 'raw' method but we need it for the 'groupBy' method to work
            ->where([
                ['user_id', '=', $userId]
            ])
            ->groupBy('package_id')
            ->get();

        return $pkgFund;
    }

    public static function getUserInvestment( $packageId, $userId, $miningStatus = 'active' ){

        $sqlWhere = array();

        $sqlWhere[] = array('package_id', '=', $packageId);
        $sqlWhere[] = array('user_id', '=', $userId);

        if ($miningStatus != '') {
            $sqlWhere[] = array('mining_status', '=', $miningStatus);
        }

        $pkgFund = DB::table(self::myFundsTable())
            ->where($sqlWhere)
            ->orderBy('id', 'desc')
            ->first();

        return $pkgFund;
    }


    public static function getSinglePackageFund( $userId, $fundId ){
        $pkgFund = DB::table( self::myFundsTable() )->where([
                ['id', '=', $fundId],
                ['user_id', '=', $userId]
            ])
            ->orderBy('id', 'desc')
            ->first();

        return $pkgFund;
    }

    public static function getUserInvestmentBreakdown( $fundId, $userId ){
        $pkgFund = DB::table( self::myFundsBreakdownTable() )->where([
            ['package_fund_id', '=', $fundId],
            ['user_id', '=', $userId]
        ])
        ->orderBy('id', 'desc')
        ->get();

        return $pkgFund;
    }

    public static function getTotalFunds( $packageId ){
        $total = DB::table( self::myFundsTable() )->where([
            ['package_id', '=', $packageId],
            ['status', '=', 'completed']
        ])->sum('max_amount');

        return $total;
    }

    public static function checkInvestor( $packageId, $userId, $tradeStatus = 'active' ) {
        $pkgFund = DB::table( self::myFundsTable() )->where([
            ['package_id', '=', $packageId],
            ['user_id', '=', $userId],
            ['mining_status', '=', $tradeStatus]
        ])->first();

        return $pkgFund;
    }

    public static function pushFundToBank( $fundInfo ){
        $updateInfo = array(
            'mining_status' => 'closed',
            'status' => 'withdrawn'
        );
        DB::table( self::myFundsTable() )
            ->where('id', $fundInfo->id)
            ->update($updateInfo);

        $desc = 'Closed package fund: '.self::getPackageName($fundInfo->package_id);

        if ( strlen($fundInfo->transaction_code) > 49 ) {
            $trCode = substr($fundInfo->transaction_code, 0, 49);
            
        }else{
            $trCode = $fundInfo->transaction_code;
        }

        $params = array(
            'user_id' => $fundInfo->user_id,
            'transaction_code' => $trCode,
            'amount' => $fundInfo->amount,
            'description' => $desc,
            'type' => 'in',
            'section' => 'closed-package-fund',
            'transaction_date' => date('Y-m-d H:i:s'),
            'status' => 'completed'
        );
        UserBank::insertBankTransaction2( $params );

        return true;
    }

    public static function updateTradeAmount( $fundId, $tradeAmount ){
        $updateInfo = array(
            'amount' => $tradeAmount
        );
        DB::table( self::myFundsTable() )
            ->where('id', $fundId)
            ->update($updateInfo);
    }

    public static function updateFundField( $fundId, $updData ){
        $ret = DB::table( self::myFundsTable() )
            ->where('id', $fundId)
            ->update($updData);

        return $ret;
    }
}

