<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class UserBtsAccounts{
    private $table = 'users_bts_accounts';
    private $userId = 0;

    public function __construct( $user_id ) {
        $this->userId = $user_id;
    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'users_bts_accounts';
    }

    public static function getBtsAccounts( $userId ){
        $tr = DB::table( self::myTable() )->where([
            ['user_id', '=', $userId]
        ])->get();
    }
    
    public static function saveBtsAccount( $userId, $btsAccount){
        $checkBtsAccount = DB::table( self::myTable() )->where([
            ['bts_username', '=', $btsAccount]
        ])->first();

        if ( $checkBtsAccount ) {
            if ( $userId == $checkBtsAccount->user_id ) {
                $ret = $checkBtsAccount->bts_username;

            }else{
                $ret = false;
            }

        }else{
            $data = array(
                'user_id' => $userId,
                'bts_username' => $btsAccount,
                'date_created' => date('Y-m-d H:i:s'),
                'details' => ''
            );
            DB::table( self::myTable() )->insert( $data );
            $ret = $btsAccount;
        }

        return $ret;
    }
}
