<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;
use App\Helpers\LpjHelpers;


class CryptoShufflePackage{
    private $mysql1 = 'mysql';
    private $mysql2 = 'mysql2';

    private $pkgTable = 'crypto_shuffle_packages';
    private $ticketsTable = 'crypto_shuffle_tickets';
    private $ordersTable = 'crypto_shuffle_orders';
    private $matrixTable = 'crypto_shuffle_matrix';
    
    private $userId = 0;

    private $me = false;

    public function __construct()
    {

    }

    public static function pkgTable()
    {
        return 'crypto_shuffle_packages';
    }
    public static function ticketsTable()
    {
        return 'crypto_shuffle_tickets';
    }
    public static function matrixTable()
    {
        return 'crypto_shuffle_matrix';
    }

    public static function ordersTable()
    {
        return 'crypto_shuffle_orders';
    }

    public static function queueTable()
    {
        return 'crypto_shuffle_order_queue';
    }

    public static function getPackages( $params )
    {
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';
        $packageIds = false;

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        $sqlWhere[] = array('hide', '=', 0 );

        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }

        # get count
        $trCount = DB::table(self::pkgTable())
            ->where($sqlWhere)
            ->count();

        # get list
        if( $packageIds ){
            $trList = DB::table(self::pkgTable())
                ->where($sqlWhere)
                ->whereIn('id', $packageIds)
                ->orderBy($sortBy, $sortOrder)
                ->paginate($qlimit);

        }else{
            $trList = DB::table(self::pkgTable())
                ->where($sqlWhere)
                ->orderBy($sortBy, $sortOrder)
                ->paginate($qlimit);
        }
            

        # 
        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks,
            'sqlWhere' => $sqlWhere
        );

        return $ret;
    }

    public static function getSingePackage( $packageId )
    {
        $pkg = DB::table( self::pkgTable() )->where([
            ['id', '=', $packageId]
        ])->first();

        return $pkg;
    }

    public static function getSingePackageByName( $packageName )
    {
        $pkg = DB::table( self::pkgTable() )->where([
            ['name', '=', $packageName]
        ])->first();

        return $pkg;
    }

    public static function getPackageName( $packageId )
    {
        $name = DB::table( self::pkgTable() )->where([
            ['id', '=', $packageId]
        ])->value('name');

        return $name;
    }

    public static function getPackageByIds( $packageIds )
    {
        if ( empty($packageIds) ) {
            return false;
        }

        $pkg = DB::table( self::pkgTable() )
            ->select('id', 'name')
            ->whereIn('id', $packageIds)
            ->get();

        return $pkg;
    }

    public static function updatePackageStatus( $params )
    {
        $packageId = 0;

        if ( !empty($params) ) {
            extract($params);
        }

        $updateInfo = array(
            'updated_at' => date('Y-m-d H:i:s')
        );

        if ( isset($fundedStatus) ){
            $updateInfo['funded_status'] = $fundedStatus; 
        }

        if ( isset($status) ) {
            $updateInfo['status'] = $status; 
        }

        DB::table( self::pkgTable() )
            ->where('id', $packageId)
            ->update($updateInfo);
    }


    # QUEUE ORDERS
    public static function queueOrder( $params )
    {
        $packageId = 0;
        $ticketQuantity = 0;
        $status = 'pending';
        $time = date('Y-m-d H:i:s');
        $queueCode = 'x';

        if ( !empty($params) ) {
            extract($params);
        }

        $order = array(
            'user_id' => $userId,
            'package_id' => $packageId,
            'quantity' => $ticketQuantity,
            'status' => $status,
            'time' => $time,
            'code' => $queueCode,
        );
        $queueId = DB::table( self::queueTable() )->insertGetId( $order );

        return $queueId;
    }

    public static function getCurrentQueuedOrder( $packageId )
    {
        $qOrder = DB::table( self::queueTable() )->where([
                ['status', '=', 'q'],
                ['package_id', '=', $packageId]
            ])
            ->orderBy('id', 'asc')
            ->first();

        return $qOrder;
    }

    public static function deleteQueuedOrder( $queueCode )
    {
        DB::table( self::queueTable() )->where('code', '=', $queueCode)->delete();
    }

    public static function insertOrder( $params )
    {
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'orderId' => 0
        );

        # expected parameters, default values
        $transactionCode = '';
        $userId = 0;
        $packageId = 0;
        $ticketCodes = array();
        $ticketPrice = 0;
        $ticketQuantity = 0;
        $totalAmount = 0;
        $date = date('Y-m-d H:i:s');
        $status = 'pending';
        $note = '';


        # initial variable values above will be replaced if they exist in the $params data
        if( !empty( $params ) ){
            $ret['params_arr'] = $params;
            extract($params);
        }

        $orderParams = array(
            'transaction_code' => $transactionCode,
            'user_id' => $userId,
            'package_id' => $packageId,
            'ticket_codes' => serialize( $ticketCodes ),
            'item_price' => $ticketPrice,
            'quantity' => $ticketQuantity,
            'total_amount' => $totalAmount,
            'date' => $date,
            'status' => $status,
            'note' => json_decode($note)
        );
        $orderId = DB::table( self::ordersTable() )->insertGetId( $orderParams );
        $ret['orderId'] = $orderId;

        if( $orderId ){
            $ret['msg'] = 'success';
            
        }else{
            $ret['isError'] = true;
            $ret['msg'] = 'failed';
        }

        return $ret;
    }

    public static function onPaymentComplete( $params )
    {
        $userId = 0;
        $packageId = 0;
        $ticketPrice = 1;
        $ticketQuantity = 10;
        $totalAmount = 10;
        $tierConfig = '';
        $paymentStatus = 'pending';
        $packageName = 'Test';
        $soldTicketCnt = 0;
        $ticketTotalCnt = 100;
        $transactionCode = 'x';

        $ticketCodes = '';
        $orderId = 0;

        if ( !empty($params) ) {
           extract( $params );
        }

        # genearate ticket codes
        # -------------------------
        $ticketCodes = self::generatTicketCodes( $packageId, $ticketQuantity );

        # insert ticket order
        # -------------------------
        $orderParams = array(
            'transactionCode' => $transactionCode,
            'userId' => $userId,
            'packageId' => $packageId,
            'ticketCodes' => $ticketCodes,
            'ticketPrice' => $ticketPrice,
            'ticketQuantity' => $ticketQuantity,
            'totalAmount' => $totalAmount,
            'status' => $paymentStatus,
        );
        $res = self::insertOrder( $orderParams );
        $orderId = $res['orderId'];

        # update number of tickets sold
        # -------------------------
        $newSoldTicketCount = $soldTicketCnt + $ticketQuantity;
        $updateInfo = array(
            'sold_ticket_cnt' => $newSoldTicketCount,
        );
        if ( $newSoldTicketCount >= $ticketTotalCnt ) {
            $updateInfo['status'] = 'active';
        }
        DB::table( self::pkgTable() )
            ->where('id', $packageId)
            ->update($updateInfo);


        # compute affililate commissions
        # -------------------------
        $rcParams = array(
            'investorUserId' => $userId,
            'downlineUserId' => $userId,
            'transactionCode' => $transactionCode,
            'packageId' => $packageId,
            'packageName' => $packageName,
            'packageCat' => 'cryptoshuffle',
            'fundId' => $orderId,
            'amount' => $totalAmount,
            'tierConfig' => unserialize( $tierConfig )
        );
        ReferralCredit::computeUplineRC( $rcParams );
    }

    public static function generatTicketCodes( $packageId, $ticketQuantity ){
        $codeArr = array();
        $pkgId = str_pad($packageId, 3, '0', STR_PAD_LEFT);

        for ( $i=1; $i<=$ticketQuantity; $i++ ) {
            $code = LpjHelpers::lpjGenerateRandString2(5);
            $codeArr[] = $pkgId.$code;
        }

        return $codeArr;
    }

    
    public static function getUserFundedPackages( $userId )
    {
        $pkgFund = DB::table( self::ordersTable() )
            ->select( DB::raw('package_id') ) # i don't really know why we need to used the 'raw' method but we need it for the 'groupBy' method to work
            ->where([
                ['user_id', '=', $userId]
            ])
            ->groupBy('package_id')
            ->get();

        return $pkgFund;
    }

    public static function getUserPurchasedTickets( $packageId, $userId )
    {
        $pkgFund = DB::table( self::ordersTable() )->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId]
            ])
            ->orderBy('id', 'desc')
            ->get();

        return $pkgFund;
    }


    public static function getSinglePackageFund( $userId, $fundId )
    {
        $pkgFund = DB::table( self::myFundsTable() )->where([
                ['id', '=', $fundId],
                ['user_id', '=', $userId]
            ])
            ->orderBy('id', 'desc')
            ->first();

        return $pkgFund;
    }

    public static function getSinglePackageFundByPkgId( $userId, $packageId )
    {
        $pkgFund = DB::table( self::myFundsTable() )->where([
                ['package_id', '=', $packageId],
                ['user_id', '=', $userId]
            ])
            ->orderBy('id', 'desc')
            ->first();

        return $pkgFund;
    }


    public static function countSoldTickets( $packageId )
    {
        $total = DB::table( self::ordersTable() )->where([
            ['package_id', '=', $packageId],
            ['status', '=', 'completed']
        ])->sum('quantity');

        return $total;
    }


}

