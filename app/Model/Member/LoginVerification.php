<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class LoginVerification
{
    protected $table = 'users_login_codes';

    public static function myTable(){
        return 'users_login_codes';
    }

    public static function getSingle( $username, $status ){
        $d = DB::table( self::myTable() )->where([
                ['username', '=', $username],
                ['status', '=', $status],
            ])
            ->orderBy('id', 'DESC')
            ->first();

        return $d;
    }

    public static function getSingle2( $browserCode, $verificationCode, $status ){
        $d = DB::table( self::myTable() )->where([
                ['browser_code', '=', $browserCode],
                ['verification_code', '=', $verificationCode],
                ['status', '=', $status],
            ])
            ->orderBy('id', 'DESC')
            ->first();

        return $d;
    }

    public static function insert( $params ){
        return DB::table( self::myTable() )->insert( $params );
    }

    public static function updateStatus( $verificationCode, $status ){
        $updateInfo = array(
            'status' => $status
        );
        DB::table( self::myTable() )
            ->where('verification_code', $verificationCode)
            ->update($updateInfo);
    }

    public static function delete(){
        
    }
}
