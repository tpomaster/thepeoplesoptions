<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class MemberTier {
    private $table = 'users';
    private $user_id = 0;

    // public function __construct( $user_id )
    // {
    //     $this->user_id = $user_id;
    // }

    public static function myTable(){
        return 'users';
    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    # this function uses LpjPaginator
    public function getReferrals( $params = array() ){
        $parentId = 0;
        $qpage = 0;
        $qlimit = 5;

        $sortBy = 'id';
        $sortBy2 = 'created_at';
        $sortOrder = 'DESC';

        $skip = 0;


        if ( !empty($params) ) {
            extract($params);
        }

        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }

        $skip = $qpage*$qlimit;
        $take = $qlimit;

        $refCount = DB::table($this->table)
            ->where([
                ['parent_id', '=', $parentId]
            ])->count();

        $referrals = DB::table($this->table)
            ->select('id', 'username', 'membership', 'firstname', 'lastname', 'email', 'created_at', 'direct_ref_cnt')
            ->where([
                ['parent_id', '=', $parentId]
            ])
            ->orderBy($sortBy2, $sortOrder)
            ->skip($skip)
            ->take($take)
            ->get();

        $ret = array(
            'referrals' => $referrals,
            'refCount' => $refCount
        );

        return $ret;
    }

    # this function uses laravel pagination
    public function getReferrals2( $params = array() ){
        $parentId = 0;
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortBy2 = 'created_at';
        $sortOrder = 'DESC';

        $currTierLvl = '1';
        $sponsorUsername = 'litopj2';

        if ( !empty($params) ) {
            extract($params);
        }

        $referrals = DB::table($this->table)
            ->select('id', 'username', 'membership', 'firstname', 'lastname', 'email', 'created_at', 'direct_ref_cnt')
            ->where([
                ['parent_id', '=', $parentId]
            ])
            ->orderBy($sortBy2, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $referrals->appends([
                'sortBy' => $sortBy,
                'sortBy2' => $sortBy2,
                'sortOrder' => $sortOrder,
                'parentId' => $sponsorUsername,
                'qpage' => $qpage,
                'page' => $qpage,
                'qlimit' => $qlimit,
                'currTierLvl' => $currTierLvl
            ])->links();

        $ret = array(
            'referrals' => $referrals,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    #
    public static function getUpline( $userId ){
        $user = DB::table( self::myTable() )->where([
            ['id', '=', $userId]
        ])->first();


        $upline = false;
        if ( $user && $user->parent_id != 0 ) {
            $upline = DB::table( self::myTable() )->where([
                ['id', '=', $user->parent_id]
            ])->first();
        }

        return $upline;
    }

}
