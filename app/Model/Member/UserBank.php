<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class UserBank{
    private $table = 'users_bank';
    private $trTable = 'users_bank_transactions';
    private $userId = 0;

    public function __construct( $user_id ) {
        $this->userId = $user_id;
    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'users_bank';
    }

    public static function myTrTable(){
        return 'users_bank_transactions';
    }
    
    public function getUserBank() {
        $userBank = DB::table($this->table)->where([
            ['user_id', '=', $this->userId]
        ])->first();

        if ( !$userBank ) {
            $userBank = $this->createUserBank();
        }

        return $userBank;
    }

    public static function getUserBank2( $userId ){
        $userBank = DB::table( self::myTable() )->where([
            ['user_id', '=', $userId]
        ])->first();

        if ( !$userBank ) {
            $userBank = self::createUserBank2();
        }

        return $userBank;
    }

    public function createUserBank() {
        $bankData = array(
            'user_id' => $this->userId,
            'completed_in' => 0,
            'completed_out' => 0,
            'pending_in' => 0,
            'pending_out' => 0,

            'referral_credit' => 0,
            'package_earning' => 0,
            'deposit' => 0,
            'closed_package' => 0,

            'package_purchase' => 0,
            'cap_off_trade_amt' => 0,
            'payout' => 0,

            'available_bal' => 300000,
        );
        DB::table($this->table)->insert( $bankData );

        return (object) $bankData;
    }

    public static function createUserBank2( $userId ){
        $bankData = array(
            'user_id' => $userId,
            'completed_in' => 0,
            'completed_out' => 0,
            'pending_in' => 0,
            'pending_out' => 0,

            'referral_credit' => 0,
            'package_earning' => 0,
            'deposit' => 0,
            'closed_package' => 0,

            'package_purchase' => 0,
            'cap_off_trade_amt' => 0,
            'payout' => 0,

            'available_bal' => 0,
        );
        DB::table( self::myTable() )->insert( $bankData );

        return (object) $bankData;
    }


    public function getTransactions( $params ) {
        $qpage = 1;
        $qlimit = 10;
        $qsortBy = 'id';
        $qsortOrder = ' DESC';
        $id = '';
        $userId = '';
        $section = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( isset( $section ) && $section != '' ){
            $sqlWhere[] = array('section', '=', $section );
        }

        # get count
        $trCount = DB::table($this->trTable)
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table($this->trTable)
            ->where($sqlWhere)
            ->orderBy($qsortBy, $qsortOrder)
            ->skip($skip)
            ->take($take)
            ->get();

        $ret = array(
            'count' => $trCount,
            'list' => $trList
        );

        return $ret;
    }

    public function getTransactions2( $params ) {
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }

        if( isset( $section ) && $section != '' ){
            $sqlWhere[] = array('section', '=', $section );
        }

        # get count
        $trCount = DB::table($this->trTable)
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table($this->trTable)
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function getSingleTrnx( $params ){
        $id = '';
        $userId = '';
        $status = '';

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }

        $tr = DB::table( self::myTrTable() )
            ->where($sqlWhere)
            ->first();

        return $tr;
    }


    public function insertBankTransaction( $params ) {
        $user_id = 0;
        $transaction_code = '';
        $amount = 0;
        $description = '';
        $type = 'in';
        $section = 'deposit';
        $transaction_date = date('Y-m-d H:i:s');
        $status = 'pending';
        $details = array();

        extract($params);

        $bankTrData = array(
            'user_id' => $user_id,
            'transaction_code' => $transaction_code,
            'amount' => $amount,
            'description' => $description,
            'type' => $type,
            'section' => $section,
            'transaction_date' => $transaction_date,
            'status' => $status,
            'details' => json_encode( $details )
        );
        DB::table($this->trTable)->insert( $bankTrData );

        return $bankTrData;
    }

    public static function insertBankTransaction2( $params ){
        $user_id = 0;
        $transaction_code = '';
        $amount = 0;
        $description = '';
        $type = 'in';
        $section = 'deposit';
        $transaction_date = date('Y-m-d H:i:s');
        $status = 'pending';
        $details = array();

        extract($params);

        $bankTrData = array(
            'user_id' => $user_id,
            'transaction_code' => $transaction_code,
            'amount' => $amount,
            'description' => $description,
            'type' => $type,
            'section' => $section,
            'transaction_date' => $transaction_date,
            'status' => $status,
            'details' => json_encode( $details )
        );
        DB::table( self::myTrTable() )->insert( $bankTrData );

        return $bankTrData;
    }

    public static function getTransactionSum( $params ){
        $userId = 0;
        $status = '';
        $type = '';
        $section = '';

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # where statement
        $sqlWhere  = array();
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( $type != '' ){
            $sqlWhere[] = array('type', '=', $type );
        }
        if( $section != '' ){
            $sqlWhere[] = array('section', '=', $section );
        }

        # get sum
        $sum = DB::table( self::myTrTable() )
            ->where($sqlWhere)
            ->sum('amount');

        return $sum;
    }

    public function updateTransactionStatus( $trCode, $status ) {
        $updateArr = array(
            'status' => $status
        );

        return DB::table($this->trTable)
            ->where('transaction_code', $trCode)
            ->update($updateArr);
    }

    public static function updateTransactionStatusStatic( $trCode, $status ) {
        $me = new self( 1 );

        $updateArr = array(
            'status' => $status
        );

        return DB::table($me->trTable)
            ->where('transaction_code', $trCode)
            ->update($updateArr);
    }





    public static function computeAvalableBal( $userId, $userBank = false ){
        if ( !$userBank ) {
            $userBank = self::getUserBank2( $userId );
        }
        
        $b = $userBank;

        $overallTotal = 0;
        if ( $b ) {
            $overallTotal = ($b->deposit + $b->referral_credit + $b->package_earning + $b->closed_package) - ($b->package_purchase + $b->cap_off_trade_amt + $b->payout);

            self::updateOvarallTotal( $userId, $overallTotal );
        }
    }

    public static function updateOvarallTotal( $userId, $total ){
        $updateInfo = array(
            'available_bal' => $total
        );

        $res = DB::table( self::myTable() )
            ->where('user_id', $userId)
            ->update($updateInfo);

        return $res;
    }

    public static function updateBankSectionTotal( $userId = 0, $section = 0, $total = 0){
        $bankData = array(
            $section => $total
        );

        DB::table( self::myTable() )
            ->where('user_id', $userId)
            ->update( $bankData );
    }

    # individual recompute for: deposit, payout, pacakge_purchase, and cap_off_trade_amt
    public static function recomputeBankSectionTotal( $userId, $section, $status = '' ){
        # get bank transactions sum by section
        $params2 = array(
            'userId' => $userId,
            'section' => $section
        );

        if ( $status != '' ) {
            $params2['status'] = $status;
        }

        $computedSum = self::getTransactionSum( $params2 );

        $sectionName = '';
        switch ($section) {
            case "deposit":
                $sectionName = 'deposit';
                break;
            case "package-payment":
                $sectionName = 'package_purchase';
                break;
            case "cap-off-trade-amount":
                $sectionName = 'cap_off_trade_amt';
                break;
            case "withdrawal":
                $sectionName = 'payout';
                break;
            default:
                $sectionName = '';
        }

        # stop if sectionName is empty
        if ( $sectionName == '' ) {
            return false;
        }

        $bankData = array(
            $sectionName => $computedSum,
        );

        DB::table( self::myTable() )
            ->where('user_id', $userId)
            ->update( $bankData );
    }

    public static function reComputeBankTotals( $params ){
        $userId = 0;
        $refCredOverallTotal = 0;
        $packageEarningTotal = 0;

        if( !empty( $params ) ){
            extract($params);
        }

        # completed in
        $params2 = array(
            'userId' => $userId,
            'type' => 'in',
            'status' => 'completed'
        );
        $completedIn = self::getTransactionSum( $params2 );

        # completed out
        $params2 = array(
            'userId' => $userId,
            'type' => 'out',
            'status' => 'completed'
        );
        $completedOut = self::getTransactionSum( $params2 );

        # completed out
        $params2 = array(
            'userId' => $userId,
            'type' => 'out',
            'status' => 'pending'
        );
        $pendingOut = self::getTransactionSum( $params2 );



        # deposit
        $params2 = array(
            'userId' => $userId,
            'section' => 'deposit',
            'status' => 'completed'
        );
        $deposit = self::getTransactionSum( $params2 );

        # package purchase
        $params2 = array(
            'userId' => $userId,
            'section' => 'package-payment',
            'status' => 'completed'
        );
        $packagePurchase = self::getTransactionSum( $params2 );

        # cap-off-trade-amount
        $params2 = array(
            'userId' => $userId,
            'section' => 'cap-off-trade-amount',
            'status' => 'completed'
        );
        $capOffTradeAmount = self::getTransactionSum( $params2 );

        # closed-package-fund
        $params2 = array(
            'userId' => $userId,
            'section' => 'closed-package-fund',
            'status' => 'completed'
        );
        $closedPackageFund = self::getTransactionSum( $params2 );

        # payout
        $params2 = array(
            'userId' => $userId,
            'section' => 'withdrawal',
            'status' => 'completed'
        );
        $payout = self::getTransactionSum( $params2 );


        $availableBalance = $completedIn - ( $completedOut + $pendingOut );

        $bankData = array(
            'completed_in' => $completedIn,
            'completed_out' => $completedOut,
            'pending_in' => 0,
            'pending_out' => $pendingOut,
            
            'deposit' => $deposit,
            'closed_package' => $closedPackageFund,
            'package_purchase' => $packagePurchase,
            'cap_off_trade_amt' => $capOffTradeAmount,
            'payout' => $payout,
        );

        if ( $refCredOverallTotal != 0 ) {
            $bankData['referral_credit'] = $refCredOverallTotal;
            $availableBalance = $availableBalance + $refCredOverallTotal;
        }

        if ( $packageEarningTotal != 0 ) {
            $bankData['package_earning'] = $packageEarningTotal;
            $availableBalance = $availableBalance + $packageEarningTotal;
        }

        $bankData['available_bal'] = $availableBalance;

        DB::table( self::myTable() )
            ->where('user_id', $userId)
            ->update( $bankData );
    }
}
