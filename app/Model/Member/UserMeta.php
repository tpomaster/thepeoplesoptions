<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class UserMeta
{
    private $table = 'users_meta';
    private $user_id = 0;


    public function __construct( $user_id ) 
    {
        $this->user_id = $user_id;
    }

    public function setDbTable( $dbTable )
    {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'users_meta';
    }
    
    public function addNew( $params )
    {
        $phone = '';
        $address = '';
        $city = '';
        $state = '';
        $zip = '';
        $country = '';

        if ( $params ) {
            extract($params);

        }else{
            return false;
        }

        $regData = array(
            'user_id' => $this->user_id,
            'phone' => $phone,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zip' => $zip,
            'country' => $country,
        );
        return DB::table($this->table)->insert( $regData );
    }

    public function update( $params )
    {
        $updData = array();

        if ( $params ) {
            extract($params);

        }else{
            return false;
        }

        if ( isset($phone) ) {
            $updData['phone'] = $phone;
        }

        if ( isset($address) ) {
            $updData['address'] = $address;
        }

        if ( isset($city) ) {
            $updData['city'] = $city;
        }

        if ( isset($state) ) {
            $updData['state'] = $state;
        }

        if ( isset($zip) ) {
            $updData['zip'] = $zip;
        }

        if ( isset($country) ) {
            $updData['country'] = $country;
        }

        if ( isset($tpo_marketing_code) ) {
            $updData['tpo_marketing_code'] = $tpo_marketing_code;
        }

        if ( empty($updData) ) {
            return false;
        }

        return DB::table($this->table)
            ->where('user_id', $this->user_id)
            ->update($updData);
    }

    public function getSingle()
    {
        $userMeta = DB::table($this->table)->where([
            ['user_id', '=', $this->user_id]
        ])->first();

        // $userMeta = array(
        //     'user_id' => $this->user_id,
        //     'phone' => '345',
        // );

        return $userMeta;
    }

    
    public function getSingleByField( $field, $value )
    {
        $userMeta = DB::table($this->table)->where([
            [$field, '=', $value]
        ])->first();

        return $userMeta;
    }

    public static function getUserMeta( $userId, $fields ){

        if ( empty($fields) ) {
            return false;
        }

        $userMeta = DB::table( self::myTable() )
        ->select( $fields )
        ->where([
            ['user_id', '=', $userId]
        ])->first();

        return $userMeta;
    }

    public static function updateUserMeta( $userId, $updData ){
        $ret = DB::table( self::myTable() )
            ->where('user_id', $userId)
            ->update($updData);

        return $ret;
    }

}
