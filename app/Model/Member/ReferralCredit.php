<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

use App\Model\Member\MemberTier;
use App\Model\Member\CryptoTradingPackage;
use App\Model\Member\MembershipPackage;

class ReferralCredit{
    private $table = 'referral_credits';
    private $totalsTable = 'referral_credit_totals';
    
    private $userId = 0;
    private $me = false;

    public function __construct() {

    }

    public static function myTable(){
        return 'referral_credits';
    }
    public static function myTotalsTable(){
        return 'referral_credit_totals';
    }

    public static function computeUplineRC( $params ){
        # expected parameters
        $investorUserId = 0;
        $downlineUserId = 0;
        $transactionCode = '';
        $packageCat = '';
        $packageId = 0;
        $packageName = '';
        $fundId = 0;
        $amount = 0;

        # init other vars
        $refCreditAmount = 0;
        $refCreditPercent = 0;
        $tierLevel = 0;
        $date = date('Y-m-d H:i:s');
        $status = 'completed';
        $tierConfig = false;

        # get parameters
        if( !empty( $params ) ){
            extract($params);
        }

        # get upline
        $uplineUserId = false;
        $upline = MemberTier::getUpline( $downlineUserId );
        if ( $upline ) {
            $uplineUserId = $upline->id;
        }

        # stop if no upline id
        # ----------------------------
        if ( !$uplineUserId ) {
            return false;
        }

        # stop if no tier config data
        # ----------------------------
        if ( !$tierConfig ) {
            return false;
        }


        # FOR MEMBERSHIP PACKAGES
        # ----------------------------
        if ( $packageCat == 'membership' ) {
            ### INFO ------------------
                # lets make sure that the upline will not earn commisson on amount higher than what he paid for
                # if upline paid for 150 usd membership and investor paid for 500 usd membership
                # upline will only earn a commission from the 120 usd and not from 500 usd

                # if upline paid for 1,500 usd membership and investor paid for 500 usd membership
                # upline will earn a commission from the 500 usd and not from 1,500 usd
            ### END INFO ---------------

            # get upline membership level
            # -------------------------
            $uplineMembershipLevel = 'free';
            if ( $upline ) {
                $uplineMembershipLevel = $upline->membership;
            }

            # get upline membership pacakge info
            # -------------------------
            $uplineMembershipPkgName = ucwords($uplineMembershipLevel);
            $uplineMembershipPkgInfo = MembershipPackage::getSingePackageByName( $uplineMembershipPkgName );

            if ( $uplineMembershipPkgInfo ) {
                # this will make sure that the upline will not earn more than what he invested on
                if ( $uplineMembershipPkgInfo->membership_fee <= $amount ) {
                    $amount = $uplineMembershipPkgInfo->membership_fee;
                }
            }
        }
        # ----------------------------

        $tierCount = count($tierConfig);
        $refCreditPercent = (float)$tierConfig[$tierLevel]['referral_credit'] / 100;

        $refCreditAmount = $amount * $refCreditPercent;


        # on tier 2 and up, insert commissions only if membership is not free
        # ----------------------------
        $membershipArr = array( 'silver', 'gold', 'platinum' );

        if ( $tierLevel == 0 || in_array($upline->membership, $membershipArr) ) {

            if ( $packageCat == 'membership' && $upline->membership == 'free' ) {
                # free members will not earn referral credits from direct ferrals who are buying membership packages 

            }else{
                # insert commission / referral credit
                $note = array(
                    'amount' => $amount,
                    'ref_credit_percent' => $refCreditPercent,
                    'package_name' => $packageName,
                );
                $RefCreditParams = array(
                    'user_id' => $uplineUserId, // commission receiver
                    'downline_user_id' => $downlineUserId, // who referred the investor
                    'investor_user_id' => $investorUserId, // who invested
                    'package_id' => $packageId,
                    'fund_id' => $fundId,
                    'transaction_code'  => $transactionCode,
                    'amount' => $refCreditAmount,
                    'tier_level' => $tierLevel+1,
                    'status' => $status,
                    'note' => json_encode($note),
                    'created_at' => $date,
                );

                if ( $packageCat != '' ) {
                    $RefCreditParams['package_cat'] = $packageCat;
                }

                //pr( $RefCreditParams );
                DB::table( self::myTable() )->insert( $RefCreditParams );
            }
        }
        


        # compute next commission / referral credit
        # ----------------------------
        $params['tierLevel'] = $tierLevel+1;
        $params['downlineUserId'] = $uplineUserId;
        
        if ( $params['tierLevel'] < $tierCount ) {
            self::computeUplineRC( $params );
        }
    }

    public static function getSingleRefCred( $refCredId ){
        $refCred = DB::table( self::myTable() )->where([
                ['id', '=', $refCredId]
            ])
            ->first();

        return $refCred;
    }

    public static function getReferralCredits( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';
        $fundId = '';
        $packageCat = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }
        if( isset( $packageId ) && $packageId != 0 ){
            $sqlWhere[] = array('package_id', '=', $packageId );
        }
        if( isset( $fundId ) && $fundId != '' ){
            $sqlWhere[] = array('fund_id', '=', $fundId );
        }
        if( isset( $packageCat ) && $packageCat != '' ){
            $sqlWhere[] = array('package_cat', '=', $packageCat );
        }

        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function updateRefCredStatus( $commissionId ){
        $updateInfo = array(
            'status' => 'withdrawn'
        );
        DB::table( self::myTable() )
            ->where('id', $commissionId)
            ->update($updateInfo);
    }

    public static function getRefCredSumByCategory( $status, $category, $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['status', '=', $status],
                ['package_cat', '=', $category],
                ['user_id', '=', $userId]
            ])->sum('amount');

        return $sum;
    }

    public static function getRefCredSumByPackageId( $status, $category, $userId, $packageId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['status', '=', $status],
                ['package_cat', '=', $category],
                ['user_id', '=', $userId],
                ['package_id', '=', $packageId],
            ])->sum('amount');

        return $sum;
    }

    /**   
    * get overall sum
    */
    public static function getOverallSum( $userId ){
        $sum = DB::table( self::myTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'withdrawn']

            ])->sum('amount');

        return $sum;
    }






    # ==================================================
    # functions for tatals table

    public static function createRefCredTotals( $userId ){
        $refCredData = array(
            'user_id' => $userId,
            'raffle' => 0,
            'cryptotrading' => 0,
            'cryptomining' => 0,
            'membership' => 0,
            'cryptoexchange' => 0,
            'cryptoatm' => 0,
            'total' => 0
        );
        DB::table( self::myTotalsTable() )->insert( $refCredData );

        return (object) $refCredData;
    }

    public static function getRefCredTotals( $userId ){
        $refCred = DB::table( self::myTotalsTable() )->where([
                ['user_id', '=', $userId]
            ])
            ->first();

        if ( !$refCred ) {
            $refCred = self::createRefCredTotals( $userId );
        }

        return $refCred;
    }

    public static function updatePkgCatTotalRefCred( $packageCat, $total, $userId ){
        # check for existing refcred totals
        $rcTotals = self::getRefCredTotals( $userId );
        if ( !$rcTotals ) {
            self::createRefCredTotals( $userId );
        }

        $updateInfo = array(
            $packageCat => $total
        );

        $res = DB::table( self::myTotalsTable() )
            ->where('user_id', $userId)
            ->update($updateInfo);

        return $res;
    }

    public static function updateOverallTotal( $userId, $total ){
        $updateInfo = array(
            'total' => $total
        );

        $res = DB::table( self::myTotalsTable() )
            ->where('user_id', $userId)
            ->update($updateInfo);

        return $res;
    }
}

