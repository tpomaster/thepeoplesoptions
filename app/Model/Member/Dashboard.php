<?php

namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class Dashboard
{
	public static function myUserTable(){
        return 'users';
    }

    public static function myBankTable(){
        return 'users_bank';
    }

    public static function myAnnouncementsTable(){
        return 'announcements';
    }

    public static function myNewsTable(){
        return 'news';
    }

    public static function myFaqTable(){
        return 'faqs';
    }

    public static function myCrytoTradingTable(){
        return 'crypto_trading_user_trades';
    }

    public static function myCrytoMiningTable(){
        return 'crypto_mining_user_profits';
    }

    public static function myCrytoExchangeTable(){
        return 'crypto_exchange_user_profits';
    }

    public static function cryptoShuffleTable(){
        return 'crypto_shuffle_orders';
    }

    public static function myRefCreditsTable(){
        return 'referral_credits';
    }

    public static function miningFundsTable(){
        return 'crypto_mining_funds';
    }

    public static function tradingFundsTable(){
        return 'crypto_trading_funds';
    }

    public static function exchangeFundsTable(){
        return 'crypto_exchange_funds';
    }

    public static function membershipPowerBonusTable(){
        return 'membership_power_bonus';
    }

    public static function packageEarningsTable(){
        return 'package_earnings';
    }

    public static function dailyEarningsTable(){
        return 'member_daily_earnings';
    }

    public static function getUserDateCreated( $userId ){
        $start_date = DB::table( self::myUserTable() )
            ->where('id', '=', $userId)
            ->value('created_at');

        return $start_date;
    }

    public static function getTeamCount( $parentId ){
        $teams = DB::table( self::myUserTable() )
            ->where([
                ['parent_id', '=', $parentId]
            ])->count('id');

        return $teams;
    }

    public static function getUserBank( $userId ){
        $userBank = DB::table( self::myBankTable() )
            ->where([
                ['user_id', '=', $userId]
            ])
            ->first();

        if ( !$userBank ) {
            # create user bank
            $bankData = array(
                'user_id' => $userId,
                'completed_in' => 0,
                'completed_out' => 0,
                'pending_in' => 0,
                'pending_out' => 0,

                'referral_credit' => 0,
                'package_earning' => 0,
                'deposit' => 0,
                'closed_package' => 0,

                'package_purchase' => 0,
                'cap_off_trade_amt' => 0,
                'payout' => 0,

                'available_bal' => 0,
            );
            DB::table( self::myBankTable() )->insert( $bankData );

            $userBank = (object) $bankData;
        }

        return $userBank;
    }

    public static function getDailyTotalEarning( $userId, $date ){
        $packageEarnings = DB::table( self::packageEarningsTable() )
            ->where([
                ['user_id', '=', $userId]
            ])
            ->whereDay('withdraw_date', '=', date('d', strtotime($date)))
            ->whereMonth('withdraw_date', '=', date('m', strtotime($date)))
            ->whereYear('withdraw_date', '=', date('Y', strtotime($date)))
            ->sum('amount');

        $refCreditsTotal = DB::table( self::myRefCreditsTable() )
            ->where([
                ['user_id', '=', $userId]
            ])
            ->whereDay('created_at', '=', date('d', strtotime($date)))
            ->whereMonth('created_at', '=', date('m', strtotime($date)))
            ->whereYear('created_at', '=', date('Y', strtotime($date)))
            ->sum('amount');

        $totalEarnings = $packageEarnings + $refCreditsTotal;

        return $totalEarnings;
    }

    public static function checkDailyEarnings( $userId ){
        $dataChecker = DB::table( self::dailyEarningsTable() )
            ->where('user_id', '=', $userId)
            ->count();

        return $dataChecker;
    }

    public static function insertDailyEarnings( $userId, $currentDate ){
        $totalEarnings = 0;

        $dateCheck = DB::table( self::dailyEarningsTable() )
            ->where('user_id', '=', $userId)
            ->whereDay('date', '=', date('d', strtotime($currentDate)))
            ->whereMonth('date', '=', date('m', strtotime($currentDate)))
            ->whereYear('date', '=', date('Y', strtotime($currentDate)))
            ->first();

        if ( !$dateCheck ) {
            $totalEarnings = self::getDailyTotalEarning( $userId, $currentDate );

            $MemDailyEarningsParams = array(
                'user_id' => $userId, // user id
                'date' => $currentDate, // current date
                'total_earnings' => $totalEarnings, // total earnings
            );

            DB::table( self::dailyEarningsTable() )->insert( $MemDailyEarningsParams );
        } 
           
        return $totalEarnings;
    }

    public static function getLatestDailyEarnings( $userId ){
        $dateChecker = DB::table( self::dailyEarningsTable() )
            ->where('user_id', '=', $userId)
            ->latest('date')
            ->first();

        return $dateChecker->date;
    }


    public static function getGraphData( $userId ) {
        $chartData = DB::table( self::dailyEarningsTable() )
            ->select('date', 'total_earnings')
            ->where('user_id', '=', $userId)
            ->orderBy('date','ASC')
            ->get();

        return $chartData;
    }


    public static function getDashboardPie( $userId ){
        $miningFunds = DB::table( self::miningFundsTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('amount');

        $tradingFunds = DB::table( self::tradingFundsTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('amount');

        $exchangeFunds = DB::table( self::exchangeFundsTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('amount');

        $shuffleFunds = DB::table( self::cryptoShuffleTable() )
            ->where([
                ['user_id', '=', $userId]
            ])->sum('total_amount');

        return $pieData = [
            'miningFunds' => $miningFunds,
            'tradingFunds' => $tradingFunds,
            'exchangeFunds' => $exchangeFunds,
            'shuffleFunds' => $shuffleFunds
        ];
    }

    public static function getAnnouncements( $params ){
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortOrder = ' ASC';

        $announcements = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        # get count
        $annCount = DB::table( self::myAnnouncementsTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $annList = DB::table( self::myAnnouncementsTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $annList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $annCount,
            'list' => $annList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function getNews( $params ){
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortOrder = ' DESC';

        $news = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        # get count
        $newsCount = DB::table( self::myNewsTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $newsList = DB::table( self::myNewsTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $newsList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $newsCount,
            'list' => $newsList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }

    public static function getFaqs( $params ){
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortOrder = ' DESC';

        $faqs = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        # get count
        $faqCount = DB::table( self::myFaqTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $faqList = DB::table( self::myFaqTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $faqList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $faqCount,
            'list' => $faqList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }


    public static function getReminders( $userId ){
        $tradingProfit = DB::table( self::myCrytoTradingTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed'],
                ['trade_earning', '>', 0]
            ])
            ->count('user_profit');

        $miningProfit = DB::table( self::myCrytoMiningTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->count('user_profit');

        $exchangeProfit = DB::table( self::myCrytoExchangeTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->count('net_profit');

        $RefCreditsProfit = DB::table( self::myRefCreditsTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->count('amount');

        $powerBonus = DB::table( self::membershipPowerBonusTable() )
            ->where([
                ['user_id', '=', $userId],
                ['status', '=', 'completed']
            ])
            ->count('amount');

        $profitsReminder = array(
            'tradingProfit' => $tradingProfit,
            'miningProfit' => $miningProfit,
            'exchangeProfit' => $exchangeProfit,
            'RefCreditsProfit' => $RefCreditsProfit,
            'powerBonus' => $powerBonus
        );

        return $profitsReminder;
    }


    public static function getAnnouncementsAds( $userId ) {

        $visitChecker = DB::table( self::myUserTable() )
            ->where([
                ['id', '=', $userId]
            ])
            ->value('last_login');

        if ( empty($visitChecker) ) {
            $latestPopups = DB::table( self::myAnnouncementsTable() )
                ->orderBy('id','DESC')
                ->limit(5)
                ->get();

            $countPopups = DB::table( self::myAnnouncementsTable() )
                ->count('id');

            $popupsData = array(
                'latestPopups' => $latestPopups,
                'countPopups' => 5
            );   

            return $popupsData;

        } else {
            $getLastLogin = $visitChecker;

            $latestPopups = DB::table ( self::myAnnouncementsTable() )
                ->whereDate('created_at', '>', $getLastLogin)
                ->get();

            $countPopups = DB::table ( self::myAnnouncementsTable() )
                ->whereDate('created_at', '>', $getLastLogin)
                ->count('id');


            $popupsData = array(
                'latestPopups' => $latestPopups,
                'countPopups' => $countPopups
            ); 

            return $popupsData;

        }
    }

    public static function updateLastLogin ( $userId, $curDate ) {

        DB::table ( self::myUserTable() )
            ->where([
                ['id', '=', $userId]
            ])
            ->update(['last_login' => $curDate]);
    }

    public static function getLastLogin ( $userId ) {

        $last_login = DB::table( self::myUserTable() )
            ->where('id', '=', $userId)
            ->value('last_login');

        return $last_login;
    }

}
