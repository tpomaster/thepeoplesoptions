<?php

namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class InvestmentLoop
{
    public static function myUsersBankTable(){
        return 'users_bank';
    }

    // public static function getUsersPayment( $userId ){
    // 	$checkBalance = DB::table( self::myUsersBankTable() )
    //         ->where('user_id', '=', $userId)
    //         ->value('available_bal');

    //     return $checkBalance;
    // }

    public static function getUsersPayment( $params ){
        $qpage = 1;
        $qlimit = 10;
        $sortBy = 'id';
        $sortOrder = ' DESC';
        $id = '';
        $userId = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();
        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }

        # get count
        $trCount = DB::table( self::myUsersBankTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myUsersBankTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $trList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $trCount,
            'list' => $trList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }
}
