<?php

namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class Faq
{
    public static function myFaqTable(){
        return 'faqs';
    }

    public static function getFaqs( $params ){
        $qpage = 1;
        $qlimit = 5;
        $sortBy = 'id';
        $sortOrder = ' DESC';

        $faqs = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # pagination
        # hack for skip parameter
        if ( $qpage > 0 ) {
            $qpage--;
        }
        $skip = $qpage*$qlimit;
        $take = $qlimit;

        # where statement
        $sqlWhere  = array();

        # get count
        $faqCount = DB::table( self::myFaqTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $faqList = DB::table( self::myFaqTable() )
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->paginate($qlimit);

        $pageLinks = $faqList->appends([
            'sortBy' => $sortBy, 
            'sortOrder' => $sortOrder,
            'qpage' => $qpage,
            'page' => $qpage,
            'qlimit' => $qlimit,
        ])->links();

        $ret = array(
            'count' => $faqCount,
            'list' => $faqList,
            'pageLinks' => $pageLinks
        );

        return $ret;
    }
}
