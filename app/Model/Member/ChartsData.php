<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class ChartsData
{
    // packages graph
	public static function tradingPackagesEarningsTable(){
        return 'crypto_trading_trades';
    }

    public static function miningPackagesEarningsTable(){
        return 'crypto_mining_profits';
    }

    public static function exchangePackagesEarningsTable(){
        return 'crypto_exchange_profits';
    }

    public static function getTradingPackageGraphData( $packageId ) {
        $packageChartData = DB::table( self::tradingPackagesEarningsTable() )
            ->select('date', 'profit_percent')
            ->where('package_id', '=', $packageId)
            ->orderBy('date','ASC')
            ->get();

        return $packageChartData;
    }

    public static function getMiningPackageGraphData( $packageId ) {
        $packageChartData = DB::table( self::miningPackagesEarningsTable() )
            ->select('date', 'profit_percent')
            ->where('package_id', '=', $packageId)
            ->orderBy('date','ASC')
            ->get();

        return $packageChartData;
    }

    public static function getExchangePackageGraphData( $packageId ) {
        $packageChartData = DB::table( self::exchangePackagesEarningsTable() )
            ->select('date', 'profit_percent')
            ->where('package_id', '=', $packageId)
            ->orderBy('date','ASC')
            ->get();

        return $packageChartData;
    }
}
