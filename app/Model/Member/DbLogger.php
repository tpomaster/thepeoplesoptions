<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;

class DbLogger {
    private $table = 'log_any';
    private $ipnTable = 'log_ipn';

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public function setIPNDbTable( $dbTable ) {
        $this->ipnTable = $dbTable;
    }

    public static function logAny( $key, $val, $section = 'xxx' ) {
        $me = new self();

        $ins = array(
            'section' => $section,
            'key' => $key,
            'val' => $val,
            'date' =>  date('Y-m-d H:i:s')
        );
        return DB::table($me->table)->insert( $ins );
    }

    public static function logIPN( $params ) {
        $me = new self();

        $gateway = 'x';
        $trCode = 'x';
        $gTrCode = 'x';
        $gUserId = 'x';
        $status = '';
        $ipn = array();

        if ( $params ) {
            extract($params);
        }

        if ( is_array($ipn) || is_object ($ipn) ) {
            $ipn = serialize($ipn);
        }

        $ins = array(
            'gateway' => $gateway,
            'transaction_code' => $trCode,
            'gateway_transaction_code' => $gTrCode,
            'gateway_user_id' => $gUserId,
            'status' => $status,
            'date' =>  date('Y-m-d H:i:s'),
            'ipn_data' => $ipn
        );
        return DB::table($me->ipnTable)->insert( $ins );
    }

    public function getLog( $id ) {
        $ret = DB::table($this->table)->where([
            ['id', '=', $id]
        ])->first();

        return $ret;
    }

    public function getLogByField( $field, $value ) {
        $ret = DB::table($this->table)->where([
            [$field, '=', $value]
        ])->first();

        return $ret;
    }


    public function getIPNLog( $id ) {
        $ret = DB::table($this->table)->where([
            ['id', '=', $id]
        ])->first();

        return $ret;
    }

    public function getIPNLogByField( $field, $value ) {
        $ret = DB::table($this->table)->where([
            [$field, '=', $value]
        ])->first();

        return $ret;
    }
}