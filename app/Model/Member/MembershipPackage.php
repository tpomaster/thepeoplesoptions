<?php
namespace App\Model\Member;

use Illuminate\Support\Facades\DB;


class MembershipPackage{
    private $table = 'membership_packages';
    private $paymentsTable = 'membership_payments';
   
    private $userId = 0;

    private $me = false;

    public function __construct() {

    }

    public function setDbTable( $dbTable ) {
        $this->table = $dbTable;
    }

    public static function myTable(){
        return 'membership_packages';
    }
    public static function myPaymentsTable(){
        return 'membership_payments';
    }


    public static function getPackages( $params ) {
        $sortBy = 'order';
        $sortOrder = ' ASC';
        $id = '';
        $userId = '';

        $bankTrnx = false;

        # overwrite default
        if( !empty( $params ) ){
            extract($params);
        }

        # where statement
        $sqlWhere  = array();
        $sqlWhere[] = array('hide', '=', 0 );

        if( $id != '' && $id != 0 ){
            $sqlWhere[] = array('id', '=', $id );
        }
        if( $userId != '' && $userId != 0 ){
            $sqlWhere[] = array('user_id', '=', $userId );
        }
        if( isset( $status ) && $status != '' ){
            $sqlWhere[] = array('status', '=', $status );
        }

        # get count
        $trCount = DB::table( self::myTable() )
            ->where($sqlWhere)
            ->count();

        # get list
        $trList = DB::table( self::myTable() )
            ->select('id', 'name', 'membership_fee', 'duration', 'status')
            ->where($sqlWhere)
            ->orderBy($sortBy, $sortOrder)
            ->get();


        return $trList;
    }

    public static function getSingePackage( $packageId ){
        $pkg = DB::table( self::myTable() )->where([
            ['id', '=', $packageId]
        ])->first();

        return $pkg;
    }

    public static function getPackageByIds( $packageIds )
    {
        if ( empty($packageIds) ) {
            return false;
        }

        $pkg = DB::table( self::pkgTable() )
            ->select('id', 'name')
            ->whereIn('id', $packageIds)
            ->get();

        return $pkg;
    }

    public static function getSingePackageByName( $packageName ){
        $pkg = DB::table( self::myTable() )->where([
            ['name', '=', $packageName]
        ])->first();

        return $pkg;
    }


    # Membership Payments
    public static function insertMembershipPayment( $params ){
        $ret = array(
            'isError' => false,
            'msg' => array(),
            'hasPrevMembership' => false
        );

        # expected parameters, default values
        $userId = 0;
        $packageId = 0;
        $amount = 0;
        $date = date('Y-m-d H:i:s');
        $status = 'pending';
        $transactionCode = '';
        $membershipLevel = 'silver';

        # other important parameters
        $packageFundId = 0;
        $hasInvestment = false;

        # initial variable values above will be replaced if they exist in the $params data
        if( !empty( $params ) ){
            $ret['params_arr'] = $params;
            extract($params);
        }

        # check if the user already have membership
        $activeMembership = self::checkActiveMembership( $packageId, $userId );

        if ( $activeMembership ) {
            # end active membership
            $updateInfo = array(
                'status' => 'closed'
            );
            DB::table( self::myPaymentsTable() )
                ->where('id', $activeMembership->id)
                ->update($updateInfo);
        }

        # this is the user's first time to fund this package
        # insert package fund or user investment
        $newMembership = array(
            'user_id' => $userId,
            'transaction_code' => $transactionCode,
            'package_id' => $packageId,
            'amount' => $amount,
            'membership_level' => $membershipLevel,
            'created_at' => $date,
            'status' => $status,
            'start_date' => $date,
            'end_date' => date ("Y-m-d", strtotime("+1 year", strtotime($date)))
        );
        $packageFundId = DB::table( self::myPaymentsTable() )->insertGetId( $newMembership );
        $ret['packageFundId'] = $packageFundId;

        if( $packageFundId ){
            $ret['msg'] = 'success';

        }else{
            $ret['isError'] = true;
            $ret['msg'] = 'failed';
        }

        return $ret;
    }

    public static function getUserInvestment( $packageId, $userId ){
        $pkgFund = DB::table( self::myFundsTable() )->where([
            ['package_id', '=', $packageId],
            ['user_id', '=', $userId]
        ])->first();

        return $pkgFund;
    }

    public static function checkActiveMembership( $packageId, $userId, $status = 'completed' ) {
        $pkgFund = DB::table( self::myPaymentsTable() )->where([
            ['package_id', '=', $packageId],
            ['user_id', '=', $userId],
            ['status', '=', $status]
        ])->first();

        return $pkgFund;
    }

}

