<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Model\Admin\Admin' => 'App\Policies\Admin\AdminPolicy',
        'App\Model\Admin\Role' => 'App\Policies\Admin\RolePolicy',
        'App\Model\Admin\Permission' => 'App\Policies\Admin\PermissionPolicy',
        'App\Model\Admin\Affiliate' => 'App\Policies\Admin\AffiliatePolicy',
        'App\Model\Admin\UserBank' => 'App\Policies\Admin\UserBankPolicy',
        'App\Model\Admin\ReferralCredit' => 'App\Policies\Admin\ReferralCreditPolicy',
        'App\Model\Admin\Payout' => 'App\Policies\Admin\PayoutPolicy',
        'App\Model\Admin\CryptoTradingPackage' => 'App\Policies\Admin\CryptoTradingPackagePolicy',
        'App\Model\Admin\CryptoMiningPackage' => 'App\Policies\Admin\CryptoMiningPackagePolicy',
        'App\Model\Admin\CryptoExchangePackage' => 'App\Policies\Admin\CryptoExchangePackagePolicy',
        'App\Model\Admin\CryptoShufflePackage' => 'App\Policies\Admin\CryptoShufflePackagePolicy',
        'App\Model\Admin\MembershipPackage' => 'App\Policies\Admin\MembershipPackagePolicy',
        'App\Model\Admin\News' => 'App\Policies\Admin\NewsPolicy',
        'App\Model\Admin\Announcement' => 'App\Policies\Admin\AnnouncementPolicy',
        'App\Model\Admin\Faq' => 'App\Policies\Admin\FaqPolicy',
        'App\Model\Admin\Setting' => 'App\Policies\Admin\SettingPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Gate::resource('admin', 'App\Policies\Admin\AdminPolicy');
        /*Gate::before(function ($admin) {
            return $admin->hasRole('super-admin');
        });*/
    }
}
