<?php
namespace App\Helpers;

use App\Helpers\CoinPaymentsAPI;
use Illuminate\Support\Facades\DB;

use App\Model\Member\UserBank;
use App\Model\Member\UserbankFund;

class CoinPaymentsPG {
	private $minAmount = 300;
	private $table = 'lpj_bank_fund_request';

	private $merchantId = 'c6f6a5bc521cd3cf773af18545c0041e';
    private $publicKey = 'b21fcdcda3d9b5e7e869d919287f2c298098a9d0e24d367a4b3a47fa01bf7839';
    private $privateKey = '1337c1191A7644054ce6B933b024Dfb8a456F9481c2e1d17b5f0C16c56B8A4f2';

	public function trDescription(){
		return 'Add Fund via Bitcoin (CoinPayments)';
	}

	public function isValidMinAmount( $amount ){
		if( $amount < $this->minAmount ){
			return false;
		}else{
			return true;
		}
	}

	public function getMinAmount(){
		return $this->minAmount;
	}

	public function generatePaymentButton( $params = array() ){
		
        $itemName = $this->trDescription();
        $amount = 0;
        $itemNumber = 0; # package id
        $userId = 'yyy'; # user id
        $invoice = 0; # transaction code
        $trCode = 'zzz';

        $userEmail = 'litopj@yahoo.com';
        $userLogin = 'litopj';

        $ipnCount = 0;

        if ( !empty($params) ) {
            extract($params);
        }


        $returnUrl = url('/').'/member/tpo-bank';
        $cancelUrl = url('/').'/member/cancel-addfund/?cnl='.$trCode;
        $ipnUrl = url('/').'/process-ipn';


        //$cps = new CoinPaymentsAPI();
        //$cps->Setup( $this->privateKey, $this->publicKey );

        $req = array(
            'amount' => $amount,
            'currency1' => 'USD',
            'currency2' => 'BTC',
            'buyer_email' => $userEmail,
            'buyer_name' => $userLogin,
            'item_name' => $itemName,
            'item_number' => $itemNumber,
            'invoice' => $invoice,
            'custom' => $userId,
            'ipn_url' => $ipnUrl,
        );

        //$result = $cps->CreateTransaction($req);

        $result = array(
        	'error' => 'ok',
        	'result' => array(
        		'txn_id' => 'xxx',
        		'amount' => '0.12345',
        		'address' => 'asdf',
        		'qrcode_url' => 'http://cdnqrcgde.s3-eu-west-1.amazonaws.com/wp-content/uploads/2013/11/jpeg.jpg',
        		'status_url' => 'http://litopj.com'
        	)
        );

        $buttonScript = '';
        if ( $result['error'] == 'ok' ) { 
        	ob_start();
        	?>
            <div id="coinpayments_form">
                <div class="invoice">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>
                                    Transaction Created with ID:
                                </th>
                                <td>
                                    <?php echo $result['result']['txn_id']; ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Total Amount to Send:
                                </th>
                                <td>
                                    <span class=""><?php echo sprintf('%.08f', $result['result']['amount']) ?></span><span class="">&nbsp;BTC</span>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Send to this BTC Address:
                                </th>
                                <td>
                                    <?php echo $result['result']['address']; ?>
                                    <div style="margin-top: 10px;">
                                        <img src="<?php echo $result['result']['qrcode_url']; ?>" alt="If this QR code failed to load on your browser, please click the button below if you want to scan the QR code.">
                                    </div>
                                    <div style="margin-top: 10px;">
                                    	<a class="btn btn-primary btn-sm" target="_blank" href="<?php echo $result['result']['status_url']; ?>" target="_blank">View QR Code</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Your TPO Bank will Receive:
                                </th>
                                <td>
                                    <?php echo $amount; ?> USD
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Monitor Transaction Status here:
                                </th>
                                <td>
                                    <a class="btn btn-primary btn-sm" target="_blank" href="<?php echo $result['result']['status_url']; ?>" target="_blank">STATUS URL</a>
                                </td>
                            </tr>
                        </table>

                        <p class="coinpayments_btn_wrap" style="text-align: center;">
                            <a class="btn btn-danger btn-lg cancel_addfund_btn" data-cnl="<?php echo $trCode; ?>" data-item="<?php echo $itemNumber ?>" href="<?php echo $cancelUrl; ?>">
                                Cancel Transaction
                            </a>
                        </p>
                    </div>
                </div>
            </div><?php
            $buttonScript = ob_get_contents();
            ob_end_clean();

        }else{ 
        	ob_start();
        	?>
            <div class="callout callout-danger">
                <h4>Error. </h4>
                <p>
                    <strong>Message: </strong><?php echo $result['error']; ?>
                </p>
            </div><?php
            $buttonScript = ob_get_contents();
            ob_end_clean();
        }

        return $buttonScript;
	}

	private function generateHMAC( $ipn ){
		$s = ':';
        $ipnSecret = 'tpoisthebest';
        return hash_hmac('sha512', $ipn, $ipnSecret);
	}

	public static function processIPN( $ipn, $validateHMAC = true ){
		$me = new self();

        $status = 'pending';

        $ret = array(
            'isError' => false,
            'msg' => '',
            'data' => ''
        );

        # custom = user_id
        # item_number = package_id
        # invoice = transaction_code
        # txn_id = coinpayments transaction id
        
        # make sure the needed parameters are completed
        if ( !isset($ipn['invoice']) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing invoice.';
        }

        if ( !isset($ipn['txn_id']) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'Missing transaction id.';
        }
        
        # get transaction
        if( $ret['isError'] == false ){
            $paymentTrnx = UserbankFund::getSingleTransactionStatic('transaction_code', $ipn['invoice']);

            if( !$paymentTrnx ){
                $ret['isError'] = true;
                $ret['msg'][] = 'Transaction not found.';
            }
        }

        # also lets make sure that txn_id is only used once
        if( $ret['isError'] == false ){
            $isDuplicate = UserbankFund::checkDuplicateTransaction( $ipn['txn_id'], $paymentTrnx->transaction_code );

            if( $isDuplicate > 0 ){
                $ret['isError'] = true;
                $ret['msg'][] = 'A duplicate transaction was detected.';
            }
        }

        # validate amount
        if( $ret['isError'] == false ){
            if( $ipn['amount1'] != $paymentTrnx->amount ){
                $ret['isError'] = true;
                $ret['msg'][] = 'Data alteration detected. Please contact us.';
            }
        }

        # validate user
        if( $ret['isError'] == false ){
            if( $ipn['custom'] != $paymentTrnx->user_id ){
                $ret['isError'] = true;
                $ret['msg'][] = 'User ID match error.';
            }
        }

        # hmac validation
        if( $ret['isError'] == false ){
            if ( $validateHMAC ) {
                $request = file_get_contents('php://input');
                $hmac = 'xxx';

                if ( $request ) {
                    $hmac = $me->generateHMAC( $request );
                }

                if ( !isset($_SERVER['HTTP_HMAC']) ) {
                    $ret['isError'] = true;
                    $ret['msg'] = 'Missing HMAC Signature.';

                }elseif( $_SERVER['HTTP_HMAC'] != $hmac ){
                    $ret['isError'] = true;
                    $ret['msg'][] = 'HMAC signature match error.';
                }
            }
        }

        # status
        if( $ret['isError'] == false ){
            if( $ipn['status'] == 0 && $ipn['status_text'] == 'Waiting for buyer funds...' ){
                $status = 'processing';

                $ret['isError'] = true;
                $ret['msg'][] = 'Waiting for buyer funds...';

            }elseif( $ipn['status'] >= 100 ){
                $status = 'completed';
                
            }else{
                $ret['isError'] = true;
                $ret['msg'][] = $ipn['status_text'];
            }
        }

        # update payment transaction/order
        if( $ret['isError'] == false ){

            if ( $status == 'completed' ) {
                $params = array(
                    'gateway' => 'coinpayments',
                    'status' => strtolower( $status ),
                    'gateway_tr_id' => $ipn['txn_id'],
                    'currency' => $ipn['currency2'],
                    'details' => json_encode($ipn),
                );
                UserbankFund::updateTransaction( $params, $paymentTrnx->id );
                UserBank::updateTransactionStatusStatic( $paymentTrnx->transaction_code, 'completed' );
            }

            $ret['id'] = $paymentTrnx->id;
            $ret['status'] = $status;
            $ret['paymentTrnx'] = $paymentTrnx;
            $ret['ipnData'] = $ipn;
        }

        return $ret;
	}
}