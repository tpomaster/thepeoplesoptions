<?php
namespace App\Helpers;

class TpoMarketingAPI {
	public static function config( $k ){
        $config = array(
            'api_url' => 'https://thepeoplesoptions.com/tpomarketing',
            'api_key1' => '7F93BB78E7045F54217EF98FF7777420',
            'api_key2' => '329F8A802E88CD199B8A23D16F6E6EF2',
            'api_key3' => 'BC883FE6B7E8B1E7FCF95E1BD7AD24F9', # update contact activity
            'api_key4' => 'DBA081075B78A0C583430A66D03CBE8E'
        );

        if ( config('app.is_sandbox') == true ) {
            $config['api_url'] = 'https://thepeoplesoptions.com/tpomarketing/sandbox';
            $config['api_key1'] = 'CEECEF99C7E666B5836FE7AB09293806';
            $config['api_key2'] = '329F8A802E88CD199B8A23D16F6E6EF2';
            $config['api_key3'] = 'F24DB28F32908C92C2E2B9D8F0E84B5F'; # update contact activity
            $config['api_key4'] = 'DBA081075B78A0C583430A66D03CBE8E';
        }

        return $config[$k];
    }

    public static function createAccount( $userdata ){
        $APIendpoint = self::config('api_url').'/tpoconnect/';
        $APIkey = self::config('api_key2');
        
        $action = 2;  # 1 = login; 2: register 3: update contact activity
        $username = 'test';
        $password = 'xxx';
        $membership = 'free';
        $role = 'member';
        $email = 'test@y.com';
        $firstname = 'fname';
        $lastname = 'lname';
        $phone = '12345';
        $address = '';
        $city = '';
        $state = '';
        $zipcode = '';
        $country = '';
        $password = '';

        if ( !empty($userdata) ) {
            extract($userdata);
        }

        $params = array(
            'apikey' => $APIkey,
            'action' => $action,

            'role' => $role,
            'username' => $username,
            'password' => $password,
            'membership' => $membership,

            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'phone' => $phone,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zipcode' => $zipcode,
            'country' => $country,
        );

        $apiParams = $APIendpoint.'?'.http_build_query($params);
        //dd($apiParams);

        $ret = LpjHelpers::curlGet2( $apiParams );

        if ( $ret ) {
            $ret = json_decode( $ret );
        }

        return $ret;
    }


    public static function updatePersonalInfo( $userdata ){
        $APIendpoint = self::config('api_url').'/tpo-change/';
        $APIkey = self::config('api_key4');
        
        $action = 1; # 1 = update personal info; 2: change membership
        $username = 'test';
        $password = '';

        $email = 'test@y.com';
        $firstname = 'fname';
        $lastname = 'lname';
        $phone = '12345';
        $address = '';
        $city = '';
        $state = '';
        $zipcode = '';
        $country = '';
        
        if ( !empty($userdata) ) {
            extract($userdata);
        }

        $params = array(
            'apikey' => $APIkey,
            'action' => $action,
            'username' => $username,
            'password' => $password,

            'email' => $email,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'phone' => $phone,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'zipcode' => $zipcode,
            'country' => $country,
        );

        $apiParams = $APIendpoint.'?'.http_build_query($params);
        //dd($apiParams);

        $ret = LpjHelpers::curlGet2( $apiParams );

        if ( $ret ) {
            $ret = json_decode( $ret );
        }

        return $ret;
    }

    public static function login( $userdata ){
        $APIendpoint = self::config('api_url').'/tpoconnect/';
        $APIkey = self::config('api_key1');
        
        $action = 1; # 1 = login; 2: register 3: update contact activity
        $username = 'test';
        $password = '';
        $membership = 'free';

        if ( !empty($userdata) ) {
            extract($userdata);
        }

        $params = array(
            'apikey' => $APIkey,
            'action' => $action,
            'username' => $username,
            'password' => $password,
            'membership' => $membership,
        );

        $apiParams = $APIendpoint.'?'.http_build_query($params);
        //dd($apiParams);

        $ret = LpjHelpers::curlGet2( $apiParams );

        if ( $ret ) {
            $ret = json_decode( $ret );
        }

        return $ret;
    }


    public static function contactRegistered( $uplineUsername = '', $email = '' ){
        $APIendpoint = self::config('api_url').'/tpoconnect/';
        $APIkey = self::config('api_key3');
        $action = 3; # 1: login | 2: register | 3: update contact activity

        $regLink = url('/').'/member/'.$uplineUsername.'/register';

        $params = array(
            'apikey' => $APIkey,
            'action' => $action,
            'reg_link' => $regLink,
            'email' => $email,
        );

        $apiParams = $APIendpoint.'?'.http_build_query($params);
        //dd($apiParams);

        $ret = LpjHelpers::curlGet2( $apiParams );

        if ( $ret ) {
            $ret = json_decode( $ret );
        }

        return $ret;
    }
};

