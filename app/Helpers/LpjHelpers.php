<?php
namespace App\Helpers;

class LpjHelpers{
    public static function pr( $a, $b = 'var name' ) {
        print( '<pre>' ); echo "<div>".$b."</div>"; print_r( $a ); print( '</pre><br>' );
    }

    # filter sanitize string
    public static function fss( $str ){
        if ( is_array( $str ) ) {
            $ret_arr = array();

            foreach ($str as $k => $v) {
                if ( is_array($v) ) {
                    $ret_arr[$k] = fss( $v );

                }else{
                    $str = trim($v);
                    $str2 = filter_var( $str, FILTER_SANITIZE_STRING );

                    # if numeric, remove comma
                    $num = str_replace( ',', '', $str2 );
                    if( is_numeric($num) ){
                        $str2 = $num;
                    }

                    $ret_arr[$k] = $str2;
                }
            }

            return $ret_arr;
                
        }else{
            $str = trim($str);
            $str2 = filter_var( $str, FILTER_SANITIZE_STRING );

            # if numeric, remove comma
            $num = str_replace( ',', '', $str2 );
            if( is_numeric($num) ){
                $str2 = $num;
                
            }

            return $str2;
        }
    }


    public static function lpjGenerateRandString( $length = 8 ) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public static function lpjGenerateRandString2( $length ) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public static function lpjGenerateRandString3( $length ) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public static function lpjGenerateRandNum( $length ) {
        $characters = '0123456789';
        
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public static function generateTrCode(){
        return time().self::lpjGenerateRandString(8);
    }


    public static function lpjValidateUsername( $username, $ret = false ){
        if ( !$ret ) {
            $ret = array(
                'isError' => false,
                'msg' => ''
            );
        }
        
        $allowed = array("-", "_");
        $valid = false;

        # check if empty
        if ( empty( $username ) ) {
            $ret['isError'] = true;
            $ret['msg'][] = 'USERNAME must not be empty.';        
        }

        # check length
        if( $ret['isError'] == false ){
            if( strlen( $username ) <= 3 ){
                $ret['isError'] = true;
                $ret['msg'][] = 'USERNAME Must be more than 3 characters.';

            }elseif( $username > 20 ){
                $ret['isError'] = true;
                $ret['msg'][] = 'USERNAME Must not be more than 20 characters.';
            }
        }

        # check first character
        if( $ret['isError'] == false ){
            $first_char = substr( $username, 0, 1 );

            if ( !ctype_alpha($first_char) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'First character of your USERNAME must be an alphabet.';
            }
        }

        # check characters
        if( $ret['isError'] == false ){
            if ( !ctype_alnum( str_replace($allowed, '', $username ) ) ) {
                $ret['isError'] = true;
                $ret['msg'][] = 'Only alphabets and numbers are allowed for USERNAME.';
            }
        }
        
        return $ret;
    }

    public static function lpjCheckPasswordStrength( $password, $ret = false ) {
        if ( !$ret ) {
            $ret = array(
                'isError' => false,
                'msg' => ''
            );
        }

        if (!preg_match_all('$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$', $password)){
            $ret['msg'][] = "Password must be atleast 8 characters containing atleast 1 lowercase letter 
                and at least one uppercase letter and at least one number and at least a special character (non-word characters";
            $ret['isError'] = true;
        }

        return $ret;
    }


    public static function lpjIsEmail( $email, $ret = false ) {
        if ( !$ret ) {
            $ret = array(
                'isError' => false,
                'msg' => ''
            );
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $ret['msg'][] = "Invalid email format.";
            $ret['isError'] = true;
        }

        return $ret;
    }


    public static function num( $num, $comma = true ){
        if ( !$comma ) {
            return number_format( (float)$num, 0, '.', '' );
        }else{
            return number_format( (float)$num, 0, '.', ',' );
        }
    }

    public static function amt2( $num, $comma = true ){
        $num = bcdiv($num, 1, 2);

        if ( !$comma ) {
            return number_format( (float)$num, 2, '.', '' );
        }else{
            return number_format( (float)$num, 2, '.', ',' );
        }
    }
    public static function amt3( $num ){
        $num = bcdiv($num, 1, 3);
        return number_format( (float)$num, 3, '.', ',' );
    }
    public static function amt4( $num ){
        $num = bcdiv($num, 1, 4);
        return number_format( (float)$num, 4, '.', ',' );
    }

    public static function nice_date( $date_str ){
        if ( $date_str == '0000-00-00 00:00:00' || $date_str == '0000-00-00') {
            return false;
        }
        
        $d = date( 'M. d Y', strtotime( $date_str ) );

        if ( $d == 'Jan. 01 1970' ) {
            return $date_str;

        }else{
            return $d;
        }
    }

    public static function niceDate( $dateStr ){
        if ( $dateStr == '0000-00-00 00:00:00' || $dateStr == '0000-00-00') {
            return false;
        }
        
        $d = date( 'M. d Y', strtotime( $dateStr ) );

        if ( $d == 'Jan. 01 1970' ) {
            return $dateStr;

        }else{
            return $d;
        }
    }

    // get the date range of the week which the [$date] belongs to
    public static function getWeekDateRange( $startDate ){
        $startDay = date( 'l', strtotime( $startDate ) );

        if( $startDay == 'Monday' ){
            $dateFrom = date( 'Y-m-d', strtotime( $startDate ) );
        }else{
            $dateFrom = date('Y-m-d', strtotime( "previous monday", strtotime($startDate) ) );
        }

        if( $startDay == 'Sunday' ){
            $dateTo = date('Y-m-d', strtotime( $startDate ) );
        }else{
            $dateTo = date('Y-m-d', strtotime( "next sunday", strtotime($startDate) ) );
        }

        $ret['dateFrom'] = $dateFrom;
        $ret['dateTo'] = $dateTo;

        return $ret;
    }

    public static function addDays( $date, $days ){
        $date = strtotime("+".$days." days", strtotime($date));
        return  date("Y-m-d", $date);
    }

    public static function dbDate($dateStr = ''){
        if ( $dateStr == '0000-00-00 00:00:00' || $dateStr == '0000-00-00') {
            return date('Y-m-d H:i:s');
            
        }elseif ( $dateStr == '') {
            return date('Y-m-d H:i:s');
        
        }else{
            return date('Y-m-d H:i:s', strtotime($dateStr));
        }
    }

    public static function asset($path, $secure = null){
        $assetVersion = config('view.asset_version');
        return app('url')->asset($path.'?v='.$assetVersion, $secure);
    }


    public static function generateName(){
        $p = array(
            'ba', 'be', 'bi', 'bo', 'bu', 'by', 'bie', 'bou',
            'ca', 'ce', 'ci', 'co', 'cu', 'cy', 'cie', 'cou',
            'da', 'de', 'di', 'do', 'du', 'dy', 'die', 'dou',
            'fa', 'fe', 'fi', 'fo', 'fu', 'fy', 'fie', 'fou',
            'ga', 'ge', 'gi', 'go', 'gu', 'gy', 'gie', 'gou',
            'ha', 'he', 'hi', 'ho', 'hu', 'hy', 'hie', 'hou',
            'ja', 'je', 'ji', 'jo', 'ju', 'jy', 'jie', 'jou',
            'ka', 'ke', 'ki', 'ko', 'ku', 'ky', 'kie', 'kou',
            'la', 'le', 'li', 'lo', 'lu', 'ly', 'lie', 'lou',
            'ma', 'me', 'mi', 'mo', 'mu', 'my', 'mie', 'mou',
            'na', 'ne', 'ni', 'no', 'nu', 'ny', 'nie', 'nou',
            'pa', 'pe', 'pi', 'po', 'pu', 'py', 'pie', 'pou',
            'qa', 'qe', 'qi', 'qo', 'qu', 'qy', 'qie', 'qou',
            'ra', 're', 'ri', 'ro', 'ru', 'ry', 'rie', 'rou',
            'sa', 'se', 'si', 'so', 'su', 'sy', 'sie', 'sou',
            'ta', 'te', 'ti', 'to', 'tu', 'ty', 'tie', 'tou',
            'va', 've', 'vi', 'vo', 'vu', 'vy', 'vie', 'vou',
            'wa', 'we', 'wi', 'wo', 'wu', 'wy', 'wie', 'wou',
            'xa', 'xe', 'xi', 'xo', 'xu', 'xy', 'xie', 'xou',
            'za', 'ze', 'zi', 'zo', 'zu', 'zy', 'zie', 'zou',
        );

        $counter = array( 1, 2, 3 );
        $count = $counter[array_rand($counter)];

        $name = '';
        for ($i=0; $i <= $count; $i++) { 
            $name .= $p[array_rand($p)];
        }

        return trim($name);
        //return array_rand( $p );
    }

    public static function getExcerpt( $string, $charCount ) {
        $strip = strip_tags( $string );
        return substr($strip, 0, $charCount ) . "...";
    }


    public static function curlGet( $url, $params ){
        $ch = curl_init();

        $params_get = http_build_query($params);

        curl_setopt($ch, CURLOPT_URL, $url.'?'.$params_get); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);

        if( !$response ){
            return 'Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch);
        }

        return $response;
    }

    public static function curlGet2( $url ){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

}












