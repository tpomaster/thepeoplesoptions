<?php
namespace App\Helpers;

class TpoBlockchain {
    protected $_var = array(
        'key' => '5758c576-df13-4b40-bcf6-b3ad7c30f342',
        'xpub' => 'xpub6C1LKkksVWutrW2UzYkcBo9ksSNK7kTN3G4X8DcoHK8M2jbpycNaZCCZKik2Gkz7RFGMS72USEBxYLwrwszxUiHHRiq7nuWqaHdXSJeESrX',
        'secret' => 'litopjaccount',
        'blockchainRoot' => 'https://blockchain.info/',
        'blockchainApiEndpoint' => 'https://api.blockchain.info/',
        'siteRoot' => 'https://thepeoplesoptions.com/blockchain/',
    );


    public function __construct() {

    }

    protected function _check() {
        if (!isset($this->_variables['current'])) {
            throw new Exception('Pagination::current must be set.');
        } elseif (!isset($this->_variables['total'])) {
            throw new Exception('Pagination::total must be set.');
        }
    }

}
