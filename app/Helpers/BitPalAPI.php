<?php
namespace App\Helpers;

use Log;

class BitPalAPI {
	public static function config( $k ){
        // https://wallet.bitpalbts.com/#/oauth/authorize?client_id=wtFhcpq8sUFlpRSANIdGFKTU6j75bXCAwaLK2Qhg&response_type=code&redirect_uri=https://thepeoplesoptions.com/sandbox/oauth-code

        $config = array(
            'client_id' => 'wtFhcpq8sUFlpRSANIdGFKTU6j75bXCAwaLK2Qhg',
            'client_secret' => 'znH4PT8gle3Pa9UBTxCZX0n2P5rxVtQc6PqeXvn1GKkteVKAHTlY1VXmWKonYMlsTN57XJ9lAggCm4TlBgoUh3xzauRopscc5uRZiRtHByjbUTNetNxXtKKihx2IxiLI',
            'api_home_url' => 'https://wallet.bitpalbts.com',
            'callback_url' => url('/').'/member/oauth-code',
        );

        if ( config('app.is_sandbox') == true ) {
            $config = array(
                'client_id' => 'CXLGlbzSYArb8RWlvKYHbcOn1CXhdvhBkNkOomSt',
                'client_secret' => 'v9paOHG8WVbqteb9LaVtukSkIiamrNWpJ4abtFOa4yrnOyPyhKaXDebnmwd4LrsOxUoEhGvCr6EB0w20kCkUmwRIqYreDiAjgdnQEORTno83biJZcbFCHjon1m4S8oZm',
                'api_home_url' => 'https://testbox.bitpalbts.com',
                'callback_url' => url('/').'/member/oauth-code',
            );
        }

        return $config[$k];
    }

    public static function getAccessToken( $code, $callbackUrl ){
    	$data = array(
		    'code' => $code,
		    'grant_type' => 'authorization_code',
		    'client_id' => self::config('client_id'),
		    'client_secret' => self::config('client_secret'),
		    'redirect_uri' => $callbackUrl,
		);

    	$curl = curl_init();
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_URL, self::config('api_home_url').'/api/v1/oauth/token/');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($curl);
		curl_close($curl);

		$auth_code = json_decode($result);

		# TEST ACCESS TOKEN
        # ====================================
        #$auth_code = json_decode('{"token_type": "Bearer", "expires_in": 36000, "refresh_token": "bWg4D0uLaqnBXRUv1heqCMlsnugQYT", "access_token": "VaTjIJfBJFKjfEQLiLn4yR40GWLQrk", "scope": "history wallet_transfer"}');


		return $auth_code;
    }

    public static function transfer( $params ){
        $transaction_code = 'xxx';
        $bts_asset = 'BTS';
        $pinned_asset = 'crypto';
        $amount = 1;
        $to_wallet = 'tpo-addfunds';
        $from_wallet = 'bitpal-bts';
        $comment = '';
        $callback = self::config('callback_url').'?state='.$transaction_code;
        $access_token = '6ZGkYxkxCw4WdfJflI6XTVq8zgYTSX';

        if ( !empty($params) ) {
            extract($params);
        }

        //$bitpal_config = self::config();

        # BITPALBTS SEND
        $param_hash = '';
        $data = array(
            'amount' => $amount,
            'asset' => $bts_asset,
            'pinned_asset' => $pinned_asset,
            "comment" => $comment,
            'receiver' => $to_wallet,
            'sender' => $from_wallet,
            'callback' => $callback
        );
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::config('api_home_url').'/api/v1/transfers/');
        $headers = array(
            'Authorization: Bearer '.$access_token,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($curl);
        curl_close($curl);

        Log::info('BitPalAPI::transfer:result'. json_encode($result));

        $send_res = json_decode($result);
        return $send_res;
    }

    public static function parseBitpalMsg( $send_ret ){
        $error_message = '';

        if ( $send_ret->status == 422) {
            foreach ($send_ret->errors->data as $k => $v) {
                if (is_array($v)) {
                    $error_message .= implode(', ', $v);
                }else{
                    $error_message .= $v.', ';
                }
                
            }
        }

        if ( $send_ret->status == 401) {
            $error_message = $send_ret->errors->message;
        }

        return $error_message;
    }

    # this will return asset1 value vs asset2
    public static function getExchangeRate( $asset1 = 'USD', $asset2 = 'BTS'){

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::config('api_home_url').'/api/v1/market/ticker/'.$asset1.'/'.$asset2);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        $result = curl_exec($curl);
        curl_close($curl);

        $send_res = json_decode($result);

        if ( isset($send_res->status) && $send_res->status == 200 ) {
            return $send_res->data->latest_price;

        }else{
            return false;
        }
    }

    public static function getBitsharesAccountsFromApi( $access_token ){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::config('api_home_url').'/api/v1/crypto_accounts/');
        $headers = array(
            'Content-type: application/json',
            'Authorization: Bearer '.$access_token,
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        curl_close($curl);

        $btsAccounts = json_decode($result);

        # TEST BTS ACCOUNTS
        # ====================================
        #$btsAccounts = json_decode('{"links": null, "meta": null, "data": [{"id": 15, "crypto_id": "1.2.158133", "username": "litopj2017", "user": 16, "created_at": 1514270546, "is_default": false, "address": "ZtnoDqsxOUU17PhG2cwJQlbT91tLbEuh", "balances": [{"symbol": "BTS", "amount": 626.30351}, {"symbol": "USD", "amount": 0.121}, {"symbol": "BTCCORE", "amount": 0.052187}]}, {"id": 16, "crypto_id": "1.2.540871", "username": "litopj2019", "user": 16, "created_at": 1514270684, "is_default": false, "address": "usICHSkxd7uFBblJMLGowBLYrqvwgfIe", "balances": [{"symbol": "BTS", "amount": 54.85021}, {"symbol": "BTCCORE", "amount": 0.0002}]}, {"id": 120, "crypto_id": "1.2.670028", "username": "lito-pjx", "user": 16, "created_at": 1516977058, "is_default": true, "address": "XaA8n8WKVnEvCHxH3OuOPerkKc0Mzz0j", "balances": [{"symbol": "BTS", "amount": 49.98227}, {"symbol": "BTCCORE", "amount": 0.002745}, {"symbol": "XISTOCASH", "amount": 0.1}]}], "status": 200}');
        
        return $btsAccounts;
    }

    public static function bitpalAmtFormat( $num ){
	    return number_format( (float)$num, 8, '.', '' );
	}

};

