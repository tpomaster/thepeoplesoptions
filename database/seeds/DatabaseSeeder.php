<?php

use App\Model\Admin\Admin;
use App\Model\Admin\Permission;
use App\Model\Admin\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // Ask for db migration refresh, default is no
        if ($this->command->confirm('Do you wish to refresh migration before seeding, it will clear all old data ?')) {

            // Call the php artisan migrate:fresh
            $this->command->call('migrate:fresh');

            $this->command->warn("Data cleared, starting from blank database.");
        }

        // Seed the default permissions
        $permissions = Permission::defaultPermissions();

        foreach ($permissions as $category => $permission) {

            foreach ($permission['name'] as $name) {

                $slug = strtolower(str_replace(' ', '-', trim($name)));

                Permission::firstOrCreate(['name' => $name, 'slug' => $slug, 'category' => $category]);
            }

        }

        $this->command->info('Default Permissions added.');


        $roles_array = ['Super Admin','Package Manager'];

        // add roles
        foreach ($roles_array as $role) {
            $role_slug = strtolower(str_replace(' ', '-', trim($role)));

            $role = Role::firstOrCreate(['name' => trim($role), 'slug' => $role_slug]);

            if ( $role->name == 'Super Admin' ) {
                // assign all permissions
                $role->syncPermissions(Permission::all('slug')->toArray());
                $this->command->info('Admin granted all the permissions');
            }

            // create one user for each role
            $this->createUser($role->slug);
        }

        $this->command->info('Roles added successfully');
    }

    /**
     * Create a user with given role
     *
     * @param $role
     */
    private function createUser($role)
    {
        if ($role == 'super-admin' ) {
            // $user = factory(User::class)->create();
            $admin = Admin::create([
                'firstname' => 'John',
                'lastname'  => 'Doe',
                'username'  => 'john.doe',
                'password'  => Hash::make('password'),
                'pincode'   => 12345,
                'email'     => 'john.doe@email.com',
            ]);
        } else {
            $admin = Admin::create([
                'firstname' => 'Jane',
                'lastname'  => 'Doe',
                'username'  => 'jane.doe',
                'pincode'   => 54321,
                'password'  => Hash::make('password'),
                'email'     => 'jane.doe@email.com',
            ]);
        }

        $admin->assignRole($role);
    }
}
