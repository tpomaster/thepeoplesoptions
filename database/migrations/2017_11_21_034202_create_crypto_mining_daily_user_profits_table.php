<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoMiningDailyUserProfitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_mining_daily_user_profits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->integer('package_fund_id')->unsigned();
            $table->integer('profit_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('date');
            $table->decimal('mining_amount', 16, 6);
            $table->decimal('mining_profit', 16, 6);
            $table->decimal('management_fee', 16, 6);
            $table->decimal('user_profit', 16, 6);
            $table->string('status');
            $table->text('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_mining_daily_user_profits');
    }
}

