<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->decimal('membership_fee');
            $table->smallInteger('duration')->nullable();
            $table->string('status')->default('available');
            $table->boolean('hide')->default(true);
            $table->binary('packages_config');
            $table->binary('tier_config');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_packages');
    }
}
