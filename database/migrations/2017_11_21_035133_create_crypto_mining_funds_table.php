<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoMiningFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_mining_funds', function (Blueprint $table) {
            $table->increments('id');
            $table->text('transaction_code');
            $table->integer('user_id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->decimal('amount', 16, 6);
            $table->integer('share_percentage');
            $table->dateTime('date');
            $table->string('status');
            $table->tinyInteger('duration');
            $table->tinyInteger('trade_count');
            $table->dateTime('trade_start_date');
            $table->dateTime('trade_end_date');
            $table->dateTime('last_trade_date');
            $table->string('last_trade_code', 50);
            $table->char('trade_status', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_mining_funds');
    }
}
