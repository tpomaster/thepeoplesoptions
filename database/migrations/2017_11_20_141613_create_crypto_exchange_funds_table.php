<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoExchangeFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_exchange_funds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_code')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->decimal('amount', 16, 6);
            $table->decimal('max_amount', 16, 6);
            $table->integer('share_percentage');
            $table->dateTime('date');
            $table->char('status', 30);
            $table->tinyInteger('duration');
            $table->tinyInteger('trade_count');
            $table->dateTime('trade_start_date');
            $table->dateTime('trade_end_date');
            $table->dateTime('last_trade_date');
            $table->string('last_trade_code');
            $table->char('trade_status', 30)->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_exchange_funds');
    }
}
