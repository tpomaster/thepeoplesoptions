<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoShufflePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_shuffle_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('status')->default('available');
            $table->integer('max_per_order');
            $table->decimal('ticket_price', 16, 6);
            $table->decimal('referral_base_credit', 16, 6);
            $table->boolean('hide')->default(true);
            $table->binary('tier_config');
            $table->integer('ticket_total_cnt')->nullable();
            $table->integer('sold_ticket_cnt')->nullable();
            $table->text('package_terms')->nullable();
            $table->string('wire_payment_inst')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_shuffle_packages');
    }
}
