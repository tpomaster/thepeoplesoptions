<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoTradingFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_trading_funds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('transaction_code');
            $table->bigInteger('user_id', 20)->unsigned();
            $table->integer('package_id', 11)->unsigned();
            $table->decimal('amount', 16, 6);
            $table->decimal('max_amount', 16, 6);
            $table->integer('share_percentage', 11);
            $table->char('status', 50);
            $table->smallInteger('duration', 5);
            $table->smallInteger('trade_count', 5);
            $table->timestamp('trade_start_date');
            $table->timestamp('trade_end_date');
            $table->timestamp('last_trade_date');
            $table->string('last_trade_code', 50);
            $table->char('trade_status',50)->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_trading_funds');
    }
}
