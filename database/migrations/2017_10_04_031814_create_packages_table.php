<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('category');
            $table->decimal('package_amount');
            $table->string('status')->default('available');
            $table->boolean('hide')->default(true);
            $table->decimal('funded_status')->default(0);
            $table->boolean('for_membership')->default(true);
            $table->string('risk_profile')->nullable();
            $table->string('trade_frequency')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
