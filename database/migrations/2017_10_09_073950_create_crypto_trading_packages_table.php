<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoTradingPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_trading_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 225);
            $table->decimal('amount', 11, 2);
            $table->smallInteger('duration', 5);
            $table->string('status', 50)->default('available');
            $table->boolean('hide')->default(true);
            $table->decimal('funded_status', 11, 2)->default(0);
            $table->boolean('for_membership')->default(false);
            $table->string('risk_profile', 50);
            $table->string('trade_frequency', 50);
            $table->text('tier_config');
            $table->text('management_fee');
            $table->text('stats')->nullable();
            $table->smallInteger('posted_trades', 5)->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_trading_packages');
    }
}
