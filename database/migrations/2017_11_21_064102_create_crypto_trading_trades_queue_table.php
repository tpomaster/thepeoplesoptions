<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoTradingTradesQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_trading_trades_queue', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->string('code', 50);
            $table->integer('num_of_trades');
            $table->dateTime('date');
            $table->dateTime('exec_date');
            $table->decimal('trade_amount', 16, 6);
            $table->decimal('profit', 16, 6);
            $table->char('for_membership', 4);
            $table->string('status', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_trading_trades_queue');
    }
}
