<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoTradingUserTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_trading_user_trades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('package_id')->unsigned();
            $table->integer('trade_id')->unsigned();
            $table->integer('package_fund_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('date');
            $table->decimal('trade_amount', 16, 6);
            $table->decimal('trade_earning', 16, 6);
            $table->decimal('management_fee', 16, 6);
            $table->decimal('user_profit', 16, 6);
            $table->string('status', 50);
            $table->text('details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_trading_user_trades');
    }
}
