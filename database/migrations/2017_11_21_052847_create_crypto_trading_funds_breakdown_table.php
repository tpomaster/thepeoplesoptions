<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoTradingFundsBreakdownTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_trading_funds_breakdown', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('package_id', 11)->unsigned();
            $table->bigInteger('package_fund_id', 20)->unsigned();
            $table->string('transaction_code', 50);
            $table->bigInteger('user_id', 20)->unsigned();
            $table->decimal('amount', 16, 6);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_trading_funds_breakdown');
    }
}
