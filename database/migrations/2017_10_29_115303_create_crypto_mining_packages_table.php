<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptoMiningPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crypto_mining_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->decimal('amount');
            $table->smallInteger('duration');
            $table->string('status')->default('available');
            $table->boolean('hide')->default(true);
            $table->decimal('funded_status')->default(0);
            $table->boolean('for_membership')->default(false);
            $table->string('risk_profile');
            $table->string('trade_frequency');
            $table->binary('tier_config');
            $table->binary('management_fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crypto_mining_packages');
    }
}
