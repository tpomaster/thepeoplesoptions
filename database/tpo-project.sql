-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 02, 2017 at 09:25 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tpo-project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `firstname`, `lastname`, `username`, `password`, `email`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'John', 'Doe', 'john.doe', '$2y$10$gYW7Kxszm8CncRqqZ0TMbe8krXg2iDmyZhTrbeeN4CKZPZ8O1DxAa', 'john.doe@email.com', 1, 'EDH1j0BmEUHIrmqKulm5giCtgrFlPIcyafaIE63QNIlRMASiOZpsWhcf8PTq', '2017-10-01 18:48:54', '2017-10-01 18:48:54'),
(2, 'Jane', 'Doe', 'jane.doe', '$2y$10$NuQQtF3KJ1zeKmOqL3cLlOUla3TMjVy2nepnb26IRHcS9UtOS3EzW', 'jane.doe@email.com', 1, NULL, '2017-10-01 18:48:54', '2017-10-01 18:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permission`
--

CREATE TABLE `admin_permission` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`admin_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_09_11_155717_create_admins_table', 1),
(4, '2017_09_11_155734_create_roles_table', 1),
(5, '2017_09_18_102128_create_permissions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-21 06:47:26'),
('ctSijxDq@xmail.com', 'jZskw7SQuDFaNg9MyecDmd9chuPsWJBS1506437414pbiphxgxF3ld9pSWQQIEj5dPEkbstj0Y', '2017-09-26 06:50:14'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:17:55'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:18:24'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:35:59'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:37:59'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:38:25'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:39:00'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:39:07'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:39:12'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:39:24'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:41:56'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:41:58'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:45:07'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:45:33'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 04:45:35'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 05:00:31'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-27 05:31:00'),
('a9rvIcST@xmail.com', 'vpkm22u6N12T0aXloPJz9Rj82Ba8eUgb1506005246', '2017-09-28 03:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `category`, `created_at`, `updated_at`) VALUES
(1, 'View Users', 'view-users', NULL, 'users', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(2, 'Add Users', 'add-users', NULL, 'users', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(3, 'Edit Users', 'edit-users', NULL, 'users', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(4, 'Delete Users', 'delete-users', NULL, 'users', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(5, 'View Roles', 'view-roles', NULL, 'roles', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(6, 'Add Roles', 'add-roles', NULL, 'roles', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(7, 'Edit Roles', 'edit-roles', NULL, 'roles', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(8, 'Delete Roles', 'delete-roles', NULL, 'roles', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(9, 'View Permissions', 'view-permissions', NULL, 'permissions', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(10, 'Add Permissions', 'add-permissions', NULL, 'permissions', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(11, 'Edit Permissions', 'edit-permissions', NULL, 'permissions', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(12, 'Delete Permissions', 'delete-permissions', NULL, 'permissions', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(13, 'View Packages', 'view-packages', NULL, 'packages', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(14, 'Add Packages', 'add-packages', NULL, 'packages', '2017-10-01 18:48:52', '2017-10-01 18:48:52'),
(15, 'Edit Packages', 'edit-packages', NULL, 'packages', '2017-10-01 18:48:53', '2017-10-01 18:48:53'),
(16, 'Delete Packages', 'delete-packages', NULL, 'packages', '2017-10-01 18:48:53', '2017-10-01 18:48:53');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'super-admin', NULL, '2017-10-01 18:48:53', '2017-10-01 18:48:53'),
(2, 'Package Manager', 'package-manager', NULL, '2017-10-01 18:48:54', '2017-10-01 18:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `membership` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `bitshares_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bitshares_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `parent_id`, `firstname`, `lastname`, `username`, `email`, `membership`, `status`, `bitshares_id`, `bitshares_name`, `password`, `activation_code`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, '0', 'Lito PJ', '', 'litopj', 'lite@yahoo.com', 'free', 1, NULL, NULL, '$2y$10$SvbxPJqizX/2wqTdcvHaveCj4AAz.BbLLRbEs9WS1Zqae7PfBXwHW', '', '3xTD6AdEaE4AAqOuzlzfVO0KOBeT8ZQWpodIYLyZP5TN17CmIFtKJhqxmB2d', NULL, NULL, NULL),
(2, '0', 'Lito PJ', '', 'litopj2', 'lite2@yahoo.com', 'free', 0, NULL, NULL, '$2y$10$/vlu7ZKXgr70.O4deaymS.m4Ozu/y6Hcb0D.Z1ZtwZ5lkPPw/xJXK', '', '', NULL, NULL, NULL),
(3, '2', 'marj db', '', 'mharjz', 'marj@yahoo.com', 'platinum', 2, NULL, NULL, '$2y$10$/vlu7ZKXgr70.O4deaymS.m4Ozu/y6Hcb0D.Z1ZtwZ5lkPPw/xJXK', '', '', NULL, NULL, NULL),
(4, '1', 'ZXnvFFrl', 'ZXnvFFrl', 'ZXnvFFrl', 'ZXnvFFrl@xmail.com', 'free', 1, NULL, NULL, '$2y$10$9nsLePWwpB2bWDVqCGmPXOYziZJguZRwN4K/FVd2tHFG5yXAPSFxe', '', '', NULL, '2017-09-18 04:51:52', NULL),
(5, '1', 'c3tAfiFp', 'c3tAfiFp', 'c3tAfiFp', 'c3tAfiFp@xmail.com', 'free', 1, NULL, NULL, '$2y$10$dSpwmwUuM7FXTdNFtWW/S.dNUy3Zl/1bFObNycCZpv9FOZ3i7uGwO', '$2y$10$ryL3UPplEhtv8pHGmNLlBuqjsBUP.LPQc5esqGadIFHJoDNdTYgUO', '', NULL, '2017-09-19 04:09:44', NULL),
(6, '1', '20ymbCMJ', '20ymbCMJ', '20ymbCMJ', '20ymbCMJ@xmail.com', 'free', 2, NULL, NULL, '$2y$10$7tcKUBzjmacWk.HOzhH3p.nxO2GAPFXQQe7SHpqwf0vtJR.EV.F6O', '', '', NULL, '2017-09-19 04:10:06', NULL),
(7, '1', 'tGZBcaJ2', 'tGZBcaJ2', 'tGZBcaJ2', 'tGZBcaJ2@xmail.com', 'free', 2, NULL, NULL, '$2y$10$T28JabCmc9la8bnIpoyduO.m4iTGImLfLhis/9PsVe4QO9TTL99Kq', '', '', NULL, '2017-09-19 04:11:19', NULL),
(8, '3', 'oL5iFjFi', 'oL5iFjFi', 'oL5iFjFi', 'oL5iFjFi@xmail.com', 'free', 2, NULL, NULL, '$2y$10$rvSxLepK52B4l/oyIyJFse3kdrXukRmTyKXfXCwVJrRjg1qf/YMue', '', '', NULL, '2017-09-20 03:57:17', NULL),
(9, '3', 'uoP1U70U', 'uoP1U70U', 'uoP1U70U', 'uoP1U70U@xmail.com', 'free', 2, NULL, NULL, '$2y$10$OdPI.0wUrwcdaIb7Orf4TOOv6iO2oy80L6SvP2Z3s9u2kQwMXIhBC', '', '', NULL, '2017-09-20 04:09:57', NULL),
(10, '1', 'Q5DxImz1', 'Q5DxImz1', 'Q5DxImz1', 'Q5DxImz1@xmail.com', 'free', 1, NULL, NULL, '$2y$10$ks.8NQSixi/S7BebhyXOhO7yA8LIgfWHcSlJp97kRwfam4e55mLum', '$2y$10$u.kWew6.NkW0XRihaIxkGeZuqvpsAgJWe55NM95jGBckWu.qVG76a', '', NULL, '2017-09-20 05:25:19', NULL),
(11, '1', 'IpGxMHTf', 'IpGxMHTf', 'IpGxMHTf', 'IpGxMHTf@xmail.com', 'free', 2, NULL, NULL, '$2y$10$H.GbBUY0kAmexmQdLef2/e6U3FEiBpJciauhW2OszAok8yLyVD8/a', '', 'roraLPU0PFl8bOCJEsuCmEM5g1oAwptsmZYRKqn1Ao9eqBxnfo2xvOvmchH1', NULL, '2017-09-20 06:23:14', NULL),
(12, '4', 'MUHifUXe', 'MUHifUXe', 'MUHifUXe', 'MUHifUXe@xmail.com', 'free', 2, NULL, NULL, '$2y$10$GoPkxntlJu1MLoynjI6nAesH/GEyh6nofEZKWK/7eQNGms5UpGxxa', '', 'jLstATsTOOi8wc95z4OCo6XcYumOn3TUUNgr8f5YYdqHxeedYpBaNUoCPHiV', NULL, '2017-09-20 06:30:44', NULL),
(13, '1', 'a9rvIcST', 'a9rvIcST', 'a9rvIcST', 'a9rvIcST@xmail.com', 'free', 1, NULL, NULL, '$2y$10$beHH0DUZNXUSUkEG20uTAeQeyV0mbFmq6VSVTzGTmJgC0Q0TzE.K6', '$2y$10$Gtm0LUeXUAICw1qSrc/hfOq1SSCKFcMDZyhipyNL0gQdQdkFgGKoy', '', NULL, '2017-09-21 02:14:29', NULL),
(14, '1', 'C5Bke4gF', 'C5Bke4gF', 'C5Bke4gF', 'C5Bke4gF@xmail.com', 'free', 1, NULL, NULL, '$2y$10$BAZFptvZvbzhJp0abmCXLuQWIiCkWaUfURsmfNDWPpIjPLqXskH22', '$2y$10$WO33AmVyWGFhJ1tj3ULVMeEu.tmluuRNtCKdFBzu4WkL.GyMfyuam', '', NULL, '2017-09-25 23:29:35', NULL),
(15, '1', 'z1vdff8A', 'z1vdff8A', 'z1vdff8A', 'z1vdff8A@xmail.com', 'free', 1, NULL, NULL, '$2y$10$DpdoEXzDli0QdJmUYk4u8OM6HVQpoQc9V3POcOcRmbbrUdf/3orna', '$2y$10$ZyYEv4zJGgm8Vofbsq0TreC6zI3JlB.knS0icO4CmCrjGsn5dImTi', '', NULL, '2017-09-25 23:32:49', NULL),
(16, '1', 'GD7UWJhj', 'GD7UWJhj', 'GD7UWJhj', 'GD7UWJhj@xmail.com', 'free', 1, NULL, NULL, '$2y$10$7fyzI.FRbUI45Q5Ra2hSBO6PhEDJG9F2Lyo1uM5SwlM8eXscRqn5O', '$2y$10$eprErjzODq6NR/7g54ZKyOZ67dhDts.atK1n3imeEXd5ldfLSDCfu', '', NULL, '2017-09-25 23:34:58', NULL),
(17, '1', 'pX53Kqg1', 'pX53Kqg1', 'pX53Kqg1', 'pX53Kqg1@xmail.com', 'free', 1, NULL, NULL, '$2y$10$G/rPkC6QLmhbFoNtRm5zFOCdNSrwyuoKqh9CFQHhnlKcxVm7aa9Iy', '$2y$10$0cXeALkTF8BPhrcS94VGmegL1TtGMqp2oN4eWdY/cIVIQGyHv2eVa', '', NULL, '2017-09-25 23:36:23', NULL),
(18, '1', 'I0oFEl6H', 'I0oFEl6H', 'I0oFEl6H', 'I0oFEl6H@xmail.com', 'free', 1, NULL, NULL, '$2y$10$jXqNxfi4M1d5udiRsnDM9e2iHAGJOg7VRsc.jhjiD94Z3Ard7nYm6', '$2y$10$Rz0cOyo9Al4fpwYSuN.Lw.nhvXuxQOcpBf0ruUx0shakk8yiYM6NO', '', NULL, '2017-09-25 23:37:53', NULL),
(19, '1', 'CM2LDtMl', 'CM2LDtMl', 'CM2LDtMl', 'CM2LDtMl@xmail.com', 'free', 1, NULL, NULL, '$2y$10$12UgKZI7ha361xtrnlt0nu9q/eYXfxU5AN.f2bt4N3BSXFHnfgMnm', '$2y$10$ok8Bz294dWpS9EARIHl2ZeCA9UY3paZqBc9X2dfdIuZMGlTOJZHXu', '', NULL, '2017-09-25 23:43:07', NULL),
(20, '1', 'sEhhVsmK', 'sEhhVsmK', 'sEhhVsmK', 'sEhhVsmK@xmail.com', 'free', 1, NULL, NULL, '$2y$10$5SQvnDa8g/M2./0KRBEHe.Zc5Sg/vi3lEpIGSgeMWVYxATcis0Rte', '$2y$10$fyX4yCeL.w4oU1cRUWX89eBb4htghMpQoo8Eo2XhudpfHfF.19BMa', '', NULL, '2017-09-26 00:23:10', NULL),
(21, '1', 'Tn3QabR5', 'Tn3QabR5', 'Tn3QabR5', 'Tn3QabR5@xmail.com', 'free', 1, NULL, NULL, '$2y$10$kOt9VRo6Ia5byjU81PjPPehIc/XAdkoVUmoSIFRxmy.1h3xPhfSMG', '$2y$10$X5B4ZDOsOQikUsi/zdD8BO9dNOGl5Cl1xYwAfOX0fnEmWhdUxuXcK', '', NULL, '2017-09-26 01:22:39', NULL),
(22, '1', 'a15OxbUB', 'a15OxbUB', 'a15OxbUB', 'a15OxbUB@xmail.com', 'free', 1, NULL, NULL, '$2y$10$GT2RzvyP70isSoBQIGHHX.rDjfYdA9Qmw6JfX7yoRsoFNRZhkD9uu', '$2y$10$/s5Py1V3VvOXZsKd52uLZugCZcceKywnJ9LIotr2w/kBQekm958pC', '', NULL, '2017-09-26 01:23:26', NULL),
(23, '1', 'vsa8xRxy', 'vsa8xRxy', 'vsa8xRxy', 'vsa8xRxy@xmail.com', 'free', 1, NULL, NULL, '$2y$10$pnTz723ZcjLkMUyicbfBkOrz7gE884q11hfpMarh9qMEI.cHApE12', '$2y$10$/kfKTGekd9VxYl3eDdHYh.6rTcANpJxovcB2enNxp5drvK9YGiINS', '', NULL, '2017-09-26 01:39:44', NULL),
(24, '1', 'zgdvbw7Y', 'zgdvbw7Y', 'zgdvbw7Y', 'zgdvbw7Y@xmail.com', 'free', 1, NULL, NULL, '$2y$10$J44xwLMaSYrQrO1aH7HwguLFQ2vwd6fUNLFWsj8/ADoFcpVBpW8Pe', '$2y$10$T5p2ttO/QB9j6zzrzYKwvOhaAwJhqGHt0ALQ5uIK1px0Ipm.MOKCy', '', NULL, '2017-09-26 01:42:02', NULL),
(25, '1', 'dLCmVpxn', 'dLCmVpxn', 'dLCmVpxn', 'dLCmVpxn@xmail.com', 'free', 1, NULL, NULL, '$2y$10$kmAVMKG9FMnnSeXB5y6TZe5yNfsunuP8c4OhAbHm/JbkcSBg1Wm8a', '$2y$10$HrSs5Fzi90O3j9/ydvwRs.9pL9Ym1.76NZXMABX9edTMcIWpIOd7C', '', NULL, '2017-09-26 01:57:07', NULL),
(26, '1', 't0mFLvNJ', 't0mFLvNJ', 't0mFLvNJ', 't0mFLvNJ@xmail.com', 'free', 1, NULL, NULL, '$2y$10$CFUnMRg8PnHh9BMFGoPzDe6w2RXWagpX38BXaBnaU7eCQJ/.CXsia', '$2y$10$Kh1ktn.tKOGmHVHO8B.C.uJ.vZCOHPRP8kWFnraVjr8KzZALBMFDC', '', NULL, '2017-09-26 02:00:04', NULL),
(27, '1', 'XZpNHSqT', 'XZpNHSqT', 'XZpNHSqT', 'XZpNHSqT@xmail.com', 'free', 1, NULL, NULL, '$2y$10$JAGL7B3FUNGuPpbvsTcVpOcxDq/AoZ0g3FzvEALa1Sns0o8zj21.y', '$2y$10$8asbZCEx8GNDj1.7Lmc11ehLDeDZOLF1setn7or4t2jZNWT.3Y1LS', '', NULL, '2017-09-26 03:17:32', NULL),
(28, '1', 'wTLetESY', 'wTLetESY', 'wTLetESY', 'wTLetESY@xmail.com', 'free', 1, NULL, NULL, '$2y$10$k8F7fGrC1EaODRhH9teyLux6QFIPEN9o4TJnug6pvke3yEpP7zQOy', '$2y$10$bhp/1ZXzWKWbCoxn5gZwrO2oH.rqgOfdjmzS1mrhmGtz/ldRYhvxO', '', NULL, '2017-09-26 03:22:17', NULL),
(29, '1', 'IjriPNB8', 'IjriPNB8', 'IjriPNB8', 'IjriPNB8@xmail.com', 'free', 1, NULL, NULL, '$2y$10$Z02lpqxj1Ka3shOf.WjKP.M.Ogar/az7pQ84qAa5UUCiYe7ejEsca', '$2y$10$YZj4CrSOXWm19k2MkhuzcuTr8ZUstKWY8o.bKZlKdOcqItRmpOaO6', '', NULL, '2017-09-26 03:24:50', NULL),
(30, '1', 'ctSijxDq', 'ctSijxDq', 'ctSijxDq', 'ctSijxDq@xmail.com', 'free', 2, NULL, NULL, '$2y$10$USLCO6CxxjrN.kH3fu4ip.ToxLiPrpAg7rJszFwaSV0sy.GB02agi', '', '', NULL, '2017-09-26 03:40:55', NULL),
(31, '1', 'gfVM8Uf4', 'gfVM8Uf4', 'gfVM8Uf4', 'gfVM8Uf4@xmail.com', 'free', 1, NULL, NULL, '$2y$10$Mo9O/ep/sM6Tps7IgRi2POyO67lG8z7bR/yYeKwdoEcZWm8rFYdIW', '$2y$10$QJAdrKvyPe26uPA.lfypj.5/IeEAgLsn2d8.X7zHcFSrXSrYepl5S', '', NULL, '2017-09-26 05:52:12', NULL),
(32, '1', 'Fqe6YcUi', 'Fqe6YcUi', 'Fqe6YcUi', 'Fqe6YcUi@xmail.com', 'free', 1, NULL, NULL, '$2y$10$IHiYJjqcoEifgNauj2AREeZAfh0qB/zYsatrDJ4ZQg07qgHjG8EtW', '$2y$10$fFd/zoBSKSjFaHCVr47fTetg1tprzdhsHaoQQj75QnLypZnLOtW/K', '', NULL, '2017-09-26 05:52:54', NULL),
(33, '1', 'G7mZP0Ou', 'G7mZP0Ou', 'G7mZP0Ou', 'G7mZP0Ou@xmail.com', 'free', 1, NULL, NULL, '$2y$10$GQTAjfLVkYBoVNunN5eFJO4IcNChspRV63H2pqTNFpChQglPTK/82', '$2y$10$4lo8SUMZYafZSY5I8VsIneG5/Gt2SFzzMkoL6kqQskCbbm.vSoMDW', '', NULL, '2017-09-26 05:54:18', NULL),
(34, '1', 'JogKzsBY', 'JogKzsBY', 'JogKzsBY', 'JogKzsBY@xmail.com', 'free', 1, NULL, NULL, '$2y$10$lnWD7bj0J3oorVdm01oD/uTSo3t5pHaHEp22U7TxwTS7.XIrmqcVi', '$2y$10$Oy8CDMiS6Jozii//7Mtf9eNy3xmC1wxE.DBUfVyTXRQ.cMLjuJCku', '', NULL, '2017-09-26 22:17:59', NULL),
(35, '1', 'hcn8ZMMk', 'hcn8ZMMk', 'hcn8ZMMk', 'hcn8ZMMk@xmail.com', 'free', 1, NULL, NULL, '$2y$10$7fEAFVlD5MzBbWzdbP7zFu7ypuAOgpIUPHoJl43Jq.YF50Zg1Bati', '$2y$10$m5/vKhHSXp36OLH4TvSu..FcCpigDc5K0SqZdGV.b6kItEKeIGMp2', '', NULL, '2017-09-26 22:18:27', NULL),
(36, '1', 'xQ2dtY2P', 'xQ2dtY2P', 'xQ2dtY2P', 'xQ2dtY2P@xmail.com', 'free', 1, NULL, NULL, '$2y$10$DAkStKbsTe8kdlkTCFGsI.IjfuqqZN0cZDElsuQrKYKVD1qpD/Odi', '$2y$10$oacol74ACQjslYkE2zPp5ugLhvgp0VZpaZkFrsTGKNOLGK5FKUzou', '', NULL, '2017-09-26 22:45:50', NULL),
(37, '1', 'eGyTxP7X', 'eGyTxP7X', 'eGyTxP7X', 'eGyTxP7X@xmail.com', 'free', 1, NULL, NULL, '$2y$10$LKqDyfBcVUwPfY5pTqlww.pibvvYXz.w.xUXvFhzf1g7A8dwm31LO', '$2y$10$gaB/OGOJbN2mM6w/aQ5fLeLifYyzfQKbhXoCDrAEk9Aiv5MhX0l2q', '', NULL, '2017-09-26 22:46:11', NULL),
(38, '12', 'ERyFPxaH', 'ERyFPxaH', 'ERyFPxaH', 'ERyFPxaH@xmail.com', 'free', 1, NULL, NULL, '$2y$10$bs625WbNGL4bkm8XT7mV1uhV1U4vwyFgR0ejmEbDl.2BUQel2zjuS', '$2y$10$Q06roKxodPNLOrU.n1YppeqpCLQbkGzpDzMPgNuxZin5h1KHb.N0O', '', NULL, '2017-09-27 03:33:45', NULL),
(39, '12', 'Nn1wk5c1', 'Nn1wk5c1', 'Nn1wk5c1', 'Nn1wk5c1@xmail.com', 'free', 1, NULL, NULL, '$2y$10$lCYKBgdv.cldyfK5W3Dva.sk0Qn0yTBhBrD0EN1RJpJDDPSlqlIrG', '$2y$10$8j1iAgKHwvb1s9fExiOC/.6d2i5.lz/Ls28y1r3JhoELSIZC6FlE.', '', NULL, '2017-09-27 03:36:39', NULL),
(40, '1', 'ubxjzEjo', 'ubxjzEjo', 'ubxjzEjo', 'ubxjzEjo@xmail.com', 'free', 1, NULL, NULL, '$2y$10$8pMCTNk7ZlsEdWHyA/1mvOW0p8fUCl4.R67EQDiZ2cEBG1i.RKlIu', '$2y$10$I9JrPrO0lbcDwpv1DI/f7.ULJVwP/TDrlgj1vlD14xs0WvNzzNbbO', '', NULL, '2017-09-28 01:50:39', NULL),
(41, '1', 'UOsWWbkp', 'UOsWWbkp', 'UOsWWbkp', 'UOsWWbkp@xmail.com', 'free', 1, NULL, NULL, '$2y$10$28aK1Z3kpiYRPPgu9KxQwuNgWT.pER3OeJfzFYanUkywrogB3rzX6', '$2y$10$rGFMX/EdYZWbcG6.mxrEzuz/DoFhz9evGgFDe.738U2U6EPCThrVy', '', NULL, '2017-09-29 04:10:54', NULL),
(42, '1', 'Louis', 'Guiban', 'lbfg15', '7Z5lTNXB@xmail.com', 'free', 2, NULL, NULL, '$2y$10$rX.n.WfCll/qogNWRX0s5uvoiePF1WznDRGoIidhj3zHgo7Pcf2tu', '$2y$10$C/n6KPa7ixehVljSa7HNcuh2xQagr/M7gaLXYKWnlCIGNKvPrpL/e', '', NULL, '2017-10-02 00:42:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_meta`
--

CREATE TABLE `users_meta` (
  `id` int(20) NOT NULL,
  `user_id` int(20) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `address` longtext,
  `city` longtext,
  `state` longtext,
  `zip` longtext,
  `country` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admin_permission`
--
ALTER TABLE `admin_permission`
  ADD PRIMARY KEY (`admin_id`,`permission_id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`admin_id`,`role_id`),
  ADD KEY `admin_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD CONSTRAINT `admin_role_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `admin_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
