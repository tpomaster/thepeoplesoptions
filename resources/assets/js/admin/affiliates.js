/**
 * File affiliates.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    function get_query_parameters(k, url){
        var p={};

        if (! url) url = location.search;

        url.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})

        return k?p[k]:p;
    }

    function render_affiliate_tree( child_tier_wrap, args = {} ) {
        var defaults = {
                'parent_username'   : $('.parentRow').data('parentUsername'),
                'current_tier_lvl'  : 1,
                'qpage'             : 1,
                'page'              : 1,
                'qlimit'            : 10,
                'sort_by'           : 'firstname',
                'sort_order'        : 'ASC'
            },
            data = $.extend({}, defaults, args);
            child_tier_wrap = child_tier_wrap ? child_tier_wrap : $('.parentRow .childTierWrap:first-child');
            /*if ( ! child_tier_wrap ) {
                $('.affiliates-tree').append('<div class="parentRow tierWrap tierWrap-'+data['parent_username']+'"><div class="childTierWrap"></div></div>');
                child_tier_wrap = $('.affiliates-tree').find('.parentRow .childTierWrap');
            }*/

        $.ajax({
            url: '/admin/affiliates/show/tree',
            type: 'POST',
            data: data,
        })
        .done(function(response) {
            if ( response.error ) {
                flash_message('Affiliates', response.message, 'error');
            } else {
                child_tier_wrap.html(response.html);
                // flash_message('Affiliates', response.message, 'success');
            }
        })
        .fail(function(data) {
            console.log(data.responseJSON);
        })
        .always(function() {
            console.log("complete");
        });
    }

    // Check if DOM is ready.
    $(function() {

        render_affiliate_tree();

        $('.affiliates-tree')
        .on('submit', '.filter-form', function (e) {
            e.preventDefault();

            var view = 'tree',
                username = $(this).find('#username').val();

            var origin = $(location).attr('origin'),
                link = origin + '/admin/affiliates/view/tree/' + username;

            $(location).attr('href', link );
        })
        .on('click', '.getTeamBtn', function (e) {
            e.preventDefault();

            var args = {
                    'parent_username'   : $(this).data('parentUsername'),
                    'current_tier_lvl'  : $(this).data('currentTierLvl')
                },
                child_tier_wrap = $(this).parent('.tierWrap').find('.childTierWrap');

            if ( $(this).hasClass('isActive') ) {
                child_tier_wrap.slideUp();
            } else {
                render_affiliate_tree(child_tier_wrap, args);

                child_tier_wrap.slideDown();
            }

            $(this).toggleClass('isActive');
        })
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var link = $(this).attr('href'),
                parent_username = get_query_parameters('parent_username', link),
                args = get_query_parameters(null, link),
                child_tier_wrap = $(this).parents('.tierWrap-' + parent_username).find('.childTierWrap');

                render_affiliate_tree(child_tier_wrap, args );

            return false;
        });
    });

} )( jQuery );
