/**
 * File login.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    // Check if DOM is ready.
    $(function() {
        $('#login-form').submit(function(e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                form_data = $(this).serialize();

            $(".loader").fadeIn("fast");

            $.post(ajaxurl, form_data, function (data) {
                location.reload()
            })
            .fail(function(error) {
                $(".loader").fadeOut("fast");

                if (error.status === 422) {
                    $.each(error.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                }
            });

        });
    });

} )( jQuery );
