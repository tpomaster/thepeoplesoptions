﻿
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
// Vue.component('login-form', require('./components/Login.vue'));
// Vue.component('data-table', require('./components/data-table/DataTable.vue'));
// Vue.component('admin-data-table', require('./components/data-table/AdminDataTable.vue'));
// Vue.component('modal', require('./components/modal.vue'));

// countdown timer


jQuery(document).ready(function(){
    header_countdown = new Tock({
        countdown: true,
        callback: function () {
            //console.log(header_countdown.lap() / 1000);
            jQuery('#header_countdown').html( header_countdown.msToTimecode(header_countdown.lap()) );
        },
        complete: function () {
            //console.log(header_countdown.lap() / 1000);
        }
    });

    header_countdown.start(jQuery('#header_countdown_clock').val());
});


( function ( $ ) {
    'use strict';

    $(function () {
        $('body').css({'overflow':'auto', 'height':'auto', 'position':'relative'});
        // $(".loader").fadeOut("slow");

        $('body')
        .on('click', '.modal-btn', function (e) {
            e.preventDefault();

            var id = $(this).data('id'),
            action = $(this).data('action'),
            title = $(this).data('title'),
            model = $(this).data('model'),
            ajaxurl = model + '/';

            if (action == 'edit') {
                ajaxurl = model + '/' + id + '/' + action;
            } else if (action == 'create') {
                ajaxurl = model + '/' + action;
            } else if (action == 'show') {
                ajaxurl = model + '/' + id;
            }

            $(".loader").fadeIn("fast");

            $.ajax({
                url: ajaxurl,
                /*beforeSend: function () {
                    $(".loader").fadeIn("slow");
                }*/
            })
            .done(function ( data ) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            })
            .fail(function(error) {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('click', '.ajax-pagination a', function(e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.ajax({
                url : url
            }).done(function (data) {
                $('.ajax-container').html(data);
            });

            // window.history.pushState("", "", url);
        });

        $('.tpo-marketing-login').on('click', function(e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $(".loader").fadeIn('fast');

            $.get(ajaxurl, function(data) {
                new PNotify({
                    title           : data.title,
                    text            : data.message,
                    type            : data.status,
                    animate_speed   : 'fast',
                    styling         : 'bootstrap3'
                });

                $(".loader").fadeOut('fast');

                if (data.url) {
                    window.location.replace(data.url);
                }
            });
        });
    });
} )( jQuery );
