/**
 * File packages.js.
 *
 * The code for packages.
 */

( function( $ ) {
    'use strict';

    var pathname    = location.pathname,
        model       = '',
        page        = $.isNumeric( get_url_params('page') ) ? get_url_params('page') : 1;

    if (~pathname.indexOf('crypto-trading')) {
        model = 'crypto-trading-packages';
    } else if (~pathname.indexOf('crypto-mining') ) {
        model = 'crypto-mining-packages';
    }

    function renderPackages(page) {
        var pathname    = location.pathname,
            ajaxurl     = '',
            page        = page;

        if (~pathname.indexOf('crypto-trading')) {
            ajaxurl = 'crypto-trading-packages';
        } else if (~pathname.indexOf('crypto-mining') ) {
            ajaxurl = 'crypto-mining-packages';
        } else if (~pathname.indexOf('crypto-exchange') ) {
            ajaxurl = 'crypto-exchange-packages';
        } else if (~pathname.indexOf('membership') ) {
            ajaxurl = 'membership-packages';
        }

        ajaxurl = page ? ajaxurl + '?page=' + page : ajaxurl;

        $.ajax({
            url: ajaxurl,
            type: 'GET',
        })
        .done(function (html) {
            $('.packages').html(html);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        console.log("Render Packages");
    }

    // Check if DOM is ready.
    $(document).ready(function() {
        renderPackages(page);
        console.log(page);

        var tier_counter = 1,
            package_counter = 1;

        $('#package-modal')
        .on('click', '.package-form .add-tier-btn', function (e) {
            e.preventDefault();

            var tier_config = $('#tier-config'),
                row = document.createElement('div'),
                remove_row = 'remove-tier-'+tier_counter;

            row.setAttribute('class', 'row ' + remove_row);

            row.innerHTML = '<div class="col-md-5 col-sm-5 col-xs-12"><label>Tier #</label><input type="number" name="tier_level[]" class="form-control"></input><span class="help-block error tier_level_'+tier_counter+'-error"></span></div><div class="col-md-5 col-sm-5 col-xs-12"><label>Referral Credit %</label><input type="number" name="referral_credit[]" class="form-control"></input><span class="help-block error referral_credit_'+tier_counter+'-error"></div><div class="col-md-2 col-sm-2 col-xs-12"><label>&nbsp;</label><button class="remove-tier-btn btn btn-danger form-control" data-tier-count="'+tier_counter+'" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div>';

            tier_config.append(row);

            tier_counter++;
        })
        .on('click', '.package-form .remove-tier-btn', function(e) {
            e.preventDefault();

            var tier_count = $(this).data('tierCount');

            $('.remove-tier-'+tier_count).remove();
        })
        .on('click', '.package-form .add-package-btn', function (e) {
            e.preventDefault();

            var packages_config = $('#packages-config'),
                row = document.createElement('div'),
                remove_row = 'remove-package-'+package_counter;

            row.setAttribute('class', 'row ' + remove_row);

            row.innerHTML = '<div class="col-md-4 col-sm-4 col-xs-12"><label>Category</label><input type="text" name="package_category[]" class="form-control"></input><span class="help-block error package_category_'+package_counter+'-error"></span></div><div class="col-md-3 col-sm-3 col-xs-12"><label>Package ID</label><input type="number" name="package_id[]" class="form-control"></input><span class="help-block error package_id_'+package_counter+'-error"></div><div class="col-md-3 col-sm-4 col-xs-12"><label>Amount to invest</label><input type="number" name="package_amount[]" class="form-control"></input><span class="help-block error package_amount_'+package_counter+'-error"></span></div><div class="col-md-2 col-sm-2 col-xs-12"><label>&nbsp;</label><button class="remove-package-btn btn btn-danger form-control" data-package-count="'+package_counter+'" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div>';

            packages_config.append(row);

            package_counter++;
        })
        .on('click', '.package-form .remove-package-btn', function(e) {
            e.preventDefault();

            var package_counter = $(this).data('packageCount');

            $('.remove-package-'+package_counter).remove();
        })
        .on('submit', '.package-form', function (e) {
            e.preventDefault();
            submit_modal( $(this) );
            renderPackages( page );
        });

        // Trade Queue
        $('#package-modal')
        .on('change', '[name="profit_percent"]', function (e) {
            e.preventDefault();

            var package_amount = $('#package-modal .package-amount').val(),
                profit_percent = $(this).val() / 100,
                profit_amount  = profit_percent * package_amount;

            $('#package-modal [name="profit"]').val(profit_amount);
        })
        .on('change', '[name="profit"]', function (e) {
            e.preventDefault();

            var package_amount = $('#package-modal .package-amount').val(),
                profit_amount  = $(this).val() / package_amount,
                profit_percent = round_to( profit_amount * 100, 2 );

            $('#package-modal [name="profit_percent"]').val(profit_percent);
        })
        .on('submit', '.queue-trade-form, .queue-profits-form', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                data    = $(this).serialize();

            $.post($(this).attr('action'), data, function (data, textStatus, xhr) {
                $('#package-modal').modal('hide');

                flash_message( data.title, data.message, data.status );
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            });
        });

        $('#confirmation-modal')
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form = $(this),
            ajaxurl = form.attr('action'),
            form_data = form.serialize(),
            form_method = form.attr('method');

            $.ajax({
                url: ajaxurl,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'JSON',
                data: {
                    "_method": 'DELETE',
                    // _token: $('meta[name="csrf-token"]').attr('content'),
                    'pincode': $(this).find('#pincode').val()
                },
            })
            .done(function (data) {
                $('#confirmation-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                renderPackages( page );

                console.log(data);
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                }
            })
            .always(function() {
                console.log("complete");
            });
        });

        $('.packages-content')
        .on('click', '.modal-btn', function (e) {
            e.preventDefault();

            var id = $(this).data('id'),
            action = $(this).data('action'),
            title = $(this).data('title'),
            model = $(this).data('model'),
            ajaxurl = model + '/';

            if (action == 'edit') {
                ajaxurl = model + '/' + id + '/' + action;
            } else {
                ajaxurl = model + '/' + action;
            }

            $.ajax({ url: ajaxurl })
            .done(function ( data ) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('click', '.delete-package', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'delete'
                }
            })
            .done(function (data) {
                $('#confirmation-modal .modal-body').append(data);

                $('#confirmation-modal').on('shown.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html(data);
                });

                $('#confirmation-modal').on('hidden.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html('');
                });

                $('#confirmation-modal').modal();
            })
            .fail(function(data) {
                console.log("error");
                console.log(data);
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('click', '.add-trade-queue-btn, .add-profit-queue-btn', function (e) {
            e.preventDefault();

            var title = $(this).attr('title'),
                ajaxurl = $(this).attr('href'),
                package_id = $(this).data('id');

            $.get(ajaxurl, function (data) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);

                    $('.admin-modal #date').datetimepicker({
                        useCurrent: false, //Important! See issue #1075
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // maxDate: moment()
                    });

                    $('.admin-modal #exec_date').datetimepicker({
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // minDate: moment().startOf('d')
                    });
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        });
    });

} )( jQuery );
