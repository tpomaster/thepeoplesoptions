window.Vue = require('vue');

import {Tabs, Tab} from 'vue-tabs-component';

Vue.component('affiliates-data-table', require('./components/affiliates/data-table.vue'));
Vue.component('affiliates-modal', require('./components/affiliates/Modal.vue'));
Vue.component('affiliates-confirmation-modal', require('./components/affiliates/ConfirmationModal.vue'));
Vue.component('pagination', require('./components/pagination.vue'));
Vue.component('tabs', Tabs);
Vue.component('tab', Tab);
// Vue.component('wysiwyg', require('./components/Wysiwyg.vue'));

Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});

const app = new Vue({
    el: '#affiliates',
    data: {
        showModal: false
    },
    methods: {
        openModal() {
            this.showModal = true;
        },
        closeModal() {
            this.showModal = false;
        },
        submitAndClose() {

        }
    }
});
