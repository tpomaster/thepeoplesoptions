/**
 * File show.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    var admin_modal = $('.admin-modal');

    function show_modal(title = '', body = '') {
        admin_modal.on('shown.bs.modal', function(){
            admin_modal.find('.modal-title').html(title);
            admin_modal.find('.modal-body').html(body);
        });

        admin_modal.on('hidden.bs.modal', function(){
            admin_modal.find('.modal-title').empty();
            admin_modal.find('.modal-body').empty();
        });

        admin_modal.modal();
    }

    function display_orders(package_id) {
        $.get('/admin/crypto-shuffle-packages/'+ package_id +'/orders', function (html) {
            $('#package-orders').html(html);
        });
    }

    // Check if DOM is ready.
    $(function() {
        var package_id = $('.package-container').data('packageId');

        display_orders(package_id);

        $('#package-orders')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function (data) {
                $('#package-orders').html(data);
            });
        })
        .on('click', '.view-referrals-btn', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                show_modal(title, data);
            });
        })
        .on('click', '.delete-order', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                show_modal(title, data);
            });
        });


        $('#package-modal')
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form        = $(this),
                ajaxurl     = form.attr('action'),
                data        = form.serialize();

            $.post(ajaxurl, data, function(data, textStatus, xhr) {
                $('#package-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                display_orders(package_id);
            });
        });
    });

} )( jQuery );
