/**
 * File search-filter.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    // Check if DOM is ready.
    $(function() {
        $('.package-filter').submit(function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                data    = $(this).serialize();

            $.post(ajaxurl, data, function (html, textStatus, xhr) {
                $('.packages').html(html);
            })
            .fail(function (data) {
                console.log(data.responseJSON);
            });

        });
    });

} )( jQuery );
