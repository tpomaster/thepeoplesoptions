/**
 * File trades-queue.js.
 *
 * The code for package trades queues.
 */

( function( $ ) {
    'use strict';

    function render_queued_trades() {
        $.get('/admin/crypto-trading-trades-queue/', function (data) {
            $('.queued-trades-container').html( data );
        });
    }

	// Check if DOM is ready.
    $(function() {

        render_queued_trades()

        $('.queued-box #date').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            // maxDate: moment()
        });

        $('.queued-box #exec_date').datetimepicker({
            ignoreReadonly: true,
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            // minDate: moment().startOf('d')
        });

        // Queued Trades Table Container
        $('.queued-trades-box')
        .on('click', '.edit-btn', function (e) {
            e.preventDefault();

            var title = $(this).data('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('#trade-queue-modal').modal();

                $('#trade-queue-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);

                    $('#trade-queue-modal #date').datetimepicker({
                        // useCurrent: false, //Important! See issue #1075
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // maxDate: moment()
                    });

                    $('#trade-queue-modal #exec_date').datetimepicker({
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // minDate: moment().startOf('d')
                    });
                });

                $('#trade-queue-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        })
        .on('click', '.delete-btn', function(e) {
            e.preventDefault();

            var title = $(this).data('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('#trade-queue-modal').modal();

                $('#trade-queue-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('#trade-queue-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        })
        .on('click', '.compute-btn', function (e) {
            e.preventDefault();

            var trade_item = $(this).parents('.queued-trade-item'),
                queue_id = $(this).data('id');

            var i = 0;

            $('.loader').fadeIn('fast', function() {
                $(this).toggleClass('loader loader-hold');
            });

            function compute_queued_trade() {
                $.post('/admin/crypto-trading-trades/compute', { 'queued_trade_id' : queue_id }, function(data, textStatus, xhr) {

                    var status = data.status,
                        type = data.status == 'completed' ? 'success' : 'info';

                    if ( status == 'completed' ) {
                        console.log(queue_id + ' this trade is computed.');

                        $('.loader-hold').fadeOut('fast', function() {
                            $(this).toggleClass('loader loader-hold');
                        });

                        flash_message( data.title, data.message, type );

                        render_queued_trades();
                    } else {
                        console.log(data.message);

                        flash_message( data.title, data.message, type );

                        compute_queued_trade();
                    }
                });
            }

            compute_queued_trade();
        })
        .on('click', '.compute-all-btn', function (e) {
            e.preventDefault();

            var queued_trades = [];

            $('.queued-trade-item').each(function(index, el) {
                if ( $(this).data('id') ) queued_trades.push($(this).data('id'));
            });

            var i = 0;

            function compute_queued_trade() {
                var queue_id = queued_trades[i];

                $('.loader').fadeIn('fast', function() {
                    $(this).toggleClass('loader loader-hold');
                });

                if (queue_id) {
                    $.post('/admin/crypto-trading-trades/compute', { 'queued_trade_id' : queue_id }, function(data, textStatus, xhr) {
                        var status = data.status,
                            type = data.status == 'completed' ? 'success' : 'info';

                        if ( status == 'completed' ) {
                            flash_message( data.title, data.message, type );
                            render_queued_trades();
                            i++;

                            if (i >= queued_trades.length) {
                                $('.loader-hold').fadeOut('fast', function() {
                                    $(this).toggleClass('loader loader-hold');
                                });
                            } else {
                                console.log(data.message);
                                compute_queued_trade();
                            }
                        } else {
                            flash_message( data.title, data.message, type );
                            compute_queued_trade();
                        }
                    });
                }
            }

            compute_queued_trade();
        })
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $('.queued-trades-container').html( data );
            });
        });

        // Trade Queue Modal
        $('#trade-queue-modal')
        .on('change', '[name="profit_percent"]', function (e) {
            e.preventDefault();

            var trade_amount   = $('#trade-queue-modal [name="trade_amount"]').val(),
                profit_percent = $(this).val() / 100,
                profit_amount  = profit_percent * trade_amount;

            $('#trade-queue-modal [name="profit"]').val(profit_amount);
        })
        .on('change', '[name="profit"]', function (e) {
            e.preventDefault();

            var trade_amount   = $('#trade-queue-modal [name="trade_amount"]').val(),
                profit_amount  = $(this).val() / trade_amount,
                profit_percent = round_to( profit_amount * 100, 2 );

            $('#trade-queue-modal [name="profit_percent"]').val(profit_percent);
        })
        .on('submit', '.modal-form', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                data    = $(this).serialize();

            $.post($(this).attr('action'), data, function (data, textStatus, xhr) {
                $('#trade-queue-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                render_queued_trades();
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            });
        })
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form        = $(this),
                ajaxurl     = form.attr('action'),
                data        = form.serialize(),
                form_method = form.attr('method');

            $.post(ajaxurl, data, function(data, textStatus, xhr) {
                $('#trade-queue-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                render_queued_trades();
            });
        });

        // Queue Trade Form
        $('.queue-trade-form')
        .on('change', '[name="package_id"]', function (e) {
            e.preventDefault();

            var package_id = $(this).val();

            $.get('/admin/crypto-trading-packages/' + package_id, function (data) {
                $('.queue-trade-form [name="trade_amount"]').val( data.amount );
            });
        })
        .on('change', '[name="profit_percent"]', function (e) {
            e.preventDefault();

            var trade_amount   = $('.queue-trade-form [name="trade_amount"]').val(),
                profit_percent = $(this).val() / 100,
                profit_amount  = profit_percent * trade_amount;

            $('.queue-trade-form [name="profit"]').val(profit_amount);
        })
        .on('change', '[name="profit"]', function (e) {
            e.preventDefault();

            var trade_amount   = $('.queue-trade-form [name="trade_amount"]').val(),
                profit_amount  = $(this).val() / trade_amount,
                profit_percent = round_to( profit_amount * 100, 2 );

            $('.queue-trade-form [name="profit_percent"]').val(profit_percent);
        })
        .on('submit', function (e) {
            e.preventDefault();

            var data = $(this).serialize(),
                form = $(this);

            $.post($(this).attr('action'), data, function (data, textStatus, xhr) {
                flash_message( data.title, data.message, data.status );

                form[0].reset();

                render_queued_trades();
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            });
        });

    });

})(jQuery);
