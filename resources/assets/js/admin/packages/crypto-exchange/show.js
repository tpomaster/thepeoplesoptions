/**
 * File queue.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    var admin_modal = $('.admin-modal');

    function show_modal(title = '', body = '') {
        admin_modal.on('shown.bs.modal', function(){
            admin_modal.find('.modal-title').html(title);
            admin_modal.find('.modal-body').html(body);
        });

        admin_modal.on('hidden.bs.modal', function(){
            admin_modal.find('.modal-title').empty();
            admin_modal.find('.modal-body').empty();
        });

        admin_modal.modal();
    }

    function display_investors(package_id) {
        $.get('/admin/crypto-exchange-packages/'+ package_id +'/investors', function (html) {
            $('#package-investors').html(html);
        });
    }

    function display_profits(package_id) {
        $.get('/admin/crypto-exchange-packages/'+ package_id +'/profits', function (html) {
            $('#package-profits').html(html);
        });
    }

    // Check if DOM is ready.
    $(function() {
        var package_id = $('.package-container').data('packageId');

        $('#package-investors')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $('#package-investors').html( data );
            });

        });

        $('#package-profits')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $('#package-profits').html( data );
            });

        })
        .on('click', '.delete-profit', function (e) {
            e.preventDefault();

            var title = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('#package-modal').on('shown.bs.modal', function(){
                    $('.admin-modal .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('#package-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });

                $('#package-modal').modal();
            });
        });

        $('#package-modal')
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form        = $(this),
                ajaxurl     = form.attr('action'),
                data        = form.serialize(),
                form_method = form.attr('method');

            $.post(ajaxurl, data, function(data, textStatus, xhr) {
                $('#package-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                display_investors(package_id);
                display_profits(package_id);
            });
        });

        $('#package-investors, #package-members')
        .on('click', '.view-referrals-btn', function (e) {
            e.preventDefault();

            var title = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        });


        $('.package-summary')
        .on('click', '.close-package', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, {action: 'close'}).done(function (html) {
                show_modal(title, html);
            });
        });
    });

} )( jQuery );
