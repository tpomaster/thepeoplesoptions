/**
 * File queue.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    // Check if DOM is ready.
    $(function() {

        $('#package-members')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $('#package-members').html( data );
            });

        })
        .on('click', '.view-referrals-btn', function (e) {
            e.preventDefault();

            var title = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        });
    });

} )( jQuery );
