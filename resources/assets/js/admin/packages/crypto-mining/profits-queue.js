/**
 * File queue.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    function round_to(n, digits) {
        var negative = false;
        if (digits === undefined) {
            digits = 0;
        }
            if( n < 0) {
            negative = true;
          n = n * -1;
        }
        var multiplicator = Math.pow(10, digits);
        n = parseFloat((n * multiplicator).toFixed(11));
        n = (Math.round(n) / multiplicator).toFixed(2);
        if( negative ) {
            n = (n * -1).toFixed(2);
        }
        return n;
    }

    function render_queued_items() {
        $.get('/admin/crypto-mining-profits-queue/', function (data) {
            $('.queued-items-container').html( data );
        });
    }

    // Check if DOM is ready.
    $(function() {

        render_queued_items();

        $('.queued-box #date').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            // maxDate: moment()
        });

        $('.queued-box #exec_date').datetimepicker({
            ignoreReadonly: true,
            allowInputToggle: true,
            format: 'YYYY-MM-DD',
            // minDate: moment().startOf('d')
        });

        $('.queue-form')
        .on('change', '[name="package_id"]', function (e) {
            e.preventDefault();

            var package_id = $(this).val();

            $.get('/admin/crypto-mining-packages/' + package_id, function (data) {
                $('.queue-form [name="mining_amount"]').val( data.amount );
            });
        })
        .on('change', '[name="profit_percent"]', function (e) {
            e.preventDefault();

            var mining_amount  = $('.queue-form [name="mining_amount"]').val(),
                profit_percent = $(this).val() / 100,
                profit_amount  = profit_percent * mining_amount;

            $('.queue-form [name="profit"]').val(profit_amount);
        })
        .on('change', '[name="profit"]', function (e) {
            e.preventDefault();

            var mining_amount  = $('.queue-form [name="mining_amount"]').val(),
                profit_amount  = $(this).val() / mining_amount,
                profit_percent = round_to( profit_amount * 100, 2 );

            $('.queue-form [name="profit_percent"]').val(profit_percent);
        })
        .on('submit', function (e) {
            e.preventDefault();

            var data = $(this).serialize(),
                form = $(this);

            $.post($(this).attr('action'), data, function (data, textStatus, xhr) {
                flash_message( data.title, data.message, data.status );

                form[0].reset();

                render_queued_items()
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            });
        });

        // Queued Trades Table Container
        $('.queued-items-box')
        .on('click', '.edit-btn', function (e) {
            e.preventDefault();


            var title = $(this).data('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('.item-queue-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);

                    $('#item-queue-modal #date').datetimepicker({
                        // useCurrent: false, //Important! See issue #1075
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // maxDate: moment()
                    });

                    $('.item-queue-modal #exec_date').datetimepicker({
                        ignoreReadonly: true,
                        allowInputToggle: true,
                        format: 'YYYY-MM-DD',
                        // minDate: moment().startOf('d')
                    });
                });

                $('.item-queue-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });

                $('.item-queue-modal').modal();
            });
        })
        .on('click', '.delete-btn', function(e) {
            e.preventDefault();

            var title = $(this).data('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (data) {
                $('.item-queue-modal').modal();

                $('.item-queue-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.item-queue-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            });
        })
        .on('click', '.compute-btn', function (e) {
            e.preventDefault();

            var queue_item = $(this).parents('.queued-item'),
                queue_id = $(this).data('id');

            var i = 0;

            $('.loader').fadeIn('fast', function() {
                $(this).toggleClass('loader loader-hold');
            });

            function compute_queued_trade() {
                $.post('/admin/crypto-mining-profits/compute', { 'queue_id' : queue_id }, function(data, textStatus, xhr) {

                    var status = data.status,
                        type = data.status == 'completed' ? 'success' : 'info';

                    if ( status == 'completed' ) {
                        $('.loader-hold').fadeOut('fast', function() {
                            $(this).toggleClass('loader loader-hold');
                        });

                        console.log(queue_id + ' this trade is computed.');

                        flash_message( data.title, data.message, type );

                        render_queued_items();
                    } else {
                        console.log(data.message);

                        flash_message( data.title, data.message, type );

                        compute_queued_trade();
                    }
                });
            }

            compute_queued_trade();
        })
        .on('click', '.compute-all-btn', function (e) {
            e.preventDefault();

            var queue_ids = [];

            $('.loader').fadeIn('fast', function() {
                $(this).toggleClass('loader loader-hold');
            });

            $('.queued-item').each(function(index, el) {
                if ( $(this).data('id') ) queue_ids.push($(this).data('id'));
            });

            var i = 0;

            function compute_queued_trade() {
                var queue_id = queue_ids[i];

                if (queue_id) {
                    $.post('/admin/crypto-mining-profits/compute', { 'queue_id' : queue_id }, function(data, textStatus, xhr) {

                        var status = data.status,
                            type = data.status == 'completed' ? 'success' : 'info';

                        if ( status == 'completed' ) {
                            console.log(queue_id + ' this trade is computed.');

                            flash_message( data.title, data.message, type );
                            render_queued_items();

                            i++;

                            if (i >= queue_ids.length) {
                                $('.loader-hold').fadeOut('fast', function() {
                                    $(this).toggleClass('loader loader-hold');
                                });
                            } else {
                                compute_queued_trade();
                            }

                        } else {
                            flash_message( data.title, data.message, type );

                            compute_queued_trade();
                        }
                    });
                }
            }

            compute_queued_trade();
        })
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.get(url, function(data) {
                $('.queued-items-container').html( data );
            });
        });

        // Trade Queue Modal
        $('.item-queue-modal')
        .on('change', '[name="profit_percent"]', function (e) {
            e.preventDefault();

            var mining_amount  = $('.item-queue-modal [name="mining_amount"]').val(),
                profit_percent = $(this).val() / 100,
                profit_amount  = profit_percent * mining_amount;

            $('.item-queue-modal [name="profit"]').val(profit_amount);
        })
        .on('change', '[name="profit"]', function (e) {
            e.preventDefault();

            var mining_amount  = $('.item-queue-modal [name="mining_amount"]').val(),
                profit_amount  = $(this).val() / mining_amount,
                profit_percent = round_to( profit_amount * 100, 2 );

            $('.item-queue-modal [name="profit_percent"]').val(profit_percent);
        })
        .on('submit', '.modal-form', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                data    = $(this).serialize();

            $.post($(this).attr('action'), data, function (data, textStatus, xhr) {
                $('.item-queue-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                render_queued_items();
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            });
        })
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form        = $(this),
                ajaxurl     = form.attr('action'),
                data        = form.serialize(),
                form_method = form.attr('method');

            $.post(ajaxurl, data, function(data, textStatus, xhr) {
                $('.item-queue-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                render_queued_items();
            });
        });
    });

} )( jQuery );
