( function ($) {
    'use strict';

    var page = get_url_params('page');

    function render_announcement(page) {
        var ajaxurl     = 'announcements',
            page        = page;

        ajaxurl = page ? ajaxurl + '?page=' + page : ajaxurl;

        $.ajax({
            url: ajaxurl,
            type: 'GET',
        })
        .done(function (html) {
            $('.announcement-list').html(html);
        })
        .fail(function(error) {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    $(function() {
        render_announcement(page);

        $('#announcement-modal')
        .on('submit', '.modal-form', function (e) {
            e.preventDefault();
            // submit_modal( $(this) );
            var formData = new FormData($(this)[0]);

            var form = $(this),
                ajaxurl = form.attr('action'),
                form_data = form.serialize(),
                form_method = form.attr('method');

            $.ajax({
                url: ajaxurl,
                type: form_method,
                data: formData,
                cache: false,
                processData: false,
                contentType: false
            })
            .done(function (data) {
                $('.admin-modal').modal('hide');
                render_announcement( page );
                flash_message( data.title, data.message, data.status );

                console.log(data);
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            })
            .always(function () {
                console.log("complete");
            });
        });

        $('.announcement-list')
        .on('click', '.delete-announcement', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'delete'
                }
            })
            .done(function (data) {
                $('#confirmation-modal .modal-body').append(data);

                $('#confirmation-modal').on('shown.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html(data);
                });

                $('#confirmation-modal').on('hidden.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html('');
                });

                $('#confirmation-modal').modal();
            })
            .fail(function(data) {
                console.log("error");
                console.log(data);
            })
            .always(function() {
                console.log("complete");
            });
        });

        $('#confirmation-modal')
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form = $(this),
            ajaxurl = form.attr('action'),
            form_data = form.serialize(),
            form_method = form.attr('method');

            $.ajax({
                url: ajaxurl,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'JSON',
                data: {
                    "_method": 'DELETE',
                    // _token: $('meta[name="csrf-token"]').attr('content'),
                    'pincode': $(this).find('#pincode').val()
                },
            })
            .done(function (data) {
                $('#confirmation-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                render_announcement( page );
            })
            .fail(function(data) {
                console.log(data);
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        });
    });

})(jQuery);
