/**
 * File referrals.js.
 *
 * The code for your theme JavaScript source should reside in this file.
 */

( function( $ ) {
    'use strict';

    // Check if DOM is ready.
    $(function() {

        $('#referrals-table-container')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('href'),
            })
            .done(function(html) {
                $('#referrals-table-container').html(html);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

            // return false;
        });
    });

} )( jQuery );
