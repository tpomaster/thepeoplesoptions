( function ($) {
    'use strict';

    var admin_modal = $('.admin-modal'),
        container   = $('#settings-container');

    function display_settings_list() {
        $.get('/admin/settings', function (html) {
            $('#settings-container').html(html);
        });
    }

    function show_modal(title = '', body = '') {
        admin_modal.on('shown.bs.modal', function(){
            admin_modal.find('.modal-title').html(title);
            admin_modal.find('.modal-body').html(body);
        });

        admin_modal.on('hidden.bs.modal', function(){
            admin_modal.find('.modal-title').empty();
            admin_modal.find('.modal-body').empty();
        });

        admin_modal.modal();
    }

    function submit_form_modal( form ) {

        // Clear Form Errors
        form.find('.error, .help-block').html('');

        var form        = form,
            ajaxurl     = form.attr('action'),
            form_data   = form.serialize(),
            form_method = form.attr('method');

        $.ajax({
            url: ajaxurl,
            type: form_method,
            data: form_data,
        })
        .done(function (data) {
            admin_modal.modal('hide');

            flash_message( data.title, data.message, data.status );

            console.log(data);
        })
        .fail(function (data) {
            console.log(data.responseJSON);
            if (data.status === 422) {
                $.each(data.responseJSON.errors, function (key, value) {
                    $('.'+key+'-error').html(value);
                });
            }
        });
    }

    $(function() {

        display_settings_list();

        $('.new-setting-btn')
        .on('click', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (html) {
                show_modal(title, html);
            });
        });

        container
        .on('click', '.edit-setting', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (html) {
                show_modal(title, html);
            });
        })
        .on('click', '.delete-setting', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, { action : 'delete' }, function (html) {
                show_modal(title, html);
            });
        })
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (html) {
                container.html( html );
            });
        });



        admin_modal
        .on('submit', 'form, .modal-form', function (e) {
            e.preventDefault();

            submit_form_modal( $(this) );

            display_settings_list();
        });

    });

})(jQuery);
