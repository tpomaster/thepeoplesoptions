$(document).ready(function() {

    /* Admin */
    var admin_table = $('#admin-users-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "users/alladmins",
            "dataType": "json",
            "type": "POST",
            "data": { _token: $('meta[name="csrf-token"]').attr('content')}
        },
        "columns": [
        { "data": "id" },
        { "data": "firstname" },
        { "data": "lastname" },
        { "data": "email" },
        { "data": "username" },
        { "data": "role" },
        { "data": "status" },
        { "data": "actions" }
        ]
    });

    $('#confirmation-modal')
    .on('submit', '.confirmation-form', function (e) {
        e.preventDefault();

        var form = $(this),
        ajaxurl = form.attr('action'),
        form_data = form.serialize(),
        form_method = form.attr('method');

        $(".loader").fadeIn("fast");

        $.ajax({
            url: ajaxurl,
            type: 'DELETE',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            dataType: 'JSON',
            data: {
                "_method": 'DELETE',
                // _token: $('meta[name="csrf-token"]').attr('content'),
                'pincode': $(this).find('#pincode').val()
            },
        })
        .done(function (data) {
            $('#confirmation-modal').modal('hide');

            $(".loader").fadeOut("fast");

            flash_message( data.title, data.message, data.status );

            admin_table.ajax.reload(null, false);

            console.log(data);
        })
        .fail(function(data) {
            console.log("error");
            console.log(data);
        })
        .always(function() {
            console.log("complete");
        });
    });

    $('#admin-users-table')
    .on('click', '.delete-user', function (e) {
        e.preventDefault();
        var ajaxurl = $(this).attr('href'),
            token   = $(this).data("token");

        $.ajax({
            url: ajaxurl,
            type: 'GET',
            data: {
                action: 'delete'
            },
        })
        .done(function (data) {
            $('#confirmation-modal .modal-body').append(data);

            $('#confirmation-modal').on('shown.bs.modal', function(){
                $('#confirmation-modal .modal-body').html(data);
            });

            $('#confirmation-modal').on('hidden.bs.modal', function(){
                $('#confirmation-modal .modal-body').html('');
            });

            $('#confirmation-modal').modal();
        })
        .fail(function(data) {
            console.log("error");
            console.log(data);
        })
        .always(function() {
            console.log("complete");
        });
    });

    // Admin Modal
    $('.admin-modal')
    .on('submit', '.modal-form', function (e) {
        e.preventDefault();

        var form = $(this),
        ajaxurl = form.attr('action'),
        form_data = form.serialize(),
        form_method = form.attr('method');

        form.find('.error').empty();

        $(".loader").fadeIn("fast");

        $.ajax({
            url: ajaxurl,
            type: form_method,
            data: form_data
        })
        .done(function (data) {
            $('.admin-modal').modal('hide');

            $(".loader").fadeOut("fast");

            flash_message( data.title, data.message, data.status );

            admin_table.ajax.reload(null, false);

            console.log(data);

            console.log('Reload Admin Datatable.');
        })
        .fail(function (data) {
            console.log(data.responseJSON);
            $(".loader").fadeOut("fast");
            if (data.status === 422) {
                $.each(data.responseJSON.errors, function (key, value) {
                    var key = key.replace('.', '_');

                    $('.'+key+'-error').html(value);
                });
                console.log('error 422');
            }
        })
        .always(function () {
            console.log("complete");
        });
    });
});
