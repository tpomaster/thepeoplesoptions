﻿// countdown timer
jQuery(document).ready(function(){
    header_countdown = new Tock({
        countdown: true,
        callback: function () {
            //console.log(header_countdown.lap() / 1000);
            jQuery('#header_countdown').html( header_countdown.msToTimecode(header_countdown.lap()) );
        },
        complete: function () {
            //console.log(header_countdown.lap() / 1000);
        }
    });

    header_countdown.start(jQuery('#header_countdown_clock').val());
});

$(document).ready(function() {
    $('.ui-pnotify').remove();
    $(".loader").fadeOut("slow");
});
