( function ($) {
    'use strict';

    var admin_modal         = $('.admin-modal'),
        confirmation_modal  = $('#confirmation-modal'),
        container           = $('#payouts-container'),
        table_container     = $('#payouts-table-container'),
        summary             = $('#payouts-summary');

    function display_payouts_list() {

        $.get('/admin/payouts', function (html) {
            table_container.html(html);
        });
    }

    function show_modal(title = '', body = '') {
        admin_modal.on('shown.bs.modal', function(){
            admin_modal.find('.modal-title').html(title);
            admin_modal.find('.modal-body').html(body);
        });

        admin_modal.on('hidden.bs.modal', function(){
            admin_modal.find('.modal-title').empty();
            admin_modal.find('.modal-body').empty();
        });

        admin_modal.modal();
    }

    function submit_form_modal( form ) {

        // Clear Form Errors
        form.find('.error, .help-block').html('');

        var form        = form,
            ajaxurl     = form.attr('action'),
            form_data   = form.serialize(),
            form_method = form.attr('method');

        $.ajax({
            url: ajaxurl,
            type: form_method,
            data: form_data
        })
        .done(function (data) {
            admin_modal.modal('hide');
            flash_message( data.title, data.message, data.status );

            display_payouts_list();
        })
        .fail(function (data) {
            console.log(data.responseJSON);
            if (data.status === 422) {
                $.each(data.responseJSON.errors, function (key, value) {
                    $('.'+key+'-error').html(value);
                });
            }
        });
    }

    function submit_approval_form_modal( form ) {

        // Clear Form Errors
        form.find('.error, .help-block').html('');

        var form        = form,
            ajaxurl     = form.attr('action'),
            form_data   = form.serialize(),
            form_method = form.attr('method');

        $.ajax({
            url: ajaxurl,
            type: form_method,
            data: form_data,
        })
        .done(function (html) {
            admin_modal.find('.modal-title').html('Approval Confirmation');
            admin_modal.find('.modal-body').html(html);
        })
        .fail(function (data) {
            console.log(data.responseJSON);
            if (data.status === 422) {
                $.each(data.responseJSON.errors, function (key, value) {
                    $('.'+key+'-error').html(value);
                });
            }
        });
    }

    $(function() {

        display_payouts_list();

        table_container
        .on('click', '.approve-payout', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (html) {
                show_modal(title, html);
            });
        })
        .on('click', '.cancel-payout', function (e) {
            e.preventDefault();

            var title   = $(this).attr('title'),
                ajaxurl = $(this).attr('href');

            $.ajax({
                url: ajaxurl,
                data: { action: 'cancel' }
            })
            .done(function (data) {
                $('#confirmation-modal .modal-body').append(data);

                $('#confirmation-modal').on('shown.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html(data);
                });

                $('#confirmation-modal').on('hidden.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html('');
                });

                $('#confirmation-modal').modal();
            });
        })
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $.get(ajaxurl, function (html) {
                table_container.html( html );
            });
        });

        admin_modal
        .on('submit', '.modal-form, .approval-form', function (e) {
            e.preventDefault();

            submit_form_modal( $(this) );

            // display_payouts_list();
        });

        confirmation_modal
        .on('submit', '.confirmation-form', function (e) {
            e.preventDefault();

            var form = $(this),
            ajaxurl = form.attr('action'),
            form_data = form.serialize(),
            form_method = form.attr('method');

            $.post(ajaxurl, {'pincode': $(this).find('#pincode').val()})
            .done(function (data) {
                $('#confirmation-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                display_payouts_list();
            })
            .fail(function (data) {
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                }
            });
        });

        summary
        .on('click', '.filter-link', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href'),
                params  = { status : $(this).data('status') };

            $.post(ajaxurl, params, function (html, textStatus, xhr) {
                table_container.html(html);
            });
        });

    });

})(jQuery);
