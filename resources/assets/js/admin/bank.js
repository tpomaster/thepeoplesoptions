/**
 * File packages.js.
 *
 * The code for packages.
 */

( function( $ ) {
    'use strict';

    function render_transactions(url) {
        console.log(url);

        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function (html) {
            $('.bank-container .transactions-table').html(html);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    function render_users(url) {
        console.log(url);

        $.ajax({
            url: url,
            type: 'GET',
        })
        .done(function (html) {
            $('#users-table-container').html(html);
            $('.transactions-table tbody').empty().append('<tr><td>Click on a Username under TPO User Banks Section</td></tr>');
            $('.transactions-table .total-count').remove();
            $('.transactions-table .pagination').remove();
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }

    // Check if DOM is ready.
    $(function() {
        $('.transactions-filter #from-date').datetimepicker({
            ignoreReadonly: true,
            allowInputToggle: true,
        });

        $('.transactions-filter #to-date').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            allowInputToggle: true,
        });

        $(".transactions-filter #from-date").on("dp.change", function (e) {
            $('.transactions-filter #to-date').data("DateTimePicker").minDate(e.date);
        });

        $(".transactions-filter #to-date").on("dp.change", function (e) {
            $('.transactions-filter #from-date').data("DateTimePicker").maxDate(e.date);
        });

        $('.transactions-filter').submit(function(e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('action'),
                form_data = $(this).serialize();

            $.ajax({
                url: ajaxurl,
                type: 'POST',
                // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: form_data,
                beforeSend: function () {
                }
            })
            .done(function(data) {
                $('.bank-container .transactions-table').html(data);

                // render_transactions( ajaxurl );
            })
            .fail(function (data) {
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                }
            });


            console.log('Transaction Filter Form Submitted!');
        });

        $('.transactions-summary')
        .on('click', '.filter-link', function (e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href'),
                params  = { section: $(this).data('section') };

            $.post(ajaxurl, params, function (data, textStatus, xhr) {
                $('.bank-container .transactions-table').html(data);
            });
        });

        $('#sendfund-form').submit(function(e) {
            e.preventDefault();

            var form = $(this),
                ajaxurl = form.attr('action'),
                form_data = form.serialize(),
                form_method = form.attr('method');

            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: form_data
            })
            .done(function (data) {
                $('#send-fund').modal('hide');

                $('#confirmation-modal .modal-body').append(data);

                $('#confirmation-modal').on('shown.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html(data);
                });

                $('#confirmation-modal').on('hidden.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html('');
                });

                $('#confirmation-modal').modal();
            })
            .fail(function(data) {
                console.log("error");
                console.log(data);
            })
            .always(function() {
                console.log("complete");
            });
        });

        $('#confirmation-modal')
        .on('submit', '.fund-confirmation', function (e) {
            e.preventDefault();

            var form = $(this),
            ajaxurl = form.attr('action'),
            form_data = form.serialize(),
            form_method = form.attr('method');

            $.ajax({
                url: ajaxurl,
                type: form_method,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: form_data,
            })
            .done(function (data) {
                // $('#send-fund').modal('hide');
                $('#confirmation-modal').modal('hide');
                flash_message( data.title, data.message, data.status );
                location.reload();
                console.log(data);
            })
            .fail(function (data) {
                console.log(data.responseJSON);
                if (data.status === 422) {
                    $.each(data.responseJSON.errors, function (key, value) {
                        $('.'+key+'-error').html(value);
                    });
                    console.log('error 422');
                } else {

                }
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('submit', '.delete-confirmation', function (e) {
            e.preventDefault();

            var form = $(this),
            ajaxurl = form.attr('action');

            $.ajax({
                url: ajaxurl,
                type: 'DELETE',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dataType: 'JSON',
                data: {
                    "_method": 'DELETE',
                    'pincode': $(this).find('#pincode').val()
                },
            })
            .done(function (data) {
                $('#confirmation-modal').modal('hide');

                flash_message( data.title, data.message, data.status );

                console.log(data);
            })
            .fail(function(data) {
                console.log("error");
                console.log(data);
            })
            .always(function() {
                console.log("complete");
            });
        });

        $('.bank-container')
        .on('click', '.modal-btn', function (e) {
            e.preventDefault();

            var id = $(this).data('id'),
            action = $(this).data('action'),
            title = $(this).data('title'),
            model = $(this).data('model'),
            ajaxurl = model + '/';

            if (action == 'edit') {
                ajaxurl = model + '/' + id + '/' + action;
            } else {
                ajaxurl = model + '/' + action;
            }

            $.ajax({
                url: ajaxurl,
            })
            .done(function ( data ) {
                $('.admin-modal').modal();

                $('.admin-modal').on('shown.bs.modal', function(){
                    $('.modal-header .modal-title').html(title);
                    $('.admin-modal .modal-body').html(data);
                });

                $('.admin-modal').on('hidden.bs.modal', function(){
                    $('.admin-modal .modal-body').html('');
                });
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('click', '.delete-transaction', function(e) {
            e.preventDefault();

            var ajaxurl = $(this).attr('href');

            $.ajax({
                url: ajaxurl,
                type: 'GET',
                data: {
                    action: 'delete'
                }
            })
            .done(function (data) {
                $('#confirmation-modal .modal-body').append(data);

                $('#confirmation-modal').on('shown.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html(data);
                });

                $('#confirmation-modal').on('hidden.bs.modal', function(){
                    $('#confirmation-modal .modal-body').html('');
                });

                $('#confirmation-modal').modal();
            })
            .fail(function(data) {
                console.log("error");
                console.log(data);
            })
            .always(function() {
                console.log("complete");
            });
        })
        .on('click', '.toggle-search', function(){
            $(".transactions-filter").slideToggle("slow");
        });

        $('.transactions-table')
        .on('click', '.filter .pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href'),
                form_data = $('.transactions-filter').serialize();

            $.ajax({
                url : url,
                type: 'POST',
                data: form_data,
                beforeSend: function () {
                }
            }).done(function (data) {
                $('.bank-container .transactions-table').html(data);

                console.log(data);

                // render_transactions( url );
                console.log('filtered Pagination');

            }).fail(function () {
                alert('Content could not be loaded.');
            });

            // window.history.pushState("", "", url);
        });

        $('.transactions-table')
        .on('click', '.no-filter .pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.ajax({
                url : url,
                beforeSend: function () {
                }
            }).done(function (data) {
                $('.bank-container .transactions-table').html(data);

                render_transactions( url );

            }).fail(function () {
                alert('Content could not be loaded.');
            });

            // window.history.pushState("", "", url);
        });

        $('#users-table-container')
        .on('click', '.pagination a', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.ajax({
                url : url,
                beforeSend: function () {
                }
            }).done(function (data) {
                $('#users-table-container').html(data);

                render_users( url );
            }).fail(function () {
                alert('Content could not be loaded.');
            });
        });

        $('#users-table-container')
        .on('click', '.user-link', function (e) {
            e.preventDefault();

            var url = $(this).attr('href');

            $.ajax({
                url : url,
                beforeSend: function () {
                }
            }).done(function (data) {
                $('.bank-container .transactions-table').html(data);

                render_transactions( url );
            }).fail(function () {
                alert('Content could not be loaded.');
            });
        });

        $('#user-search')
        .on('submit', function (e) {
            e.preventDefault();

            var url = $(this).attr('action'),
                form_data = $(this).serialize(),
                form_method = $(this).attr('method');

            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: form_method,
                data: form_data,
            })
            .done(function (html) {
                $('#users-table-container').html(html);

                $('.transactions-table tbody').empty().append('<tr><td>Click on a Username under TPO User Banks Section</td></tr>');
                $('.transactions-table .total-count').remove();
                $('.transactions-table .pagination').remove();

                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        });

        $('#bank-modal')
        .on('submit', '.transaction-form', function (e) {
            e.preventDefault();

            submit_modal( $(this) );

            render_transactions( 'tpo-bank-transaction' );
        });
    });

})(jQuery);
