function get_url_params( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}

function get_search_params(k, url){
    var p={};

    if (! url) url = location.search;

    url.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})

    return k?p[k]:p;
}

function flash_message(title, message, status) {
    new PNotify({
        title           : title,
        text            : message,
        type            : status,
        animate_speed   : 'fast',
        styling         : 'bootstrap3'
    });
};

function submit_modal( form ) {
    var form = form,
    ajaxurl = form.attr('action'),
    form_data = form.serialize(),
    form_method = form.attr('method');

    form.find('.error').empty();

    $.ajax({
        url: ajaxurl,
        type: form_method,
        data: form_data
    })
    .done(function (data) {
        $('.admin-modal').modal('hide');

        flash_message( data.title, data.message, data.status );

        console.log(data);
    })
    .fail(function (data) {
        if (data.status === 422) {
            $.each(data.responseJSON.errors, function (key, value) {
                var key = key.replace('.', '_');

                $('.'+key+'-error').html(value);
            });
        }
    });
}

function round_to(n, digits) {
    var negative = false;
    if (digits === undefined) {
        digits = 0;
    }
        if( n < 0) {
        negative = true;
      n = n * -1;
    }
    var multiplicator = Math.pow(10, digits);
    n = parseFloat((n * multiplicator).toFixed(11));
    n = (Math.round(n) / multiplicator).toFixed(2);
    if( negative ) {
        n = (n * -1).toFixed(2);
    }
    return n;
}

$(document)
.ajaxError(function (event, xhr, settings, thrownError) {
    if ( thrownError == 'Internal Server Error' ) {
        msgs = ['Something went wrong. Please contact us'];
        flash_message( 'Ajax Error', msgs, 'error' );
    } else {
        if ( xhr.status == '419' ) {
            msgs = ['Session Expired. Please Login.'];
            flash_message( 'Ajax Error', msgs, 'error' );

        } else if ( xhr.status == '401' ) {
            msgs = ['Session Expired. I will now redirect you to Login Page.'];
            flash_message( 'Ajax Error', msgs, 'error' );

            setTimeout(function () {
                location.href = '/admin/login';
            }, 3000);
        }
    }

    $(".loader").fadeOut('fast');
})
.ajaxSend(function () {
    $(".loader").fadeIn("fast");
})
.ajaxSuccess(function (event, request, settings) {
    $(".loader").fadeOut('fast');
});

$(document).ready(function() {
    $('.ui-pnotify').remove();
});
