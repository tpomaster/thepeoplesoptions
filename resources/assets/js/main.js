jQuery(document).ready(function () {
	// loading icon while page load
	jQuery(window).load(function() {
		jQuery('body').css({'overflow':'auto', 'height':'auto', 'position':'relative'});
		jQuery(".loader").fadeOut("slow");
	});

	// header animate when scroll
	jQuery(window).scroll(function(){
	    if (jQuery(window).scrollTop() >= 20) {
	       jQuery('header').addClass('scroll-active');
	    }
	    else {
	       jQuery('header').removeClass('scroll-active');
	    }
	});
});