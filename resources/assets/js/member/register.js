
jQuery('.member_reg_form').submit(function(){
    var formElem = jQuery(this);
    var inputData = formElem.serialize();

    show_global_ajax_loader();

    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        dataType: 'json',
        data: inputData,
        success: function( result ){
            
            if( result.isError == true ){
                show_page_ajax_message( result.msg, 'danger');

            }else{
                show_page_ajax_message( result.msg, 'success');

                if ( result.redirectUrl != '') {
                    window.location.href = result.redirectUrl;
                }
            }

            hide_global_ajax_loader();
        },
        error: function( jqXHR, textStatus, errorThrown ){

            if ( jqXHR.responseText ) {

                var msgs = jQuery.parseJSON( jqXHR.responseText );

                jQuery.each(msgs, function( i, val ){
                    console.log( val );
                });

                show_page_ajax_message( msgs, 'danger');

            }else{
                msgs = ['SOmething went wrong. Please contact us'];
                show_page_ajax_message( msgs, 'danger');
            }

            hide_global_ajax_loader();
        }
    });

    return false;
});