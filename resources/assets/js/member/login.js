
jQuery('.member_login_form').submit(function(){
    var form_elem = jQuery(this);
    var input_data = form_elem.serialize();

    show_global_ajax_loader();

    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        dataType: 'json',
        data: input_data,
        success: function( result ){
            
            if( result.isError == true ){
                show_page_ajax_message( result.msg, 'danger');

            }else{
                show_page_ajax_message( result.msg, 'success');

                // jQuery('.login_step_1_wrap').hide();
                // jQuery('.login_step_2_wrap').show();

                if ( result.redirectUrl != '') {
                    //jQuery('.login_step_2_wrap').hide();

                    console.log(result);
                    
                    setTimeout(function(){ 
                        window.location.href = result.redirectUrl;
                    }, 1000);
                }
            }

            hide_global_ajax_loader();
        },
        error: function( jqXHR, textStatus, errorThrown ){

            if ( jqXHR.responseText ) {

                var msgs = jQuery.parseJSON( jqXHR.responseText );

                jQuery.each(msgs, function( i, val ){
                    console.log( val );
                });

                show_page_ajax_message( msgs, 'danger');

            }else{
                msgs = ['SOmething went wrong. Please contact us'];
                show_page_ajax_message( msgs, 'danger');
            }

            hide_global_ajax_loader();
        }
    });

    return false;
});