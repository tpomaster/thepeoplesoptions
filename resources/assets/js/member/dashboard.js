﻿// loading icon on page load
$(document).ready(function() {
    $('.ui-pnotify').remove();
    $('body').css({'overflow':'auto', 'height':'auto', 'position':'relative'});
    $(".loader").fadeOut("slow");

    // popup on load
    $('#myModal').modal('show');

});


// membership toggle
$(window).resize(function() {
    if( $(this).width() <= 1340 ) {
        $(".togle-list").hide();
        $(".mermbership-column .info-header").addClass("toggle-info");
    } else {
        $(".togle-list").show();
        $(".mermbership-column .info-header").removeClass("toggle-info");
    }
});


// popup series
$(".ann_popup_modal").each(function(){
    var currentModal = $(this);
    
    //click next
    currentModal.find('.btn-next').click(function(){ 
        // jQuery(this).addClass('test');

        currentModal.closest(".ann_popup_modal").nextAll(".ann_popup_modal").first().modal('show'); 
        currentModal.fadeOut('slow');
    });
});


jQuery(document).ready(function(){
    header_countdown = new Tock({
        countdown: true,
        callback: function () {
            //console.log(header_countdown.lap() / 1000);
            jQuery('#header_countdown').html( header_countdown.msToTimecode(header_countdown.lap()) );
        },
        complete: function () {
            //console.log(header_countdown.lap() / 1000);
        }
    });

    header_countdown.start(jQuery('#header_countdown_clock').val());
});


//date time picker
if ( $('#from-date').length ) {
    $('#user-search #from-date').datetimepicker({
            ignoreReadonly: true,
            allowInputToggle: true,
    });
}

if ( $('#from-date').length ) {
    $('#user-search #to-date').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            ignoreReadonly: true,
            allowInputToggle: true,
    });
}
    
// end date time picker