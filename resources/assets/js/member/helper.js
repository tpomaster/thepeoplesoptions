jQuery(function () {
    jQuery('[data-toggle="tooltip"]').tooltip()
})

jQuery(document).on( 'click', '.close_msg', function(){
    jQuery(this).parent('.alert').remove();
    return false;
});


function show_global_ajax_loader(){
    jQuery('.global_ajax_loader').show();
}

function hide_global_ajax_loader(){
    jQuery('.global_ajax_loader').fadeOut();
}

function show_ajax_message( ajax_result_wrap, msg_arr, elem_class ){
    ajax_result_wrap.addClass(elem_class).show();

    jQuery.each( msg_arr, function( index, value ){

        jQuery( '<p class="alert alert-'+elem_class+' alert-dismissible">'+value+'<a href="#" class="close_msg">x</a></p>' ).appendTo(ajax_result_wrap)
            .delay(8000)
            .animate({
                width: '0px', height:'0px', opacity: 0, margin: '0px', padding: '0px'
            },{
                duration: 600,
                complete: function(){
                    jQuery(this).remove(); 
                }
            }
        );
    });
}


function show_page_ajax_message( msg_arr, elem_class ){
    var ajax_result_wrap = jQuery('.page_ajax_message_wrap .ajax_result');
    ajax_result_wrap.addClass(elem_class).show();

    jQuery.each( msg_arr, function( index, value ){

        jQuery( '<p class="alert alert-'+elem_class+' alert-dismissible">'+value+'<a href="#" class="close_msg">x</a></p>' ).appendTo(ajax_result_wrap)
            .delay(30000)
            .animate({
                width: '0px', height:'0px', opacity: 0, margin: '0px', padding: '0px'
            },{
                duration: 600,
                complete: function(){
                    jQuery(this).remove(); 
                }
            }
        );
    });
}

function handle_ajax_error( jqXHR, textStatus, errorThrown ){
    console.log(textStatus); 

    if ( errorThrown == 'Internal Server Error' ) {
        msgs = ['Something went wrong. Please contact us'];
        show_page_ajax_message( msgs, 'danger');
        hide_global_ajax_loader();

    }else{
        if ( jqXHR.status == '419' ) {
            msgs = ['Session Expired. I will now refresh this page.'];
            show_page_ajax_message( msgs, 'danger');

            setTimeout(function(){
                location.href = window.location.href;
            }, 3000);

        }else if ( jqXHR.status == '401' ) {
            msgs = ['Session Expired. I will now redirect you to Login Page.'];
            show_page_ajax_message( msgs, 'danger');

            setTimeout(function(){
                location.href = '/member/login';
            }, 3000);

        }else if ( jqXHR.responseText ) {
            var msgs = jQuery.parseJSON( jqXHR.responseText );
            show_page_ajax_message( msgs, 'danger');
            hide_global_ajax_loader();

        }else{
            msgs = ['Something went wrong. Please contact us'];
            show_page_ajax_message( msgs, 'danger');
            hide_global_ajax_loader();
        }
    }
}


// jQuery(document).ready(function () {
//     jQuery(document).ajaxError(function(event, jqXHR, settings, errorThrown) {
//         handle_ajax_error( jqXHR, '', errorThrown );
//     });
// });




/* Allow number keys only */
jQuery(document).on( 'keydown', '.lpj_num_only', function(e){
    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});


/*
get url query vars
author: Lito Poclis Jr
sample usage: lpj_parse_url('https://pinoyrecruiters.com/admin.php?page=pr-commissions-admin&tab=penny_matrix', 'tab');
return: 'penny_matrix'
*/
function lpj_parse_url( str, key){
    var search = lpj_create_a( str ).search;
    var params = search.split("?"); 
    var params2 = params[1].split("&");

    for ( var i = 0; i < params2.length; i++ ) { 
        var pairs = params2[i].split("="); 
        if(pairs[0] == key) { 
            return pairs[1]; 
        } 
    } 

    return params2;
}

function lpj_create_a( url ) {
    var a = document.createElement('a');
    a.href = url;
    return a;
}