@foreach ($news_collection as $news )

  <div class="news-box">
    <div class="row">
      <div class="news-thumbnail col-md-1 col-sm-12 col-xs-12">
        <?php 
            $imageHolder = $news->thumbnail_path;
            $setImage = 'images/nthumb15.png';

            if ( !empty($imageHolder) ) {
                $setImage = 'storage/'.$imageHolder;
            }
        ?>
        <img src="<?php echo LpjHelpers::asset($setImage); ?>" alt="{{ $news->title }}" width="100%" height="auto">
      </div>
      <div class="news-content col-md-9 col-sm-12 col-xs-12">
        <h3><a>{{ $news->title }}</a></h3>
        <div class="generated-content">
          <!-- {!! html_entity_decode($news->content) !!} -->
          <p><?php echo LpjHelpers::getExcerpt($news->content, 300); ?></p>
        </div>
      </div>
      <div class="news-side col-md-2 col-ms-12 col-xs-12">
        <a href="{{ route('news.show', $news->id) }}"
          class="modal-btn btn btn-xs yellow-btn"
          title       = "{{ $news->title }}"
          data-id     = "{{ $news->id }}"
          data-action = "show"
          data-model  = "news"
          data-title  = "{{ $news->title }}">
          <span class="glyphicon glyphicon-search"></span>
        </a>
        {{-- <a class="modal-btn btn btn-xs theme-btn"><span class="glyphicon glyphicon-edit"></span></a> --}}
        <a href="#"
          class       = "modal-btn btn btn-xs theme-btn"
          title       = "Edit News"
          data-id     = "{{ $news->id }}"
          data-action = "edit"
          data-model  = "news"
          data-title  = "Edit News" >
          <span title="edit" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>
        <a href="{{ route('news.confirmation', $news->id) }}" class="delete-news btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
      </div>
    </div>
  </div>

@endforeach

{{ $news_collection->links('vendor.pagination.ajax-default') }}


