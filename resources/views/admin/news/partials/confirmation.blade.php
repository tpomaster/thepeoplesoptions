@if ( $action == 'delete' )

  <form action="{{ route('news.destroy', $news->id) }}" method="POST" class="confirmation-form">
    <div class="form-group">
      <p>Are you sure you want to delete <strong>{{ $news->title }}</strong>? The delete action can’t be undone.</p>

      <p>Please type in your user pin code to confirm.</p>

      <label for="pincode">Pin Code:</label>
      <input type="password" class="form-control" id="pincode" name="pincode">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block">I understand the consequences, delete this news</button>
    </div>
  </form>

@else

  <p>Update content</p>

@endif
