@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- news css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/0.11.1/trix.css">
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/news.css') }}">
@stop

@section('main_container')
  <!-- page content -->
  <div class="news-section right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>News</h1>
      <h2>
        <button
          class       = "modal-btn btn btn-md theme-btn"
          type        = "button"
          title       = "Add New News"
          data-toggle = "modal"
          data-action = "create"
          data-model  = "news"
          data-title  = "Add New Crypto Exchange Packages" >
          Add New News
        </button>
      </h2>
    </div>

    <div class="news-list ajax-container"></div>

    @include ('admin.news.partials.modal')
  </div>
  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  {{-- TinyMCE --}}
  <script src="{{ LpjHelpers::asset('js/admin/vendors/tinymce/tinymce.js') }}"></script>
  <script src="{{ LpjHelpers::asset('js/admin/news.js') }}"></script>
@stop
