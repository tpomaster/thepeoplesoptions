@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- announcement css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/0.11.1/trix.css">
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/announcement.css') }}">

@stop

@section('main_container')
  <!-- page content -->
  <div class="announcement-section right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Announcements</h1>
      <h2>
        <button
          class       = "modal-btn btn btn-md theme-btn"
          type        = "button"
          title       = "Add New Announcement"
          data-toggle = "modal"
          data-action = "create"
          data-model  = "announcements"
          data-title  = "Add New Announcement" >
          Add New Announcement
        </button>
      </h2>
    </div>

    <div class="announcement-list ajax-container"></div>

    @include ('admin.announcements.partials.modal')
  </div>

  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  {{-- TinyMCE --}}
  <script src="{{ LpjHelpers::asset('js/admin/vendors/tinymce/tinymce.js') }}"></script>
  <script src="{{ LpjHelpers::asset('js/admin/announcement.js') }}"></script>
@stop
