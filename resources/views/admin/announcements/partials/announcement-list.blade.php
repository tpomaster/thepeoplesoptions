@foreach ($announcements as $announcement )

  <div class="announcement-box">
    <div class="row">
      <div class="ann-thumbnail col-md-1 col-sm-12 col-xs-12">
        <?php 
            $imageHolder = $announcement->thumbnail_path;
            $setImage = 'images/nthumb8.png';

            if ( !empty($imageHolder) ) {
                $setImage = 'storage/'.$imageHolder;
            }
        ?>
        <img src="<?php echo LpjHelpers::asset($setImage); ?>" alt="{{ $announcement->title }}" width="100%" height="auto">
      </div>
      <div class="ann-content col-md-9 col-sm-12 col-xs-12">
        <h3><a>{{ $announcement->title }}</a></h3>
        <div class="generated-content">
          <!-- {!! html_entity_decode($announcement->content) !!} -->
          <p><?php echo LpjHelpers::getExcerpt($announcement->content, 300); ?></p>
        </div>
      </div>
      <div class="ann-side col-md-2 col-ms-12 col-xs-12">
        <a href="{{ route('announcements.show', $announcement->id) }}"
          class="modal-btn btn btn-xs yellow-btn"
          title       = "{{ $announcement->title }}"
          data-id     = "{{ $announcement->id }}"
          data-action = "show"
          data-model  = "announcements"
          data-title  = "{{ $announcement->title }}">
          <span class="glyphicon glyphicon-search"></span>
        </a>
        {{-- <a class="modal-btn btn btn-xs theme-btn"><span class="glyphicon glyphicon-edit"></span></a> --}}
        <a href="#"
          class       = "modal-btn btn btn-xs theme-btn"
          title       = "Edit Announcement"
          data-id     = "{{ $announcement->id }}"
          data-action = "edit"
          data-model  = "announcements"
          data-title  = "Edit Announcement" >
          <span title="edit" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>
        <a href="{{ route('announcements.confirmation', $announcement->id) }}" class="delete-announcement btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
      </div>
    </div>
  </div>

@endforeach

{{ $announcements->links('vendor.pagination.ajax-default') }}
