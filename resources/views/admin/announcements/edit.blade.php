<form action="{{ route('announcements.update', $announcement->id) }}" method="POST" enctype="multipart/form-data" data-parsley-validate class="modal-form package-form form-horizontal form-label-left">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  @include ('admin.announcements.partials.form-fields')

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="text-center">
      <button type="submit" class="btn theme-btn">Submit</button>
      <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
    </div>
  </div>
</form>
