<form action="{{ route('announcements.store') }}" method="POST" enctype="multipart/form-data" data-parsley-validate id="admin-modal" class="modal-form package-form form-horizontal form-label-left">
  {{ csrf_field() }}

  @include ('admin.announcements.partials.form-fields')

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="text-center">
      <button type="submit" class="btn theme-btn">Submit</button>
      <a href="#" class="btn btn-danger" data-dismiss="modal">Cancel</a>
    </div>
  </div>
</form>
