<div class="faq-content">
  {!! $faq->content !!}
</div>

<div class="ln_solid"></div>

<div class="form-group">
    <div class="text-center">
		<a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
	</div>
</div>
