@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- news css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/0.11.1/trix.css">
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/faq.css') }}">
@stop

@section('main_container')
  <!-- page content -->
  <div class="faq-section right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>FAQ</h1>
      <h2>
        <button
          class       = "modal-btn btn btn-md theme-btn"
          type        = "button"
          title       = "Add New FAQ"
          data-toggle = "modal"
          data-action = "create"
          data-model  = "faq"
          data-title  = "Add New FAQ" >
          Add New FAQ
        </button>
      </h2>
    </div>

    <div class="faq-list ajax-container"></div>

    @include ('admin.faq.partials.modal')
  </div>

  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  {{-- TinyMCE --}}
  <script src="{{ LpjHelpers::asset('js/admin/vendors/tinymce/tinymce.js') }}"></script>
  <script src="{{ LpjHelpers::asset('js/admin/faq.js') }}"></script>
@stop