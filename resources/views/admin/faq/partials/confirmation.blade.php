@if ( $action == 'delete' )

  <form action="{{ route('faq.destroy', $faq->id) }}" method="POST" class="confirmation-form">
    <div class="form-group">
      <p>Are you sure you want to delete <strong>{{ $faq->title }}</strong>? The delete action can’t be undone.</p>

      <p>Please type in your user pin code to confirm.</p>

      <label for="pincode">Pin Code:</label>
      <input type="password" class="form-control" id="pincode" name="pincode">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block">I understand the consequences, delete this faq</button>
    </div>
  </form>

@else

  <p>Update content</p>

@endif
