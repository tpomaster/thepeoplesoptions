<div class="row">
  @if ( $faq->thumbnail_path )
    <div class="form-group col-md-6 col-sm-6 col-xs-12">
      <img src="{{ $faq->thumbnail() }}" alt="{{ $faq->title }}" width="100%" height="auto" class="thumbnail">
    </div>
  @endif

  <div class="form-group col-md-6 col-sm-6 col-xs-12">
    <label for="thumbnail">Thumbnail</label>
    <input class="form-control" id="thumbnail" name="thumbnail" type="file">
  </div>
</div>

<div class="form-group ">
  <label class="control-label" for="title">Title</label>
  <input class="form-control" id="title" name="title" type="text" value="{{ old('title', $faq->title) }}" />
</div>
<div class="form-group ">
  <label class="control-label" for="content">Content</label>
  {{-- <wysiwyg></wysiwyg> --}}
  <!-- <input id="trix" type="hidden" name="content" value="{{ old('content', $faq->content) }}">
  <trix-editor input="trix"></trix-editor> -->

  <label class="control-label" for="content">TinyMCE</label>
  <textarea name="content" class="tinymce-editor">{{ old('content', $faq->content) }}</textarea>
  <script>
    var editor_config = {
      branding: false,
      path_absolute : "/",
      selector: ".tinymce-editor",
      height: 300,
      plugins: [
        "advlist autolink lists link image charmap preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script>
</div>