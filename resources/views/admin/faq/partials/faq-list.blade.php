@foreach ($faqs as $faq )

  <div class="faq-box">
    <div class="row">
      <div class="faq-thumbnail col-md-1 col-sm-12 col-xs-12">
        <?php 
          $imageHolder = $faq->thumbnail_path;
          $setImage = 'images/nthumb14.png';

          if ( !empty($imageHolder) ) {
            $setImage = 'storage/'.$imageHolder;
          }
        ?>
        <img src="<?php echo LpjHelpers::asset($setImage); ?>" alt="{{ $faq->title }}" width="100%" height="auto">
      </div>
      <div class="faq-content col-md-9 col-sm-12 col-xs-12">
        <h3><a>{{ $faq->title }}</a></h3>
        <div class="generated-content">
          <!-- {!! html_entity_decode($faq->content) !!} -->
          <p><?php echo LpjHelpers::getExcerpt($faq->content, 300); ?></p>
        </div>
      </div>
      <div class="faq-side col-md-2 col-ms-12 col-xs-12">
        <a href="{{ route('faq.show', $faq->id) }}"
          class="modal-btn btn btn-xs yellow-btn"
          title       = "{{ $faq->title }}"
          data-id     = "{{ $faq->id }}"
          data-action = "show"
          data-model  = "faq"
          data-title  = "{{ $faq->title }}">
          <span class="glyphicon glyphicon-search"></span>
        </a>
        {{-- <a class="modal-btn btn btn-xs theme-btn"><span class="glyphicon glyphicon-edit"></span></a> --}}
        <a href="#"
          class       = "modal-btn btn btn-xs theme-btn"
          title       = "Edit faq"
          data-id     = "{{ $faq->id }}"
          data-action = "edit"
          data-model  = "faq"
          data-title  = "Edit faq" >
          <span title="edit" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </a>
        <a href="{{ route('faq.confirmation', $faq->id) }}" class="delete-faq btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
      </div>
    </div>
  </div>

@endforeach

{{ $faqs->links('vendor.pagination.ajax-default') }}

