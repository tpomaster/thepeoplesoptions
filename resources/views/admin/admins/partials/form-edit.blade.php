<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#basic-information">Basic Information</a></li>
  @if (auth()->user()->hasRole('super-admin') || ( auth()->user()->id == $admin->id ))
    <li><a data-toggle="tab" href="#password">Password</a></li>
    <li><a data-toggle="tab" href="#pincode">Pincode</a></li>
  @endif
  @if (auth()->user()->hasRole('super-admin'))
    <li><a data-toggle="tab" href="#permissions-section">Permissions</a></li>
  @endif
</ul>

<div class="tab-content">
  <div id="basic-information" class="tab-pane fade in active">

    <h4>Basic Information</h4>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="firstname" name="firstname"  value="{{ old('firstname', $admin->firstname) }}" class="form-control col-md-7 col-xs-12">
        <span class="help-block error firstname-error"></span>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="lastname" name="lastname"  value="{{ old('lastname', $admin->lastname) }}" class="form-control col-md-7 col-xs-12">
        <span class="help-block error lastname-error"></span>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="email" id="email" name="email" value="{{ old('email', $admin->email) }}" class="form-control col-md-7 col-xs-12">
        <span class="help-block error email-error"></span>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span></label>
      <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="username" name="username" value="{{ old('username', $admin->username) }}" class="form-control col-md-7 col-xs-12">
        <span class="help-block error username-error"></span>
      </div>
    </div>
  </div>
  @if (auth()->user()->hasRole('super-admin') || ( auth()->user()->id == $admin->id ))
    <div id="password" class="tab-pane fade">
      <h4>Password</h4>

      @if (auth()->user()->id == $admin->id )
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_password">Current Password</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" id="current_password" name="current_password" class="form-control col-md-7 col-xs-12">
            <span><small>Leave blank to leave unchanged</small></span>
            <span class="help-block error current_password-error"></span>
          </div>
        </div>
      @endif

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">New Password</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12">
          <span><small>Leave blank to leave unchanged</small></span>
          <span class="help-block error password-error"></span>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password_confirmation">New Password Confirmation</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="password" id="password_confirmation" name="password_confirmation" class="form-control col-md-7 col-xs-12">
          <span class="help-block error password_confirmation-error"></span>
        </div>
      </div>
    </div>
    <div id="pincode" class="tab-pane fade">
      <h4>Pincode</h4>

      @if (auth()->user()->id == $admin->id )
        <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="current_pincode">Current Pincode</label>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="password" id="current_pincode" name="current_pincode" class="form-control col-md-7 col-xs-12">
            <span><small>Leave blank to leave unchanged</small></span>
            <span class="help-block error current_pincode-error"></span>
          </div>
        </div>
      @endif

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pincode">New Pincode</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="password" id="pincode" name="pincode" class="form-control col-md-7 col-xs-12">
          <span><small>Leave blank to leave unchanged</small></span>
          <span class="help-block error pincode-error"></span>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pincode_confirmation">New Pincode Confirmation</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="password" id="pincode_confirmation" name="pincode_confirmation" class="form-control col-md-7 col-xs-12">
          <span class="help-block error pincode_confirmation-error"></span>
        </div>
      </div>
    </div>
  @endif
  @if (auth()->user()->hasRole('super-admin'))
    <div id="permissions-section" class="tab-pane fade">
      <h4>Permissions</h4>
      @include ('admin.admins.partials.form-permissions')
    </div>
  @endif
</div>
