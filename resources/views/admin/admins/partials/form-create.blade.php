<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="firstname" name="firstname"  value="{{ old('firstname', $admin->firstname) }}" class="form-control col-md-7 col-xs-12">
    <span class="help-block error firstname-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="lastname" name="lastname"  value="{{ old('lastname', $admin->lastname) }}" class="form-control col-md-7 col-xs-12">
    <span class="help-block error lastname-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="email" id="email" name="email" value="{{ old('email', $admin->email) }}" class="form-control col-md-7 col-xs-12">
    <span class="help-block error email-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="username" name="username" value="{{ old('username', $admin->username) }}" class="form-control col-md-7 col-xs-12">
    <span class="help-block error username-error"></span>
  </div>
</div>


<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12">
    <span class="help-block error password-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password_confirmation">Password Confirmation <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control col-md-7 col-xs-12">
    <span class="help-block error password_confirmation-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pincode">Pincode <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="password" id="pincode" name="pincode" class="form-control col-md-7 col-xs-12">
    <span class="help-block error pincode-error"></span>
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pincode_confirmation">Pincode Confirmation <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="password" id="pincode_confirmation" name="pincode_confirmation" class="form-control col-md-7 col-xs-12">
    <span class="help-block error pincode_confirmation-error"></span>
  </div>
</div>

@include ('admin.admins.partials.form-permissions')
