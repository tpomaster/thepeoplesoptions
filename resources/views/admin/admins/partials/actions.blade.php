<a href="{{ route('admin.edit', $admin->id) }}" class="btn btn-xs theme-btn">Edit</a>

<form action="{{ route('admin.destroy', $admin->id) }}" method="POST">
  {{ csrf_field() }}
  {{ method_field('DELETE') }}

  <button type="submit" class="btn btn-xs btn-danger">Delete</button>
</form>

