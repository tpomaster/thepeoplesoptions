@if ( $action == 'delete' )

  <form action="{{ route('admin.destroy', $admin->id) }}" method="POST" class="confirmation-form">
    {{-- {{ csrf_field() }} --}}
    {{-- {{ method_field('DELETE') }} --}}
    <div class="form-group">
      <p>Are you sure you want to delete <strong>{{ $admin->username }}</strong>? The delete action can’t be undone and this admin will be permanently gone.</p>

      <p>Please type in your user pin code to confirm.</p>

      <label for="pincode">Pin Code:</label>
      <input type="password" class="form-control" id="pincode" name="pincode">
    </div>

    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block">I understand the consequences, delete this admin</button>
    </div>
  </form>

@else

  <p>Update content</p>

@endif
