<form action="{{ route('admin.update', $admin->id) }}" method="POST" data-parsley-validate class="modal-form form-horizontal form-label-left">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  @include ('admin.admins.partials.form-edit')

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <a href="{{ route('admin.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>
</form>
