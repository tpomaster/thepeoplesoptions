<form action="{{ route('admin.store') }}" method="POST" class="modal-form form-horizontal form-label-left">
  {{ csrf_field() }}

  @include ('admin.admins.partials.form-create')

  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <a href="{{ route('admin.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>
</form>
