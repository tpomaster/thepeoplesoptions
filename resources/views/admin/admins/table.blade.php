<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Email</th>
      <th>Username</th>
      <th>Role</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($admins as $admin)
      <tr>
        <td>{{ $admin->id }}</td>
        <td>{{ $admin->firstname }}</td>
        <td>{{ $admin->lastname }}</td>
        <td>{{ $admin->email }}</td>
        <td>{{ $admin->username }}</td>
        <td>
          @foreach ($admin->roles as $role)
            {{ $role->name }}
          @endforeach
        </td>
        <td>{{ $admin->status }}</td>
        <td>
          <a href="{{ route('admin.edit', $admin->id) }}" class="btn btn-xs theme-btn">Edit</a>

          <form action="{{ route('admin.destroy', $admin->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button type="submit" class="btn btn-xs btn-danger">Delete</button>
          </form>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($admins, 'links') )
  <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
     Showing {{ ($admins->currentpage() - 1) * $admins->perpage() + 1 }}
     to {{ (($admins->currentpage() - 1) * $admins->perpage()) + $admins->count() }}
     of {{ $admins->total() }} entries
    </div>
    <div class="text-right col-md-6 col-sm-6 col-xs-12">
      {{ $admins->links() }}
    </div>
  </div>
@endif



