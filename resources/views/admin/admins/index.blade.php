@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">

        <div class="header-page">
            <h1>Admin Users</h1>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>
                    {{-- <a href="{{ route('admin.create') }}" class="btn btn-md theme-btn">Add New</a> --}}
                    <button
                      class="modal-btn btn btn-md theme-btn"
                      type = "button"
                      title = "Add New Admin User"
                      data-toggle = "modal"
                      {{-- data-id = "{{ $admin->id }}" --}}
                      data-action = "create"
                      data-model = "users"
                      data-title = "Add New Admin User" >
                      Add New
                    </button>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="admin-users-table" class="table table-bordered">
                <thead>
                  <th>ID</th>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Role</th>
                  <th>Status</th>
                  <th>Actions</th>
                </thead>
              </table>
            </div>
        </div>

        <!-- <div class="x_panel">
          <div class="x_title">
            <h2>Admin DataTable</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <admin-data-table endpoint="{{ route('users.index') }}" route="{{ route('admin.index') }}"></admin-data-table>
          </div>
        </div> -->
    </div>
    <!-- /page content -->

    <!-- Admin Modal -->
    <div class="admin-modal modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body"></div>
        </div>
      </div>
    </div>
@endsection

@section('pagespecificscripts')
  {{-- Admins Scripts --}}
  <script src="{{ LpjHelpers::asset('js/admin/admins.js') }}"></script>
@stop
