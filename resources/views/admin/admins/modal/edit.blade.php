<form action="{{ route('admin.update', $admin->id) }}" method="POST" data-parsley-validate class="modal-form form-horizontal form-label-left">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="firstname" name="firstname" value="{{ old('firstname', $admin->firstname) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error firstname-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="lastname" name="lastname" value="{{ old('lastname', $admin->lastname) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error lastname-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="email" id="email" name="email" value="{{ old('email', $admin->email) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error email-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="username" name="username" value="{{ old('username', $admin->username) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error username-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role">Assign Role <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <select id="role" name="role" class="form-control col-md-7 col-xs-12">
          @foreach ($roles as $role)
              <option value="{{ $role->id }}"
                @foreach ($admin->roles as $admin_role)
                    @if ($admin_role->id == $role->id)
                      selected
                    @endif
                  @endforeach
                >{{ $role->name }}</option>
          @endforeach
      </select>
      <span class="help-block error role-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Assign Permissions <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">

      {{-- Selected Permissions --}}
      @php $selected_permissions = []; @endphp

      @foreach ($permissions as $permission)
          @foreach ($admin->permissions as $admin_permission)
              @if ($admin_permission->id == $permission->id)
                  @php
                      $selected_permissions[] = $permission->id;
                  @endphp
              @endif
          @endforeach
      @endforeach

      <div class="row">
        <div class="col-md-6">
            <strong>Users:</strong>
            @foreach ($permissions as $permission)
                @if ($permission->category == 'users')
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                            @if(is_array(old('permissions')) && in_array($permission->id, old('permissions')) || in_array($permission->id, $selected_permissions)) checked @endif
                            >{{ $permission->name }}
                        </label>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-6">
            <strong>Roles:</strong>
            @foreach ($permissions as $permission)
                @if ($permission->category == 'roles')
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                            @if(is_array(old('permissions')) && in_array($permission->id, old('permissions')) || in_array($permission->id, $selected_permissions)) checked @endif
                            >{{ $permission->name }}
                        </label>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-6">
            <strong>Permissions:</strong>
            @foreach ($permissions as $permission)
                @if ($permission->category == 'permissions')
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                            @if(is_array(old('permissions')) && in_array($permission->id, old('permissions')) || in_array($permission->id, $selected_permissions)) checked @endif
                            >{{ $permission->name }}
                        </label>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-6">
            <strong>Packages:</strong>
            @foreach ($permissions as $permission)
                @if ($permission->category == 'packages')
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                            @if(is_array(old('permissions')) && in_array($permission->id, old('permissions')) || in_array($permission->id, $selected_permissions)) checked @endif
                            >{{ $permission->name }}
                        </label>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-md-12">
          <span class="help-block error permissions-error"></span>
        </div>
      </div>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Password <span class="required"></span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <a href="{{ route('admin.password.change', $admin->id) }}" class="btn theme-btn">Change Password</a>
    </div>
  </div>

  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <a href="{{ route('admin.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>
</form>
