<form action="{{ route('admin.store') }}" method="POST" data-parsley-validate id="create-admin-form" class="modal-form form-horizontal form-label-left">
  {{ csrf_field() }}

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="firstname">First Name <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="firstname" name="firstname"  value="{{ old('firstname') }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error firstname-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lastname">Last Name <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="lastname" name="lastname"  value="{{ old('lastname') }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error lastname-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error email-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="text" id="username" name="username" value="{{ old('username') }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error username-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="password" id="password" name="password" class="form-control col-md-7 col-xs-12">
      <span class="help-block error password-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pincode">Pincode <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <input type="password" id="pincode" name="pincode" class="form-control col-md-7 col-xs-12">
      <span class="help-block error pincode-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Assign Role <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
      <select id="role" name="role" class="form-control col-md-7 col-xs-12">
          @foreach ($roles as $role)
            <option value="{{ $role->id }}">{{ $role->name }}</option>
          @endforeach
      </select>
      <span class="help-block error role-error"></span>
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Assign Permissions <span class="required">*</span></label>
    <div class="col-md-6 col-sm-6 col-xs-12">

      <div class="row">
          <div class="col-md-6">
              <strong>Users:</strong>
              @foreach ($permissions as $permission)
                  @if ($permission->category == 'users')
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                              @if(is_array(old('permissions')) && in_array($permission->id, old('permissions'))) checked @endif
                              >{{ $permission->name }}
                          </label>
                      </div>
                  @endif
              @endforeach
          </div>

          <div class="col-md-6">
              <strong>Roles:</strong>
              @foreach ($permissions as $permission)
                  @if ($permission->category == 'roles')
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                              @if(is_array(old('permissions')) && in_array($permission->id, old('permissions'))) checked @endif
                              >{{ $permission->name }}
                          </label>
                      </div>
                  @endif
              @endforeach
          </div>

          <div class="col-md-6">
              <strong>Permissions:</strong>
              @foreach ($permissions as $permission)
                  @if ($permission->category == 'permissions')
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                              @if(is_array(old('permissions')) && in_array($permission->id, old('permissions'))) checked @endif
                              >{{ $permission->name }}
                          </label>
                      </div>
                  @endif
              @endforeach
          </div>

          <div class="col-md-6">
              <strong>Packages:</strong>
              @foreach ($permissions as $permission)
                  @if ($permission->category == 'packages')
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" name="permissions[]" value="{{ $permission->id }}"
                              @if(is_array(old('permissions')) && in_array($permission->id, old('permissions'))) checked @endif
                              >{{ $permission->name }}
                          </label>
                      </div>
                  @endif
              @endforeach
          </div>
      </div>
      <span class="help-block error permissions-error"></span>
    </div>
  </div>


  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
      <a href="{{ route('admin.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>

  @if (count($errors))
      <ul class="alert alert-danger">
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
  @endif
</form>
