<!-- dashboard chart -->
<div class="row">
  <div class="col-md-12 col-xs-12">
    <div class="x_panel tile">
      <div class="x_title">
        <h2><i class="fa fa-line-chart"></i> DAILY EARNINGS</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="data-loading">
          <div class="col-md-12 col-xs-12">
            <i class="fa fa-cog fa-spin"></i>
            <p class="loader-text">Checking For New Content...</p>
          </div>
        </div>
        <div class="x-content-box">
          <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
              <div id="pkg-chart-1" style="height: 350px; min-width: 200px"></div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 top-agents">
              <div id="investment-chart" style="height: 350px"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>