@extends('admin.layouts.blank')

@section('main_container')

        <!-- page content -->
        <div class="right_col content-overide dashboard-content" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
              <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/teams.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-group"></i> Total Members</span>
                <div class="count"><?php echo $total_members; ?></div>
                <a href="{{ route('affiliates.index', 'list') }}"><span class="count_bottom">More Info »</span></a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
              <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/balances.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-bank"></i> TPO Bank Available Balance</span>
                <div class="count"><small>$</small> <?php echo LpjHelpers::amt2($balance); ?></div>
                <a href="{{ route('tpo-bank.index') }}"><span class="count_bottom">More Info »</span></a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
              <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-bar-chart"></i> Combined Earnings Total</span>
                <div class="count"><small>$</small> <?php echo LpjHelpers::amt2($combined_earnings); ?></div>
                <a href="{{ route('tpo-bank.index') }}"><span class="count_bottom">More Info »</span></a>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
              <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/payout.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-money"></i> Total Payouts</span>
                <div class="count"><small>$</small> <?php echo LpjHelpers::amt2($payout); ?></div>
                <a href="{{ route('payouts.index') }}"><span class="count_bottom">More Info »</span></a>
              </div>
            </div>
          </div>

          <!-- admin graph -->
          @include('admin/dashboard/charts/charts')


          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2><i class="fa fa-bullhorn"></i> 5 LATEST PURCHASE </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  @foreach ($transactions as $transaction)

                    <?php
                      $pkgDetails = false;
                      if ( !empty($transaction->details) ) {
                        $pkgDetails = json_decode($transaction->details, true);
                      }
                    ?>
                    <div class="x-admin-box">
                      <h3 class="purchase-title">
                        <?php if ( $pkgDetails ): ?>
                          <a href="/admin/<?php echo $catSlugs[$pkgDetails['package_category']] ?>/<?php echo $pkgDetails['package_id']; ?>">
                            <?php echo $transaction->description; ?>
                          </a>
                        <?php else: ?>
                          <?php echo $transaction->description; ?>
                        <?php endif ?>
                      </h3>
                      <p><strong>User:</strong> <?php echo $transaction->username; ?> <span>|</span> <strong>Amount:</strong> $ <?php echo LpjHelpers::amt2($transaction->amount); ?> <span>|</span> <strong>Date:</strong> <?php echo $transaction->transaction_date; ?></p>
                    </div>

                  @endforeach

                </div>
              </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                  <h2><i class="fa fa-bullhorn"></i> 5 LATEST REGISTRATION </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  @foreach ($users as $user)

                    <div class="x-admin-box">
                      <h3 class="username"><strong><?php echo $user->username; ?></strong> ( <?php echo $user->firstname; ?> <?php echo $user->lastname; ?> )  </h3>
                      <p><strong>email:</strong> <?php echo $user->email; ?> <span>|</span>
                        <strong> membership:</strong> <?php echo $user->membership; ?> <span>|</span>
                        <strong>Date:</strong> <?php echo $user->created_at; ?></p>
                    </div>

                  @endforeach

                </div>
              </div>
            </div>


            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2><i class="fa fa-envelope"></i> SEND ANNOUNCEMENT </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form id="announcement_frm" action="#" method="post">
                    <div class="form-group">
                      <input class="form-control" name="title" placeholder="Title" type="text">
                    </div>
                    <div>
                      <textarea class="textarea form-control" placeholder="Content" name="content" style="width: 100%; height: 250px; "></textarea>
                    </div>
                    <div>
                      <hr>
                      <button id="save_announcement_btn" class="pull-right btn btn-default" type="submit">Send <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <div style="display: none;">
            <a href="" class="get_graph_data_btn">Get Graph Data</a>

            <div class="graph_start_dates"></div>

            <form id="looping_graphdata_process">
                {{ csrf_field() }}
                <input type="text" id="new_start_date" name="new_start_date" value="<?php echo $new_start_date; ?>">
                <input type="text" id="runTime" name="runTime" value="<?php echo $runTime; ?>">
            </form>
        </div>
@endsection

@section('pagespecificscripts')
  <script src="https://code.highcharts.com/stock/highstock.js"></script>
  <script src="https://code.highcharts.com/highcharts-3d.js"></script>
  <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
  <script src="{{ LpjHelpers::asset('js/admin/dashboard.js') }}"></script>

  <script type="text/javascript">
    // line chart
    // setTimeout(function(){
      
    // }, 3000);

    var dashboardLoopProcessUrl = "{{ route('admin.loopDashboardGraphDataPost') }}";

    jQuery('.get_graph_data_btn').click(function(){
      loop_graphdata_process();
      return false;
    });

    jQuery( document ).ready(function() {
      jQuery('#new_start_date').val('');
      jQuery('#runTime').val('0');
      jQuery( ".get_graph_data_btn" ).trigger( "click" );
    });

    /*/ ==============================================================
    //  dashboard get graph data loop
    // ==============================================================/*/
    function loop_graphdata_process(){
        var form_elem = jQuery('#looping_graphdata_process');
        var input_data = form_elem.serialize();

        // $('.loader').fadeIn('fast');

        jQuery.ajax({
            type: "POST",
            url: dashboardLoopProcessUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery('#new_start_date').val(ret.new_start_date2);
                jQuery('#runTime').val(ret.new_runTime);
                jQuery('.graph_start_dates').append('<p>'+ret.new_start_date2+' - '+ ret.end_date +'</p>');
                jQuery('.loader-text').html(ret.message);

                if ( ret.msg == 'continue' ) {
                  console.log(ret);

                  loop_graphdata_process();

                }else{
                  console.log('DONE');
                  // $('.loader').fadeOut('fast');
                  $.getJSON('{{ route("admin.getChartData") }}?', function (data) {
                      // Create the chart 1
                      Highcharts.stockChart('pkg-chart-1', {

                          rangeSelector: {
                              selected: 1
                          },

                          title: {
                              text: 'Daily Earnings Chart'
                          },

                          series: [{
                              name: 'Earnings',
                              data: data,
                              tooltip: {
                                  valueDecimals: 2
                              }
                          }]
                      });

                      // donut chart
                      Highcharts.chart('investment-chart', {
                        chart: {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45
                            }
                        },
                        title: {
                            text: 'Funded Packages Summary'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 100,
                                depth: 45
                            }
                        },
                        series: [{
                            name: 'Delivered amount',
                            data: [
                                ['Trading', <?php echo $tradingFunds ?> ],
                                ['Exchange', <?php echo $exchangeFunds ?>],
                                ['Mining', <?php echo $miningFunds ?>],
                                ['CruptoShuffle', <?php echo $shuffleFunds ?>],
                            ]
                        }]
                    });
                  });
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
              handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
      }
  </script>
@stop
