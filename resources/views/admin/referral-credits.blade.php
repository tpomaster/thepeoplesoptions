@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/credits.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Referral Credits</h1>
    </div>

    <div class="credits-container row">
      <div class="col-md-12 col-s-12 col-xs-12">
        <div class="x_panel tile">
          <div class="x_title">
            <h2>TPO Bank (In/Out) Transactions History:</h2>
          </div>

          <div class="history-box">
            <div class="table-holder">
              <table class="table table-striped table-hover">
                <thead class="thead-inverse">
                  <tr>
                    <th>Package</th>
                    <th>Affiliate</th>
                    <th>Investor</th>
                    <th>Tier</th>
                    <th>Earned</th>
                    <th>Date</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$0.15</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$0.75</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="withdrawn">withdrawn</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$0.15</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$0.75</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Hope Ticket Testing</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$0.13</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Hope Ticket Testing</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$0.26</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Silver</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$6.00</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="withdrawn">withdrawn</span></td>
                  </tr>
                  <tr>
                    <td>Silver</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$12.00</td>
                    <td>Oct. 24 2017</td>
                    <td><span class="withdrawn">withdrawn</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$1.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$7.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$1.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="withdrawn">withdrawn</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$7.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Hope Ticket Testing</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$1.29</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Hope Ticket Testing</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$2.58</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Platinum</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$60.00</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>Platinum</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$120.00</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$0.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto T - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$2.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>tolits</td>
                    <td>tpomlitoconverted</td>
                    <td>3</td>
                    <td>$0.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                  <tr>
                    <td>TPO Crypto M - For Membership</td>
                    <td>litopj</td>
                    <td>tpomlitoconverted</td>
                    <td>1</td>
                    <td>$2.50</td>
                    <td>Oct. 19 2017</td>
                    <td><span class="completed">completed</span></td>
                  </tr>
                </tbody>
              </table>
              <p>Total Count: 20</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection
