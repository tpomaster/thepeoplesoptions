@extends('admin.layouts.blank')

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">

    <div class="header-page">
      <h1>Settings</h1>
    </div>

    <div class="x_panel tile">
      <div class="x_title">
        <a href="{{ route('settings.create') }}" class="new-setting-btn btn btn-md theme-btn" title="New Setting">Add New</a>
        <div class="clearfix"></div>
      </div>
      <div id="settings-container" class="x_content">
        {{-- @include ('admin.settings.partials.table') --}}
      </div>
    </div>
  </div>

  @include ('admin.components.modal');

  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  <script src="{{ LpjHelpers::asset('js/admin/settings.js') }}"></script>
@stop
