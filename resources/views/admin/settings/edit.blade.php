<form action="{{ route('settings.update', $setting->id) }}" method="POST">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}

  @include ('admin.settings.partials.form-fields')

</form>
