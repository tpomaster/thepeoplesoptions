<form action="{{ route('settings.store') }}" method="POST">
  {{ csrf_field() }}

  @include ('admin.settings.partials.form-fields')

</form>
