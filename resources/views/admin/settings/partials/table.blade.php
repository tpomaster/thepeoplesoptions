<table id="settings-table" class="datatable table table-striped table-hover">
  <thead>
    <tr>
      <th>ID</th>
      <th>Key</th>
      <th>Name</th>
      <th>Value</th>
      <th>Page</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($settings as $setting)
      <tr>
        <td>{{ $setting->id }}</td>
        <td>{{ $setting->key }}</td>
        <td>{{ $setting->name }}</td>
        <td>{{ $setting->value }}</td>
        <td>{{ $setting->page }}</td>
        <td>
          <a href="{{ route('settings.edit', $setting->id) }}" title="Edit Setting" class="edit-setting btn theme-btn">
            <span title="Edit Setting" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
          </a>
          <a href="{{ route('settings.confirmation', $setting->id) }}" title="Delete Setting" class="delete-setting btn btn-danger">
            <span title="Delete Setting" class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

{{ $settings->links() }}
