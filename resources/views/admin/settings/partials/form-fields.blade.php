<div class="form-group">
  <label for="key">Key</label>
  <input type="text" class="form-control" id="key" name="key" value="{{ old('key', $setting->key) }}">
  <span class="help-block key-error error"></span>
</div>

<div class="form-group">
  <label for="name">Name</label>
  <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $setting->name) }}">
  <span class="help-block name-error error"></span>
</div>

<div class="form-group">
  <label for="value">Value</label>
  <textarea class="form-control" rows="5" id="value" name="value">{{ old('value', $setting->value) }}</textarea>
  <span class="help-block value-error error"></span>
</div>

<div class="form-group">
  <label for="page">Page</label>
  <input type="text" class="form-control" id="page" name="page" value="{{ old('page', $setting->page) }}">
  <span class="help-block page-error error"></span>
</div>

<div class="form-group">
  <button type="submit" class="btn btn-primary">Submit</button>
</div>
