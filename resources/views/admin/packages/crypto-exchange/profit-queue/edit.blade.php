<form action="{{ route('crypto-exchange-profits-queue.update', $profit->id) }}" method="POST" class="queue-trade-form modal-form package-queued-form form-horizontal form-label-left">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  <div class="form-group">
    <label for="package_id">Package</label>
    <span class="form-control">{{ $package->name }}</span>
  </div>

  <div class="row">

    <div class="form-group col-sm-6 col-md-6">
      <label for="date">Trade Date: (format: yyyy-mm-dd)</label>
      <div class='input-group date datetimepicker' id='date'>
        <input type='text' class="form-control" name="date" value="{{ date('Y-m-d', strtotime( $profit->date)) }}" readonly />
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </span>
      </div>
      <span class="date-error help-block error"></span>
    </div>

    <div class="form-group col-sm-6 col-md-6">
      <label for="for_execution_date">Execution Date: (format: yyyy-mm-dd)</label>
      <div class='input-group date datetimepicker' id='exec_date'>
        <input type='text' class="form-control" name="exec_date" value="{{ date('Y-m-d', strtotime( $profit->exec_date)) }}" readonly />
        <span class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </span>
      </div>
      <span class="exec_date-error help-block error"></span>
    </div>

    <div class="form-group col-sm-6 col-md-6">
      <label for="exchange_amount">Package exchange Amount:</label>
      <div class="input-group">
        <input type="number" id="exchange_amount" name="exchange_amount" required="required" class="form-control" value="{{ LpjHelpers::amt2($profit->exchange_amount, false) }}" readonly>
        <span class="input-group-addon">$</span>
      </div>
    </div>

    <div class="form-group col-sm-6 col-md-6">
      <label for="profit_percent">Profit Percentage:</label>
      <div class="input-group">
        <input type="text" id="profit_percent" name="profit_percent" required="required" class="form-control" value="{{ LpjHelpers::amt2($profit->profit_percent, false) }}">
        <span class="input-group-addon">%</span>
      </div>
    </div>

    <div class="form-group col-sm-6 col-md-6">
      <label for="profit">Profit Amount:</label>
      <div class="input-group">
        <input type="text" id="profit" name="profit" required="required" class="form-control" value="{{ LpjHelpers::amt2($profit->profit, false) }}">
        <span class="input-group-addon">$</span>
      </div>
    </div>

  </div>

  <div class="form-group">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    <button type="submit" class="btn theme-btn">Save</button>
  </div>

</form>
