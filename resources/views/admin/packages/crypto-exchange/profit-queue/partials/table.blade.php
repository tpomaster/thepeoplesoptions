<table id="queued-profits-table" class="queued-profits-table queued-items-table table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>Package</th>
      <th>Profit</th>
      <th>Trade Date</th>
      <th>Execution Date</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($queued_profits as $profit)
      <tr class="queued-item"
        @if ( date('Y-m-d') == date('Y-m-d', strtotime($profit->exec_date)) )
          data-id="{{ $profit->id }}"
        @endif
      >
        <td>{{ $profit->name }}</td>
        <td>$ {{ LpjHelpers::amt2($profit->profit) }}</td>
        <td>{{ date('M d, Y', strtotime($profit->date)) }}</td>
        <td>{{ date('M d, Y', strtotime($profit->exec_date)) }}</td>
        <td>
          <a href="" class="compute-btn btn btn-xs theme-btn"
            title       = "Compute User Trade"
            data-title  = "Compute User Trade"

            @if ( date('Y-m-d') == date('Y-m-d', strtotime($profit->exec_date)) )
              data-id="{{ $profit->id }}"
            @endif
            {{-- data-id     = "{{ $profit->id }}" --}}
          >
            <i class="fa fa-calculator"></i>
          </a>
          <a href="{{ route('crypto-exchange-profits-queue.edit', $profit->id) }}" class="edit-btn btn btn-xs theme-btn"
            title       = "Edit Queued Profit"
            data-title  = "Edit Queued Profit"
          >
            <i class="fa fa-edit"></i>
          </a>
          <a href="{{ route('crypto-exchange-profits-queue.confirmation', $profit->id) }}" class="delete-btn btn btn-xs btn-danger"
            title       = "Delete Queued Profit"
            data-title  = "Queued Profit Delete Confirmation"
          >
            <span class="glyphicon glyphicon-remove"></span>
          </a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($queued_profits, 'links') )
  <div>
    <div>
      Showing {{($queued_profits->currentpage()-1)*$queued_profits->perpage()+1}}
      to {{(($queued_profits->currentpage()-1)*$queued_profits->perpage())+$queued_profits->count()}}
      of {{$queued_profits->total()}} entries
    </div>
  </div>
  <div class="no-filter">
    {{ $queued_profits->links() }}
  </div>
@endif
