<table class="table table-bordered">
  <thead>
    <tr>
      <th>Member</th>
      <th>Amount</th>
      <th>Purchase Date</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Profits Count</th>
      <th>Exchange Status</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ( $investors as $investor )
      <tr>
        <td>{{ $investor->firstname .' '. $investor->lastname . '( ' . $investor->username .' )' }}</td>
        <td>$ {{ LpjHelpers::amt2($investor->amount) }}</td>
        <td>{{ date('M. d, Y', strtotime( $investor->purchase_date )) }}</td>
        <td>{{ $investor->start_date ? date('M. d, Y', strtotime( $investor->start_date )) : '' }}</td>
        <td>
          @php
            $end_date = $investor->end_date ? strtotime( $investor->end_date ) : strtotime( $investor->start_date ." + 1 year" );
            echo date('M. d, Y', $end_date);
          @endphp
        </td>
        <td>{{ ucfirst($investor->profits_count) }}</td>
        <td>{{ ucfirst($investor->exchange_status) }}</td>
        <td>
          <a href="{{ route('referral-credits.show', [$investor->user_id, 'cryptoexchange', $package->id]) }}"
            title="Members Earned From This Purchase"
            class="view-referrals-btn btn theme-btn">
            <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
          </a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($investors, 'links') )
  <div>Showing {{($investors->currentpage()-1)*$investors->perpage()+1}} to {{(($investors->currentpage()-1)*$investors->perpage())+$investors->count()}} of {{$investors->total()}} entries</div>
  {{ $investors->links() }}
@endif
