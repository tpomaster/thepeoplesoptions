<div class="x_panel tile col-sm-12">

  <h2>Search Filter:</h2>

  <form action="{{ route('crypto-trading-packages.filter') }}" method="POST" class="package-filter row">
    {{ csrf_field() }}

    <div class="form-group col-md-4 col-sm-4 col-xs-12">
      <label>Name</label>
      <input type="text" name="name" class="form-control">
      <span class="name-error help-block error"></span>
    </div>

    <div class="form-group col-md-4 col-sm-4 col-xs-12">
      <label>Status</label>
      <select name="status" class="form-control">
        <option value="">All</option>
        @foreach ($options['statuses'] as $status)
          <option value="{{ $status }}">{{ ucwords($status) }}</option>
        @endforeach
      </select>
      <span class="status-error help-block error"></span>
    </div>

    <div class="form-group col-md-4 col-sm-4 col-xs-12">
      <label>Risk Profile</label>
      <select name="risk_profile" class="form-control">
        <option value="">All</option>
        @foreach ($options['risk_profiles'] as $risk_profile)
          <option value="{{ $risk_profile }}">{{ ucwords($risk_profile) }}</option>
        @endforeach
      </select>
      <span class="risk_profile-error help-block error"></span>
    </div>

    <div class="form-group col-md-12 col-sm-12 col-xs-12">
      <div class="pull-right">
        <button class="btn theme-btn" name="submit" type="submit">Filter</button>
      </div>
    </div>
  </form>

</div>
