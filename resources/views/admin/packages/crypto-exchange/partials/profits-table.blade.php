<table class="table table-bordered">
  <thead>
    <tr>
      <th>Profit</th>
      <th>Date</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($profits as $profit)
      <tr>
        <td>$ {{ LpjHelpers::amt2($profit->profit)}}</td>
        <td>{{ date('M d, Y', strtotime($profit->date)) }}</td>
        <td>
          <a href="{{ route('crypto-exchange-profits.confirmation', $profit->id) }}" title="Delete Profit" class="delete-profit btn btn-danger"><span title="delete" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($profits, 'links') )
  <div>Showing {{($profits->currentpage()-1)*$profits->perpage()+1}} to {{(($profits->currentpage()-1)*$profits->perpage())+$profits->count()}} of {{$profits->total()}} entries</div>
  {{ $profits->links() }}
@endif
