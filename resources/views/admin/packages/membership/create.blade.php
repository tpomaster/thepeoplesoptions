<form action="{{ route('membership-packages.store') }}" method="POST" data-parsley-validate class="modal-form package-form form-horizontal form-label-left">
  {{ csrf_field() }}

  @include ('admin.packages.membership.partials.form')

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="text-center">
      <a href="{{ route('membership-packages.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>
</form>
