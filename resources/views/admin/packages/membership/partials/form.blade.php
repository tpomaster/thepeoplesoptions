<div class="row">
  <div class="col-md-7 col-sm-6 col-xs-12">
    <div class="form-group">
      <label for="name">Package Name <span class="required">*</span></label>
      <input type="text" id="name" name="name" value="{{ old('name', $package->name) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error name-error"></span>
    </div>

    <div class="form-group">
      <label for="status">Status <span class="required">*</span></label>
      <select id="status" name="status" class="form-control col-md-7 col-xs-12">
        @foreach ($statuses as $status)
          @php $selected = $status == $package->status ? 'selected' : '' @endphp

          <option value="{{ $status }}" {{ $selected }}>{{ ucfirst($status) }}</option>
        @endforeach
      </select>

      <span class="help-block error status-error"></span>
    </div>

    <div class="form-group">
      <label for="membership_fee">Membership Fee <span class="required">*</span></label>
      <input type="number" step="any" id="membership_fee" name="membership_fee" value="{{ old('membership_fee', $package->membership_fee) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error membership_fee-error"></span>
    </div>

    <div id="package-config-container" class="form-group multiple-form-group" data-max=3>
      <label for="packages_config">Amount to invest on packages:</label>

      <div id="packages-config">

        @if ( $package->packages_config )
          @php
            $packages_config = unserialize($package->packages_config);
            $package_counter = 0;
          @endphp

          @foreach ($packages_config as $config)
            <div class="row remove-package-{{ $package_counter }}">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <label>Category</label>
                <input type="text" name="package_category[]" class="form-control" value="{{ $config['package_category'] }}"></input>
                <span class="help-block error package_category_{{ $package_counter }}-error"></span>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <label>Package ID</label>
                <input type="number" step="any" name="package_id[]" class="form-control" value="{{ $config['package_id'] }}"></input>
                <span class="help-block error package_id_{{ $package_counter }}-error"></span>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <label>Amount to invest</label>
                <input type="number" step="any" name="package_amount[]" class="form-control" value="{{ $config['package_amount'] }}"></input>
                <span class="help-block error package_amount_{{ $package_counter }}-error"></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-12">
                <label>&nbsp;</label>
                <button class="remove-package-btn btn btn-danger form-control" data-package-count="{{ $package_counter }}" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
              </div>
            </div>
            @php
              $package_counter++;
            @endphp
          @endforeach
        @else
          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
              <label>Category</label>
              <input type="text" name="package_category[]" class="form-control"></input>
              <span class="help-block error package_category_0-error"></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label>Package ID</label>
              <input type="number" step="any" name="package_id[]" class="form-control"></input>
              <span class="help-block error package_id_0-error"></span>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
              <label>Amount to invest</label>
              <input type="number" step="any" name="package_amount[]" class="form-control"></input>
              <span class="help-block error package_amount_0-error"></span>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
              <label>&nbsp;</label>
              <button class="btn btn-danger form-control" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
            </div>
          </div>
        @endif
      </div>

      <div class="row text-right">
        <div class="col-md-offset-10 col-md-2 col-sm-offset-10 col-sm-2 col-xs-12">
          <button class="add-package-btn btn theme-btn form-control" type="button"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
        </div>
      </div>
    </div>

  </div>

  <div class="col-md-5 col-sm-6 col-xs-12">
    <div class="form-group">

      <div id="tier-config-container" class="form-group multiple-form-group" data-max=3>
        <label for="tier_config">Tier Config</label>

        <div id="tier-config">

          @if ( $package->tier_config )
            @php
              $tier_config = unserialize($package->tier_config);
              $tier_counter = 0;
            @endphp

            @foreach ($tier_config as $config)
              <div class="row remove-tier-{{ $tier_counter }}">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Tier #</label>
                  <input type="number" step="any" name="tier_level[]" class="form-control" value="{{ $config['tier_level'] }}"></input>
                  <span class="help-block error tier_level_{{ $tier_counter }}-error"></span>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Referral Credit %</label>
                  <input type="number" step="any" name="referral_credit[]" class="form-control" value="{{ $config['referral_credit'] }}"></input>
                  <span class="help-block error referral_credit_{{ $tier_counter }}-error"></span>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-12">
                  <label>&nbsp;</label>
                  <button class="remove-tier-btn btn btn-danger form-control" data-tier-count="{{ $tier_counter }}" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
                </div>
              </div>
              @php
                $tier_counter++;
              @endphp
            @endforeach
          @else
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Tier #</label>
                <input type="number" step="any" name="tier_level[]" class="form-control"></input>
                <span class="help-block error tier_level_0-error"></span>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Referral Credit %</label>
                <input type="number" step="any" name="referral_credit[]" class="form-control"></input>
                <span class="help-block error referral_credit_0-error"></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-12">
                <label>&nbsp;</label>
                <button class="btn btn-danger form-control" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
              </div>
            </div>
          @endif
        </div>

        <div class="row text-right">
          <div class="col-md-offset-10 col-md-2 col-sm-offset-10 col-sm-2 col-xs-12">
            <button class="add-tier-btn btn theme-btn form-control" type="button"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

