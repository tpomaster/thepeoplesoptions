@if ($packages->isNotEmpty())
  @foreach ($packages as $package)

    @php
      switch ($package->status) {
        case 'available':
          $status_class = 'label tpo-label-success';
          break;

        case 'active':
          $status_class = 'label tpo-label-info';
          break;

        case 'closed':
          $status_class = 'label tpo-label-default';
          break;

        default:
          $status_class = 'label tpo-label-success';
          break;
      }
    @endphp

    <div class="package-info col-md-4 col-xs-12">
      <div class="panel">
        <div class="panel-heading"><strong><img src="{{LpjHelpers::asset('images/package-avatar.png')}}" class="package-avatar" alt="">  {{ strtoupper($package->name) }}</strong></div>
        <div class="panel-body">
          <div class="row">
            <ul class="col-md-10 col-xs-12">
              <li class="list-group-item">
                <strong>Status:</strong> <span class="package-stats {{ $package->status }} pull-right">{{ strtoupper($package->status) }}</span>
              </li>
              <li class="list-group-item">
                <strong>Membership Fee:</strong> <span class="pull-right">{{ $package->membership_fee }}</span>
              </li>
            </ul>
            <div class="col-md-2 col-xs-12">
              <button
                class       = "modal-btn btn theme-btn"
                type        = "button"
                title       = "Edit Crypto Membership Packages"
                data-toggle = "modal"
                data-id     = {{ $package->id }}
                data-action = "edit"
                data-model  = "membership-packages"
                data-title  = "Edit Crypto Membership Packages" >
                <span title="edit" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
              </button>

              <a href="{{ route('membership-packages.show', $package->id) }}" class="btn theme-btn">
                <span title="show" class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
              </a>

              <a href='{{ route('membership-packages.confirmation', $package->id) }}' title='DELETE' class='delete-package btn btn-danger'><span title="delete" class="glyphicon glyphicon-remove" aria-hidden="true"></span></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

  @endforeach

  <div class="col-md-12 col-sm-12 col-xs-12">
    {{ $packages->links('vendor.pagination.ajax-default') }}
  </div>
@else
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="alert alert-info text-center">
      <strong>No packages found.</strong>
    </div>
  </div>
@endif
