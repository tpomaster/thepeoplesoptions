<table class="table table-bordered">
  <thead>
    <tr>
      <th>Member</th>
      <th>Amount</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Status</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ( $members as $member )
      <tr>
        <td>{{ $member->firstname .' '. $member->lastname . '( ' . $member->username .' )' }}</td>
        <td>$ {{ LpjHelpers::amt2($member->amount) }}</td>
        <td>{{  date('M. d, Y', strtotime( $member->start_date )) }}</td>
        <td>
          @php
            $end_date = $member->end_date ? strtotime( $member->end_date ) : strtotime( $member->start_date ." + 1 year" );
            echo date('M. d, Y', $end_date);
          @endphp
        </td>
        <td>{{ ucfirst($member->status) }}</td>
        <td>
          <a href="{{ route('referral-credits.show', [$member->user_id, 'membership', $package->id]) }}"
            title="Members Earned From This Purchase" class="view-referrals-btn btn theme-btn">
            <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
          </a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($members, 'links') )
  <div>Showing {{($members->currentpage()-1)*$members->perpage()+1}} to {{(($members->currentpage()-1)*$members->perpage())+$members->count()}} of {{$members->total()}} entries</div>

  {{ $members->links() }}
@endif
