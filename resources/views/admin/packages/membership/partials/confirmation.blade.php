@if ( $members_count == 0 )

  @if ( $action == 'delete' )

    <form action="{{ route('membership-packages.destroy', $package->id) }}" method="POST" class="confirmation-form">
      {{-- {{ csrf_field() }} --}}
      {{-- {{ method_field('DELETE') }} --}}
      <div class="form-group">
        <p>Are you sure you want to delete <strong>{{ $package->name }}</strong>? The delete action can’t be undone and all your package's content will be permanently gone.</p>

        <p>Please type in your user pin code to confirm.</p>

        <label for="pincode">Pin Code:</label>
        <input type="password" class="form-control" id="pincode" name="pincode">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">I understand the consequences, delete this package</button>
      </div>
    </form>

  @else

    <p>Update content</p>

  @endif

@else

  <div class="alert alert-info">
    <p><strong>Can't delete package that have investors.</strong></p>
  </div>

@endif
