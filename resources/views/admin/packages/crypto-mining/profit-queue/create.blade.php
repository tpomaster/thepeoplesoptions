<form action="{{ route('crypto-mining-profits-queue.store') }}" method="POST" class="queue-profits-form queue-form modal-form package-queued-form form-horizontal form-label-left">
  {{ csrf_field() }}

  @php
    $package_id = isset($package) ? $package->id : '';
    $mining_amount = isset($package) ? $package->amount : '';
  @endphp

  <div class="form-group">
    <label for="package_id">Select Package</label>

    <select id="package_id" name="package_id" class="form-control" {{ $package_id ? 'disabled' : '' }}>
      <option value="">Select Package</option>
      @foreach ($packages as $package)
        <option value="{{ $package->id }}" {{ $package->id == $package_id ? 'selected' : '' }}>{{ $package->name }}</option>
      @endforeach
    </select>

    @if ($package_id)
      <input type="hidden" name="package_id" value="{{ $package_id }}">
    @endif
  </div>

  <div class="form-group">
    <label for="date">Trade Date: (format: yyyy-mm-dd)</label>
    <div class='input-group date' id='date'>
      <input type='text' class="form-control" name="date" readonly="readonly" />
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
    <span class="date-error help-block error"></span>
  </div>

  <div class="form-group">
    <label for="for_execution_date">Execution Date: (format: yyyy-mm-dd)</label>
    <div class='input-group date' id='exec_date'>
      <input type='text' class="form-control" name="exec_date" value="{{ date('Y-m-d') }}" readonly="readonly" />
      <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
      </span>
    </div>
    <span class="exec_date-error help-block error"></span>
  </div>

  <div class="form-group">
    <label for="mining_amount">Package Mining Amount:</label>
    <div class="input-group">
      <input type="number" id="mining_amount" name="mining_amount" required="required" class="package-amount form-control" value="{{ $mining_amount }}" readonly>
      <span class="input-group-addon">$</span>
    </div>
  </div>

  <div class="form-group">
    <label for="profit_percent">Profit Percentage:</label>
    <div class="input-group">
      <input type="text" id="profit_percent" name="profit_percent" required="required" class="form-control">
      <span class="input-group-addon">%</span>
    </div>
  </div>

  <div class="form-group">
    <label for="profit">Profit Amount:</label>
    <div class="input-group">
      <input type="text" id="profit" name="profit" required="required" class="form-control">
      <span class="input-group-addon">$</span>
    </div>
  </div>

  <div class="form-group">
    <button type="submit" class="btn theme-btn">Submit</button>
  </div>

</form>
