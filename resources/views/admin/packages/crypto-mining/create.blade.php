<form action="{{ route('crypto-mining-packages.store') }}" method="POST" data-parsley-validate class="modal-form package-form form-horizontal form-label-left">
  {{ csrf_field() }}

  @include ('admin.packages.crypto-mining.partials.form')

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="text-center">
      <a href="{{ route('crypto-mining-packages.index') }}" class="btn btn-danger">Cancel</a>
      <button type="submit" class="btn theme-btn">Submit</button>
    </div>
  </div>
</form>
