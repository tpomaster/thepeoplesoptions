@if ($investors_count > 0 && $action == 'delete')

  <div class="alert alert-info">
    <p><strong>Can't delete package that have investors.</strong></p>
  </div>

@else
    <form action="{{ $route }}" method="POST" class="confirmation-form">
      {{ csrf_field() }}

      @if (in_array($action, ['update', 'close']))
        {{ method_field('PATCH') }}
      @else
        {{ method_field('DELETE') }}
      @endif

      <div class="form-group">
        <p>
          Are you sure you want to {{ $action }} <strong>{{ $package->name }}</strong>?
          @if ($action == 'delete')
            The delete action can’t be undone and all your package's content will be permanently gone.
          @endif
        </p>

        <p>Please type in your user pin code to confirm.</p>

        <label for="pincode">Pin Code:</label>
        <input type="password" class="form-control" id="pincode" name="pincode">
        <span class="help-block error pincode-error"></span>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">I understand the consequences, {{ $action }} this package</button>
      </div>
    </form>

@endif
