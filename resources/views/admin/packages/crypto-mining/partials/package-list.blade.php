@if ($packages->isNotEmpty())

  @foreach ($packages as $package)

    @php
      switch ($package->risk_profile) {
        case 'conservative':
          $panel_class = 'tpo-default';
          break;

        case 'moderate':
          $panel_class = 'tpo-primary';
          break;

        case 'aggressive':
          $panel_class = 'tpo-info';
          break;

        case 'risky':
          $panel_class = 'tpo-warning';
          break;

        case 'high-risk':
          $panel_class = 'tpo-danger';
          break;

        default:
          $panel_class = 'tpo-default';
          break;
      }

      switch ($package->status) {
        case 'available':
          $status_class = 'label tpo-label-success';
          break;

        case 'active':
          $status_class = 'label tpo-label-info';
          break;

        case 'closed':
          $status_class = 'label tpo-label-default';
          break;

        default:
          $status_class = 'label tpo-label-success';
          break;
      }
    @endphp

    <div class="package-info col-md-6 col-xs-12">
      <div class="panel {{ $panel_class }}">
        <div class="panel-heading"><strong><img src="{{LpjHelpers::asset('images/package-avatar.png')}}" class="package-avatar" alt=""> {{ $package->name }}</strong></div>
        <div class="panel-body">
          <div class="row">
            <ul class="col-md-10 col-xs-12">
              <li class="list-group-item">
                <strong>Funded: </strong> <span class="pull-right">$ {{ LpjHelpers::amt2($package->funded_amount) }} ( {{ LpjHelpers::amt2(($package->funded_amount / $package->amount) * 100) }}% )</span>
                <div class="clearfix"></div>
                <div class="progress">
                  <div class="progress-bar" role="progressbar" aria-valuenow="{{ LpjHelpers::amt2(($package->funded_amount / $package->amount) * 100) }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ LpjHelpers::amt2(($package->funded_amount / $package->amount) * 100) }}%">
                    <span class="sr-only">{{ LpjHelpers::amt2(($package->funded_amount / $package->amount) * 100) }}% Complete</span>
                  </div>
                </div>
              </li>
              <li class="list-group-item">
                <strong>Status:</strong> <span class="package-stats {{ $package->status }} pull-right">{{ strtoupper($package->status) }}</span>
              </li>
              <li class="list-group-item">
                <strong>Risk Profile:</strong> <span class="risk-status pull-right">{{ ucfirst($package->risk_profile) }}</span>
              </li>
              <li class="list-group-item">
                <strong>For Membership:</strong> <span class="pull-right">{{ $package->for_membership ? 'For Membership' : 'Public Offer' }}</span>
              </li>
              <li class="list-group-item">
                <strong>Package Total Amount:</strong> <span class="pull-right">{{ $package->amount }}</span>
              </li>
              <li class="list-group-item">
                <strong>No. of profits posted:</strong> <span class="pull-right">{{ $package->posted_profits }}</span>
              </li>
            </ul>
            <div class="col-md-2 col-xs-12">
              <button
                class       = "modal-btn btn theme-btn"
                type        = "button"
                title       = "Edit Crypto Mining Packages"
                data-toggle = "modal"
                data-id     = {{ $package->id }}
                data-action = "edit"
                data-model  = "crypto-mining-packages"
                data-title  = "Edit Crypto Mining Packages" >
                <span title="edit" class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
              </button>

              <a href="{{ route('crypto-mining-packages.show', $package->id) }}" class="btn theme-btn">
                <span title="show" class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
              </a>

              @if ($package->status == 'funded-active')
                <a href="{{ route('crypto-mining-profits-queue.create', $package->id) }}" class="add-profit-queue-btn btn theme-btn" title="Add Profit Queue">
                  <i class="fa fa-line-chart"></i>
                </a>
              @endif

              <a href='{{ route('crypto-mining-packages.confirmation', $package->id) }}' title='DELETE' class='delete-package btn btn-danger'><span title="delete" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

  @endforeach

  <div class="col-md-12 col-sm-12 col-xs-12">
    {{ $packages->links('vendor.pagination.ajax-default') }}
  </div>
@else
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="alert alert-info text-center">
      <strong>No packages found.</strong>
    </div>
  </div>
@endif

