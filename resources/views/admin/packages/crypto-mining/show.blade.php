@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/package-info.css') }}">

@stop

@section('main_container')
  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>View Package</h1>
    </div>

    <div class="row">
      <div class="package-summary col-md-3 col-xs-12">
        <div class="x_panel">
          <h2 class="panel-title"><img src="{{LpjHelpers::asset('images/package-avatar.png')}}" class="package-avatar" alt=""> {{ $package->name }}</h2>
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <span class="info-header">Package Status:</span>
              <span class="pull-right package-status {{ $package->status }}">
                  {{ strtoupper($package->status) }}
              </span>
            </li>
          </ul>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <span class="info-header">Trade Status:</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">Start Date: </span>
              <span class="pull-right">{{ $package->start_date ? date('M. d, Y', strtotime( $package->start_date )) : 'Pending' }}</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">End Date: </span>
              <span class="pull-right">{{ $package->end_date ? date('M. d, Y', strtotime( $package->end_date )) : 'Pending' }}</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">No. of profits posted: </span>
              <span class="pull-right">{{ $package->posted_profits ?: 0 }}</span>
            </li>
          </ul>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <span class="info-header">Profit Totals:</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">Profit: </span>
              <span class="pull-right">$ {{ LpjHelpers::amt2($earnings['mining_profit']) }}</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">Management Fee: </span>
              <span class="pull-right">$ {{ LpjHelpers::amt2($earnings['management_fee']) }}</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">Net Earning: </span>
              <span class="pull-right">$ {{ LpjHelpers::amt2($earnings['net_earning']) }}</span>
            </li>
          </ul>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <span class="info-header">Others:</span>
            </li>

            <li class="list-group-item">
              <span class="info-title">Referral Credits: </span>
              <span class="pull-right">$ {{ LpjHelpers::amt2($referral_credits) }}</span>
            </li>
          </ul>

          @if ($package->status != 'closed')
            <div class="text-center">
              <a href="{{ route('crypto-mining-packages.confirmation', $package->id) }}" title="Close {{ $package->name }} Package" class="close-package btn theme-btn">CLOSE THIS PACKAGE</a>
            </div>
          @endif
        </div>
      </div>

      <div class="col-md-9 col-xs-12">
        <div class="single-package-container package-container" data-package-id={{ $package->id }}>
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a  href="#package-information" data-toggle="tab">Package Information</a></li>
              <li><a href="#package-investors" data-toggle="tab">Investors</a></li>
              <li><a href="#package-profits" data-toggle="tab">Profits</a></li>
            </ul>
            <div class="tab-content clearfix">
              <div class="tab-pane fade in active" id="package-information">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                        <h2 class="tab-title">{{ $package->name }}</h2>
                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                            <span class="info-title">For Membership Package Only: </span>
                            <span class="pull-right">{{ $package->for_membership ? 'Yes' : 'No' }}</span>
                          </li>

                          <li class="list-group-item">
                            <span class="info-title">Duration: </span>
                            <span class="pull-right">{{ $package->duration }} {{ str_plural('Day', $package->duration) }}</span>
                          </li>

                          <li class="list-group-item">
                            <span class="info-title">Package Amount: </span>
                            <span class="pull-right">$ {{ $package->amount }}</span>
                          </li>

                          <li class="list-group-item {{ $package->risk_profile }}">
                            <span class="info-title">Risk Profile: </span>
                            <span class="package-status risk-status pull-right">{{ strtoupper($package->risk_profile) }}</span>
                          </li>

                          <li class="list-group-item">
                            <span class="info-title">Trade Frequency: </span>
                            <span class="pull-right">{{ ucfirst($package->trade_frequency) }}</span>
                          </li>
                        </ul>
                      </div>
                      <div class="x_panel">
                        <h2 class="tab-title">Package Terms</h2>
                        <div class="panel-wrap">
                          <button type="button" class="btn theme-btn" data-toggle="modal" data-target="#package-terms-conditions">View Terms and Conditions</button>
                          <!-- Modal -->
                          <div class="modal fade" id="package-terms-conditions" role="dialog">
                            <div class="modal-dialog modal-lg">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Terms and Conditions</h4>
                                </div>
                                <div class="modal-body">
                                  <p>Package Terms and Conditions</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                        <h2 class="tab-title">Management Fee ( % )</h2>
                        <div class="panel-wrap">
                          @php
                            $management_fee = unserialize($package->management_fee);
                          @endphp

                          <table class="table table-striped table-hover">
                            <thead>
                              <tr>
                                <th>Membership</th>
                                <th>Management Fee</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($management_fee as $key => $value)
                                <tr>
                                  <td>{{ ucfirst($key) }}</td>
                                  <td>{{ $value }} %</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="x_panel">
                        <h2 class="tab-title">Tier Levels</h2>
                        <div class="panel-wrap">
                          @php
                            $tier_config = unserialize($package->tier_config);
                          @endphp

                          <table class="table table-striped table-hover">
                            <thead>
                              <tr>
                                <th>Tier</th>
                                <th>Referral Credit</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($tier_config as $config)
                                <tr>
                                  <td>{{ ucfirst($config['tier_level']) }}</td>
                                  <td>{{ $config['referral_credit'] }} %</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="package-investors"></div>
              <div class="tab-pane fade" id="package-profits"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->

  @include ('admin.packages.modal')

@endsection

@section('pagespecificscripts')
  <script src="{{ LpjHelpers::asset('js/admin/packages/crypto-mining/show.js') }}"></script>
@stop
