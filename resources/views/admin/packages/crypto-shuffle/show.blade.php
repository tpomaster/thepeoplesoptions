@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/package-info.css') }}">

@stop

@section('main_container')
  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>View Package</h1>
    </div>

    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="single-package-container package-container" data-package-id="{{ $package->id }}">
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul class="nav nav-tabs">
              <li class="active"><a  href="#package-information" data-toggle="tab">Package Information</a></li>
              <li><a href="#package-orders" data-toggle="tab">Orders</a></li>
              <li><a href="#package-winners" data-toggle="tab">Winners</a></li>
            </ul>
            <div class="tab-content clearfix">
              <div class="tab-pane fade in active" id="package-information">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                        <h2 class="tab-title">{{ $package->name }}</h2>
                          <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <span class="info-header">Package Status:</span>
                              <span class="pull-right package-stats {{ $package->status }}">
                                  {{ strtoupper($package->status) }}
                              </span>
                            </li>
                            <li class="list-group-item">
                              <span class="info-header">Max Ticket per Order:</span>
                              <span class="pull-right">{{ $package->max_per_order }}</span>
                            </li>
                            <li class="list-group-item">
                              <span class="info-header">Ticket Price:</span>
                              <span class="pull-right">$ {{ LpjHelpers::amt2($package->ticket_price) }}</span>
                            </li>
                            <li class="list-group-item">
                              <span class="info-header">Referral Credit Per Ticket:</span>
                              <span class="pull-right">$ {{ LpjHelpers::amt2($package->referral_base_credit) }}</span>
                            </li>
                          </ul>
                      </div>
                      <div class="x_panel">
                        <h2 class="tab-title">Tier Levels</h2>
                        <div class="panel-wrap">
                          @php
                            $tier_config = unserialize($package->tier_config);
                          @endphp

                          <table class="table table-striped table-hover">
                            <thead>
                              <tr>
                                <th>Tier</th>
                                <th>Referral Credit</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($tier_config as $config)
                                <tr>
                                  <td>{{ ucfirst($config['tier_level']) }}</td>
                                  <td>{{ $config['referral_credit'] }} %</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <div class="x_panel">
                        <h2 class="tab-title">Stats</h2>
                        <div class="panel-wrap">
                          <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                              <span class="info-header">Sold Tickets:</span>
                              <span class="pull-right">{{ $package->sold_ticket_cnt }}</span>
                            </li>
                            <li class="list-group-item">
                              <span class="info-header">Unsold Tickets:</span>
                              <span class="pull-right">{{ $package->ticket_total_cnt - $package->sold_ticket_cnt }}</span>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="x_panel">
                        <h2 class="tab-title">Package Terms</h2>
                        <div class="panel-wrap">
                          <button type="button" class="btn theme-btn" data-toggle="modal" data-target="#package-terms-conditions">View Terms and Conditions</button>
                          <!-- Modal -->
                          <div class="modal fade" id="package-terms-conditions" role="dialog">
                            <div class="modal-dialog modal-lg">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Terms and Conditions</h4>
                                </div>
                                <div class="modal-body">
                                  <p>Package Terms and Conditions</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              {{-- Package Orders --}}
              <div class="tab-pane fade" id="package-orders"></div>
              {{-- Pacakge Winners --}}
              <div class="tab-pane fade" id="package-winners"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->

  @include ('admin.packages.modal')
@endsection

@section('pagespecificscripts')
  <script src="{{ LpjHelpers::asset('js/admin/packages/crypto-shuffle/show.js') }}"></script>
@stop
