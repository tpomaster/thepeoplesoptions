@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/package-info.css') }}">

@endsection

@section('main_container')

  <!-- page content -->
  <div class="packages-content right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Crypto Shuffle Packages</h1>
    </div>

    <div class="row">
      <div class="col-md-12 col-xs-12">
        <button
          class       = "modal-btn btn theme-btn"
          type        = "button"
          title       = "Add New Crypto Shuffle Packages"
          data-toggle = "modal"
          data-action = "create"
          data-model  = "crypto-shuffle-packages"
          data-title  = "Add New Crypto Shuffle Packages" >
          Add Package
        </button>
      </div>

      <section class="packages ajax-container">
        @if (count($packages) > 0)
          @include ('admin.packages.crypto-shuffle.partials.package-list')
        @endif
      </section>

    </div>
  </div>
  <!-- /page content -->

  @include ('admin.packages.modal')

@endsection

@section('pagespecificscripts')
  {{-- Packages Scripts --}}
  <script src="{{ LpjHelpers::asset('js/admin/packages.js') }}"></script>
@stop
