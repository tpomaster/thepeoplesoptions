@if ($orders->count() > 0)

  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Package</th>
        <th>Affiliate</th>
        <th>Ticket Count</th>
        <th>Amount</th>
        <th>Date</th>
        <th>Status</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($orders as $order)
        <tr>
          <td>{{ $order->package_name }}</td>
          <td>{{ $order->username }}</td>
          <td>{{ $order->quantity }}</td>
          <td>$ {{ LpjHelpers::amt2($order->total_amount) }}</td>
          <td>{{ date('F d, Y', strtotime($order->date)) }}</td>
          <td>{{ ucfirst($order->status) }}</td>
          <td>
            <a href="{{ route('referral-credits.show', [$order->user_id, 'cryptoshuffle', $order->package_id]) }}" title="Members Earned From This Purchase" class="view-referrals-btn btn-sm theme-btn"><span aria-hidden="true" class="glyphicon glyphicon-zoom-in"></span></a>
            <a href="{{ route('crypto-shuffle-orders.confirmation', $order->id) }}" title="Delete Order" class="delete-order btn-sm btn-danger"><span title="delete" aria-hidden="true" class="glyphicon glyphicon-remove"></span></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if ( method_exists($orders, 'links') )
    <div>
      Showing {{($orders->currentpage()-1) * $orders->perpage()+1}}
      to {{(($orders->currentpage()-1) * $orders->perpage()) + $orders->count()}}
      of {{$orders->total()}} entries
    </div>

    {{ $orders->links('vendor.pagination.ajax-default') }}
  @else
    Total Orders: {{ $orders->count() }}
  @endif

@else
  <div class="alert alert-info text-center">
    No Orders Yet.
  </div>
@endif
