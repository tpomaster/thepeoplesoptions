<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      <label for="name">Package Name <span class="required">*</span></label>
      <input type="text" id="name" name="name" value="{{ old('name', $package->name) }}" class="form-control col-md-7 col-xs-12">
      <span class="help-block error name-error"></span>
    </div>

    <div class="form-group">
      <label for="status">Status <span class="required">*</span></label>
      <select id="status" name="status" class="form-control col-md-7 col-xs-12">
        @foreach ($statuses as $status)
          @php $selected = $status == $package->status ? 'selected' : '' @endphp

          <option value="{{ $status }}" {{ $selected }}>{{ ucfirst($status) }}</option>
        @endforeach
      </select>
      <span class="help-block error status-error"></span>
    </div>

    <div class="form-group">
      <label for="hide">Hide <span class="required">*</span></label>
      <select id="hide" name="hide" class="form-control col-md-7 col-xs-12">
        <option value="0" {{ !$package->hide ? 'selected' : '' }}>No</option>
        <option value="1" {{ $package->hide ? 'selected' : '' }}>Yes</option>
      </select>
      <span class="help-block error hide-error"></span>
    </div>

    <div class="form-group">
      <label for="max_per_order">Max Ticket per Order <span class="required">*</span></label>
      <input type="number" step="any" id="max_per_order" name="max_per_order" value="{{ old('max_per_order', $package->max_per_order) }}" class="form-control col-md-7 col-xs-12" {{ $package->sold_ticket_cnt > 0 ? 'disabled' : '' }}>
      <span class="help-block error max_per_order-error"></span>
    </div>

    <div class="form-group">
      <label for="ticket_price">Ticket Price <span class="required">*</span></label>
      <input type="number" step="any" id="ticket_price" name="ticket_price" value="{{ old('ticket_price', $package->ticket_price) }}" class="form-control col-md-7 col-xs-12" {{ $package->sold_ticket_cnt > 0 ? 'disabled' : '' }}>
      <span class="help-block error ticket_price-error"></span>
    </div>

    <div class="form-group">
      <label for="referral_base_credit">Referral Credit Per Ticket <span class="required">*</span></label>
      <input type="number" step="any" id="referral_base_credit" name="referral_base_credit" value="{{ old('referral_base_credit', $package->referral_base_credit) }}" class="form-control col-md-7 col-xs-12" {{ $package->sold_ticket_cnt > 0 ? 'disabled' : '' }}>
      <span class="help-block error referral_base_credit-error"></span>
    </div>

  </div>

  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">

      <div id="tier-config-container" class="form-group multiple-form-group" data-max=3>
        <label for="tier_config">Tier Config</label>

        <div id="tier-config">

          @if ( $package->tier_config )
            @php
              $tier_config = unserialize($package->tier_config);
              $tier_counter = 0;
            @endphp

            @foreach ($tier_config as $config)
              <div class="row remove-tier-{{ $tier_counter }}">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Tier #</label>
                  <input type="number" name="tier_level[]" class="form-control" value="{{ $config['tier_level'] }}" {{ $package->sold_ticket_cnt > 0 ? 'disabled' : '' }}></input>
                  <span class="help-block error tier_level_{{ $tier_counter }}-error"></span>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Referral Credit %</label>
                  <input type="number" name="referral_credit[]" class="form-control" value="{{ $config['referral_credit'] }}" {{ $package->sold_ticket_cnt > 0 ? 'disabled' : '' }}></input>
                  <span class="help-block error referral_credit_{{ $tier_counter }}-error"></span>
                </div>

                <div class="col-md-2 col-sm-2 col-xs-12">
                  <label>&nbsp;</label>
                  <button class="remove-tier-btn btn btn-danger form-control" data-tier-count="{{ $tier_counter }}" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
                </div>
              </div>
              @php
                $tier_counter++;
              @endphp
            @endforeach
          @else
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Tier #</label>
                <input type="number" name="tier_level[]" class="form-control"></input>
                <span class="help-block error tier_level_0-error"></span>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Referral Credit %</label>
                <input type="number" name="referral_credit[]" class="form-control"></input>
                <span class="help-block error referral_credit_0-error"></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-12">
                <label>&nbsp;</label>
                <button class="btn btn-danger form-control" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
              </div>
            </div>
          @endif
        </div>

        @if ($package->sold_ticket_cnt <= 0)
          <div class="row text-right">
            <div class="col-md-offset-10 col-md-2 col-sm-offset-10 col-sm-2 col-xs-12">
              <button class="add-tier-btn btn theme-btn form-control" type="button"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
