<table id="queued-trades-table" class="queued-trades-table table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>Package</th>
      <th>Profit</th>
      <th>Trade Date</th>
      <th>Execution Date</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($queued_trades as $trade)
      <tr class="queued-trade-item"
        @if ( date('Y-m-d') == date('Y-m-d', strtotime($trade->exec_date)) )
          data-id="{{ $trade->id }}"
        @endif
      >
        <td>{{ $trade->name }}</td>
        <td>$ {{ LpjHelpers::amt2($trade->profit) }}</td>
        <td>{{ date('M d, Y', strtotime($trade->date)) }}</td>
        <td>{{ date('M d, Y', strtotime($trade->exec_date)) }}</td>
        <td>
          <a href="{{ route('crypto-trading-trades.compute') }}" class="compute-btn btn btn-xs theme-btn"
            title       = "Compute User Trade"
            data-title  = "Compute User Trade"

            @if ( date('Y-m-d') == date('Y-m-d', strtotime($trade->exec_date)) )
              data-id="{{ $trade->id }}"
            @endif
            {{-- data-id     = "{{ $trade->id }}" --}}
          >
            <i class="fa fa-calculator"></i>
          </a>
          <a href="{{ route('crypto-trading-trades-queue.edit', $trade->id) }}" class="edit-btn btn btn-xs theme-btn"
            title       = "Edit Queued Trade"
            data-title  = "Edit Queued Trade"
          >
            <i class="fa fa-edit"></i>
          </a>
          <a href="{{ route('crypto-trading-trades-queue.confirmation', $trade->id) }}" class="delete-btn btn btn-xs btn-danger"
            title       = "Delete Queued Trade"
            data-title  = "Queued Trade Delete Confirmation"
          >
            <span class="glyphicon glyphicon-remove"></span>
          </a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($queued_trades, 'links') )
  <div>
    <div>
      Showing {{($queued_trades->currentpage()-1)*$queued_trades->perpage()+1}}
      to {{(($queued_trades->currentpage()-1)*$queued_trades->perpage())+$queued_trades->count()}}
      of {{$queued_trades->total()}} entries
    </div>
  </div>
  <div class="no-filter">
    {{ $queued_trades->links() }}
  </div>
@endif
