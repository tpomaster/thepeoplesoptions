@foreach ($queued_trades as $trade)
  <tr>
    <td>{{ $trade->name }}</td>
    <td>$ {{ LpjHelpers::amt2($trade->profit) }}</td>
    <td>{{ date('M d, Y', strtotime($trade->date)) }}</td>
    <td>{{ date('M d, Y', strtotime($trade->exec_date)) }}</td>
    <td>
      <a href="" class="modal-btn btn btn-xs theme-btn"><i class="fa fa-edit"></i></a>
      <a href="" class="delete-transaction btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
    </td>
  </tr>
@endforeach
