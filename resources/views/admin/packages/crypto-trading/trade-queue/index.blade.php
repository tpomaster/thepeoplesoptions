@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/package-queue.css') }}">

@endsection

@section('main_container')

  <!-- page content -->
  <div class="queue-content right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Package</h1>
    </div>

    <div class="row">
      <div class="col-md-6 col-s-12 col-xs-12">
        <div class="x_panel tile">
          <div class="x_title">
            <h2>Queued Trades List</h2>
          </div>
          <div class="queued-trades-box queued-box">
            <div class="queued-trades-container table-holder"></div>
            <div class="row">
              <div class="col-md-4 col-s-12 col-xs-12">
                <button type="button" class="compute-all-btn btn theme-btn full">Compute All</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-s-12 col-xs-12">
        <div class="x_panel tile">
          <div class="x_title">
            <h2>Queue Trade</h2>
          </div>
          <div class="queued-box">
            @include ('admin.packages.crypto-trading.trade-queue.create')
          </div>
        </div>
      </div>
    </div>
  </div>

  @include ('admin.packages.crypto-trading.trade-queue.partials.modal')
@endsection

@section('pagespecificscripts')
  {{-- Package Queue Scripts --}}
  {{-- <script src="{{ LpjHelpers::asset('js/admin/package-queue.js') }}"></script> --}}
  <script src="{{ LpjHelpers::asset('js/admin/packages/crypto-trading/trades-queue.js') }}"></script>
@stop
