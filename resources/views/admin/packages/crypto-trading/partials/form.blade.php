<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      <label for="name">Package Name <span class="required">*</span></label>
      <input type="text" id="name" name="name" value="{{ old('name', $package->name) ? old('name', $package->name) :  'TPO Crypto T - ' }}" class="form-control col-md-7 col-xs-12">
    </div>

    <div class="form-group">
      <label for="status">Status <span class="required">*</span></label>
      <select id="status" name="status" class="form-control col-md-7 col-xs-12">
        @foreach ($options['statuses'] as $status)
          @php $selected = $status == $package->status ? 'selected' : '' @endphp

          <option value="{{ $status }}" {{ $selected }}>{{ ucfirst($status) }}</option>
        @endforeach
      </select>

      <span class="help-block error status-error"></span>
    </div>

    <div class="form-group">
      <label for="hide">Hide <span class="required">*</span></label>
      <select id="hide" name="hide" class="form-control col-md-7 col-xs-12">
        <option value="0" {{ !$package->hide ? 'selected' : '' }}>No</option>
        <option value="1" {{ $package->hide ? 'selected' : '' }}>Yes</option>
      </select>
      <span class="help-block error for_membership-error"></span>
    </div>

    <div class="form-group">
      <label for="for_membership">For Membership Package Only <span class="required">*</span></label>
      <select id="for_membership" name="for_membership" class="form-control col-md-7 col-xs-12" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
        <option value="0" {{ !$package->for_membership ? 'selected' : '' }}>No</option>
        <option value="1" {{ $package->for_membership ? 'selected' : '' }}>Yes</option>
      </select>
      <span class="help-block error for_membership-error"></span>
    </div>

    <div class="form-group">
      <label for="duration">Duration (No. of Days) <span class="required">*</span></label>
      <input type="text" id="duration" name="duration" value="{{ old('duration', $package->duration) }}" class="form-control col-md-7 col-xs-12" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
      <span class="help-block error duration-error"></span>
    </div>

    <div class="form-group">
      <label for="amount">Total amount of package <span class="required">*</span></label>
      <input type="number" step="any" id="amount" name="amount" value="{{ old('amount', $package->amount) }}" class="form-control col-md-7 col-xs-12" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
      <span class="help-block error amount-error"></span>
    </div>

    <div class="form-group">
      <label for="risk_profile">Risk Profile <span class="required">*</span></label>
      <select id="risk_profile" name="risk_profile" class="form-control col-md-7 col-xs-12" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
        @foreach ($options['risk_profiles'] as $risk_profile)
          @php $selected = $risk_profile == $package->risk_profile ? 'selected' : '' @endphp

          <option value="{{ $risk_profile }}" {{ $selected }}>{{ ucfirst($risk_profile) }}</option>
        @endforeach
      </select>

      <span class="help-block error risk_profile-error"></span>
    </div>

    <div class="form-group">
      <label for="trade_frequency">Trade Frequency <span class="required">*</span></label>
      <select id="trade_frequency" name="trade_frequency" class="form-control col-md-7 col-xs-12" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
        @foreach ($options['trade_frequencies'] as $trade_frequency)
          @php $selected = $trade_frequency == $package->trade_frequency ? 'selected' : '' @endphp

          <option value="{{ $trade_frequency }}" {{ $selected }}>{{ ucfirst($trade_frequency) }}</option>
        @endforeach
      </select>

      <span class="help-block error risk_profile-error"></span>
    </div>
  </div>

  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      <label for="management_fee">Management Fee (%)</label>

      @php
        $management_fee = unserialize($package->management_fee);
      @endphp

      <div class="row">
        @foreach ($options['member_types'] as $member_type)
          <div class="col-md-3 col-sm-3 col-xs-12">
            <label for="management_fee[{{ $member_type }}]">{{ ucfirst($member_type) }}</label>
            <input type="number" name="management_fee[{{ $member_type }}]" value="{{ old('management_fee['.$member_type.']', $management_fee[$member_type]) }}" class="form-control" {{ $package->funded_status > 0 ? 'disabled' : '' }}>
            <span class="help-block error management_fee_{{$member_type}}-error"></span>
          </div>
        @endforeach
      </div>
      <div id="tier-config-container" class="form-group multiple-form-group" data-max=3>
        <label for="tier_config">Tier Config</label>

        <div id="tier-config">

          @if ( $package->tier_config )
            @php
              $tier_config = unserialize($package->tier_config);
              $tier_counter = 0;
            @endphp

            @foreach ($tier_config as $config)
              <div class="row remove-tier-{{ $tier_counter }}">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Tier #</label>
                  <input type="number" step="any" name="tier_level[]" class="form-control" value="{{ $config['tier_level'] }}" {{ $package->funded_status > 0 ? 'disabled' : '' }}></input>
                  <span class="help-block error tier_level_{{ $tier_counter }}-error"></span>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <label>Referral Credit %</label>
                  <input type="number" step="any" name="referral_credit[]" class="form-control" value="{{ $config['referral_credit'] }}" {{ $package->funded_status > 0 ? 'disabled' : '' }}></input>
                  <span class="help-block error referral_credit_{{ $tier_counter }}-error"></span>
                </div>

                @if ($package->funded_status <= 0)
                  <div class="col-md-2 col-sm-2 col-xs-12">
                    <label>&nbsp;</label>
                    <button class="remove-tier-btn btn btn-danger form-control" data-tier-count="{{ $tier_counter }}" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
                  </div>
                @endif
              </div>
              @php
                $tier_counter++;
              @endphp
            @endforeach
          @else
            <div class="row">
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Tier #</label>
                <input type="number" name="tier_level[]" class="form-control"></input>
                <span class="help-block error tier_level_0-error"></span>
              </div>
              <div class="col-md-5 col-sm-5 col-xs-12">
                <label>Referral Credit %</label>
                <input type="number" name="referral_credit[]" class="form-control"></input>
                <span class="help-block error referral_credit_0-error"></span>
              </div>

              <div class="col-md-2 col-sm-2 col-xs-12">
                <label>&nbsp;</label>
                <button class="btn btn-danger form-control" type="button"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
              </div>
            </div>
          @endif
        </div>

        @if ($package->funded_status <= 0)
          <div class="row text-right">
            <div class="col-md-offset-10 col-md-2 col-sm-offset-10 col-sm-2 col-xs-12">
              <button class="add-tier-btn btn theme-btn form-control" type="button"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
