<table id="trades-table" class="table table-bordered">
  <thead>
    <tr>
      <th>Earning</th>
      <th>Number of Trades</th>
      <th>Trade Date</th>
      <th>&nbsp;</th>
    </tr>
  </thead>
  <tbody>
    @foreach ( $trades as $trade )
      <tr>
        <td>$ {{ LpjHelpers::amt2($trade->profit)}}</td>
        <td>{{ $trade->num_of_trades }}</td>
        <td>{{ date('M d, Y', strtotime($trade->date)) }}</td>
        <td>
          <a href="{{ route('crypto-trading-trades.confirmation', $trade->id) }}" title="Delete Trade" class="delete-trade btn btn-danger"><span title="delete" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($trades, 'links') )
  <div>Showing {{($trades->currentpage()-1)*$trades->perpage()+1}} to {{(($trades->currentpage()-1)*$trades->perpage())+$trades->count()}} of {{$trades->total()}} entries</div>

  {{ $trades->links() }}
@endif
