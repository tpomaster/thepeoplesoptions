﻿<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>The Peoples Options</title>

    <!-- Styles -->
    <link href="{{ LpjHelpers::asset('css/admin/app.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ LpjHelpers::asset('css/gentelella.min.css') }}" rel="stylesheet">
    <!-- page specific styles -->
    @yield('pagespecificstyles')
</head>
<body id="dashboard" class="nav-md">
    <div class="loader toggle-hidden">
        <i class="fa fa-cog fa-spin"></i>
    </div>
    <div id="app" class="container body">

        <div class="main_container">

            @include('admin/includes/sidebar')

            @include('admin/includes/topbar')

            @yield('main_container')

            @include('admin/includes/flash-msg')

            @include('admin/includes/confirmation')

            @include('admin/includes/footer')

        </div>

    </div>

    <!-- Scripts -->
    <script src="{{ LpjHelpers::asset('js/admin/app.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ LpjHelpers::asset('js/gentelella.min.js') }}"></script>

    <script src="{{ LpjHelpers::asset('js/member/tock.min.js') }}"></script>

    <!-- Request::is('admin/crypto-trading-packages')
    url()->current() != url('/admin') -->
    @if (! Route::is('admin.dashboard'))
      <script src="{{ LpjHelpers::asset('js/admin/helper.js') }}"></script>
    @endif

    @yield('pagespecificscripts')
</body>
</html>
