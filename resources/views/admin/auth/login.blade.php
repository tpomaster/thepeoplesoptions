@extends('admin.layouts.app')

@section('content')
  	{{-- <login-form></login-form> --}}
  <div class="container">
      <div class="row">
          <div class="login-container">
              <img src="{{LpjHelpers::asset('images/login-logo.png')}}" class="logo" alt="logo"/>
              <h2 class="pull-right">LOGIN</h2>
              <div class="clearfix"></div>

              <form action="{{ route('admin.login.submit') }}" id="login-form" class="form-horizontal" role="form">
                  {{ csrf_field() }}
                  <div class="login-box">
                      <div class="form-group">
                          <div class="field-container">
                              <label>Username</label>
                              <input id="username" type="username" class="form-control" name="username" required autofocus>
                              <span class="glyphicon glyphicon-user input-icon"></span>
                              <span class="username-error help-block error"></span>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="field-container">
                              <label>Password</label>
                              <input id="password" type="password" class="form-control" name="password" required>
                              <span class="glyphicon glyphicon-lock input-icon"></span>
                              <span class="password-error help-block error"></span>
                          </div>
                      </div>

                      <div class="field-container row">
                          <div class="col-md-8 text-left nopadding">
                              <p>Have you forgotten your <a class="bold" href="">password?</a></p>
                              <p><a><i class="fa fa-home"></i> Back to Home Page</a></p>
                          </div>
                          <div class="col-md-4 text-right nopadding">
                              <button type="submit" class="btn yellow-btn">Login</button>
                          </div>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
@endsection

@section('pagespecificscripts')
  {{-- Packages Scripts --}}
  <script src="{{ LpjHelpers::asset('js/admin/login.js') }}"></script>
@stop
