<!-- edit bank description -->
<div class="modal fade" id="edit-desc" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Bank Transaction Description</h4>
      </div>
      <div class="modal-body">
        <form id="editdesc-form">
          <div class="form-group">
            <label class="control-label">Transaction Description </label>
            <textarea class="form-control"></textarea>
          </div>

          <div class="form-group">
              <button class="btn theme-btn" name="submit" type="submit">Submit</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end edit bank description -->