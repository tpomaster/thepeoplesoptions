<!-- added fund -->
<div class="modal fade" id="added-fund" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve Added fund via BTS</h4>
      </div>
      <div class="modal-body">
        <form id="addedfund-form">
          <div class="form-group">
            <label class="control-label">Enter Transaction Code </label>
            <textarea class="form-control" id="banktrxdesc_inp" name="trx_desc"></textarea>
          </div>

          <div class="form-group">
              <button class="btn theme-btn" name="submit" type="submit">Submit</button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end added fund -->