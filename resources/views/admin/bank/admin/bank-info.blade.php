<!-- bank info -->
<div class="modal fade" id="bank-info" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Other TPO Bank Info.</h4>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          <li class="list-group-item">
            <span class="info-title">Unpushed Earnings</span> <span class="pull-right">$228,672.12</span>
          </li>
          <li class="list-group-item">
            <span class="info-title">Active CryptoTrading Funds</span> <span class="pull-right">$519,374.00</span>
          </li>
          <li class="list-group-item">
            <span class="info-title">Active CryptoMining Funds</span> <span class="pull-right">$92,925.00</span>
          </li>  
          <li class="list-group-item">
            <span class="info-title">Active CryptoExchange Funds</span> <span class="pull-right">$0.00</span>
          </li>  
          <li class="list-group-item">
            <span class="info-title">Current CryptoShuffle Sold Tickets Total</span> <span class="pull-right">$77,778.00</span>
          </li>  
        </ul>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end bank info -->