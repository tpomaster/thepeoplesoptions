<form action="{{ route('tpo-bank.send-fund') }}" method="POST" class="fund-confirmation confirmation-form">
  {{-- {{ csrf_field() }} --}}
  {{-- {{ method_field('DELETE') }} --}}
  <div class="form-group">
    <p>Are you sure you want to send {{ $amount }} to {{ $username }}?</p>

    <p>Please type in your user pin code to confirm.</p>

    <label for="pincode">Pin Code:</label>
    <input type="password" class="form-control" id="pincode" name="pincode">
  </div>

  <div class="form-group">
    <input type="hidden" name="amount" value="{{ $amount }}">
    <input type="hidden" name="username" value="{{ $username }}">
    <input type="hidden" name="description" value="{{ $description }}">
    <button type="submit" class="btn btn-primary btn-block">Send Fund</button>
  </div>
</form>
