@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/bank.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>TPO Bank</h1>
    </div>

    <div class="row">
      <div class="bank-container">
        <div class="col-md-3 col-xs-12">
          <div class="tile-stats-box">
            <div class="icon-holder"><img src="{{LpjHelpers::asset('images/balances.png')}}" alt="" class="icon-holder-img"></div>
            <span class="count_top"><i class="fa fa-pie-chart"></i> TOTAL BANK BALANCE</span>
            <div class="count">$ {{ LpjHelpers::amt2($summary['balance']) }}</div>
          </div>
          <button type="button" class="btn theme-btn full" data-toggle="modal" data-target="#send-fund">Send Fund</button>
          <!-- send fund -->
          @include('admin/bank/admin/send-fund')

          <div class="transactions-summary x_panel tile">
            <div class="x_title">
              <h2>Bank Transactions Summary</h2>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-usd"></i>
                <a href="{{ route('tpo-bank-transaction.filter') }}" class="filter-link" data-section="deposit">$ {{ LpjHelpers::amt2($summary['added_fund']) }}</a>
              </h3>
              <h4><span class="in">in</span> Added Fund </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-money"></i>
                <a href="{{ route('referral-credits.index') }}" target="_blank">$ {{ LpjHelpers::amt2($summary['referral_credits']) }}</a>
              </h3>
              <h4><span class="in">in</span> Referral Credits </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-line-chart"></i>
                <a>$ {{ LpjHelpers::amt2($summary['package_earnings']) }}</a>
              </h3>
              <h4><span class="in">in</span> Package Earnings </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-sign-out"></i>
                <a href="{{ route('tpo-bank-transaction.filter') }}" class="filter-link" data-section="closed-package-fund">$ {{ LpjHelpers::amt2($summary['closed_package_fund']) }}</a>
              </h3>
              <h4><span class="in">in</span> Closed Package Fund - Pushed to Bank </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-credit-card"></i>
                <a href="{{ route('tpo-bank-transaction.filter') }}" class="filter-link" data-section="withdrawal">$ {{ LpjHelpers::amt2($summary['payouts']) }} </a>
              </h3>
              <h4><span class="out">out</span> Payouts/Withdrawals </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-shopping-cart"></i>
                <a href="{{ route('tpo-bank-transaction.filter') }}" class="filter-link" data-section="package-payment">$ {{ LpjHelpers::amt2($summary['package_payment']) }}</a>
              </h3>
              <h4><span class="out">out</span> Invested on TPO Packages </h4>
            </div>
            <div class="breakdown-box">
              <h3>
                <i class="fa fa-exchange"></i>
                <a href="{{ route('tpo-bank-transaction.filter') }}" class="filter-link" data-section="cap-off-trade-amount">$ {{ LpjHelpers::amt2($summary['cap_off_trade_amount']) }}</a>
              </h3>
              <h4><span class="out">out</span> Used to Cap Off Trade Amounts </h4>
            </div>
          </div>

          <button type="button" class="btn theme-btn full" data-toggle="modal" data-target="#added-fund">Approve added fund via Bitshares</button>
          <!-- added fund -->
          @include('admin/bank/admin/added-fund')

          <button type="button" class="btn yellow-btn full" data-toggle="modal" data-target="#bank-info">Other Bank Info</button>
          <!-- bank info -->
          @include('admin/bank/admin/bank-info')
        </div>
        <div class="col-md-9 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Search Filter:</h2>
              <a class="btn theme-btn toggle-search"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
            <form action="{{ route('tpo-bank-transaction.filter') }}" method="POST" class="transactions-filter row toggle-hidden">
              {{ csrf_field() }}

              <div class="form-group col-md-4 col-xs-12">
                <label>Username</label>
                <input type="text" name="username" class="form-control">
                <span class="username-error help-block error"></span>
              </div>

              <div class="form-group col-md-4 col-s-4 col-xs-12">
                <label>Type</label>
                <select name="type" class="form-control">
                  <option value="">All</option>
                  <option value="in">In</option>
                  <option value="out">Out</option>
                </select>
                <span class="type-error help-block error"></span>
              </div>

              <div class="form-group col-md-4 col-s-4 col-xs-12">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="">All</option>
                  <option value="completed">Completed</option>
                  <option value="pending">Pending</option>
                  <option value="cancelled">Cancelled</option>
                </select>
                <span class="status-error help-block error"></span>
              </div>

              <div class="form-group col-md-6 col-xs-12">
                <label>From</label>
                <div class='input-group date' id='from-date'>
                  <input type='text' class="form-control" name="from_date" readonly="readonly" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
                <span class="from_date-error help-block error"></span>
              </div>

              <div class="form-group col-md-6 col-xs-12">
                <label>To</label>
                <div class='input-group date' id='to-date'>
                  <input type='text' class="form-control" name="to_date" readonly="readonly" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
                <span class="to_date-error help-block error"></span>
              </div>

              <div class="form-group col-md-12">
                <div class="pull-right">
                  <button class="btn theme-btn" name="submit" type="submit">Filter</button>
                </div>
              </div>
            </form>
            <div class="x_title">
              <h2>TPO Bank (In/Out) Transactions History:</h2>
            </div>
            <div class="history-box">
              <div class="transactions-table table-holder">
                <table id="bank-transactions-history" class="table table-striped table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>User</th>
                      <th>Description</th>
                      <th>Amount</th>
                      <th>Type</th>
                      <th>Date</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($transactions as $transaction)
                      <tr>
                        <td>{{ $transaction->username }}</td>
                        <td>{{ $transaction->description }}</td>
                        <td>$ {{ LpjHelpers::amt2($transaction->amount) }} </td>
                        <td><span class="type {{ $transaction->section .' '. $transaction->type }}">{{ $transaction->type }}</span></td>
                        <td>{{ date('M. d, Y', strtotime($transaction->transaction_date)) }}</td>
                        <td><span class="{{ $transaction->status }}">{{ $transaction->status }}</span></td>
                        <td>
                          <a href="{{ route('tpo-bank-transaction.edit', $transaction->id) }}" class="modal-btn btn btn-xs theme-btn" title="Edit Description" data-id="{{ $transaction->id }}" data-action="edit" data-model="tpo-bank-transaction" data-title="Edit Description">
                            <i class="fa fa-edit"></i>
                          </a>

                          @if ($transaction->section == 'deposit')
                            <a href="{{ route('tpo-bank-transaction.confirmation', $transaction->id) }}" title="DELETE" class="delete-transaction btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>

                <div>
                  @if ( method_exists($transactions, 'links') )
                    <div>Showing {{($transactions->currentpage()-1)*$transactions->perpage()+1}} to {{(($transactions->currentpage()-1)*$transactions->perpage())+$transactions->count()}} of {{$transactions->total()}} entries</div>
                  @endif
                </div>
                <div class="no-filter">
                  {{ $transactions->links() }}
                </div>
                <!-- edit bank description -->
              </div>
              @include('admin.bank.admin.partials.modal')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  {{-- Bank Scripts --}}
  <script src="{{ LpjHelpers::asset('js/admin/bank.js') }}"></script>
@stop
