<!-- send fund -->
<div class="modal fade" id="send-fund" data-backdrop="static" data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Fund</h4>
      </div>
      <div class="modal-body">
        <form id="sendfund-form" action="{{ route('tpo-bank.send-fund-confirmation') }}" method="POST">
          <div class="form-group">
            <label class="control-label">Enter Amount ($)</label>
            <input class="form-control lpj_num_only" id="amount" name="amount" type="text">
          </div>

          <div class="form-group">
            <label class="control-label">Username</label>
            <input class="form-control" id="username" name="username" type="text">
          </div>

          <div class="form-group ">
            <label class="control-label">Description</label>
            <textarea class="form-control" id="description" name="description" cols="30" rows="2"></textarea>
          </div>

          <div class="form-group">
              <button class="btn theme-btn" name="submit" type="submit">Submit</button>
              <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
          <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>
<!-- end send fund -->
