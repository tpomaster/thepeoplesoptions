<table id="bank-transactions-history" class="filter table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>User</th>
      <th>Description</th>
      <th>Amount</th>
      <th>Type</th>
      <th>Date</th>
      <th>Status</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($transactions as $transaction)
      <tr>
        <td>{{ $transaction->username }}</td>
        <td>{{ $transaction->description }}</td>
        <td>$ {{ LpjHelpers::amt2($transaction->amount) }} </td>
        <td><span class="type {{ $transaction->section .' '. $transaction->type }}">{{ $transaction->type }}</span></td>
        <td>{{ $transaction->transaction_date }}</td>
        <td><span class="{{ $transaction->status }} complete">{{ $transaction->status }}</span></td>
        <td>
          <a href="{{ route('tpo-bank-transaction.edit', $transaction->id) }}" class="modal-btn btn btn-xs theme-btn" title="Edit Description" data-id="{{ $transaction->id }}" data-action="edit" data-model="tpo-bank-transaction" data-title="Edit Description">
            <i class="fa fa-edit"></i>
          </a>

          @if ($transaction->section == 'deposit')
            <a href="{{ route('tpo-bank-transaction.confirmation', $transaction->id) }}" title="DELETE" class="delete-transaction btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove"></span></a>
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($transactions, 'links') )
  <div>
    Showing {{ ($transactions->currentpage() - 1) * $transactions->perpage() + 1 }} to
    {{ (($transactions->currentpage() - 1) * $transactions->perpage()) + $transactions->count() }}
    of {{ $transactions->total() }} entries
  </div>
  <div class="filter">
    {{ $transactions->appends(request()->except(['page','_token']))->links() }}
  </div>
@endif
