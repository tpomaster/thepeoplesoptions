<form action="{{ route('tpo-bank-transaction.destroy', $transaction->id) }}" method="POST" class="delete-confirmation confirmation-form">
  {{-- {{ csrf_field() }} --}}
  {{-- {{ method_field('DELETE') }} --}}
  <div class="form-group">
    <strong>Transaction to delete</strong>

    <label for="pincode">Pin Code:</label>
    <input type="password" class="form-control" id="pincode" name="pincode">
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-block">Delete</button>
  </div>
</form>
