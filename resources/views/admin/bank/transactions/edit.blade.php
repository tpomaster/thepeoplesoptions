<form action="{{ route('tpo-bank-transaction.update', $transaction->id) }}" method="POST" class="transaction-form">
  {{ csrf_field() }}
  {{ method_field('PUT') }}

  <div class="form-group">
    <label class="control-label" for="description">Transaction Description </label>
    <textarea class="form-control" name="description">{{ $transaction->description }}</textarea>
  </div>

  <div class="form-group">
    <button class="btn theme-btn" name="submit" type="submit">Submit</button>
  </div>
</form>
