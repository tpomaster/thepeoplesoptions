<table class="table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>Description</th>
      <th>Amount</th>
      <th>Type</th>
      <th>Date</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($transactions as $transaction)
      <tr>
        <td>{{ $transaction->description }}</td>
        <td>$ {{ LpjHelpers::amt2($transaction->amount) }}</td>
        <td>{{ $transaction->type }}</td>
        <td>{{ $transaction->transaction_date }}</td>
        <td>{{ $transaction->status }}</td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($transactions, 'links') )
  <div>
    Showing {{ ($transactions->currentpage() - 1) * $transactions->perpage() + 1 }} to
    {{ (($transactions->currentpage() - 1) * $transactions->perpage()) + $transactions->count() }}
    of {{ $transactions->total() }} entries
  </div>

  <div class="no-filter">
    {{ $transactions->links() }}
  </div>
@endif


