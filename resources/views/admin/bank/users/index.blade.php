@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/bank.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>TPO Bank</h1>
    </div>

    <div class="bank-container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>TPO User Banks:</h2>
            </div>
            <div class="history-box">
              <div id="users-table-container" class="table-holder">
                <table id="users-table" class="table table-striped table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>Username</th>
                      <th>Name</th>
                      <th><a>Bank Total</a></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($users as $user)
                      <tr>
                        <td><a href="{{ route('tpo-bank-transaction.show', $user->id) }}" class="user-link" data-id="{{ $user->id }}">{{ $user->username }}</a></td>
                        <td>{{ $user->firstname .' '. $user->lastname }}</td>
                        <td>$ {{ LpjHelpers::amt2($user->available_bal) }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>

                <div>
                  @if ( method_exists($users, 'links') )
                    <div>
                      Showing {{($users->currentpage()-1)*$users->perpage()+1}} to
                      {{(($users->currentpage()-1)*$users->perpage())+$users->count()}} of
                      {{$users->total()}} entries
                    </div>
                    {{ $users->links('vendor.pagination.ajax-default') }}
                  @endif
                </div>

                {{-- <p>Total Count: {{ $users->count() }}</p> --}}
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Search Filter:</h2>
              <a class="btn theme-btn toggle-search"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
            <form action="{{ route('tpo-bank-transaction.filter') }}" method="POST" class="transactions-filter row toggle-hidden">
              {{ csrf_field() }}

              <div class="form-group col-md-4 col-xs-12">
                <label>Description</label>
                <input type="text" name="description" class="form-control">
              </div>

              <div class="form-group col-md-4 col-s-4 col-xs-12">
                <label>Type</label>
                <select name="type" class="form-control">
                  <option value="">All</option>
                  <option value="in">In</option>
                  <option value="out">Out</option>
                </select>
              </div>

              <div class="form-group col-md-4 col-s-4 col-xs-12">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="">All</option>
                  <option value="completed">Completed</option>
                  <option value="pending">Pending</option>
                  <option value="cancelled">Cancelled</option>
                </select>
              </div>

              <div class="form-group col-md-6 col-xs-12">
                <label>From</label>
                <div class='input-group date' id='from-date'>
                  <input type='text' class="form-control" name="from_date" readonly="readonly" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>

              <div class="form-group col-md-6 col-xs-12">
                <label>To</label>
                <div class='input-group date' id='to-date'>
                  <input type='text' class="form-control" name="to_date" readonly="readonly" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>

              <div class="form-group col-md-12">
                <div class="pull-right">
                  <button class="btn theme-btn" name="submit" type="submit">Filter</button>
                </div>
              </div>
            </form>

            <div class="x_title">
              <h2>TPO User Bank Transactions:</h2>
            </div>
            <div class="history-box">
              <div class="transactions-table table-holder">
                <table class="table table-striped table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>Description</th>
                      <th>Amount</th>
                      <th>Type</th>
                      <th>Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Click on a Username under TPO User Banks Section</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection

@section('pagespecificscripts')
  {{-- Bank Scripts --}}
  <script src="{{ LpjHelpers::asset('js/admin/bank.js') }}"></script>
@stop
