@if (isset($details))
  <table id="users-table" class="table table-striped table-hover">
    <thead class="thead-inverse">
      <tr>
        <th>Username</th>
        <th>Name</th>
        <th><a>Bank Total</a></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($details as $user)
        <tr>
          <td><a href="{{ route('tpo-bank-transaction.show', $user->id) }}" class="user-link" data-id="{{ $user->id }}">{{ $user->username }}</a></td>
          <td>{{ $user->firstname .' '. $user->lastname }}</td>
          <td>$ {{ LpjHelpers::amt2($user->available_bal) }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

  @if ($details){!! $details->render() !!} @endif

@elseif(isset($message))
  <p>{{ $message }}</p>
@endif
