@if (isset($users))
  <table id="users-table" class="table table-striped table-hover">
    <thead class="thead-inverse">
      <tr>
        <th>Username</th>
        <th>Name</th>
        <th><a>Bank Total</a></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
        <tr>
          <td><a href="{{ route('tpo-bank-transaction.show', $user->id) }}" class="user-link" data-id="{{ $user->id }}">{{ $user->username }}</a></td>
          <td>{{ $user->firstname .' '. $user->lastname }}</td>
          <td>$ {{ LpjHelpers::amt2($user->available_bal) }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

@if ( method_exists($users, 'links') )
  <div>
    Showing {{($users->currentpage()-1)*$users->perpage()+1}} to
    {{(($users->currentpage()-1)*$users->perpage())+$users->count()}} of
    {{$users->total()}} entries
  </div>
  {{ $users->links('vendor.pagination.ajax-default') }}
@endif

@elseif(isset($message))
  <p>{{ $message }}</p>
@endif
