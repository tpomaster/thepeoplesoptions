@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix" role="main">
        <div class="header-page">
            <h1>403</h1>

            <h2>Access denied</h2>

            <p>Full authentication is required to access this resource.</p>
        </div>
    </div>
    <!-- /page content -->
@endsection
