@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/payout.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Payout</h1>
    </div>

    <div class="payout-container">
      <div class="row">
        <div class="col-md-3 col-xs-12">
          <div class="tile-stats-box">
            <div class="icon-holder"><img src="{{LpjHelpers::asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
            <span class="count_top"><i class="fa fa-pie-chart"></i> Total Payout</span>
            <div class="count">$ 7,622.50</div>
          </div>
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Payout Totals</h2>
            </div>
            <div class="breakdown-box">
              <h3><i class="fa fa-check-circle"></i> <a href="{{ route('payout.completed') }}">$ 7,622.50</a></h3>
              <h4>Completed Total </h4>
            </div>
            <div class="breakdown-box">
              <h3><i class="fa fa-clock-o"></i> <a href="{{ route('payout.pending') }}">$ 0.00</a></h3>
              <h4>Pending Total </h4>
            </div>
            <div class="breakdown-box">
              <p><strong>NOTE:</strong> This section displays the total amount you withdraw from your TPO Bank to you Bitshares account. </p>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Payout History</h2>
            </div>
            <div class="history-box">
              <div class="transactions-table table-holder">
                <table class="table table-striped table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>User</th>
                      <th>BTS name</th>
                      <th>Mem. Level</th>
                      <th>Amt</th>
                      <th>Withdraw Fee</th>
                      <th>Net Amount</th>
                      <th>Date Req.</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>edthompson</td>
                      <td></td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>05-19-2017 22:57:11</td>
                      <td><a class="btn2 theme-btn">Approve</a></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td></td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>05-15-2017 07:23:28</td>
                      <td><a class="btn2 theme-btn">Approve</a></td>
                    </tr>
                  </tbody>
                </table>
                <p>Total Count: 2</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection