@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/payout.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Payout</h1>
    </div>

    <div class="payout-container">
      <div class="row">
        <div class="col-md-3 col-xs-12">
          <div class="tile-stats-box">
            <div class="icon-holder"><img src="{{LpjHelpers::asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
            <span class="count_top"><i class="fa fa-pie-chart"></i> Total Payout</span>
            <div class="count">$ 7,622.50</div>
          </div>
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Payout Totals</h2>
            </div>
            <div class="breakdown-box">
              <h3><i class="fa fa-check-circle"></i> <a href="{{ route('payout.completed') }}">$ 7,622.50</a></h3>
              <h4>Completed Total </h4>
            </div>
            <div class="breakdown-box">
              <h3><i class="fa fa-clock-o"></i> <a href="{{ route('payout.pending') }}">$ 0.00</a></h3>
              <h4>Pending Total </h4>
            </div>
            <div class="breakdown-box">
              <p><strong>NOTE:</strong> This section displays the total amount you withdraw from your TPO Bank to you Bitshares account. </p>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Payout History</h2>
            </div>
            <div class="history-box">
              <div class="transactions-table table-holder">
                <table class="table table-striped table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>User</th>
                      <th>BTS name</th>
                      <th>Mem. Level</th>
                      <th>Amt</th>
                      <th>Withdraw Fee</th>
                      <th>Net Amount</th>
                      <th>Date Req.</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>anthon</td>
                      <td>anthon-alindada</td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>09-08-2017 02:12:21</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>Jhen1</td>
                      <td>dream2</td>
                      <td>platinum</td>
                      <td>$5,000.00</td>
                      <td>$100.00</td>
                      <td>$4,900.00</td>
                      <td>08-27-2017 22:52:58</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>ally1</td>
                      <td>dream1</td>
                      <td>platinum</td>
                      <td>$100.00</td>
                      <td>$2.00</td>
                      <td>$98.00</td>
                      <td>08-24-2017 03:16:25</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>robinh</td>
                      <td>pfc-a</td>
                      <td>free</td>
                      <td>$30.00</td>
                      <td>$1.50</td>
                      <td>$28.50</td>
                      <td>06-01-2017 12:16:24</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>try88</td>
                      <td>pfc-a</td>
                      <td>free</td>
                      <td>$10.00</td>
                      <td>$0.50</td>
                      <td>$9.50</td>
                      <td>05-25-2017 10:08:42</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>try85</td>
                      <td>kingfishrocks-50</td>
                      <td>platinum</td>
                      <td>$50.00</td>
                      <td>$1.00</td>
                      <td>$49.00</td>
                      <td>05-24-2017 12:27:10</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>try85</td>
                      <td>kingfishrocks-50</td>
                      <td>platinum</td>
                      <td>$50.00</td>
                      <td>$1.00</td>
                      <td>$49.00</td>
                      <td>05-24-2017 10:15:52</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>try85</td>
                      <td>kingfisrocks-50</td>
                      <td>platinum</td>
                      <td>$100.00</td>
                      <td>$2.00</td>
                      <td>$98.00</td>
                      <td>05-23-2017 09:48:40</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>try85</td>
                      <td>kingfisrocks-50</td>
                      <td>platinum</td>
                      <td>$50.00</td>
                      <td>$1.00</td>
                      <td>$49.00</td>
                      <td>05-23-2017 09:37:28</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td>litopj2017</td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>05-23-2017 05:34:00</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td>litopj2017</td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>05-23-2017 05:33:53</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td>litopj2017</td>
                      <td>platinum</td>
                      <td>$10.00</td>
                      <td>$0.20</td>
                      <td>$9.80</td>
                      <td>05-22-2017 10:15:45</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td>litopj2017</td>
                      <td>platinum</td>
                      <td>$123.00</td>
                      <td>$2.46</td>
                      <td>$120.54</td>
                      <td>05-22-2017 09:13:29</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>litopj</td>
                      <td>litopj2017</td>
                      <td>platinum</td>
                      <td>$12.00</td>
                      <td>$0.24</td>
                      <td>$11.76</td>
                      <td>05-22-2017 09:06:48</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                    <tr>
                      <td>tinat</td>
                      <td></td>
                      <td>platinum</td>
                      <td>$100.00</td>
                      <td>$2.00</td>
                      <td>$98.00</td>
                      <td>05-22-2017 05:34:36</td>
                      <td><span class="complete">completed</span></td>
                    </tr>
                  </tbody>
                </table>
                <p>Total Count: 15</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection