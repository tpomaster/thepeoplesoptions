@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">
      <div class="header-page">
        <h1>Add New Permission</h1>
      </div>
      <div class="x_panel">
        <div class="x_content">
          <br />
          <form action="{{ route('permissions.store') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            {{ csrf_field() }}

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="name" name="name" required="required" value="{{ old('name') }}" class="form-control col-md-7 col-xs-12">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="slug">Slug <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="slug" name="slug" required="required" value="{{ old('slug') }}" class="form-control col-md-7 col-xs-12">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category">Category <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select id="category" name="category" class="form-control col-md-7 col-xs-12">
                    <option value="">Select Category</option>

                    @php
                        $categories = ['users', 'roles', 'permissions', 'packages'];
                    @endphp

                    @foreach ($categories as $category)
                        <option value="{{ $category }}" {{ (old('category') == $category ? 'selected' : '') }}>{{ $category }}</option>
                    @endforeach
                </select>
              </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="{{ route('permissions.index') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
            </div>

            @if (count($errors))
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
          </form>
        </div>
      </div>
    </div>
    <!-- /page content -->
@endsection


