@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">

        <div class="header-page">
            <h1>Admin Permissions</h1>
        </div>

        <div class="x_panel tile">
            <div class="x_title">
                <a href="{{ route('permissions.create') }}" class="btn btn-md theme-btn">Add New</a>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table class="datatable table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($permissions as $permission)
                            <tr>
                                <td>{{ $permission->id }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->slug }}</td>
                                <td>{{ $permission->category }}</td>
                                <td>
                                  <a href="{{ route('permissions.edit', $permission->id) }}" class="btn btn-xs theme-btn">Edit</a>

                                  <form action="{{ route('permissions.destroy', $permission->id) }}" method="POST">
                                      {{ csrf_field() }}
                                      {{ method_field('DELETE') }}

                                      <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                  </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
