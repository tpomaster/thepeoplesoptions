<div class="tile-stats-box">
  <div class="icon-holder"><img src="{{LpjHelpers::asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
  <span class="count_top"><i class="fa fa-pie-chart"></i> Total Payout</span>
  <div class="count">$ {{ LpjHelpers::amt2($total_payouts['all']) }}</div>
</div>
<div id="payouts-summary" class="x_panel tile">
  <div class="x_title">
    <h2>Payout Totals</h2>
  </div>
  <div class="breakdown-box">
    <h3><i class="fa fa-check-circle"></i> <a href="{{ route('payouts.filter') }}" class="filter-link" data-status="completed">$ {{ LpjHelpers::amt2($total_payouts['completed']) }}</a></h3>
    <h4>Completed Total </h4>
  </div>
  <div class="breakdown-box">
    <h3><i class="fa fa-clock-o"></i> <a href="{{ route('payouts.filter') }}" class="filter-link" data-status="pending">$ {{ LpjHelpers::amt2($total_payouts['pending']) }}</a></h3>
    <h4>Pending Total </h4>
  </div>
  <div class="breakdown-box">
    <p><strong>NOTE:</strong> This section displays the total amount you withdraw from your TPO Bank to you Bitshares account. </p>
  </div>
</div>
