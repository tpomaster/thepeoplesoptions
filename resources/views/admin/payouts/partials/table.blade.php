<table class="table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>User</th>
      <th>BTS name</th>
      <th>Mem. Level</th>
      <th>Amt</th>
      <th>Withdraw Fee</th>
      <th>Net Amount</th>
      <th>Date Req.</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($payouts as $payout)
      @php
        $details = json_decode($payout->details);
      @endphp
      <tr>
        <td>
          {{ $payout->member->username }}
        </td>
        <td>
          {{ $details->bts_wallet }}
        </td>
        <td>
          {{ $payout->member->membership }}
        </td>
        <td>
          {{ LpjHelpers::amt2($payout->amount) }}
        </td>
        <td>
          {{ LpjHelpers::amt2($payout->withdrawal_fee) }}
        </td>
        <td>
          {{ LpjHelpers::amt2($payout->net_amount) }}
        </td>
        <td>
          {{ $payout->request_date }}
        </td>
        <td>
          @if ($payout->status == 'pending')
            <a href="{{ route('payouts.edit', $payout->id) }}" title="Approve Payout" class="approve-payout btn2 theme-btn">APPROVE</a>
            <a href="{{ route('payouts.confirmation', $payout->id) }}" title="Cancel Payout" class="cancel-payout btn2 theme-btn">CANCEL</a>
          @elseif ($payout->status == 'cancelled')
            <span class="cancelled" style="background: #999;">{{ $payout->status }}</span>
          @else
            <span class="complete">{{ $payout->status }}</span>
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($payouts, 'links') )
  <div>
    Showing {{($payouts->currentpage()-1)*$payouts->perpage()+1}} to
    {{(($payouts->currentpage()-1)*$payouts->perpage())+$payouts->count()}} of
    {{$payouts->total()}} entries
  </div>
  {{ $payouts->appends(request()->except(['page','_token']))->links('vendor.pagination.ajax-default') }}
@endif

