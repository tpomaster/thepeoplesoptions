<div class="x_panel tile">
  <div class="x_title">
    <h2>Payout History</h2>

    @if ($bitpal_info['access_token_time'] && $bitpal_info['access_token_lifetime'] < 60)
      <a href="{{ $bitpal_info['auth_url'] }}" class="btn theme-btn" target="_blank">Login to another BitPalBTS account</a>
    @else
      <a href="{{ $bitpal_info['auth_url'] }}" class="btn theme-btn" target="_blank">BitPalBTS access token expired</a>
    @endif


    <div class="clearfix"></div>
  </div>
  <div id="payouts-table-container" class="table-holder history-box"></div>
</div>
