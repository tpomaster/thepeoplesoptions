<form action="{{ route('payouts.approve', $payout->id) }}" method="GET" class="approval-form">
  {{ csrf_field() }}

  <div class="form-group">
    <label for="bts_account">Select Wallet</label>
    <select class="form-control" id="bts_account" name="bts_account">
      @foreach ($bts_accounts as $account)
        @php
          $selected = Auth::user()->bitpal_default_bts_account == $account ? 'selected' : '';
        @endphp

        <option value="{{ $account }}" {{ $selected }}>{{ $account }}</option>
      @endforeach
    </select>
    <span class="bts_account-error help-block error"></span>
  </div>

  <div class="form-group">
    @php
      $details = json_decode($payout->details);
    @endphp

    <label for="sent_to_bts">Send To</label>
    <input type="text" class="form-control" value="{{ $details->bts_wallet }}" disabled>
  </div>

  <div class="form-group">
    <label for="comment">Comment</label>
    <textarea class="form-control" rows="5" id="comment" name="comment">TPO Payout request. TPO Username: {{ $payout->member->username }}. Date Requested: {{ date('M d, Y', strtotime($payout->request_date)) }}. Date Approved: {{ date('M d, Y') }}.</textarea>
    <span class="comment-error help-block error"></span>
  </div>

  <div class="form-group">
    <label for="pincode">Pin Code:</label>
    <input type="password" class="form-control" id="pincode" name="pincode">
    <small>Please type in your user pin code to confirm.</small>

    <span class="help-block error pincode-error"></span>
  </div>

  <div class="form-group">
    <input type="hidden" class="form-control" name="action" value="approve">
    <button type="submit" class="btn btn-primary">Approve</button>
  </div>
</form>
