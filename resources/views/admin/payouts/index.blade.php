@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/payout.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Payout</h1>
    </div>

    <div id="payouts-container" class="payout-container">
      <div class="row">
        <div class="col-md-3 col-xs-12">
          @include ('admin.payouts.partials.summary')
        </div>
        <div class="col-md-9 col-xs-12">
          @include ('admin.payouts.partials.history-table')
        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->

  @include ('admin.components.modal')

@endsection

@section('pagespecificscripts')
  <script src="{{ LpjHelpers::asset('js/admin/payouts.js') }}"></script>
@stop
