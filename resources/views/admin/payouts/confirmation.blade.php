<form action="{{ $route }}" method="POST" class="modal-form confirmation-form">
  {{ csrf_field() }}

  @if ($action == 'delete')
    {{ method_field('DELETE') }}
  @elseif ($action == 'update' || $action == 'approve')
    {{ method_field('PATCH') }}
  @endif

  <div class="form-group">
    <p>Are you sure you want to {{ $action }} this payout? This action can’t be undone and will be permanently gone.</p>

    <p>Please type in your user pin code to confirm.</p>

    <label for="pincode">Pin Code:</label>
    <input type="password" class="form-control" id="pincode" name="pincode">

    <span class="help-block error pincode-error"></span>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-block">I understand the consequences, {{ $action }} this payout</button>
  </div>
</form>


