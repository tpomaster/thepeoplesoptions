@php
    extract($settings);
@endphp

@foreach ($referrals as $referral)
    <div class="clearfix {{ $tier_lvl_class }} tierWrap tierWrap-{{ $referral->username }}">
        <a  href="{{ $current_tier_lvl == 6 ? route('affiliates.index', ['tree', $referral->username]) : '#' }}"
            {{ $current_tier_lvl == 6 ? 'target="_blank"' : '' }}
            class="tableBar click {{ $btn_class }} tableCol"
            data-parent-username="{{ $referral->username }}"
            data-current-tier-lvl="{{ $next_lvl }}"
        >

            @if ($referral->direct_ref_cnt >= 1)
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
            @endif

            <span>Tier {{ $current_tier_lvl }}</span>
        </a>
        <div class="tableUserName tableCol">{{ $referral->username }}</div>
        <div class="tableFirstName tableCol">{{ $referral->firstname }}</div>
        <div class="tableLastName tableCol">{{ $referral->lastname }}</div>
        <div class="tableDateReg tableCol">{{ $referral->created_at }}</div>
        <div class="tableMembership tableCol">{{ $referral->membership }}</div>
        <div class="clear"></div>
        <div class="childTierWrap"></div>
    </div>
@endforeach

@if ( method_exists($referrals, 'links') )
  @if ( $referrals->total() > $referrals->perpage() )
      <div class="tierPaginationWrap">
          <span class="paginationInfo">
              Showing {{($referrals->currentpage()-1)*$referrals->perpage()+1}} to {{(($referrals->currentpage()-1)*$referrals->perpage())+$referrals->count()}} of {{$referrals->total()}}
          </span>

          {{ $referrals->links() }}
      </div>
  @endif
@endif


