@extends('admin.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/tree-view.css') }}">
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">

        <div class="header-page">
            <h1>Affiliates Tree View</h1>
        </div>

        <div id="treeView-content" class="affiliates-tree x_panel tile">

            <form action="#" class="filter-form">
              <div class="form-group">
                <label for="username">Username:</label>
                <input type="username" class="form-control" id="username" required>
              </div>
              <button type="submit" class="btn btn-default">Search</button>
            </form>

            <div class="headerRow clearfix">
                <div class="blankLabel"></div>
                <div class="userNameLabel">Username</div>
                <div class="firstNameLabel">First Name</div>
                <div class="lastNameLabel">Last Name</div>
                <div class="dateRegLabel">Date Registered</div>
                <div class="membershipLabel">Membership</div>
            </div>

            <div class="parentRow tierWrap tierWrap-{{ $settings['parent_username'] }}" data-parent-username="{{ $settings['parent_username'] }}">
                <div class="childTierWrap"></div>
            </div>
        </div>

    </div>
    <!-- /page content -->
@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/admin/affiliates.js') }}"></script>
@stop
