@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">

        <div class="header-page">
            <h1>Affiliates</h1>
        </div>

        <div class="x_panel">
            <div class="x_title">
                <h2>Affiliates List View</h2>
                <div class="clearfix"></div>
            </div>
            <div id="affiliates" class="x_content">
              <affiliates-data-table endpoint="{{ route('datatable.affiliates.index') }}"></affiliates-data-table>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/admin/affiliates-list.js') }}"></script>
@stop
