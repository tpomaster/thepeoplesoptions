<form action="{{ route('affiliates.update', $user->id) }}" method="POST" data-parsley-validate class="modal-form">
  <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">

      <div class="form-group">
        <label for="sponsor">Sponsor</label>
        <input type="text" class="form-control" id="sponsor" name="sponsor" value="{{ $sponsor->username . ' | ' . $sponsor->email }}" readonly>
      </div>

      <div class="form-group">
        <label for="affilliate_url">Affiliate URL</label>
        <input type="text" class="form-control" id="affilliate-url" name="affilliate_url" value="" readonly>
      </div>

      <div class="form-group">
        <label for="username">Username <span class="required">*</span></label>
        <input type="text" id="username" name="username" required="required" value="{{ old('username', $user->username) }}" class="form-control">
        <small class="note">Allowed characters: a to z, A to Z (letters), 0 to 9 (numbers), - (dash), _ (underscore), .(dot/period) and @.</small>
      </div>

      <div class="form-group">
        <label for="email">Email *</label>
        <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $user->email) }}">
      </div>

      <div class="form-group">
        <label for="phone">Cellphone Number</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone', $user->phone) }}">
      </div>

      <div class="form-group">
        <label for="firstname">First Name *</label>
        <input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname', $user->firstname) }}">
      </div>

      <div class="form-group">
        <label for="lastname">Last Name</label>
        <input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname', $user->lastname) }}">
      </div>

    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="form-group">
        <label for="nickname">Nick Name</label>
        <input type="text" class="form-control" id="nickname" name="nickname" value="{{ old('nickname') }}">
      </div>

      <div class="form-group">
        <label for="display-name">Display Name</label>
        <input type="text" class="form-control" id="display-name" name="display_name" value="{{ old('display_name') }}">
      </div>

      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" rows="5" id="description" name="description"></textarea>
      </div>

      <div class="form-group">
        <label for="password">New Password</label>
        <input type="text" class="form-control" id="password" name="password">
        <small class="note">Leave this empty if you don't want to change your password.</small>
      </div>

      <div class="form-group">
        <label for="confirm-password">Confirm New Password</label>
        <input type="text" class="form-control" id="confirm-password" name="confirm_password">
        <small class="note">Leave this empty if you don't want to change your password.</small>
      </div>

    </div>
  </div>

  <div class="ln_solid"></div>

  <div class="form-group">
    <div class="text-center">
      <button type="submit" class="btn theme-btn">Update</button>
      <button type="submit" class="btn theme-btn"><i class="fa fa-key" aria-hidden="true"></i></button>
    </div>
  </div>

</form>
