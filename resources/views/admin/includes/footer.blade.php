<!-- footer content -->
<footer>
	<div class="footer-overide">
		<div class="pull-left footer-time">
      <?php /*/ ?>
      New York Time: <?php echo date( 'M. d Y H:i', current_time( 'timestamp', 0 ) ); ?>
      <?php /*/ ?>
      <a href="https://time.is/New_York" id="time_is_link" rel="nofollow">Time in New York:</a>
      <span id="New_York_z161"></span>
      <script src="//widget.time.is/en.js"></script>
      <script>
        time_is_widget.init({New_York_z161:{template:"DATE TIME", time_format:"12hours:minutes:seconds AMPM", date_format:"dayname, monthname dnum, year"}});
      </script>

      <span>Server Time: <?php echo date( 'M. d Y H:i'); ?></span>

		</div>
		<div class="pull-right footer-copyright">
		 	Copyright &copy; <?php echo date('Y'); ?> The Peoples Options . All rights reserved.
		</div>
		<div class="clearfix"></div>
	</div>
</footer>
<!-- /footer content -->
