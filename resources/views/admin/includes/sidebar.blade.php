<div class="col-md-3 left_col theme-overide">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="index.html" class="site_title"><img src="{{LpjHelpers::asset('images/small-logo.png')}}" alt="" class="retain-logo"> <img src="{{LpjHelpers::asset('images/text-logo.png')}}" alt="" class="collapse-logo"> <span> ADMIN</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="{{LpjHelpers::asset('images/avatar.png')}}" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <h2>{{ Auth::user()->firstname . ' ' .  Auth::user()->lastname}}</h2>
        <span class="green">{{ strtoupper(Auth::user()->roles->first()->name) }}</span>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>MENU</h3>
        <ul class="nav side-menu">
          <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> Home </a></li>
          <li><a><i class="fa fa-group"></i> Admin Users <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li><a href="{{ route('admin.index') }}"> View List </a></li>
                {{-- <li><a href="{{ route('roles.index') }}"> Roles </a></li> --}}
                {{-- <li><a href="{{ route('permissions.index') }}"> Permissions </a></li> --}}
              </ul>
          </li>
          <li><a><i class="fa fa-group"></i> Affiliates <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('affiliates.index', 'tree') }}"> Tree View </a></li>
              <li><a href="{{ route('affiliates.index', 'list') }}"> List View </a></li>
            </ul>
          </li>
          <li><a href="{{ route('tpo-bank.index') }}"><i class="fa fa-bank"></i> TPO Bank </a></li>
          <li><a href="{{ route('user-bank.index') }}"><i class="fa fa-bank"></i> Users Bank </a></li>
          <li><a href="{{ route('referral-credits.index') }}"><i class="fa fa-usd"></i> Referral Credits </a></li>
          <li><a href="{{ route('payouts.index') }}"><i class="fa fa-credit-card"></i> Payout </a></li>
          @can('tpo-marketing', Auth::user())
            <li><a href="{{ route('tpomarketing.login') }}" class="tpo-marketing-login"><i class="fa fa-suitcase"></i> TPOMarketing </a></li>
          @endcan
        </ul>
      </div>
      <div class="menu_section">
        <h3>PACKAGES</h3>
        <ul class="nav side-menu">
          <li><a><i class="fa fa-ticket"></i> CryptoShuffle Ticket <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('crypto-shuffle-packages.index') }}"> Packages </a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-address-card"></i> Membership <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              {{-- <li><a> Create </a></li> --}}
              <li><a href="{{ route('membership-packages.index') }}"> Packages </a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-diamond"></i> Crypto Mining <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('crypto-mining-packages.index') }}"> Packages </a></li>
              <li><a href="{{ route('crypto-mining-profits-queue.index') }}"> Profit Queue </a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-line-chart"></i> Crypto Trading <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('crypto-trading-packages.index') }}"> Packages </a></li>
              <li><a href="{{ route('crypto-trading-trades-queue.index') }}"> Trade Queue </a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-exchange"></i> Crypto Exchange <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('crypto-exchange-packages.index') }}"> Packages </a></li>
              <li><a href="{{ route('crypto-exchange-profits-queue.index') }}"> Profit Queue </a></li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="menu_section bottom-side-menu">
        <ul class="nav side-menu">
          <li><a href="{{ route('announcements.index') }}"><i class="fa fa-bullhorn"></i> Announcement </a></li>
          <li><a href="{{ route('news.index') }}"><i class="fa fa-newspaper-o"></i> News </a></li>
          <li><a href="{{ route('faq.index') }}"><i class="fa fa-question"></i> FAQ </a></li>
          <li><a href="{{ route('settings.index') }}"><i class="fa fa-gears"></i> Settings </a></li>
          <li><a href="{{ url('/admin/logout') }}"><i class="fa fa-sign-out"></i> Logout </a></li>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
