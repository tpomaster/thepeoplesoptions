<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="name" name="name" required="required" value="{{ old('name', $role->name) }}" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Slug <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="slug" name="slug" required="required" value="{{ old('slug', $role->slug) }}" class="form-control col-md-7 col-xs-12">
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Assign Permissions <span class="required">*</span></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="panel-group" id="permissions-accordion">
        @foreach ($permissions as $category => $values)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#permissions-accordion" href="#{{ $category }}">{{ ucfirst($category) }}</a>
                    </h4>
                </div>
                <div id="{{ $category }}" class="panel-collapse collapse">
                    <div class="panel-body">
                        @foreach ($values as $permission)
                            <label class="checkbox-inline">
                                <input type="checkbox" name="permissions[]" value="{{ $permission['id'] }}"
                                @if(is_array(old('permissions')) && in_array($permission['id'], old('permissions')) || in_array($permission['id'], $selected_permissions)) checked @endif
                                >{{ $permission['name'] }}
                            </label>
                        @endforeach
                    </div>
                </div>
            </div>

        @endforeach
    </div>
  </div>
</div>
