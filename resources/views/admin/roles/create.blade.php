@extends('admin.layouts.blank')

@section('main_container')

    <!-- page content -->
    <div class="right_col clearfix content-overide" role="main">
      <div class="header-page">
        <h1>Add New Admin Role</h1>
      </div>
      <div class="x_panel">
        <div class="x_content">
          <br />
          <form action="{{ route('roles.store') }}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            {{ csrf_field() }}

            @include ('admin.roles.partials.form-fields')

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="{{ route('roles.index') }}" class="btn btn-danger">Cancel</a>
                <button type="submit" class="btn theme-btn">Submit</button>
              </div>
            </div>

            @if (count($errors))
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
          </form>
        </div>
      </div>
    </div>
    <!-- /page content -->
@endsection


