<table class="table table-bordered">
  <thead>
    <tr>
      <th>Username</th>
      <th>Amount</th>
      <th>Tier Level</th>
      {{-- <th>Transaction Code</th> --}}
    </tr>
  </thead>
  <tbody>
    @foreach ( $referral_credits as $credit )
      <tr>
        <td>{{ $credit->username}}</td>
        <td>$ {{ LpjHelpers::amt2($credit->amount) }}</td>
        <td>{{ $credit->tier_level }}</td>
        {{-- <td>{{ $credit->transaction_code}}</td> --}}
      </tr>
    @endforeach
  </tbody>
</table>
