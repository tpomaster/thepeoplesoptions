@extends('admin.layouts.blank')

@section('pagespecificstyles')

  <!-- my account css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/admin/credits.css') }}">

@stop

@section('main_container')

  <!-- page content -->
  <div class="right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>Referral Credits</h1>
    </div>

    <div class="credits-container row">
      <div class="col-md-12 col-s-12 col-xs-12">
        <div class="x_panel tile">
          <div class="x_title">
            <h2>TPO Bank (In/Out) Transactions History:</h2>
          </div>

          <div id="referrals-table-container" class="table-holder">
            @include ('admin.referral-credits.partials.table')
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- /page content -->
@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/admin/referral-credits.js') }}"></script>
@stop
