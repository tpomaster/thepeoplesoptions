<table class="table table-striped table-hover">
  <thead class="thead-inverse">
    <tr>
      <th>Package</th>
      <th>Package Category</th>
      <th>Affiliate</th>
      <th>Investor</th>
      <th>Tier</th>
      <th>Earned</th>
      <th>Date</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($referrals as $referral)
      <tr>
        <td>{{ $referral->package_name }}</td>
        <td>{{ $referral->package_cat }}</td>
        <td>{{ $referral->username }}</td>
        <td>{{ $referral->investor_name }}</td>
        <td>{{ $referral->tier_level }}</td>
        <td>{{ LpjHelpers::amt2($referral->amount) }}</td>
        <td>{{ date('M. d, Y', strtotime($referral->date)) }}</td>
        <td><span class="{{ $referral->status }}">{{ ucfirst($referral->status) }}</span></td>
      </tr>
    @endforeach
  </tbody>
</table>

@if ( method_exists($referral_credits, 'links') )
  <div>
    Showing {{($referral_credits->currentpage()-1)*$referral_credits->perpage()+1}} to
    {{(($referral_credits->currentpage()-1)*$referral_credits->perpage())+$referral_credits->count()}} of
    {{$referral_credits->total()}} entries
  </div>
  <div>
    {{ $referral_credits->links() }}
  </div>
@endif
