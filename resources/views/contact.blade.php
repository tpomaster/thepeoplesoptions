@extends('layout.layout', ['body_class' => 'inner-page contact-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>Contact Us</h1>
		</div>
	</div>
	<div id="inner-content">
		<div class="container">
			<p>You can contact our support team here. We typically reply to support tickets within 24-48 hours but we do our best to provide support to our members as quickly as possible.</p>
			<div class="contact-form">
				@if(Session::has('success'))
				   <div class="alert alert-success">
				     {{ Session::get('success') }}
				   </div>
				@endif

				<form action="{{ route('contactus.store') }}" method="POST">
					{{ csrf_field() }} 
					<div class="clearfix">
						<div class="inr-left {{ $errors->has('name') ? 'has-error' : '' }}">
							<input type="text" name="name" placeholder="Name">
							<span class="text-danger">{{ $errors->first('name') }}</span>
						</div>
						<div class="inr-right {{ $errors->has('email') ? 'has-error' : '' }}">
							<input type="email" name="email" placeholder="Email">
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>
					</div>
					<div class="clearfix">
						<div class="inr-left {{ $errors->has('address') ? 'has-error' : '' }}">
							<input type="text" name="address" placeholder="Address">
							<span class="text-danger">{{ $errors->first('address') }}</span>
						</div>
						<div class="inr-right {{ $errors->has('phone') ? 'has-error' : '' }}">
							<input type="text" name="phone" placeholder="Phone">
							<span class="text-danger">{{ $errors->first('phone') }}</span>
						</div>
					</div>
					
					<div class="clearfix {{ $errors->has('message') ? 'has-error' : '' }}">
						<textarea name="message" placeholder="Message"></textarea>
						<span class="text-danger">{{ $errors->first('message') }}</span>
					</div>
					<input class="button" type="submit" value="SUBMIT">
				</form>
			</div>
		</div>
	</div>
@endsection