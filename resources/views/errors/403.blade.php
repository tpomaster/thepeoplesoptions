<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Custom Theme Style -->
    <link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">
</head>

<body class="nav-md">
    <div id="app" class="container body">
        <div class="main_container">
            <!-- page content -->
            <div class="col-md-12">
                <div class="col-middle">
                    <div class="text-center text-center">
                        <h1 class="error-number">403</h1>

                        <h2>Access denied</h2>
                        
                        <p>Full authentication is required to access this resource. <a href="#">Report this?</a></p>

                        <a class="btn btn-primary btn-lg" href="/">Go Back to Home Page</a>
                    </div>
                </div>
            </div>
            <!-- /page content -->
        </div>
    </div>

</body>
</html>