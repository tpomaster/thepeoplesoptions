<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>The Peoples Options</title>

        <script type='text/javascript' src="{{ LpjHelpers::asset('js/jquery.js') }}"></script>
        <script type='text/javascript' src="{{ LpjHelpers::asset('js/jquery-migrate.js') }}"></script>

        <!-- responsive menu -->
        <link href="{{ LpjHelpers::asset('css/front-end/jquery.sidr.dark.min.css') }}" rel="stylesheet">
        <!-- fancybox -->
        <link href="{{ LpjHelpers::asset('css/front-end/jquery.fancybox.min.css') }}" rel="stylesheet">
        <!-- page specific styles -->
        @yield('pagespecificstyles')
        <!-- Styles -->
        <link href="{{ LpjHelpers::asset('css/front-end/style.css') }}" rel="stylesheet">

        <!-- <script type="text/javascript" src="{{ LpjHelpers::asset('js/jquery.js') }}" async></script>
        <script type="text/javascript" src="{{ LpjHelpers::asset('js/jquery-migrate.js') }}" async></script> -->
        

    </head>
    <body @unless(empty($body_class))
            class="{{$body_class}}"
          @endunless
        >
        <div class="loader">
            <i class="fa fa-cog fa-spin"></i>
        </div>

        <header>
            <div class="container clearfix">
                <a href="{{ route('home.index') }}"><img src="{{ LpjHelpers::asset('images/logo.png') }}" class="logo" alt="image of the peoples options header logo"/></a>
                <div class="top-right">
                    <ul class="top-nav testtest">
                        <li {{{ (Request::is('home.index') ? 'class=active' : '') }}}><a href="{{ route('home.index') }}">HOME</a></li>
                        <li {{{ (Request::is('about-tpo') ? 'class=active' : '') }}}><a href="{{ route('about-tpo') }}">ABOUT TPO</a></li>
                        <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}">ABOUT CRYPTOCURRENCY</a></li>
                        <li {{{ (Request::is('faq') ? 'class=active' : '') }}}><a href="{{ route('faq') }}">FAQ</a></li>
                        <li {{{ (Request::is('contact') ? 'class=active' : '') }}}><a href="{{ route('contact') }}">CONTACT US</a></li>
                        <li><a href="{{ route('member.login') }}"><i class="fa fa-home"></i> MY ACCOUNT</a></li>
                        <!-- <li><a href="{{ route('member.login') }}"><i class="fa fa-lock" aria-hidden="true"></i> LOGIN</a></li> -->
                    </ul>
                    <h2>Buy, Sell, &amp; Trade Bitcoin</h2>
                    <ul class="service-nav">
                        <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptomining">CRYPTO MINING</a></li>
                        <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptotrading">CRYPTO TRADING </a></li>
                        <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptoshuffleticket">THE CRYPTOSHUFFLE TICKET</a></li>
                        <li {{{ (Request::is('membership-packages') ? 'class=active' : '') }}}><a href="{{ route('membership-packages') }}">MEMBERSHIP PACKAGES</a></li>
                    </ul>
                </div>
            </div>
            <div id="responsive-menu">
                <a id="left-menu-toggle-btn" href="{{ route('home.index') }}">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                    <strong style="display: none;">MENU</strong>
                </a>

                <div id="navigation">
                    <nav class="nav">
                        <ul>
                            <li {{{ (Request::is('home.index') ? 'class=active' : '') }}}><a href="{{ route('home.index') }}">HOME</a></li>
                            <li {{{ (Request::is('about-tpo') ? 'class=active' : '') }}}><a href="{{ route('about-tpo') }}">ABOUT TPO</a></li>
                            <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}">ABOUT CRYPTOCURRENCY</a></li>
                            <li {{{ (Request::is('faq') ? 'class=active' : '') }}}><a href="{{ route('faq') }}">FAQ</a></li>
                            <li {{{ (Request::is('contact') ? 'class=active' : '') }}}><a href="{{ route('contact') }}">CONTACT US</a></li>
                            <li><a href="{{ route('member.login') }}">MY ACCOUNT</a></li>
                            <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptomining">CRYPTO MINING</a></li>
                            <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptotrading">CRYPTO TRADING</a></li>
                            <li {{{ (Request::is('about-cryptocurrency') ? 'class=active' : '') }}}><a href="{{ route('about-cryptocurrency') }}#cryptoshuffleticket">CRYPTOSHUFFLE TICKET</a></li>
                            <li {{{ (Request::is('membership-packages') ? 'class=active' : '') }}}><a href="{{ route('membership-packages') }}">MEMBERSHIP PACKAGES</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

        @yield('main_content')

        <div id="footer-top">
            <div class="container clearfix">
                <div class="tftr-box">
                    <a href="{{ route('home.index') }}"><img src="{{ LpjHelpers::asset('images/ftr-logo.png') }}" class="ftr-logo" alt="image of the peoples options bottom logo image"/></a>
                    <h2>Buy, Sell, &amp; Trade Bitcoin</h2>
                </div>
                <div class="tftr-box">
                    <h3>SITEMAP</h3>
                    <ul>
                        <li><a href="{{ route('home.index') }}">HOME</a></li>
                        <li><a href="{{ route('about-tpo') }}">ABOUT TPO</a></li>
                        <li><a href="{{ route('about-cryptocurrency') }}">ABOUT CRYPTOCURRENCY</a></li>
                        <li><a href="{{ route('faq') }}">FAQ</a></li>
                        <li><a href="{{ route('membership-packages') }}">MEMBERSHIP PACKAGES</a></li>
                        <li><a href="{{ route('member.dashboard') }}">DASHBOARD</a></li>
                    </ul>
                </div>
                <div class="tftr-box">
                    <h3>SUPPORT</h3>
                    <ul>
                        <li><a href="#">CONTACT SUPPORT</a></li>
                        <li><a href="#">SUPPORT RESOURCES</a></li>
                        <li><a href="#">SWITCH TO MOBILE SITE</a></li>
                        <li><a href="#">CONTACT US</a></li>
                        <li><a href="#">FEES</a></li>
                        <li><a href="#">PRIVACY POLICY</a></li>
                        <li><a href="#">TERMS OF USE</a></li>
                    </ul>
                </div>
                <div class="tftr-box">
                    <h3>FOLLOW US</h3>
                    <ul>
                        <li><a href="#"><i class="fa fa-twitter"></i> TWITTER</a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i> FACEBOOK</a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i> INSTAGRAM</a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i> GOOGLE+</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <footer>
            <div class="container clearfix">
                <div class="footer-left">
                    <p>
                        Server time: <strong><?php echo date('Y-m-d H:i:s'); ?></strong><br>
                        Users currently online: <strong><?php echo rand(1999,99999); ?></strong>
                    </p>
                </div>
                <div class="footer-right">
                    <p>COPYRIGHT &copy; <?php echo date('Y') ?> THE PEOPLE’S OPTIONS. ALL RIGHTS RESERVED.</p>
                </div>
            </div>
        </footer>

        
        <!-- <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
        <script type="text/javascript" src="{{ LpjHelpers::asset('js/jquery.sidr.min.js') }}"></script>
        <script type="text/javascript" src="{{ LpjHelpers::asset('js/jquery.fancybox.min.js') }}"></script>
        <!-- Scripts -->
        <script type="text/javascript"  src="{{ LpjHelpers::asset('js/main.js') }}"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // toggle menu
                jQuery('#left-menu-toggle-btn').sidr({
                  name: 'sidr-main',
                  source: '#navigation'
                });
            });
        </script>
        @yield('pagespecificscripts')
    </body>
</html>