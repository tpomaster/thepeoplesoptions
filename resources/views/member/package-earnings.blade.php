@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/referral.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Package Earnings<span></span></h1>
    </div>

    <div class="row">
        <div class="referral-container">
            <div class="col-md-3 col-xs-12">
                <div class="tile-stats-box">
                    <div class="icon-holder"><img src="{{asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
                    <span class="count_top"><i class="fa fa-line-chart"></i> Total Package Earnings</span>
                    <div class="count green pkg_earning_overall_total">$<?php echo LpjHelpers::amt2($TotalPkgEarning); ?></div>
                </div>
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>Package Earnings Total By Category</h2>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-address-card"></i> 
                            <a href="" class="pkgearning_filter_btn membership_pkgearning" data-cat="membership">
                                $<?php echo LpjHelpers::amt2($MSpkgEarning); ?>
                            </a>
                        </h3>
                        <h4>Membership Power Bonus </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-line-chart"></i> 
                            <a href="" class="pkgearning_filter_btn cryptotrading_pkgearning" data-cat="cryptotrading">
                                $<?php echo LpjHelpers::amt2($CTpkgEarning); ?>
                            </a>
                        </h3>
                        <h4>Crypto Trading Package Earning </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-diamond"></i> 
                            <a href="" class="pkgearning_filter_btn cryptomining_pkgearning" data-cat="cryptomining">
                                $<?php echo LpjHelpers::amt2($CMpkgEarning); ?>        
                            </a>
                        </h3>
                        <h4>Crypto Mining Package Earning </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-exchange"></i> 
                            <a href="" class="pkgearning_filter_btn cryptoexchange_pkgearning" data-cat="cryptoexchange">
                                $<?php echo LpjHelpers::amt2($CEpkgEarning); ?>
                            </a>
                        </h3>
                        <h4>Crypto Exchange Package Earning </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-exchange"></i> 
                            <a href="" class="pkgearning_filter_btn cryptoshuffle_pkgearning" data-cat="cryptoshuffle">
                                $<?php echo LpjHelpers::amt2($CSpkgEarning); ?>
                            </a>
                        </h3>
                        <h4>Crypto Shuffle Package Earning </h4>
                    </div>

                    <div class="breakdown-box">
                        <h5 class="red"></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="x_panel tile">
                    <div style="display: none;">
                        <div class="x_title">
                            <h2>Search Filter:</h2>
                            <a class="btn theme-btn toggle-search"><i class="fa fa-search" aria-hidden="true"></i></a>
                        </div>
                        <div class="search-form">
                            <form id="user-search" action="{{ route('tpo-bank-transaction.search') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Category</label>
                                            <input class="form-control" name="category" type="text">
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Package Name</label>
                                            <input class="form-control" name="pkg_name" type="text">
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Referral</label>
                                            <input class="form-control" name="referral" type="text">
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date From</label>
                                            <div class='input-group date' id='from-date'>
                                                <input type='text' class="form-control" name="from_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date To</label>
                                            <div class='input-group date' id='to-date'>
                                                <input type='text' class="form-control" name="to_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn theme-btn" name="submit" type="submit">Filter</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        

                    <div class="x_title">
                        <h2>Package Earnings History:</h2>
                    </div>
                    <div class="referral-box">
                        <div class="table-holder">
                            <table class="table table-striped table-hover">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Category</th>
                                        <th>Package Name</th>
                                        <th>Net Amount</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody class="package_earnings_wrap">
                                    <tr>
                                        <td colspan="100">No Package Earnings</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p></p>

                        <div class="pkg_earning_pagination_wrap"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->


<div style="display: none;">
    <form id="pkg_earning_frm">
        {{ csrf_field() }}
        <input type="text" id="pe_package_id" name="packageId" value="">
        <input type="text" id="pe_package_cat" name="packageCat" value="">
        <input type="text" id="pe_qpage" name="qpage" value="1">
        <input type="text" id="pe_page" name="page" value="1">
        <input type="text" id="pe_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="pkg_earning_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="pep_package_id" name="packageId" value="">
        <input type="text" id="pep_package_cat" name="packageCat" value="">
        <input type="text" id="pep_pkgearning_id" name="pkgEarningId" value="0">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var pkgEarningUrl = "{{ route('member.packageEarnings') }}";
    var pkgEarningPushtobankUrl = "{{ route('member.pkgEarningPushToBank') }}";
    

    /*/ ==============================================================
    //  referral credits
    // ==============================================================/*/
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".package_earnings_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#pkg_earning_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: pkgEarningUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.pkg_earning_pagination_wrap').html(ret.paginationHtml);
                jQuery('.pkg_earning_total').html(ret.rcTotal);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  referral credits pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.pkg_earning_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#pe_qpage').val( qpage );
            jQuery('#pe_page').val( page );
            jQuery('#pe_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });


    jQuery(document).on( 'click', '.pkgearning_filter_btn', function(){
        var pe_package_cat = jQuery(this).data('cat');

        jQuery('#pe_package_cat').val(pe_package_cat);
        jQuery('#pe_qpage').val( 1 );
        jQuery('#pe_page').val( 1 );

        get_referral_credits();
        
        return false;
    });


    /*/ ==============================================================
    //  referral credits push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.pushtobank_btn', function(){
        _this = jQuery(this);
        var category = jQuery(this).data('cat');
        var commId = jQuery(this).data('commid');

        jQuery('#pep_pkgearning_id').val( commId );
        jQuery('#pep_package_cat').val( category );

        show_global_ajax_loader();

        var form_elem = jQuery('#pkg_earning_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: pkgEarningPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.'+category+'_pkgearning').text('$'+rdata.rcTotal);
                    jQuery('.pkg_earning_overall_total').text('$'+rdata.rcOverallTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


</script>
@stop