@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/referral.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Investment Loop <span></span></h1>
    </div>

    <div class="row">
        <div class="referral-container">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>Referral Credits History:</h2>
                    </div>
                    <div class="referral-box">
                        <div class="table-holder">
                            <table class="table table-striped table-hover">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Users ID</th>
                                        <th>Username</th>
                                        <th>Available Balance</th>
                                    </tr>
                                </thead>
                                <tbody class="referral_credits_wrap">
                                    <tr>
                                        <td colspan="100">No referral Credits Earned</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p></p>

                        <div class="il-pagination-wrap"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->


<div style="display: none;">
    <form id="get_ref_credit_frm">
        {{ csrf_field() }}
        <input type="text" id="il_user_id" name="userId" value="">
        <input type="text" id="il_qpage" name="qpage" value="1">
        <input type="text" id="il_page" name="page" value="1">
        <input type="text" id="il_qlimit" name="qlimit" value="20">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var refCredUrl = "{{ route('member.getUsersPayment') }}";
    

    /*/ ==============================================================
    //  referral credits
    // ==============================================================/*/
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".referral_credits_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.il-pagination-wrap').html(ret.paginationHtml);

                jQuery('.referral_credit_total').html(ret.rcTotal);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  referral credits pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.il-pagination-wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#il_qpage').val( qpage );
            jQuery('#il_page').val( page );
            jQuery('#il_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });


</script>
@stop