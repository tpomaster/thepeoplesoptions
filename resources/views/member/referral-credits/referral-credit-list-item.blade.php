<!-- for c-trading, c-mining, c-exchange -->

<?php if ( $RefCredits ): ?>
    <?php foreach ($RefCredits as $rc){
        $note = json_decode($rc->note);
        ?>
        <tr class="refcred_item_<?php echo $rc->transaction_code; ?>">
            <td><?php echo $rc->package_cat; ?></td>
            <td>
                <?php if ( isset($note->package_name) ): ?>
                    <?php echo $note->package_name; ?>
                <?php else: ?>
                    <?php echo $rc->package_id; ?>
                <?php endif ?>
            </td>
            <td><?php echo $ReferralNames[$rc->investor_user_id]['username']; ?></td>
            <td><?php echo $rc->tier_level; ?></td>
            <td>$<?php echo LpjHelpers::amt2($rc->amount); ?></td>
            <td><?php echo $rc->created_at; ?></td>
            <td>
                <?php if ( $rc->status != 'withdrawn' ): ?>
                    <a href="#" class="btn theme-btn pushtobank_btn" data-commid="<?php echo $rc->id; ?>" data-cat="<?php echo $rc->package_cat; ?>">Push To Bank</a>

                <?php else: ?>
                    <?php echo $rc->status ?>
                <?php endif ?>
            </td>
        </tr>
    <?php } ?>

<?php else: ?>
    <tr>
        <td colspan="100">
            None
        </td>
    </tr>
<?php endif ?>
    
