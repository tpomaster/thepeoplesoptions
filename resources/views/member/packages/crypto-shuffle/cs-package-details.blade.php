@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/cryptotrading-packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Available Packages</h1>
    </div>

    <div class="row package-details">
        <div class="col-lg-3 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?php if ( $pkg->status == 'available' ): ?>
                        @include('member/packages/crypto-shuffle/detail-parts/cs-stat-available')

                    <?php else: ?>
                        @include('member/packages/crypto-shuffle/detail-parts/cs-stat-active')
                    <?php endif ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="disclaimer">
                        <div class="x_panel tile">
                            <h2 class="panel-title">Disclaimer</h2>
                            <ul>
                                <li>Losing money is inevitable, Members should not purchase packages with funds they cannot afford to lose.</li>
                                <li>The daily earnings are calculated and transferred to the member’s account based on the membership level, funded amount, and package option chosen.</li>
                                <li>Daily earnings will vary based on packages and terms for packages.</li>
                                <li>Earnings/losses accrue 7 days a week, including weekends and holidays.</li>
                                <li>Earnings are deposited in your TPO account. Losses are taken from the funded amount. You have 24 hours/1 week to replace the loss to continue trading at the original funded amount.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-xs-12">
            <div class="single-package-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a  href="#1b" data-toggle="tab">Package Info</a></li>
                    <li><a href="#2b" data-toggle="tab">Purchase Ticket</a></li>
                    <li><a href="#3b" data-toggle="tab">Referral Credits</a></li>
                    <li><a href="#4b" data-toggle="tab">Winning Tickets</a></li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane fade in active" id="1b">
                        <div class="package-wrap2">
                            <h2 class="tab-title">Package Information</h2>
                            <div class="package-summary">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <span class="info-title">Ticket Price:</span>
                                                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->ticket_price); ?></span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Max Ticket per Order::</span>
                                                <span class="pull-right">1,000 Tickets</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Referral Credit Per Ticket:</span>
                                                <span class="pull-right">0.043</span>
                                            </li>
                                            
                                            <li class="list-group-item">
                                                <span class="info-title">Tier Levels:</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row tier-table">
                                    <div class="col-md-12 col-s-12 col-xs-12">
                                        <div class="table-holder">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Tier</th>
                                                        <th>Referral Credit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($pkg->tier_config as $k => $t): ?>
                                                        <tr>
                                                            <td><?php echo $t['tier_level']; ?></td>
                                                            <td><?php echo $t['referral_credit'] ?>%</td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <button type="button" class="btn theme-btn full" data-toggle="modal" data-target="#terms">View Terms and Conditions</button>
                                    <!-- terms and condition -->
                                    @include('member/packages/crypto-shuffle/detail-parts/cs-terms')
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="2b">
                        <div class="package-wrap2">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h2 class="tab-title">Purchase Tickets</h2>
    
                                    <?php if ( $pkg->available_tickets_count > 0 && $pkg->status == 'available' ): ?>
                                        <div class="fund-summary">
                                            <h3>Available for Funding: <span class="user_profit_total">$<?php echo LpjHelpers::amt2($pkg->available_tickets_count); ?></span></h3>
                                        </div>

                                        <input type="hidden" id="package_name" name="packageName" value="<?php echo $pkg->name ?>">
                                        <input type="hidden" id="min_amount" name="minAmount" value="10">
                                        <input type="hidden" id="ticket_price" name="minAmount" value="<?php echo LpjHelpers::amt2($pkg->ticket_price); ?>">

                                        <form class="" id="queue_order_form">
                                            {{ csrf_field() }}
                                            <input type="hidden" id="pid" name="pid" value="<?php echo $pkg->id ?>">
                                            <input type="hidden" id="queueCode" name="queueCode" value="<?php echo $queueCode ?>">

                                            <div class="form-group ">
                                                <label class="control-label ">Enter How many Tickets:</label>
                                                <input id="ticket_quantity" class="form-control lpj_num_only" name="ticketQuantity" value="10" type="text">
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label ">Payment Option:</label>
                                                <input id="payment_gateway2" class="form-control" name="paymentGateway" value="TPO Bank" disabled="disabled" type="text">
                                                <div class>
                                                    <p>
                                                        Your TPO Bank Balance:
                                                        <strong class="tpo_bank_balance" data-tpobank="<?php echo $userBank->available_bal; ?>">$<?php echo LpjHelpers::amt2($userBank->available_bal); ?></strong>
                                                        <a class="btn theme-btn btn-sm" target="_blank" href="{{ route('member.tpoBank') }}">Add Balance?</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="agree_with_terms" type="checkbox">
                                                        I Agree to the
                                                        <a class="agree_with_terms_btn" href="#">Terms and Conditions</a>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="#" class="btn theme-btn btn-lg confirm_payment_btn">Purchase</a>
                                            </div>
                                        </form>


                                        <form method="post" id="process_queued_order_form" action="" style="display:none;">
                                            {{ csrf_field() }}
                                            <input type="text" id="pid" name="pid" value="<?php echo $pkg->id ?>">
                                            <input type="text" id="queue_code" name="queueCode" value="<?php echo $queueCode; ?>">
                                            <button class="btn btn-primary process_queued_order_form_btn" name="submit" type="submit">Pay Now</button>
                                        </form>

                                    <?php else: ?>
                                        <div class="fund-summary">
                                            <h4>Tickets are now sold out!</h4>
                                        </div>
                                    <?php endif ?>
                                        
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="funded-summary">
                                        <h2 class="tab-title">Tickets you Purchased</h2>
                                        <?php if ( $myPurchasedTickets): ?>
                                            <div class="table-holder">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Ticket Count</th>
                                                            <th>Amount</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($myPurchasedTickets as $k => $order): ?>
                                                            <tr>
                                                                <td>
                                                                    <a class="ticket_codes_btn" data-oid="<?php echo $order->id ?>" href="#">
                                                                        <?php echo LpjHelpers::num($order->quantity); ?>
                                                                    </a>
                                                                    <div class="ticket_codes_wrap ticket_codes_wrap_<?php echo $order->id ?>" style="display: none;">
                                                                        <?php if ( !empty($order->ticket_codes) ): ?>
                                                                            <?php $tickets = unserialize($order->ticket_codes); ?>
                                                                            <?php foreach ($tickets as $k => $v): ?>
                                                                                <span><?php echo $v; ?></span>, &nbsp;
                                                                            <?php endforeach ?>
                                                                        <?php endif ?>
                                                                        <?php //echo $order->ticket_codes; ?>
                                                                    </div>        
                                                                </td>
                                                                <td>$<?php echo LpjHelpers::amt2($order->total_amount); ?></td>
                                                                <td><?php echo LpjHelpers::nice_date($order->date); ?></td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php else: ?>
                                            <p>
                                                None
                                            </p>
                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="3b">
                        <div class="package-wrap2">
                            <h2 class="tab-title">Referral Credits</h2>
                            <div class="fund-summary">
                                <h3>Total Referral Credits: <span class="referral_credit_total">$0.00</span></h3>
                            </div>
                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Package</th>
                                            <th>Referral</th>
                                            <th>Tier</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="referral_credits_wrap">
                                        <tr>
                                            <td colspan="100">No referral Credits Earned</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="refcred_pagination_wrap"></div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="4b">
                        <div class="package-wrap2">
                            <h2 class="tab-title">Winning Tickets</h2>
                            <div class="fund-summary">
                                <h3>Total NET Earning: <span class="user_profit_total">$0.00</span></h3>
                                <h5>Total Withdrawn Earnings: <span class="withdrawn_user_profit_total">$0.00</span></h5>
                                <h5>Total Unwithdrawn Earning: <span class="unwithdrawn_user_profit_total">$0.00</span></h5>
                            </div>
                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Ticket Code</th>
                                            <th>Reward</th>
                                            <th>Tier Level</th>
                                            <th>Redeem Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="user_profit_wrap">
                                        <tr>
                                            <td colspan="100">No Profits</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="user_profit_pagination_wrap"></div>
                            <span class="clearfix"></span>
                            <div class="row" style="display: none;">
                                <div class="col-md-3 col-xs-12">
                                    <a class="btn theme-btn">Refresh List</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->

<div id="payment_confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Confirm Payment</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap payment_confirmation_content_wrap">
                    <div class="ajax_target">
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="invoice_wrap">
                                    <div class="countdown_wrap">
                                        You only have <span id="countdown_display">45</span> seconds to press the <strong>"PAY NOW"</strong> button.
                                    </div>
                                    <input type="hidden" id="countdown_clock" placeholder="00:00" value="01:20">

                                    <p>
                                        <span>Package Name: </span> 
                                        <strong class="package_name2"> </strong>
                                    </p>
                                    <p>
                                        <span>Ticket Quantity: </span>
                                        <strong class="ticket_quantity2"> </strong>
                                    </p>
                                    <p>
                                        <span>Ticket Price: </span>
                                        $<strong class="ticket_price2"> </strong>
                                    </p>
                                    <p>
                                        <span>Amount to Pay: </span>
                                        $<strong class="fund_amount2"> </strong>
                                    </p>

                                    <p>
                                        <a href="#" class="btn theme-btn btn-lg pay_now_btn">Pay Now</a>
                                        <a href="#" class="btn theme-btn btn-lg cancel_payment_btn">Cancel</a>
                                    </p>
                                </div>
                                
                                <div class="order_queued_msg_wrap" style="display: none;">
                                    <div class="alert alert-info">
                                        <h2>Order Queued.</h2>
                                        <h3>Your Order was placed in the order queue. Please do not close your browser/tab. This will not take more than 2 minutes.</h3>
                                    </div>
                                </div>

                                <div class="success_payment_wrap" style="display: none;">
                                    <div class="alert alert-success">
                                        <h2>Payment has been completed.</h2>
                                        <p>Please refresh this page.</p>
                                    </div>
                                    <p>
                                        <a href="" class="btn theme-btn btn-sm">Refresh</a>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>

<div id="ticket_codes_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="modaltitle">Ticket Codes</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="ticket_codes_modal_content">
                                
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn theme-btn" data-dismiss="modal">Close</button>
            </div>
        </div>  
    </div>
</div>

<div style="display: none;">
    <form id="get_ref_credit_frm">
        {{ csrf_field() }}
        <input type="text" id="rc_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="rc_package_cat" name="packageCat" value="cryptoshuffle">
        <input type="text" id="rc_qpage" name="qpage" value="1">
        <input type="text" id="rc_page" name="page" value="1">
        <input type="text" id="rc_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="get_ref_credit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="rcp_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="rcp_package_cat" name="packageCat" value="raffle">
        <input type="text" id="rcp_refcred_id" name="refCredId" value="0">
    </form>
</div>

<div style="display: none;">
    <form id="get_user_profit_frm">
        {{ csrf_field() }}
        <input type="text" id="up_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="up_fund_id" name="fundId" value="0">
        <input type="text" id="up_qpage" name="qpage" value="1">
        <input type="text" id="up_page" name="page" value="1">
        <input type="text" id="up_qlimit" name="qlimit" value="5">
    </form>
</div>

<div style="display: none;">
    <form id="user_profit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="upp_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="upp_profit_id" name="profitId" value="0">
    </form>
</div>


@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>
<script src="{{ asset('js/member/tock.min.js') }}"> </script>

<script type="text/javascript">
    var queueOrderUrl = "{{ route('member.cryptoShuffleQueueOrder') }}";
    var processOrderUrl = "{{ route('member.cryptoShuffleProcessOrder') }}";
    var bankUrl = "{{ route('member.tpoBank') }}";
    var refCredUrl = "{{ route('member.getReferralCredits') }}";
    var refCredPushtobankUrl = "{{ route('member.refCredPushToBank') }}";
    var userProfitUrl = "{{ route('member.cExchangeGetUserProfits') }}";
    var userProfitPushtoBankUrl = "{{ route('member.cExchangeUserProfitsPushToBank') }}";
    
    var countdown = false;

    jQuery(document).ready(function(){
        countdown = Tock({
            countdown: true,
            callback: function () {
                /*/console.log(countdown.lap() / 1000);/*/
                jQuery('#payment_confirmation_modal').find('#countdown_display').html(countdown.msToTime(countdown.lap()));

            },
            complete: function () {
                jQuery('#payment_confirmation_modal').find('.cancel_payment_btn').trigger('click');
            }
        });
    });

    function countdown_destroy(){
        countdown.stop();
        countdown.reset();
    }

    /* ==============================================================
    payment confirmation popup
    ============================================================== */
    jQuery('.confirm_payment_btn').click(function(){
        var ticket_quantity = jQuery('#ticket_quantity').val();
        var ticket_price = jQuery('#ticket_price').val();

        jQuery('.package_name2').html( jQuery('#package_name').val() );
        jQuery('.ticket_quantity2').html( ticket_quantity );
        jQuery('.ticket_price2').html( ticket_price );

        var amount_to_pay = ticket_quantity * ticket_price;
        jQuery('.fund_amount2').html( amount_to_pay );
        
        var msg = validate_payment_form();

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            countdown.stop();
            countdown.reset();
            countdown.start(jQuery('#countdown_clock').val());
                
            jQuery('#payment_confirmation_modal').modal({
                backdrop: 'static',
                keyboard: false
            });

            jQuery('.invoice_wrap').show();
            jQuery('.order_queued_msg_wrap').hide();
            jQuery('.success_payment_wrap').hide();
        }

        return false;
    });

    /* cancel payment */
    jQuery(document).on( 'click', '.cancel_payment_btn', function(){
        jQuery('#payment_confirmation_modal').modal('hide');
        countdown_destroy();
        return false;
    });


    /* ==============================================================
    submit payment / queue order
    ============================================================== */
    jQuery(document).on( 'click', '.pay_now_btn', function(){
        jQuery('#queue_order_form').submit();
        
        return false;
    });

    jQuery('#queue_order_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();
        countdown_destroy();

        var msg = validate_payment_form();

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            var input_data = jQuery(this).serialize();
            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: queueOrderUrl,
                dataType: 'json',
                data: input_data,
                success: function( data ){
                    if ( data.isError ) {
                        show_page_ajax_message( data.msg, 'danger');

                    }else{
                        jQuery('.invoice_wrap').hide();
                        jQuery('.order_queued_msg_wrap').show();
                        jQuery('.success_payment_wrap').hide();

                        setTimeout(function(){
                            jQuery('.process_queued_order_form_btn').trigger('click');
                        }, 3000);
                    }
                        
                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        }

        return false;
    });


    function validate_payment_form(){
        var msg = '';

        var tpo_bank_balance = parseFloat( jQuery('.tpo_bank_balance').data("tpobank") );
        var ticket_quantity = jQuery('#ticket_quantity').val();
        var ticket_price = jQuery('#ticket_price').val();
        var amount_to_pay = ticket_quantity * ticket_price;

        var max_ticket_per_order = 1000;

        if ( jQuery('#agree_with_terms').is(":checked") === false ) {
            msg = 'You have to agree with the terms and conditions to continue';

        }else if( amount_to_pay > tpo_bank_balance ){
            msg = 'Insufficient Balance.';

        }else if( ticket_quantity > max_ticket_per_order ){
            msg = 'Minimum tickets that you can purchase per order is $'+max_ticket_per_order+'.';
        }
        // else if( fund_amount < min_amount ){
        //     msg = 'Minimum Fund Amount is $'+min_amount+'.';
        // }

        return msg;
    }


    jQuery('#process_queued_order_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();
        countdown_destroy();

        var msg = validate_payment_form();

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            var input_data = jQuery(this).serialize();
            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: processOrderUrl,
                dataType: 'json',
                data: input_data,
                success: function( data ){
                    if ( data.isError ) {
                        if ( data.action == 'loopform') {
                            setTimeout(function(){
                                jQuery('.process_queued_order_form_btn').trigger('click');
                            }, 3000);
                            
                        }else{
                            show_page_ajax_message( data.msg, 'danger');
                        }

                    }else{
                        jQuery('.invoice_wrap').hide();
                        jQuery('.order_queued_msg_wrap').hide();
                        jQuery('.success_payment_wrap').show();
                    }
                        
                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        }

        return false;
    });


    /*/ ==============================================================
    //  referral credits
    // ==============================================================/*/
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".referral_credits_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.refcred_pagination_wrap').html(ret.paginationHtml);

                jQuery('.referral_credit_total').html(ret.rcTotal);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  referral credits pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.refcred_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#rc_qpage').val( qpage );
            jQuery('#rc_page').val( page );
            jQuery('#rc_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });


    /*/ ==============================================================
    //  referral credits push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var commId = jQuery(this).data('commid');

        jQuery('#rcp_refcred_id').val( commId );

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.referral_credit_total').text('$'+rdata.rcTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    /*/ ==============================================================
    //  get user profit
    // ==============================================================/*/
    //get_user_profit();
    function get_user_profit(){
        show_global_ajax_loader();

        var form_elem = jQuery('#get_user_profit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery(".user_profit_wrap").html(ret.returnHTML);
                jQuery('.user_profit_pagination_wrap').html(ret.paginationHtml);

                jQuery('.user_profit_total').html('$'+ret.pkgEarningTotal);
                jQuery('.withdrawn_user_profit_total').html('$'+ret.pkgEarningTotalWithdrawn);
                jQuery('.unwithdrawn_user_profit_total').html('$'+ret.pkgEarningTotalPending);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  user profit pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.user_profit_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#up_qpage').val( qpage );
            jQuery('#up_page').val( page );
            jQuery('#up_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_user_profit();
        }

        return false;
    });


    /*/ ==============================================================
    // user profit push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.userprofit_pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var trid = jQuery(this).data('trid');

        jQuery('#upp_profit_id').val( trid );

        show_global_ajax_loader();

        var form_elem = jQuery('#user_profit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitPushtoBankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.user_profit_total').html('$'+rdata.profitTotal);
                    jQuery('.withdrawn_user_profit_total').html('$'+rdata.profitTotalWithdrawn);
                    jQuery('.unwithdrawn_user_profit_total').html('$'+rdata.profitTotalPending);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

    /*/ ==============================================================
    // view ticket codes
    // ==============================================================/*/
    jQuery(document).on( 'click', '.ticket_codes_btn', function(){
        _this = jQuery(this);
        var order_id = jQuery(this).data('oid');

        var content = jQuery('.ticket_codes_wrap_'+order_id).html();
        console.log(content);

        jQuery('.ticket_codes_modal_content').html(content);
        jQuery('#ticket_codes_modal').modal({
            backdrop: 'static',
            keyboard: false
        });

        return false;
    });

</script>
@stop