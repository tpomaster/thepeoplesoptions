<?php if ( !empty($packages) ): ?>
    <?php foreach ($packages as $pkg) : ?>

        <?php //if ( $pkg->status == 'available' ): ?>
            <div class="package-wrap">
                <div class="row">
                    <div class="col-lg-4 col-xs-12 package-info">
                        <h3><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?> </h3>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="info-title">Package Status:</span>
                                <span class="pull-right package-status status-<?php echo $pkg->status; ?>"><?php echo strtoupper( $pkg->status ); ?></span>
                            </li>
                            <li class="list-group-item">
                                <span class="info-title">Sold Ticket Count:</span>
                                <span class="pull-right tpo-warning"><?php echo LpjHelpers::num($pkg->sold_ticket_cnt); ?></span>
                            </li>
                            <li class="list-group-item">
                                <span class="info-title">Available Ticket Count:</span>
                                <span class="pull-right"><?php echo LpjHelpers::num($pkg->ticket_total_cnt); ?></span>
                            </li>
                        </ul>
                        <div class="package-btns">
                            <a class="btn theme-btn clearfix" href="{{ route('member.cryptoShufflePackageDetails', ['packageId' => $pkg->id]) }}">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12 chart-box">
                        <?php 
                        $funded_percent = ( (float)$pkg->sold_ticket_cnt / (float)$pkg->ticket_total_cnt )*100;
                        $funded_percent = LpjHelpers::amt2($funded_percent);
                        //$funded_percent = ($total_fund/$package_amount)*100;
                        ?>
                        <h3>Sold Ticket Count: <span class="fund-percent"><?php echo LpjHelpers::num($pkg->sold_ticket_cnt); ?> ( <?php echo $funded_percent; ?>% )</span></h3>
                        <div id="progress-<?php echo $pkg->id ?>" class="gauge-chart"></div>
                    </div>
                    <div class="col-lg-2 col-xs-12 traiders-info">
                        <img src="{{asset('images/memberPic3.png')}}" class="trade-avatar" alt="">
                        <h3><span class="crypto">CRYPTOSHUFFLE</span><span class="package-shuffle">TICKET</span></h3>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    var pkgid = <?php echo $pkg->id; ?>;
                    var chart_container_id = 'progress-<?php echo $pkg->id; ?>';

                    get_package_guage( <?php echo $funded_percent; ?>, chart_container_id);

                    jQuery('.new_earning_btn').click(function(){
                        jQuery(this).hide();
                    });
                });
            </script>
        <?php //endif ?>
    
    <?php endforeach; ?>

<?php else: ?>
    <div class="package-wrap">
        <div class="row">
            <div class="col-lg-4 col-xs-12 package-info">
                <h3><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> No Result </h3>
            </div>
        </div>
    </div>
<?php endif ?>
