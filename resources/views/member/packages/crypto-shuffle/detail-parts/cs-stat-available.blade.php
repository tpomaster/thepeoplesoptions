<div class="package-details-box">
    <div class="x_panel tile">
        <h2 class="panel-title"><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?></h2>

        <ul class="list-group">
            <li class="list-group-item clearfix">
                <span class="info-header">Package Status:</span>
                <span class="pull-right package-status fund-<?php echo $pkg->status; ?>"><?php echo $pkg->status; ?></span>
            </li>
            <li class="list-group-item clearfix">
                <span class="info-title">Sold Ticket Count:</span>
                <span class="pull-right"><?php echo LpjHelpers::amt2($pkg->sold_ticket_cnt); ?></span>
            </li>
            <li class="list-group-item clearfix">
                <span class="info-title">Sold Ticket Percentage:</span>
                <span class="pull-right"><?php echo LpjHelpers::amt2($pkg->sold_percent); ?>%</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Available Ticket Count:</span>
                <span class="pull-right"><?php echo LpjHelpers::amt2($pkg->available_tickets_count); ?></span>
            </li>
        </ul>


        
    </div>
</div>