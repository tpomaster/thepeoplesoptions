<div class="package-details-box">
    <div class="x_panel tile">
        <h2 class="panel-title"><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?></h2>

        <ul class="list-group">
            <li class="list-group-item">
                <span class="info-header">Package Status:</span>
                <span class="pull-right package-status fund-<?php echo $pkg->status; ?>"><?php echo $pkg->status; ?></span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Tickets are now sold out.</span>
            </li>
        </ul>


        <ul class="list-group">
            <li class="list-group-item">
                <span class="info-header">Profit Totals:</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Profit</span>
                <span class="pull-right">$0.00</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Management Fee:</span>
                <span class="pull-right">$0.00</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Net Earning:</span>
                <span class="pull-right">$0.00</span>
            </li>
        </ul>


    </div>
</div>