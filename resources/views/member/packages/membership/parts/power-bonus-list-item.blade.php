<!-- for c-trading, c-mining, c-exchange -->

<?php if ( $powBonuses ): ?>
    <?php foreach ($powBonuses as $pb){ ?>
        <tr class="pow_bonus_item_<?php echo $pb->id; ?>">
            <td>$<?php echo LpjHelpers::amt2($pb->amount); ?></td>
            <td><?php echo LpjHelpers::nice_date($pb->date_added); ?></td>
            <td>
                <?php if ( $pb->status != 'withdrawn' ): ?>
                    <a href="#" class="btn theme-btn pushtobank_powbonus_btn" data-bonusid="<?php echo $pb->id; ?>">Push To Bank</a>

                <?php else: ?>
                    <?php echo $pb->status ?>
                <?php endif ?>
            </td>
        </tr>
    <?php } ?>

<?php else: ?>
    <tr>
        <td colspan="100">
            None
        </td>
    </tr>
<?php endif ?>
    
