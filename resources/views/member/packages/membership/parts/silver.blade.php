<div class="mermbership-column column2 clearfix">
    <div class="info-header clearfix">
        <div class="mobile-info">
            <h2 class="membership-title">Silver Membership</h2>
            <h3 class="membership-sub"><span class="price-inline"><span class="membership-dollar">$</span><?php echo number_format($pkg->membership_fee); ?></span><span class="price-inline price-small">.00<span class="year-label">/ YEAR</span></span><span class="sub-status">Yearly Membership Pricing</span></h3>
        </div>
        <div class="mobile-info info-btn">
            <?php if ( $membership == 'silver'): ?>
                <div>
                    This is you current membership. <br>
                    It will expire on: 
                    <?php if ( !empty( $membershipEndDate )): ?>
                        <?php echo LpjHelpers::niceDate($membershipEndDate); ?>
                    <?php endif ?>
                </div>

                <?php if ( $membershipDaysLeft < 30 ): ?>
                    <a href="" class="join_now_btn" 
                        data-pkgid="<?php echo $pkg->id; ?>" 
                        data-amount="<?php echo $pkg->membership_fee ?>" 
                        data-name="<?php echo $pkg->name ?>"
                    >
                        RENEW
                        <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                        <span class="ftr-btn-info">Comes with special packages</span>
                    </a>
                <?php endif ?>

            <?php else: ?>
                <?php
                $label = 'UPGRADE';
                if ( $membership == 'free'){ 
                    $label = 'JOIN NOW';
                } ?>

                <?php if ( $membership == 'free' ): ?>
                    <a href="" class="join_now_btn" data-pkgid="<?php echo $pkg->id; ?>" data-amount="<?php echo $pkg->membership_fee ?>" data-name="<?php echo $pkg->name ?>">
                        <?php echo $label ?>
                        <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                        <span class="ftr-btn-info">Comes with special packages</span>
                    </a>

                <?php else: ?>
                    <?php if ( $membershipDaysLeft < 30 ): ?>
                        <a href="" class="join_now_btn" 
                            data-pkgid="<?php echo $pkg->id; ?>" 
                            data-amount="<?php echo $pkg->membership_fee ?>" 
                            data-name="<?php echo $pkg->name ?>"
                        >
                            <?php echo $label ?>
                            <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                            <span class="ftr-btn-info">Comes with special packages</span>
                        </a>
                    <?php endif ?>
                <?php endif ?>

            <?php endif ?>
        </div>
    </div>
    <div class="togle-list">
        <div class="members-list">
            <ul>
                <li><span class="list-label">Withdrawals from your account</span>Two per month</li>
                <li><span class="list-label">Withdrawal Limit</span>Up to $2000 every month</li>
                <li><span class="list-label">Withdrawal Fee</span>at 4%</li>
                <li class="spacing"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                <li><span class="list-label">Management Fee</span>Up to 35%</li>
                <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                <li><span class="list-label">Access to our CMS</span>Partial Access</li>
                <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                <li><span class="list-label">Bonuses</span><i class="fa fa-times-circle-o" aria-hidden="true"></i></li>
            </ul>
        </div>
        <div class="info-footer">
            <?php if ( $membership == 'silver'): ?>
                <div>
                    This is you current membership. <br>
                    It will expire on: 
                    <?php if ( !empty( $membershipEndDate )): ?>
                        <?php echo LpjHelpers::niceDate($membershipEndDate); ?>
                    <?php endif ?>
                </div>

                <?php if ( $membershipDaysLeft < 30 ): ?>
                    <a href="" class="join_now_btn" 
                        data-pkgid="<?php echo $pkg->id; ?>" 
                        data-amount="<?php echo $pkg->membership_fee ?>" 
                        data-name="<?php echo $pkg->name ?>"
                    >
                        RENEW
                        <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                        <span class="ftr-btn-info">Comes with special packages</span>
                    </a>
                <?php endif ?>

            <?php else: ?>
                <?php
                $label = 'UPGRADE';
                if ( $membership == 'free'){ 
                    $label = 'JOIN NOW';
                } ?>

                <?php if ( $membership == 'free' ): ?>
                    <a href="" class="join_now_btn" data-pkgid="<?php echo $pkg->id; ?>" data-amount="<?php echo $pkg->membership_fee ?>" data-name="<?php echo $pkg->name ?>">
                        <?php echo $label ?>
                        <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                        <span class="ftr-btn-info">Comes with special packages</span>
                    </a>

                <?php else: ?>
                    <?php if ( $membershipDaysLeft < 30 ): ?>
                        <a href="" class="join_now_btn" 
                            data-pkgid="<?php echo $pkg->id; ?>" 
                            data-amount="<?php echo $pkg->membership_fee ?>" 
                            data-name="<?php echo $pkg->name ?>"
                        >
                            <?php echo $label ?>
                            <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
                            <span class="ftr-btn-info">Comes with special packages</span>
                        </a>
                    <?php endif ?>
                <?php endif ?>

            <?php endif ?>

        </div>
    </div>
</div>