@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/membership.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Membership Package</h1>
    </div>
        
    <div class="row">
        <div class="col-md-12 col-s-12 col-xs-12">
            <div class="membership-info">

                <div class="mermbership-column column1">
                    <div class="info-header">
                        <img src="{{ asset('images/membership-logo.png') }}" alt="">
                    </div>
                    <div class="members-list">
                        <ul>
                            <li>Withdrawals from your account</li>
                            <li>Withdrawal Limit</li>
                            <li>Withdrawal Fee</li>
                            <li class="spacing">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</li>
                            <li class="spacing">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</li>
                            <li>Management Fee</li>
                            <li>Free Bitshares Registration</li>
                            <li>Access to our CMS</li>
                            <li>Special Packages</li>
                            <li>Bonuses</li>
                        </ul>
                    </div>
                    <div class="info-footer"></div>
                </div>

                
                <?php foreach ($pkgs as $k => $pkg): ?>
                    <?php $file_name = 'member/packages/membership/parts/.' . strtolower( $pkg->name ); ?>
                    @include($file_name)
                <?php endforeach ?>

                <div class="mermbership-column column5 clearfix">
                    <div class="info-header clearfix">
                        <div class="mobile-info">
                            <h2 class="membership-title">Bronze Membership</h2>
                            <h3 class="membership-sub"><span class="price-inline">FREE</span><span class="sub-status">Yearly Membership Pricing</span></h3>
                        </div>
                    </div>
                    <div class="togle-list">
                        <div class="members-list">
                            <ul>
                                <li><span class="list-label">Withdrawals from your account</span>One per month</li>
                                <li><span class="list-label">Withdrawal Limit</span>Up to $1000 every month</li>
                                <li><span class="list-label">Withdrawal Fee</span>at 5%</li>
                                <li class="spacing2"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span>Earn partially from your personal referrals</li>
                                <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                                <li><span class="list-label">Management Fee</span>Up to 45%</li>
                                <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                                <li><span class="list-label">Access to our CMS</span>No Access</li>
                                <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
                                <li><span class="list-label">Bonuses</span><i class="fa fa-times-circle-o" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <form class="" id="fund_package_form">
                    {{ csrf_field() }}
                    <input type="hidden" id="membership_frm_pid" name="pid" value="">
                </form>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div id="payment_confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Confirm Payment</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap payment_confirmation_content_wrap">
                    <div class="ajax_target">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="invoice_wrap">
                                    <p>
                                        <span>Membership Level: </span> 
                                        <strong class="package_name2"> </strong>
                                    </p>
                                    <p>
                                        <span>Membership Fee: </span>
                                        $<strong class="fund_amount2"> </strong>
                                    </p>
                                    <p>
                                        <a href="#" class="btn theme-btn btn-lg pay_now_btn">Pay Now</a>
                                        <a href="#" class="btn theme-btn btn-lg cancel_payment_btn">Cancel</a>
                                    </p>
                                </div>
                                
                                <div class="success_payment_wrap">
                                    <div class="callout callout-success">
                                        <h2>Payment has been completed.</h2>
                                        <p>Please refresh this page.</p>
                                    </div>
                                    <p>
                                        <a href="" class="btn theme-btn btn-sm">Refresh</a>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default close_payment_modal" data-dismiss="modal">Close</button>
            </div> -->
        </div>  
    </div>
</div>

<!-- end page content -->

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var orderUrl = "{{ route('member.membershipProcessOrder') }}";
    var bankUrl = "{{ route('member.tpoBank') }}";
    var refCredUrl = "{{ route('member.getReferralCredits') }}";

    jQuery('.join_now_btn').click(function(){
        jQuery('.package_name2').html( jQuery(this).data('name') );
        jQuery('.fund_amount2').html( jQuery(this).data('amount') );

        var msg = '';

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            jQuery('#payment_confirmation_modal').modal({
                backdrop: 'static',
                keyboard: false
            });

            jQuery('.invoice_wrap').show();
            jQuery('.success_payment_wrap').hide();

            jQuery('#membership_frm_pid').val( jQuery(this).data('pkgid') );
        }

        return false;
    });

    /* cancel payment */
    jQuery(document).on( 'click', '.cancel_payment_btn', function(){
        jQuery('#payment_confirmation_modal').modal('hide');
        return false;
    });

    jQuery(document).on( 'click', '.pay_now_btn', function(){
        jQuery('#fund_package_form').submit();
        
        return false;
    });

    /* submit payment */
    jQuery('#fund_package_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        var input_data = jQuery(this).serialize();
        var ajax_target_wrap = jQuery(".payment_confirmation_content_wrap");
        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: orderUrl,
            dataType: 'json',
            data: input_data,
            success: function( data ){
                if ( data.msg2 == 'order_queued' ) {
                    /* show queued message */

                }else if ( data.msg2 == 'insuficient_funds' ) {
                    var msg_arr = ['Order failed. You dont have enough bank fund. <a class="btn btn-xs btn-success" href="'+bankUrl+'">Add Balance</a>'];
                    show_page_ajax_message( msg_arr, 'danger');
                    hide_global_ajax_loader();

                    jQuery('#payment_confirmation_modal').modal('hide');

                }else{
                    if ( data.isError ) {
                        show_page_ajax_message( data.msg, 'danger');

                    }else{
                        //show_page_ajax_message( data.msg, 'success');
                        jQuery('.invoice_wrap').hide();
                        jQuery('.success_payment_wrap').show();
                    }
                        
                    hide_global_ajax_loader();
                }
            },
            error: function(xhr,status,error){
                hide_global_ajax_loader();

                var msg_arr = ['Order Failed. Please Inform us about this.'];
                show_page_ajax_message( msg_arr, 'danger');
            }
        });


        return false;
    });

</script>
@stop