@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/membership.css') }}">
    <link rel="stylesheet" href="{{ asset('css/member/members.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Membership Package</h1>
    </div>
        
    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="current-membership">
                <div class="x_panel tile">
                    <div class="breakdown-box">
                        <h3 class="info-header">
                            Your Current Membership Package: 
                            <span class="<?php echo $membership; ?>-member"><?php echo strtoupper( $membership ); ?></span>
                            <span style="font-size: 12px; font-weight: normal;">
                                <?php if ( !empty( $membershipEndDate )): ?>
                                    Valid Until: <?php echo LpjHelpers::niceDate($membershipEndDate); ?>
                                    <br>
                                    Membership will expire in <?php echo $membershipDaysLeft; ?> days
                                <?php endif ?>
                            </span>
                        </h3>
                    </div>
                        

                    <?php if ( isset($mpkgs['cryptoshuffle']) ): ?>
                        <div class="breakdown-box">
                            <h3 class="info-items-label">
                                <i class="fa fa-ticket"></i>&nbsp;
                                Amount Invested in "CryptoShuffle Ticket" package:
                            </h3>
                            <h4 class="info-items">
                                <a target="_blank" href="{{ route('member.cryptoShufflePackageDetails', ['packageId' => $mpkgs['cryptoshuffle']['package_id']]) }}">
                                <?php echo $mpkgs['cryptoshuffle']['package_name']; ?></a><br>
                                $<?php echo $mpkgs['cryptoshuffle']['amount_invested'] ?>
                            </h4>
                        </div>
                    <?php endif ?>

                    <?php if ( isset($mpkgs['cryptomining']) ): ?>
                        <div class="breakdown-box">
                            <h3 class="info-items-label">
                                <i class="fa fa-diamond"></i>&nbsp;
                                Amount Invested in "Crypto Mining" package:
                            </h3>
                            <h4 class="info-items">
                                <a target="_blank" href="{{ route('member.cryptoMiningPackageDetails', ['packageId' => $mpkgs['cryptomining']['package_id']]) }}"><?php echo $mpkgs['cryptomining']['package_name']; ?></a><br>
                                $<?php echo $mpkgs['cryptomining']['amount_invested'] ?>
                            </h4>
                        </div>
                    <?php endif ?>

                    <?php if ( isset($mpkgs['cryptotrading']) ): ?>
                        <div class="breakdown-box">
                            <h3 class="info-items-label">
                                <i class="fa fa-line-chart"></i>&nbsp;
                                Amount Invested in "Crypto Trading" package:
                            </h3>
                            <h4 class="info-items">
                                <a target="_blank" href="{{ route('member.crypto-trading-package-details', ['packageId' => $mpkgs['cryptotrading']['package_id']]) }}"><?php echo $mpkgs['cryptotrading']['package_name']; ?></a><br>
                                $<?php echo $mpkgs['cryptotrading']['amount_invested'] ?>
                            </h4>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-xs-12">
            <div class="membership-tab">
                <ul class="nav nav-tabs">
                    <li class="active"><a  href="#1m" data-toggle="tab">Package Details</a></li>
                    <li><a href="#2m" data-toggle="tab">Upgrade</a></li>
                    <li><a href="#3m" data-toggle="tab">Referral Credits</a></li>
                    <li><a href="#4m" data-toggle="tab">Power Bonus</a></li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane fade in active" id="1m">
                        <div class="membership-wrap">
                            <div class="tab-title">
                                <h2>Your Current Membership Package: <span class="member-status <?php echo $membership; ?>"><?php echo strtoupper( $membership ); ?></span></h2>
                            </div>
                            <div class="clearfix">

                                <div class="col-md-6 col-sm-6 col-xs-12 membership-columns">
                                    <h2 class="tab-subtitle">Membership Package Details</h2>
                                    <?php $file_name = 'member/packages/membership/features/.' . strtolower( $membership ).'-features'; ?>
                                    @include($file_name)
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12" style="border:none;">
                                    <div class="membership-columns">
                                        <h2 class="tab-subtitle">TPO Marketing </h2>
                                        <div class="row">
                                            <div class="col-lg-8 col-xs-12">
                                                <form id="tpo_marketing_login_frm">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn theme-btn">Go To TPO MARKETING</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                        
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="2m">
                        <div class="membership-info tab-membership">

                            <?php foreach ($pkgs as $k => $pkg): ?>
                                <?php $file_name = 'member/packages/membership/parts/.' . strtolower( $pkg->name ); ?>
                                @include($file_name)
                            <?php endforeach ?>

                            <form class="" id="fund_package_form">
                                {{ csrf_field() }}
                                <input type="hidden" id="membership_frm_pid" name="pid" value="<?php echo $pkg->id ?>">
                            </form>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="3m">
                        <div class="membership-wrap">
                            <div class="tab-title">
                                <h2>Your Referral Credits</h2>
                            </div>
                            <div class="fund-summary">
                                <h3>Total Referral Credits: <span class="referral_credit_total">$<?php echo LpjHelpers::amt2($refCredTotal); ?></span></h3>
                            </div>
                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Referral</th>
                                            <th>Membership</th>
                                            <th>Tier</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="referral_credits_wrap">
                                        <tr>
                                            <td colspan="100">No referral Credits Earned</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="refcred_pagination_wrap"></div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane fade" id="4m">
                        <div class="membership-wrap">
                            <div class="tab-title">
                                <h2>Power Bonus</h2>
                            </div>
                            <div class="fund-summary">
                                <h3>Total Power Bonus: <span class="power_bonus_total">$0.00</span></h3>
                            </div>
                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="power_bonus_list_wrap">
                                        <tr>
                                            <td>$1,500.00</td>
                                            <td>Aug. 24 2017</td>
                                            <td>withdrawn</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="power_bonus_pagination_wrap"></div>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->

<div id="payment_confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Confirm Payment</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap payment_confirmation_content_wrap">
                    <div class="ajax_target">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="invoice_wrap">
                                    <p>
                                        <span>Membership Level: </span> 
                                        <strong class="package_name2"> </strong>
                                    </p>
                                    <p>
                                        <span>Membership Fee: </span>
                                        $<strong class="fund_amount2"> </strong>
                                    </p>
                                    <p>
                                        <a href="#" class="btn theme-btn btn-lg pay_now_btn">Pay Now</a>
                                        <a href="#" class="btn theme-btn btn-lg cancel_payment_btn">Cancel</a>
                                    </p>
                                </div>
                                
                                <div class="success_payment_wrap">
                                    <div class="callout callout-success">
                                        <h2>Payment has been completed.</h2>
                                        <p>Please refresh this page.</p>
                                    </div>
                                    <p>
                                        <a href="" class="btn theme-btn btn-sm">Refresh</a>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default close_payment_modal" data-dismiss="modal">Close</button>
            </div> -->
        </div>  
    </div>
</div>


<div style="display: none;">
    <form id="get_ref_credit_frm">
        {{ csrf_field() }}
        <input type="text" id="rc_package_cat" name="packageCat" value="membership">
        <input type="text" id="rc_qpage" name="qpage" value="1">
        <input type="text" id="rc_page" name="page" value="1">
        <input type="text" id="rc_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="get_ref_credit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="rcp_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="rcp_package_cat" name="packageCat" value="membership">
        <input type="text" id="rcp_refcred_id" name="refCredId" value="0">
    </form>
</div>


<div style="display: none;">
    <form id="get_power_bunos_frm">
        {{ csrf_field() }}
        <input type="text" id="pb_qpage" name="qpage" value="1">
        <input type="text" id="pb_page" name="page" value="1">
        <input type="text" id="pb_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="power_bunos_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="pbp_bonus_id" name="powBonusId" value="0">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var orderUrl = "{{ route('member.membershipProcessOrder') }}";
    var bankUrl = "{{ route('member.tpoBank') }}";
    var refCredUrl = "{{ route('member.getReferralCredits') }}";
    var refCredPushtobankUrl = "{{ route('member.refCredPushToBank') }}";
    var powerBonusUrl = "{{ route('member.getMembershipPowerBonus') }}";
    var powBonusPushtobankUrl = "{{ route('member.powerBonusPushToBank') }}";

    jQuery('.join_now_btn').click(function(){
        jQuery('.package_name2').html( jQuery(this).data('name') );
        jQuery('.fund_amount2').html( jQuery(this).data('amount') );

        var msg = '';

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            jQuery('#payment_confirmation_modal').modal({
                backdrop: 'static',
                keyboard: false
            });

            jQuery('.invoice_wrap').show();
            jQuery('.success_payment_wrap').hide();

            jQuery('#membership_frm_pid').val( jQuery(this).data('pkgid') );
        }

        return false;
    });

    /* cancel payment */
    jQuery(document).on( 'click', '.cancel_payment_btn', function(){
        jQuery('#payment_confirmation_modal').modal('hide');
        return false;
    });

    jQuery(document).on( 'click', '.pay_now_btn', function(){
        jQuery('#fund_package_form').submit();
        
        return false;
    });

    /* submit payment */
    jQuery('#fund_package_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        var input_data = jQuery(this).serialize();
        var ajax_target_wrap = jQuery(".payment_confirmation_content_wrap");
        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: orderUrl,
            dataType: 'json',
            data: input_data,
            success: function( data ){
                if ( data.msg2 == 'order_queued' ) {
                    /* show queued message */

                }else if ( data.msg2 == 'insuficient_funds' ) {
                    var msg_arr = ['Order failed. You dont have enough bank fund. <a class="btn btn-xs btn-success" href="'+bankUrl+'">Add Balance</a>'];
                    show_page_ajax_message( msg_arr, 'danger');
                    hide_global_ajax_loader();

                    jQuery('#payment_confirmation_modal').modal('hide');

                }else{
                    if ( data.isError ) {
                        show_page_ajax_message( data.msg, 'danger');

                    }else{
                        //show_page_ajax_message( data.msg, 'success');
                        jQuery('.invoice_wrap').hide();
                        jQuery('.success_payment_wrap').show();
                    }
                        
                    hide_global_ajax_loader();
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });


        return false;
    });


    /* referral credits */
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".referral_credits_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.refcred_pagination_wrap').html(ret.paginationHtml);
                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    /*
    referral credits pagination
    */
    jQuery(document).on( 'click', '.refcred_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#rc_qpage').val( qpage );
            jQuery('#rc_page').val( page );
            jQuery('#rc_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });

    /*
    referral credits push to bank
    */
    jQuery(document).on( 'click', '.pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var commId = jQuery(this).data('commid');

        jQuery('#rcp_refcred_id').val( commId );

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.referral_credit_total').text('$'+rdata.rcTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });




    /* ==============================================
    get power bonus list
    ================================================= */
    get_power_bonus();
    function get_power_bonus(){
        var ajax_target_wrap = jQuery(".power_bonus_list_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_power_bunos_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: powerBonusUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.power_bonus_pagination_wrap').html(ret.paginationHtml);
                jQuery('.power_bonus_total').html('$'+ret.powBonusTotal);
                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    /*
    power bonus pagination
    */
    jQuery(document).on( 'click', '.power_bonus_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#pb_qpage').val( qpage );
            jQuery('#pb_page').val( page );
            jQuery('#pb_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_power_bonus();
        }

        return false;
    });

    /* ==============================================
    power bonus push to bank
    ============================================== */
    jQuery(document).on( 'click', '.pushtobank_powbonus_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var bonusId = jQuery(this).data('bonusid');

        jQuery('#pbp_bonus_id').val( bonusId );

        show_global_ajax_loader();

        var form_elem = jQuery('#power_bunos_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: powBonusPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.referral_credit_total').text('$'+rdata.rcTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    /* ==============================================
    TPO Marketing
    ============================================== */
    var tpoMarketingLoginUrl = "{{ route('member.tpoMarketingLogin') }}";
    jQuery(document).on( 'submit', '#tpo_marketing_login_frm', function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: tpoMarketingLoginUrl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');
                    hide_global_ajax_loader();

                }else{
                    show_page_ajax_message( result.msg, 'success');
                    window.location.replace(result.redirectUrl);
                }

            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

</script>
@stop