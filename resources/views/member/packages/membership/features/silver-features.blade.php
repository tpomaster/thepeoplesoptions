<div class="silver-info">
	<table class="table table-striped table-hover">
		<tr>
			<td>Withdrawals from your account</td>
			<td>Two per month</td>
		</tr>
		<tr>
			<td>Withdrawal Limit</td>
			<td>Up to $2000 every month</td>
		</tr>
		<tr>
			<td>Withdrawal Fee</td>
			<td>at 4%</td>
		</tr>
		<tr>
			<td>Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</td>
			<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
		</tr>
		<tr>
			<td>Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</td>
			<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
		</tr>
		<tr>
			<td>Management Fee</td>
			<td>Up to 35%</td>
		</tr>
		<tr>
			<td>Free Bitshares Registration</td>
			<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
		</tr>
		<tr>
			<td>Access to our CMS</td>
			<td>Partial Access</td>
		</tr>
		<tr>
			<td>Special Packages</td>
			<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
		</tr>
		<tr>
			<td>Bonuses</td>
			<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
		</tr>
	</table>
</div>