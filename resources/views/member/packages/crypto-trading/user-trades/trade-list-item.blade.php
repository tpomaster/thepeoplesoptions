<?php if ( $userTrades ): ?>
    <?php foreach ($userTrades as $tr){?>
        <?php $details = json_decode($tr->details); ?>
        <tr class="profit_item_<?php echo $tr->id; ?>">
            <td>$<?php echo LpjHelpers::amt2($tr->trade_amount); ?></td>
            <td><?php echo $details->num_of_trades; ?></td>
            <td>$<?php echo LpjHelpers::amt2($tr->trade_earning); ?></td>
            <td>
                <?php if ( $tr->management_fee >= 0.01 ): ?>
                    $<?php echo LpjHelpers::amt2($tr->management_fee); ?>
                <?php else: ?>
                    $<?php echo LpjHelpers::amt4($tr->management_fee); ?>
                <?php endif; ?>
            </td>
            <td>$<?php echo LpjHelpers::amt2($tr->user_profit); ?></td>
            <td><?php echo LpjHelpers::nice_date($tr->date); ?></td>
            <td>
                <?php if ( $tr->trade_earning < 0 && $tr->status != 'cappedoff' ): ?>      
                    <a class="btn theme-btn usertrade_capoff_single_btn" data-pid="<?php echo $tr->package_id ?>" data-trid="<?php echo $tr->id ?>">Cap-Off</a>
                    
                <?php elseif ( $tr->status == 'completed'): ?>
                    <a class="btn theme-btn usertrade_pushtobank_btn" data-trid="<?php echo $tr->id ?>">Push To Bank</a>

                <?php else: ?>
                    <?php echo $tr->status; ?>
                <?php endif ?>
            </td>
        </tr>
    <?php } ?>

<?php else: ?>
    <tr>
        <td colspan="100">
            None
        </td>
    </tr>
<?php endif ?>
    
