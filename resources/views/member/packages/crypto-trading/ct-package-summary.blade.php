<?php foreach ($packages as $pkg){?>

    <?php if ( $pkg->status == 'available' ): ?>
        <div class="package-wrap">
            <div class="row">
                <div class="col-lg-4 col-xs-12 package-info">
                    <h3><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?> </h3>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="info-title">Package Status:</span>
                            <span class="pull-right package-status status-<?php echo $pkg->status; ?>"><?php echo strtoupper( $pkg->status ); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Risk Profile:</span>
                            <span class="pull-right tpo-warning riskp-<?php echo $pkg->risk_profile; ?>"><?php echo strtoupper( $pkg->risk_profile ); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Total amount of package:</span>
                            <span class="pull-right">$<?php echo LpjHelpers::amt2( $pkg->amount ); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Funded Amount:</span>
                            <span class="pull-right funded">$<?php echo LpjHelpers::amt2( $pkg->funded_status ); ?></span>
                        </li>
                    </ul>
                    <div class="package-btns">
                        <a class="btn theme-btn clearfix" href="{{ route('member.crypto-trading-package-details', ['packageId' => $pkg->id]) }}">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 chart-box">
                    <?php 
                    $funded_percent = ( (float)$pkg->funded_status / (float)$pkg->amount )*100;
                    $funded_percent = LpjHelpers::amt2($funded_percent);
                    //$funded_percent = ($total_fund/$package_amount)*100;
                    ?>
                    <h3>Funded: <span class="fund-percent">$<?php echo LpjHelpers::amt2($pkg->funded_status); ?> ( <?php echo $funded_percent; ?>% )</span></h3>
                    <div id="progress-<?php echo $pkg->id ?>" class="gauge-chart"></div>
                </div>
                <div class="col-lg-2 col-xs-12 traiders-info">
                    <img src="{{asset('images/memberPic2.png')}}" class="trade-avatar" alt="">
                    <h3><span class="crypto">CRYPTO</span><span class="package-trading">TRADING</span></h3>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                var pkgid = <?php echo $pkg->id; ?>;
                var chart_container_id = 'progress-<?php echo $pkg->id; ?>';

                get_package_guage( <?php echo $funded_percent; ?>, chart_container_id);

                jQuery('.new_earning_btn').click(function(){
                    jQuery(this).hide();
                });
            });
        </script>
    <?php endif ?>
    
    <?php if ( $pkg->status != 'available' ): ?>
        <div class="package-wrap pkg-<?php echo $pkg->status; ?>">
            <div class="row">
                <div class="col-lg-4 col-xs-12 package-info">
                    <h3><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?> </h3>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="info-title">Package Status:</span>
                            <span class="pull-right package-status status-<?php echo $pkg->status; ?>"><?php echo strtoupper( $pkg->status ); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Risk Profile:</span>
                            <span class="pull-right tpo-warning riskp-<?php echo $pkg->risk_profile; ?>"><?php echo strtoupper( $pkg->risk_profile ); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Total amount of package:</span>
                            <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->amount); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Funded Amount:</span>
                            <span class="pull-right funded">$<?php echo LpjHelpers::amt2($pkg->funded_status); ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Posted Trades Count:</span>
                            <span class="pull-right funded"><?php echo $pkg->posted_trades; ?></span>
                        </li>
                    </ul>
                    <div class="package-btns">
                        <?php if ( isset($negProfitSum[$pkg->id]) && $negProfitSum[$pkg->id] != false ): ?>
                            <a class="btn theme-btn clearfix unwithdran_profit_btn" data-pid="<?php echo $pkg->id; ?>" href="#">
                                <i class="fa fa-usd"></i>
                                <?php echo  LpjHelpers::amt2($negProfitSum[$pkg->id]); ?>
                            </a>
                        <?php endif ?>

                        <?php if ( isset($uwProfitSum[$pkg->id]) && $uwProfitSum[$pkg->id] != false && $uwProfitSum[$pkg->id] != 0 ): ?>
                            <a class="btn theme-btn clearfix unwithdran_profit_btn" data-pid="<?php echo $pkg->id; ?>" href="#">
                                <i class="fa fa-usd"></i>
                                <?php echo  LpjHelpers::amt2($uwProfitSum[$pkg->id]); ?>
                            </a>
                        <?php endif ?>
                        
                        <a class="btn theme-btn clearfix" href="{{ route('member.crypto-trading-package-details', ['packageId' => $pkg->id]) }}">
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12 chart-box">
                    <!-- <h3>Package Earnings Chart:</h3> -->
                    <div id="pkg-chart-<?php echo $pkg->id; ?>" class="line-graph"></div>
                </div>
                <div class="col-lg-2 col-xs-12 traiders-info">
                    <img src="{{asset('images/memberPic2.png')}}" class="trade-avatar" alt="">
                    <h3><span class="crypto">CRYPTO</span><span class="package-trading">TRADING</span></h3>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                var pkgid = <?php echo $pkg->id; ?>;
                var chart_container_id = 'pkg-chart-<?php echo $pkg->id; ?>';

                console.log(pkgid);

                // get_package_chart( pkgid, chart_container_id);

                $.getJSON('{{ route("member.getTradingPkgChartData") }}?pkgchartdata='+pkgid, function (data) {
                      // Create the chart 1
                      Highcharts.stockChart(chart_container_id, {

                          chart: {
                              height: 310
                          },

                          rangeSelector: {
                              selected: 1
                          },

                          title: {
                              text: 'Daily Earnings Chart'
                          },

                          series: [{
                              name: 'Earnings(%)',
                              data: data,
                              tooltip: {
                                  valueDecimals: 2
                              }
                          }]
                      });
                  });

                jQuery('.new_earning_btn').click(function(){
                    jQuery(this).hide();
                });
            });
        </script>
    <?php endif ?>
<?php } ?>
