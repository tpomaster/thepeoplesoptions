<div class="package-details-box">
    <div class="x_panel tile">
        <h2 class="panel-title"><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?></h2>

        <ul class="list-group">
            <li class="list-group-item">
                <span class="info-header">Package Status:</span>
                <span class="pull-right package-status fund-<?php echo $pkg->status; ?>"><?php echo $pkg->status; ?></span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Funded:</span>
                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->funded_amount); ?> ( <?php echo LpjHelpers::amt2($pkg->funded_percent); ?>% )</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Available for Funding:</span>
                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->available_for_funding); ?></span>
            </li>
        </ul>

        <?php if ( $myFund ): ?>
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="info-header">Status:</span>
                    <span class="pull-right package-status fund-<?php echo $myFund->exchange_status; ?>"><?php echo $myFund->exchange_status; ?></span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Your Fund:</span>
                    <span class="pull-right">$<?php echo LpjHelpers::amt2($myFund->amount); ?></span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Start Date:</span>
                    <span class="pull-right">Pending</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">End Date:</span>
                    <span class="pull-right">Pending</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">No. of profits posted:</span>
                    <span class="pull-right">0</span>
                </li>
            </ul>

            <ul class="list-group">
                <li class="list-group-item">
                    <span class="info-header">Profit Totals:</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Profit:</span>
                    <span class="pull-right">$0.00</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Management Fee:</span>
                    <span class="pull-right">$0.00</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Net Earning:</span>
                    <span class="pull-right">$0.00</span>
                </li>
            </ul>
        <?php endif ?>
        
    </div>
</div>