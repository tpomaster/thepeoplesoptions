<div class="package-details-box">
    <div class="x_panel tile">
        <h2 class="panel-title"><img src="{{asset('images/package-avatar.png')}}" class="package-avatar" alt=""> <?php echo $pkg->name; ?></h2>

        <ul class="list-group">
            <li class="list-group-item">
                <span class="info-header">Package Status:</span>
                <span class="pull-right package-status fund-<?php echo $pkg->status; ?>"><?php echo $pkg->status; ?></span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Funded:</span>
                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->funded_amount); ?> ( <?php echo LpjHelpers::amt2($pkg->funded_percent); ?>% )</span>
            </li>
            <li class="list-group-item">
                <span class="info-title">Available for Funding:</span>
                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->available_for_funding); ?></span>
            </li>
        </ul>

        <?php if ( $myFund ): ?>
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="info-header">Status:</span>
                    <span class="pull-right package-status fund-<?php echo $myFund->exchange_status; ?>"><?php echo $myFund->exchange_status; ?></span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Your Fund:</span>
                    <span class="pull-right">$<?php echo LpjHelpers::amt2($myFund->amount); ?></span>
                </li>
                <?php if ( $myFund->exchange_status == 'closed' ): ?>
                    <?php if ( $myFund->status == 'completed' ): ?>
                        <li class="list-group-item">
                            <a class="btn btn-sm theme-btn pull-right push_cryptoexchange_fund_to_bank" data-fundid="<?php echo $myFund->id; ?>" href="#">Push to TPO Bank</a>
                            <span class="clearing clear clearfix"></span>
                        </li>
                    <?php endif ?>
                        
                    <?php if ( $myFund->status == 'withdrawn' ): ?>
                        <li class="list-group-item">
                            <span class="pull-right"><i>Fund pushed to bank</i></span>
                            <span class="clearing clear clearfix"></span>
                        </li>
                    <?php endif ?>
                <?php endif; ?>

                <li class="list-group-item">
                    <span class="info-title">Start Date:</span>
                    <span class="pull-right">
                        <?php echo $tradeStartDate; ?>
                    </span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">End Date:</span>
                    <span class="pull-right">
                        <?php echo $tradeEndDate; ?>
                    </span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">No. of trades posted:</span>
                    <span class="pull-right"><?php echo $pkg->posted_profits; ?></span>
                </li>
            </ul>

            <ul class="list-group">
                <li class="list-group-item">
                    <span class="info-header">Profit Totals:</span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Profit</span>
                    <span class="pull-right">$<?php echo LpjHelpers::amt2($grossEarning); ?></span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Management Fee:</span>
                    <span class="pull-right">$<?php echo LpjHelpers::amt2($totalManagementFee); ?></span>
                </li>
                <li class="list-group-item">
                    <span class="info-title">Net Earning:</span>
                    <span class="pull-right">$<?php echo LpjHelpers::amt2($netEarning); ?></span>
                </li>
            </ul>
        <?php endif ?>
        
    </div>
</div>