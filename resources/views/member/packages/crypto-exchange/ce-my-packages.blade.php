@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/cryptotrading-packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1><?php echo $pageTitle2; ?> </h1>
    </div>
            
    <div class="row">
        <div class="col-md-12 col-s-12 col-xs-12">
            <div class="packages-container">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a class="my-funded-tab" href="#funded-active" data-status="funded-active" data-toggle="tab">Funded Active</a>
                    </li>
                    <li>
                        <a class="my-funded-tab" href="#funded-active" data-status="available" data-toggle="tab">Available</a>
                    </li>
                    <li>
                        <a class="my-funded-tab" href="#funded-active" data-status="closed" data-toggle="tab">Closed</a>
                    </li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane fade in active" id="funded-active">
                            <div class="active_packages_list_wrap packages_list_wrap"></div>
                            <div class="active_pagination_wrap"></div>

                            <span class="clearing"></span>
                        </div>
                    </div>
                                            
                    <div class="tab-pane fade" id="available-package">
                        <div class="available_packages_list_wrap packages_list_wrap"></div>
                        <div class="available_pagination_wrap"></div>

                        <span class="clearing"></span>
                    </div>

                    <div class="tab-pane fade" id="closed-package">
                        <div class="closed_packages_list_wrap packages_list_wrap"></div>
                        <div class="closed_pagination_wrap"></div>

                        <span class="clearing"></span>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- end page content -->

<div style="display: none;">
    <form id="cancel_addfund_frm">
        {{ csrf_field() }}
        <input type="text" id="cnl_trcode_inp" name="cnl" value="">
    </form>

    <form id="filter_frm">
        {{ csrf_field() }}
        <input type="text" id="f_status" name="status" value="<?php echo $status; ?>">
        <input type="text" id="f_mypackages" name="myPackages" value="<?php echo $myPackages; ?>">
        <input type="text" id="f_qpage" name="qpage" value="1">
        <input type="text" id="f_page" name="page" value="1">
        <input type="text" id="f_qlimit" name="qlimit" value="10">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection


@section('pagespecificscripts')
    <script src="https://code.highcharts.com/stock/highstock.js"></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    
    <script src="{{ asset('js/member/helper.js') }}"> </script>

    <script type="text/javascript">
        var ajaxurl = "{{ route('member.addFund') }}";
        var ajaxCancelUrl = "{{ route('member.cancelAddFund') }}";
        var ajaxPackagesListUrl = "{{ route('member.cryptoExchangePackagesListAjax') }}";


        jQuery(document).on( 'click', '.my-funded-tab', function(){
            var pStatus = jQuery(this).data('status');
            jQuery('#f_status').val(pStatus);

            get_packages();
        });

        get_packages();
        /* ========================================
        packages
        ======================================== */
        function get_packages(){
            var ajax_target_wrap = jQuery(".packages_list_wrap");

            show_global_ajax_loader();

            var form_elem = jQuery('#filter_frm');
            var input_data = form_elem.serialize();

            jQuery.ajax({
                type: "POST",
                url: ajaxPackagesListUrl,
                dataType: 'json',
                data: input_data,
                success: function(ret){
                    jQuery( ajax_target_wrap ).html(ret.returnHTML);
                    jQuery('.pagination_wrap').html(ret.paginationHtml);
                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });
        }

        /* ========================================
        packages pagination
        ======================================== */
        jQuery(document).on( 'click', '.pagination a', function(){
            var thisElem = jQuery(this);
            var a_link = jQuery(this).attr( 'href' );

            if ( a_link != '#' ) {
                var qpage = lpj_parse_url( a_link, 'qpage' );
                var page = lpj_parse_url( a_link, 'page' );
                var qlimit = lpj_parse_url( a_link, 'qlimit' );
                var sortBy = lpj_parse_url( a_link, 'sortBy' );
                var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

                jQuery('#f_qpage').val( qpage );
                jQuery('#f_page').val( page );
                jQuery('#f_qlimit').val( qlimit );
                // jQuery('#f_sortBy').val( sortBy );
                // jQuery('#f_sortOrder').val( sortOrder );

                get_packages();
            }

            return false;
        });

        

        function get_package_chart( pkgid, chart_container ){

            $.getJSON('https://thepeoplesoptions.com/?pkgchartdata='+pkgid+'&callback=?', function (data) {
                Highcharts.stockChart(chart_container, {
                    rangeSelector: {
                        enabled: false
                    },
                    scrollbar: {
                        enabled: false
                    },
                    navigator: {
                        enabled: false
                    },
                    
                    // title: {
                    //     text: 'Package Earnings Chart'
                    // },
                    subtitle: {
                        text: 'Package Earnings Chart' 
                    },
                    series: [{
                        name: 'Earning',
                        data: data,
                        tooltip: {
                            valueDecimals: 2
                        }
                    }]
                });
            });
        }

        function get_package_guage( funded_percentage, chart_container_id ){

            // gauge graph
            var gaugeOptions = {

                chart: {
                    type: 'solidgauge'
                },

                title: null,

                pane: {
                    center: ['50%', '85%'],
                    size: '140%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#fff',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },

                // the value axis
                yAxis: {
                    stops: [
                        [0.1, '#55BF3B'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#DF5353'] // red
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickAmount: 2,
                    title: {
                        y: -70
                    },
                    labels: {
                        y: 16
                    }
                },

                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 5,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                }
            };

            // The speed gauge
            var chartSpeed = Highcharts.chart(chart_container_id, Highcharts.merge(gaugeOptions, {
                yAxis: {
                    min: 0,
                    max: 100,
                    // title: {
                    //     text: 'Funded: '
                    // }
                },

                credits: {
                    enabled: false
                },

                series: [{
                    name: 'Progress',
                    data: [funded_percentage],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span>'
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    }
                }]

            }));
        }
    </script>
@stop