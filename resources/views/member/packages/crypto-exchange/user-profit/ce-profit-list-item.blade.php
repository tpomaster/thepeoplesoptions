<?php if ( $userProfit ): ?>
    <?php foreach ($userProfit as $tr){?>
        <tr class="profit_item_<?php echo $tr->id; ?>">
            <td>$<?php echo LpjHelpers::amt2($tr->investment); ?></td>
            <td><?php echo $tr->id; ?></td>
            <td>$<?php echo LpjHelpers::amt2($tr->gross_profit); ?></td>
            <td>
                <?php if ( $tr->management_fee >= 0.01 ): ?>
                    $<?php echo LpjHelpers::amt2($tr->management_fee); ?>
                <?php else: ?>
                    $<?php echo LpjHelpers::amt4($tr->management_fee); ?>
                <?php endif; ?>
            </td>
            <td>$<?php echo LpjHelpers::amt2($tr->net_profit); ?></td>
            <td><?php echo LpjHelpers::nice_date($tr->date); ?></td>
            <td>
                <?php if ( $tr->gross_profit < 0 ): ?>      
                    <?php echo $tr->status; ?>
                    
                <?php elseif ( $tr->status == 'completed'): ?>
                    <a class="btn theme-btn userprofit_pushtobank_btn" data-trid="<?php echo $tr->id ?>">Push To Bank</a>

                <?php else: ?>
                    <?php echo $tr->status; ?>
                <?php endif ?>
            </td>
        </tr>
    <?php } ?>

<?php else: ?>
    <tr>
        <td colspan="100">
            None
        </td>
    </tr>
<?php endif ?>
    
