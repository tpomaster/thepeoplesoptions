@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/cryptotrading-packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">

    <div class="row package-details">
        <div class="col-lg-12 col-xs-12">
            
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="disclaimer">
                        <div class="x_panel tile" style="text-align: center;">
                             <h1>Package Not Found</h1>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- end page content -->

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>
@stop