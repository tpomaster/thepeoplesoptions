@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/cryptotrading-packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Crypto Trading Package Details</h1>
    </div>

    <div class="row package-details">
        <div class="col-lg-3 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <?php if ( $pkg->status == 'available' ): ?>
                        @include('member/packages/crypto-mining/detail-parts/cm-stat-available')

                    <?php else: ?>
                        @include('member/packages/crypto-mining/detail-parts/cm-stat-active')
                    <?php endif ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="disclaimer">
                        <div class="x_panel tile">
                            <h2 class="panel-title">Disclaimer</h2>
                            <ul>
                                <li>Losing money is inevitable, Members should not purchase packages with funds they cannot afford to lose.</li>
                                <li>The daily earnings are calculated and transferred to the member’s account based on the membership level, funded amount, and package option chosen.</li>
                                <li>Daily earnings will vary based on packages and terms for packages.</li>
                                <li>Earnings/losses accrue 7 days a week, including weekends and holidays.</li>
                                <li>Earnings are deposited in your TPO account. Losses are taken from the funded amount. You have 24 hours/1 week to replace the loss to continue trading at the original funded amount.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-xs-12">
            <div class="single-package-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a  href="#1b" data-toggle="tab">Package Info</a></li>
                    <li><a href="#2b" data-toggle="tab">Fund This Package</a></li>
                    <li><a href="#3b" data-toggle="tab">Referral Credits</a></li>
                    <li><a href="#4b" data-toggle="tab">Mining Profit</a></li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane fade in active" id="1b">
                        <div class="package-wrap2">
                            <h2 class="tab-title">Package Information</h2>
                            <div class="package-summary">
                                <div class="row">
                                    <div class="col-lg-6 col-xs-12">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <span class="info-title">Total amount of package:</span>
                                                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->amount); ?></span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Duration:</span>
                                                <span class="pull-right"><?php echo $pkg->duration; ?> Days</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Management Fee:</span>
                                                <span class="pull-right"><?php echo $pkg->management_fee ?> %</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Risk Profile:</span>
                                                <span class="pull-right riskp-<?php echo $pkg->risk_profile ?>"><?php echo $pkg->risk_profile ?></span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Profit Posting Schedule</span>
                                                <span class="pull-right"><?php echo $pkg->profit_posting_freq ?></span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="info-title">Tier Levels:</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row tier-table">
                                    <div class="col-md-12 col-s-12 col-xs-12">
                                        <div class="table-holder">
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Tier</th>
                                                        <th>Referral Credit</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($pkg->tier_config as $k => $t): ?>
                                                        <tr>
                                                            <td><?php echo $t['tier_level']; ?></td>
                                                            <td><?php echo $t['referral_credit'] ?>%</td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <button type="button" class="btn theme-btn full" data-toggle="modal" data-target="#terms">View Terms and Conditions</button>
                                    <!-- terms and condition -->
                                    @include('member/packages/crypto-mining/detail-parts/cm-terms')
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="2b">
                        <div class="package-wrap2">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h2 class="tab-title">Fund this package</h2>
                                    <?php if( $pkg->for_membership == 1 ): ?>
                                        <div class="fund-summary">
                                            <h4>You can only fund this by joining our Membership Packages.</h4>
                                        </div>

                                    <?php elseif ( $pkg->available_for_funding > 0 && $pkg->status == 'available' ): ?>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <span class="info-title">Available for Funding:</span>
                                                <span class="pull-right">$<?php echo LpjHelpers::amt2($pkg->available_for_funding); ?></span>
                                            </li>
                                        </ul>

                                        <input type="hidden" id="package_name" name="packageName" value="<?php echo $pkg->name ?>">
                                        <input type="hidden" id="min_amount" name="minAmount" value="10">

                                        <form class="" id="fund_package_form">
                                            {{ csrf_field() }}
                                            <input type="hidden" id="pid" name="pid" value="<?php echo $pkg->id ?>">

                                            <div class="form-group ">
                                                <label class="control-label ">Enter Fund Amount:</label>
                                                <input id="fund_amount" class="form-control lpj_num_only" name="fundAmount" value="10" type="text">
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label ">Payment Option:</label>
                                                <input id="payment_gateway2" class="form-control" name="paymentGateway" value="TPO Bank" disabled="disabled" type="text">
                                                <div class>
                                                    <p>
                                                    Your TPO Bank Balance:
                                                    <strong class="tpo_bank_balance" data-tpobank="<?php echo $userBank->available_bal; ?>">$<?php echo LpjHelpers::amt2($userBank->available_bal); ?></strong>
                                                    <a class="btn theme-btn btn-sm" target="_blank" href="{{ route('member.tpoBank') }}">Add Balance?</a>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="agree_with_terms" type="checkbox">
                                                        I Agree to the
                                                        <a class="agree_with_terms_btn" href="">Terms and Conditions</a>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <a href="#" class="btn theme-btn btn-lg confirm_payment_btn">Purchase</a>
                                            </div>
                                        </form>

                                    <?php else: ?>
                                        <div class="fund-summary">
                                            <h4>Package is now fully funded</h4>
                                        </div>
                                    <?php endif ?>
                                        
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="funded-summary">
                                        <h2 class="tab-title">Your Package Fund</h2>
                                        <?php if ( $myFund): ?>
                                            <div class="fund-summary">
                                                <h4>Your Total Fund: <span>$<?php echo LpjHelpers::amt2($myFund->max_amount); ?></span></h4>
                                                <h5>Mining Amount: <span>$<?php echo LpjHelpers::amt2($myFund->amount); ?></span></h5>
                                            </div>
                                            <h6>Breakdown:</h6>
                                            <div class="table-holder">
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Amount</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($myFundBreakdown as $k => $fund): ?>
                                                            <tr>
                                                                <td>$<?php echo LpjHelpers::amt2($fund->amount); ?></td>
                                                                <td><?php echo LpjHelpers::nice_date($myFund->created_at); ?></td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php else: ?>
                                            <p>
                                                None
                                            </p>
                                        <?php endif ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="3b">
                        <div class="package-wrap2">
                            <h2 class="tab-title">Referral Credits</h2>
                            <div class="fund-summary">
                                <h3>Total Referral Credits: <span class="referral_credit_total">$0.00</span></h3>
                            </div>
                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Package</th>
                                            <th>Referral</th>
                                            <th>Tier</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="referral_credits_wrap">
                                        <tr>
                                            <td colspan="100">No referral Credits Earned</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="refcred_pagination_wrap"></div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="4b">
                        <div class="package-wrap">
                            <h2 class="tab-title">Mining Profit</h2>
                            <div class="fund-summary">
                                <h3>Total NET Earning: <span class="user_profit_total">$0.00</span></h3>
                                <h5>Total Withdrawn Earnings: <span class="withdrawn_user_profit_total">$0.00</span></h5>
                                <h5>Total Unwithdrawn Earning: <span class="unwithdrawn_user_profit_total">$0.00</span></h5>
                            </div>

                            <div class="table-holder">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Mining Amount</th>
                                            <th>Mining Profit</th>
                                            <th>Management Fee (<?php echo $managementFee ?>%)</th>
                                            <th>Net Earning</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="user_profit_wrap">
                                        <tr>
                                            <td colspan="100">No Mining Profits</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="user_profit_pagination_wrap"></div>
                            <span class="clearfix"></span>
                            <div class="row" style="display: none;">
                                <div class="col-md-3 col-xs-12">
                                    <a class="btn theme-btn">Refresh List</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->

<div id="payment_confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Confirm Payment</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap payment_confirmation_content_wrap">
                    <div class="ajax_target">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="invoice_wrap">
                                    <p>
                                        <span>Package Name: </span> 
                                        <strong class="package_name2"> </strong>
                                    </p>
                                    <p>
                                        <span>Amount to Fund: </span>
                                        $<strong class="fund_amount2"> </strong>
                                    </p>
                                    <p>
                                        <a href="#" class="btn theme-btn btn-lg pay_now_btn">Pay Now</a>
                                        <a href="#" class="btn theme-btn btn-lg cancel_payment_btn">Cancel</a>
                                    </p>
                                </div>
                                
                                <div class="success_payment_wrap">
                                    <div class="callout callout-success">
                                        <h2>Payment has been completed.</h2>
                                        <p>Please refresh this page.</p>
                                    </div>
                                    <p>
                                        <a href="" class="btn theme-btn btn-sm">Refresh</a>
                                    </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default close_payment_modal" data-dismiss="modal">Close</button>
            </div> -->
        </div>  
    </div>
</div>

<div style="display: none;">
    <form id="get_ref_credit_frm">
        {{ csrf_field() }}
        <input type="text" id="rc_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="rc_package_cat" name="packageCat" value="cryptomining">
        <input type="text" id="rc_qpage" name="qpage" value="1">
        <input type="text" id="rc_page" name="page" value="1">
        <input type="text" id="rc_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="get_ref_credit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="rcp_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="rcp_package_cat" name="packageCat" value="cryptomining">
        <input type="text" id="rcp_refcred_id" name="refCredId" value="0">
    </form>
</div>

<div style="display: none;">
    <form id="get_user_profit_frm">
        {{ csrf_field() }}
        <input type="text" id="ut_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="ut_fund_id" name="fundId" value="0">
        <input type="text" id="ut_qpage" name="qpage" value="1">
        <input type="text" id="ut_page" name="page" value="1">
        <input type="text" id="ut_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="user_profit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="utp_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="utp_profit_id" name="profitId" value="0">
    </form>
</div>

<div style="display: none;">
    <form id="push_pkg_fund_frm">
        {{ csrf_field() }}
        <input type="text" id="pf_package_id" name="packageId" value="<?php echo $pkg->id; ?>">
        <input type="text" id="pf_fund_id" name="fundId" value="">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var orderUrl = "{{ route('member.cryptoMiningProcessOrder') }}";
    var bankUrl = "{{ route('member.tpoBank') }}";
    var refCredUrl = "{{ route('member.getReferralCredits') }}";
    var refCredPushtobankUrl = "{{ route('member.refCredPushToBank') }}";
    var userProfitUrl = "{{ route('member.cMiningGetUserProfits') }}";
    var userProfitPushtoBankUrl = "{{ route('member.cMiningUserProfitsPushToBank') }}";
    var pushPkgFundUrl = "{{ route('member.cryptoMiningPushFundToBank') }}";
    

    /* confirm payment */
    jQuery('.confirm_payment_btn').click(function(){
        jQuery('.package_name2').html( jQuery('#package_name').val() );
        jQuery('.fund_amount2').html( jQuery('#fund_amount').val() );

        var msg = validate_payment_form();

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            jQuery('#payment_confirmation_modal').modal({
                backdrop: 'static',
                keyboard: false
            });

            jQuery('.invoice_wrap').show();
            jQuery('.success_payment_wrap').hide();
        }

        return false;
    });

    /* cancel payment */
    jQuery(document).on( 'click', '.cancel_payment_btn', function(){
        jQuery('#payment_confirmation_modal').modal('hide');
        return false;
    });


    jQuery(document).on( 'click', '.pay_now_btn', function(){
        jQuery('#fund_package_form').submit();
        
        return false;
    });

    /* submit payment */
    jQuery('#fund_package_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        var msg = validate_payment_form();

        if ( msg != '' ) {
            var msg_arr = [msg];
            show_page_ajax_message( msg_arr, 'danger');

        }else{
            var input_data = jQuery(this).serialize();
            var ajax_target_wrap = jQuery(".payment_confirmation_content_wrap");
            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: orderUrl,
                dataType: 'json',
                data: input_data,
                success: function( data ){
                    if ( data.msg2 == 'order_queued' ) {
                        /* show queued message */

                    }else if ( data.msg2 == 'insuficient_funds' ) {
                        var msg_arr = ['Order failed. You dont have enough bank fund. <a class="btn btn-xs btn-success" href="'+bankUrl+'">Add Balance</a>'];
                        show_page_ajax_message( msg_arr, 'danger');
                        hide_global_ajax_loader();

                        jQuery('#payment_confirmation_modal').modal('hide');

                    }else{
                        if ( data.isError ) {
                            show_page_ajax_message( data.msg, 'danger');

                        }else{
                            //show_page_ajax_message( data.msg, 'success');
                            jQuery('.invoice_wrap').hide();
                            jQuery('.success_payment_wrap').show();
                        }
                            
                        hide_global_ajax_loader();
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        }

        return false;
    });


    function validate_payment_form(){
        var msg = '';

        var tpo_bank_balance = parseFloat( jQuery('.tpo_bank_balance').data("tpobank") );
        var fund_amount = parseFloat( jQuery('#fund_amount').val() );
        var min_amount = parseFloat( jQuery('#min_amount').val() );

        if ( jQuery('#agree_with_terms').is(":checked") === false ) {
            msg = 'You have to agree with the terms and conditions to continue';

        }else if( fund_amount > tpo_bank_balance ){
            msg = 'Insufficient Balance.';

        }
        // else if( fund_amount < min_amount ){
        //     msg = 'Minimum Fund Amount is $'+min_amount+'.';
        // }

        return msg;
    }


    /*/ ==============================================================
    //  referral credits
    // ==============================================================/*/
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".referral_credits_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.refcred_pagination_wrap').html(ret.paginationHtml);

                jQuery('.referral_credit_total').html(ret.rcTotal);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  referral credits pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.refcred_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#rc_qpage').val( qpage );
            jQuery('#rc_page').val( page );
            jQuery('#rc_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });


    /*/ ==============================================================
    //  referral credits push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var commId = jQuery(this).data('commid');

        jQuery('#rcp_refcred_id').val( commId );

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.referral_credit_total').text('$'+rdata.rcTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    /*/ ==============================================================
    //  get user profit
    // ==============================================================/*/
    get_user_profit();
    function get_user_profit(){
        show_global_ajax_loader();

        var form_elem = jQuery('#get_user_profit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery(".user_profit_wrap").html(ret.returnHTML);
                jQuery('.user_profit_pagination_wrap').html(ret.paginationHtml);

                jQuery('.user_profit_total').html('$'+ret.pkgEarningTotal);
                jQuery('.withdrawn_user_profit_total').html('$'+ret.pkgEarningTotalWithdrawn);
                jQuery('.unwithdrawn_user_profit_total').html('$'+ret.pkgEarningTotalPending);

                hide_global_ajax_loader();
            }
        });
    }


    /*/ ==============================================================
    //  user profit pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.user_profit_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#ut_qpage').val( qpage );
            jQuery('#ut_page').val( page );
            jQuery('#ut_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_user_profit();
        }

        return false;
    });


    /*/ ==============================================================
    // user profit push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.userprofit_pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var trid = jQuery(this).data('trid');

        jQuery('#utp_profit_id').val( trid );

        show_global_ajax_loader();

        var form_elem = jQuery('#user_profit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitPushtoBankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.user_profit_total').html('$'+rdata.miningProfitTotal);
                    jQuery('.withdrawn_user_profit_total').html('$'+rdata.miningProfitTotalWithdrawn);
                    jQuery('.unwithdrawn_user_profit_total').html('$'+rdata.miningProfitTotalPending);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    /*/ ==============================================================
    // push package fund
    // ==============================================================/*/
    jQuery(document).on( 'click', '.push_cryptomining_fund_to_bank', function(){
        _this = jQuery(this);

        var fundid = jQuery(this).data('fundid');

        jQuery('#pf_fund_id').val( fundid );

        var form_elem = jQuery('#push_pkg_fund_frm');
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: pushPkgFundUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );
                    _this.after('<span class="pull-right"><i>Fund pushed to bank</i></span>')
                        .remove();
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });
</script>
@stop