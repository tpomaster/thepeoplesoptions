@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/cryptotrading-packages.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1><?php echo $pageTitle2; ?> </h1>
    </div>

    <div class="packages_list_wrap"></div>
    <div class="packages_pagination_wrap">Total Count: 0</div>

    <span class="clearing"></span>
</div>
<!-- end page content -->

<div style="display: none;">
    <form id="cancel_addfund_frm">
        {{ csrf_field() }}
        <input type="text" id="cnl_trcode_inp" name="cnl" value="">
    </form>

    <form id="filter_frm">
        {{ csrf_field() }}
        <input type="text" id="f_status" name="status" value="<?php echo $status; ?>">
        <input type="text" id="f_mypackages" name="myPackages" value="<?php echo $myPackages; ?>">
        <input type="text" id="f_qpage" name="qpage" value="1">
        <input type="text" id="f_page" name="page" value="1">
        <input type="text" id="f_qlimit" name="qlimit" value="10">
    </form>
</div>

<div id="user_profit_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Unpushed Mining Profit</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap ">
                    
                    <div class="table-holder">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Your Fund</th>
                                    <th>Mining Profit</th>
                                    <th>Management Fee </th>
                                    <th>Net Earning</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody class="user_profit_wrap">
                                <tr>
                                    <td colspan="100">No Result</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="user_profit_pagination_wrap"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default close_payment_modal" data-dismiss="modal">Close</button>
            </div>
        </div>  
    </div>
</div>

<div style="display: none;">
    <form id="get_user_profit_frm">
        {{ csrf_field() }}
        <input type="text" id="ut_package_id" name="packageId" value="0">
        <input type="text" id="ut_fund_id" name="fundId" value="0">
        <input type="text" id="ut_status" name="status" value="completed">
        <input type="text" id="ut_qpage" name="qpage" value="1">
        <input type="text" id="ut_page" name="page" value="1">
        <input type="text" id="ut_qlimit" name="qlimit" value="5">
    </form>
</div>

<div style="display: none;">
    <form id="user_profit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="utp_package_id" name="packageId" value="0">
        <input type="text" id="utp_profit_id" name="profitId" value="0">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection


@section('pagespecificscripts')
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var ajaxurl = "{{ route('member.addFund') }}";
    var ajaxCancelUrl = "{{ route('member.cancelAddFund') }}";
    var ajaxPackagesListUrl = "{{ route('member.cryptoMiningPackagesListAjax') }}";

    get_packages();

    /*
    pagination
     */
    jQuery(document).on( 'click', '.pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#f_qpage').val( qpage );
            jQuery('#f_page').val( page );
            jQuery('#f_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_packages();
        }

        return false;
    });

    /* PACKAGES */
    function get_packages(){
        var ajax_target_wrap = jQuery(".packages_list_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#filter_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: ajaxPackagesListUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.packages_pagination_wrap').html(ret.paginationHtml);
                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    function get_package_chart( pkgid, chart_container ){

        $.getJSON('https://thepeoplesoptions.com/?pkgchartdata='+pkgid+'&callback=?', function (data) {
            Highcharts.stockChart(chart_container, {
                rangeSelector: {
                    enabled: false
                },
                scrollbar: {
                    enabled: false
                },
                navigator: {
                    enabled: false
                },
                
                // title: {
                //     text: 'Package Earnings Chart'
                // },
                subtitle: {
                    text: 'Package Earnings Chart' 
                },
                series: [{
                    name: 'Earning',
                    data: data,
                    tooltip: {
                        valueDecimals: 2
                    }
                }]
            });
        });
    }

    function get_package_guage( funded_percentage, chart_container_id ){

        // gauge graph
        var gaugeOptions = {

            chart: {
                type: 'solidgauge'
            },

            title: null,

            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#fff',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },

            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        // The speed gauge
        var chartSpeed = Highcharts.chart(chart_container_id, Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 100,
                // title: {
                //     text: 'Funded: '
                // }
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Progress',
                data: [funded_percentage],
                dataLabels: {
                    format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                        ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}%</span>'
                },
                tooltip: {
                    valueSuffix: ' %'
                }
            }]

        }));
    }

        
</script>

<script type="text/javascript">
    var userProfitUrl = "{{ route('member.cMiningGetUserProfits') }}";
    var userProfitPushtoBankUrl = "{{ route('member.cMiningUserProfitsPushToBank') }}";

    /*/ ==============================================================
    //  get user profit
    // ==============================================================/*/
    get_user_profit();
    function get_user_profit(){
        show_global_ajax_loader();

        var form_elem = jQuery('#get_user_profit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery(".user_profit_wrap").html(ret.returnHTML);
                jQuery('.user_profit_pagination_wrap').html(ret.paginationHtml);

                hide_global_ajax_loader();
            }
        });
    }


    /*/ ==============================================================
    //  user profit pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.user_profit_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#ut_qpage').val( qpage );
            jQuery('#ut_page').val( page );
            jQuery('#ut_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_user_profit();
        }

        return false;
    });


    /*/ ==============================================================
    // user trades push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.userprofit_pushtobank_btn', function(){
        _this = jQuery(this);
        var category = 'cryptotrading';
        var trid = jQuery(this).data('trid');

        jQuery('#utp_profit_id').val( trid );

        show_global_ajax_loader();

        var form_elem = jQuery('#user_profit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: userProfitPushtoBankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.user_trades_total').html('$'+rdata.tradeProfitTotal);
                    jQuery('.withdrawn_user_trades_total').html('$'+rdata.tradeProfitTotalWithdrawn);
                    jQuery('.unwithdrawn_user_trades_total').html('$'+rdata.tradeProfitTotalPending);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });



    jQuery(document).on( 'click', '.unwithdran_profit_btn', function(){
        jQuery('#user_profit_modal').modal({
            backdrop: 'static',
            keyboard: false
        });

        var pkg_id = jQuery(this).data('pid');
        jQuery('#ut_package_id').val(pkg_id);

        get_user_profit();

        jQuery(this).hide();
        
        return false;
    }); 
</script>
@stop