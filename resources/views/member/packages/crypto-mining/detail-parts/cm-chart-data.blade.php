<?php if ( $packageChartData ): ?>
[<?php 
	$dataArray = array();
	foreach ($packageChartData as $data) { 
		$dataArray[] = '['.strtotime($data->date).'000,'.LpjHelpers::amt2($data->profit_percent, false).']';
	}
	echo implode( ',',$dataArray );?>]
<?php else: ?>
[[0000000000000,0.00]]
<?php endif ?>