<!-- addFund content-->
<div class="modal fade" id="addFund" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Fund</h4>
            </div>
            <div class="modal-body">
                <div id="addfund-accordion" class="box-group">
                    
                    <!-- fund section -->
                    <div class="addFund-section panel panel-default">
                        <a class="collapse-trigger clearfix" data-toggle="collapse" data-target="#collapse3" data-parent="#addfund-accordion">
                            <img src="https://thepeoplesoptions.com/wp-content/themes/tpo2017/dashboard/img/bitpalbts-logo.png" style="margin: 0px;" alt="" class="collapse-img">
                            <span class="collapseTitle-container" style="padding: 25px 0;">
                                <span class="collapse-title">Add fun via BitPalBTS.com</span>
                            </span>
                        </a>

                        <div id="collapse3" class="collapse in" style="display: blockx;">
                            <div class="collapse-body">

                                <?php if ( !empty($btsAccountsDropdown) && $bitpalAccessTokenLifeTime >= 120 ): ?>
                                    <div class="reset_bitpalbts_auth_wrap" style="padding: 20px; font-size: 15px; background: #FFF;">
                                        <div class="alert alert-danger">
                                            <h4>BitPalBTS Authentication Token Expired</h4>
                                            <p>Please click on the button below to renew BitPalBTS auth token.</p>
                                        </div>
                                        <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Renew Auth Token</a>
                                    </div>

                                <?php else: ?>

                                    <div class="reset_bitpalbts_auth_wrap" style="margin-left: 30px; font-size: 15px; display: none;">
                                        <div class="alert alert-danger">
                                            <h4>BitPalBTS Authentication Token Expired</h4>
                                            <p>Please click on the button below to renew BitPalBTS auth token.</p>
                                        </div>
                                        <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Renew Auth Token</a>
                                    </div>

                                    <?php if ( $accessToken == '' || empty($btsAccountsDropdown) ): ?>
                                        <div style="padding: 20px;">
                                            <?php if ( !empty($myBTSWallets) ): ?>
                                                <div style="margin-left: 30px; font-size: 15px;">
                                                    <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Add Fund With Bitpal BTS</a>
                                                </div>

                                            <?php else: ?>
                                                <h4>
                                                    1. Click the button below to create your BitPalBTS account <br>
                                                    (<i style="font-size: 14px;">skip to #2 if you already have a BitPalBTS account</i>):
                                                </h4>
                                                <div style="margin-left: 30px; font-size: 14px;">
                                                    <a class="btn theme-btn" href="<?php echo $bitpalSignupUrl; ?>" target="_blank">Create BitPalBTS account</a>
                                                </div><br>
                                                <h4>
                                                    2. Click the button below to Add Fund
                                                </h4>
                                                <div style="margin-left: 30px; font-size: 15px;">
                                                    <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Add Fund With BitPalBTS</a>
                                                </div>
                                            <?php endif ?>
                                        </div>

                                    <?php else: ?>
                                        <?php # FORCE THE USER TO SELECT ONLY ONE BITSHARES ACCOUNT TO BE USED IN ADDING AND WITHDRAWING FUND ?>
                                        <?php if ( $bitsharesAccount == '' ): ?>
                                            
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <h1>
                                                        You have to set your Bitshares Account first.
                                                    </h1>
                                                    <form id="save_final_bitshares_account_form">
                                                        {{ csrf_field() }}
                                                        <label>Select your default bitshares account:</label>
                                                        <select class="form-control" type="text" name="bitshares_account" id="select_bitshares_account">
                                                            <?php foreach ($btsAccountsDropdown as $k => $v): ?>
                                                                <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                        <p> - this is the only bitshares account that you can use to Add and Withdraw Funds</p>

                                                        <button type="submit" class="btn theme-btn">Save Bitshares Account</button>
                                                    </form>
                                                </div>
                                            </div>

                                        <?php else: ?>
                                            <div class=" add_fund_bts_wrap ajax_target_wrap" style="padding: 20px;">
                                                <div class="ajax_target"></div>
                                                <div class="ajax_loader_box" style="display:none;"></div>
                                                <div class="ajax_result" style="display:none;"></div>
                                                
                                                <form method="post" id="add_fund_form_bts" action="" style="margin-top: 10px;">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" id="action" name="action" value="add_bank_fund_bitpalbts">
                                                    
                                                    <div class="bts_payment_field">
                                                        <div class="form-group ">
                                                            <div class="row">
                                                                <?php # BITSHARES ACCOUNT IS NOW PRESELECTED ?>
                                                                <?php /*/ ?>
                                                                <p class="col-md-12">
                                                                    <label class="control-label ">
                                                                        Select Wallet
                                                                    </label>
                                                                    <select name="btsAccount" id="bts_wallet" class="form-control">
                                                                        <?php foreach ($btsAccountsDropdown as $k => $v): ?>
                                                                            <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                                        <?php endforeach ?>
                                                                    </select>
                                                                </p>
                                                                <?php /*/ ?>

                                                                <div class="col-md-6">
                                                                    <label class="control-label ">
                                                                        How much would you like to add to your Fund? <br>
                                                                        <strong style="color: red">(Enter USD amount)</strong>
                                                                    </label>
                                                                    <input class="form-control lpj_num_only" id="amount" name="amount" type="text" value="10" />
                                                                    <div>
                                                                        <i>this will be converted to BTS if you select BTS asset.</i>
                                                                    </div>
                                                                    <div class="bts_conversion_wrap">
                                                                        <div style="padding: 3px 0;">
                                                                            <div class="bts_usd_price_wrap">
                                                                                ( <span class="bts_usd_price"><?php echo $USDtoBTSRate; ?></span> USD = 1 BTS )
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <label class="control-label ">
                                                                        Select Bitshares Asset to be used<br>
                                                                        <strong style="color: red">&nbsp;</strong>
                                                                    </label>
                                                                    <select name="btsAsset" id="bts_asset" class="form-control">
                                                                        <option value="bitusd">bitUSD</option>
                                                                        <option value="bts">BTS</option>
                                                                    </select>

                                                                    <div class="converted_to_bts_wrap" style="text-align: right; padding: 3px 0px; display: none;">
                                                                        = <span class="converted_to_bts">0</span> &nbsp; BTS
                                                                    </div>
                                                                    <div class="converted_to_bitusd_wrap" style="text-align: right; padding: 3px 0px;">
                                                                        = <span class="converted_to_bitusd">10</span> &nbsp; bitUSD
                                                                    </div>
                                                                </div>

                                                                <span class="clearing"></span> 
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <a class="btn theme-btn pull-right bitpalbts_confirm_btn" href="#">Confirm</a>
                                                        </div>
                                                        
                                                        <span class="clearing"></span>
                                                    </div>

                                                    <div class="invoice_wrap" style="display: none;">
                                                        <h3>
                                                            Confirm
                                                        </h3>
                                                        <div class="well">
                                                            <div class="invoice">
                                                                <div class="table-responsive">
                                                                    <table class="table">
                                                                        <tr>
                                                                            <th>
                                                                                Pay with this Bistshares account
                                                                            </th>
                                                                            <td>
                                                                                <span class="selected_bts_wallet"><?php echo $bitsharesAccount; ?></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>
                                                                                You will pay
                                                                            </th>
                                                                            <td>
                                                                                <span class="you_will_pay_amount">10</span>&nbsp;<span class="you_will_pay_asset">bitUSD</span>
                                                                                <input class="form-control lpj_num_only" id="amount2" name="amount2" type="hidden" value="10" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>
                                                                                Your TPO Bank will recieve
                                                                            </th>
                                                                            <td>
                                                                                <span class="you_will_recieve">10</span> USD
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            
                                                        <div class="form-group">
                                                            <div>
                                                                <button class="btn theme-btn btn-lg purchase_frm_btn pull-right" name="submit" type="submit">
                                                                Pay Now
                                                                </button>

                                                                <a class="btn theme-btn btn-lg pull-right bitpalbts_cancel_confirm_btn" href="#" style="margin-right: 10px;">Back</a>
                                                            </div>
                                                        </div>

                                                        <span class="clearing"></span>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php endif ?>

                                            
                                    <?php endif ?>

                                <?php endif ?>

                            </div>
                        </div>
                    </div>
                    <!-- end fund section -->

                </div>
            </div>
            <div class="modal-footer" style="display: none;">
                <button type="button" class="btn theme-btn close_payment_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>  
<!-- end addFund content -->


