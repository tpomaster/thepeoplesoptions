<?php foreach ($bankTrnxs as $tr){?>
    <tr class="bank_tr_<?php echo $tr->transaction_code; ?> sect_<?php echo $tr->section ?>">
        <td>
            <div style="max-width: 400px;">
                <?php echo $tr->description; ?>
            </div>        
        </td>
        <td><?php echo LpjHelpers::amt2($tr->amount); ?></td>
        <td>
        	<?php if ( $tr->type == 'in'): ?>
        		<span class="in"><i class="fa fa-arrow-down "></i> in</span>

        	<?php else: ?>
        		<span class="out"><i class="fa fa-shopping-cart "></i> out</span>
        	<?php endif ?>
        </td>
        <td><?php echo LpjHelpers::nice_date($tr->transaction_date); ?></td>
        <td>
            <span class="status-<?php echo $tr->status; ?> tr_status"><?php echo $tr->status; ?></span>
        </td>
        <td>
        	<?php if ( $tr->status == 'pending' && $tr->section == 'withdrawal' ): ?>
                <?php /*/ ?><?php /*/ ?>
        		<a href="#" data-trcode="<?php echo $tr->transaction_code; ?>" class="btn theme-btn btn-xs cancel_payout_btn">cancel</a>
        	<?php endif ?>
        </td>
    </tr>
<?php } ?>
