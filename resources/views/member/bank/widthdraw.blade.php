<!-- widthraw -->
<div class="modal fade" id="widthraw" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	        <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Withdraw</h4>
	        </div>
	        <div class="modal-body" style="background: #FFF;">
                <?php if ( $withdrawFOrmEnabled ): ?>

                    <?php if ( $withdrawFormReqmtPassed ): ?>

                        <?php if ( $bitsharesAccount == '' ): ?>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <h1>
                                        You have to set your Bitshares Account first.
                                    </h1>
                                    
                                    <form id="save_final_bitshares_account_form">
                                        {{ csrf_field() }}
                                        <label>Select your default bitshares account:</label>
                                        <select class="form-control" type="text" name="bitshares_account" id="select_bitshares_account">
                                            <?php foreach ($btsAccountsDropdown as $k => $v): ?>
                                                <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <p> - this is the only bitshares account that you can use to Add and Withdraw Funds</p>

                                        <button type="submit" class="btn theme-btn">Save Bitshares Account</button>
                                    </form>
                                </div>
                            </div>

                        <?php else: ?>
                            
                            <form method="post" id="withdraw_form" action="">
                                {{ csrf_field() }}

                                <div class="form-group ">
                                    <label class="control-label ">
                                        Enter Amount to withdraw ($)
                                    </label>
                                    <input class="form-control lpj_num_only" id="withdraw_amount" name="amount" type="text" value="10" />
                                </div>

                                <div class="form-group ">
                                    <label class="control-label ">
                                        Your Bitshares Account: 
                                    </label>
                                    <span><?php echo $bitsharesAccount; ?></span>
                                    <br>
                                    <span style="color: red;">
                                        - Please make sure that this is your bitshares account. It should be connected to your BitPalBTS.com account.
                                    </span>
                                </div>

                                <div class="form-group">
                                    <div>
                                        <button class="btn theme-btn purchase_frm_btn" name="submit" type="submit">
                                        Submit
                                        </button>
                                    </div>
                                </div>
                            </form>

                        <?php endif ?>
                            

                        <div class="notes ta_center">
                            <strong>NOTES:</strong> <br>
                             - Maximum Payout Amount depends on your membership level. <a href="https://thepeoplesoptions.com/membership-packages/" target="_blank">Learn More</a><br>
                             - Payout requests will be processed within 48 hours after submission
                        </div>

                    <?php else: ?>
                        <p style="font-size: 18px; margin-bottom: 30px;">
                            To be able to withdraw, you must first try to Add Funds using your BitPalBTS account.
                            It is during the Add Fund process that we verify your Bitshares accounts.
                        </p>

                        <h4>
                            1. Click the button below to create your BitPalBTS account. <br>
                            (<i style="font-size: 14px;">skip to #2 if you already have a BitPalBTS account</i>):
                        </h4>
                        <div style="margin-left: 30px; font-size: 14px;">
                            <a class="btn theme-btn" href="<?php echo $bitpalSignupUrl; ?>" target="_blank">Create BitPalBTS account</a>
                        </div><br>
                        <h4>
                            2. Click the button below to Add Fund
                        </h4>
                        <div style="margin-left: 30px; font-size: 15px;">
                            <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Add Fund With BitPalBTS</a>
                        </div>

                    <?php endif ?>
                        

                <?php else: ?>
                    <div style="font-size: 16px;">
                        <p>
                            In line with TPO’s dedication to provide and progress better services, we at TPO have developed a new Bitshares wallet that is being integrated and tested by our programmers.  This design is conceptualized to offer streamlined automated cross-functionality and integration with other wallets, platforms and crypto currencies. It will in effect facilitate a better and faster movement of funds much lower and reasonable fees.
                        </p>
                        <p>
                            This Bitshares wallet will replace the bitcoin wallets that TPO has been utilizing/accepting.  Much to everyone’s dismay, the recent surge of BTC transactions hampered the confirmation of transactions and caused abrupt and rapid increase in fees.
                        </p>
                        <p>
                            In view of this development, the “TPO Withdrawal”, as well as the “TPO Add Funds” functions will be temporarily turned off for the time being. 
                        </p>
                        <p>
                            Thank you <br>
                            TPO Management 
                        </p>
                    </div>
                <?php endif ?>
	        </div>
	        <div class="modal-footer">
	          	<button type="button" class="btn theme-btn close_payout_modal" data-dismiss="modal">Close</button>
	        </div>
	    </div>
    </div>
</div>
<!-- end widthraw -->


