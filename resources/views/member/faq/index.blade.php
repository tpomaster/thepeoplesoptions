@extends('member.layouts.blank')

@section('pagespecificstyles')

  <!-- news css-->
  <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/faq.css') }}">
@stop

@section('main_container')
  <!-- page content -->
  <div class="faq-section right_col clearfix content-overide" role="main">
    <div class="header-page">
      <h1>FAQ</h1>
    </div>

    <div class="faq-list">
      <div class="faq-ajax-container">
        <h3>No FAQ Yet</h3>
      </div>

      <div class="faq_pagination_wrap"></div>
    </div>
  </div>

  <div style="display: none;">
      <form id="get_faq_frm">
          {{ csrf_field() }}
          <input type="text" id="faq_qpage" name="qpage" value="1">
          <input type="text" id="faq_page" name="page" value="1">
          <input type="text" id="faq_qlimit" name="qlimit" value="4">
      </form>
  </div>

  <!-- /page content -->
@include('member/includes/ajax-loader')
@endsection

@section('pagespecificscripts')
  <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
  <script type="text/javascript">
      // ajax announcement content
      var dashboardFaqUrl = "{{ route('member.getFaqsContent') }}";

      /*/ ==============================================================
      //  dashboard faq
      // ==============================================================/*/
      get_faq();
      function get_faq(){
          var ajax_target_wrap = jQuery(".faq-ajax-container");

          show_global_ajax_loader();

          var form_elem = jQuery('#get_faq_frm');
          var input_data = form_elem.serialize();

          jQuery.ajax({
              type: "POST",
              url: dashboardFaqUrl,
              dataType: 'json',
              data: input_data,
              success: function(ret){
                  jQuery( ajax_target_wrap ).html(ret.returnHTML);
                  jQuery('.faq_pagination_wrap').html(ret.paginationHtml);

                  hide_global_ajax_loader();
              },
              error: function( jqXHR, textStatus, errorThrown ){
                  handle_ajax_error(jqXHR, textStatus, errorThrown);
              }
          });
      }

      /*/ ==============================================================
      //  news pagination
      // ==============================================================/*/
      jQuery(document).on( 'click', '.faq_pagination_wrap .pagination a', function(){
          var thisElem = jQuery(this);
          var a_link = jQuery(this).attr( 'href' );

          if ( a_link != '#' ) {
              var qpage = lpj_parse_url( a_link, 'qpage' );
              var page = lpj_parse_url( a_link, 'page' );
              var qlimit = lpj_parse_url( a_link, 'qlimit' );
              var sortBy = lpj_parse_url( a_link, 'sortBy' );
              var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

              jQuery('#faq_qpage').val( qpage );
              jQuery('#faq_page').val( page );
              jQuery('#faq_qlimit').val( qlimit );
              // jQuery('#f_sortBy').val( sortBy );
              // jQuery('#f_sortOrder').val( sortOrder );

              get_faq();
          }

          return false;
      });
  </script>
@stop