<?php if ( $Faqs ): ?>
    <?php foreach ($Faqs as $faq){?>
        <div class="faq-box faq_item_<?php echo $faq->id; ?>">
          <div class="row">
            <div class="faq-thumbnail col-md-1 col-sm-12 col-xs-12">
              <?php 
                $imageHolder = $faq->thumbnail_path;
                $setImage = 'images/nthumb14.png';

                if ( !empty($imageHolder) ) {
                  $setImage = 'storage/'.$imageHolder;
                }
              ?>
              <img src="<?php echo LpjHelpers::asset($setImage); ?>" alt="<?php echo $faq->title; ?>" width="100%" height="auto">
            </div>
            <div class="faq-content col-md-9 col-sm-12 col-xs-12">
              <h3><a href="" data-toggle="modal" data-target="#faq_<?php echo($faq->id); ?>"><?php echo $faq->title; ?></a></h3>
              <div class="generated-content">
                <!-- {!! html_entity_decode($faq->content) !!} -->
                <p><?php echo LpjHelpers::getExcerpt($faq->content, 300); ?></p>
              </div>
            </div>
            <div class="faq-side col-md-2 col-ms-12 col-xs-12">
              <a href="" class= "modal-btn btn btn-xs theme-btn" data-toggle="modal" data-target="#faq_<?php echo($faq->id); ?>">
                <span class="glyphicon glyphicon-search"></span>
              </a>
            </div>
          </div>
        </div>

        <div class="modal fade" id="faq_<?php echo($faq->id); ?>" data-backdrop="static" data-keyboard="false" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $faq->title; ?></h4>
              </div>
              <div class="modal-body">
                <?php echo $faq->content; ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default black-btn" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    <?php } ?>
<?php else: ?>
    <h3>No FAQ Yet</h3>
<?php endif ?>

