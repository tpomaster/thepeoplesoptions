<!-- announcements section -->

	<?php if ( $latestPopups ): ?>
		<?php 
				$numItems = count($latestPopups);
    			$modal_increment = 1;
    			$totalCount = 0;
    		?>
    	<?php foreach ($latestPopups as $ann){?>
    		

			<div class="modal ann_popup_modal" id="myModal<?php if($modal_increment > 1): echo $modal_increment; endif; ?>" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog animate modal-lg multi-pane">
			    <div class="modal-content clearfix">
			    	<?php 
			    		$imageHolder = $ann->thumbnail_path;
			    		$setImage = 'images/announcement-img.png';

			    		if ( !empty($imageHolder) ) {
			    			$setImage = 'storage/'.$imageHolder;
			    		}
			    	?>
			    	<div class="modal-left" style="background: url('<?php echo LpjHelpers::asset($setImage); ?>') no-repeat center; background-size: cover; height: 100%;">
			    	</div>
			    	<div class="modal-right">
				      <div class="modal-header">
				        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-file-text-o"></i> ANNOUNCEMENTS</h4>
				      </div>
				      <div class="modal-body">
					    <h2><?php echo $ann->title; ?></h2>
					    <?php echo $ann->content; ?>
				      </div>
				      <div class="modal-footer clearfix">
						<div class="modalFooter-left">
							<?php echo $modal_increment; ?> / <?php echo $countPopups; ?>
						</div>
				      	<div class="modalFooter-right">
							<?php if(++$totalCount === $numItems) { ?>
					        	<a href="#" class="btn btn-default black-btn" data-dismiss="modal">Close</a>
					        <?php } else { ?>
					        	<a href="#" class="btn btn-default black-btn btn-next">Next</a>
					        <?php }?>
					    </div>
				      </div>
				    </div>
			    </div>
			  </div>
			</div>

			<?php 
    			$modal_increment++;
    		?>

		<?php } ?>
	<?php else: ?>
	<?php endif ?>