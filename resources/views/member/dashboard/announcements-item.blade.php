<!-- for announcement -->

<?php if ( $Announcements ): ?>
    <?php foreach ($Announcements as $ann){?>
        <?php 
            $imageHolder = $ann->thumbnail_path;
            $setImage = 'images/nthumb8.png';

            if ( !empty($imageHolder) ) {
                $setImage = 'storage/'.$imageHolder;
            }
        ?>
        <div class="x-content-box clearfix ann_item_<?php echo $ann->id; ?>">
            <img src="<?php echo LpjHelpers::asset($setImage); ?>" class="pull-left dashboard-thumbs" alt="">
            <div class="pull-right announcement-info">
                <h3 class="announcements-title"><?php echo $ann->title; ?></h3>
                <?php echo LpjHelpers::getExcerpt($ann->content, 110); ?>
                <br/>
                <a href="" data-toggle="modal" data-target="#announcement_<?php echo($ann->id); ?>">READ MORE</a>
            </div>
        </div>

        <div class="modal fade" id="announcement_<?php echo($ann->id); ?>" data-backdrop="static" data-keyboard="false" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $ann->title; ?></h4>
              </div>
              <div class="modal-body">
                <?php echo $ann->content; ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default black-btn" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    <?php } ?>
<?php else: ?>
    <div class="x-content-box clearfix">
        <div class="announcement-info">
            <h3 class="announcements-title">No Announcement Yet</h3>
        </div>
    </div>
<?php endif ?>