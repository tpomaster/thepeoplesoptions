<!-- for news -->

<?php if ( $News ): ?>
    <?php foreach ($News as $newsData){?>
        <?php 
            $imageHolder = $newsData->thumbnail_path;
            $setImage = 'images/nthumb15.png';

            if ( !empty($imageHolder) ) {
                $setImage = 'storage/'.$imageHolder;
            }
        ?>
        <div class="x-content-box clearfix news_item_<?php echo $newsData->id; ?>">
            <img src="<?php echo LpjHelpers::asset($setImage); ?>" class="pull-left dashboard-thumbs" alt="">
            <div class="pull-right announcement-info">
                <h3 class="announcements-title"><?php echo $newsData->title; ?></h3>
                <?php echo LpjHelpers::getExcerpt($newsData->content, 110); ?>
                <br/>
                <a href="" data-toggle="modal" data-target="#news_<?php echo($newsData->id); ?>">READ MORE</a>
            </div>
        </div>

        <div class="modal fade" id="news_<?php echo($newsData->id); ?>" data-backdrop="static" data-keyboard="false" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $newsData->title; ?></h4>
              </div>
              <div class="modal-body">
                <?php echo $newsData->content; ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default black-btn" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
    <?php } ?>
<?php else: ?>
    <div class="x-content-box clearfix">
        <div class="announcement-info">
            <h3 class="announcements-title">No News Yet</h3>
        </div>
    </div>
<?php endif ?>