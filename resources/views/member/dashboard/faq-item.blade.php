<!-- for faq -->

<?php if ( $Faqs ): ?>
    <?php foreach ($Faqs as $faq){?>
        <li class="faq_item_<?php echo $faq->id; ?>">
        	<a href="" data-toggle="modal" data-target="#faq_<?php echo($faq->id); ?>"><?php echo $faq->title; ?></a>

			<div class="modal fade" id="faq_<?php echo($faq->id); ?>" data-backdrop="static" data-keyboard="false" role="dialog">
	          <div class="modal-dialog modal-lg">
	            <div class="modal-content">
	              <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title"><?php echo $faq->title; ?></h4>
	              </div>
	              <div class="modal-body">
	                <?php echo $faq->content; ?>
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default black-btn" data-dismiss="modal">Close</button>
	              </div>
	            </div>
	          </div>
	        </div>
        </li>
    <?php } ?>
<?php else: ?>
    <li><a href="">No FAQ Yet</a></li>
<?php endif ?>