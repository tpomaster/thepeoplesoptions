<?php if ( $chartData ): ?>
[<?php 
	$dataArray = array();
	foreach ($chartData as $data) { 
		$dataArray[] = '['.strtotime($data->date).'000,'.LpjHelpers::amt2($data->total_earnings, false).']';
	}
	echo implode( ',',$dataArray );?>]
<?php else: ?>
[[0000000000000,0.00]]
<?php endif ?>