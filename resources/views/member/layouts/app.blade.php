<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>The Peoples Options</title>

    <style id="init-style">
        *{color:#fff; border:0; background:none;} 
        img,button{display:none;}
        input { display: none; }
    </style>

    <script type="text/javascript">
        /* Minified Version: */
        function loadStyle(c,b){var a=document.createElement('link');a.rel='stylesheet',a.readyState?a.onreadystatechange=function(){(a.readyState=='loaded'||a.readyState=='complete')&&(a.onreadystatechange=null,b());}:a.onload=function(){b();},a.href=c,document.getElementsByTagName('head')[0].appendChild(a);}
        function loadScript(c,b){var a=document.createElement('script');a.type='text/javascript',a.readyState?a.onreadystatechange=function(){(a.readyState=='loaded'||a.readyState=='complete')&&(a.onreadystatechange=null,b());}:a.onload=function(){b();},a.src=c,document.body.appendChild(a);}
    </script>

    @yield('pagespecificstyles')
</head>
<body class="login-overide" style="padding-bottom: 100px;">
    <div class="loader">
        <i class="fa fa-cog fa-spin"></i>
    </div>
    <div id="app">
        @yield('content')
    </div>

    <script type="text/javascript">
        loadStyle("{{ LpjHelpers::asset('css/member/login-style.css') }}", function(){
            document.getElementById("init-style").outerHTML=''; /* Remove initial styles from your page */
        });         
    </script>

    <!-- Scripts -->
    <script src="{{ LpjHelpers::asset('js/member/app.js') }}"></script>
    <script src="{{ LpjHelpers::asset('js/member/dashboard.js') }}"></script>

    <!-- iCheck -->
    <script src="{{ LpjHelpers::asset('js/member/icheck.min.js') }}"> </script>
    <script>
        jQuery(function () {
            jQuery('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

    @yield('pagespecificscripts')

</body>
</html>
