﻿<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>The Peoples Options</title>

    <!-- Styles -->
    <link href="{{ asset('css/member/app.css') }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('css/gentelella.min.css') }}" rel="stylesheet">
    <!-- page specific styles -->
    @yield('pagespecificstyles')
    <!-- page temporary css overwrites -->
    <link href="{{ asset('css/overwrite.css') }}" rel="stylesheet">
</head>
<body id="dashboard" class="nav-md">
    <div class="loader">
        <i class="fa fa-cog fa-spin"></i>
    </div>
    <!-- <div id="app" class="container body"> -->
    <div class="container body">

        <div class="main_container">

            @include('member/includes/sidebar')

            @include('member/includes/topbar')

            @yield('main_container')

            @include('member/includes/footer')

        </div>

    </div>

    <div id="continue_session_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle" style="">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="text-align: center;">
                <div class="modal-header">
                    <h2 class="modal-title" id="modaltitle">Do you want to continue your sesson?</h2>
                </div>
                <div class="modal-body">
                    <div class="model_content_wrap" style="font-size: 16px;">
                        For security reasons, your session will time out <br>
                        after 30 seconds unless you continue.
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn theme-btn close_payment_modal" data-dismiss="modal">Continue Session</button>
                </div>
            </div>  
        </div>
    </div>
    
    <!-- BEGIN PHP Live! HTML Code -->
    <span style="color: #0000FF; text-decoration: underline; cursor: pointer;display:block; position: fixed; bottom: 0; right: 10px; z-index: 999999;" id="phplive_btn_1492070199" onclick="phplive_launch_chat_0(0)"></span>
    <script type="text/javascript">

    (function() {
    var phplive_e_1492070199 = document.createElement("script") ;
    phplive_e_1492070199.type = "text/javascript" ;
    phplive_e_1492070199.async = true ;
    phplive_e_1492070199.src = "https://thepeoplesoptions.com/phplive/js/phplive_v2.js.php?v=0|1492070199|0|" ;
    document.getElementById("phplive_btn_1492070199").appendChild( phplive_e_1492070199 ) ;
    })() ;

    </script>
    <!-- END PHP Live! HTML Code -->
    
    <!-- Scripts -->
    <script src="{{ LpjHelpers::asset('js/member/app.js') }}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{ LpjHelpers::asset('js/gentelella.min.js') }}"></script>
    <script src="{{ LpjHelpers::asset('js/member/dashboard.js') }}"></script>
    <script src="{{ LpjHelpers::asset('js/member/tock.min.js') }}"></script>

    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
    <!-- page specific scripts -->
    @yield('pagespecificscripts')

    <!-- autologout after 30 min -->
    <script type="text/javascript">
        /* autologout */
        var idleTime = 0;
        jQuery(document).ready(function () {
            /* Increment the idle time counter every minute. */
            var idleInterval = setInterval(timerIncrement, 30000);

            /* Zero the idle timer on mouse movement. */
            jQuery(this).mousemove(function (e) {
                // idleTime = 0;
            });

            jQuery(this).click(function (e) {
                idleTime = 0;
            });

            jQuery(this).keypress(function (e) {
                idleTime = 0;
            });
        });

        function timerIncrement() {
            idleTime = idleTime + 1;

            console.log( idleTime );

            if ( idleTime == 59 ) {
                jQuery('#continue_session_modal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }

            if ( idleTime > 60 ) { /* 20 minutes */
                var logoutUrl = "{{ route('member.logout') }}";
                location.href = logoutUrl;
            }
        }
    </script>

</body>
</html>