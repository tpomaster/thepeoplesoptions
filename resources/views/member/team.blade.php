@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/tree-view.css') }}">
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
	<div class="header-page">
		<h1>Team Tree View</h1>
	</div>

	<div class="row">
		<div class="col-md-12 col-s-12 col-xs-12">
			

			<div id="treeView-content">
				<?php if ( $sponsorInfo ): ?>
					<div class="sponsor-info">
						<p>
							Your Sponsor: <strong><?php echo $sponsorInfo->username; ?></strong> ( <?php echo $sponsorInfo->membership; ?>)
						</p>
					</div>
					<hr>
				<?php endif ?>
					

				<div class="headerRow clearfix">
					<div class="blankLabel"></div>
					<div class="userNameLabel">Username</div>
					<div class="firstNameLabel">First Name</div>
					<div class="lastNameLabel">Last Name</div>
					<div class="dateRegLabel">Date Registered</div>
					<div class="membershipLabel">Membership</div>
				</div>

				<div class="parentRow tierWrap tierWrap<?php echo $parentUsername; ?>">
					<a href="#" class="btn btn-primary getTeamBtn getDirectReferralsBtn" data-parentid="<?php echo $parentUsername; ?>" data-currtierlvl="1" style="display: none;">get team</a>
					<div class="childTierWrap">

					</div>
				</div>

				<div>
					<form id="getTeamFrm" style="display: none;">
						{{ csrf_field() }}
						<input class="" type="text" name="parentId" value="123" id="parentId">
						<input class="" type="text" name="currTierLvl" value="1" id="currTierLvl">
						<input class="" type="text" name="qpage" value="1" id="qpage">
						<input class="" type="text" name="page" value="1" id="page">
						<input class="" type="text" name="qlimit" value="10" id="qlimit">
						<input class="" type="text" name="sortBy" value="created_at" id="sortBy">
						<input class="" type="text" name="sortOrder" value="DESC" id="sortOrder">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@include('member/includes/ajax-loader')

<!-- end page content -->
@endsection

@section('pagespecificscripts')
<script type="text/javascript">
    var ajaxurl = '{{ route('member.team2.ajax') }}';
</script>
<script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('.getDirectReferralsBtn').trigger('click');
	});

	jQuery(document).on( 'click', '.getTeamBtn', function(){
		var parentId = jQuery(this).data('parentid');
		var currTierLvl = jQuery(this).data('currtierlvl');
		var childTierWrap = jQuery(this).parent('.tierWrap').find('.childTierWrap');

		if ( jQuery(this).hasClass( "isActive" ) ) {
			childTierWrap.slideUp();

		}else{
			jQuery('#parentId').val( parentId );
    		jQuery('#currTierLvl').val( currTierLvl );
    		jQuery('#qpage').val( 1 );
    		jQuery('#page').val( 1 );
    		jQuery('#qlimit').val( 10 );

    		get_child_tier_2( childTierWrap );

    		childTierWrap.slideDown();
		}

		jQuery(this).toggleClass('isActive');

		return false;
	});

	// use this if you are using LpjPaginator
	function get_child_tier( childTierWrap ){
		var formElem = jQuery('#getTeamFrm');
		var inputData = formElem.serialize();

		show_global_ajax_loader();

		jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: inputData,
            success: function( result ){

                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');
                }else{
                    //show_page_ajax_message( result.msg, 'success');
                    childTierWrap.html(result.msg);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false
	}

	// use this if you are using laravel pagination
	function get_child_tier_2( childTierWrap ){
		var formElem = jQuery('#getTeamFrm');
		var inputData = formElem.serialize();

		show_global_ajax_loader();

		jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: inputData,
            success: function( result ){

                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');

                }else{
                    //show_page_ajax_message( result.msg, 'success');
                    childTierWrap.html(result.msg);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false
	}

	/*
	pagination
	 */
	jQuery(document).on( 'click', '.pagination a', function(){
	    var thisElem = jQuery(this);
	    var a_link = jQuery(this).attr( 'href' );

	    if ( a_link != '#' ) {
	    	var parentId = lpj_parse_url( a_link, 'parentId' );
	    	var currTierLvl = lpj_parse_url( a_link, 'currTierLvl' );
	        var qpage = lpj_parse_url( a_link, 'qpage' );
	        var page = lpj_parse_url( a_link, 'page' );
	        var qlimit = lpj_parse_url( a_link, 'qlimit' );
	        var sortBy = lpj_parse_url( a_link, 'sortBy' );
	        var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

	        jQuery('#parentId').val( parentId );
    		jQuery('#currTierLvl').val( currTierLvl );
    		jQuery('#qpage').val( qpage );
    		jQuery('#page').val( page );
    		jQuery('#qlimit').val( qlimit );
    		jQuery('#sortBy').val( sortBy );
    		jQuery('#sortOrder').val( sortOrder );

	        var childTierWrap = jQuery(this).parents('.tierWrap'+parentId).find('.childTierWrap');

	        get_child_tier_2( childTierWrap );
	    }

	    return false;
	});

</script>
@stop

