@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/bank.css') }}">
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>TPO Bank <span>your internal wallet</span></h1>
    </div>
            
    <div class="row">
        <div class="bank-container">
            <div class="col-md-3 col-xs-12">
                <button type="button" class="btn theme-btn full" id="bank_add_fund_btn" data-toggle="modal" data-target="#addFund">Add Fund</button>
                <!-- add fund -->
                @include('member/bank/add-fund')

                <button type="button" class="btn yellow-btn full" id="bank_withdraw_btn" data-toggle="modal" data-target="#widthraw">Withdraw</button>
                <!-- widthraw -->
                @include('member/bank/widthdraw')

                <div class="tile-stats-box">
                    <div class="icon-holder"><img src="{{LpjHelpers::asset('images/balances.png')}}" alt="" class="icon-holder-img"></div>
                    <span class="count_top"><i class="fa fa-pie-chart"></i> AVAILABLE BALANCE</span>
                    <div class="count"><?php echo LpjHelpers::amt2($userBank->available_bal); ?></div>
                </div>

                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>Bank Transactions Summary</h2>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-arrow-down"></i> 
                            <a href="#" class="bank_filter_btn" data-section="deposit"><?php echo LpjHelpers::amt2($userBank->deposit); ?></a>
                        </h3>
                        <h4><span class="in">in</span> Added Fund </h4>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-sign-out"></i> 
                            <a href="" class="bank_filter_btn" data-section="closed-package-fund"><?php echo LpjHelpers::amt2($userBank->closed_package); ?></a>
                        </h3>
                        <h4><span class="in">in</span> Closed Package Fund - Pushed to Bank </h4>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-money"></i> 
                            <a href="{{ route('member.referralCredits') }}" target="_blank" class="bank_filter_btnx" data-section="referral-credit"><?php echo LpjHelpers::amt2($userBank->referral_credit); ?></a>
                        </h3>
                        <h4><span class="in">in</span> Referral Credits </h4>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-line-chart"></i> 
                            <a href="{{ route('member.packageEarnings') }}" target="_blank" class="bank_filter_btnx" data-section="package-earning"><?php echo LpjHelpers::amt2($userBank->package_earning); ?></a>
                        </h3>
                        <h4><span class="in">in</span> Package Earnings </h4>
                    </div>
                    
                    <div class="breakdown-box">
                        <h3><i class="fa fa-shopping-cart "></i> 
                            <a href="" class="bank_filter_btn" data-section="package-payment"><?php echo LpjHelpers::amt2($userBank->package_purchase); ?></a>
                        </h3>
                        <h4><span class="out">out</span> Purchased on TPO Packages </h4>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-arrow-up"></i> 
                            <a href="#" class="bank_filter_btn" data-section="withdrawal"><?php echo LpjHelpers::amt2($userBank->payout); ?></a>
                        </h3>
                        <h4><span class="out">out</span> Payout </h4>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-cart-plus"></i> 
                            <a href="#" class="bank_filter_btn" data-section="cap-off-trade-amount"><?php echo LpjHelpers::amt2($userBank->cap_off_trade_amt); ?></a>
                        </h3>
                        <h4><span class="out">out</span> Used to Cap Off Trade Amt. </h4>
                    </div>
                </div>
            </div>

			<div class="col-md-9 col-xs-12">
				<div class="x_panel tile">
                    <div style="display: none;">
                        <div class="x_title">
                            <h2>Search Filter:</h2>
                            <a class="btn theme-btn toggle-search"><i class="fa fa-search" aria-hidden="true"></i></a>
                        </div>
                        <div class="search-form">
                            <form id="user-search" action="{{ route('tpo-bank-transaction.search') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Description / Amount</label>
                                            <input class="form-control" name="user_search" type="text">
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Type</label>
                                            <select class="form-control" name="filter">
                                                <option value="">ALL</option>
                                                <option value="in">IN</option>
                                                <option value="out">OUT</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Status</label>
                                            <select class="form-control" name="filter">
                                                <option value="">ALL</option>
                                                <option value="completed">Completed</option>
                                                <option value="pending">Pending</option>
                                                <option value="cancelled">Cancelled</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date From</label>
                                            <div class='input-group date' id='from-date'>
                                                <input type='text' class="form-control" name="from_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date To</label>
                                            <div class='input-group date' id='to-date'>
                                                <input type='text' class="form-control" name="to_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 col-xs-12">
                                            <div class="pull-right">
                                                <button class="btn theme-btn" name="submit" type="submit">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
    					
					<div class="x_title">
						<h2>TPO Bank (In/Out) Transactions History:</h2>
					</div>

					<div class="history-box">
						<div class="table-holder">
							<table class="table table-striped table-hover">
								<thead class="thead-inverse">
									<tr>
										<th>Description</th>
										<th>Amount</th>
										<th>Type</th>
										<th>Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody class="bank_transactions_wrap">
										
								</tbody>
							</table>
						</div>  
					</div>
					
					<div class="pagination_wrap">Total Count: 9</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="display: none;">
    <form id="cancel_addfund_frm">
        {{ csrf_field() }}
        <input type="text" id="cnl_trcode_inp" name="cnl" value="">
    </form>

    <form id="bank_trnx_filter_frm">
        {{ csrf_field() }}
        <input type="text" id="f_section" name="section" value="">
        <input type="text" id="f_qpage" name="qpage" value="1">
        <input type="text" id="f_page" name="page" value="1">
        <input type="text" id="f_qlimit" name="qlimit" value="20">
    </form>
</div>

<div id="payout_cancel_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modaltitle">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                <h4 class="modal-title" id="modaltitle">Confirm Action</h4>
            </div>
            <div class="modal-body">
                <div class="model_content_wrap payment_confirmation_content_wrap">
                    <div class="ajax_target">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="invoice_wrap">
                                    <p>
                                        Are you sure you want to cancel your withdrawal request?
                                    </p>

                                    <form id="cancel_pyaout_form">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="payout_trcode" name="transactionCode" value="">

                                        <p>
                                            <button type="submit" class="btn theme-btn btn-lg">Yes</button>
                                            <a href="#" class="btn theme-btn btn-lg discontinue_cancel_payout_btn">No</a>
                                        </p>
                                    </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: none;">
                <button type="button" class="btn btn-default close_cancel_payout_modal" data-dismiss="modal">Close</button>
            </div>
        </div>  
    </div>
</div>


<!-- end page content -->

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')

<script type="text/javascript">
    var ajaxurl = "{{ route('member.addFund') }}";
    var ajaxCancelUrl = "{{ route('member.cancelAddFund') }}";
    var ajaxBankTrnxUrl = "{{ route('member.bankTransactions') }}";

    var ajaxAddFundUrl = "{{ route('member.tpoBankBitpalAddFund') }}";
    var exchangeRateUrl = "{{ route('member.tpoBankBitpalExchangeRate') }}";

    var cancelPayoutUrl = "{{ route('member.cancelPayout') }}";

    /* =================================================
    COINPAYMENTS
    ================================================= */
    jQuery('#add_fund_form_bitcoin').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');

                }else{
                    show_page_ajax_message( result.msg, 'success');

                    jQuery('.coinpayments_invoice_wrap').html(result.buttonScript);
                    jQuery('#addfund-accordion').hide();

                    get_bank_transactions();
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

    jQuery(document).on( 'click', '.cancel_addfund_btn', function(){
        show_global_ajax_loader();
        var trcode = jQuery(this).data('cnl');
        jQuery('#cnl_trcode_inp').val(trcode);

        var form_elem = jQuery('#cancel_addfund_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: ajaxCancelUrl,
            data: input_data,
            dataType: 'json',
            success: function(rdata){
                if ( rdata.is_error === true ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );
                    jQuery('.coinpayments_invoice_wrap').html('');
                    jQuery('#addfund-accordion').show();
                    
                    //get_bank_transactions( 1, 20 );
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
        return false;
    });



    get_bank_transactions();

    /* =================================================
    BANK TRANSACTIONS 
    ================================================= */
    function get_bank_transactions(){
        var ajax_target_wrap = jQuery(".bank_transactions_wrap");

        show_global_ajax_loader();

        // var f_section = 'deposit';
        // jQuery('#f_section').val(f_section);

        var form_elem = jQuery('#bank_trnx_filter_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: ajaxBankTrnxUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.pagination_wrap').html(ret.paginationHtml);
                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    jQuery(document).on( 'click', '.bank_filter_btn', function(){
        var f_section = jQuery(this).data('section');

        jQuery('#f_section').val(f_section);
        get_bank_transactions();
        
        return false;
    });

    jQuery(document).on( 'click', '.bank_filter_btn_2', function(){
        var f_section = jQuery(this).data('section');

        jQuery('#f_section').val(f_section);
        //get_bank_transactions();
        
        return false;
    });

    /* =================================================
    bank transactions pagination
    ================================================= */
    jQuery(document).on( 'click', '.pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#f_qpage').val( qpage );
            jQuery('#f_page').val( page );
            jQuery('#f_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_bank_transactions();
        }

        return false;
    });

	/* =================================================
    search form toggle
    ================================================= */
    jQuery(".search-form").hide();

    jQuery(document).on('click', '.toggle-search', function(){
    	$(".search-form").slideToggle("slow");
      	return false;
  	});





    /* =================================================
    BITPAL
    ================================================= */
    jQuery('.bitpalbts_confirm_btn').click(function(){
        jQuery('.bts_payment_field').hide();
        jQuery('.invoice_wrap').show();

        return false;
    });

    jQuery('.bitpalbts_cancel_confirm_btn').click(function(){
        jQuery('.bts_payment_field').show();
        jQuery('.invoice_wrap').hide();

        return false;
    });



    function get_bts_usd_price_from_widget(){
        jQuery.ajax({
            type: "POST",
            url: exchangeRateUrl,
            data: {
                action: 'get_usd_to_bts_rate',
                x: 'x'
            },
            dataType: 'json',
            success: function(rdata){
                if ( rdata.usd_to_bts_rate ) {
                    jQuery('.bts_usd_price').html( rdata.usd_to_bts_rate );

                }else{
                    setTimeout(function(){ 
                        get_bts_usd_price_from_widget();
                    }, 15000);
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });      
    }

    function update_converted_to_bts(){
        var usd_amt = jQuery('#amount').val();
        var usd_amt2 = parseInt(usd_amt);

        var bts_usd_price = parseFloat( jQuery('.bts_usd_price').html() );
        var bts_to_1usd = 1/bts_usd_price;
        var bts_v = usd_amt2 * bts_to_1usd;

        jQuery('.converted_to_bts').html(bts_v);
        jQuery('.converted_to_bitusd').html(usd_amt2);
    }

    function update_invoice(){
        var usd_amt = jQuery('#amount').val();
        var bts_asset = jQuery('#bts_asset').val();
        var converted_to_bts = jQuery('.converted_to_bts').html();

        jQuery('.you_will_recieve').html(usd_amt);

        if ( bts_asset == 'bts') {
            var bts_asset2 = 'BTS';
            jQuery('.you_will_pay_amount').html(converted_to_bts);
            jQuery('.you_will_pay_asset').html(bts_asset2);
            jQuery('#amount2').val(converted_to_bts);

            jQuery('.converted_to_bts_wrap').show();
            jQuery('.converted_to_bitusd_wrap').hide();

        }else{
            var bts_asset2 = 'bitUSD';
            jQuery('.you_will_pay_amount').html(usd_amt);
            jQuery('.you_will_pay_asset').html(bts_asset2);
            jQuery('#amount2').val(usd_amt);

            jQuery('.converted_to_bts_wrap').hide();
            jQuery('.converted_to_bitusd_wrap').show();
        }
    }


    jQuery(document).ready(function(){
        get_bts_usd_price_from_widget();
        update_converted_to_bts();

        setInterval(function(){     
            //get_bts_usd_price_from_widget();

            var usd_amt = jQuery('#amount').val();
            var usd_amt2 = parseInt(usd_amt);

            if ( usd_amt2 > 0 ) {
                var bts_usd_price = parseFloat( jQuery('.bts_usd_price').html() );
                var bts_to_1usd = 1/bts_usd_price;
                var bts_v = usd_amt2 * bts_to_1usd;

                jQuery('.converted_to_bts').html(bts_v);
                jQuery('.converted_to_bitusd').html(usd_amt2);

                update_invoice();
            }

        }, 20000);
    });


    /* update converted bts value when amount is changed */
    jQuery(document).ready(function(){
        jQuery('#amount').change(function(){
            get_bts_usd_price_from_widget();

            var usd_amt = jQuery('#amount').val();
            var usd_amt2 = parseInt(usd_amt);

            if ( usd_amt2 > 0 ) {
                var bts_usd_price = parseFloat( jQuery('.bts_usd_price').html() );
                var bts_to_1usd = 1/bts_usd_price;
                var bts_v = usd_amt2 * bts_to_1usd;

                jQuery('.converted_to_bts').html(bts_v);
                jQuery('.converted_to_bitusd').html(usd_amt2);

                update_invoice();
            }
        });

        jQuery('#bts_asset').change(function(){
            var bts_asset = jQuery('#bts_asset').val();
            update_invoice();
        });

        jQuery('#bts_wallet').change(function(){
            var bts_wallet = jQuery(this).val();
            jQuery('.selected_bts_wallet').html( bts_wallet );
        });

        get_bts_usd_price_from_widget();
    });


    jQuery('#add_fund_form_bts').submit(function(){
        var amount = jQuery('#amount').val();

        if ( amount < 1 || amount > 5000 ) {
            var msg_arr = ['Amount should be 1 to 5000.'];
            show_page_ajax_message( msg_arr, 'danger' );

            return false;

        }else{

            var input_data = jQuery(this).serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: ajaxAddFundUrl,
                data: input_data,
                dataType: 'json',
                success: function(rdata){
                    if ( rdata.isError === true ) {
                        show_page_ajax_message( rdata.msg, 'danger' );

                        if ( rdata.send_status == '401' ) {
                            jQuery('.reset_bitpalbts_auth_wrap').show();
                            jQuery('.add_fund_bts_wrap').remove();
                        }

                    }else{
                        show_page_ajax_message( rdata.msg, 'success' );
                        get_bank_transactions( 1, 20 );

                        jQuery('.bts_payment_field').show();
                        jQuery('.invoice_wrap').hide();

                        jQuery('.close_payment_modal').trigger('click');
                    }

                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        }
    });
</script>


<?php if ( $openTab == 'addfundform' ): ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#bank_add_fund_btn').trigger('click');

            setTimeout(function(){ 
                jQuery('.bitpalbts_accordion').trigger('click');
            }, 100);
        });
    </script>
<?php endif ?>


<script type="text/javascript">
    var payoutUrl = "{{ route('member.processPayoutRequest') }}";

    /*/----------------------------------------------/*/
    /*/ WITHDRAW /*/
    jQuery(document).on( 'submit', '#withdraw_form', function(){
        show_global_ajax_loader();
        var input_data = jQuery(this).serialize();

        jQuery.ajax({
            type: "POST",
            url: payoutUrl,
            dataType: 'json',
            data: input_data,
            success: function( result ){

                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger' );

                }else{
                    show_page_ajax_message( result.msg, 'success' );
                    
                    get_bank_transactions();
                    jQuery('.close_payout_modal').trigger('click');
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    jQuery(document).on( 'click', '.cancel_payout_btn', function(){
        var this_btn = jQuery(this);
        var trcode = this_btn.data('trcode');
        jQuery('#payout_trcode').val(trcode);

        jQuery('#payout_cancel_modal').modal({
            backdrop: 'static',
            keyboard: false
        });

        return false;
    });

    jQuery(document).on( 'click', '.discontinue_cancel_payout_btn', function(){
        jQuery('.close_cancel_payout_modal').trigger('click');

        return false;
    });


    jQuery(document).on( 'submit', '#cancel_pyaout_form', function(){
        show_global_ajax_loader();

        var input_data = jQuery(this).serialize();

        var trcode = jQuery('#payout_trcode').val();

        jQuery.ajax({
            type: "POST",
            url: cancelPayoutUrl,
            data: input_data,
            dataType: 'json',
            success: function(result){
                if ( result.isError === true ) {
                    show_page_ajax_message( result.msg, 'danger' );

                }else{
                    show_page_ajax_message( result.msg, 'success' );

                    jQuery('.bank_tr_'+trcode).find('.tr_status').html('Cancelled');
                    jQuery('.bank_tr_'+trcode).find('.tr_status').removeClass('status-pending');
                    jQuery('.bank_tr_'+trcode).find('.tr_status').addClass('status-cancelled');
                    jQuery('.bank_tr_'+trcode).find('.cancel_payout_btn').remove();

                    jQuery('.close_cancel_payout_modal').trigger('click');
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });


        return false;
    });

</script>

<?php if ( isset($_GET['tab']) && $_GET['tab'] == 'withdrawform' ): ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#bank_withdraw_btn').trigger('click');
        });
    </script>
<?php endif ?>

<?php if ( $bitsharesAccount == '' ): ?>
    <script type="text/javascript">
        var updateBtsAccountUrl = "{{ route('member.updateBtsAccount') }}";
        var redirectUrl = "{{ route('member.tpoBank') }}?tab=addfundform";

        jQuery('#save_final_bitshares_account_form').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: updateBtsAccountUrl,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');
                        hide_global_ajax_loader();

                    }else{
                        show_page_ajax_message( result.msg, 'success');
                        window.location.replace(redirectUrl);
                    }
                    
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });
    </script>
<?php endif ?>

@stop