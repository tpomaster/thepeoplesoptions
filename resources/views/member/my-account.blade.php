@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/my-account.css') }}">
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>My Account <span>personal info / contact info / login info</span></h1>
    </div>

    <div class="row">
        <div class="col-md-3 col-xs-12">
            <div class="myAccount-box">
                <div class="x_panel tile">
                    <div class="profilePic-container">
                        <img src="{{LpjHelpers::asset('images/display-img.png')}}" alt="..." class="img-circle display-picture">
                    </div>
                    <div class="profile-topInfo">
                        <h3><?php echo $user->firstname; ?> <?php echo $user->lastname; ?></h3>
                        <h4><?php echo strtoupper( $user->membership ); ?> Account</h4>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="info-title">Referral Link:</span>
                            <div class="input-group">
                                <input id="ref_link" class="form-control" name="ref_link" value="<?php echo $aff_url; ?>" type="text">
                                <span class="input-group-btn">
                                    <button class="btn theme-btn btn-flat copy_ref_link" type="button">Copy</button>
                                </span>
                            </div>
                            <div class="copied" style="display: none;"> </div>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Username:</span>
                            <span class="pull-right"><?php echo $user->username; ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Email:</span>
                            <span class="pull-right"><?php echo $user->email; ?></span>
                        </li>
                        <li class="list-group-item">
                            <span class="info-title">Bitshares Name:</span>
                            <span class="pull-right"><?php echo $user->bitshares_name; ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-xs-12">
            <div class="myAccount-tab">
                <ul class="nav nav-tabs">
                    <li class="active"><a  href="#1b" data-toggle="tab"><i class="fa fa-user"></i> Edit Personal Info</a></li>
                    <li><a href="#2b" data-toggle="tab"><i class="fa fa-pencil-square-o logininfo_tab_btn"></i> Edit Login Info</a></li>
                    <li><a href="#3b" data-toggle="tab"><i class="fa fa-btc bitshares_tab_btn"></i> Bitshares</a></li>
                    <li><a href="#4b" data-toggle="tab"><i class="fa fa-usd tpomarketing_tab_btn"></i> TPO Marketing</a></li>
                </ul>

                <div class="tab-content clearfix">
                    <div class="tab-pane fade in active" id="1b">
                        <div class="col-md-12 col-xs-12">
                            <div class="tab-title">
                                <h2>Personal Information</h2>
                            </div>
                            <div class="account-form">
                                <form id="update_personal_info_frm" >
                                    {{ csrf_field() }}
                                    <input type="hidden" name="action" value="update_personal_info">
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>First Name <span class="red">*</span></label>
                                                <input class="form-control" type="text" name="firstname" value="<?php echo $user->firstname; ?>" id="first-name">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Last Name <span class="red">*</span></label>
                                                <input class="form-control" type="text" name="lastname" value="<?php echo $user->lastname; ?>" id="last-name">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Email <span class="red">*</span></label>
                                                <input class="form-control" type="email" name="email" value="<?php echo $user->email; ?>" id="email" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input class="form-control" type="text" id="phone" name="phone" value="<?php echo $user_meta->phone; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input class="form-control" type="text" id="address" name="address" value="<?php echo $user_meta->address; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>City</label>
                                                <input class="form-control" type="text" id="city" name="city" value="<?php echo $user_meta->city; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>State/Province</label>
                                                <input class="form-control" type="text" id="state" name="state" value="<?php echo $user_meta->state; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Zip Code</label>
                                                <input class="form-control" type="text" id="zip" name="zip" value="<?php echo $user_meta->zip; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <label>Country</label>
                                                <input class="form-control" type="text" id="country" name="country" value="<?php echo $user_meta->country; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn theme-btn">Update Information</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="2b">
                        <div class="col-md-12 col-xs-12">
                            <div class="tab-title">
                                <h2>Account</h2>
                            </div>
                            <div class="account-form">
                                <form id="update_account_info_frm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="action" value="update_account_info">


                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Username <span class="red">*</span></label>
                                                <input class="form-control" type="text" disabled="disabled" value="<?php echo $user->username; ?>" id="username" name="username">
                                                <p>Please email us if you want to change your username.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label>New Password</label>
                                                <input class="form-control" type="text" id="new-password" name="password">
                                                <p>Leave this empty if you don’t want to change your password.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Confirm New Password</label>
                                                <input class="form-control" type="text" id="confirm-password" name="password2">
                                                <p>Leave this empty if you don’t want to change your password.</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn theme-btn">Update Information</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="3b">
                        <div class="col-md-12 col-xs-12">
                            <div class="tab-title">
                                <h2>Edit Bitshares Info</h2>
                            </div>
                            <div class="bitshares-tab">

                                <?php if ( $user->bitshares_name == '' ): ?>
                                    
                                    <?php if ( $user_meta->bitpal_bts_accounts != '' ): ?>
                                        <?php 
                                        $bts_accounts = json_decode($user_meta->bitpal_bts_accounts); 
                                        //print_r($bts_accounts);
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <form id="save_final_bitshares_account_form">
                                                    {{ csrf_field() }}
                                                    <label>Select your FINAL bitshares account:</label>
                                                    <select class="form-control" type="text" name="bitshares_account" id="select_bitshares_account">
                                                        <?php foreach ($bts_accounts as $k => $v): ?>
                                                            <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                    <p>- this is the only bitshares account that you can use to Add and Withdraw Funds.</p>

                                                    <button type="submit" class="btn theme-btn">Update Information</button>
                                                </form>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        <a class="btn theme-btn" href="<?php echo $bitpalAuthUrl; ?>">Connect to BitPalBTS</a>

                                    <?php endif ?>

                                <?php else: ?>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                            <div class="bitshares-box">
                                                <h3 class="clearfix">Bitshares Account Verified: <?php echo $user->bitshares_name; ?> <img src="{{LpjHelpers::asset('images/check.png')}}" alt=""></h3>
                                                <p>Please email us if you want to change your bitshares account.</p>
                                            </div>
                                        </div>
                                    </div>

                                <?php endif ?>


                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="4b">
                        <div class="col-md-6 col-xs-12">
                            <div class="tab-title">
                                <h2>Your TPO Marketing Account</h2>
                            </div>
                            <div class="row marketing-tab">
                                <div class="col-md-8 col-xs-12">
                                    <form id="tpo_marketing_login_frm">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn theme-btn">Go To TPO MARKETING</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                                        
                </div>
            </div>
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

<!-- end page content -->
@endsection


@section('pagespecificscripts')
<script type="text/javascript">
    var ajaxurl = "{{ route('member.updateMyAccount') }}";
    var updateBtsAccountUrl = "{{ route('member.updateBtsAccount') }}";
    var tpoMarketingLoginUrl = "{{ route('member.tpoMarketingLogin') }}";
    var redirectUrl = "{{ route('member.myAccount') }}?tab=bitshares";
</script>
<script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
<script type="text/javascript">
    jQuery('#update_personal_info_frm').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');

                }else{
                    show_page_ajax_message( result.msg, 'success');
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

    jQuery('#update_account_info_frm').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');

                }else{
                    show_page_ajax_message( result.msg, 'success');
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


    jQuery('#save_final_bitshares_account_form').submit(function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: updateBtsAccountUrl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');
                    hide_global_ajax_loader();

                }else{
                    show_page_ajax_message( result.msg, 'success');
                    window.location.replace(redirectUrl);
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

    jQuery(document).on( 'submit', '#tpo_marketing_login_frm', function(){
        var form_elem = jQuery(this);
        var input_data = form_elem.serialize();

        show_global_ajax_loader();

        jQuery.ajax({
            type: "POST",
            url: tpoMarketingLoginUrl,
            dataType: 'json',
            data: input_data,
            success: function( result ){
                if( result.isError == true ){
                    show_page_ajax_message( result.msg, 'danger');
                    hide_global_ajax_loader();

                }else{
                    show_page_ajax_message( result.msg, 'success');
                    window.location.replace(result.redirectUrl);
                }

            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });

</script>

<script type="text/javascript">
jQuery(function() {
    jQuery('.copy_ref_link').click(function() {
        jQuery('#ref_link').focus();
        jQuery('#ref_link').select();
        document.execCommand('copy');

        jQuery(".copied").text("Copied to clipboard").show().fadeOut(4000);
    });
});
</script>

<?php if ( isset($_GET['tab']) ): ?>
    <script type="text/javascript">
        var cur_tab = '<?php echo LpjHelpers::fss($_GET['tab']); ?>';
        jQuery(document).ready(function(){
            jQuery('.'+cur_tab+'_tab_btn').trigger('click');
        });
    </script>
<?php endif ?>
@stop