@extends('member.layouts.app')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('content')
<div class="login-logo">
    <img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div>

<div class="container">
    <div class="row">
        <div class="login-container">
            <h2>RESET PASSWORD</h2>

            <form class="form-horizontal retrieve_pass_form" method="POST" action="">
                <div class="login-box">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="field-container">
                            <label>Your email</label>
                            <input id="email" type="email" name="email" value="" required autofocus placeholder="Email">
                            <span class="glyphicon glyphicon-envelope input-icon"></span>
                        </div>
                    </div>
                    <div class="field-container row">
                        <div class="col-md-8 text-left nopadding">
                            <p><a href="{{ route('member.login') }}"><i class="fa fa-sign-in"></i> Login</a></p>
                            <p><a href="https://thepeoplesoptions.com"><i class="fa fa-home"></i> Back to Home Page</a></p>
                        </div>
                        <div class="col-md-4 text-right nopadding">
                            <button type="submit" class="btn yellow-btn">Send</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>

    <script type="text/javascript">
        var ajaxurl = "{{ route('member.forgotPassword.submit') }}";

        jQuery('.retrieve_pass_form').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');

                    }else{
                        show_page_ajax_message( result.msg, 'success');
                    }

                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });
    </script>
@stop