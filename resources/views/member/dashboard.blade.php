@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- ajax css-->
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')

<div class="announcement-series">
    @include('member/dashboard/announcements-popup')
</div>

<!-- page content -->
<div class="right_col content-overide dashboard-content" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
            <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/teams.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-user"></i> TEAMS</span>
                <div class="count"><?php echo $teamCount; ?> Teams</div>
                <a href="{{ route('member.team') }}"><span class="count_bottom">More Info <span class="stats-arw">»</span></span></a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
            <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/balances.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-pie-chart"></i> BALANCES</span>
                <div class="count">$ <?php echo LpjHelpers::amt2($availBal); ?></div>
                <a href="{{ route('member.tpoBank') }}"><span class="count_bottom">More Info <span class="stats-arw">»</span></span></a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
            <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-line-chart"></i> EARNINGS </span>
                <div class="count green">$ <?php echo LpjHelpers::amt2($totalEarnings); ?></div>
                <a href="{{ route('member.packageEarnings') }}"><span class="count_bottom">More Info <span class="stats-arw">»</span></span></a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 tile_stats_count">
            <div class="tile-stats-box">
                <div class="icon-holder"><img src="{{LpjHelpers::asset('images/payout.png')}}" alt="" class="icon-holder-img"></div>
                <span class="count_top"><i class="fa fa-usd"></i> PAYOUTS</span>
                <div class="count">$ <?php echo LpjHelpers::amt2($payout); ?></div>
                <a href="{{ route('member.tpoBank') }}"><span class="count_bottom">More Info <span class="stats-arw">»</span></span></a>
            </div>
        </div>
    </div>
    <!-- /top tiles -->

    @include('member/dashboard/charts/charts')

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile">
                <div class="x_title">
                    <h2><i class="fa fa-bullhorn"></i> ANNOUNCEMENTS</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content announcement_ajax">
                    <h3>No Announcement Yet</h3>
                </div>

                <div class="ann_pagination_wrap"></div>
            </div>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                    <h2><i class="fa fa-file-text-o"></i> FEATURED NEWS</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content news_ajax">
                    <h3>No News Yet</h3>
                </div>

                <div class="news_pagination_wrap"></div>
            </div>
        </div>


        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel tile">
                        <div class="x_title">
                            <h2><i class="fa fa-exclamation-triangle"></i> REMINDERS</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <ul class="notification-list">
                                <?php if ($tradingProfit != 0) { ?>
                                    <li><a href="{{ route('member.crypto-trading-my-packages-list') }}">You have <span class="red-notif"><?php echo $tradingProfit; ?></span> unwithdrawn Crypto Trading Profit. <span class="notif-icons"><i class="fa fa-btc"></i></span></a></li>
                                <?php } ?>

                                <?php if ($miningProfit != 0) { ?>
                                    <li><a href="{{ route('member.cryptoMiningMyPackagesList') }}">You have <span class="red-notif"><?php echo $miningProfit; ?></span> unwithdrawn Crypto Mining Profit. <span class="notif-icons"><i class="fa fa-btc"></i></span></a></li>
                                <?php } ?>

                                <?php if ($exchangeProfit != 0) { ?>
                                    <li><a href="{{ route('member.cryptoExchangeMyPackagesList') }}">You have <span class="red-notif"><?php echo $exchangeProfit; ?></span> unwithdrawn Crypto Exchange Profit. <span class="notif-icons"><i class="fa fa-btc"></i></span></a></li>
                                <?php } ?>

                                <?php if ($powerBonus != 0) { ?>
                                    <li><a href="{{ route('member.membership-packages') }}">You have <span class="red-notif"><?php echo $powerBonus; ?></span> unwithdrawn Membership Power Bonus. <span class="notif-icons"><i class="fa fa-btc"></i></span></a></li>
                                <?php } ?>

                                <?php if ($RefCreditsProfit != 0) { ?>
                                    <li><a href="{{ route('member.referralCredits') }}">You have <span class="red-notif"><?php echo $RefCreditsProfit; ?></span> unwithdrawn Referral Credit. <span class="notif-icons"><i class="fa fa-btc"></i></span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel tile">
                        <div class="x_title">
                            <h2><i class="fa fa-question-circle"></i> FREQUENT ASKED QUESTIONS</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <ul class="faq-list faq_ajax">
                                <li><a href="">No FAQ Yet</a></li>
                            </ul>

                            <div class="faq_pagination_wrap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<div style="display: none;">
    <form id="get_ann_frm">
        {{ csrf_field() }}
        <input type="text" id="ann_qpage" name="qpage" value="1">
        <input type="text" id="ann_page" name="page" value="1">
        <input type="text" id="ann_qlimit" name="qlimit" value="4">
    </form>
</div>

<div style="display: none;">
    <form id="get_news_frm">
        {{ csrf_field() }}
        <input type="text" id="news_qpage" name="qpage" value="1">
        <input type="text" id="news_page" name="page" value="1">
        <input type="text" id="news_qlimit" name="qlimit" value="4">
    </form>
</div>

<div style="display: none;">
    <form id="get_faq_frm">
        {{ csrf_field() }}
        <input type="text" id="faq_qpage" name="qpage" value="1">
        <input type="text" id="faq_page" name="page" value="1">
        <input type="text" id="faq_qlimit" name="qlimit" value="5">
    </form>
</div>

<div style="display: none;">
    <a href="" class="get_graph_data_btn">Get Graph Data</a>

    <div class="graph_start_dates"></div>

    <form id="looping_graphdata_process">
        {{ csrf_field() }}
        <input type="text" id="new_start_date" name="new_start_date" value="<?php echo $new_start_date; ?>">
        <input type="text" id="runTime" name="runTime" value="<?php echo $runTime; ?>">
    </form>
</div>


@include('member/includes/ajax-loader')
@endsection


@section('pagespecificscripts')
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="{{ asset('js/member/helper.js') }}"> </script>
<script type="text/javascript">
    // line chart
    // setTimeout(function(){
        
    // }, 3000);


    // ajax announcement content
    var dashboardAnnUrl = "{{ route('member.getDashboardAnnouncements') }}";
    var dashboardNewsUrl = "{{ route('member.getDashboardNews') }}";
    var dashboardFaqUrl = "{{ route('member.getDashboardFaqs') }}";
    var dashboardLoopProcessUrl = "{{ route('member.loopDashboardGraphDataPost') }}";

    jQuery('.get_graph_data_btn').click(function(){
        loop_graphdata_process();
        return false;
    });

    jQuery( document ).ready(function() {
        jQuery('#new_start_date').val('');
        jQuery('#runTime').val('0');
        jQuery( ".get_graph_data_btn" ).trigger(  "click" );
    });

    /*/ ==============================================================
    //  dashboard get graph data loop
    // ==============================================================/*/
    function loop_graphdata_process(){
        var form_elem = jQuery('#looping_graphdata_process');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: dashboardLoopProcessUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery('#new_start_date').val(ret.new_start_date2);
                jQuery('#runTime').val(ret.new_runTime);
                jQuery('.graph_start_dates').append('<p>'+ret.new_start_date2+' - '+ ret.end_date +'</p>');
                jQuery('.loader-text').html(ret.message);

                if ( ret.msg == 'continue' ) {
                    console.log(ret);

                    loop_graphdata_process();

                }else{
                    console.log('DONE');

                    document.getElementById("data-loading").style.display = "none";

                    $.getJSON('{{ route("member.getChartData") }}?', function (data) {
                        // Create the chart 1
                        Highcharts.stockChart('pkg-chart-1', {

                            rangeSelector: {
                                selected: 1
                            },

                            title: {
                                text: 'Daily Earnings Chart'
                            },

                            series: [{
                                name: 'Earnings',
                                data: data,
                                tooltip: {
                                    valueDecimals: 2
                                }
                            }]
                        });

                        // donut chart
                        Highcharts.chart('investment-chart', {
                          chart: {
                              type: 'pie',
                              options3d: {
                                  enabled: true,
                                  alpha: 45
                              }
                          },
                          title: {
                              text: 'Funded Packages Summary'
                          },
                          plotOptions: {
                              pie: {
                                  innerSize: 100,
                                  depth: 45
                              }
                          },
                          series: [{
                              name: 'Delivered amount',
                              data: [
                                  ['Trading', <?php echo $tradingFunds ?> ],
                                  ['Exchange', <?php echo $exchangeFunds ?>],
                                  ['Mining', <?php echo $miningFunds ?>],
                                  ['CruptoShuffle', <?php echo $shuffleFunds ?>],
                              ]
                          }]
                      });
                    });

                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            },
            complete: function() {

            }
          });
    }


    /*/ ==============================================================
    //  dashboard announcements
    // ==============================================================/*/
    get_dashboard_ann();
    function get_dashboard_ann(){
        var ajax_target_wrap = jQuery(".announcement_ajax");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ann_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: dashboardAnnUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.ann_pagination_wrap').html(ret.paginationHtml);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    /*/ ==============================================================
    //  announcement pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.ann_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#ann_qpage').val( qpage );
            jQuery('#ann_page').val( page );
            jQuery('#ann_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_dashboard_ann();
        }

        return false;
    });

    /*/ ==============================================================
    //  dashboard news
    // ==============================================================/*/
    get_dashboard_news();
    function get_dashboard_news(){
        var ajax_target_wrap = jQuery(".news_ajax");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_news_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: dashboardNewsUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.news_pagination_wrap').html(ret.paginationHtml);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    /*/ ==============================================================
    //  news pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.news_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#news_qpage').val( qpage );
            jQuery('#news_page').val( page );
            jQuery('#news_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_dashboard_news();
        }

        return false;
    });

    /*/ ==============================================================
    //  dashboard faq
    // ==============================================================/*/
    get_dashboard_faq();
    function get_dashboard_faq(){
        var ajax_target_wrap = jQuery(".faq_ajax");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_faq_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: dashboardFaqUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.faq_pagination_wrap').html(ret.paginationHtml);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }

    /*/ ==============================================================
    //  news pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.faq_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#faq_qpage').val( qpage );
            jQuery('#faq_page').val( page );
            jQuery('#faq_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_dashboard_faq();
        }

        return false;
    });

</script>
@stop