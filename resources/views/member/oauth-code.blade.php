@extends('member.layouts.app')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('content')
<!-- <div class="login-logo">
    <img src="{{ asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div> -->

<div class="container">
    <div class="row">
        <div class="login-container">
            <img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/>
            <h2 class="pull-right">BitPal Authentication</h2>
            <div class="clearfix"></div>

            <p>
                
            </p>

            <div class="reset_bitpalbts_auth_wrap" >
                <div class="alert alert-danger">
                    <h4>Authentication Error:</h4>
                    <p><?php echo $msg; ?></p>
                </div>
                <a class="btn theme-btn" href="{{ route('member.tpoBank') }}?tab=addfundform">Back to Dashboard</a>
            </div>
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
@stop
