@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/referral.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
    <div class="header-page">
        <h1>Referral Credits <span></span></h1>
    </div>

    <div class="row">
        <div class="referral-container">
            <div class="col-md-3 col-xs-12">
                <div class="tile-stats-box">
                    <div class="icon-holder"><img src="{{asset('images/earnings.png')}}" alt="" class="icon-holder-img"></div>
                    <span class="count_top"><i class="fa fa-line-chart"></i> Total Referral Credits</span>
                    <div class="count green refcred_overall_total">$<?php echo LpjHelpers::amt2($TotalRefCred); ?></div>
                </div>
                <div class="x_panel tile">
                    <div class="x_title">
                        <h2>Referral Credits Total By Category</h2>
                    </div>
                    <div class="breakdown-box">
                        <h3><i class="fa fa-address-card"></i> 
                            <a href="" class="refcred_filter_btn membership_refcred" data-cat="membership">
                                $<?php echo LpjHelpers::amt2($MSrefCred); ?>
                            </a>
                        </h3>
                        <h4>Membership Referral Credit </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-line-chart"></i> 
                            <a href="" class="refcred_filter_btn cryptotrading_refcred" data-cat="cryptotrading">
                                $<?php echo LpjHelpers::amt2($CTrefCred); ?>
                            </a>
                        </h3>
                        <h4>Crypto Trading Referral Credit </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-diamond"></i> 
                            <a href="" class="refcred_filter_btn cryptomining_refcred" data-cat="cryptomining">
                                $<?php echo LpjHelpers::amt2($CMrefCred); ?>        
                            </a>
                        </h3>
                        <h4>Crypto Mining Referral Credit </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-exchange"></i> 
                            <a href="" class="refcred_filter_btn cryptoexchange_refcred" data-cat="cryptoexchange">
                                $<?php echo LpjHelpers::amt2($CErefCred); ?>
                            </a>
                        </h3>
                        <h4>Crypto Exchange Referral Credit </h4>
                    </div>

                    <div class="breakdown-box">
                        <h3><i class="fa fa-exchange"></i> 
                            <a href="" class="refcred_filter_btn cryptoshuffle_refcred" data-cat="cryptoshuffle">
                                $<?php echo LpjHelpers::amt2($CSrefCred); ?>
                            </a>
                        </h3>
                        <h4>Crypto Shuffle Referral Credit </h4>
                    </div>

                    <div class="breakdown-box">
                        <h5 class="red">NOTE: The amounts displayed above shows only the referral credits that you pushed/withdrawn to your TPO Bank.</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-xs-12">
                <div class="x_panel tile">
                    <div style="display: none;">
                        <div class="x_title">
                            <h2>Search Filter:</h2>
                            <a class="btn theme-btn toggle-search"><i class="fa fa-search" aria-hidden="true"></i></a>
                        </div>
                        <div class="search-form">
                            <form id="user-search" action="{{ route('tpo-bank-transaction.search') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Category</label>
                                            <input class="form-control" name="category" type="text">
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Package Name</label>
                                            <input class="form-control" name="pkg_name" type="text">
                                        </div>
                                        <div class="form-group col-md-4 col-xs-12">
                                            <label>Referral</label>
                                            <input class="form-control" name="referral" type="text">
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date From</label>
                                            <div class='input-group date' id='from-date'>
                                                <input type='text' class="form-control" name="from_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-xs-12">
                                            <label>Date To</label>
                                            <div class='input-group date' id='to-date'>
                                                <input type='text' class="form-control" name="to_date" readonly="readonly" />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn theme-btn" name="submit" type="submit">Filter</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        

                    <div class="x_title">
                        <h2>Referral Credits History:</h2>
                    </div>
                    <div class="referral-box">
                        <div class="table-holder">
                            <table class="table table-striped table-hover">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th>Category</th>
                                        <th>Package Name</th>
                                        <th>Referral</th>
                                        <th>Tier</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody class="referral_credits_wrap">
                                    <tr>
                                        <td colspan="100">No referral Credits Earned</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <p></p>

                        <div class="refcred_pagination_wrap"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->


<div style="display: none;">
    <form id="get_ref_credit_frm">
        {{ csrf_field() }}
        <input type="text" id="rc_package_id" name="packageId" value="">
        <input type="text" id="rc_package_cat" name="packageCat" value="">
        <input type="text" id="rc_qpage" name="qpage" value="1">
        <input type="text" id="rc_page" name="page" value="1">
        <input type="text" id="rc_qlimit" name="qlimit" value="20">
    </form>
</div>

<div style="display: none;">
    <form id="get_ref_credit_pushtobank_frm">
        {{ csrf_field() }}
        <input type="text" id="rcp_package_id" name="packageId" value="">
        <input type="text" id="rcp_package_cat" name="packageCat" value="">
        <input type="text" id="rcp_refcred_id" name="refCredId" value="0">
    </form>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
<script src="{{ asset('js/member/helper.js') }}"> </script>

<script type="text/javascript">
    var refCredUrl = "{{ route('member.getReferralCredits') }}";
    var refCredPushtobankUrl = "{{ route('member.refCredPushToBank') }}";
    

    /*/ ==============================================================
    //  referral credits
    // ==============================================================/*/
    get_referral_credits();
    function get_referral_credits(){
        var ajax_target_wrap = jQuery(".referral_credits_wrap");

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredUrl,
            dataType: 'json',
            data: input_data,
            success: function(ret){
                jQuery( ajax_target_wrap ).html(ret.returnHTML);
                jQuery('.refcred_pagination_wrap').html(ret.paginationHtml);

                jQuery('.referral_credit_total').html(ret.rcTotal);

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });
    }


    /*/ ==============================================================
    //  referral credits pagination
    // ==============================================================/*/
    jQuery(document).on( 'click', '.refcred_pagination_wrap .pagination a', function(){
        var thisElem = jQuery(this);
        var a_link = jQuery(this).attr( 'href' );

        if ( a_link != '#' ) {
            var qpage = lpj_parse_url( a_link, 'qpage' );
            var page = lpj_parse_url( a_link, 'page' );
            var qlimit = lpj_parse_url( a_link, 'qlimit' );
            var sortBy = lpj_parse_url( a_link, 'sortBy' );
            var sortOrder = lpj_parse_url( a_link, 'sortOrder' );

            jQuery('#rc_qpage').val( qpage );
            jQuery('#rc_page').val( page );
            jQuery('#rc_qlimit').val( qlimit );
            // jQuery('#f_sortBy').val( sortBy );
            // jQuery('#f_sortOrder').val( sortOrder );

            get_referral_credits();
        }

        return false;
    });


    jQuery(document).on( 'click', '.refcred_filter_btn', function(){
        var rc_package_cat = jQuery(this).data('cat');

        jQuery('#rc_package_cat').val(rc_package_cat);
        get_referral_credits();
        
        return false;
    });


    /*/ ==============================================================
    //  referral credits push to bank
    // ==============================================================/*/
    jQuery(document).on( 'click', '.pushtobank_btn', function(){
        _this = jQuery(this);
        var category = jQuery(this).data('cat');
        var commId = jQuery(this).data('commid');

        jQuery('#rcp_refcred_id').val( commId );
        jQuery('#rcp_package_cat').val( category );

        show_global_ajax_loader();

        var form_elem = jQuery('#get_ref_credit_pushtobank_frm');
        var input_data = form_elem.serialize();

        jQuery.ajax({
            type: "POST",
            url: refCredPushtobankUrl,
            dataType: 'json',
            data: input_data,
            success: function(rdata){
                if ( rdata.isError ) {
                    show_page_ajax_message( rdata.msg, 'danger' );

                }else{
                    show_page_ajax_message( rdata.msg, 'success' );

                    _this.after('<span>withdrawn</span>')
                        .remove();

                    jQuery('.'+category+'_refcred').text('$'+rdata.rcTotal);
                    jQuery('.refcred_overall_total').text('$'+rdata.rcOverallTotal);
                    // jQuery('.commission_unwithdrawn_total').text('$'+rdata.total_unwithdrawn);
                }

                hide_global_ajax_loader();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                handle_ajax_error(jqXHR, textStatus, errorThrown);
            }
        });

        return false;
    });


</script>
@stop