<html>
    <body>
        <div style="font-family: tahoma, verdana, arial; color:#000000;">
            <h3 style="color: #A5242E; font-size: 20px;">Password Reset Request.</h3>
            <p>Our records indicate you have reset your password.  Below is your login information.  Please keep this in file for future use.</p>
            <p> <strong>Username: </strong> {{ $username }} </p>
            <p> <strong>Password: </strong> {{ $newPass }} </p>
            <p> You can now login with your new password at: <strong><a href="{{ $loginUrl }}">The People's Options</a></strong> </p>
            <br>
            <p>Thank you,</p>
            <p>TPO Team</p>
        </div>
    </body>
</html>