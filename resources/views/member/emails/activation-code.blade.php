<html>
    <body>
        <div style="font-family: tahoma, verdana, arial; color:#000000; ">
            <h3><span style="color: #A5242E; font-size: 20px;">Dear {{$name}},</span></h3>
            <p>Thank you for registering.  Please click the link below to verify your email address. </p>
            <p><span style="color: #A5242E;"><strong>Account activation link: </strong></span> <a href="{{$verificationUrl}}">{{$verificationUrl}}</a> </p>
            <p>Once you have completed this step you can go back to our site, <a href="{{$loginUrl}}">ThePeoplesOptions.com</a>  to login and complete your profile.</p>
            <br>
            <hr>
            <p>Thank you</p>
            <p>TPO Team</p>
        </div>
    </body>
</html>