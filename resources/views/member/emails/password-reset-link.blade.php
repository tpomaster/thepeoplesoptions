<html>
    <body>
        <div style="font-family: tahoma, verdana, arial; color:#000000;">
            <h3 style="color: #A5242E; font-size: 20px;">Password Reset Request.</h3>
            <p>Using this email address, someone requested to reset the password for this account with The Peoples Options.</p>
            <p> <strong>{{ $siteurl }}</strong> </p>
            <p> <strong>Username: </strong> {{ $username }} </p>
            <p>If this was a mistake, just ignore this email and nothing will happen.</p>
            <p>To reset your password, visit this page: <a href="{{ $resetUrl }}">RESET PASSWORD</a></p>
            <br>
            <p>Thank you,</p>
            <p>TPO Team</p>
        </div>
    </body>
</html>