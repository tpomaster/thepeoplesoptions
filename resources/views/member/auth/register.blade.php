@extends('member.layouts.app')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('content')
    <header>
        <a href="http://the-peoples-options.dev/"><img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/></a>
    </header>

    <div class="container register-wrap">
        <div class="container-fluid">
            <div class="col-md-7 nopadding">
                <div class="sponsor-wrap">
                    <h2><span>Welcome to</span>THE PEOPLE'S OPTIONS</h2>
                    <p>Create a free account with The Peopl's Options and have the opportunity to earn through Crypto Mining, Crypto Trading, The CryptoShuffle Ticket, and Referral Commissions. You'll join thousands of other people who share your interest in cryptocurrency! Be a part of the revolution!</p>
                    <p>TPO offers members the following services: </p>
                    <div class="offers clearfix">
                        <img src="{{ LpjHelpers::asset('images/mine.png') }}" alt="" class="img-left">
                        <div class="offer-content">
                            <h5>Cryptocurrency Mining</h5>
                            <p>We negotiate the best packages for mining for you.</p>
                        </div>
                    </div>
                    <div class="offers clearfix">
                        <img src="{{ LpjHelpers::asset('images/register-bitcoin.png') }}" alt="" class="img-left">
                        <div class="offer-content">
                            <h5>Cryptocurrency Trading</h5>
                            <p>We fully manage your accounts. Just decide which packages are right for you, fund them, and let our skilled and experienced team do the trading on your behalf. We offer long term and short term options.</p>
                        </div>
                    </div>
                    <div class="offers clearfix">
                        <img src="{{ LpjHelpers::asset('images/ticket.png') }}" alt="" class="img-left">
                        <div class="offer-content">
                            <h5>The CryptoShuffle Ticket</h5>
                            <p>We believe in giving back. Part of the proceeds from The CryptoShuffle Ticket draws are given to charity.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="register-box-body">
                    <h3 class="login-box-msg"> Register </h3>
                    <?php if ( $errsuc['isError'] == true ): ?>
                        <h3 style="text-align: center; color: red;">
                            You need to have a sponsor <br>to be able to register. <br>Please contact us now.
                        </h3>

                    <?php else: ?>

                        <form class="member_reg_form" role="form" method="POST" action="{{ route('member.register.submit') }}">
                            {{ csrf_field() }}
                            <div class="form-group has-feedback">
                                <input type="text" placeholder="First Name" id="firstname" name="firstname" value="<?php echo $fake_data['firstname']; ?>" />
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="text" placeholder="Last Name" id="lastname" name="lastname" value="<?php echo $fake_data['lastname']; ?>" />
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>

                            <div class="form-group has-feedback">                                            
                                <input type="text" placeholder="Username" id="username" name="username" value="<?php echo $fake_data['username']; ?>">      
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>        
                            </div> <!-- /control-group -->

                            <div class="form-group has-feedback">
                                <input type="email" placeholder="Email" id="email" name="email" value="<?php echo $fake_data['email']; ?>" />
                                <span class="glyphicon glyphicon-envelope form-control-feedback">
                                </span>
                            </div>

                            <div class="form-group has-feedback">
                                <input type="password" placeholder="Password" id="password" name="password" value="<?php echo $fake_data['password']; ?>" />
                                <span class="glyphicon glyphicon-lock form-control-feedback">
                                </span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" placeholder="Retype password" id="confirm_pass" name="confirm_pass" value="<?php echo $fake_data['password']; ?>" />
                                <span class="glyphicon glyphicon-lock form-control-feedback">
                                </span>
                            </div>

                            <div class="checkbox icheck">
                            <label class="">
                                <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                                    <input type="checkbox" id="agree_with_terms" name="agreetoterms" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                                    <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins>
                                </div> &nbsp;
                                I agree to the <a href="" target="_blank">terms</a>
                            </label>
                            </div>

                            <div class="row">
                               
                                <div class="col-xs-12">
                                    <button type="submit" class="btn yellow-btn btn-block btn-flat">
                                        Register
                                    </button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                    <?php endif ?>

                </div>
            </div>
        </div>
    </div>

    @include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script type="text/javascript">
        var ajaxurl = '{{ route('member.register.submit') }}';
    </script>
    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
    <script src="{{ LpjHelpers::asset('js/member/register.js') }}"> </script>
@stop