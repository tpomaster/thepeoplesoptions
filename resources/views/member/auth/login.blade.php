﻿@extends('member.layouts.app')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('content')
<!-- <div class="login-logo">
    <img src="{{ asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div> -->

<div class="container">
    <div class="row">
        <div class="login-container">
            <img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/>
            <h2 class="pull-right">LOGIN</h2>
            <div class="clearfix"></div>

            <?php if ( isset( $verifyMsg )): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><?php echo $verifyMsg; ?></strong>
                </div>
            <?php endif ?>

            <div class="login_step_1_wrap">
                <form class="form-horizontal member_login_form" id="member_login_form" method="POST" action="{{ route('member.login.ajax') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="browser" value="<?php echo $browserCode; ?>">

                    <div class="login-box">
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <div class="field-container">
                                <label>Your username</label>
                                <input id="username" type="text" name="username" value="" required autofocus placeholder="Username">
                                <span class="glyphicon glyphicon-user input-icon"></span>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="field-container">
                                <label>Your password</label>
                                <input id="password" type="password" name="password" value="" required placeholder="Password">
                                <span class="glyphicon glyphicon-lock input-icon"></span>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="field-container row">
                            <div class="col-md-8 text-left nopadding">
                                <p>Have you forgotten your <a class="bold" href="{{ route('member.forgotPassword') }}">password?</a></p>
                                <p><a href="https://thepeoplesoptions.com"><i class="fa fa-home"></i> Back to Home Page</a></p>
                            </div>
                            <div class="col-md-4 text-right nopadding">
                                <button type="submit" class="btn yellow-btn">Login</button>
                            </div>
                        </div>
                        
                    </div>
                </form>
            </div>

            <div class="login_step_2_wrap" style="display: none;">
                <p style="text-align: center; color: red;"> 
                    Please check your email, We sent you a verification code. <br>Verification code will expire after 15 minutes
                </p>

                <h4 style="text-align: center;">
                    Enter Verification Code
                </h4>

                <form id="user_login_step_2_frm" action="" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="browser" value="<?php echo $browserCode; ?>">
        
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Verification Code" id="verification_code" name="verificationCode"/>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"> </span>
                    </div>
                    
                    <div class="row">

                        <div class="col-xs-12">
                            <button type="submit" class="btn yellow-btn btn-block btn-flat">
                                Verify
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
                
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>
    <!-- <script src="{{ LpjHelpers::asset('js/member/login.js') }}"> </script> -->

    <script type="text/javascript">
        var loginStep1Url = "{{ route('member.login.ajax') }}";
        var loginStep2Url = "{{ route('member.login2.ajax') }}";
    </script>

    <script type="text/javascript">
        jQuery('#member_login_form').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: loginStep1Url,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');

                    }else{
                        show_page_ajax_message( result.msg, 'success');

                        jQuery('.login_step_1_wrap').hide();
                        jQuery('.login_step_2_wrap').show();

                        if ( result.redirectUrl != '') {
                            jQuery('.login_step_2_wrap').hide();
                            window.location.href = result.redirectUrl;
                        }
                    }

                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });


        jQuery('#user_login_step_2_frm').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: loginStep2Url,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');
                        hide_global_ajax_loader();

                    }else{
                        show_page_ajax_message( result.msg, 'success');
                        jQuery('.login_step_2_wrap').hide();

                        if ( result.redirectUrl != '') {
                            window.location.href = result.redirectUrl;
                        }
                        
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });

    </script>
@stop
