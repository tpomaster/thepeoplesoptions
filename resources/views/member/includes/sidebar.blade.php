<div class="col-md-3 left_col theme-overide">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="https://thepeoplesoptions.com/member/" class="site_title"><img src="{{LpjHelpers::asset('images/small-logo.png')}}" alt="" class="retain-logo"> <img src="{{LpjHelpers::asset('images/text-logo.png')}}" alt="" class="collapse-logo"> <span> AFFILIATE</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{LpjHelpers::asset('images/avatar.png')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <h2><?php echo $currentUser->username ?></h2>
                <span class="green"><?php echo strtoupper($currentUser->membership); ?></span>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section menu-category">
                <h3>MENU</h3>
                <ul class="nav side-menu">
                    <li><a href="{{ route('member.login.ajax') }}"><i class="fa fa-home"></i> Home </a></li>
                    <li><a href="{{ route('member.myAccount') }}"><i class="fa fa-user"></i> My Account </a></li>
                    <li><a href="{{ route('member.team') }}"><i class="fa fa-group"></i> Team </a></li>
                    <li><a href="{{ route('member.tpoBank') }}"><i class="fa fa-bank"></i> TPO Bank </a></li>
                    <li><a href="{{ route('member.referralCredits') }}"><i class="fa fa-usd"></i> Referral Credits </a></li>
                    <li><a href="{{ route('member.membership-packages') }}"><i class="fa fa-address-card"></i> Membership </a></li>
                </ul>
            </div>
            <div class="menu_section">
                <h3>PACKAGES</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-line-chart"></i> Crypto Trading <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('member.crypto-trading-my-packages-list') }}"> My Funded Packages </a></li>
                            <li><a href="{{ route('member.crypto-trading-packages-available', ['status' => 'available']) }}"> Available Packages </a></li>
                            <li><a href="{{ route('member.crypto-trading-pacakges-list') }}"> All Packages </a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-diamond"></i> Crypto Mining <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('member.cryptoMiningMyPackagesList') }}"> My Funded Packages </a></li>
                            <li><a href="{{ route('member.cryptoMiningPackagesAvailable', ['status' => 'available']) }}"> Available Packages </a></li>
                            <li><a href="{{ route('member.cryptoMiningPackagesList') }}"> All Packages </a></li>
                        </ul>
                    </li>

                    <li><a><i class="fa fa-exchange"></i> Crypto Exchange <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('member.cryptoExchangeMyPackagesList') }}"> My Funded Packages </a></li>
                            <li><a href="{{ route('member.cryptoExchangePackagesAvailable', ['status' => 'available']) }}"> Available Packages </a></li>
                            <li><a href="{{ route('member.cryptoExchangePackagesList') }}"> All Packages </a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-ticket"></i> CryptoShuffle Ticket <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('member.cryptoShufflePackagesList') }}"> Packages </a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="menu_section bottom-side-menu">
                <ul class="nav side-menu">
                    <li><a href="{{ route('member.faqindex') }}"><i class="fa fa-question-circle"></i> FAQ </a></li>
                    <li><a href="{{ url('/member/logout') }}"><i class="fa fa-sign-out"></i> Logout </a></li>
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a href="{{ route('member.tpoBank') }}" data-toggle="tooltip" data-placement="top" title="TPO Bank">
                <i class="fa fa-bank"></i>
            </a>
            <a href="{{ route('member.myAccount') }}" data-toggle="tooltip" data-placement="top" title="My Account">
                <i class="fa fa-user"></i>
            </a>
            <a href="{{ route('member.faqindex') }}" data-toggle="tooltip" data-placement="top" title="Help">
                <i class="fa fa-question-circle"></i>
            </a>
            <a href="{{ url('/member/logout') }}" data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>