<!-- footer content -->
<footer class="ftr-collapse">
	<div class="footer-overidex">
		<div class="col-md-8 col-xs-12 footer-time">
			<div>
	            <p style="font-size: 16px;">
	                <?php /*/ ?>
	                New York Time: <?php echo date( 'M. d Y H:i', current_time( 'timestamp', 0 ) ); ?>
	                <?php /*/ ?>
	                <a href="https://time.is/New_York" id="time_is_link" rel="nofollow" style="font-size:24px">Time in New York:</a>
	                <span id="New_York_z161" style="font-size:24px"></span>
	                <script src="//widget.time.is/en.js"></script>
	                <script>
	                    time_is_widget.init({New_York_z161:{template:"DATE TIME", time_format:"12hours:minutes:seconds AMPM", date_format:"dayname, monthname dnum, year"}});
	                </script>
	            </p>

                <p>
                    Server Time: <?php echo date( 'M. d Y H:i'); ?>
                </p>
	        </div>

		</div>

		<div class="col-md-4 col-xs-12 footer-copyright">
		 	Copyright &copy; <?php echo date('Y'); ?> The Peoples Options . All rights reserved.
		</div>
		<div class="clearfix"></div>
	</div>
</footer>
<!-- /footer content -->