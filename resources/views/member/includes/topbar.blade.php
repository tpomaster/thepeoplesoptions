﻿<!-- top navigation -->
<div class="top_nav top-overide">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Hi, <?php echo $currentUser->firstname ?>. Welcome to your dashboard.
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ route('member.myAccount') }}"> Profile</a></li>
                        <!-- <li>
                            <a href="{{ route('member.tpoBank') }}">
                                <span class="badge bg-noti pull-right">50%</span>
                                <span>Settings</span>
                            </a>
                        </li> -->
                        <li><a href="{{ route('member.tpoBank') }}">TPO Bank</a></li>
                        <li><a href="{{ url('/member/logout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <!-- <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-noti">6</span>
                    </a> -->
                    <ul id="menu1" class="dropdown-menu notif-list list-unstyled msg_list" role="menu">
                        <li>
                            <a>
                                <span><i class="fa fa-info-circle"></i> You have <span class="notif-value">1</span> unwithdrawn Crypto Trading Profit.</span>
                            </a>
                        </li>
                        <li>
                            <a>
                                <span><i class="fa fa-info-circle"></i> You have <span class="notif-value">21</span> unwithdrawn Referral Credit.</span>
                            </a>
                        </li>
                        <li>
                            <div class="text-center">
                                <a>
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li id="top-countdown">
                    <i class="fa fa-hourglass-half"></i> TPO countdown for the next posting of earnings: <span></span>

                    <span class="header_countdown_wrap">
                        <?php 
                        $c_orig_time = '23:59:59';
                        $c_orig_time = strtotime($c_orig_time);

                        $c_current_time = date('G:i:s');
                        $c_current_time2 = strtotime($c_current_time) - 18000;
                        $c_start_time = $c_orig_time - $c_current_time2;
                        ?>
                        <input type="hidden" id="header_countdown_clock" placeholder="00:00:00" value="<?php echo date( 'G:i:s', $c_start_time ); ?>">
                        <span id="header_countdown"></span>
                        <span style="display: none;">
                            <?php echo date('Y-m-d H:i:s', $c_current_time2); ?>
                        </span>
                    </span>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->
