@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/tree-view.css') }}">

@stop

@section('main_container')
	<!-- page content -->
        <div class="right_col content-overide" role="main">
        	<div class="header-page">
				<h1>Team Tree View</h1>
			</div>

			<div class="row">
				<div class="col-md-12 col-s-12 col-xs-12">
					<div id="treeView-content">
						<div class="headerRow clearfix">
							<div class="blankLabel"></div>
							<div class="userNameLabel">Username</div>
							<div class="firstNameLabel">First Name</div>
							<div class="lastNameLabel">Last Name</div>
							<div class="dateRegLabel">Date Registered</div>
							<div class="membershipLabel">Membership</div>
						</div>
						<div class="parentRow">
							<!-- level one -->
							<div class="levelOne clearfix">
								<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 1</span></div>
								<div class="tableUserName">Pickel1</div>
								<div class="tableFirstName">Johnny</div>
								<div class="tableLastName">KlingBiel</div>
								<div class="tableDateReg">6 / 8 / 2017</div>
								<div class="tableMembership">Silver</div>
								<div class="clear"></div>

								<!-- level 2 -->
								<div class="levelTwo child clearfix">
									<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 2</span></div>
									<div class="tableUserName">Pickel1</div>
									<div class="tableFirstName">Johnny</div>
									<div class="tableLastName">KlingBiel</div>
									<div class="tableDateReg">6 / 8 / 2017</div>
									<div class="tableMembership">Silver</div>
									<div class="clear"></div>

									<!-- level 3 -->
									<div class="levelThree child clearfix">
										<div class="tableBar"><i></i> <span>Tier 3</span></div>
										<div class="tableUserName">Pickel1</div>
										<div class="tableFirstName">Johnny</div>
										<div class="tableLastName">KlingBiel</div>
										<div class="tableDateReg">6 / 8 / 2017</div>
										<div class="tableMembership">Gold</div>
										<div class="clear"></div>
									</div>

									<!-- level 3 -->
									<div class="levelThree child clearfix">
										<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 3</span></div>
										<div class="tableUserName">Pickel1</div>
										<div class="tableFirstName">Johnny</div>
										<div class="tableLastName">KlingBiel</div>
										<div class="tableDateReg">6 / 8 / 2017</div>
										<div class="tableMembership">Gold</div>
										<div class="clear"></div>

										<!-- level 4 -->
										<div class="levelFour child clearfix">
											<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 4</span></div>
											<div class="tableUserName">Pickel1</div>
											<div class="tableFirstName">Johnny</div>
											<div class="tableLastName">KlingBiel</div>
											<div class="tableDateReg">6 / 8 / 2017</div>
											<div class="tableMembership">Platinum</div>
											<div class="clear"></div>

											<!-- level 5 -->
											<div class="levelFive child clearfix">
												<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 5</span></div>
												<div class="tableUserName">Pickel1</div>
												<div class="tableFirstName">Johnny</div>
												<div class="tableLastName">KlingBiel</div>
												<div class="tableDateReg">6 / 8 / 2017</div>
												<div class="tableMembership">Platinum</div>
												<div class="clear"></div>

												<!-- level 6 -->
												<div class="levelSix child clearfix">
													<div class="tableBar"><i></i> <span>Tier 6</span></div>
													<div class="tableUserName">Pickel1</div>
													<div class="tableFirstName">Johnny</div>
													<div class="tableLastName">KlingBiel</div>
													<div class="tableDateReg">6 / 8 / 2017</div>
													<div class="tableMembership">Silver</div>
												</div>

												<!-- level 6 -->
												<div class="levelSix child clearfix">
													<div class="tableBar"><i></i> <span>Tier 6</span></div>
													<div class="tableUserName">Pickel1</div>
													<div class="tableFirstName">Johnny</div>
													<div class="tableLastName">KlingBiel</div>
													<div class="tableDateReg">6 / 8 / 2017</div>
													<div class="tableMembership">Gold</div>
												</div>

												<!-- level 6 -->
												<div class="levelSix child clearfix">
													<div class="tableBar"><i></i> <span>Tier 6</span></div>
													<div class="tableUserName">Pickel1</div>
													<div class="tableFirstName">Johnny</div>
													<div class="tableLastName">KlingBiel</div>
													<div class="tableDateReg">6 / 8 / 2017</div>
													<div class="tableMembership">Silver</div>
												</div>
											</div>

											<!-- level 5 -->
											<div class="levelFive child clearfix">
												<div class="tableBar"><i></i> <span>Tier 5</span></div>
												<div class="tableUserName">Pickel1</div>
												<div class="tableFirstName">Johnny</div>
												<div class="tableLastName">KlingBiel</div>
												<div class="tableDateReg">6 / 8 / 2017</div>
												<div class="tableMembership">Gold</div>
												<div class="clear"></div>
											</div>

											<!-- level 5 -->
											<div class="levelFive child clearfix">
												<div class="tableBar"><i></i> <span>Tier 5</span></div>
												<div class="tableUserName">Pickel1</div>
												<div class="tableFirstName">Johnny</div>
												<div class="tableLastName">KlingBiel</div>
												<div class="tableDateReg">6 / 8 / 2017</div>
												<div class="tableMembership">Platinum</div>
												<div class="clear"></div>
											</div>
										</div>

										<!-- level 4 -->
										<div class="levelFour child clearfix">
											<div class="tableBar click"><i></i> <span>Tier 4</span></div>
											<div class="tableUserName">Pickel1</div>
											<div class="tableFirstName">Johnny</div>
											<div class="tableLastName">KlingBiel</div>
											<div class="tableDateReg">6 / 8 / 2017</div>
											<div class="tableMembership">Platinum</div>
											<div class="clear"></div>
										</div>
									</div>

									<!-- level 3 -->
									<div class="levelThree child clearfix">
										<div class="tableBar"><i></i> <span>Tier 3</span></div>
										<div class="tableUserName">Pickel1</div>
										<div class="tableFirstName">Johnny</div>
										<div class="tableLastName">KlingBiel</div>
										<div class="tableDateReg">6 / 8 / 2017</div>
										<div class="tableMembership">Gold</div>
										<div class="clear"></div>
									</div>
								</div>

								<!-- level 2 -->
								<div class="levelTwo child clearfix">
									<div class="tableBar"><i></i> <span>Tier 2</span></div>
									<div class="tableUserName">Pickel1</div>
									<div class="tableFirstName">Johnny</div>
									<div class="tableLastName">KlingBiel</div>
									<div class="tableDateReg">6 / 8 / 2017</div>
									<div class="tableMembership">Silver</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
						<div class="parentRow">
							<!-- level one -->
							<div class="levelOne clearfix">
								<div class="tableBar"><i></i> <span>Tier 1</span></div>
								<div class="tableUserName">Pickel1</div>
								<div class="tableFirstName">Johnny</div>
								<div class="tableLastName">KlingBiel</div>
								<div class="tableDateReg">6 / 8 / 2017</div>
								<div class="tableMembership">Silver</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="parentRow">
							<!-- level one -->
							<div class="levelOne clearfix">
								<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 1</span></div>
								<div class="tableUserName">Pickel1</div>
								<div class="tableFirstName">Johnny</div>
								<div class="tableLastName">KlingBiel</div>
								<div class="tableDateReg">6 / 8 / 2017</div>
								<div class="tableMembership">Silver</div>
								<div class="clear"></div>

								<!-- level 2 -->
								<div class="levelTwo child clearfix">
									<div class="tableBar click"><i class="fa fa-angle-double-right" aria-hidden="true"></i> <span>Tier 2</span></div>
									<div class="tableUserName">Pickel1</div>
									<div class="tableFirstName">Johnny</div>
									<div class="tableLastName">KlingBiel</div>
									<div class="tableDateReg">6 / 8 / 2017</div>
									<div class="tableMembership">Gold</div>
									<div class="clear"></div>

									<!-- level 3 -->
									<div class="levelThree child clearfix">
										<div class="tableBar"><i></i> <span>Tier 3</span></div>
										<div class="tableUserName">Pickel1</div>
										<div class="tableFirstName">Johnny</div>
										<div class="tableLastName">KlingBiel</div>
										<div class="tableDateReg">6 / 8 / 2017</div>
										<div class="tableMembership">Gold</div>
										<div class="clear"></div>
									</div>

									<!-- level 3 -->
									<div class="levelThree child clearfix">
										<div class="tableBar"><i></i> <span>Tier 3</span></div>
										<div class="tableUserName">Pickel1</div>
										<div class="tableFirstName">Johnny</div>
										<div class="tableLastName">KlingBiel</div>
										<div class="tableDateReg">6 / 8 / 2017</div>
										<div class="tableMembership">Gold</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- end page content -->
@endsection