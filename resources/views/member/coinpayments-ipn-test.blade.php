@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ asset('css/member/my-account.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ajax.css') }}">

@stop

@section('main_container')
<!-- page content -->
<div class="right_col content-overide" role="main">
  <div class="header-page">
    <h1>Test IPN</h1>
  </div>

  <div class="row">
    <div class="col-md-12 col-xs-12">

        <div class="col-md-12 col-xs-12">
          <div class="tab-title">
            <h2>Coinpayments</h2>
          </div>
          <div class="account-form">

            <form id="update_personal_info_frm" >
              
              <div class="row">
                <?php foreach ($ipn as $k => $v): ?>
                  <div class="col-md-4 col-xs-12">
                    <div class="form-group">
                      <label><?php echo $k ?></label>
                      <input class="form-control" type="text" name="<?php echo $k ?>" value="<?php echo $v ?>" id="first-name">
                    </div>
                  </div>
                <?php endforeach ?>
              </div>

              <div class="row">
                <div class="col-md-4 col-xs-12">
                  <div class="form-group">
                    <button type="submit" class="btn theme-btn">Submit</button>
                  </div>
                </div>
              </div>
            </form>

          </div>
        </div>
                    
    </div>
  </div>
</div>

@include('member/includes/ajax-loader')

<!-- end page content -->
@endsection


@section('pagespecificscripts')
    <script type="text/javascript">
        var ajaxurl = "{{ route('member.processCoinpaymentsIpn') }}";
    </script>
    <script src="{{ asset('js/member/helper.js') }}"> </script>
    <script type="text/javascript">

        jQuery('#update_personal_info_frm').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');

                    }else{
                        show_page_ajax_message( result.msg, 'success');
                    }

                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });

    </script>
@stop