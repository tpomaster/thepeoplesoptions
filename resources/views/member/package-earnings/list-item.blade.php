
<?php if ( !empty($pkgEarnings) ): ?>
    <?php foreach ($pkgEarnings as $earning){
        $note = json_decode($earning->note);
        ?>
        <tr class="refcred_item_<?php echo $earning->id; ?>">
            <td><?php echo $earning->package_cat; ?></td>
            <td>
                <?php if ( isset($pkgNames[$earning->package_cat][$earning->package_id]) ): ?>
                    <?php echo $pkgNames[$earning->package_cat][$earning->package_id] ?>

                <?php else: ?>
                    <?php if ( $earning->package_cat == 'membership'): ?>
                        Platinum Membership
                    <?php else: ?>
                        <?php echo $earning->package_id; ?>
                    <?php endif ?>
                <?php endif ?>
            </td>
            <td>$<?php echo LpjHelpers::amt2($earning->amount); ?></td>
            <td><?php echo $earning->withdraw_date; ?></td>
        </tr>
    <?php } ?>

<?php else: ?>
    <tr>
        <td colspan="100">
            No Reslult
        </td>
    </tr>
<?php endif ?>
    
