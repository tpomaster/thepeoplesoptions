@extends('member.layouts.app')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/ajax.css') }}">

@stop

@section('content')
<div class="login-logo">
    <img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div>

<div class="container">
    <div class="row">
        <div class="login-container">
            <h2>RESET PASSWORD</h2>

            <?php if ( $isError ): ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><?php echo $msg; ?></strong>
                </div>

                <div class="">
                    <p><a href="{{ route('member.login') }}"><i class="fa fa-sign-in"></i> Login</a></p>
                    <p><a href="/"><i class="fa fa-home"></i> Back to Home Page</a></p>
                </div>

            <?php else: ?>
                <form class="form-horizontal retrieve_pass_form" method="POST" action="">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="<?php echo $token; ?>" >
                    <input type="hidden" name="email" value="<?php echo $keyId; ?>" >

                    <div class="login-box">
                        <div class="retrieve_fields">
                            <div class="form-group has-feedback">
                                <div class="field-container">
                                    <label>Your password</label>
                                    <input type="password" class="form-control" placeholder="Password" id="password1" name="password1"/>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"> </span>
                                </div>
                            </div>
                           
                            <div class="form-group has-feedback">
                                <div class="field-container">
                                    <label>Confirm password</label>
                                    <input type="password" class="form-control" placeholder="Confirm Password" id="password2" name="password2"/>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"> </span>
                                </div>
                            </div>

                            <div class="field-container row">
                                <div class="col-md-8 text-left nopadding">
                                    <p><a href="{{ route('member.login') }}"><i class="fa fa-sign-in"></i> Login</a></p>
                                    <p><a href="/"><i class="fa fa-home"></i> Back to Home Page</a></p>
                                </div>
                                <div class="col-md-4 text-right nopadding">
                                    <button type="submit" class="btn yellow-btn">Send</button>
                                </div>
                            </div>

                            <span class="clearing"></span>
                        </div>
                            
                        <div class="login_link_wrap" style="display: none; text-align: center;">
                            <p style="color: #FFF;">
                                You can now login using your new password.
                            </p>
                            <p><a href="{{ route('member.login') }}" class="btn yellow-btn"><i class="fa fa-sign-in"></i> Login</a></p>
                        </div>
                    </div>

                </form>
            <?php endif ?>

        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script src="{{ LpjHelpers::asset('js/member/helper.js') }}"> </script>

    <script type="text/javascript">
        var ajaxurl = "{{ route('member.setNewPassword.submit') }}";

        jQuery('.retrieve_pass_form').submit(function(){
            var form_elem = jQuery(this);
            var input_data = form_elem.serialize();

            show_global_ajax_loader();

            jQuery.ajax({
                type: "POST",
                url: ajaxurl,
                dataType: 'json',
                data: input_data,
                success: function( result ){
                    
                    if( result.isError == true ){
                        show_page_ajax_message( result.msg, 'danger');

                    }else{
                        show_page_ajax_message( result.msg, 'success');
                        jQuery('.retrieve_fields').html('');
                        jQuery('.login_link_wrap').show();
                    }

                    hide_global_ajax_loader();
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    handle_ajax_error(jqXHR, textStatus, errorThrown);
                }
            });

            return false;
        });
    </script>
@stop