@extends('member.layouts.blank')

@section('pagespecificstyles')

    <!-- my account css-->
    <link rel="stylesheet" href="{{ LpjHelpers::asset('css/member/faq.css') }}">

@stop

@section('main_container')
  <!-- page content -->
  <div class="right_col content-overide" role="main">
    <div class="header-page">
      <h1>FAQ</h1>
    </div>

    <div class="row">
      <div class="faq-container">
        <div class="col-md-3 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Questions</h2>
            </div>
            <ul class="list-group">
              <li class="list-group-item"><a>How to Transfer Funds from Bitshares to Another Cryptocurrency Wallet</a></li>
              <li class="list-group-item"><a>How to Enroll into TPO Membership</a></li>
              <li class="list-group-item"><a>How to Withdraw Funds</a></li>
              <li class="list-group-item"><a>Adding Funds Using Your Bitshares Wallet</a></li>
              <li class="list-group-item"><a>How to Push Funds from Closed TPO Packages to Your TPO Bank</a></li>
              <li class="list-group-item"><a>How to Link Your Bitshares Wallet to Your TPO Account</a></li>
              <li class="list-group-item"><a>How to Fund a TPO Package</a></li>
              <li class="list-group-item"><a>How to Create a Bitshares Wallet</a></li>
              <li class="list-group-item"><a>How to Access TPO Marketing Site</a></li>
              <li class="list-group-item"><a>How to Push TPO Earnings to Your TPO Bank</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-9 col-xs-12">
          <div class="x_panel tile">
            <div class="x_title">
              <h2>Answer</h2>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->
@endsection