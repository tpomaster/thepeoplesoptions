@extends('layout.layout', ['body_class' => 'inner-page membership-packages-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>Membership Packages</h1>
		</div>
	</div>
	<div id="inner-content">
		<!-- packages -->
    	@include('includes/packages')

		<div class="container">
			<h2 class="fancy-h2">Platinum Membership</h2>
			<p>Platinum Membership at TPO is the best option for members. This level gives you access to all benefits, including the ability to make daily withdrawals and no limit to the amount of funds you can withdraw per month. You also enjoy the lowest withdrawal fee at 2%. You can earn commission from your personal referrals, as well as on Tier 2 and Tier 3 Mining and Trading Packages. Platinum members can also earn from Tiers 2-6 for The CryptoShuffle Ticket. Amount of Tiers vary on special packages.</p>
			<p>Platinum members also have the advantage of the lowest management fees at up to 25%. Fees may vary depending on the type of package. <strong>As a Platinum member</strong>, you also receive a free Bitshares registration, as well as full and complete access to our Content Management System (CMS) to help you invite new members to TPO. Platinum membership comes with special packages, including $150 funding in the Membership Mining Pool (MMP) Package, $150 funding in the Membership Trading Pool (MTP) Package, and $300 in The CryptoShuffle Ticket purchases.</p>
			<p>Platinum members are eligible to receive a Platinum Bonus. When a Platinum member refers 3 other Platinum members they will receive a $1,500 bonus. The bonus will be received for every 3 Platinum members referred and/or renewed.</p>

			<h2 class="fancy-h2">Gold Membership</h2>
			<p>Gold Membership allows you to make weekly withdrawals from your account, totaling up to the amount of $3000 every month. Withdrawal fees for Gold members are only 3%. You can earn commission from your personal referrals, as well as on Tier 2 and Tier 3 Mining and Trading Packages. Gold members can also earn from Tiers 2-6 for The CryptoShuffle Ticket. Amount of Tiers vary on special packages. </p>
			<p>The management fee for Gold membership is up to only 30%. Fees may vary depending on the type of package. <strong>As a Gold member</strong>, you also receive a free Bitshares registration, as well as medium level access to our Content Management System (CMS) (please see below image for details) to help you invite new members to TPO. Gold membership comes with special packages, including $50 funding in the Membership Mining Pool (MMP) Package, $50 funding in the Membership Trading Pool (MTP) Package, and $100 in The CryptoShuffle Ticket purchases.</p>

			<h2 class="fancy-h2">Silver Membership</h2>
			<p>Silver Membership allows you to make two withdrawals per month, amounting to a total of up to $2000. Withdrawal fees for Silver Members is at 4%. You can earn commission from your personal referrals, as well as on Tier 2 and Tier 3 Mining and Trading Packages. </p>
			<p>Silver members can also earn from Tiers 2-6 for The CryptoShuffle Ticket. Amount of Tiers vary on special packages.</p>
			<p>The management fee for Silver Membership is up to 35%. Fees may vary depending on the type of package. <strong>As a Silver member</strong>, you also receive a free Bitshares registration, as well as partial access to our Content Management System (CMS) (please see below image for details) to help you invite new members to TPO. Silver membership comes with special packages, including $15 funding in the Membership Mining Pool (MMP) Package, $15 funding in the Membership Trading Pool (MTP) Package, and $30 in The CryptoShuffle Ticket purchases.</p>

			<h2 class="fancy-h2">Bronze Membership</h2>
			<p>TPO's free membership package allows you to make one withdrawal per month, limited to the amount of $1000 with a 5% withdrawal fee. As a Bronze Member, you can earn partially from your personal referrals only. Bronze members only earn commissions on The CryptoShuffle Ticket purchases and package purchases. They do not earn commissions for membership package purchases. </p>
			<p>The management fee for Bronze membership is up to 45%. Fees may vary depending on the type of package. Bronze Membership also comes with free Bitshares registration but no Content Management System (CMS) access.</p>
			
			<div class="box-infos clearfix">
				<div class="inr-left">
					<h3 class="heading-h3">MEMBERSHIP MINING POOL (MMP) PACKAGE</h3>
					<div class="list-box">
						<ul>
							<li>The included amount with your membership level will go into the MMP</li>
							<li>This package lasts for nine (9) months</li>
							<li>Pays out daily</li>
							<li>Funding amount will be returned to the member when the package expires in nine (9) months</li>
						</ul>
					</div>
				</div>
				<div class="inr-right">
					<h3 class="heading-h3">MEMBERSHIP TRADING POOL (MTP) PACKAGE</h3>
					<div class="list-box">
						<ul>
							<li>The included amount with your membership level will go into the MTP</li>
							<li>This package lasts for ninety (90) days</li>
							<li>Pays out daily</li>
							<li>Funding amount will be returned to the member when the package expires in ninety (90) days</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="table-overflow">
				<table class="responsive-table">
					<tr>
						<th class="detail-title">MEMBERSHIP FEATURES</th>
						<th class="bronze-member">Bronze<span>FREE</span></th>
						<th class="silver-member">Silver<span>$ 150 YEARLY</span></th>
						<th class="gold-member">Gold<span>$ 500 YEARLY</span></th>
						<th class="platinum-member">Platinum<span>$ 1500 YEARLY</span></th>
					</tr>
					<tr>
						<td>Withdrawals Allowed Per Month*</td>
						<td>1</td>
						<td>2</td>
						<td>Weekly</td>
						<td>Daily</td>
					</tr>
					<tr>
						<td>Maximum Amount of Withdrawal per Month**</td>
						<td>1000</td>
						<td>2000</td>
						<td>3000</td>
						<td>Unlimited</td>
					</tr>
					<tr>
						<td>Earn from Personal Referrals***</td>
						<td>Partial</td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Earn from Tiers****</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td>2 and 3</td>
						<td>2 and 3</td>
						<td>2 and 3</td>
					</tr>
					<tr>
						<td>Withdrawal Fee</td>
						<td>5%</td>
						<td>4%</td>
						<td>3%</td>
						<td>2%</td>
					</tr>
					<tr>
						<td>Management Fee*****</td>
						<td>Up to 45%</td>
						<td>Up to 35%</td>
						<td>Up to 30%</td>
						<td>Up to 25%</td>
					</tr>
					<tr>
						<td>BTS Registration</td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Content Management System (CMS)</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Basic Capture and Lead Pages</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Advanced Capture and Lead Pages</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td>1 each</td>
						<td>All</td>
					</tr>
					<tr>
						<td>Create Your Own Capture and Lead Pages</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Contacts</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td>100</td>
						<td>500</td>
						<td>Unlimited</td>
					</tr>
					<tr>
						<td>Emails per Month</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td>400/month</td>
						<td>2000/month</td>
						<td>Unlimited</td>
					</tr>
					<tr>
						<td>Autoresponder Emails Provided</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td>4</td>
						<td>8</td>
						<td>16</td>
					</tr>
					<tr>
						<td>Broadcast to all Your Contacts</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
					<tr>
						<td>Membership Mining Pool Package Included</td>
						<td>0</td>
						<td>15</td>
						<td>50</td>
						<td>150</td>
					</tr>
					<tr>
						<td>Membership Trading Pool Package Included</td>
						<td>0</td>
						<td>15</td>
						<td>50</td>
						<td>150</td>
					</tr>
					<tr>
						<td>The CryptoShuffle Ticket Included</td>
						<td>0</td>
						<td>30</td>
						<td>100</td>
						<td>300</td>
					</tr>
					<tr>
						<td>Platinum Bonus******</td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-times-circle-o" aria-hidden="true"></i></td>
						<td><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
					</tr>
				</table>
			</div>

			<p><span class="red">*</span> FREE members can take one (1) withdrawal on the 1st day of month only. Silver members can take two (2) withdrawals one between the 1st and the 14th and the second between the 15th and the end of month only. Gold members can take withdrawals once a week only. Platinum members can have unlimited withdrawals.</p>
			<p><span class="red">**</span> FREE members are limited to $1000 per month. Silver members are allowed two (2) withdrawals per month but the TOTAL limit for BOTH withdrawals combined is $2,000 per month. Gold members are allowed weekly withdrawals per month but the TOTAL limit for ALL withdrawals combined is $3,000 per month. Platinum members are unlimited.</p>
			<p><span class="red">***</span> Free members only earn commissions on The CryptoShuffle Ticket purchases and package purchases. They do not earn commissions for membership package purchases.</p>
			<p><span class="red">****</span> The amount of tiers may vary on special packages. Silver members and above will earn from all tiers on that special package and all tiers of The CryptoShuffle Ticket</p>
			<p><span class="red">*****</span> This management fee is locked in at the time of funding the package. If you upgrade your membership level after you fund a package, the new management fee does not become active until you fund another package. This fee may vary based on the type of package.</p>
		</div>
	</div>
@endsection

@section('pagespecificscripts')
    <script type="text/javascript">
        // responsive toggle
        jQuery(document).ready(function() {
            if( jQuery(this).width() <= 1340 ) {
              jQuery(".togle-list").hide();
              jQuery(".mermbership-column .info-header").addClass("toggle-info");
            }

            // membership toggle
            jQuery(".info-header.toggle-info").click(function(){
              jQuery( this ).parent().siblings().find('.togle-list').slideUp();
              jQuery( this ).siblings('.togle-list').slideToggle("slow");
            });
        });

        // membership toggle
        jQuery(window).resize(function() {
          if( jQuery(this).width() <= 1340 ) {
            jQuery(".togle-list").hide();
            jQuery(".mermbership-column .info-header").addClass("toggle-info");
          } else {
            jQuery(".togle-list").show();
            jQuery(".mermbership-column .info-header").removeClass("toggle-info");
          }
        });
    </script>
@stop