@extends('layout.layout', ['body_class' => 'home-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/slick-theme.css') }}"/>

@stop

@section('main_content')
    <!-- membership -->
    @include('includes/banner')
    
    <!-- membership -->
    @include('includes/membership')

    <!-- packages -->
    @include('includes/packages')

    <div id="content">
        <div id="graph-one" class="graph1-animate">
            <div id="graph-two" class="graph2-animate">
                
            </div>
        </div>
        <div class="container">
            <div id="parallax-box" class="parallax-container">
                <div id="par1" class="parallax box1"></div>
                <div id="par2" class="parallax box2"></div>
                <div id="par3" class="parallax box3"></div>
                <div id="par4" class="parallax box4"></div>
                <div id="par5" class="parallax box5"></div>
                <div id="par6" class="parallax box6"></div>
                <div id="par7" class="parallax trading-box">
                    <div class="trade-head clearfix">
                        <div class="crypto-key">
                            <img src="{{ LpjHelpers::asset('images/crypto-key.png') }}" alt="">
                        </div>
                        <div class="head-mid">
                            <img src="{{ LpjHelpers::asset('images/graph-logo.png') }}" alt="">
                            <h2>TPO Crypto T</h2>
                        </div>
                        <div class="graph-top-info">
                            <h3>Membership</h3>
                            <h4>PREMIUM</h4>
                        </div>
                    </div>
                    <div class="trade-info">
                        <ul>
                            <li class="package-status clearfix">
                                <span class="package-title left">Package Status:</span>
                                <span class="package-info fund-active right">FUNDED-ACTIVE</span>
                            </li>
                            <li class="clearfix">
                                <span class="package-title left">Total amount of package:</span>
                                <span class="package-info right">Indefinite</span>
                            </li>
                        </ul>
                        <div id="pkg-chart-1" class="line-graph"></div>
                        <div class="clearfix">
                            <a class="btn theme-btn clearfix" href="#"><i class="fa fa-search-plus"></i><span style="display: none;">Graph Button Link</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-bottom">
                <h1>What does The People's Options offer that other platforms can't?</h1>
                <ul>
                    <li>Members can join for FREE and earn from referrals and packages</li>
                    <li>Start with The CryptoShuffle Ticket for as low as $1.00</li>
                </ul>
                <ul>
                    <li>Start with Crypto Mining or Trading package for as low as $10.00</li>
                    <li>A managed account - Each member will have their own back office and full control of their individual funds.</li>
                </ul>
                <!-- <a class="cb-button" href="">CHOOSE PACKAGES</a> -->
            </div>
        </div>
    </div>
    <!-- paertners -->
    @include('includes/partners')

    <!-- testimonials -->
    @include('includes/testimonials')

    <!-- news -->
    @include('includes/news')

    <!-- contact us section -->
    @include('includes/contact-section')

@endsection

@section('pagespecificscripts')
    <script type="text/javascript" src="{{ LpjHelpers::asset('js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ LpjHelpers::asset('js/skrollr.min.js') }}"></script>
    <script type="text/javascript" src="{{ LpjHelpers::asset('js/scrollreveal.min.js') }}"></script>
    <script src="https://code.highcharts.com/stock/highstock.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // slider
            jQuery('.bnr-slider').slick({
              dots: true,
              infinite: true,
              autoplay: true,
              speed: 500,
              fade: true,
              cssEase: 'linear'
            });

            jQuery('.testimonial-slider').slick({
              dots: true,
              infinite: true,
              autoplay: true,
              speed: 500,
              fade: true,
              cssEase: 'linear'
            });

            jQuery('.partners-slider').slick({
              dots: false,
              infinite: true,
              autoplay: true,
              speed: 500,
              slidesToShow: 6,
              responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 3,
                    infinite: true,
                    dots: true
                  }
                },
                {
                  breakpoint: 600,
                  settings: {
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1
                  }
                }
              ]
            });
        });

        // responsive toggle
        jQuery(document).ready(function() {
            if( jQuery(this).width() <= 1340 ) {
              jQuery(".togle-list").hide();
              jQuery(".mermbership-column .info-header").addClass("toggle-info");
            }

            // membership toggle
            jQuery(".info-header.toggle-info").click(function(){
              jQuery( this ).parent().siblings().find('.togle-list').slideUp();
              jQuery( this ).siblings('.togle-list').slideToggle("slow");
            });
        });

        // membership toggle
        jQuery(window).resize(function() {
          if( jQuery(this).width() <= 1340 ) {
            jQuery(".togle-list").hide();
            jQuery(".mermbership-column .info-header").addClass("toggle-info");
          } else {
            jQuery(".togle-list").show();
            jQuery(".mermbership-column .info-header").removeClass("toggle-info");
          }
        });

        // parallax scroll
        window.onload = function() {
            skrollr.init({
                forceHeight: false
            });
        }

        var s = skrollr.init({          
            mobileCheck: function() {
                return false;
            }
        });

        // graph
        jQuery.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=?', function (data) {

            // Create the chart 1
            Highcharts.stockChart('pkg-chart-1', {


                rangeSelector: {
                    enabled: false
                },

                scrollbar: {
                    enabled: false
                },

                navigator: {
                    enabled: false
                },

                title: {
                    text: 'Package Earnings Chart'
                },

                series: [{
                    name: 'Earning',
                    data: data,
                    tooltip: {
                        valueDecimals: 2
                    }
                }]
            });
        });

        // graph animation
        jQuery("#content").hover(
          function () {
            jQuery("#graph-one").removeClass("graph1-animate");
            jQuery("#graph-two").removeClass("graph2-animate");
          },
          function () {
            jQuery("#graph-one").addClass("graph1-animate");
            jQuery("#graph-two").addClass("graph2-animate");
          }
        );


        jQuery('#content #graph-one').mousemove(function(e){
            var amountMovedX = (e.pageX * -1 / 15);
            var amountMovedY = (e.pageY * 1 / 50);
            jQuery(this).css('background-position', amountMovedX + 'px ' + amountMovedY + 'px');
        });

        jQuery('#content #graph-two').mousemove(function(e){
            var amountMovedX = (e.pageX * -1 / 5);
            var amountMovedY = (e.pageY * 1 / 32);
            jQuery(this).css('background-position', amountMovedX + 'px ' + amountMovedY + 'px');
        });

        // section reveal
        window.sr = ScrollReveal();
        sr.reveal('header', { duration: 1000 ,origin: 'top',mobile: false,reset: true});
        // sr.reveal('#banner', { duration: 1000 ,origin: 'bottom',mobile: false,reset: true});
        sr.reveal('#membership-section', { duration: 1000 ,origin: 'bottom',mobile: false,reset: true});
        sr.reveal('#packages-section', { duration: 1000 ,origin: 'right',mobile: false,reset: true});
        sr.reveal('.mermbership-column', { duration: 2000 ,origin: 'top',mobile: false,reset: true}, 100);
        sr.reveal('#content', { duration: 1000 ,origin: 'bottom',mobile: false,reset: true});
        sr.reveal('.parallax-container', { duration: 1500 ,origin: 'left',mobile: false,reset: true}, 100);
        sr.reveal('.content-bottom', { duration: 2000 ,origin: 'right',mobile: false,reset: true}, 150);
        sr.reveal('#partners-section', { duration: 1000 ,origin: 'top',mobile: false,reset: true});
        //sr.reveal('.partners-icons', { duration: 100 ,origin: 'left',mobile: false,reset: true}, 100);
        sr.reveal('#testimonial-section', { duration: 1000 ,origin: 'bottom',mobile: false,reset: true});
        sr.reveal('.rev-img', { duration: 1000 ,origin: 'left',mobile: false,reset: true}, 100);
        sr.reveal('.rev-info', { duration: 1500 ,origin: 'right',mobile: false,reset: true}, 100);
        sr.reveal('#news-section', { duration: 1000 ,origin: 'top',mobile: false,reset: true});
        sr.reveal('.news-left', { duration: 1000 ,origin: 'left',mobile: false,reset: true});
        sr.reveal('.news-box', { duration: 1500 ,origin: 'left',mobile: false,reset: true}, 100);
        sr.reveal('.news-right', { duration: 1000 ,origin: 'right',mobile: false,reset: true});
        sr.reveal('.news-detail', { duration: 1500 ,origin: 'right',mobile: false,reset: true}, 100);
        sr.reveal('#contact-section', { duration: 1000 ,origin: 'left',mobile: false,reset: true});
        sr.reveal('.cn-box', { duration: 1000 ,origin: 'right',mobile: false,reset: true}, 100);
        sr.reveal('#footer-top', { duration: 1000 ,origin: 'bottom',mobile: false,reset: true});
        sr.reveal('.tftr-box', { duration: 1000 ,origin: 'right',mobile: false,reset: true}, 100);
        sr.reveal('footer', { duration: 1000 ,origin: 'top',mobile: false,reset: true});
        sr.reveal('.footer-left', { duration: 1000 ,origin: 'left',mobile: false,reset: true}, 100);
        sr.reveal('.footer-right', { duration: 1500 ,origin: 'right',mobile: false,reset: true}, 100);
    </script>
@stop
