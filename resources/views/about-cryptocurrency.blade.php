@extends('layout.layout', ['body_class' => 'inner-page about-crypto-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>About Cryptocurrency</h1>
		</div>
	</div>
	<div id="inner-content">
		<div class="container">
			<h2 class="fancy-h2">What is Cryptocurrency?</h2>
			<p>Cryptocurrency is digital currency that employs encryption techniques to generate new currency. The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</p>
			<p>To date, the largest and most recognized type of cryptocurrency is Bitcoin. </p>
			<p>Cryptocurrency is a medium of exchange, and is used to make purchases while allowing you to monitor "wallet" transactions. The exchange rate for each type of coin is determined by that coin's particular supply and demand. Most cryptocurrencies are designed to gradually decrease in production which adds to their value. Bitcoins in existence, for example, are not expected to exceed a total of 21 million. </p>
			<p>Cryptocurrencies are not controlled by governments or boards and are therefore less susceptible to seizure. Alternatively, Fiat currencies—those a government declares to be legal tender, but not backed by a physical commodity, such as the US dollar, the British Pound, or the Euro— are supervised by a country's (or group of countries) financial regulating bodies. </p>
			<p>Cryptocurrencies can be exchanged via a digital currency wallet for other currencies, including Fiat currencies.</p>
			<p>Cryptocurrency legal statuses vary for every country. Some governments ban or impose restrictions on their use while others use them freely. </p>
			<div class="float-container clearfix">
				<div class="inr-left">
					<div class="vid-caption">
						<h2 class="fancy-h2">TPO – About Cryptocurrency</h2>
						<p>What are cryptocurrencies? These are digital currencies that use encryption techniques to generate new currencies and ensure the security of transactions. Cryptocurrency can be used to make purchases via the internet, for lower fees. Watch this video to learn more.</p>
					</div>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/6lq2LwAKUY0?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
			</div>

			<div class="float-container clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/Gc2en3nHxA4?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
				<div class="inr-right">
					<div class="vid-caption">
						<h2 class="fancy-h2">What is Bitcoin</h2>
						<p>Bitcoin is the first decentralized digital currency. Learn the basics of how Bitcoin works, its advantages, and how it's changing businesses and the financial world. </p>
					</div>
				</div>
			</div>

			<div class="float-container clearfix">
				<div class="inr-left">
					<div class="vid-caption">
						<h2 class="fancy-h2">How Bitcoin Works in 5 Minutes</h2>
						<p>This five-minute video introduction goes more into detail about how Bitcoins work, how transactions are verified, and how the integrity of digital transactions is ensured by a global community that maintains the Bitcoin "ledger." </p>
					</div>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/l9jOJk30eQs?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
			</div>

			<div class="float-container clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/Yncxg_IMQ1A?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
				<div class="inr-right">
					<div class="vid-caption">
						<h2 class="fancy-h2">About Digital Currencies</h2>
						<p>Learn about the advantages of digital currencies. These include lower fees, no account freezes, no arbitrary limits, and no restrictions on your account. This video also touches briefly on how digital coins are mined. </p>
					</div>
				</div>
			</div>

			<h2 id="cryptomining" class="fancy-h2">What is Cryptocurrency Mining?</h2>
			<p>Cryptocurrency transactions are validated by miners. Through the use of specially designed software, miners solve math problems to compute, process and verify digital currency transactions. When transactions are successfully computed, the miner is rewarded with coins. Problem difficulty determines how quickly each problem is solved with mining pools competing to problem solve.</p>
			<p>While governments and financial boards determine when to print additional Fiat currencies, this mining number-crunching system runs the cryptocurrency networks. Cryptocurrency mining is competitive in that mining pools try to beat others with faster problem solving. This system ensures fairness and keeps the network stable.</p>
			<div class="float-container clearfix">
				<div class="inr-left">
					<div class="vid-caption">
						<h2 class="fancy-h2">TPO – About Crypto Mining</h2>
						<p>Crypto mining involves the use of computer resources and specially designed software to verify digital currency transactions. The process known as "mining" is participated in by a global network of miners or mining pools.</p>
					</div>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/ZZUDZrwHDFM?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
			</div>

			<div class="float-container clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/uike95OzlNk?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
				<div class="inr-right">
					<div class="vid-caption">
						<h2 class="fancy-h2">What is Mining?</h2>
						<p>Where do Bitcoins come from? Digital currencies are generated through a mathematical process known as mining. The video talks about what mining involves and how miners are given incentives as they take on the responsibility of keeping Bitcoin transactions secure. </p>
					</div>
				</div>
			</div>

			<h2 id="cryptotrading"  class="fancy-h2">What is Cryptocurrency Trading?</h2>
			<p>Cryptocurrency trading is the Forex equivalent of cryptocurrencies. Apart from mining, trading is an opportunity to profit in the world of cryptocurrency, without the need for special hardware.</p>
			<p>Given all cryptocurrency trading is online, it is easy to enter and leave the market. And, as with Forex, the goal is to buy low, and sell high. Cryptocurrency trading takes time and patience, and also involves research and analysis of market trends.</p>
			<div class="float-container clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/EuzLoXdYo_Q?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
				<div class="inr-right">
					<div class="vid-caption">
						<h2 class="fancy-h2">About Crypto Trading</h2>
						<p>This explainer video tells you about the great opportunities in cryptocurrency trading. Sign up for a membership with TPO and we trade on your behalf.</p>
					</div>
				</div>
			</div>

			<h2 id="cryptoshuffleticket" class="fancy-h2">What is The CryptoShuffle Ticket?</h2>
			<p>The CryptoShuffle Ticket is a numbers game of chance for charity. Each purchased CryptoShuffle Ticket, generates a random alphanumeric value. Ticket numbers are also distributed in random order to guarantee that no "hacking" occurs. Security is of paramount importance throughout the cryptocurrency world.</p>
			<p>When all CryptoShuffle Ticket are sold, the assigned numbers are shuffled and a winner is chosen. After a winner is declared, the next shuffle begins. Part of the proceeds of the each CryptoShuffle Ticket shuffle are given to charity.</p>
			<div class="float-container clearfix">
				<div class="inr-left">
					<div class="vid-caption">
						<h2 class="fancy-h2">TPO-The CryptoShuffle Ticket</h2>
						<p>TPO offers The CryptoShuffle Ticket, which gives you a chance to win in the shuffles. Part of the proceeds are given to charity.</p>
					</div>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/ZWk_Ht6h5TY?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
				</div>
			</div>
		</div>
	</div>
@endsection