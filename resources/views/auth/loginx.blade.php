@extends('member.layouts.app')

@section('content')
<div class="login-logo">
    <img src="{{ asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div>

<div class="container">
    <div class="row">
        <div class="login-container">
            <h2>LOGIN</h2>

            <form class="form-horizontal member_login_form" method="POST" action="{{ route('member.login.ajax') }}">
                <div class="login-box">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <div class="field-container">
                            <label>Your username</label>
                            <input id="username" type="text" name="username" value="{{ old('username') }}" required autofocus placeholder="Username">
                            <span class="glyphicon glyphicon-user input-icon"></span>

                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="field-container">
                            <label>Your password</label>
                            <input id="password" type="password" name="password" required placeholder="Password">
                            <span class="glyphicon glyphicon-lock input-icon"></span>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="field-container row">
                        <div class="col-md-8 text-left nopadding">
                            <p>Have you forgotten your <a class="bold" href="{{ route('member.forgotPassword') }}">password?</a></p>
                            <p><a href="http://the-peoples-options.dev"><i class="fa fa-home"></i> Back to Home Page</a></p>
                        </div>
                        <div class="col-md-4 text-right nopadding">
                            <button type="submit" class="btn yellow-btn">Login</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script src="{{ asset('js/member/helper.js') }}"> </script>
    <script src="{{ asset('js/member/login.js') }}"> </script>

    <script type="text/javascript">
        var ajaxurl = "{{ route('member.login.ajax') }}";
    </script>
@stop
