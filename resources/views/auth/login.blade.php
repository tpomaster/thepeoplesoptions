@extends('member.layouts.app')

@section('pagespecificstyles')

@stop

@section('content')
<!-- <div class="login-logo">
    <img src="{{ asset('images/login-logo.png') }}" class="logo" alt="logo"/>
</div> -->

<div class="container">
    <div class="row">
        <div class="login-container xxx">
            <img src="{{ LpjHelpers::asset('images/login-logo.png') }}" class="logo" alt="logo"/>
            <h2 class="pull-right">LOGIN</h2>
            <div class="clearfix"></div>

            <h3>You are in a wrong login page</h3>
            <a class="btn theme-btn" href="https://thepeoplesoptions.com/member/login">Here is the correct login page.</a>
                
        </div>
    </div>
</div>

@include('member/includes/ajax-loader')

@endsection

@section('pagespecificscripts')
    <script type="text/javascript">
        jQuery(document).load(function(){
            window.location.href = 'https://thepeoplesoptions.com/member/login';
        });
    </script>
@stop
