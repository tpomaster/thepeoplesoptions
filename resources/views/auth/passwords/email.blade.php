@extends('member.layouts.app')

@section('content')
<div class="login-logo">
    <img src="{{asset('images/login-logo.png')}}" alt="">
</div>

<div class="container">
    <div class="row">
        <div class="login-title">
            <h2>Reset Password</h2>
        </div>
        <div class="login-container">
            <div class="login-box">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="field-container">
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required placeholder="E-Mail Address">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                            <button type="submit" class="btn yellow-btn">Send Password Reset Link</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
