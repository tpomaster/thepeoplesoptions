@extends('layout.layout', ['body_class' => 'inner-page ref-commissions-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>Referral Commissions</h1>
		</div>
	</div>
	<div id="inner-content">
		<div class="container">
			<p>Affiliate marketing is one of the most efficient methods of attracting new members to a company. By becoming a member of The People's Options, you automatically qualify to earn referral commissions. This is a mutually beneficial arrangement between TPO and members with additional bonuses offered for Platinum members.</p>
			<p>Personally referred members are members that have used your direct referral link to become a member.</p>
			<p>TPO has a Tiered referral system. Tier 1 is your personally referred member. Tier 2 is Tier 1's personally referred member. Tier 3 is Tier 2's personally referred member, and so on.</p>
			<p>As a free member of TPO, you are able to earn from your personally referred members (Tier 1) only.</p>
			<p>Free members earn from their personally referred members' purchases of The CryptoShuffle Ticket and funding of packages.</p>
			<p>Free members DO NOT earn from their personally referred members' purchase of a membership level (Silver, Gold or Platinum).</p>
			<p>As a Silver, Gold or Platinum level member, you are able to earn from your personally referred members (Tier 1), as well as from Tier 2 and Tier 3 for package funding and Tiers 2-6 for The CryptoShuffle Ticket Purchases. Note: The amount of Tiers may vary based on special packages.</p>
			<p>Silver, Gold or Platinum level members also earn from the purchase of a membership level based on their own membership level. (See below for clarification)</p>
			<br/>
			<h2 class="fancy-h2">As a Silver member, you will receive the following commissions for membership level purchases:</h2>

			<div class="silver-table table-overflow">
				<table class="responsive-table">
					<tr>
						<th></th>
						<th>Tier 1</th>
						<th>Tier 2</th>
						<th>Tier 3</th>
					</tr>
					<tr>
						<td>Silver</td>
						<td>$12.00</td>
						<td>$8.00</td>
						<td>$6.00</td>
					</tr>
					<tr>
						<td>Gold</td>
						<td>$12.00</td>
						<td>$8.00</td>
						<td>$6.00</td>
					</tr>
					<tr>
						<td>Platinum</td>
						<td>$12.00</td>
						<td>$8.00</td>
						<td>$6.00</td>
					</tr>
				</table>
			</div>
			<br/>

			<h2 class="fancy-h2">As a Gold member, you will receive the following commissions for membership level purchases:</h2>

			<div class="gold-table table-overflow">
				<table class="responsive-table">
					<tr>
						<th></th>
						<th>Tier 1</th>
						<th>Tier 2</th>
						<th>Tier 3</th>
					</tr>
					<tr>
						<td>Silver</td>
						<td>$12.00</td>
						<td>$8.00</td>
						<td>$6.00</td>
					</tr>
					<tr>
						<td>Gold</td>
						<td>$40.00</td>
						<td>$27.00</td>
						<td>$20.00</td>
					</tr>
					<tr>
						<td>Platinum</td>
						<td>$40.00</td>
						<td>$27.00</td>
						<td>$20.00</td>
					</tr>
				</table>
			</div>
			<br/>

			<h2 class="fancy-h2">As a Platinum member, you will receive the following commissions for membership level purchases:</h2>

			<div class="platinum-table table-overflow">
				<table class="responsive-table">
					<tr>
						<th></th>
						<th>Tier 1</th>
						<th>Tier 2</th>
						<th>Tier 3</th>
					</tr>
					<tr>
						<td>Silver</td>
						<td>$12.00</td>
						<td>$8.00</td>
						<td>$6.00</td>
					</tr>
					<tr>
						<td>Gold</td>
						<td>$40.00</td>
						<td>$27.00</td>
						<td>$20.00</td>
					</tr>
					<tr>
						<td>Platinum</td>
						<td>$120.00</td>
						<td>$80.00</td>
						<td>$60.00</td>
					</tr>
				</table>
			</div>

			<p>Platinum members are eligible to receive a Platinum Bonus. When a Platinum member refers 3 other Platinum members they will receive a $1,500 bonus. They will receive the bonus for every 3 Platinum members referred and renewed. There is no limit to the amount of time this bonus can be received.</p>
			<br/>

			<div class="clearfix">
				<div class="inr-left">
					<h2 class="fancy-h2">Commissions for Funding Mining Packages</h2>
					<p>Free members only earn from Tier 1 on these purchases. Silver, Gold and Platinum earn from all Tiers.</p>
					<p>Commissions and tiers for funding mining packages will vary based on special packages offered. Please make sure you read each package's terms to understand what the commission rate is for that specific package.</p>
					<p>The following commission table is for example purposes only and based on a Basic $25,000 Package:</p>
				</div>
				<div class="inr-right">
					<div class="normal-table table-overflow">
						<table class="responsive-table">
							<tr>
								<th>Tier</th>
								<th>Commission Amount</th>
							</tr>
							<tr>
								<td>1</td>
								<td>5%</td>
							</tr>
							<tr>
								<td>2</td>
								<td>3%</td>
							</tr>
							<tr>
								<td>3</td>
								<td>1%</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<br/>

			<div class="clearfix">
				<div class="inr-left">
					<h2 class="fancy-h2">Commissions for Funding Trading Packages</h2>
					<p>Free members only earn from Tier 1 on these purchases. Silver, Gold and Platinum earn from all Tiers.</p>
					<p>Commissions and tiers for funding trading packages will vary based on special packages offered. Please make sure you read each package's terms to understand what the commission rate is for that specific package.</p>
					<p>The following commission table is for example purposes only and based on a Basic $25,000 Package:</p>
				</div>
				<div class="inr-right">
					<div class="normal-table table-overflow">
						<table class="responsive-table">
							<tr>
								<th>Tier</th>
								<th>Commission Amount</th>
							</tr>
							<tr>
								<td>1</td>
								<td>5%</td>
							</tr>
							<tr>
								<td>2</td>
								<td>3%</td>
							</tr>
							<tr>
								<td>3</td>
								<td>2%</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<br/>

			<div class="clearfix">
				<div class="inr-left">
					<h2 class="fancy-h2">Commissions for The CryptoShuffle Ticket Purchases</h2>
					<p>Free members only earn from Tier 1 on these purchases. Silver, Gold and Platinum earn from all Tiers.</p>
					<p>Commissions and tiers for The CryptoShuffle Ticket purchases will vary based on special games offered. Please make sure you read each game's terms to understand what the commission rate is for that specific game.</p>
					<p>The following commission table is for example purposes only and based on a Basic $1 Game with 2,097,151 tickets sold:</p>
				</div>
				<div class="inr-right">
					<div class="normal-table table-overflow">
						<table class="responsive-table">
							<tr>
								<th>Tier</th>
								<th>Commission Amount</th>
							</tr>
							<tr>
								<td>1</td>
								<td>$0.0086</td>
							</tr>
							<tr>
								<td>2</td>
								<td>$0.0086</td>
							</tr>
							<tr>
								<td>3</td>
								<td>$0.00645</td>
							</tr>
							<tr>
								<td>4</td>
								<td>$0.0043</td>
							</tr>
							<tr>
								<td>5</td>
								<td>$0.0043</td>
							</tr>
							<tr>
								<td>6</td>
								<td>$0.01075</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection