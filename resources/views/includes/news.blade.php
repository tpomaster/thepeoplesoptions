<div id="news-section">
	<div class="container">
		<h2><span>THE LATEST FROM OUR PARTNERS</span></h2>
		<div class="clearfix">
			<div class="news-left">
				<div class="news-main">
					<!-- main news -->
					<?php if ( $mainNews ): ?>
						<div class="news-focus clearfix">
							<div class="desktop-news-bg" style="background: url('<?php echo $mainNews['getNewsImage']; ?>'); background-position: center; background-size: cover;">
								<img src="<?php echo $mainNews['getNewsImage']; ?>" class="detail-thumbs" alt="">
								<div class="focus-right">
									<h3><a data-fancybox data-src="#main_1" href="javascript:;" title="<?php echo $mainNews['title']; ?>"><?php echo $mainNews['title']; ?></a></h3>
									<h4><?php echo LpjHelpers::getExcerpt($mainNews['content'], 40); ?></h4>
								</div>
							</div>

							<div style="display: none;" id="main_1" class="FE-modal">
					            <div class="modal-header">
					              <h4 class="modal-title"><?php echo $mainNews['title']; ?></h4>
					            </div>
					            <div class="modal-body">
					              <?php echo $mainNews['content']; ?>

					              <hr/>
					              <p><strong>Source: <a target="_blank" href="<?php echo $mainNews['link']; ?>"><?php echo $mainNews['link']; ?></a></strong></p>
					            </div>
					        </div>
						</div>
					<?php endif; ?>

					<!-- subnews -->
					<?php if ( $subNews ): ?>
					<div class="news-main-thumbs">
						<?php $ctr = 0; ?>
						<?php foreach ($subNews as $k => $sNews) { ?>
							<div class="news-thumbs">
								<div class="subnews-img" style="background: url('<?php echo $sNews['getNewsImage']; ?>'); background-position: center; background-size: cover;"></div>
								<img src="<?php echo $sNews['getNewsImage']; ?>" class="detail-thumbs" alt="">
								<h3><a data-fancybox data-src="#sNews_<?php echo $ctr; ?>" href="javascript:;" title="<?php echo $sNews['title']; ?>"><?php echo LpjHelpers::getExcerpt($sNews['title'], 40); ?></a></h3>
								<p><?php echo LpjHelpers::getExcerpt($sNews['content'], 50); ?></p>

								<div style="display: none;" id="sNews_<?php echo $ctr; ?>" class="FE-modal">
						            <div class="modal-header">
						              <h4 class="modal-title"><?php echo $sNews['title']; ?></h4>
						            </div>
						            <div class="modal-body">
						              	<?php echo $sNews['content']; ?>

						              	<hr/>
					              		<p><strong>Source: <a target="_blank" href="<?php echo $sNews['link']; ?>"><?php echo $sNews['link']; ?></a></strong></p>
						            </div>
						        </div>
							</div>
					        <?php $ctr++; ?>
						<?php } ?>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="news-right">
				<!-- mininews -->
				<?php if ( $miniNews ): ?>
					<?php $ctr2 = 0; ?>
					<?php foreach ($miniNews as $k2 => $minNews) { ?>
						<div class="news-detail clearfix">
							<div class="mininews-img" style="background: url('<?php echo $minNews['getNewsImage']; ?>'); background-position: center; background-size: cover;"></div>
							<img src="<?php echo $minNews['getNewsImage']; ?>" class="detail-thumbs" alt="">
							<div class="detail-con">
								<h5><a data-fancybox data-src="#minNews_<?php echo $ctr2; ?>" href="javascript:;" title="<?php echo $sNews['title']; ?>"><?php echo $minNews['title']; ?></a></h5>
								<p><?php echo LpjHelpers::getExcerpt($minNews['content'], 50); ?></p>
							</div>

							<div style="display: none;" id="minNews_<?php echo $ctr2; ?>" class="FE-modal">
						        <div class="modal-header">
						          <h4 class="modal-title"><?php echo $minNews['title']; ?></h4>
						        </div>
						        <div class="modal-body">
						          	<?php echo $minNews['content']; ?>

						          	<hr/>
					          		<p><strong>Source: <a target="_blank" href="<?php echo $minNews['link']; ?>"><?php echo $minNews['link']; ?></a></strong></p>
						        </div>
						    </div>
						</div>
						<?php $ctr2++; ?>
					<?php } ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="exchage-rates">
			<?php /*/ ?>
			<!-- EXCHANGERATES.ORG.UK LIVE FOREX RATES TICKER START -->
				<script type="text/javascript">	
					var w = '800';
					var s = '1';
					var mbg = 'rgba(0, 0, 0, 0.4)';
					var bs = 'yes';
					var bc = 'FFFFFF';
					var f = 'verdana';
					var fs = '16px';
					var fc = '455A64';
					var lc = '455A64';
					var lhc = 'FE9A00';
					var vc = '0404FF';

					var ccHost = (("https:" == document.location.protocol) ? "https://www." : "http://www.");
					document.write(unescape("%3Cscript src='" + ccHost + "currency.me.uk/remote/CUK-LFOREXRTICKER-1.php' type='text/javascript'%3E%3C/script%3E"));
				</script>
			<!-- EXCHANGERATES.ORG.UK LIVE FOREX RATES TICKER END -->
			<?php /*/ ?>
			
			<script type="text/javascript">
			baseUrl = "https://widgets.cryptocompare.com/";
			var scripts = document.getElementsByTagName("script");
			var embedder = scripts[ scripts.length - 1 ];
			var cccTheme = {"General":{"enableMarquee":true}};
			(function (){
			var appName = encodeURIComponent(window.location.hostname);
			if(appName==""){appName="local";}
			var s = document.createElement("script");
			s.type = "text/javascript";
			s.async = true;
			var theUrl = baseUrl+'serve/v3/coin/header?fsyms=BTC,ETH,BTS&tsyms=USD,CNY,PHP,EUR,GBP';
			s.src = theUrl + ( theUrl.indexOf("?") >= 0 ? "&" : "?") + "app=" + appName;
			embedder.parentNode.appendChild(s);
			})();
			</script>
		</div>
	</div>
</div>