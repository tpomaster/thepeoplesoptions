<div id="testimonial-section">
	<div class="container">
		<h2><span>CLIENT TESTIMONIALS</span></h2>
	</div>
	<div class="testimonial-slider">
		<div class="review-box">
			<div class="container clearfix">
				<div class="rev-img">
					<img src="{{ LpjHelpers::asset('images/rev-img2.png') }}" alt="">
				</div>
				<div class="rev-info">
					<p>"I've funded several different packages with TPO and couldn't be more pleased with the performance of my packages. My earnings are posted every day and I can withdraw at any time. It doesn't get any better than that!" </p>
					<h3>Maurice Sterling</h3>
					<h4>Palatka, Florida</h4>
				</div>
			</div>
		</div>
		<div class="review-box">
			<div class="container clearfix">
				<div class="rev-img">
					<img src="{{ LpjHelpers::asset('images/rev-img2.png') }}" alt="">
				</div>
				<div class="rev-info">
					<p>"I absolutely love TPO! In the six months TPO has been managing my trading account, I have more than doubled my original funded amount. It has been an excellent source of passive income, and I highly recommend TPO to anyone who’s seeking new ways to earn passive income. I’ve seen nothing like it!"</p>
					<h3>Mike</h3>
				</div>
			</div>
		</div>
	</div>
</div>