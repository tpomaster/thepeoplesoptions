<div id="membership-section" class="section">
	<div class="container">
		<h2><span>CHOOSE A MEMBERSHIP</span></h2>
		<div class="membership-container">
			<div class="membership-box package1">
				<img src="{{ LpjHelpers::asset('images/memberPic1.png') }}" alt="crypto mining image">
				<h3><span>CRYPTO</span>MINING</h3>
				<ul>
					<li>EASY TO FUND</li>
					<li>SECURE AND PROFITABLE OPTIONS</li>
					<li>REAL TIME RESULTS</li>
					<li>DAILY PAYMENTS</li>
				</ul>
				<a href="{{ route('membership-packages') }}">SEE PACKAGES</a>
			</div>
			<div class="membership-box package2">
				<img src="{{ LpjHelpers::asset('images/memberPic2.png') }}" alt="crypto trading image">
				<h3><span>CRYPTO</span>TRADING</h3>
				<ul>
					<li>EASY TO FUND</li>
					<li>SECURE AND PROFITABLE OPTIONS</li>
					<li>FULL MANAGED ACCOUNTS</li>
					<li>DAILY PAYMENTS</li>
				</ul>
				<a href="{{ route('membership-packages') }}">SEE PACKAGES</a>
			</div>
			<div class="membership-box package3">
				<img src="{{ LpjHelpers::asset('images/memberPic3.png') }}" alt="cryptoshuffle ticket image">
				<h3><span>CRYPTOSHUFFLE</span>TICKET</h3>
				<ul>
					<li>EASY TO FUND</li>
					<li>SECURE</li>
					<li>PRIZE AMOUNTS VARY</li>
					<li>PORTION OF THE PROCEEDS GO TO CHARITY</li>
				</ul>
				<a href="{{ route('membership-packages') }}">SEE PACKAGES</a>
			</div>
			<div class="membership-box package4">
				<img src="{{ LpjHelpers::asset('images/memberPic4.png') }}" alt="crypto exchange image">
				<h3><span>CRYPTO</span>EXCHANGE</h3>
				<ul>
					<li>EASY TO FUND</li>
					<li>SECURE AND PROFITABLE OPTIONS</li>
					<li>FULL MANAGED ACCOUNTS</li>
					<li>DAILY PAYMENTS</li>
				</ul>
				<a href="{{ route('membership-packages') }}">SEE PACKAGES</a>
			</div>
		</div>
	</div>
</div>