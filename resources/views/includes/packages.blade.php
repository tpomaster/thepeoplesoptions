<div id="packages-section">
	<div class="container">
		<h2 class="section-heading"><span>MEMBERSHIP PACKAGES</span></h2>
		<div class="membership-info clearfix">
      <div class="mermbership-column column1">
        <div class="info-header">
          <img src="{{ LpjHelpers::asset('images/membership-logo.png') }}" alt="">
        </div>
        <div class="members-list">
          <ul>
            <li>Withdrawals from your account</li>
            <li>Withdrawal Limit</li>
            <li>Withdrawal Fee</li>
            <li class="spacing">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</li>
            <li class="spacing">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</li>
            <li>Management Fee</li>
            <li>Free Bitshares Registration</li>
            <li>Access to our CMS</li>
            <li>Special Packages</li>
            <li>Bonuses</li>
          </ul>
        </div>
        <!-- <div class="info-footer"></div> -->
      </div>
      <div class="mermbership-column column2 clearfix">
        <div class="info-header clearfix">
          <div class="mobile-info">
            <h2 class="membership-title">Silver Membership</h2>
            <h3 class="membership-sub"><span class="price-inline"><span class="membership-dollar">$</span>150</span><span class="price-inline price-small">.00<span class="year-label">/ YEAR</span></span><span class="sub-status">Yearly Membership Pricing</span></h3>
          </div>
          <!-- <div class="mobile-info info-btn">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
        <div class="togle-list">
          <div class="members-list">
            <ul>
              <li><span class="list-label">Withdrawals from your account</span>Two per month</li>
              <li><span class="list-label">Withdrawal Limit</span>Up to $2000 every month</li>
              <li><span class="list-label">Withdrawal Fee</span>at 4%</li>
              <li class="spacing"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Management Fee</span>Up to 35%</li>
              <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Access to our CMS</span>Partial Access</li>
              <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Bonuses</span><i class="fa fa-times-circle-o" aria-hidden="true"></i></li>
            </ul>
          </div>
          <!-- <div class="info-footer">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
      </div>
      <div class="mermbership-column column3 clearfix">
        <img src="{{ LpjHelpers::asset('images/hot.png') }}" alt="" class="membership-promo">
        <div class="info-header clearfix">
          <div class="mobile-info">
            <h2 class="membership-title">Platinum <span>Membership</span></h2>
            <h3 class="membership-sub"><span class="price-inline"><span class="membership-dollar">$</span>1500</span><span class="price-inline price-small">.00<span class="year-label">/ YEAR</span></span><span class="sub-status">Yearly Membership Pricing</span></h3>
          </div>
          <!-- <div class="mobile-info info-btn">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
        <div class="togle-list">
          <div class="members-list">
            <ul>
              <li><span class="list-label">Withdrawals from your account</span>Daily</li>
              <li><span class="list-label">Withdrawal Limit</span>No Limits</li>
              <li><span class="list-label">Withdrawal Fee</span>at 2%</li>
              <li class="spacing"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Management Fee</span>Up to 25%</li>
              <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Access to our CMS</span>Full Access</li>
              <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Bonuses</span>Platinum Bonus of $1500</li>
            </ul>
          </div>
          <!-- <div class="info-footer">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
            <p>Platinum members are eligible to receive a Platinum Bonus. </p>
          </div> -->
        </div>
      </div>
      <div class="mermbership-column column4 clearfix">
        <div class="info-header clearfix">
          <div class="mobile-info">
            <h2 class="membership-title">Gold Membership</h2>
            <h3 class="membership-sub"><span class="price-inline"><span class="membership-dollar">$</span>500</span><span class="price-inline price-small">.00<span class="year-label">/ YEAR</span></span><span class="sub-status">Yearly Membership Pricing</span></h3>
          </div>
          <!-- <div class="mobile-info info-btn">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
        <div class="togle-list">
          <div class="members-list">
            <ul>
              <li><span class="list-label">Withdrawals from your account</span>Weekly</li>
              <li><span class="list-label">Withdrawal Limit</span>Up to $3000 every month</li>
              <li><span class="list-label">Withdrawal Fee</span>at 3%</li>
              <li class="spacing"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Management Fee</span>Up to 30%</li>
              <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Access to our CMS</span>Medium Level Access</li>
              <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Bonuses</span><i class="fa fa-times-circle-o" aria-hidden="true"></i></li>
            </ul>
          </div>
          <!-- <div class="info-footer">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
      </div>
      <div class="mermbership-column column5 clearfix">
        <div class="info-header clearfix">
          <div class="mobile-info">
            <h2 class="membership-title">Bronze Membership</h2>
            <h3 class="membership-sub"><span class="price-inline">FREE</span><span class="sub-status">Yearly Membership Pricing</span></h3>
          </div>
          <!-- <div class="mobile-info info-btn">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
        <div class="togle-list">
          <div class="members-list">
            <ul>
              <li><span class="list-label">Withdrawals from your account</span>One per month</li>
              <li><span class="list-label">Withdrawal Limit</span>Up to $1000 every month</li>
              <li><span class="list-label">Withdrawal Fee</span>at 5%</li>
              <li class="spacing2"><span class="list-label">Earn commission from your personal referrals, Tier 2 and Tier 3 Mining and Trading Packages.</span>Earn partially from your personal referrals</li>
              <li class="spacing"><span class="list-label">Earn commissions on The CryptoShuffle Ticket purchases and package purchases.</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Management Fee</span>Up to 45%</li>
              <li><span class="list-label">Free Bitshares Registration</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Access to our CMS</span>No Access</li>
              <li><span class="list-label">Special Packages</span><i class="fa fa-check-circle-o" aria-hidden="true"></i></li>
              <li><span class="list-label">Bonuses</span><i class="fa fa-times-circle-o" aria-hidden="true"></i></li>
            </ul>
          </div>
          <!-- <div class="info-footer">
            <a href="">JOIN NOW <span class="play-btn"><i class="fa fa-play-circle" aria-hidden="true"></i></span><span class="ftr-btn-info">Comes with special packages</span></a>
          </div> -->
        </div>
      </div>
    </div>
	</div>
</div>