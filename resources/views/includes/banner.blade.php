<div id="banner" class="section">
    <div class="bnr-slider">
	  <div class="bnr">
	  	<img src="{{ LpjHelpers::asset('images/banner.jpg') }}" class="bnr-big" alt="banner desktop photo"/>
	  	<div class="bnr-container">
		  	<div class="container clearfix">
		  		<div class="bnr-con">
		  			<img src="{{ LpjHelpers::asset('images/banner-crypto.png') }}" class="bnr-img" alt="crypto trading text image"/>
		  			<h2>The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</h2>
		  			<a href="{{ route('membership-packages') }}">CHOOSE PACKAGES</a>
		  		</div>
		  	</div>
		</div>
	  </div>
	  <div class="bnr">
	  	<img src="{{ LpjHelpers::asset('images/bnr2.jpg') }}" class="bnr-big" alt="banner planning photo"/>
	  	<div class="bnr-container">
		  	<div class="container clearfix">
		  		<div class="bnr-con">
		  			<img src="{{ LpjHelpers::asset('images/banner-crypto.png') }}" class="bnr-img" alt="crypto trading text image"/>
		  			<h2>The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</h2>
		  			<a href="{{ route('membership-packages') }}">CHOOSE PACKAGES</a>
		  		</div>
		  	</div>
		</div>
	  </div>
	  <div class="bnr">
	  	<img src="{{ LpjHelpers::asset('images/bnr3.jpg') }}" class="bnr-big" alt="banner ticket photo"/>
	  	<div class="bnr-container">
		  	<div class="container clearfix">
		  		<div class="bnr-con">
		  			<img src="{{ LpjHelpers::asset('images/banner-crypto.png') }}" class="bnr-img" alt="crypto trading text image"/>
		  			<h2>The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</h2>
		  			<a href="{{ route('membership-packages') }}">CHOOSE PACKAGES</a>
		  		</div>
		  	</div>
		</div>
	  </div>
	  <div class="bnr">
	  	<img src="{{ LpjHelpers::asset('images/bnr4.jpg') }}" class="bnr-big" alt="banner mining photo"/>
	  	<div class="bnr-container">
		  	<div class="container clearfix">
		  		<div class="bnr-con">
		  			<img src="{{ LpjHelpers::asset('images/banner-crypto.png') }}" class="bnr-img" alt="crypto trading text image"/>
		  			<h2>The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</h2>
		  			<a href="{{ route('membership-packages') }}">CHOOSE PACKAGES</a>
		  		</div>
		  	</div>
		</div>
	  </div>
	  <div class="bnr">
	  	<img src="{{ LpjHelpers::asset('images/bnr5.jpg') }}" class="bnr-big" alt="banner graph photo"/>
	  	<div class="bnr-container">
		  	<div class="container clearfix">
		  		<div class="bnr-con">
		  			<img src="{{ LpjHelpers::asset('images/banner-crypto.png') }}" class="bnr-img" alt="crypto trading text image"/>
		  			<h2>The use of encryption technology ensures transaction security. Person-to-person transfers transpire via the Internet thus requiring lower fees than traditional transactions.</h2>
		  			<a href="{{ route('membership-packages') }}">CHOOSE PACKAGES</a>
		  		</div>
		  	</div>
		</div>
	  </div>
	</div>
</div>