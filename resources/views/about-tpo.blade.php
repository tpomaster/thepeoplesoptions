@extends('layout.layout', ['body_class' => 'inner-page about-tpo-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>About TPO</h1>
		</div>
	</div>
	<div id="inner-content">
		<div class="container">
			<h2>THE PEOPLE'S OPTIONS – Your Cryptocurrency Solution</h2>
			<p>The People's Options (TPO) offers profitable and secure solutions to build financial success. TPO was founded by Cryptocurrency and Forex professionals and venture capitalists as an online, members only organization.</p>
			<p>TPO's plans and products, unparalleled security, and solid industry reputation deliver <strong>the best trading options</strong> available in the Cryptocurrency industry.</p>
			<p>For TPO members, success is straightforward. Sign up for a free account, make a deposit, and leave the trading to us.</p>
			<br/>
			<h2>TPO offers members the following services:</h2>
			<ul>
				<li><strong>Cryptocurrency Mining</strong><br>
					We negotiate the best packages for mining for you. </li>
				<li><strong>Cryptocurrency Trading</strong><br>
					We fully manage your accounts. Just decide which packages are right for you, fund them, and let our skilled and experienced team do the trading on your behalf. We offer long term and short term options. </li>
				<li><strong>The CryptoShuffle Ticket</strong><br/>
					We believe in giving back. Part of the proceeds from The CryptoShuffle Ticket draws are given to charity.</li>
			</ul>
			<br/>
			<h2>Join TPO today, you will surely be satisfied!</h2>
			<div class="float-container clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/gP2O9Mcnb-Q?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
					<h2 class="fancy-h2">About The People's Options</h2>
					<p>The People's Options (TPO) is a membership site to help you capitalize on cryptocurrencies. We negotiate the best packages on cryptocurrency mining. TPO also fully manages your cryptocurrency trading account. </p>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/PGGz8GFQeBw?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
					<h2 class="fancy-h2">TPO Membership Packages</h2>
					<p>Interested in signing up for membership with TPO? Find out more about our packages on cryptocurrency mining memberships and crypto trading, and how The CryptoShuffle Ticket works. </p>
				</div>
			</div>

			<h2>TPO uses BitShares exclusively:</h2>
			<p>TPO has chosen to use BitShares (BTS) exclusively for payments and withdrawals. The BitShares platform offers easy and secure transactions, is far superior, and has the highest performance compared to ALL other platforms. For example, the BitShares platform can perform 4,000 transactions per second in comparison to Bitcoin’s 7 transactions per second. BitShares is also able to scale to 100,000 transactions per second. Bitshares does not have counter-party risk and does not have to be backed by anything external to the system. The BitShares platform also gives access to all other currencies (either fiat or digital) at lightspeed. Most members have a coin they like. The great thing about the BitShares platform is that our members are able to go from BitShares to any other coin easily, usually within 3 seconds. BitShares also offers SmartCoins (example: bitUSD and bitGOLD) which are backed by BitShares (shares in the Bitshares company itself) but hold a stable value. If members want the freedom of cryptocurrency with less volatility, they can move their BitShares to the SmartCoin of their choice. </p>

			<div class="clearfix">
				<div class="inr-left">
					<iframe src="https://www.youtube.com/embed/UJYLWow7K6o?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
					<h2 class="fancy-h2">TPO Uses BitShares Exclusively</h2>
					<p>Find out why TPO has chosen to use BitShares exclusively for payments and withdrawals.</p>
				</div>
				<div class="inr-right">
					<iframe src="https://www.youtube.com/embed/UJYLWow7K6o?rel=0&amp;showinfo=0" allowfullscreen="" class="video"></iframe>
					<h2 class="fancy-h2">Intro to BitShares</h2>
					<p>A brief overview of some of the key features that make Bitshares stand out from other blockchain technologies. </p>
				</div>
			</div>
		</div>
	</div>
@endsection