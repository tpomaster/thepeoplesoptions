@extends('layout.layout', ['body_class' => 'inner-page faq-page'])

@section('pagespecificstyles')

    <!-- slider css-->
    <link rel="stylesheet" type="text/css" href="{{ LpjHelpers::asset('css/front-end/inner.css') }}"/>

@stop

@section('main_content')
	<div id="inner-banner">
		<div class="container">
			<h1>Frequently Asked Questions</h1>
		</div>
	</div>
	<div id="inner-content">
		<div class="container">
			<div class="accordion">
				<h2>Membership and Packages</h2>
			    <div class="accordion-head">
			        <h4>Who can become a member?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Membership is by invitation only. You’ll need an invitation or a referral link from an existing member to join.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Is there a minimum age to be a member?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Any person of legal age to enter into a contract is eligible to become a member.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Why is it by invitation only and why can't I join directly?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Our priority is to provide the best packages for our members. Marketing TPO to the public distracts us from this. Therefore, we prefer to have our existing members invite others into the system.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How much does it cost to be a member?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Go to our membership page for more information on our various products. There is a free membership, along with various paid memberships. <a class="red" href="{{ route('membership-packages') }}">Click this link</a> to view our membership packages offered.</p>
			    </div>
				<br/>
			    <h2>Referral Program</h2>
			    <div class="accordion-head">
			        <h4>What membership level do I need to be to earn from referrals?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>All membership levels earn from their direct referrals. Free members will only earn from purchases of Mining or Trading Packages and The Hope Ticket. Free members DO NOT earn from membership package purchases. Individuals with a paid membership may also earn income from additional tiers within the referral program that free members are not able to participate in. <a class="red" href="{{ route('referral-commissions') }}">Click here</a> for more information on the referral commissions. </p>
			    </div>
			    <br/>
			    <h2>Funds</h2>
			    <div class="accordion-head">
			        <h4>What types of payment are accepted to purchase membership or package?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>You'll need Bitshares to fund your account, you can check out our videos that explain the process of how to set up and fund your wallet.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How fast are deposits made?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>It depends on how fast the funds are transferred from your preferred wallet (e.g. Coinbase, Blockchain, etc.) to your Bitshares wallet. Once the funds are in your Bitshares wallet, you'll transfer it to your TPO account (this step takes a couple of minutes). Now you have funds that you can use to buy a membership and/or fund packages.</p>
			    </div>
			    <br/>
			    <h2>Crypto Trading and Mining</h2>
			    <div class="accordion-head">
			        <h4>Do you list daily returns?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>No, we are not a HYIP opportunity (High-yield investment program) and we can not guarantee daily returns. You may check the membership area every day after 12 am EST to see the day's returns. This is based on the performance of the trading team's daily real trades on the package you funded.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How do you calculate the earnings each day?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>The daily earnings are based on the trading profits for the day minus the management fee.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How soon does my account start earning?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>This depends on the package you've purchased. Most trading packages begin to earn profits in as little as 48 hours. Mining packages take longer to start earning.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>When are payouts made?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Payouts are made daily at 12 am EST. Members should log into their account daily and decide what to do with their funds. You have the option to withdraw the funds, keep it in referral credits, or push it to your TPO bank.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Do you charge withdrawal fees?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Yes, withdrawal fees depend on your membership level/package.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Why are there different levels of withdrawal fees?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>The annual membership fees help TPO cover operating expenses. Members who pay low membership fees are charged higher management and withdrawal fees.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How soon are requests for withdrawal processed?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Withdrawal requests are processed as soon as you hit the button. TPO verifies the transaction and once the verification is complete the funds are transferred to your Bitshares wallet. Verification time depends on the amount of withdrawal. </p>
			    </div>
			    <div class="accordion-head">
			        <h4>Do I get my deposit back at the end of a package?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>It depends on the type of package that you signed up for. Those who funded a trading package receive their deposit back at the end of the package term. For mining packages, it depends on whether it’s a package that is included with the membership fee. Deposits are not returned for regular mining packages and neither is it returned for The Hope Ticket.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>How soon can I earn my deposit back?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>There is no definite answer for this since we do real trading. Therefore earnings and losses vary every day. It also depends on the type of package you funded, whether you receive your deposit back.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Who manages our trading accounts?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>We have a team of trading experts who manage our trading packages that are funded.</p>
			    </div>
			    <br/>
			    <h2>General Account Questions</h2>
			    <div class="accordion-head">
			        <h4>I've forgotten my password, what should I do?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Click on the forgot password link, enter your username or e-mail and you'll receive instructions in your email on how to change your password. (NOTE: If you do not receive the email, check junk mail to ensure email was not routed there.)</p>
			    </div>
			    <div class="accordion-head">
			        <h4>What can I do if I keep having problems with my account?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>You can contact our support team here. We typically reply to support tickets within 24-48 hours but we do our best to provide support to our members as quickly as possible.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Can I have more than one account?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>No. Members who have more than one account will have their funds frozen on all of their accounts. This will affect the member's accrued referral commissions, earnings, and account balances.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Are relatives or roommates who share the same address allowed to have accounts?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Yes, as long as each person has their own account.</p>
			    </div>
			    <div class="accordion-head">
			        <h4>Can companies sign up for an account?</h4><div class="arrow down"></div>
			    </div>
			    <div class="accordion-body">
			        <p>Yes, businesses and other legal entities are allowed to have 1 account.</p>
			    </div>
			</div>
		</div>
	</div>
@endsection

@section('pagespecificscripts')
    <script type="text/javascript">
        jQuery('.accordion').each(function () {
		    var $accordian = jQuery(this);
		    $accordian.find('.accordion-head').on('click', function () {
	            jQuery(this).parent().find(".accordion-head").removeClass('open close');
	            jQuery(this).removeClass('open').addClass('close');
		        $accordian.find('.accordion-body').slideUp();
		        if (!jQuery(this).next().is(':visible')) {
	                jQuery(this).removeClass('close').addClass('open');
		            jQuery(this).next().slideDown();
		        }
		    });
		});
    </script>
@stop