let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/member/app.js', 'public/js/member')
   .sass('resources/assets/sass/member/app.scss', 'public/css/member')
   .sass('resources/assets/sass/member/login-style.scss', 'public/css/member')
   .sass('resources/assets/sass/member/my-account.scss', 'public/css/member')
   .sass('resources/assets/sass/member/tree-view.scss', 'public/css/member')
   .sass('resources/assets/sass/member/bank.scss', 'public/css/member')
   .sass('resources/assets/sass/member/membership.scss', 'public/css/member')
   .sass('resources/assets/sass/member/members.scss', 'public/css/member')
   .sass('resources/assets/sass/member/referral.scss', 'public/css/member')
   .sass('resources/assets/sass/member/faq.scss', 'public/css/member')
   .sass('resources/assets/sass/member/cryptotrading-packages.scss', 'public/css/member')
   .sass('resources/assets/sass/front-end/style.scss', 'public/css/front-end')
   .sass('resources/assets/sass/front-end/inner.scss', 'public/css/front-end');

// Images
mix.copy('resources/assets/images', 'public/images');

// Additional Scripts and CSS
mix.copy('resources/assets/js/main.js', 'public/js')
   .copy('resources/assets/js/slick.min.js', 'public/js')
   .copy('resources/assets/js/skrollr.min.js', 'public/js')
   .copy('resources/assets/js/scrollreveal.min.js', 'public/js')
   .copy('resources/assets/js/jquery.js', 'public/js')
   .copy('resources/assets/js/jquery.migrate.js', 'public/js')
   .copy('resources/assets/js/jquery.fancybox.min.js', 'public/js')
   .copy('resources/assets/js/jquery.sidr.min.js', 'public/js')
   .copy('resources/assets/js/member/dashboard.js', 'public/js/member')
   .copy('resources/assets/js/member/icheck.min.js', 'public/js/member')
   .copy('resources/assets/js/member/helper.js', 'public/js/member')
   .copy('resources/assets/js/member/login.js', 'public/js/member')
   .copy('resources/assets/js/member/register.js', 'public/js/member')
   .copy('resources/assets/js/member/tock.min.js', 'public/js/member')
   .copy('resources/assets/css/ajax.css', 'public/css')
   .copy('resources/assets/css/slick.css', 'public/css/front-end')
   .copy('resources/assets/css/slick-theme.css', 'public/css/front-end')
   .copy('resources/assets/css/jquery.sidr.dark.min.css', 'public/css/front-end')
   .copy('resources/assets/css/jquery.fancybox.min.css', 'public/css/front-end')
   .copy('resources/assets/css/overwrite.css', 'public/css');

// Fonts
mix.copy('resources/assets/sass/fonts', 'public/fonts');

// Gentelella
// mix.copy('node_modules/gentelella/build/js/custom.min.js', 'public/js/gentelella.min.js')
    // .copy('node_modules/gentelella/build/css/custom.min.css', 'public/css/gentelella.min.css');
mix.styles([
    'node_modules/gentelella/build/css/custom.min.css',
    'node_modules/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css',
    'node_modules/gentelella/vendors/pnotify/dist/pnotify.css',
    'node_modules/gentelella/vendors/pnotify/dist/pnotify.buttons.css',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
], 'public/css/gentelella.min.css');

mix.scripts([
    'node_modules/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js',
    'node_modules/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
    'node_modules/gentelella/vendors/pnotify/dist/pnotify.js',
    'node_modules/gentelella/vendors/pnotify/dist/pnotify.buttons.js',
    'node_modules/gentelella/vendors/moment/min/moment.min.js',
    'node_modules/gentelella/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    'node_modules/gentelella/build/js/custom.js',
], 'public/js/gentelella.min.js');

/**
 * Admin Styles and Scripts
 */

mix
.sass('resources/assets/sass/admin/app.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/login-style.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/package-info.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/package-queue.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/announcement.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/news.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/bank.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/credits.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/faq.scss', 'public/css/admin')
.sass('resources/assets/sass/admin/payout.scss', 'public/css/admin');

/* Admin Scripts */
mix.scripts(['resources/assets/js/admin/helper.js'], 'public/js/admin/helper.js');

mix
.js('resources/assets/js/admin/app.js', 'public/js/admin')
.js('resources/assets/js/admin/dashboard.js', 'public/js/admin')
.js('resources/assets/js/admin/login.js', 'public/js/admin')
.js('resources/assets/js/admin/packages.js', 'public/js/admin')
.js('resources/assets/js/admin/package-queue.js', 'public/js/admin')
.js('resources/assets/js/admin/bank.js', 'public/js/admin')
.js('resources/assets/js/admin/news.js', 'public/js/admin')
.js('resources/assets/js/admin/announcement.js', 'public/js/admin')
.js('resources/assets/js/admin/faq.js', 'public/js/admin')
.js('resources/assets/js/admin/affiliates.js', 'public/js/admin')
.js('resources/assets/js/admin/affiliates-list.js', 'public/js/admin')
.js('resources/assets/js/admin/referral-credits.js', 'public/js/admin')
.js('resources/assets/js/admin/settings.js', 'public/js/admin')
.js('resources/assets/js/admin/payouts.js', 'public/js/admin')
.js('resources/assets/js/admin/admins.js', 'public/js/admin');


// Packages Scripts
mix.js('resources/assets/js/admin/packages/search-filter.js', 'public/js/admin/packages')

// Crypto Trading Scripts
mix
.js('resources/assets/js/admin/packages/crypto-trading/show.js', 'public/js/admin/packages/crypto-trading')
.js('resources/assets/js/admin/packages/crypto-trading/trades-queue.js', 'public/js/admin/packages/crypto-trading');

// Crypto Mining Scripts
mix
.js('resources/assets/js/admin/packages/crypto-mining/show.js', 'public/js/admin/packages/crypto-mining')
.js('resources/assets/js/admin/packages/crypto-mining/profits-queue.js', 'public/js/admin/packages/crypto-mining');

// Crypto Exchange Scripts
mix
.js('resources/assets/js/admin/packages/crypto-exchange/show.js', 'public/js/admin/packages/crypto-exchange')
.js('resources/assets/js/admin/packages/crypto-exchange/profits-queue.js', 'public/js/admin/packages/crypto-exchange')

// Crypto Shuffle Scripts
mix
.js('resources/assets/js/admin/packages/crypto-shuffle/show.js', 'public/js/admin/packages/crypto-shuffle');

// Crypto Membership Scripts
mix
.js('resources/assets/js/admin/packages/membership/show.js', 'public/js/admin/packages/membership');

/* CKEditor 4 */
/*mix.scripts([
    'node_modules/ckeditor/ckeditor.js',
    'node_modules/ckeditor/adapters/jquery.js',
], 'public/js/admin/ckeditor.min.js');*/

mix
.copyDirectory('node_modules/tinymce', 'public/js/admin/vendors/tinymce');
